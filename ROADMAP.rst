This file contains some items that are left to do in this code to make it nicer
to work with and maybe usable for practical applications.

Improvements
============

- Need some examples / tutorials of how to make use of the code.

- A way to define an optimization problem symbolically would be great.

- A way to define the adjoint problem symbolically would be great. This would
  need to be based on the optimization problem somehow.

- The way that shape derivatives are handled is very manual. Can these be
  handled symbolically with a nice mapper?

- More advanced checkpointing would be great. This would likely need to be
  incorporated into the optimization and time stepping routines.

- The time stepping routines and classes are a mess.

- Solving a Stokes problem with a non-unitary viscosity ratio is still very
  slow. Would need a lot of improvements to make it viable for evolution
  equations.

Outsourcing
===========

Optimization
------------

The ``pystopt.optimize`` contains a pretty nice interface for optimization that
is just waiting to be extended to manifolds and other fancy things like that.

This would probably benefit from being a separate project.

Meshing
-------

``pystopt.mesh`` contains some cool hacks to make spectral meshes work as a
``meshmode.mesh.Mesh``.

These could probably be separated into another package.

pytential
---------

There are some things in ``pystopt.symbolic``, ``pystopt.qbx`` and ``pystopt.stokes``
that would probably benefit from being upstreamed to ``pytential``.

Some of that requires a rework of how pytential / pymbolic work. The main issue
seems to be to inject additional mappers into the code that understand the
additional variables.

Maybe worth making some sort of registry?
