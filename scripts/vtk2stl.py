# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

import pathlib

SUPPORTED_EXTENSIONS = (".vtu", ".hdf")


def convert(infile: pathlib.Path, *, overwrite: bool = False) -> None:
    import pyvista as pv

    outfile = infile.with_suffix(".stl")
    if not overwrite and outfile.exists():
        raise FileExistsError(outfile)

    print(f"Converting '{infile.name}' to '{outfile.name}'")
    mesh = pv.read(infile)
    dataset = mesh.extract_geometry()
    dataset.save(outfile)


class Processor:
    def __init__(self, *, overwrite: bool = False) -> None:
        self.overwrite = overwrite

    def __call__(self, infile) -> None:
        return convert(infile, overwrite=self.overwrite)


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("path", type=pathlib.Path)
    parser.add_argument("--overwrite", action="store_true")
    parser.add_argument("--no-parallel", action="store_true")
    parser.add_argument(
        "-j", "--jobs", type=int, default=4, help="(throttled to number of cpus)"
    )
    args = parser.parse_args()

    if args.path.is_dir():
        paths = tuple([
            path
            for path in args.path.iterdir()
            if path.is_file() and path.name[0] != "."
        ])
    else:
        paths = (args.path,)

    paths = tuple([path for path in paths if path.suffix in SUPPORTED_EXTENSIONS])
    if not paths:
        print("No (valid) files given...")
        raise SystemExit()

    print("Processing...")
    if not args.no_parallel and len(paths) > 16:
        from multiprocessing import Pool, cpu_count

        max_cpu_count = cpu_count()

        processes = args.jobs
        if processes <= 0 or processes > max_cpu_count:
            processes = max_cpu_count

        with Pool(processes=processes) as pool:
            result = pool.map_async(Processor(args.overwrite), paths)
            result.wait()
    else:
        for path in paths:
            convert(path, overwrite=args.overwrite)
