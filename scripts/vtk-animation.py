# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

import pathlib

import numpy as np

from pystopt.tools import get_default_logger

logger = get_default_logger(__file__)


def make_movie(
    filenames: list[pathlib.Path],
    *,
    dataset: str | None = None,
    output: pathlib.Path | None = None,
) -> None:
    try:
        import vedo
    except ImportError:
        logger.info("this script requires 'vedo'")
        return

    if output is None:
        output = filenames[0].parent / "movie.mp4"

    vp = vedo.Plotter(offscreen=True)
    video = vedo.Video(str(output), duration=20, backend="ffmpeg")

    from pystopt.paths import relative_to

    available_datasets = None
    world = None

    for i, filename in enumerate(filenames):
        logger.info("%s", relative_to(filename))

        mesh = vedo.Mesh(str(filename))
        mesh.rotateX(-np.pi / 2, rad=True)
        mesh.addShadow(y=-2)

        # {{{ import dataset

        if available_datasets is None:
            available_datasets = mesh.pointdata.keys()

        if dataset is not None:
            if dataset not in available_datasets:
                raise KeyError(
                    f"dataset '{dataset}' not in file; available datasets are "
                    f"{available_datasets}"
                )

            mesh.cmap("jet", mesh.pointdata[dataset])
        else:
            mesh.texture("water")
            mesh.alpha(0.95)

        # }}}

        # {{{ determine world bounds

        if world is None:
            bounds = np.array(mesh.GetBounds())
            position = np.array(mesh.GetPosition())

            # FIXME: width is hardcoded
            center = position + np.array([0, 1, 0])
            width = (bounds[1::2] - bounds[::2]) + (0, 3, 0)
            world = vedo.Box(tuple(center), size=tuple(width), alpha=0.1).wireframe()

        # }}}

        vp.show(world, mesh, resetcam=i == 0, interactive=False)
        video.addFrame()

    video.close()


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("filenames", nargs="+", type=pathlib.Path)
    parser.add_argument("-o", "--output", default=None, type=pathlib.Path)
    parser.add_argument("-d", "--dataset", default=None)
    args = parser.parse_args()

    make_movie(args.filenames, output=args.output, dataset=args.dataset)
