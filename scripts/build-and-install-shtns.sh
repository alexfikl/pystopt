#!/bin/bash
# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

# {{{ setup

set -o errexit
set -o nounset
set -o pipefail

echo "Virtual environment: ${VIRTUAL_ENV}"

# }}}

# {{{ description

pkgname=shtns
pkgver=3.7.3
archive="${pkgname}-${pkgver}.tar.gz"
archive="4e04fba84ea156974df5edaf4ee856c0f4f86e77.tar.gz"
url="https://bitbucket.org/nschaeff/${pkgname}/get/${archive}"

# }}}

builddir=$(mktemp --directory --suffix .${pkgname}.${pkgver})
pushd "${builddir}"

# {{{ download and extract

echo "Downloading '${url}'..."
curl -L -O --retry 3 --retry-delay 3 -o "${archive}" "${url}"

echo 'Extracting...'
tar -xf "${archive}"

# }}}

# {{{ build and install

pushd "nschaeff-${pkgname}-${archive:0:12}"

echo 'Configuring...'
./configure \
  --prefix="$VIRTUAL_ENV/usr" \
  --enable-openmp \
  --disable-f77 \
  --enable-python

echo 'Building...'
make -j4

echo 'Installing...'
python -m pip install .

popd

# }}}

popd
