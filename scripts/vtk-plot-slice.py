# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

import pathlib

import numpy as np


def plot(
    infile: pathlib.Path,
    outfile: pathlib.Path | None = None,
    fields: tuple[str, ...] | None = None,
    ylim: tuple[float, float] | None = None,
    ylabel: str | None = None,
) -> None:
    if outfile is None:
        outfile = infile.with_suffix("")

    import pyvista as pv

    mesh = pv.read(infile)
    mesh_y_slice = mesh.slice(normal=(0, -1, 0), origin=(0, 0, 0))

    x, y = mesh_y_slice.points[:, (0, 2)].T
    theta = 2.0 * np.pi * (y < 0) + np.arctan2(y, x)

    indices = np.argsort(theta)
    theta = theta[indices]

    datasets = []
    axis = ["x", "y", "z"]
    for name in mesh_y_slice.point_data:
        if fields is not None and name in fields:
            value = mesh.point_data[name]
            if value.ndim == 1:
                datasets.append((name, value[indices]))
            elif value.ndim == 2:
                for i in range(value.shape[-1]):
                    datasets.append(f"{name}_{axis[i]}", value[indices, i])
            else:
                raise ValueError(f"unsupported shape: {value.shape}")

    from pystopt.visualization.matplotlib import subplots

    with subplots(outfile, overwrite=True) as fig:
        ax = fig.gca()

        for name, f in datasets:
            ax.plot(theta, f, label=name)

        ax.set_xlabel(r"$\theta$")

        if ylim is not None:
            ax.set_ylim(ylim)

        if ylabel is not None:
            ax.set_ylabel(ylabel)
        else:
            ax.legend()


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("filename", type=pathlib.Path, help="filename containing data")
    parser.add_argument("-f", "--fields", nargs="+", help="name of the fields to plot")
    parser.add_argument(
        "-o", "--output", type=pathlib.Path, help="filename containing data"
    )
    parser.add_argument(
        "--ylim", nargs=2, type=float, default=None, help="limits in the y axis"
    )
    parser.add_argument("--ylabel", default=None, help="labels for the dataset")
    args = parser.parse_args()

    plot(
        args.filename,
        outfile=args.output,
        fields=tuple(args.fields),
        ylim=tuple(args.ylim) if args.ylim else args.ylim,
        ylabel=args.ylabel,
    )
