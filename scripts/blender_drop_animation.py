# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

"""
Description

The render environment should be completely set up for the STL to simply be
loaded and then rendered. This includes all lighting and other features in the
scene ready to go. The material must be created with the name given below
assigned to mat. Images will be rendered in a sequence to later be combined
into an animation/movie.
"""

from __future__ import annotations

import math
import pathlib
from typing import ClassVar

import bpy

bl_info = {
    "name": "Render a sequence of STL images of drops",
    "author": "alexfikl",
    "version": (0, 1),
    "blender": (3, 1, 0),
    "category": "Import-Export",
    "location": "File > Import/Export",
    "wiki_url": "",
}


# {{{ utils


def make_material(name):
    mat = bpy.data.materials.get(name)

    if mat is None:
        mat = bpy.data.materials.new(name)

        mat.use_nodes = True
        if mat.node_tree:
            mat.node_tree.links.clear()
            mat.node_tree.nodes.clear()

    return mat


def add_material(ob, mat):
    if ob.data.materials:
        ob.data.materials[0] = mat
    else:
        ob.data.materials.append(mat)


def clear_unused(*meshes):
    # NOTE: order matters here!
    for block in bpy.data.meshes:
        if block.users == 0 or block.name in meshes:
            bpy.data.meshes.remove(block)  # noqa: B909

    for block in bpy.data.materials:
        if block.users == 0:
            bpy.data.materials.remove(block)  # noqa: B909

    for block in bpy.data.textures:
        if block.users == 0:
            bpy.data.textures.remove(block)  # noqa: B909

    for block in bpy.data.images:
        if block.users == 0:
            bpy.data.images.remove(block)  # noqa: B909


# }}}


# {{{ objects


def add_wall(name, *, location, scale, color=None, emission=None):
    bpy.ops.mesh.primitive_cube_add(location=location, scale=scale)
    bpy.context.object.name = name

    if color is not None:
        mat = make_material(f"{name}Material")

        bsdf = mat.node_tree.nodes.new(type="ShaderNodeBsdfPrincipled")
        bsdf.inputs["Base Color"].default_value = color
        bsdf.inputs["Roughness"].default_value = 0.15

        if emission is not None:
            bsdf.inputs["Emission"].default_value = emission
            bsdf.inputs["Emission Strength"].default_value = 1.5

        output = mat.node_tree.nodes.new(type="ShaderNodeOutputMaterial")
        mat.node_tree.links.new(bsdf.outputs[0], output.inputs[0])

        add_material(bpy.context.object, mat)


def add_camera(name, *, location, rotation, scale, active=True):
    camera = bpy.data.cameras.new(name=name)
    camera.type = "ORTHO"
    camera.ortho_scale = scale

    camera_object = bpy.data.objects.new(name, camera)
    camera_object.location = location
    camera_object.rotation_euler = rotation

    bpy.context.collection.objects.link(camera_object)
    if active:
        bpy.context.scene.camera = camera_object


def add_light(
    name, *, location, rotation, size=10, energy=500, disable_cast_shadow=True
):
    light = bpy.data.lights.new(name=name, type="AREA")
    light.size = size
    light.energy = energy
    light.shape = "DISK"

    if disable_cast_shadow:
        light.cycles.cast_shadow = False

    light_object = bpy.data.objects.new(name, light)
    light_object.location = location
    light_object.rotation_euler = rotation

    bpy.context.collection.objects.link(light_object)


# }}}


# {{{ scene construction


def add_drops_from_file(infile, *, scale, use_eevee=False):
    bpy.ops.import_mesh.stl(filepath=str(infile))
    bpy.ops.transform.resize(value=(scale, scale, scale))
    ob = bpy.context.active_object

    # {{{ add material

    mat = make_material("DropletMaterial")
    if use_eevee:
        mat.shadow_method = "OPAQUE"
        mat.blend_method = "BLEND"
        mat.use_backface_culling = False
        mat.use_screen_refraction = True
        mat.refraction_depth = 1
        mat.use_sss_translucency = True
        mat.pass_index = 15

    glass = mat.node_tree.nodes.new(type="ShaderNodeBsdfGlass")
    glass.location = (0, 0)
    glass.distribution = "BECKMANN"
    glass.inputs["Roughness"].default_value = 0.1
    glass.inputs["IOR"].default_value = 1.050
    glass.inputs["Color"].default_value = (0.8, 0.85, 1, 1)

    output = mat.node_tree.nodes.new(type="ShaderNodeOutputMaterial")
    output.location = (0, 0)
    mat.node_tree.links.new(glass.outputs[0], output.inputs[0])
    add_material(ob, mat)

    # }}}

    return ob


def reset_scene(*, use_eevee: bool = False) -> None:
    # clear everything out
    for o in bpy.data.objects.values():
        bpy.data.objects.remove(o)
    clear_unused()

    if use_eevee:
        # set up some transparency
        # NOTE: there's a good chance some of these are useless or just a bad idea
        bpy.context.scene.render.engine = "BLENDER_EEVEE"
        bpy.context.scene.eevee.taa_render_samples = 256
        bpy.context.scene.eevee.use_ssr = True
        bpy.context.scene.eevee.use_ssr_refraction = True
        bpy.context.scene.eevee.use_gtao = True

        # set up some shadows
        bpy.context.scene.eevee.shadow_cube_size = "1024"
        bpy.context.scene.eevee.light_threshold = 0.001
        bpy.context.scene.eevee.gi_cubemap_resolution = "1024"
        bpy.context.scene.eevee.gi_diffuse_bounces = 16
        bpy.context.scene.eevee.use_shadow_high_bitdepth = True
        bpy.context.scene.eevee.use_soft_shadows = True
    else:
        bpy.context.scene.render.engine = "CYCLES"
        bpy.context.scene.cycles.device = "GPU"

        bpy.context.scene.cycles.adaptive_min_samples = 8
        bpy.context.scene.cycles.samples = 64
        bpy.context.scene.cycles.use_denoising = True
        bpy.context.scene.cycles.denoiser = "OPTIX"
        bpy.context.scene.cycles.denoising_input_passes = "RGB_ALBEDO"

    # {{{ add walls

    color = (0.23, 0.23, 0.23, 1)  # HEX: #3A3A3A
    emission = (1, 1, 1, 1)  # HEX: #FFFFFF

    add_wall("WallFront", location=(0, 12, 0), scale=(20, 0, 20), color=color)
    add_wall(
        "WallBack",
        location=(0, -23, 0),
        scale=(20, 0, 20),
        color=color,
        emission=emission,
    )

    add_wall(
        "WallTop",
        location=(0, 0, 10),
        scale=(20, 20, 0),
        color=color,
        emission=emission,
    )
    add_wall("WallBottom", location=(0, 0, -3), scale=(20, 20, 0), color=color)

    add_wall("WallLeft", location=(-10, 0, 0), scale=(0, 20, 15), color=color)
    add_wall("WallRight", location=(10, 0, 0), scale=(0, 20, 15), color=color)

    # }}}

    # {{{ add camera

    add_camera(
        "Camera", location=(0, -18, 4), rotation=(0.42 * math.pi, 0, 0), scale=15
    )

    # }}}

    # {{{ add lights

    add_light(
        "LightTop",
        location=(0, -4, 5),
        rotation=(math.pi / 4, 0, 0),
        size=3,
        energy=1000,
    )
    add_light(
        "LightLeft",
        location=(-8, -8, -1),
        rotation=(1.75, 0, -math.pi / 4),
        size=1,
        energy=500,
    )
    add_light(
        "LightRight",
        location=(8, -8, -1),
        rotation=(1.75, 0, math.pi / 4),
        size=1,
        energy=500,
    )

    # }}}


def render_scene_to_file(infile, outfile, *, scale, use_eevee=False):
    bpy.data.scenes["Scene"].render.filepath = str(outfile)

    # import meshes
    drops = add_drops_from_file(infile, scale=scale)

    # render
    bpy.ops.object.mode_set(mode="EDIT")
    bpy.ops.mesh.remove_doubles()
    bpy.ops.object.mode_set(mode="OBJECT")
    bpy.ops.object.shade_smooth()
    bpy.ops.render.render(write_still=True)

    # cleanup
    clear_unused(drops.name)


# }}}


# {{{ blender addon


class STLAnimation(bpy.types.Operator):
    bl_idname: str = "load.stl_anim_drops"
    bl_label: str = "Animate a series of STL files"
    bl_options: ClassVar[set[str]] = {"REGISTER", "UNDO"}
    bl_description: str = "Animate a series of STL files"

    filename_ext: str = ".stl"
    scale: float = 1.0
    material_name: str = "Material"

    directory: bpy.props.StringProperty(
        name="File path",
        description="File filepath of STL",
        maxlen=4096,
        subtype="DIR_PATH",
        default="",
    )

    filter_folder: bpy.props.BoolProperty(
        name="Filter folders",
        description="",
        default=True,
        options={"HIDDEN"},
    )

    filter_glob: bpy.props.StringProperty(
        default="*.stl",
        options={"HIDDEN"},
    )

    files: bpy.props.CollectionProperty(
        name="File path",
        type=bpy.types.OperatorFileListElement,
    )

    @classmethod
    def poll(cls, context):
        return (
            context.active_object is not None and context.active_object.type == "MESH"
        )

    def execute(self, context):
        # get file names
        directory = pathlib.Path(self.directory).resolve()
        if not directory.is_dir():
            directory = directory.parent

        files = sorted(
            [pathlib.Path(file.name) for file in self.files],
            key=lambda file: file.name,
        )

        # create directory for rendered frames
        from datetime import datetime

        today = datetime.now().strftime("%Y_%m_%d_%H%M%S")
        outdir = directory / f"Frames_{today}"
        outdir.mkdir()

        # reset scene
        reset_scene()

        # animate using all found STL files
        for i, basename in enumerate(files):
            filename = directory / basename
            outfile = outdir / f"render_{i:04d}.png"
            print(filename, outfile)
            render_scene_to_file(filename, outfile, scale=self.scale)
            clear_unused()

        return {"FINISHED"}

    def invoke(self, context, event):
        context.window_manager.fileselect_add(self)
        return {"RUNNING_MODAL"}


def menu_func_import(self, context):
    self.layout.operator(STLAnimation.bl_idname, text="Animate Drops STL (.stl)")


def register():
    bpy.utils.register_class(STLAnimation)
    bpy.types.TOPBAR_MT_file_import.append(menu_func_import)


def unregister():
    bpy.utils.unregister_class(STLAnimation)
    bpy.types.TOPBAR_MT_file_import.remove(menu_func_import)


# }}}


if __name__ == "__main__":
    register()

# vim: foldmethod=marker
