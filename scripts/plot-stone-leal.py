# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

import pathlib

import matplotlib.pyplot as mp
import numpy as np

mp.style.use(["science", "ieee"])


def plot_csv(filename):
    lambda_vs_ca = np.genfromtxt(
        filename, dtype=np.float64, delimiter=",", skip_header=True, usemask=True
    ).T

    fig = mp.figure(figsize=(10, 10), dpi=300)
    ax = fig.gca()

    for i, marker, label in zip(
        range(0, lambda_vs_ca.size, 2),
        ["o-", "--", ":"],
        [
            "$Simulation$",
            r"$\mathcal{O}(\mathrm{Ca}^2)$",
            r"$\mathcal{O}(\mathrm{Ca})$",
        ],
        strict=True,
    ):
        ax.plot(lambda_vs_ca[i], lambda_vs_ca[i + 1], marker, label=label)

    ax.set_xlabel(r"$\mathrm{Ca}$")
    ax.set_ylabel(r"$\lambda$")
    ax.legend()

    fig.savefig(filename.stem)
    mp.close(fig)


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("filename")
    args = parser.parse_args()

    plot_csv(pathlib.Path(args.filename))
