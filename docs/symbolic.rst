Symbolic Expressions
====================

Operators
---------

.. automodule:: pystopt.operators

Derivatives
-----------

.. automodule:: pystopt.derivatives

Shape Functionals
-----------------

.. automodule:: pystopt.cost

Primitives
----------

.. automodule:: pystopt.symbolic.primitives

Geometric Shape Derivatives
---------------------------

.. automodule:: pystopt.symbolic.derivatives
