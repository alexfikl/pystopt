Simulation Setup
================

Base Geometry State
-------------------

.. automodule:: pystopt.simulation.state

Unsteady Evolution
------------------

.. automodule:: pystopt.simulation.unsteady

Unconstrained Shape Optimization
--------------------------------

.. automodule:: pystopt.simulation.unconstrained

Stokes-Constrained Shape Optimization
-------------------------------------

.. automodule:: pystopt.simulation.staticstokes

Quasi-static Stokes-Constrained Optimization
--------------------------------------------

.. automodule:: pystopt.simulation.quasistokes

Helpers
-------

.. automodule:: pystopt.simulation.common
