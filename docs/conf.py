# SPDX-FileCopyrightText: 2020-2024 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

# https://www.sphinx-doc.org/en/master/usage/configuration.html
# https://www.sphinx-doc.org/en/master/usage/extensions/autodoc.html
# do not look for these imports (hard to install)
import sys
from importlib import import_module, metadata

# NOTE: setting for downstreams and handling mocked dataclasses
try:
    import arraycontext  # noqa: F401
except ImportError:
    sys._BUILDING_SPHINX_DOCS = True

# {{{ project information

m = metadata.metadata("pystopt")
project = m["Name"]
author = m["Author-email"]
copyright = f"2021-2024 {author}"  # noqa: A001
version = m["Version"]
release = version

# }}}

# {{{ general configuration

# needed extensions
extensions = [
    "sphinx.ext.autodoc",
    "sphinx.ext.intersphinx",
    "sphinx.ext.viewcode",
    "sphinx.ext.mathjax",
]

try:
    import sphinxcontrib.spelling  # noqa: F401

    extensions.append("sphinxcontrib.spelling")
except ImportError:
    pass

# extension for source files
source_suffix = {".rst": "restructuredtext"}
# name of the main (master) document
master_doc = "index"
# min sphinx version (already in pyproject.toml)
needs_sphinx = "4.0"
# files to ignore
exclude_patterns = ["_build", "Thumbs.db", ".DS_Store"]
# highlighting
pygments_style = "sphinx"

# }}}

# {{{ internationalization

language = "en"

# sphinxcontrib.spelling options
spelling_lang = "en_US"
tokenizer_lang = "en_US"
spelling_word_list_filename = "wordlist_en.txt"

# }}

# {{{ output

# html
html_theme = "sphinx_book_theme"
html_theme_options = {
    "show_toc_level": 2,
    "use_source_button": True,
    "use_repository_button": True,
    "navigation_with_keys": True,
    "repository_url": "https://gitlab.com/alexfikl/pystopt",
    "repository_branch": "main",
}

# }}}

# {{{ extension settings

nitpick_ignore_regex = [
    # needs https://github.com/sphinx-doc/sphinx/issues/13178
    ("py:class", r".*pathlib\._local\.Path"),
]

autodoc_member_order = "bysource"
autodoc_default_options = {
    "show-inheritance": None,
}
python_use_unqualified_type_names = True

# NOTE: can't use 'autodoc_typehints="description"' because it messes up
# the singledispatch functions by not showing any typehints
# autodoc_typehints = "description"

autodoc_type_aliases = {"DOFDescLike": "pystopt.symbolic.primitives.DOFDescLike"}

potential_mock_imports = [
    "pyopencl",
    "pytential",
    "meshmode",
    "loopy",
    "pymbolic",
    "arraycontext",
    "sumpy",
    "modepy",
    "pycgdescent",
    "h5pyckle",
]

autodoc_mock_imports = []
for mod in potential_mock_imports:
    try:
        import_module(mod)
    except ImportError:
        autodoc_mock_imports.append(mod)

# }}}

# {{{ links

intersphinx_mapping = {
    "arraycontext": ("https://documen.tician.de/arraycontext", None),
    "h5py": ("https://docs.h5py.org/en/stable", None),
    "h5pyapi": ("https://api.h5py.org/", None),
    "h5pyckle": ("https://h5pyckle.readthedocs.io/en/latest", None),
    "matplotlib": ("https://matplotlib.org/stable", None),
    "meshmode": ("https://documen.tician.de/meshmode", None),
    "numpy": ("https://numpy.org/doc/stable", None),
    "pymbolic": ("https://documen.tician.de/pymbolic", None),
    "pyopencl": ("https://documen.tician.de/pyopencl", None),
    "pytential": ("https://documen.tician.de/pytential", None),
    "python": ("https://docs.python.org/3", None),
    "pytools": ("https://documen.tician.de/pytools", None),
    "scipy": ("https://docs.scipy.org/doc/scipy", None),
    "sumpy": ("https://documen.tician.de/sumpy", None),
    "sympy": ("https://docs.sympy.org/dev", None),
}

# }}}
