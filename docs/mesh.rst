Meshing
=======

.. currentmodule:: pystopt.mesh

Spectral Meshes
---------------

.. automodule:: pystopt.mesh.spectral

Fourier (2D)
^^^^^^^^^^^^

.. automodule:: pystopt.mesh.fourier

Spherical Harmonics (3D)
^^^^^^^^^^^^^^^^^^^^^^^^

.. automodule:: pystopt.mesh.spharm

Generation
----------

.. automodule:: pystopt.mesh.generation

Reconstruction
--------------

.. automodule:: pystopt.mesh.processing

Connections
-----------

.. automodule:: pystopt.mesh.connection

Stabilization
-------------

.. automodule:: pystopt.stabilization.passive

.. automodule:: pystopt.stabilization.loewenberg1996
.. automodule:: pystopt.stabilization.loewenberg2001
.. automodule:: pystopt.stabilization.kropinski2001
.. automodule:: pystopt.stabilization.veerapaneni2011
.. automodule:: pystopt.stabilization.zinchenko2002
.. automodule:: pystopt.stabilization.zinchenko2013
