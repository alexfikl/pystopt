Electrostatic Equations
=======================

Interface
---------

.. automodule:: pystopt.electric.interface

Layer Potentials
----------------

.. automodule:: pystopt.electric.representations
