References
==========

.. [Pozrikidis1992] C. Pozrikidis,
    *Boundary Integral and Singularity Methods for Linearized Viscous Flow*,
    Cambridge University Press, 1992.

.. [Clift1978] R. Clift, J. R. Grace, M. E. Weber,
    *Bubbles, Drops and Particles*,
    Academic Press, 1978.

.. [Taylor1932] G. I. Taylor,
    *The Viscosity of a Fluid Containing Small Drops of Another Fluid*,
    Proceedings of the Royal Society, 1932.

.. [Walker2015] S. W. Walker, *The Shape of Things: A Practical Guide to
    Differential Geometry and the Shape Derivative*, SIAM, 2015.
