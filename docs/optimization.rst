Optimization
============

Filtering and Smoothing
-----------------------

.. automodule:: pystopt.filtering

Yukawa Kernel Smoothing
^^^^^^^^^^^^^^^^^^^^^^^

.. automodule:: pystopt.filtering.yukawa

Fourier Spectral Filtering
^^^^^^^^^^^^^^^^^^^^^^^^^^

.. automodule:: pystopt.filtering.fourier

Spherical Harmonic Spectral Filtering
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. automodule:: pystopt.filtering.spharm

Manifolds and Shape Spaces
--------------------------

.. automodule:: pystopt.optimize.riemannian

Line Search
-----------

.. automodule:: pystopt.optimize.linesearch

Descent Direction
-----------------

.. automodule:: pystopt.optimize.descentdirection


Helpers
-------

.. automodule:: pystopt.optimize.cg_utils
