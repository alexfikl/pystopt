Stokes Equations
================

Interface
---------

.. automodule:: pystopt.stokes.interface

Layer Potential Kernels
-----------------------

.. automodule:: pystopt.stokes.kernels

Layer Potentials
----------------

.. automodule:: pystopt.stokes.representations

Solving
-------

.. automodule:: pystopt.stokes.solve

Boundary Conditions
-------------------

.. automodule:: pystopt.stokes.boundary_conditions

Analytical Solutions
--------------------

.. automodule:: pystopt.stokes.analytic

.. _interface_reference:

Adjoint Boundary Conditions
---------------------------

.. automodule:: pystopt.stokes.derivatives
