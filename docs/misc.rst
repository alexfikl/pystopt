Misc
====

Visualization
-------------

.. automodule:: pystopt.visualization

Checkpointing
-------------

.. automodule:: pystopt.checkpoint.writing

Paths
-----

.. automodule:: pystopt.paths

Timers and Measuring
--------------------

.. automodule:: pystopt.measure

DOFArray Additions
------------------

.. automodule:: pystopt.dof_array

Callbacks
---------

.. automodule:: pystopt.callbacks
