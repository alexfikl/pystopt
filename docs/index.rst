Welcome to pystopt's documentation!
========================================

.. toctree::
    :maxdepth: 2

    solver_stokes
    solver_electric
    solver_evolution
    mesh
    optimization
    simulation
    symbolic
    misc
    references

Acknowledgments
===============

Work on ``pystopt`` was sponsored, in part, by the Office of Naval Research
(ONR) as part of the Multidisciplinary University Research Initiatives (MURI)
Program, under Grant Number N00014-16-1-2617.

The views and opinions expressed herein do not necessarily reflect those of the
funding agencies.

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
