# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

import numpy as np
import pytest

from pystopt import bind, sym
from pystopt.tools import (
    get_cl_array_context,
    get_default_logger,
    pytest_generate_tests_for_array_contexts,
)

import extra_test_data as etd

logger = get_default_logger(__file__)
filenamer = etd.get_filename_generator(__file__)
pytest_generate_tests = pytest_generate_tests_for_array_contexts([
    "pystopt.pyopencl",
])


# {{{ test shape functional arithmetic


def test_shape_functional_arithmetic(actx_factory, visualize):
    actx = get_cl_array_context(actx_factory)

    # {{{ geometry

    target_order = 4
    case = etd.FourierCircleTestCase(mesh_arguments={"target_order": target_order})
    discr = case.get_discretization(actx, case.resolutions[-1])

    from pytential import GeometryCollection

    places = GeometryCollection(discr, auto_where=case.name)

    # }}}

    # {{{ construct cost functionals

    import pystopt.cost as pc

    xd = sym.make_sym_vector("xd", places.ambient_dim)
    funcs = [
        pc.GeometryTrackingFunctional(
            xd=xd,
            t=sym.var("t"),
            dofdesc=places.auto_source,
            allow_tangential_gradient=True,
        ),
        pc.VolumeTrackingFunctional(vd=0),
        pc.NormalVelocityFunctional(u=xd, ud=0),
    ]
    factors = [1.5, np.pi, -1.0]

    # }}}

    # {{{ check basic arithmetic

    assert 0 * funcs[0] == 0
    assert funcs[0] * 0 == 0
    assert 1 * funcs[0] is funcs[0]
    assert funcs[0] * 1 is funcs[0]
    assert funcs[0] / 1 is funcs[0]
    assert (funcs[0] + 0) is funcs[0]

    with pytest.raises(NotImplementedError):
        _ = funcs[0] * funcs[1]

    with pytest.raises(NotImplementedError):
        _ = funcs[0] + 1

    _ = funcs[0] - funcs[1]
    _ = funcs[0] / 2.0

    # }}}

    # {{{ check sum

    alpha = 17.18
    sf = alpha * sum(
        (factor * func for factor, func in zip(factors, funcs, strict=True)), 0
    )
    xd = actx.thaw(discr.nodes())

    value = bind(places, sf(places.ambient_dim))(actx, xd=xd)
    ref_value = alpha * sum(
        factor * bind(places, func(places.ambient_dim))(actx, xd=xd)
        for factor, func in zip(factors, funcs, strict=True)
    )
    assert abs(value - ref_value) < 3.0e-14

    import pystopt.derivatives as grad
    from pystopt.dof_array import dof_array_rnorm

    for df in [grad.shape, grad.velocity, grad.traction, grad.capillary]:
        value = bind(places, df(sf, places.ambient_dim))(actx, xd=xd)
        ref_value = alpha * sum(
            factor * bind(places, df(func, places.ambient_dim))(actx, xd=xd)
            for factor, func in zip(factors, funcs, strict=True)
        )

        assert dof_array_rnorm(value, ref_value) < 3.0e-14

    # }}}


# }}}


# {{{ test_time_dependent_costs


@pytest.mark.parametrize(
    "name",
    [
        "uniform_geometry_tracking",
        "sbr_geometry_tracking",
        "bezier_geometry_tracking",
        "helix_geometry_tracking",
    ],
)
def test_time_dependent_cost(actx_factory, name, visualize):
    actx = get_cl_array_context(actx_factory)

    # {{{ cost

    ambient_dim = 3
    tmax = np.pi
    kwargs = {
        "xd": sym.nodes(ambient_dim).as_vector(),
        "t": sym.var("t"),
        "dofdesc": None,
        "allow_tangential_gradient": True,
    }
    mesh_arguments = {}

    if name == "uniform_geometry_tracking":
        kwargs["uinf_d"] = np.array([0.5, 0.5, -0.5])
    elif name == "sbr_geometry_tracking":
        kwargs["omega_d"] = np.array([0.5, 0.0, 0.0])
    elif name == "bezier_geometry_tracking":
        kwargs["t"] = sym.var("t") / tmax
        kwargs["points_d"] = (
            np.array([0, 0, 0], dtype=object),
            np.array([1, 0, 0], dtype=object),
            np.array([0, 1, 0], dtype=object),
            np.array([2, 1, 0], dtype=object),
        )
    elif name == "helix_geometry_tracking":
        mesh_arguments["offset"] = np.array([2, 0, 0])
        kwargs.update({
            "t": sym.var("t") / tmax,
            "height_d": 2.0,
            "omega_d": 2 * np.pi,
        })
    elif name == "bezier_helix_geometry_tracking":
        kwargs.update({
            "t": sym.var("t") / tmax,
            "radius": 1.0,
            "height": 1.0,
            "theta": np.pi / 6,
            "narcs": 1,
        })
    else:
        raise ValueError(f"unknown cost name: '{name}'")

    import pystopt.cost as pc

    cost = pc.cost_functional_from_name(name, **kwargs)

    # }}}

    # {{{ geometry

    target_order = 5
    case = etd.SPHSpheroidTestCase(
        mesh_arguments={"target_order": target_order, **mesh_arguments}
    )
    orig_discr = case.get_discretization(actx, case.resolutions[1])
    assert orig_discr.ambient_dim == ambient_dim

    logger.info("ndofs:     %d", orig_discr.ndofs)
    logger.info("nspec:     %d", orig_discr.nspec)
    logger.info("nelements: %d", orig_discr.mesh.nelements)

    # }}}

    # {{{ plot nodes over time

    filenames = None
    if visualize:
        dirname = filenamer.aspath().parent / name
        if not dirname.exists():
            dirname.mkdir()

        from pystopt.paths import generate_filename_series

        filenames = generate_filename_series("evolution", cwd=dirname)

        from pystopt.visualization import make_visualizer

        vis = make_visualizer(actx, orig_discr)

    from pystopt.measure import TicTocTimer

    timer = TicTocTimer()

    for i, t in enumerate(np.linspace(0.0, tmax, 128)):
        timer.tic()
        from pystopt.mesh import update_discr_from_nodes

        xd = bind(orig_discr, cost.nodes())(actx, t=t)
        discr = update_discr_from_nodes(actx, orig_discr, xd)
        timer.toc()

        zc = actx.to_numpy(
            bind(discr, sym.surface_centroid(discr.ambient_dim)[-1])(actx)
        )
        logger.info("[%3d] t = %.5e zc %.5e (%s)", i, t, zc, timer)

        if not visualize:
            continue

        from pystopt.visualization import make_same_connectivity_visualizer

        vis = make_same_connectivity_visualizer(actx, vis, discr)

        filename = next(filenames).aspath()
        vis.write_file(filename, [("distance", actx.np.sqrt(xd @ xd))], overwrite=True)

    # }}}


# }}}


if __name__ == "__main__":
    import sys

    import pyopencl as cl  # noqa: F401

    if len(sys.argv) > 1:
        exec(sys.argv[1])
    else:
        pytest.main([__file__])
