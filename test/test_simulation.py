# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

import numpy as np
import pytest

from pytools.obj_array import make_obj_array

from pystopt import bind, sym
from pystopt.dof_array import dof_array_rnorm
from pystopt.filtering import FilterType
from pystopt.tools import (
    get_cl_array_context,
    get_default_logger,
    pytest_generate_tests_for_array_contexts,
)

import extra_test_data as etd

logger = get_default_logger(__file__)
filenamer = etd.get_filename_generator(__file__)
pytest_generate_tests = pytest_generate_tests_for_array_contexts([
    "pystopt.pyopencl",
])


# {{{ test_geometry_wrangler


def check_geometry_wrangler(actx, wrangler, filename):
    if filename.exists():
        filename.unlink()

    state = wrangler.get_initial_state(actx)
    r = actx.np.sqrt(wrangler.vdot(state, state))
    assert r is not None

    state_x = wrangler.to_state_type(wrangler.from_state_type(state.x))
    error = actx.to_numpy(dof_array_rnorm(state_x, state.x, ord=np.inf))
    logger.info("to_state_type error: %.12e", error)
    assert error < 1.0e-12

    assert state.discr is None
    kappa = bind(
        state.get_geometry_collection(), sym.summed_curvature(wrangler.ambient_dim)
    )(actx)

    assert kappa is not None
    assert state.discr is not None

    from pystopt.simulation.state import get_output_fields_geometry

    fields = get_output_fields_geometry(state)
    assert "discr" in fields

    from pystopt.checkpoint.hdf import array_context_for_pickling, wrangler_for_pickling

    with array_context_for_pickling(actx), wrangler_for_pickling(wrangler):
        from pystopt.checkpoint import make_hdf_checkpoint

        with make_hdf_checkpoint(filename) as checkpoint:
            checkpoint.write("state", state, overwrite=True)

        with make_hdf_checkpoint(filename) as checkpoint:
            result = checkpoint.read("state")

        from pystopt.mesh import check_discr_same_connectivity

        assert result.discr is not None
        assert check_discr_same_connectivity(state.discr, result.discr)


@pytest.mark.parametrize(
    "case",
    [
        etd.FourierCircleTestCase(),
        etd.SPHSphereTestCase(),
    ],
)
def test_geometry_wrangler_sanity(actx_factory, case, visualize):
    actx = get_cl_array_context(actx_factory)

    nelements = case.resolutions[-1]
    discr = case.get_discretization(actx, nelements)

    from pytential import GeometryCollection

    places = GeometryCollection(discr, auto_where=case.name)
    dofdesc = places.auto_source

    from pystopt.simulation.state import GeometryWrangler

    wrangler = GeometryWrangler(places=places, dofdesc=dofdesc, is_spectral=True)
    assert wrangler.discr is discr

    filename = filenamer.with_suffix("geometry_wrangler_v0")
    check_geometry_wrangler(actx, wrangler, filename)


# }}}


# {{{ test_unconstrained_optimization_wrangler_sanity


def make_cost_functional(ambient_dim):
    context = {}
    context["t"] = 1.0
    context["xd"] = make_obj_array([np.pi, np.pi, np.pi][:ambient_dim])
    context["uinf"] = make_obj_array([1.0, 1.0, 1.0][:ambient_dim])
    context["uinf_d"] = make_obj_array([1.5, 1.5, 1.5][:ambient_dim])

    from pystopt.cost import UniformCentroidTrackingFunctional

    cost = UniformCentroidTrackingFunctional(
        xd=sym.make_sym_vector("xd", ambient_dim),
        t=sym.var("t"),
        uinf_d=sym.make_sym_vector("uinf_d", ambient_dim),
    )

    return cost, context


def check_optimization_wrangler(actx, wrangler, filename):
    state = wrangler.get_initial_state(actx)

    cost = state.evaluate_cost()
    assert cost is not None

    g = state.evaluate_gradient()
    assert g is not None

    cost_new, g_new = state.evaluate_cost_gradient()
    assert cost is cost_new
    assert g is g_new

    g_filtered = wrangler.apply_filter(actx, state.get_geometry_collection(), g)
    assert g_filtered is not None


@pytest.mark.parametrize(
    "case",
    [
        etd.FourierCircleTestCase(),
        etd.SPHSphereTestCase(),
    ],
)
def test_unconstrained_optimization_wrangler_sanity(actx_factory, case, visualize):
    actx = get_cl_array_context(actx_factory)

    nelements = case.resolutions[-1]
    discr = case.get_discretization(actx, nelements)

    from pytential import GeometryCollection

    places = GeometryCollection(discr, auto_where=case.name)
    dofdesc = places.auto_source

    from pystopt.simulation.unconstrained import UnconstrainedShapeOptimizationWrangler

    cost, context = make_cost_functional(case.ambient_dim)
    wrangler = UnconstrainedShapeOptimizationWrangler(
        places=places,
        dofdesc=dofdesc,
        is_spectral=True,
        cost=cost,
        context=context,
        filter_type=FilterType.Full,
        filter_arguments={"method": "ideal", "kmax": None},
    )

    assert wrangler.discr is discr
    filename = filenamer.with_suffix("unconstrained_optimization_wrangler_v0")
    check_geometry_wrangler(actx, wrangler, filename)
    filename = filenamer.with_suffix("unconstrained_optimization_wrangler_v1")
    check_optimization_wrangler(actx, wrangler, filename)


# }}}


# {{{ test_stokes_geometry_wrangler_sanity


def check_stokes_wrangler(actx, wrangler, filename):
    if filename.exists():
        filename.unlink()

    state = wrangler.get_initial_state(actx)
    r = actx.np.sqrt(wrangler.vdot(state, state))
    assert r is not None

    q = state.get_stokes_density()
    assert state.density is q
    u = state.get_stokes_velocity()
    assert state.velocity is u

    u_filter = wrangler.apply_filter(actx, state.get_geometry_collection(), u)
    assert u_filter is not None

    from pystopt.checkpoint.hdf import array_context_for_pickling, wrangler_for_pickling

    with array_context_for_pickling(actx), wrangler_for_pickling(wrangler):
        from pystopt.checkpoint import make_hdf_checkpoint

        with make_hdf_checkpoint(filename) as checkpoint:
            checkpoint.write("state", state, overwrite=True)

        with make_hdf_checkpoint(filename) as checkpoint:
            result = checkpoint.read("state")
            object.__setattr__(result, "wrangler", wrangler)

        assert result.discr is not None

        assert result.density is not None
        error = actx.to_numpy(dof_array_rnorm(result.get_stokes_density(), q))
        logger.info("get_stokes_density error: %.12e", error)
        assert error < 1.0e-15

        assert result.velocity is not None
        error = actx.to_numpy(dof_array_rnorm(result.get_stokes_velocity(), u))
        logger.info("get_stokes_velocity error: %.12e", error)
        assert error < 1.0e-15


@pytest.mark.parametrize(
    "case",
    [
        etd.FourierCircleTestCase(),
        etd.SPHSphereTestCase(),
    ],
)
def test_stokes_geometry_wrangler_sanity(actx_factory, case, visualize):
    actx = get_cl_array_context(actx_factory)

    nelements = case.resolutions[-1]
    qbx = case.get_layer_potential(actx, nelements)

    from pytential import GeometryCollection

    places = GeometryCollection(qbx, auto_where=case.name)
    dofdesc = places.auto_source

    from pystopt.stokes import get_farfield_boundary_from_name

    bc = get_farfield_boundary_from_name(places.ambient_dim, "uniform")

    from pystopt.stokes import TwoPhaseSingleLayerStokesRepresentation

    op = TwoPhaseSingleLayerStokesRepresentation(bc)

    from pystopt.simulation.common import StokesGeometryWrangler

    wrangler = StokesGeometryWrangler(
        places=places,
        dofdesc=dofdesc,
        is_spectral=True,
        op=op,
        lambdas={"viscosity_ratio": 1.0},
        context={"ca": 0.05, "viscosity_ratio": 1.0},
        gmres_arguments={},
        filter_type=FilterType.Full,
        filter_arguments={"method": "ideal", "kmax": None},
    )
    assert wrangler.discr is qbx.density_discr

    filename = filenamer.with_suffix("stokes_geometry_wrangler_v0")
    check_geometry_wrangler(actx, wrangler, filename)
    filename = filenamer.with_suffix("stokes_geometry_wrangler_v1")
    check_stokes_wrangler(actx, wrangler, filename)


# }}}


# {{{ test_static_stokes_wrangler_sanity


def check_stokes_optimization_wrangler(actx, wrangler, filename):
    if filename.exists():
        filename.unlink()

    state = wrangler.get_initial_state(actx)
    r = actx.np.sqrt(wrangler.vdot(state, state))
    assert r is not None

    qstar = state.get_adjoint_density()
    assert state.density_star is qstar
    ustar = state.get_adjoint_velocity()
    assert state.velocity_star is ustar

    c = state.evaluate_shape_cost()
    assert c > 0
    g = state.evaluate_shape_gradient()
    assert g is not None

    from pystopt.checkpoint.hdf import array_context_for_pickling, wrangler_for_pickling

    with array_context_for_pickling(actx), wrangler_for_pickling(wrangler):
        from pystopt.checkpoint import make_hdf_checkpoint

        with make_hdf_checkpoint(filename) as checkpoint:
            checkpoint.write("state", state, overwrite=True)

        with make_hdf_checkpoint(filename) as checkpoint:
            result = checkpoint.read("state")

        assert result.discr is not None

        assert result.density_star is not None
        error = actx.to_numpy(dof_array_rnorm(result.get_adjoint_density(), qstar))
        logger.info("get_adjoint_density error: %.12e", error)
        assert error < 1.0e-15

        assert result.velocity_star is not None
        error = actx.to_numpy(dof_array_rnorm(result.get_adjoint_velocity(), ustar))
        logger.info("get_stokes_velocity error: %.12e", error)


@pytest.mark.parametrize(
    "case",
    [
        etd.FourierCircleTestCase(),
        etd.SPHSphereTestCase(),
    ],
)
def test_static_stokes_wrangler_sanity(actx_factory, case, visualize):
    actx = get_cl_array_context(actx_factory)

    nelements = case.resolutions[0]
    qbx = case.get_layer_potential(actx, nelements)

    from pytential import GeometryCollection

    places = GeometryCollection(qbx, auto_where=case.name)
    dofdesc = places.auto_source

    from pystopt.stokes import get_farfield_boundary_from_name

    bc = get_farfield_boundary_from_name(places.ambient_dim, "uniform")

    from pystopt.stokes import TwoPhaseSingleLayerStokesRepresentation

    op = TwoPhaseSingleLayerStokesRepresentation(bc)

    from pystopt.simulation.staticstokes import StokesShapeOptimizationWrangler

    cost, context = make_cost_functional(case.ambient_dim)
    wrangler = StokesShapeOptimizationWrangler(
        places=places,
        dofdesc=dofdesc,
        is_spectral=True,
        op=op,
        cost=cost,
        lambdas={"viscosity_ratio": 1.0},
        context={**context, "ca": 0.05, "viscosity_ratio": 1.0},
        gmres_arguments={},
        filter_type=FilterType.Full,
        filter_arguments={"method": "ideal", "kmax": None},
    )
    assert wrangler.discr is qbx.density_discr

    filename = filenamer.with_suffix("static_stokes_wrangler_v0")
    check_geometry_wrangler(actx, wrangler, filename)
    filename = filenamer.with_suffix("static_stokes_wrangler_v1")
    check_stokes_wrangler(actx, wrangler, filename)
    filename = filenamer.with_suffix("static_stokes_wrangler_v2")
    check_optimization_wrangler(actx, wrangler, filename)
    filename = filenamer.with_suffix("static_stokes_wrangler_v3")
    check_stokes_optimization_wrangler(actx, wrangler, filename)


# }}}


# {{{ test_farfield_stokes_optimization_wrangler_sanity


def check_stokes_farfield_wrangler(actx, wrangler, filename):
    if filename.exists():
        filename.unlink()

    rng = np.random.default_rng(42)

    state = wrangler.get_initial_state(actx)
    r = actx.np.sqrt(wrangler.vdot(state, state))
    assert r is not None

    def asarray(x):
        return np.array(x).astype(np.float64)

    forward = state._get_forward_wrangler()
    assert all(
        np.allclose(asarray(forward.context[name]), asarray(value))
        for name, value in forward.state_to_fieldnames(state).items()
    )

    def random_like(x):
        if np.isscalar(x):
            return rng.random()
        elif isinstance(x, np.ndarray) and x.dtype.char != "O":
            return rng.random(x.shape, dtype=x.dtype)
        elif isinstance(x, np.ndarray) and x.dtype.char == "O":
            return make_obj_array([random_like(xi) for xi in x])
        else:
            raise TypeError(x)

    from dataclasses import replace

    state_rand = replace(state, x=random_like(state.x))
    forward_rand = state_rand._get_forward_wrangler()

    # check that the state was updated
    assert all(
        np.allclose(asarray(forward_rand.context[name]), asarray(value))
        for name, value in forward.state_to_fieldnames(state_rand).items()
    )
    # check that the previous values were not overwritten
    assert all(
        not np.allclose(
            asarray(forward_rand.context[name]), asarray(forward.context[name])
        )
        for name, value in forward.state_to_fieldnames(state_rand).items()
    )


@pytest.mark.parametrize(
    "case",
    [
        etd.FourierCircleTestCase(),
        etd.SPHSphereTestCase(),
    ],
)
@pytest.mark.parametrize(
    ("name", "bc_name"),
    [
        ("Uniform", "uniform"),
        ("Rotation", "solid_body_rotation"),
        ("BezierUniform", "bezier_uniform"),
        ("Helical", "helix"),
    ],
)
def test_farfield_stokes_evolution_wrangler_sanity(
    actx_factory, case, name, bc_name, visualize
):
    if bc_name == "helix" and case.ambient_dim != 3:
        pytest.skip(f"'{bc_name}' does not work in {case.ambient_dim}d")

    actx = get_cl_array_context(actx_factory)

    # {{{ geometry

    nelements = case.resolutions[0]
    qbx = case.get_layer_potential(actx, nelements)

    from pytential import GeometryCollection

    places = GeometryCollection(qbx, auto_where=case.name)
    dofdesc = places.auto_source

    # }}}

    # {{{ stokes

    bc_args = {"t": 0.5, "tmax": 1.0}
    if bc_name == "uniform":
        bc_args.update({
            "uinf": 1.0,
            "vinf": 1.0,
            "winf": 1.0,
        })
    elif bc_name == "bezier_uniform":
        bc_args.update({
            "bezier_p0": np.array([0.0, 0.0, 0.0][: case.ambient_dim], dtype=object),
            "bezier_p1": np.array([2.0, 0.0, 0.0][: case.ambient_dim], dtype=object),
            "bezier_p2": np.array([4.0, 0.0, 0.0][: case.ambient_dim], dtype=object),
            "bezier_p3": np.array([6.0, 0.0, 0.0][: case.ambient_dim], dtype=object),
        })
    elif bc_name == "solid_body_rotation":
        bc_args.update({"omega": 1.0})
    elif bc_name == "helix":
        bc_args.update({"tmax": 1.0, "omega": np.pi, "height": 1.0})
    else:
        raise AssertionError(f"Unknown 'bc_name': '{bc_name}'")

    from pystopt.simulation import farfield

    cls = getattr(farfield, f"{name}FarfieldStokesEvolutionWrangler")
    bc = cls.make_sym_boundary_condition(places.ambient_dim)

    from pystopt.stokes import TwoPhaseSingleLayerStokesRepresentation

    op = TwoPhaseSingleLayerStokesRepresentation(bc)

    from pystopt.evolution.stokes import StokesEvolutionOperator

    evolution = StokesEvolutionOperator(
        force_normal_velocity=True, force_tangential_velocity=True
    )

    # }}}

    # {{{ wranglers

    from pystopt.evolution.stepping import TimeWrangler

    cost, context = make_cost_functional(case.ambient_dim)
    time = TimeWrangler(
        time_step_name="aeuler",
        maxit=5,
        tmax=None,
        dt=1.0e-3,
        theta=1.0,
        estimate_time_step=False,
        state_filter=None,
    )

    forward = cls(
        places=places,
        dofdesc=dofdesc,
        is_spectral=True,
        op=op,
        lambdas={"viscosity_ratio": 1.0},
        gmres_arguments={},
        cost=cost,
        cost_final=None,
        evolution=evolution,
        stabilizer_name="default",
        stabilizer_arguments={},
        filter_type=FilterType.Unfiltered,
        filter_arguments={},
        context={**context, **bc_args, "ca": 0.05, "viscosity_ratio": 1.0},
    )

    from pystopt.paths import generate_filename_series

    checkpoint_directory_series = generate_filename_series(
        f"evolution_{name}".lower(), cwd=filenamer.aspath().parent
    )

    cls = getattr(farfield, f"{name}FarfieldOptimizationWrangler")
    wrangler = cls(
        time=time,
        forward=forward,
        checkpoint_directory_series=checkpoint_directory_series,
        overwrite=False,
    )

    # }}}

    # TODO: check adjoint? check evolution?

    assert forward.discr is qbx.density_discr
    filename = filenamer.with_suffix("farfield_stokes_wrangler_v0")
    check_geometry_wrangler(actx, forward, filename)
    filename = filenamer.with_suffix("farfield_stokes_wrangler_v1")
    check_stokes_wrangler(actx, forward, filename)
    filename = filenamer.with_suffix("farfield_stokes_wrangler_v2")
    check_stokes_farfield_wrangler(actx, wrangler, filename)


# }}}


if __name__ == "__main__":
    import sys

    import pyopencl as cl  # noqa: F401

    if len(sys.argv) > 1:
        exec(sys.argv[1])
    else:
        pytest.main([__file__])
