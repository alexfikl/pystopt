# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

import pytest

from pytential import GeometryCollection
from pytools.obj_array import make_obj_array

from pystopt import bind, sym
from pystopt.stokes import sti
from pystopt.tools import (
    get_cl_array_context,
    get_default_logger,
    pytest_generate_tests_for_array_contexts,
)

import extra_test_data as etd

logger = get_default_logger(__file__)
filenamer = etd.get_filename_generator(__file__)
pytest_generate_tests = pytest_generate_tests_for_array_contexts([
    "pystopt.pyopencl",
])


# {{{ Stokes


class StokesletIdentity:
    def __init__(self, ambient_dim):
        self.ambient_dim = ambient_dim

        from pystopt.stokes import make_stokeslet

        self.kernel = make_stokeslet(ambient_dim)

    @property
    def density(self):
        return sym.normal(self.ambient_dim).as_vector()

    def evaluate(self, actx, places):
        op = sti.velocity(self.kernel, self.density, qbx_forced_limit="avg")
        return bind(places, op)(actx, mu=1)

    def expected(self, actx, places):
        density_discr = places.get_discretization(places.auto_source.geometry)
        zeros = density_discr.zeros(actx)
        return make_obj_array([zeros] * self.ambient_dim)


class StressletIdentity:
    def __init__(self, ambient_dim):
        self.ambient_dim = ambient_dim

        from pystopt.stokes import make_stokeslet

        self.kernel = make_stokeslet(ambient_dim)

    @property
    def density(self):
        return sym.normal(self.ambient_dim).as_vector()

    def evaluate(self, actx, places):
        op = sti.traction(self.kernel, self.density, qbx_forced_limit="avg")
        return bind(places, op)(actx, mu=1)

    def expected(self, actx, places):
        return bind(places, -0.5 * self.density)(actx)


@pytest.mark.parametrize("op_name", ["stokeslet", "stresslet"])
@pytest.mark.parametrize("ambient_dim", [2])
def test_stokes_identities(actx_factory, op_name, ambient_dim, visualize):
    if op_name == "stokeslet":
        pytest.xfail("order mismatch (FIXME)")

    actx = get_cl_array_context(actx_factory)

    # {{{ setup

    target_order = 3
    qbx_order = 3

    if op_name == "stokeslet":
        op = StokesletIdentity(ambient_dim)
    elif op_name == "stresslet":
        op = StressletIdentity(ambient_dim)
    else:
        raise ValueError(f"unknown operator name: '{op_name}'")

    if ambient_dim == 2:
        case = etd.FourierStarfishTestCase(
            target_order=target_order, qbx_order=qbx_order, source_ovsmp=4
        )
    elif ambient_dim == 3:
        # NOTE: the 3d case doesn't work because the hole at the poles makes
        # it basically zero order all the time :\
        case = etd.SPHSphereTestCase(
            target_order=target_order, qbx_order=qbx_order, source_ovsmp=4
        )
    else:
        raise ValueError(f"unsupported dimension: {ambient_dim}d")

    logger.info("\n%s", str(case))

    # }}

    # {{{ convergence

    from pystopt.measure import EOCRecorder, stringify_eoc, verify_eoc

    eoc = EOCRecorder()

    for r in case.resolutions:
        qbx = case.get_layer_potential(actx, r).copy(_disable_refinement=True)
        places = GeometryCollection(qbx)

        density_discr = places.get_discretization(places.auto_source.geometry)
        logger.info("ndofs: %d", density_discr.ndofs)
        logger.info("nelements: %d", density_discr.mesh.nelements)

        b = op.evaluate(actx, places)
        b_expected = op.expected(actx, places)

        from pystopt.dof_array import dof_array_rnorm

        error = actx.to_numpy(dof_array_rnorm(b, b_expected))
        h_max = actx.to_numpy(bind(places, sym.h_max_from_volume(op.ambient_dim))(actx))
        logger.info("h_max %.10e error %.10e", h_max, error)

        eoc.add_data_point(h_max, error)

    logger.info("\n%s", stringify_eoc(eoc))
    logger.info(
        "ambient_dim %2d order %.3f expected %.3f",
        op.ambient_dim,
        eoc.order_estimate(),
        target_order,
    )

    # }}}

    if visualize:
        filename = filenamer.with_suffix(type(op).__name__.lower())

        from pystopt.visualization import make_visualizer

        vis = make_visualizer(actx, density_discr)

        vis.write_file(
            filename,
            [("b", b), (r"\hat{b}", b_expected)],  # noqa: RUF027
            markers=["-", ":"],
            overwrite=True,
        )

    if op_name == "stokeslet":
        # FIXME: why is this order bigger than expected? not yet asymptotic?
        target_order += 1
    else:
        target_order += 0.5

    verify_eoc(eoc, target_order)


# }}}


if __name__ == "__main__":
    import sys

    import pyopencl as cl  # noqa: F401

    if len(sys.argv) > 1:
        exec(sys.argv[1])
    else:
        pytest.main([__file__])
