# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

from dataclasses import dataclass
from functools import partial

import numpy as np
import pytest

from pytential import GeometryCollection

from pystopt import bind, sym
from pystopt.stokes import sti
from pystopt.tools import (
    get_cl_array_context,
    get_default_logger,
    pytest_generate_tests_for_array_contexts,
)

import extra_test_data as etd

logger = get_default_logger(__file__)
filenamer = etd.get_filename_generator(__file__)
pytest_generate_tests = pytest_generate_tests_for_array_contexts([
    "pystopt.pyopencl",
])


# {{{ convergence


@dataclass(frozen=True)
class ConvergenceInfo:
    error_ext: float
    error_int: float


@dataclass(frozen=True)
class VariableError:
    value: np.ndarray
    exact: np.ndarray
    error: float


def get_stokes_polar_case(
    target_order: int,
    *,
    qbx_order: int,
    source_ovsmp: int,
    radius: float = 1.0,
    fmm_order: bool | int = False,
):
    case = etd.FourierCircleTestCase(
        target_order=target_order,
        source_ovsmp=source_ovsmp,
        qbx_order=qbx_order,
        fmm_order=fmm_order,
        resolutions=[16, 32, 64, 128],
        disable_refinement=True,
        mesh_arguments={"radius": radius, "use_spectral_derivative": True},
    )

    from pystopt.stokes import PolarStokesSolution

    ex = PolarStokesSolution(
        radius=radius, capillary_number_name="ca", viscosity_ratio_name="lambda"
    )

    return case, ex


def get_stokes_taylor_case(
    target_order: int,
    *,
    qbx_order: int,
    source_ovsmp: int,
    radius: float = 1.0,
    fmm_order: bool | int = False,
    cls: str = "taylor",
):
    from pystopt.tools import on_ci

    if on_ci():
        resolutions = [8, 12, 16]
    else:
        resolutions = [8, 12, 16, 24, 32]
        # resolutions = [24]

    case = etd.SPHSphereTestCase(
        target_order=target_order,
        source_ovsmp=source_ovsmp,
        qbx_order=qbx_order,
        fmm_order=fmm_order,
        resolutions=resolutions,
        disable_refinement=True,
        mesh_arguments={"radius": radius, "use_spectral_derivative": True},
    )

    # NOTE: rotating will make the whole thing 1-ish order
    # from meshmode.mesh.processing import _get_rotation_matrix_from_angle_and_axis
    # mat = _get_rotation_matrix_from_angle_and_axis(
    #         np.pi / 2.0, np.array([0, 1, 0]))
    # from dataclasses import replace
    # case = replace(case,
    #         mesh_arguments={"transform_matrix": mat, **case.mesh_arguments})

    if cls == "taylor":
        from pystopt.stokes import Taylor

        ex = Taylor(
            radius=radius, capillary_number_name="ca", viscosity_ratio_name="lambda"
        )
    else:
        raise ValueError(f"unknown cls name: '{cls}'")

    return case, ex


def norm(actx, places, x, y=0, *, relative=False, weighted=True):
    if weighted:
        if relative:
            r = bind(
                places,
                sym.relative_norm(places.ambient_dim, sym.var("x"), sym.var("y"), p=2),
            )(actx, x=x, y=y)
        else:
            r = bind(
                places,
                sym.norm(places.ambient_dim, sym.var("x"), p=2),
            )(actx, x=x - y)
    else:
        from pystopt.dof_array import dof_array_norm, dof_array_rnorm

        if relative:
            r = dof_array_rnorm(x, y, ord=2)
        else:
            r = dof_array_norm(x - y, ord=2)

    return actx.to_numpy(r)


def evaluate_solution_error(
    actx,
    places,
    solution,
    ex,
    func,
    *,
    side=None,
    qbx_forced_limit="avg",
    context=None,
):
    if context is None:
        context = {}

    if side is None:
        v_ext = func(solution, side=+1, qbx_forced_limit=qbx_forced_limit)
        v_ex_ext = bind(places, func(ex, side=+1))(actx, **context)
        assert type(v_ext) is type(v_ex_ext)
        error_ext = norm(actx, places, v_ext, v_ex_ext)
        error_ext = VariableError(value=v_ext, exact=v_ex_ext, error=error_ext)

        v_int = func(solution, side=-1, qbx_forced_limit=qbx_forced_limit)
        v_ex_int = bind(places, func(ex, side=-1))(actx, **context)
        assert type(v_int) is type(v_ex_int)
        error_int = norm(actx, places, v_int, v_ex_int)
        error_int = VariableError(value=v_int, exact=v_ex_int, error=error_int)
    else:
        v = func(solution, side=side, qbx_forced_limit=qbx_forced_limit)
        v_ex = bind(places, func(ex, side=side))(actx, **context)
        error = norm(actx, places, v, v_ex)
        error_ext = error_int = VariableError(value=v, exact=v_ex, error=error)

    return (None, error_ext, error_int)


def run_convergence(
    actx,
    case,
    ex,
    names_and_funcs,
    resolution,
    *,
    viscosity_ratio=1.0,
    capillary_number=np.inf,
    suffix=None,
    visualize_spharm=False,
    visualize=False,
):
    # {{{ geometry

    qbx = case.get_layer_potential(actx, resolution)
    places = GeometryCollection(qbx, auto_where=case.name)

    if not qbx._disable_refinement:
        from pytential.qbx.refinement import refine_geometry_collection

        places = refine_geometry_collection(
            places,
            group_factory=case.group_factory_cls(order=case.target_order),
            refine_discr_stage=sym.QBX_SOURCE_QUAD_STAGE2,
        )

    density_discr = places.get_discretization(places.auto_source.geometry)
    h_max = actx.to_numpy(bind(places, sym.h_max_from_volume(ex.ambient_dim))(actx))

    logger.info("ndofs:     %d", density_discr.ndofs)
    logger.info("nelements: %d", density_discr.mesh.nelements)
    logger.info("h_max:     %.6e", h_max)

    # }}}

    # {{{ solve

    context = {ex.capillary_number_name: np.inf}
    lambdas = {ex.viscosity_ratio_name: viscosity_ratio}

    from pystopt.stokes import TwoPhaseSingleLayerStokesRepresentation

    op = TwoPhaseSingleLayerStokesRepresentation(ex.farfield)

    from pystopt.stokes import single_layer_solve

    solution = single_layer_solve(
        actx,
        places,
        op,
        lambdas=lambdas,
        sources=[case.name],
        context=context,
        gmres_arguments={"callback": True, "rtol": 1.0e-12},
    )

    # }}}

    # {{{ compute errors

    context.update(lambdas)
    get_error = partial(
        evaluate_solution_error, actx, places, solution, ex, context=context
    )

    r = {}
    names_and_fields = []
    names_and_errors = []
    for name, side, func in names_and_funcs:
        verr = get_error(func, side=side, qbx_forced_limit="avg")
        r[name] = ConvergenceInfo(error_ext=verr[+1].error, error_int=verr[-1].error)

        if visualize:
            error = actx.np.log10(
                actx.np.abs(verr[+1].value - verr[+1].exact) + 1.0e-15
            )

            names_and_fields.extend([
                (f"{name}_ext", verr[+1].value),
                (f"{name}_ext_exact", verr[+1].exact),
            ])
            names_and_errors.extend([
                (f"{name}_ext_error", error),
            ])

        logger.info(
            "value[%s]: ext %.15e / %.15e int %.15e / %.15e",
            name,
            norm(actx, places, verr[+1].value),
            norm(actx, places, verr[+1].exact),
            norm(actx, places, verr[-1].value),
            norm(actx, places, verr[-1].exact),
        )
        logger.info(
            "error[%s]: ext %.15e int %.15e", name, verr[+1].error, verr[-1].error
        )

    # }}}

    # {{{ plot

    if not visualize:
        return h_max, r

    from pystopt.visualization import make_visualizer

    vis = make_visualizer(actx, density_discr, vis_order=case.target_order)

    basename = filenamer.prepend(
        f"convergence_error_{suffix}_{viscosity_ratio:02.2f}_{resolution:03d}"
    )
    if places.ambient_dim == 2:
        for i, (name, _, _) in enumerate(names_and_funcs):
            f0, f1 = names_and_fields[2 * i : 2 * i + 2]
            filename = basename.with_suffix(name)
            vis.write_file(
                filename,
                [("result", f0[1]), ("exact", f1[1])],
                markers=["-", "--"],
                add_legend=True,
                overwrite=True,
            )
    else:
        vis.write_file(basename, names_and_fields + names_and_errors, overwrite=True)

    if visualize_spharm and places.ambient_dim == 3:
        from pystopt.mesh.spharm import visualize_spharm_modes

        for i, (name, _, _) in enumerate(names_and_funcs):
            f0, f1 = names_and_fields[2 * i : 2 * i + 2]

            filename = basename.with_suffix(f"{name}_modes")
            visualize_spharm_modes(
                filename,
                density_discr,
                [(name, f0[1])],
                mode="byorder",
                overwrite=True,
            )

            filename = basename.with_suffix(f"{name}_exact_modes")
            visualize_spharm_modes(
                filename,
                density_discr,
                [(name, f1[1])],
                mode="byorder",
                overwrite=True,
            )

    # }}}

    return h_max, r


# }}}


# {{{ test Stokes velocity / pressure against analytical solution


@pytest.mark.parametrize(
    ("ambient_dim", "viscosity_ratio"),
    [
        # (2, 1),
        pytest.param(2, 5, marks=pytest.mark.slowtest),
        # (3, 1),
        pytest.param(3, 5, marks=pytest.mark.slowtest),
    ],
)
@pytest.mark.parametrize("capillary_number", [np.inf])
def test_two_phase_stokes_solution(
    actx_factory, ambient_dim, viscosity_ratio, capillary_number, visualize
):
    actx = get_cl_array_context(actx_factory)

    # {{{ parameters

    target_order = 3
    enable_fmm = False
    kwargs = {"qbx_order": 4, "source_ovsmp": 4, "radius": 1.0}

    if enable_fmm:
        kwargs.update({
            "fmm_order": 10,
        })

    if ambient_dim == 2:
        case, ex = get_stokes_polar_case(target_order, **kwargs)
    elif ambient_dim == 3:
        kwargs["source_ovsmp"] += 2
        case, ex = get_stokes_taylor_case(target_order, **kwargs)
    else:
        raise ValueError

    logger.info("\n%s", str(case))

    names_and_funcs = [
        ("pressure", None, sti.pressure),
        ("velocity", +1, sti.velocity),
    ]

    # }}}

    from pystopt.measure import EOCRecorder, stringify_eoc, verify_eoc

    eoc_p_ext = EOCRecorder(name="p_ext")
    eoc_p_int = EOCRecorder(name="p_int")
    eoc_u = EOCRecorder(name="u")

    from pytools import ProcessTimer

    for i, r in enumerate(case.resolutions):
        with ProcessTimer() as p:
            h_max, result = run_convergence(
                actx,
                case,
                ex,
                names_and_funcs,
                r,
                suffix="solution",
                viscosity_ratio=viscosity_ratio,
                capillary_number=capillary_number,
                visualize=visualize and (i == len(case.resolutions) - 1),
            )
        logger.info("elapsed: %s", p)

        eoc_p_ext.add_data_point(h_max, result["pressure"].error_ext)
        eoc_p_int.add_data_point(h_max, result["pressure"].error_int)
        eoc_u.add_data_point(h_max, result["velocity"].error_ext)

    logger.info(
        "errors:\n%s",
        stringify_eoc(eoc_p_ext, eoc_p_int, eoc_u, table_format="latex"),
    )

    if ambient_dim == 2:
        # FIXME: why are these orders better? not yet asymptotic?
        verify_eoc(eoc_p_ext, target_order + 0.5)
        verify_eoc(eoc_p_int, target_order + 0.5)
        verify_eoc(eoc_u, target_order + 1.25)
    else:
        verify_eoc(eoc_p_ext, target_order + 0.6)
        verify_eoc(eoc_p_int, target_order + 0.6)
        verify_eoc(eoc_u, target_order + 0.45)


# }}}


# {{{ test stokes traction / velocity gradients against analytical solutions


@pytest.mark.parametrize(
    ("ambient_dim", "viscosity_ratio"),
    [
        # FIXME: any of these fast enough for the CI?
        pytest.param(2, 1, marks=pytest.mark.slowtest),
        pytest.param(2, 5, marks=pytest.mark.slowtest),
        pytest.param(3, 1, marks=pytest.mark.slowtest),
        pytest.param(3, 5, marks=pytest.mark.slowtest),
    ],
)
@pytest.mark.parametrize("capillary_number", [np.inf])
def test_two_phase_stokes_first_order(
    actx_factory, ambient_dim, viscosity_ratio, capillary_number, visualize
):
    actx = get_cl_array_context(actx_factory)

    # {{{ parameters

    target_order = 3
    enable_fmm = True
    kwargs = {"qbx_order": 4, "source_ovsmp": 4, "radius": 1.0}

    if enable_fmm:
        kwargs.update({
            "fmm_order": 10,
        })

    if ambient_dim == 2:
        case, ex = get_stokes_polar_case(target_order, **kwargs)
    elif ambient_dim == 3:
        kwargs["source_ovsmp"] += 2
        case, ex = get_stokes_taylor_case(target_order, **kwargs)
    else:
        raise ValueError

    logger.info("\n%s", str(case))

    names_and_funcs = [
        # name, side, func
        ("f", None, sti.traction),
        ("dudn", None, sti.normal_velocity_gradient),
    ]

    # }}}

    from pystopt.measure import EOCRecorder, stringify_eoc

    eoc_f_ext = EOCRecorder(name="f_ext")
    eoc_dudn_ext = EOCRecorder(name="dudn_ext")

    from pytools import ProcessTimer

    for i, r in enumerate(case.resolutions):
        with ProcessTimer() as p:
            h_max, result = run_convergence(
                actx,
                case,
                ex,
                names_and_funcs,
                r,
                suffix="normal",
                viscosity_ratio=viscosity_ratio,
                capillary_number=capillary_number,
                visualize=visualize and (i == len(case.resolutions) - 1),
            )
        logger.info("elapsed: %s", p)

        eoc_f_ext.add_data_point(h_max, result["f"].error_ext)
        eoc_dudn_ext.add_data_point(h_max, result["dudn"].error_ext)

    logger.info("errors:\n%s", stringify_eoc(eoc_f_ext))
    logger.info("errors:\n%s", stringify_eoc(eoc_dudn_ext))


# }}}


# {{{ test stokes tangential gradients against analytical solutions


@pytest.mark.parametrize(
    ("ambient_dim", "viscosity_ratio"),
    [
        # FIXME: any of these fast enough for the CI?
        pytest.param(2, 1, marks=pytest.mark.slowtest),
        pytest.param(2, 5, marks=pytest.mark.slowtest),
        pytest.param(3, 1, marks=pytest.mark.slowtest),
        pytest.param(3, 5, marks=pytest.mark.slowtest),
    ],
)
@pytest.mark.parametrize("capillary_number", [np.inf])
def test_two_phase_stokes_tangential_first_order(
    actx_factory, ambient_dim, viscosity_ratio, capillary_number, visualize
):
    actx = get_cl_array_context(actx_factory)

    # {{{ parameters

    target_order = 3
    enable_fmm = True
    kwargs = {"qbx_order": 4, "source_ovsmp": 4, "radius": 1.0}

    if enable_fmm:
        kwargs.update({
            "fmm_order": 10,
        })

    if ambient_dim == 2:
        case, ex = get_stokes_polar_case(target_order, **kwargs)
    elif ambient_dim == 3:
        kwargs["source_ovsmp"] += 2
        case, ex = get_stokes_taylor_case(target_order, **kwargs)
    else:
        raise ValueError

    logger.info("\n%s", str(case))

    # name, side, func
    names_and_funcs = [
        (f"t{i}", None, partial(sti.tangent_traction, tangent_idx=i))
        for i in range(ambient_dim - 1)
    ] + [
        (f"dudt{i}", None, partial(sti.tangent_velocity_gradient, tangent_idx=i))
        for i in range(ambient_dim - 1)
    ]

    # }}}

    from pystopt.measure import EOCRecorder, stringify_eoc

    eoc_t_ext = [EOCRecorder(name=f"t{i}_ext") for i in range(ambient_dim - 1)]
    eoc_dudt_ext = [EOCRecorder(name=f"dudt{i}_ext") for i in range(ambient_dim - 1)]

    from pytools import ProcessTimer

    for i, r in enumerate(case.resolutions):
        with ProcessTimer() as p:
            h_max, result = run_convergence(
                actx,
                case,
                ex,
                names_and_funcs,
                r,
                suffix="tangent",
                viscosity_ratio=viscosity_ratio,
                capillary_number=capillary_number,
                visualize=visualize and (i == len(case.resolutions) - 1),
            )
        logger.info("elapsed: %s", p)

        for j in range(ambient_dim - 1):
            eoc_t_ext[j].add_data_point(h_max, result[f"t{j}"].error_ext)
            eoc_dudt_ext[j].add_data_point(h_max, result[f"dudt{j}"].error_ext)

    logger.info("errors:\n%s", stringify_eoc(*eoc_t_ext))
    logger.info("errors:\n%s", stringify_eoc(*eoc_dudt_ext))


# }}}


# {{{ test velocity laplacian / hessian against analytical solutions


@pytest.mark.parametrize(
    ("ambient_dim", "viscosity_ratio"),
    [
        # FIXME: any of these fast enough for the CI?
        pytest.param(2, 1, marks=pytest.mark.slowtest),
        pytest.param(2, 5, marks=pytest.mark.slowtest),
        pytest.param(3, 1, marks=pytest.mark.slowtest),
        pytest.param(3, 5, marks=pytest.mark.slowtest),
    ],
)
@pytest.mark.parametrize("capillary_number", [np.inf])
def test_two_phase_stokes_second_order(
    actx_factory, ambient_dim, viscosity_ratio, capillary_number, visualize
):
    actx = get_cl_array_context(actx_factory)

    # {{{ parameters

    target_order = 5
    enable_fmm = True
    kwargs = {
        "target_order": target_order,
        "qbx_order": 4,
        "source_ovsmp": 5,
        "radius": 1.0,
    }

    if enable_fmm:
        kwargs.update({
            "fmm_order": 10,
        })

    if ambient_dim == 2:
        case, ex = get_stokes_polar_case(**kwargs)
    elif ambient_dim == 3:
        # kwargs["source_ovsmp"] += 2
        case, ex = get_stokes_taylor_case(**kwargs)
    else:
        raise ValueError

    logger.info("\n%s", str(case))

    names_and_funcs = [
        ("lap", None, sti.normal_velocity_laplacian),
        ("hes", None, sti.normal_velocity_hessian),
    ]

    # }}}

    from pystopt.measure import EOCRecorder, stringify_eoc

    eoc_lap_ext = EOCRecorder(name="lap_dot_n_ext")
    eoc_lap_int = EOCRecorder(name="lap_dot_n_int")
    eoc_hes_ext = EOCRecorder(name="hes_dot_n_ext")
    eoc_hes_int = EOCRecorder(name="hes_dot_n_int")

    from pytools import ProcessTimer

    for r in case.resolutions:
        with ProcessTimer() as p:
            h_max, result = run_convergence(
                actx,
                case,
                ex,
                names_and_funcs,
                r,
                suffix="hessian",
                viscosity_ratio=viscosity_ratio,
                capillary_number=capillary_number,
                visualize=visualize and r == case.resolutions[-1],
            )
        logger.info("elapsed: %s", p)

        eoc_lap_ext.add_data_point(h_max, result["lap"].error_ext)
        eoc_lap_int.add_data_point(h_max, result["lap"].error_int)
        eoc_hes_ext.add_data_point(h_max, result["hes"].error_ext)
        eoc_hes_int.add_data_point(h_max, result["hes"].error_int)

    logger.info(
        "errors:\n%s", stringify_eoc(eoc_lap_ext, eoc_lap_int, table_format="latex")
    )
    logger.info(
        "errors:\n%s", stringify_eoc(eoc_hes_ext, eoc_hes_int, table_format="latex")
    )


# }}}


# {{{ test_multiple_geometries


def _duplicate_discr_for_case(actx, case, resolution, offsets):
    discrs = []
    for o in offsets:
        from dataclasses import replace

        gen = replace(
            case.mesh_generator, offset=np.array([o, 0, 0][: case.ambient_dim])
        )

        from pystopt.mesh import generate_discretization

        discr = generate_discretization(gen, actx, resolution)
        discrs.append(discr)

    from pystopt.mesh import merge_disjoint_spectral_discretizations

    pre_density_discr = merge_disjoint_spectral_discretizations(actx, discrs)

    qbx = case.get_layer_potential_for_discr(
        pre_density_discr,
        target_association_tolerance=0.05,
    )

    places = GeometryCollection(
        {
            case.name: qbx,
        },
        auto_where=case.name,
    )

    return places


@pytest.mark.skip(reason="implementation details")
@pytest.mark.parametrize("ambient_dim", [2, 3])
def test_multiple_geometries(actx_factory, ambient_dim, visualize):
    actx = get_cl_array_context(actx_factory)

    # {{{ parameters

    viscosity_ratio = 1.0
    capillary_number = np.inf
    target_order = 3
    enable_fmm = False

    kwargs = {
        "target_order": target_order,
        "qbx_order": 4,
        "source_ovsmp": 5,
    }

    if enable_fmm:
        kwargs.update({
            "fmm_order": 10,
        })

    if ambient_dim == 2:
        alpha = 1 / 1.0886278076905638
        kwargs["mesh_arguments"] = {
            "k": 1,
            "a": 1,
            "b": 1,
            "transform_matrix": np.array([[0, -alpha], [alpha, 0]]),
        }

        case = etd.FourierUFOTestCase(
            resolutions=[16, 32, 64, 128], disable_refinement=True, **kwargs
        )
    elif ambient_dim == 3:
        kwargs["source_ovsmp"] += 2
        case = etd.SPHSphereTestCase(
            resolutions=[8, 12, 16, 24, 32], disable_refinement=True, **kwargs
        )
    else:
        raise ValueError

    logger.info("\n%s", str(case))

    # }}}

    # {{{

    lambdas = {"viscosity_ratio": viscosity_ratio}
    context = {"ca": capillary_number, **lambdas}
    logger.info("context: %s", context)

    from pystopt.stokes import get_farfield_boundary_from_name

    bc = get_farfield_boundary_from_name(
        ambient_dim,
        "uniform",
        uinf=1.0,
        # ambient_dim, "extensional", alpha=1.0,
    )
    from pystopt.stokes import TwoPhaseSingleLayerStokesRepresentation

    op = TwoPhaseSingleLayerStokesRepresentation(bc)

    bounding_box = None
    for o in [2, 1.75, 1.5, 1.25, 1.05]:
        places = _duplicate_discr_for_case(
            actx, case, case.resolutions[-1], offsets=(-o, o)
        )
        density_discr = places.get_discretization(case.name)

        if visualize:
            if bounding_box is None:
                from meshmode.mesh.processing import find_bounding_box

                bounding_box = find_bounding_box(density_discr.mesh)

            from sumpy.visualization import make_field_plotter_from_bbox

            fplot = make_field_plotter_from_bbox(
                bounding_box, h=(0.1, 0.1, 0.1)[:ambient_dim], extend_factor=0.25
            )

            from pytential.target import PointsTarget

            targets = PointsTarget(actx.from_numpy(fplot.points))
            places = places.merge({"targets": targets})

        from pystopt.stokes import single_layer_solve

        solution = single_layer_solve(
            actx,
            places,
            op,
            lambdas=lambdas,
            sources=[case.name],
            context=context,
            gmres_arguments={"callback": True, "rtol": 1.0e-12},
        )
        velocity = sti.velocity(solution, side=+1, qbx_forced_limit=+1)

        if not visualize:
            continue

        suffix = f"vr_{viscosity_ratio:02.2f}_ca_{capillary_number:02.2f}_{o:02.2f}"

        from pystopt.visualization import make_visualizer

        vis = make_visualizer(actx, density_discr, vis_order=case.target_order)

        if ambient_dim == 2:
            vis.write_file(
                filenamer.with_suffix(f"{suffix}_quiver"),
                [("u", velocity)],
                write_mode="quiver",
                aspect="equal",
                add_legend=False,
                overwrite=True,
            )
            vis.write_file(
                filenamer.with_suffix(f"{suffix}_velocity"),
                [("u", velocity)],
                add_legend=False,
                overwrite=True,
            )

            density = solution.density
            f_ext = sti.traction(solution, side=+1, qbx_forced_limit="avg")
            f_int = sti.traction(solution, side=-1, qbx_forced_limit="avg")

            vis.write_file(
                filenamer.with_suffix(f"{suffix}_density"),
                [("q", density)],
                add_legend=False,
                overwrite=True,
            )
            vis.write_file(
                filenamer.with_suffix(f"{suffix}_traction_ext"),
                [("f", f_ext)],
                add_legend=False,
                overwrite=True,
            )
            vis.write_file(
                filenamer.with_suffix(f"{suffix}_traction_int"),
                [("f", f_int)],
                add_legend=False,
                overwrite=True,
            )

            velocity = sti.velocity(
                solution, side=None, qbx_forced_limit=None, dofdesc="targets"
            )

            vis = make_visualizer(
                actx,
                density_discr,
                vis_order=case.target_order,
                vis_type="vtkhdf_lagrange",
            )
            vis.write_file(
                filenamer.with_suffix(f"{suffix}_volume"), [], overwrite=True
            )

            fplot.write_vtk_file(
                filenamer.with_suffix(f"{suffix}_volume").with_suffix(".vts"),
                [
                    ("ux", actx.to_numpy(velocity[0])),
                    ("uy", actx.to_numpy(velocity[1])),
                ],
                overwrite=True,
            )
        else:
            vis.write_file(
                filenamer.with_suffix(suffix),
                [
                    ("u", velocity),
                ],
                overwrite=True,
            )

    # }}}


# }}}


# {{{ test velocity smoothness


@pytest.mark.skip(reason="not ported to tensor products")
def test_velocity_smoothness(actx_factory):
    actx = get_cl_array_context(actx_factory)

    # {{{ geometry

    target_order = 4
    case = etd.FourierCircleTestCase(
        target_order=target_order, source_ovsmp=4, qbx_order=4
    )
    logger.info("\n%s", str(case))

    qbx = case.get_layer_potential(actx, case.resolutions[-1])
    places = GeometryCollection(qbx, auto_where=case.name)

    density_discr = places.get_discretization(case.name)
    logger.info("ndofs: %d", density_discr.ndofs)
    logger.info("nelements: %d", density_discr.mesh.nelements)

    # }}}

    # {{{ solve Stokes

    viscosity_ratio = 1.0
    context = {"ca": np.inf}
    lambdas = {"viscosity_ratio": viscosity_ratio}

    from pystopt.stokes import get_extensional_farfield

    sym_bc = get_extensional_farfield(ambient_dim=places.ambient_dim)

    from pystopt.stokes import TwoPhaseSingleLayerStokesRepresentation

    sym_op = TwoPhaseSingleLayerStokesRepresentation(sym_bc)

    from pystopt.stokes import single_layer_solve

    solution = single_layer_solve(
        actx,
        places,
        sym_op,
        sources=[case.name],
        lambdas=lambdas,
        context=context,
        gmres_arguments={"callback": logger},
    )

    velocity = sti.velocity(solution, qbx_forced_limit=+1, side=+1)

    # }}}

    # {{{ check velocity smoothness

    from pystopt.filtering import modal_decay_estimate

    s = [modal_decay_estimate(actx, density_discr, u) for u in velocity]

    logger.info(
        "modal estimate:    %.5e | %s", max(s), " ".join(f"{si:.5e}" for si in s)
    )
    assert max(s) <= -3.0

    from pystopt.filtering import interp_error_estimate

    s = [interp_error_estimate(actx, density_discr, u) for u in velocity]

    logger.info(
        "interp estimate:   %.5e | %s", max(s), " ".join(f"{si:.5e}" for si in s)
    )
    assert max(s) < 1.0e-3

    # }}}


# }}}


# {{{ test_spharm_stretch_factors


@pytest.mark.skip(reason="implementation details")
@pytest.mark.parametrize("target_order", [3, 5])
def test_spharm_stretch_factors(actx_factory, target_order, visualize):
    actx = get_cl_array_context(actx_factory)
    case = etd.SPHSpheroidTestCase(target_order=target_order)

    nelements = case.resolutions[-1]
    discr = case.get_discretization(actx, nelements)
    places = GeometryCollection(discr, auto_where=case.name)

    # {{{

    from pystopt.symbolic.primitives import _mapping_max_stretch_factor

    stretch_factor = bind(
        places,
        _mapping_max_stretch_factor(places.ambient_dim),
        auto_where=places.auto_source,
    )(actx)

    # }}}

    if not visualize:
        return

    from pystopt.visualization import make_visualizer

    vis = make_visualizer(actx, discr, vis_order=target_order)

    vis.write_file(
        filenamer.with_suffix(f"stretch_factor_{target_order}"),
        [
            ("stretch_factor", stretch_factor),
        ],
        overwrite=True,
    )


# }}}


if __name__ == "__main__":
    import sys

    import pyopencl as cl  # noqa: F401

    if len(sys.argv) > 1:
        exec(sys.argv[1])
    else:
        pytest.main([__file__])
