# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

import numpy as np
import numpy.linalg as la
import pytest

from pystopt.tools import get_default_logger, pytest_generate_tests_for_array_contexts

import extra_test_data as etd

logger = get_default_logger(__file__)
filenamer = etd.get_filename_generator(__file__)
pytest_generate_tests = pytest_generate_tests_for_array_contexts([
    "pystopt.pyopencl",
])


# {{{ test_fixed_time_step


def test_fixed_time_step():
    from pystopt.evolution.estimates import fixed_time_step_from

    results = {
        fixed_time_step_from(maxit=maxit, tmax=tmax, timestep=timestep)
        for maxit, tmax, timestep in [
            (None, 1.0, 1.0e-2),
            (100, None, 1.0e-2),
            (100, 1.0, None),
            (100, 1.0, 1.0e-2),
        ]
    }

    # FIXME: does this always hash correctly? floating point isn't friendly
    assert len(results) == 1


@pytest.mark.parametrize("method", ["aeuler", "assprk33"])
@pytest.mark.parametrize(
    ("maxit", "tmax", "timestep"),
    [
        (None, 1.0, 1.0e-2),
        (100, None, 1.0e-2),
        (100, 1.0, None),
    ],
)
def test_time_step_iteration(method, maxit, tmax, timestep):
    from pystopt.evolution.estimates import fixed_time_step_from

    maxit, tmax, timestep = fixed_time_step_from(
        maxit=maxit, tmax=tmax, timestep=timestep
    )
    logger.info("maxit %3d tmax %.5e dt %.5e", maxit, tmax, timestep)

    from pystopt.checkpoint import make_memory_checkpoint_manager

    checkpoint = make_memory_checkpoint_manager()

    # {{{ forward

    from pystopt.evolution.stepping import make_adjoint_time_stepper

    stepper = make_adjoint_time_stepper(
        method,
        "w",
        np.array([0.0, 0.0]),
        lambda t, w: 1.0,
        dt_start=timestep,
        checkpoint=checkpoint,
        t_start=0.0,
    )

    event = None
    for event in stepper.run(tmax=tmax, maxit=maxit):
        w = event.state_component
        assert abs(event.t - event.n * timestep) < 2.5e-15, event.t - event.n * timestep
        assert abs(w[0] - w[1]) < 1.0e-15, w[0] - w[1]

        if method == "aeuler":
            assert abs(w[0] - event.t) < 1.0e-15, w[0] - event.t
        else:
            # NOTE: stupid floating point!
            assert abs(w[0] - event.t) < 3.0e-15, w[0] - event.t

    assert abs(event.t - tmax) < 1.0e-12, event.t - tmax
    assert event.n == maxit - 1, event.n - maxit + 1

    # }}}

    # {{{ adjoint

    adjoint = stepper.make_adjoint(
        "wstar", np.array([tmax, tmax]), lambda t, w, wstar: -1.0
    )

    for event in adjoint.run(tmax=tmax, maxit=maxit):
        wstar = event.adjoint_component
        assert abs(event.t - event.n * timestep) < 2.0e-15, (
            event.t,
            event.n * timestep,
        )
        assert abs(wstar[0] - wstar[1]) < 1.0e-15, (wstar[0], wstar[1])

        if method == "aeuler":
            assert abs(wstar[0] - event.t) < 1.0e-12, wstar[0] - event.t
        else:
            # NOTE: stupid floating point!
            assert abs(wstar[0] - event.t) < 1.0e-12, (wstar[0], event.t)

    # }}}


# }}}


# {{{ test_adjoint_time_steppers


@pytest.mark.parametrize(
    ("method", "order"),
    [
        ("aeuler", 1),
        ("assprk22", 2),
        ("assprk33", 3),
        ("assprk34", 3),
    ],
)
def test_forward_integration(method, order, visualize):
    # {{{ parameters

    t_start = 1.0
    t_end = 10.0
    y0 = np.array([1.0, 3.0])

    def evaluate_solution(t: float) -> np.ndarray:
        u, v = y0
        log_t = np.sqrt(3) / 2 * np.log(t)
        du = np.sqrt(t) * (
            u * np.cos(log_t) + (2 * v - u) * np.sqrt(3) / 3 * np.sin(log_t)
        )
        dv = (
            v * np.cos(log_t) + (v - 2 * u) * np.sqrt(3) / 3 * np.sin(log_t)
        ) / np.sqrt(t)
        return np.array([du, dv])

    # }}}

    # {{{ evolve

    def evaluate_source_term(t: float, y: np.ndarray) -> np.ndarray:
        u, v = y
        return np.array([v, -u / t**2], dtype=np.float64)

    from pystopt.measure import EOCRecorder, stringify_eoc, verify_eoc

    eoc = EOCRecorder(name=method)

    for dt in 2.0 ** (-np.arange(4, 10)):
        from pystopt.evolution import make_adjoint_time_stepper

        step = make_adjoint_time_stepper(
            method, "y", y0, evaluate_source_term, dt_start=dt, t_start=t_start
        )

        history_state = [y0]
        history_ref = [y0]
        history_t = [t_start]

        for event in step.run(tmax=t_end, maxit=1000, verbose=False):
            ref_state = evaluate_solution(event.t)
            error = la.norm(event.state_component - ref_state) / la.norm(ref_state)

            history_state.append(event.state_component)
            history_ref.append(ref_state)
            history_t.append(event.t)

        logger.info("dt %.12e error %.12e", dt, error)
        eoc.add_data_point(dt, error)

    logger.info("\n%s", stringify_eoc(eoc))
    verify_eoc(eoc, expected_order=order)

    # }}}

    # {{{

    if not visualize:
        return

    history_state = np.array(history_state).T
    history_ref = np.array(history_ref).T
    history_t = np.array(history_t)

    from pystopt.visualization.matplotlib import subplots

    filename = filenamer.with_suffix(f"{method}_adjoint_forward")
    with subplots(filename, overwrite=True) as fig:
        ax = fig.gca()

        ax.semilogy(history_t, abs(history_state[0] - history_ref[0]))
        ax.semilogy(history_t, abs(history_state[1] - history_ref[1]))

        ax.set_xlabel("$t$")

    # }}}


# }}}


# {{{ test_adjoint_integration


@pytest.mark.parametrize(
    ("method", "order"),
    [
        ("aeuler", 1),
        ("assprk22", 2),
        ("assprk33", 3),
        ("assprk34", 3),
    ],
)
def test_adjoint_integration(method, order, visualize):
    # {{{ parameters

    y0 = 0.25
    ystarT = 3.0

    t_start = 0.0
    t_end = 5.0

    def evaluate_solution(t: float) -> np.ndarray:
        assert t_start == 0
        return y0 / (1 - y0 * (1 - np.cos(t)))

    def evaluate_adjoint_solution(t: float) -> np.ndarray:
        return (1 - y0 * (1 - np.cos(t))) * ystarT / (1 - y0 * (1 - np.cos(t_end)))

    # }}}

    # {{{ evolve

    def evaluate_source_term(t: float, y: np.ndarray) -> np.ndarray:
        return np.sin(t) * y**2

    def evaluate_adjoint_source_term(
        t: float,
        y: np.ndarray,
        ystar: np.ndarray,
    ) -> np.ndarray:
        return np.sin(t) * y * ystar

    from pystopt.measure import EOCRecorder, stringify_eoc, verify_eoc

    eoc = EOCRecorder(name=method)
    aeoc = EOCRecorder(name="adjoint")

    for dt in 2.0 ** (-np.arange(4, 10)):
        from pystopt.checkpoint import make_memory_checkpoint_manager

        checkpoint = make_memory_checkpoint_manager()

        # {{{ forward

        from pystopt.evolution import make_adjoint_time_stepper

        step = make_adjoint_time_stepper(
            method,
            "y",
            y0,
            evaluate_source_term,
            dt_start=dt,
            t_start=t_start,
            checkpoint=checkpoint,
        )

        history_fw_state = [y0]
        history_fw_ref = [y0]
        history_fw_t = [t_start]

        for event in step.run(tmax=t_end, maxit=10000000, verbose=False):
            ref_state = evaluate_solution(event.t)
            error = la.norm(event.state_component - ref_state) / la.norm(ref_state)

            history_fw_state.append(event.state_component)
            history_fw_ref.append(ref_state)
            history_fw_t.append(event.t)

        logger.info("dt %.12e error %.12e", dt, error)
        eoc.add_data_point(dt, error)

        # }}}

        # {{{ backward / adjoint

        step = step.make_adjoint("ystar", ystarT, evaluate_adjoint_source_term)

        history_state = [ystarT]
        history_ref = [ystarT]
        history_t = [t_end]

        for event in step.run(verbose=False):
            ref_state = evaluate_adjoint_solution(event.t)
            error = la.norm(event.adjoint_component - ref_state) / la.norm(ref_state)

            history_state.append(event.adjoint_component)
            history_ref.append(ref_state)
            history_t.append(event.t)

        logger.info("dt %.12e error %.12e", dt, error)
        aeoc.add_data_point(dt, error)

        # }}}

    logger.info("\n%s", stringify_eoc(eoc, aeoc))
    verify_eoc(eoc, expected_order=order)
    verify_eoc(aeoc, expected_order=order)

    # {{{

    if not visualize:
        return

    history_fw_state = np.array(history_fw_state)
    history_fw_ref = np.array(history_fw_ref)
    history_fw_t = np.array(history_fw_t)

    history_state = np.array(history_state)
    history_ref = np.array(history_ref)
    history_t = np.array(history_t)

    from pystopt.visualization.matplotlib import subplots

    filename = filenamer.with_suffix(f"{method}_adjoint_forward")
    with subplots(filename, overwrite=True) as fig:
        ax = fig.gca()

        if False:
            (line,) = ax.plot(history_t, history_state)
            ax.plot(history_t, history_ref, "--", color=line.get_color())
        else:
            ax.semilogy(
                history_fw_t,
                abs(history_fw_state - history_fw_ref),
                label=f"${method.upper()}$",
            )
            ax.semilogy(history_t, abs(history_state - history_ref), label="$Adjoint$")

        ax.legend(
            bbox_to_anchor=(0, 1.02, 1.0, 0.2),
            loc="lower left",
            mode="expand",
            borderaxespad=0,
            ncol=2,
        )

        ax.set_xlabel("$t$")

    # }}}


# }}}


if __name__ == "__main__":
    import sys

    import pyopencl as cl  # noqa: F401

    if len(sys.argv) > 1:
        exec(sys.argv[1])
    else:
        pytest.main([__file__])
