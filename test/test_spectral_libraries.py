# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

import numpy as np
import numpy.linalg as la
import pytest

import meshmode.discretization.poly_element as mpoly

from pystopt.tools import (
    get_cl_array_context,
    get_default_logger,
    pytest_generate_tests_for_array_contexts,
)

import extra_test_data as etd

logger = get_default_logger(__file__)
filenamer = etd.get_filename_generator(__file__)
pytest_generate_tests = pytest_generate_tests_for_array_contexts([
    "pystopt.pyopencl",
])


# {{{ test_gpyfft_compatibility


@pytest.mark.parametrize("dtype", [np.float64, np.complex128])
def test_gpyfft_compatibility(actx_factory, dtype, visualize):
    gpyfft = pytest.importorskip("gpyfft")
    actx = get_cl_array_context(actx_factory)

    dtype = np.dtype(dtype)
    rng = np.random.default_rng(42)

    f_ref = rng.random(128)
    if dtype.kind == "c":
        fhat_ref = np.fft.fft(f_ref)
    else:
        fhat_ref = np.fft.rfft(f_ref)

    # {{{ forward

    f = actx.from_numpy(f_ref.astype(dtype))
    fhat = actx.np.zeros(fhat_ref.shape, np.complex128)

    transform = gpyfft.fft.FFT(actx.context, actx.queue, in_array=f, out_array=fhat)
    (evt,) = transform.enqueue(forward=True)
    evt.wait()

    error = la.norm(actx.to_numpy(fhat) - fhat_ref) / la.norm(fhat_ref)
    logger.info("error: %.5e", error)
    assert error < 1.0e-14

    # }}}

    # {{{ backward

    f = actx.np.zeros(f_ref.shape, dtype)
    transform = gpyfft.fft.FFT(
        actx.context,
        actx.queue,
        in_array=fhat,
        out_array=f,
        real=(dtype.kind != "c"),
    )
    (evt,) = transform.enqueue(forward=False)
    evt.wait()

    error = la.norm(actx.to_numpy(f) - f_ref) / la.norm(f_ref)
    logger.info("error: %.5e", error)
    assert error < 1.0e-14

    # }}}

    # {{{ connection element group

    if dtype.kind == "c":
        return

    from pystopt.mesh.fourier import CLFFTConnectionElementGroup

    grp = CLFFTConnectionElementGroup(noutputs=f_ref.size, batches=[], is_real=True)
    assert grp.spat_shape == (f_ref.size, 1)

    fhat = grp.to_spectral(actx, f.reshape(-1, 1))
    error = la.norm(actx.to_numpy(fhat).squeeze() - fhat_ref) / la.norm(fhat_ref)
    logger.info("error: %.5e", error)

    f = grp.from_spectral(actx, fhat)
    error = la.norm(actx.to_numpy(f).squeeze() - f_ref) / la.norm(f_ref)
    logger.info("error: %.5e", error)

    # }}}


# }}}


# {{{ test_pynufft_compatibility


def get_device_tuple(queue):
    device = queue.device
    platform = device.platform

    # find device index in reikna lists
    try:
        from reikna import cluda
    except ImportError:
        return None

    api = cluda.ocl_api()
    platforms = api.get_platforms()

    reikna_api = None
    reikna_dev_num = None
    for n, devices in cluda.find_devices(api).items():
        for d in devices:
            dev = platforms[n].get_devices()[d]
            if dev == device:
                reikna_api = n
                reikna_dev_num = d

    assert reikna_api is not None

    thread = api.Thread(device)
    wavefront = api.DeviceParameters(device).warp_size
    return ("ocl", reikna_api, reikna_dev_num, platform, device, thread, wavefront)


@pytest.mark.parametrize(
    "group_factory_cls",
    [
        mpoly.InterpolatoryQuadratureSimplexElementGroup,
        # mpoly.PolynomialWarpAndBlend2DRestrictingElementGroup,
    ],
)
def test_pynufft_compatibility(actx_factory, group_factory_cls, visualize):
    pynufft = pytest.importorskip("pynufft")
    actx = get_cl_array_context(actx_factory)

    # {{{ create dummy group

    from collections import namedtuple

    MeshElementGroup = namedtuple("MeshElementGroup", ["dim"])
    mesh_el_group = MeshElementGroup(1)
    p_grp = group_factory_cls(mesh_el_group, order=3)
    e_grp = mpoly.PolynomialEquidistantSimplexElementGroup(
        mesh_el_group, order=p_grp.order
    )
    assert p_grp.nunit_dofs == e_grp.nunit_dofs

    # }}}

    # {{{ create dummy function

    nelements = 32
    nnodes = p_grp.nunit_dofs
    x = np.linspace(0.0, 1.0, nelements + 1)
    xm = (x[1:] + x[:-1]) / 2
    dx = (x[1:] - x[:-1]) / 2

    t_poly = (xm.reshape(-1, 1) + dx.reshape(-1, 1) * p_grp.unit_nodes).ravel()
    t_equi = (xm.reshape(-1, 1) + dx.reshape(-1, 1) * e_grp.unit_nodes).ravel()

    # delete repeated nodes
    if isinstance(p_grp, mpoly.PolynomialWarpAndBlend2DRestrictingElementGroup):
        t_poly = np.delete(t_poly, np.s_[nnodes::nnodes])[-1]
    t_equi = np.delete(t_equi, np.s_[nnodes::nnodes])[:-1]

    from pystopt.mesh.generation import ellipse_from_axes

    f_poly = ellipse_from_axes(t_poly, a=2.0, b=1.0)[0]
    f_equi = ellipse_from_axes(t_equi, a=2.0, b=1.0)[0]

    from pystopt.mesh.fourier import evaluate_fourier_expansion

    fhat_ref = np.fft.fft(f_equi)
    k_ref = np.fft.fftfreq(f_equi.size, d=1.0 / f_equi.size)
    f_poly_ref = actx.to_numpy(
        evaluate_fourier_expansion(
            actx,
            modes=actx.from_numpy(fhat_ref),
            k=actx.from_numpy(k_ref),
            t=actx.from_numpy(t_poly),
        )
    ).real

    error = la.norm(f_poly - f_poly_ref) / la.norm(f_poly)
    logger.info("error: %.5e", error)
    assert error < 1.0e-12

    # }}}

    # {{{ pynufft roundtrip

    devid = get_device_tuple(actx.queue)
    nufft = pynufft.NUFFT(devid)

    k_ref = np.fft.fftfreq(2 * f_poly.size)
    k_ref = np.pi * k_ref / np.max(np.abs(k_ref))
    nufft.plan(k_ref.reshape(-1, 1), (f_poly.size,), (2 * f_poly.size,), (6,))

    from pytools import ProcessTimer

    with ProcessTimer() as p:
        fhat = nufft.forward(f_poly)
    logger.info(str(p))

    with ProcessTimer() as p:
        # f = nufft.solve(fhat, solver="L1TVOLS", maxiter=32, rho=0.5)
        f = nufft.solve(fhat, solver="cg", maxiter=32)
        f = np.real(f)
    logger.info(p)

    error = la.norm(f_poly - f) / la.norm(f_poly)
    logger.info("error: %.5e", error)

    # }}}

    if not visualize:
        return

    from pystopt.visualization.matplotlib import subplots

    filename = filenamer.with_suffix("pynufft_roundtrip")
    with subplots(filename, overwrite=True) as fig:
        ax = fig.gca()
        ax.plot(t_poly, f_poly, "k-")
        ax.plot(t_poly, f.real, "--")
        ax.plot(t_poly, f.imag, ":")


# }}}


# {{{ test_shtools_vs_scipy


def test_shtools_vs_scipy(visualize):
    pysh = pytest.importorskip("pyshtools")

    nlon = 65
    nlat = nlon
    lmax = 31

    if nlon == nlat:
        grid_type = "DH"
    elif nlon == 2 * nlat:
        grid_type = "DH2"
    else:
        raise ValueError

    logger.info("nlon %d nlat %d lmax %d", nlon, nlat, lmax)

    # {{{ create spharm grid

    grid = pysh.SHGrid.from_array(np.zeros((nlat, nlon)), grid=grid_type)

    theta = grid.lats() + 90
    phi = grid.lons()
    m_phi, m_theta = np.meshgrid(phi, theta)

    logger.info("nlon %d nlat %d lmax %d", grid.nlon, grid.nlat, grid.lmax)

    # }}}

    # {{{ match to scipy

    from dataclasses import replace

    basename = replace(filenamer, stem=f"{filenamer.stem}_shtools_sanity_check_scipy")

    def visualize_ymn(filename, ymn, ymn_ref):
        from pystopt.visualization.matplotlib import preprocess_latex, subplots

        with subplots(filename, nrows=1, ncols=2, overwrite=True) as fig:
            ax = fig.axes

            ymn.plot(
                ax=ax[0],
                title=preprocess_latex(r"\texttt{scipy}"),
                titlesize=32,
                xlabel=preprocess_latex(r"\phi"),
                ylabel=preprocess_latex(r"\theta"),
            )
            ymn_ref.plot(
                ax=ax[1],
                title=preprocess_latex(r"\texttt{shtools}"),
                titlesize=32,
                xlabel=preprocess_latex(r"\phi"),
                ylabel=preprocess_latex(r"\theta"),
            )

    def visualize_fmn(filename, fmn, fmn_ref):
        from pystopt.visualization.matplotlib import preprocess_latex, subplots

        with subplots(filename, nrows=1, ncols=2, overwrite=True) as fig:
            ax = fig.axes

            fmn.plot_spectrum2d(
                ax=ax[0],
                title=preprocess_latex(r"\texttt{scipy}"),
                titlesize=32,
                order_label=preprocess_latex("m"),
                degree_label=preprocess_latex("n"),
                colorbar=None,
            )
            fmn_ref.plot_spectrum2d(
                ax=ax[1],
                title=preprocess_latex(r"\texttt{shtools}"),
                titlesize=32,
                order_label=preprocess_latex("m"),
                degree_label=preprocess_latex("n"),
                colorbar=None,
            )

    from scipy.special import sph_harm as scipy_sph_harm  # pylint: disable=E0611

    sht_fmn = np.full((2, lmax + 1, lmax + 1), 1.0e-16)
    sht_fmn = pysh.SHCoeffs.from_array(sht_fmn, normalization="ortho", csphase=-1)

    for m, n in [(0, 1), (1, 1), (2, 2), (3, 5), (2, 11)]:
        factor = (-1) ** (m + n + 2) * (1.0 if m == 0 else np.sqrt(1 / 2))
        sht_fmn.set_coeffs(factor, n, m)

        # scipy
        scipy_ymn = scipy_sph_harm(m, n, np.radians(m_phi), np.radians(m_theta))
        scipy_ymn = pysh.SHGrid.from_array(scipy_ymn.real, grid=grid_type)
        scipy_fmn = scipy_ymn.expand(normalization="ortho", csphase=-1, lmax_calc=lmax)

        # shtools
        sht_ymn = sht_fmn.expand(grid=grid_type, lmax=lmax)

        # errors
        error_ymn = la.norm(scipy_ymn.data - sht_ymn.data) / la.norm(sht_ymn.data)
        error_fmn = la.norm(scipy_fmn.coeffs - sht_fmn.coeffs) / la.norm(sht_fmn.coeffs)
        logger.info("error: ymn %.5e fmn %.5e", error_ymn, error_fmn)

        if visualize:
            filename = basename.with_suffix(f"{m:02d}_{n:02d}")
            visualize_ymn(filename, scipy_ymn + 1.0e-16, scipy_ymn - sht_ymn)
            filename = basename.with_suffix(f"{m:02d}_{n:02d}_modes")
            visualize_fmn(filename, scipy_fmn + 1.0e-16, scipy_fmn - sht_fmn)

        sht_fmn.set_coeffs(0.0, n, m)

    # }}}


# }}}


# {{{ test_shtns_vs_scipy


def _sph_to_grid(sh, xlm):
    if xlm.size == sh.nlm:  # real transform
        shape = (sh.lmax + 1, sh.mmax + 1)
    elif xlm.size == sh.nlm_cplx:  # complex transform
        shape = (sh.lmax + 1, 2 * sh.mmax + 1)
    else:
        raise ValueError("spec array has unknown size '{xlm.size}'")

    nlm = np.full(shape, np.nan + 1j * np.nan, dtype=np.complex128)
    if xlm.size == sh.nlm:
        n = sh.l
        m = sh.m
    else:
        n, m = sh.zlm(np.arange(xlm.size))
        m += sh.mmax

    nlm[n, m] = xlm
    return nlm


@pytest.mark.parametrize("cplx", [False])
def test_shtns_vs_scipy(cplx, visualize):
    nlon = 64
    nlat = nlon
    lmax = 31
    mmax = lmax
    logger.info("nlon %d nlat %d lmax %d mmax %d", nlon, nlat, lmax, mmax)

    # h = (2.0 / nlon) ** 3

    # {{{ create spharm grid

    from pystopt.mesh.spharm import _make_shtns

    sh = _make_shtns(lmax, mmax, nlat, nlon, polar_opt=1.0e-10)

    theta = np.arccos(sh.cos_theta)
    phi = (2.0 * np.pi / nlon) * np.arange(nlon)
    m_phi, m_theta = np.meshgrid(phi, theta)

    n = np.arange(sh.lmax + 1)
    if cplx:
        m = np.arange(-sh.mres * sh.mmax, sh.mres * sh.mmax + 1)
    else:
        m = np.arange(sh.mres * sh.mmax + 1)
    m_m, m_n = np.meshgrid(m, n)

    # }}}

    # {{{ match to scipy

    from dataclasses import replace

    basename = replace(filenamer, stem=f"{filenamer.stem}_shtns_sanity_check_scipy")

    def visualize_ymn(filename, ymn, ymn_ref):
        from pystopt.visualization.matplotlib import preprocess_latex, subplots

        with subplots(filename, nrows=1, ncols=2, overwrite=True) as fig:
            ax = fig.axes

            ax[0].contourf(m_theta, m_phi, ymn)
            ax[0].set_xlabel(preprocess_latex(r"$\theta$"))
            ax[0].set_ylabel(preprocess_latex(r"$\phi$"))
            ax[0].set_title(preprocess_latex(r"$\texttt{scipy}$"))

            im = ax[1].contourf(m_theta, m_phi, ymn_ref)
            ax[1].set_xlabel(preprocess_latex(r"$\theta$"))
            ax[1].set_title(preprocess_latex(r"$\texttt{shtns}$"))
            fig.colorbar(im, ax=ax[1])

    def visualize_fmn(filename, fmn, fmn_ref):
        from pystopt.visualization.matplotlib import preprocess_latex, subplots

        with subplots(filename, nrows=1, ncols=2, overwrite=True) as fig:
            ax = fig.axes

            m_fmn = _sph_to_grid(sh, fmn)
            ax[0].contourf(m_m, m_n, np.real(m_fmn), levels=32)
            # ax[0].imshow(np.abs(m_fmn))
            ax[0].set_xlabel(preprocess_latex("$m$"))
            ax[0].set_ylabel(preprocess_latex("$n$"))
            ax[0].set_title(preprocess_latex(r"$\texttt{scipy}$"), fontsize=32)

            m_fmn = _sph_to_grid(sh, fmn_ref)
            im = ax[1].contourf(m_m, m_n, np.real(m_fmn), levels=32)
            # im = ax[1].imshow(np.abs(m_fmn))
            ax[1].set_xlabel(preprocess_latex(r"$m$"))
            ax[1].set_title(preprocess_latex(r"$\texttt{shtns}$"), fontsize=28)
            fig.colorbar(im, ax=ax[1])

    from scipy.special import sph_harm as scipy_sph_harm  # pylint: disable=E0611

    if cplx:
        sh_analys = sh.analys_cplx
        sh_synth = sh.synth_cplx
        idx = sh.zidx
        shtns_fmn = np.zeros(sh.nlm_cplx, dtype=np.complex128)
    else:
        sh_analys = sh.analys
        sh_synth = sh.synth
        idx = sh.idx
        shtns_fmn = np.zeros(sh.nlm, dtype=np.complex128)

    for m, n in [(0, 1), (1, 1), (3, 5), (2, 11)]:
        if not cplx:
            shtns_fmn[idx(n, m)] = 1.0 if m == 0 else 0.5
        else:
            shtns_fmn[idx(n, m)] = 1.0

        # scipy
        scipy_ymn = scipy_sph_harm(m, n, m_phi, m_theta)
        if not cplx:
            scipy_ymn = np.real(scipy_ymn)
        scipy_fmn = sh_analys(scipy_ymn)

        # shtns
        shtns_ymn = sh_synth(shtns_fmn)
        # scipy_fmn = sh_analys(shtns_ymn)

        # errors
        error_ymn = la.norm(scipy_ymn - shtns_ymn) / la.norm(shtns_ymn)
        error_fmn = la.norm(scipy_fmn - shtns_fmn) / la.norm(shtns_fmn)
        logger.info("error: ymn %.5e fmn %.5e", error_ymn, error_fmn)

        if visualize:
            filename = basename.with_suffix(f"{m:02d}_{n:02d}")
            visualize_ymn(filename, np.real(scipy_ymn), np.real(scipy_ymn - shtns_ymn))
            # visualize_ymn(filename, np.real(scipy_ymn), np.real(shtns_ymn))
            filename = basename.with_suffix(f"{m:02d}_{n:02d}_modes")
            visualize_fmn(filename, np.abs(scipy_fmn), np.abs(scipy_fmn - shtns_fmn))
            # visualize_fmn(filename, np.real(scipy_fmn), np.real(shtns_fmn))

        shtns_fmn[idx(n, m)] = 0.0
        assert error_ymn < 1.0e-14
        assert error_fmn < 1.0e-14

    # }}}


# }}}


if __name__ == "__main__":
    import sys

    import pyopencl as cl  # noqa: F401

    if len(sys.argv) > 1:
        exec(sys.argv[1])
    else:
        pytest.main([__file__])
