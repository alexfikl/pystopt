# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

import numpy as np  # noqa: F401
import pytest

from pystopt.tools import get_default_logger, pytest_generate_tests_for_array_contexts

import extra_test_data as etd

logger = get_default_logger(__file__)
filenamer = etd.get_filename_generator(__file__)
pytest_generate_tests = pytest_generate_tests_for_array_contexts([
    "pystopt.pyopencl",
])


# {{{ test_table


@pytest.mark.parametrize("name", ["markdown", "latex", "unicode"])
def test_table(name):
    data = [
        ["Reading", "60.80", "17.63", "61", "27", "92", "25"],
        ["Writing", "60.00", "19.02", "60", "25", "99", "25"],
        ["Verbal", "60.00", "14.31", "62", "40", "91", "25"],
        ["History", "55.40", "19.29", "51", "26", "95", "25"],
        ["Math", "54.20", "17.96", "50", "23", "92", "25"],
    ]
    alignment = "lcccccr"

    from pystopt import table

    if name == "latex":
        table = table.LatexTable(ncolumns=len(data[0]), alignment=alignment)
    elif name == "markdown":
        table = table.MarkdownTable(ncolumns=len(data[0]), alignment=alignment)
    elif name == "unicode":
        table = table.UnicodeTable(
            ncolumns=len(data[0]), box_type="heavy", alignment=alignment
        )
    else:
        raise ValueError(f"unknown table name: '{name}'")

    table.add_header(["", "Mean", "SD", "Median", "Minimum", "Maximum", "Total"])
    table.add_footer(["Overall", "", "", "", "", "", ""])
    for r in data:
        table.add_row(r)

    logger.info("\n%s", table)


# }}}


# {{{ test_eoc


def test_stringify_eoc(visualize):
    from pystopt.measure import EOCRecorder, stringify_eoc

    eoc_x = EOCRecorder(name="x")
    eoc_y = EOCRecorder(name="y")

    import random

    for nelements in [8, 16, 32, 64, 128, 256, 512]:
        eoc_x.add_data_point(1.0 / nelements, random.random() * (1.0 / nelements) ** 2)
        eoc_y.add_data_point(
            1.0 / nelements, random.random() * (1.0 / nelements) ** 1.5
        )

    logger.info("\n%s", stringify_eoc(eoc_x))
    logger.info("\n%s", stringify_eoc(eoc_x, eoc_y))

    if visualize:
        from pystopt.measure import visualize_eoc

        filename = filenamer.with_suffix("stringify_eoc_order")
        visualize_eoc(filename, eoc_x, eoc_y, overwrite=True)


# }}}


# {{{ test_path_name_construction


def test_path_name_construction():
    from pystopt.paths import get_dirname, get_filename

    dirname = get_dirname("output")
    logger.info(dirname)

    filename = get_filename(__file__)
    logger.info(filename)
    filename = get_filename(__file__, cwd=dirname)
    logger.info(filename)
    filename = get_filename(__file__, cwd=dirname, ext=".h5")
    logger.info(filename)
    filename = get_filename(__file__, suffix="suffix", cwd=dirname)
    logger.info(filename)

    from pystopt.paths import generate_filename_series

    filenames = generate_filename_series("series", cwd=dirname)

    for _ in range(10):
        filename = next(filenames)
        logger.info(filename)
        logger.info(filename.with_suffix("extra"))


# }}}


# {{{ test_format_seconds


def test_format_seconds():
    from pystopt.measure import format_seconds

    hours, minutes, seconds = 3, 53, 12
    s = hours * 60 * 60 + minutes * 60 + seconds

    assert format_seconds(s) == f"{hours}h{minutes}m{seconds}s"

    hours, minutes, seconds = 52, 53, 12
    s = hours * 60 * 60 + minutes * 60 + seconds

    assert format_seconds(s) == f"{hours}h{minutes}m{seconds}s"
    assert format_seconds(s, days=True) == f"2d{4}h{minutes}m{seconds}s"


# }}}


if __name__ == "__main__":
    import sys

    if len(sys.argv) > 1:
        exec(sys.argv[1])
    else:
        pytest.main([__file__])
