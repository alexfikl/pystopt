# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

import numpy as np
import numpy.linalg as la
import pytest

from arraycontext import flatten
from pytential import GeometryCollection
from pytools.obj_array import make_obj_array

from pystopt import bind, sym
from pystopt.tools import (
    get_cl_array_context,
    get_default_logger,
    pytest_generate_tests_for_array_contexts,
)

import extra_test_data as etd

logger = get_default_logger(__file__)
filenamer = etd.get_filename_generator(__file__)
pytest_generate_tests = pytest_generate_tests_for_array_contexts([
    "pystopt.pyopencl",
])


# {{{ test_zinchenko_length_scale


@pytest.mark.parametrize(
    "cls",
    [
        etd.FourierCircleTestCase,
        etd.FourierEllipseTestCase,
        etd.SPHSphereTestCase,
        etd.SPHSpheroidTestCase,
    ],
)
def test_zinchenko_length_scale(actx_factory, cls, visualize):
    actx = get_cl_array_context(actx_factory)

    # {{{ geometry

    target_order = 3

    case = cls(
        qbx_order=target_order,
        mesh_arguments={
            "target_order": target_order,
            "transform_matrix": 2.0,
        },
    )
    logger.info("\n%s", case)

    if case.ambient_dim == 2:
        r = case.resolutions[-1]
    else:
        r = case.resolutions[-1]
        r = (r, r)

    discr = case.get_discretization(actx, r)
    logger.info("nelements: %d", discr.mesh.nelements)
    logger.info("ndofs:    %d", discr.ndofs)

    places = GeometryCollection(discr, auto_where=case.name)
    dofdesc = places.auto_source

    assert len(discr.groups) == 1
    mgrp = discr.mesh.groups[0]

    # }}}

    # {{{ setup

    from pystopt.mesh.connection import get_unique_mesh_vertex_connections

    to_conn, from_conn = get_unique_mesh_vertex_connections(places, dofdesc)

    w0 = actx.thaw(discr.nodes())
    if places.ambient_dim == 2:
        w0 = make_obj_array([w0[0], -w0[1]])
    else:
        w0 = make_obj_array([w0[0], -w0[1], w0[2]])

    w0 = actx.to_numpy(flatten(to_conn(w0), actx)).reshape(w0.size, -1)

    import pystopt.stabilization.zinchenko_util as zu

    r, d, _ = zu.get_edge_length_with_velocity(discr, mgrp, w0)

    # }}}

    # {{{ curvature terms

    z_length = zu.get_vertex_curvature_length(
        actx, places, to_conn, eps=1.0e-13, gamma=0.25, dofdesc=dofdesc
    )
    logger.info("avg(h): %.5e", np.mean(z_length))

    from pystopt.stabilization.zinchenko2013 import _z2013_cost_curvature

    z_curvature = _z2013_cost_curvature(mgrp, r, d, z_length)
    logger.info("avg(c): %.5e", np.mean(z_curvature))

    # }}}

    # {{{ compactness

    z_delta = zu.get_element_compactness_measure(discr)
    logger.info("avg(d): %.5e", np.mean(z_delta))

    from pystopt.stabilization.zinchenko2013 import _z2013_cost_compactness

    z_compact = _z2013_cost_compactness(mgrp, r, d, z_delta[0])
    logger.info("avg(c): %.5e", np.mean(z_compact))

    # }}}

    if not visualize:
        return

    nnodes = discr.groups[0].nunit_dofs

    def _from_vertex(x):
        y = DOFArray(actx, (actx.from_numpy(x.reshape(-1, 1)),))
        return from_conn(y)

    def _from_element(x):
        y = np.tile(x.reshape(-1, 1), nnodes)
        return DOFArray(actx, (actx.from_numpy(y),))

    from meshmode.dof_array import DOFArray

    z_length = _from_vertex(z_length)
    z_curvature = _from_element(z_curvature)
    z_compact = _from_element(z_compact)
    z_delta = _from_element(z_delta[0])

    kappa = bind(places, sym.summed_curvature(places.ambient_dim), auto_where=dofdesc)(
        actx
    )

    from pystopt.visualization import make_visualizer

    vis = make_visualizer(actx, discr, target_order)

    if places.ambient_dim == 2:
        filename = filenamer.with_suffix(f"zinchenko_{case.name}_length_curvature")
        vis.write_file(
            filename,
            [
                ("$h$", z_length),
                (r"$\gamma_1$", z_curvature),
                (r"$\kappa$", kappa),
            ],
            overwrite=True,
        )

        filename = filenamer.with_suffix(f"zinchenko_{case.name}_compactness")
        vis.write_file(
            filename,
            [
                # ("$d$", z_delta),
                (r"$\gamma_2$", z_compact),
            ],
            overwrite=True,
        )
    else:
        filename = filenamer.with_suffix(f"zinchenko_{case.name}")
        vis.write_file(
            filename,
            [
                (r"$\kappa$", kappa),
                ("$h_sqr$", z_length),
                ("$c_sqr$", z_delta),
                (r"$\gamma_1$", z_curvature),
                (r"$\gamma_2$", z_compact),
            ],
            overwrite=True,
        )


# }}}


# {{{ test_zinchenko_gradient


def run_zinchenko_finite_difference(actx, places, w0, *, fn=None, df=None):
    grad = df(w0).reshape(*w0.shape)
    c_0 = fn(w0)

    from pystopt.measure import EOCRecorder, stringify_eoc

    eoc = EOCRecorder(name="FD", expected_order=1.0)
    fgrad = np.empty_like(w0)

    # NOTE: in this range, we get a nice first order
    for n in range(2, 8):
        f_eps = 10 ** (-n)

        for i in np.ndindex(*w0.shape):
            w0[i] += f_eps

            c_eps = fn(w0)
            fgrad[i] = (c_eps - c_0) / f_eps

            w0[i] -= f_eps

        error = la.norm(grad - fgrad, np.inf) / la.norm(fgrad, np.inf)
        logger.info("error: %.5e", error)

        eoc.add_data_point(f_eps, error)

    logger.info("\n%s", stringify_eoc(eoc))

    return eoc, None


def run_veerapaneni_finite_difference(actx, places, xlm0, *, fn=None, df=None):
    grad = df(xlm0)
    c_0 = fn(xlm0)

    from pystopt.measure import EOCRecorder, stringify_eoc

    eoc = EOCRecorder(name="FD", expected_order=1.0)

    # NOTE: in this range, we get a nice first order
    from pystopt.dof_array import dof_array_norm, dof_array_rnorm

    fgrads = [("grad", grad)]

    for n in range(2, 8):
        f_eps = 10 ** (-n)
        fgrad = actx.np.zeros_like(xlm0)

        for d, *i in np.ndindex(xlm0.shape + xlm0[0][0].shape):
            xlm0[d][0][tuple(i)] += f_eps

            c_eps = fn(xlm0)
            fgrad[d][0][tuple(i)] = (c_eps - c_0) / f_eps

            xlm0[d][0][tuple(i)] -= f_eps

        fgrads.append((f"fgrad_{n}", fgrad))
        error = actx.to_numpy(dof_array_rnorm(grad, fgrad, ord=np.inf))
        error = actx.to_numpy(dof_array_norm(grad - fgrad, ord=np.inf))
        logger.info("error: %.5e", error)

        eoc.add_data_point(f_eps, error)

    logger.info("\n%s", stringify_eoc(eoc))

    return eoc, fgrads


def _v2011_perturbation(actx, discr, xlm):
    h = actx.np.ones_like(xlm) + 1j * actx.np.ones_like(xlm)

    from pystopt.filtering.spharm import filter_spharm_tikhonov_filter

    return make_obj_array([
        type(h_i)(
            actx,
            tuple([
                gh_i * filter_spharm_tikhonov_filter(actx, sgrp, alpha=1.0e-2, p=2.0)
                for gh_i, sgrp in zip(h_i, discr.specgroups, strict=True)
            ]),
        )
        for h_i in h
    ])


@pytest.mark.parametrize(
    ("method", "cls"),
    [
        ("zinchenko2002", etd.FourierEllipseTestCase),
        ("zinchenko2002", etd.SPHSpheroidTestCase),
        ("zinchenko2013", etd.FourierEllipseTestCase),
        ("zinchenko2013", etd.SPHSpheroidTestCase),
        ("veerapaneni2011", etd.SPHSpheroidTestCase),
    ],
)
def test_stabilization_gradient(actx_factory, method, cls, visualize):
    actx = get_cl_array_context(actx_factory)

    # {{{ geometry

    target_order = 3

    case = cls(target_order=target_order)
    logger.info("\n%s", case)

    discr = case.get_discretization(actx, case.resolutions[-2])
    logger.info("nelements: %d", discr.mesh.nelements)
    logger.info("ndofs:    %d", discr.ndofs)

    if method == "veerapaneni2011":
        from pystopt.mesh import update_discr_from_spectral

        xlm = actx.thaw(discr.xlm)
        discr = update_discr_from_spectral(
            actx, discr, xlm + _v2011_perturbation(actx, discr, xlm)
        )

    places = GeometryCollection(discr, auto_where=case.name)
    dofdesc = places.auto_source

    # }}}

    # {{{ velocity field

    x = 2.0 * np.pi * actx.thaw(discr.nodes())
    if places.ambient_dim == 2:
        w = make_obj_array([
            2.0 * actx.np.sin(x[0]) ** 2 * actx.np.cos(x[1]),
            2.0 * actx.np.cos(x[0]) * actx.np.sin(x[1]) ** 2,
        ])
        w = make_obj_array([x[0], -x[1]])
    elif places.ambient_dim == 3:
        w = make_obj_array([
            2.0 * actx.np.sin(x[0]) ** 2 * actx.np.sin(x[1]) * actx.np.sin(x[2]),
            -actx.np.sin(x[0]) * actx.np.sin(x[1]) ** 2 * actx.np.sin(x[2]),
            -actx.np.sin(x[0]) * actx.np.sin(x[1]) * actx.np.sin(x[2]) ** 2,
        ])
        w = make_obj_array([x[0], -x[1], actx.np.zeros_like(x[0])])
    else:
        raise ValueError(f"unsupported dimension: {places.ambient_dim}")

    # }}}

    # {{{ method

    if method in {"zinchenko2002", "zinchenko2013"}:
        stab_arguments = {"optim_options": {"maxiter": 512, "disp": True}}
    elif method == "veerapaneni2011":
        stab_arguments = {"optimized": True, "nmax": 8}
    else:
        raise ValueError(f"unknown method: '{method}'")

    from pystopt.stabilization import make_stabilizer_from_name

    stab = make_stabilizer_from_name(method, **stab_arguments)

    if method in {"zinchenko2002", "zinchenko2013"}:
        from pystopt.mesh.connection import get_unique_mesh_vertex_connections

        to_conn, _ = get_unique_mesh_vertex_connections(places, dofdesc)

        w0 = actx.to_numpy(flatten(to_conn(w), actx)).reshape(w.size, -1)
    elif method == "veerapaneni2011":
        w0 = actx.thaw(discr.xlm)
    else:
        raise ValueError(f"unknown method: '{method}'")

    from functools import partial

    if method == "zinchenko2002":
        import pystopt.stabilization.zinchenko_util as zu

        c_sqr = zu.get_element_compactness_measure(discr)

        from pystopt.stabilization.zinchenko2002 import _z2002_cost, _z2002_grad

        fn = partial(_z2002_cost, stab, discr, c_sqr)
        df = partial(_z2002_grad, stab, discr, c_sqr)
    elif method == "zinchenko2013":
        import pystopt.stabilization.zinchenko_util as zu

        h_sqr = zu.get_vertex_curvature_length(
            actx,
            places,
            to_conn,
            dofdesc=dofdesc,
            eps=stab.curvature_eps,
            gamma=stab.curvature_power,
        )
        c_sqr = zu.get_element_compactness_measure(discr)

        from pystopt.stabilization.zinchenko2013 import _z2013_cost, _z2013_grad

        fn = partial(_z2013_cost, stab, discr, h_sqr, c_sqr)
        df = partial(_z2013_grad, stab, discr, h_sqr, c_sqr)
    elif method == "veerapaneni2011":
        from pystopt.stabilization.veerapaneni2011 import (
            _v2011_cost_functional,
            _v2011_gradient,
        )

        def _update_collection(xlm):
            return places.merge({
                dofdesc.geometry: update_discr_from_spectral(actx, discr, xlm)
            })

        def fn(xlm):
            return _v2011_cost_functional(
                actx, _update_collection(xlm), alpha=stab.alpha, dofdesc=dofdesc
            )

        def df(xlm):
            return _v2011_gradient(
                actx, _update_collection(xlm), alpha=stab.alpha, dofdesc=dofdesc
            )

    else:
        raise ValueError(f"unknown stabilization method: '{method}'")

    # }}}

    # {{{ test

    if method in {"zinchenko2002", "zinchenko2013"}:
        eoc, fgrads = run_zinchenko_finite_difference(actx, places, w0, fn=fn, df=df)
    else:
        eoc, fgrads = run_veerapaneni_finite_difference(actx, places, w0, fn=fn, df=df)

    if visualize:
        from pystopt.measure import visualize_eoc

        filename = filenamer.with_suffix(f"{method}_{places.ambient_dim}d_order")
        visualize_eoc(filename, eoc, overwrite=True)

    v = None
    if visualize:
        v = stab(places, w, dofdesc=dofdesc)

    # }}}

    if not visualize:
        return

    from dataclasses import replace

    ambient_dim = places.ambient_dim
    basename = replace(filenamer, stem=f"{filenamer.stem}_{method}_{ambient_dim}d")

    from pystopt.visualization import make_visualizer

    vis = make_visualizer(actx, discr, target_order)

    if fgrads is not None:
        filename = basename.with_suffix("gradient")
        vis.write_file(
            filename,
            [(name, discr.from_spectral_conn(grad)) for name, grad in fgrads],
            overwrite=True,
        )

    filename = basename.with_suffix("velocity")
    vis.write_file(
        filename, [("v", v), ("w", w)], overwrite=True, show_separate="components"
    )

    if ambient_dim == 2:
        normal = bind(discr, sym.normal(ambient_dim).as_vector())(actx)
        tangent = make_obj_array([normal[1], -normal[0]])

        filename = basename.with_suffix("tangential")
        vis.write_file(
            filename,
            [
                (r"\mathbf{u} \cdot \mathbf{t}", v @ tangent),
                (r"\mathbf{w} \cdot \mathbf{t}", w @ tangent),
            ],
            overwrite=True,
        )


# }}}


# {{{ test_veerapaneni_gradient

# }}}


# {{{ test_tangential_velocity


@pytest.mark.parametrize(
    "cls",
    [
        etd.FourierCircleTestCase,
        etd.FourierEllipseTestCase,
    ],
)
@pytest.mark.parametrize("method", ["loewenberg1996", "kropinski2001"])
def test_tangential_velocity(actx_factory, cls, method, visualize):
    actx = get_cl_array_context(actx_factory)

    # {{{ geometry

    target_order = 3

    case = cls(target_order=target_order)
    logger.info("\n%s", case)

    discr = case.get_discretization(actx, case.resolutions[0])
    logger.info("nelements: %d", discr.mesh.nelements)
    logger.info("ndofs:    %d", discr.ndofs)

    places = GeometryCollection(discr, auto_where=case.name)
    dofdesc = places.auto_source

    # }}}

    # {{{

    from pystopt.stabilization import make_stabilizer_from_name

    stab = make_stabilizer_from_name(method)

    velocity = actx.thaw(discr.nodes())
    velocity = stab(places, velocity, dofdesc=dofdesc)

    # }}}

    if not visualize:
        return

    from pystopt.visualization import make_visualizer

    vis = make_visualizer(actx, discr)

    filename = filenamer.with_suffix(f"{method}_{case.name}")
    vis.write_file(
        filename,
        [
            ("u", velocity),
        ],
        add_legend=False,
        overwrite=True,
    )

    filename = filenamer.with_suffix(f"{method}_{case.name}_quiver")
    vis.write_file(
        filename,
        [
            ("u", velocity),
        ],
        write_mode="quiver",
        aspect="equal",
        add_legend=False,
        overwrite=True,
    )


@pytest.mark.slowtest
@pytest.mark.parametrize(
    "cls",
    [
        etd.SPHSpheroidTestCase,
    ],
)
@pytest.mark.parametrize(
    "method", ["veerapaneni2011", "zinchenko2002", "zinchenko2013"]
)
def test_tangential_velocity_3d(actx_factory, cls, method, visualize):
    actx = get_cl_array_context(actx_factory)

    # {{{ geometry

    target_order = 3

    case = cls(target_order=target_order)
    logger.info("\n%s", case)

    discr = case.get_discretization(actx, case.resolutions[-1])
    logger.info("nelements: %d", discr.mesh.nelements)
    logger.info("ndofs:    %d", discr.ndofs)

    places = GeometryCollection(discr, auto_where=case.name)
    dofdesc = places.auto_source

    # }}}

    # {{{

    from pytools import ProcessTimer

    kwargs = {}
    if method in {"zinchenko2002", "zinchenko2013"}:
        kwargs["optim_options"] = {"method": "L-BFGS-B", "disp": True}
    elif method == "veerapaneni2011":
        kwargs["nmax"] = case.resolutions[-1] // 4
    else:
        raise ValueError(f"unsupported method: '{method}'")

    from pystopt.stabilization import make_stabilizer_from_name

    stab = make_stabilizer_from_name(method, **kwargs)

    velocity = actx.thaw(discr.nodes())

    with ProcessTimer() as p:
        velocity = stab(places, velocity, dofdesc=dofdesc)

    logger.info("time: %s", p)

    # }}}

    if not visualize:
        return

    w = bind(places, sym.project_tangent(places.ambient_dim))(actx, x=velocity)

    from pystopt.visualization import make_visualizer

    vis = make_visualizer(actx, discr)

    filename = filenamer.with_suffix(f"{method}_{case.name}")
    vis.write_file(filename, [("u", velocity), ("w", w)], overwrite=True)


# }}}


if __name__ == "__main__":
    import sys

    import pyopencl as cl  # noqa: F401

    if len(sys.argv) > 1:
        exec(sys.argv[1])
    else:
        pytest.main([__file__])
