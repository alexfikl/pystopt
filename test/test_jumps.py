# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

from collections.abc import Callable
from dataclasses import dataclass
from functools import partial
from typing import Any

import numpy as np  # noqa: F401
import pytest

from pytential import GeometryCollection

from pystopt import bind, sym
from pystopt.dof_array import dof_array_rnorm
from pystopt.stokes import sti
from pystopt.tools import (
    get_cl_array_context,
    get_default_logger,
    pytest_generate_tests_for_array_contexts,
)

import extra_test_data as etd

logger = get_default_logger(__file__)
filenamer = etd.get_filename_generator(__file__)
pytest_generate_tests = pytest_generate_tests_for_array_contexts([
    "pystopt.pyopencl",
])


@dataclass(frozen=True)
class QBXJumpTestCase(etd.FourierEllipseTestCase):
    func: Callable = sti.velocity
    variable_name: str = "velocity"

    def density(self, actx, places):
        density_discr = places.get_discretization(self.name)

        nodes = actx.thaw(density_discr.nodes())
        r = actx.np.sqrt(sum(nodes**2))

        from pytools.obj_array import obj_array_vectorize

        return 2.0 * obj_array_vectorize(actx.np.cos, nodes) / r

    def jump(self, actx, places, op, density):
        density_sym = sym.make_sym_vector("sigma", places.ambient_dim)
        qbx_jump_sym = self.func(op, density_sym, qbx_forced_limit=+1) - self.func(
            op, density_sym, qbx_forced_limit=-1
        )

        return bind(places, qbx_jump_sym)(actx, sigma=density)


@dataclass(frozen=True)
class TaylorJumpTestCase(etd.SPHSpheroidTestCase):
    func: Callable = sti.velocity
    variable_name: str = "velocity"

    viscosity_ratio: float = 1.0
    capillary_number: float = 0.05

    solution: Any | None = None

    def density(self, actx, places):
        from pystopt.stokes import TwoPhaseSingleLayerStokesRepresentation

        op = TwoPhaseSingleLayerStokesRepresentation(self.solution.farfield)

        from pystopt.stokes import single_layer_solve

        result = single_layer_solve(
            actx,
            places,
            op,
            lambdas=[self.viscosity_ratio],
            sources=[self.name],
            context={"ca": self.capillary_number},
        )

        return result.density

    def jump(self, actx, places, op, density):
        sol_jump_sym = self.func(side=+1) - self.func(side=-1)

        return bind(places, sol_jump_sym)(
            actx, viscosity_ratio=self.viscosity_ratio, ca=self.capillary_number
        )


# {{{ run_jump_convergence


def run_jump_convergence(actx, case, resolution, *, visualize=False):
    qbx = case.get_layer_potential(actx, resolution)
    places = GeometryCollection(qbx, auto_where=case.name)

    density_discr = places.get_discretization(case.name)
    logger.info("ndofs: %d", density_discr.ndofs)

    # }}}

    # {{{ evaluate jumps

    from pystopt.stokes import StokesletJumpRelations, make_stokeslet

    op_knl = make_stokeslet(case.ambient_dim)
    op_jmp = StokesletJumpRelations(case.ambient_dim)

    # density
    density_sym = sym.make_sym_vector("sigma", places.ambient_dim)
    density = case.density(actx, places)

    # analytic stokes kernel jump
    stk_jump_sym = case.func(op_jmp, density_sym, side=+1) - case.func(
        op_jmp, density_sym, side=-1
    )
    stk_jump = bind(places, stk_jump_sym)(actx, sigma=density)

    # approximate jump (by some other method)
    apx_jump = case.jump(actx, places, op_knl, density)

    # errors
    h_max = actx.to_numpy(bind(places, sym.h_max_from_volume(case.ambient_dim))(actx))
    err = actx.to_numpy(dof_array_rnorm(apx_jump, stk_jump))
    logger.info("error: %.5e %.5e", h_max, err)

    # }}}

    if visualize:
        from pystopt.visualization import make_visualizer

        vis = make_visualizer(actx, density_discr, case.target_order)

        variable = case.variable_name
        filename = filenamer.with_suffix(f"{case.name}_{variable}_{resolution}")
        vis.write_file(
            filename,
            [
                ("approx", apx_jump),
                ("jump", stk_jump),
            ],
            markers=["-", "k--", "k:"],
            overwrite=True,
        )

        filename = filenamer.with_suffix(f"{case.name}_density_{resolution}")
        vis.write_file(
            filename,
            [
                ("sigma", density),
            ],
            overwrite=True,
        )

    return h_max, err


# }}}


# {{{ test_stokes_qbx_jumps


@pytest.mark.parametrize(
    ("name", "func"),
    [
        (None, sti.pressure),
        (None, sti.velocity),
        (None, sti.traction),
        ("tangent_traction", partial(sti.tangent_traction, tangent_idx=0)),
        (None, sti.normal_velocity_gradient),
        (
            "tangent_velocity_gradient",
            partial(sti.tangent_velocity_gradient, tangent_idx=0),
        ),
        (None, sti.normal_velocity_laplacian),
        (None, sti.normal_velocity_hessian),
    ],
)
def test_stokes_qbx_jumps(actx_factory, name, func, visualize):
    actx = get_cl_array_context(actx_factory)

    target_order = 4
    case = QBXJumpTestCase(
        target_order=target_order,
        qbx_order=target_order,
        resolutions=[32, 64, 128, 160, 192],
        source_ovsmp=4,
        func=func,
        variable_name=name if name is not None else func.__qualname__,
    )
    logger.info("\n%s", str(case))

    from pystopt.measure import EOCRecorder, stringify_eoc

    eoc = EOCRecorder(name=case.variable_name)

    for r in case.resolutions:
        h_max, err = run_jump_convergence(
            actx, case, r, visualize=visualize and r == case.resolutions[-1]
        )
        eoc.add_data_point(h_max, err)

    logger.info("\n%s", stringify_eoc(eoc))

    if visualize:
        from pystopt.measure import visualize_eoc

        filename = filenamer.with_suffix(f"{eoc.name}_eoc")
        visualize_eoc(filename, eoc, overwrite=True)

    order = {
        # zero derivatives
        "pressure": target_order - 0.3,
        "velocity": target_order + 0.1,
        "traction": target_order + 0.5,
        "tangent_traction": target_order + 0.5,
        # first derivatives
        "normal_velocity_gradient": target_order,
        "tangent_velocity_gradient": target_order - 0.5,
        # second derivatives
        "normal_velocity_laplacian": target_order - 1.25,
        "normal_velocity_hessian": target_order - 1.0,
    }[eoc.name]

    assert eoc.satisfied(order=order)


# }}}


if __name__ == "__main__":
    import sys

    if len(sys.argv) > 1:
        exec(sys.argv[1])
    else:
        pytest.main([__file__])
