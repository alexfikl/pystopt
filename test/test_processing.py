# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

import numpy as np
import pytest

import meshmode.discretization.poly_element as mpoly
from pytential import GeometryCollection

from pystopt.tools import (
    get_cl_array_context,
    get_default_logger,
    pytest_generate_tests_for_array_contexts,
)

import extra_test_data as etd

logger = get_default_logger(__file__)
filenamer = etd.get_filename_generator(__file__)
pytest_generate_tests = pytest_generate_tests_for_array_contexts([
    "pystopt.pyopencl",
])


# {{{ test_yukawa_filter


@pytest.mark.slowtest
@pytest.mark.parametrize(
    "cls",
    [
        etd.FourierCircleTestCase,
        etd.SPHSphereTestCase,
    ],
)
def test_yukawa_filter(actx_factory, cls, visualize):
    actx = get_cl_array_context(actx_factory)

    strength = 1.0e-2
    target_order = 3
    case = cls(
        target_order=target_order,
        source_ovsmp=3,
        fmm_order=10,
        disable_refinement=True,
    )
    logger.info("\n%s", str(case))

    # {{{ geometry

    resolution = case.resolutions[-1]
    qbx = case.get_layer_potential(actx, resolution)
    places = GeometryCollection(qbx, auto_where=case.name)
    dofdesc = places.auto_source

    density_discr = places.get_discretization(case.name)
    logger.info("ndofs:         %d", density_discr.ndofs)
    logger.info("nelements:     %d", density_discr.mesh.nelements)

    # }}}

    # {{{ tests

    # auto_where = case.name

    def fn(t):
        return actx.np.cos(2.0 * t) + actx.np.sin(t) - actx.np.cos(5.0 * t)

    # create a density to smooth
    nodes = actx.thaw(density_discr.nodes())
    sigma = nodes[0]
    sigma = fn(actx.np.arctan2(nodes[1], nodes[0]))
    # sigma = actx.np.ones_like(sigma)

    # add some noise
    from pystopt.dof_array import uniform_like

    noise = uniform_like(actx, sigma, a=-1.0, b=1.0, seed=42)

    amplitude = 0.01
    sigma_noisy = sigma + amplitude * noise

    # run filter on noisy version
    from pystopt.filtering import apply_yukawa_smoothing as apply_yukawa_filter

    sigma_smoothed = apply_yukawa_filter(
        actx,
        places,
        sigma_noisy,
        strength=strength,
        gmres_arguments={"maxiter": 64},
        dofdesc=dofdesc,
    )
    logger.info(
        "sigma: [%.5e, %.5e]",
        actx.to_numpy(actx.np.min(sigma)),
        actx.to_numpy(actx.np.max(sigma)),
    )
    logger.info(
        "sigma: [%.5e, %.5e]",
        actx.to_numpy(actx.np.min(sigma_smoothed)),
        actx.to_numpy(actx.np.max(sigma_smoothed)),
    )

    # from pystopt.filtering import apply_filter_spectral
    # sigma_smoothed = apply_filter_spectral(actx, density_discr, sigma_smoothed,
    #         method="ideal", kmax=resolution // 2 - 1)

    # run filter on smooth version
    sigma_smooth = apply_yukawa_filter(
        actx,
        places,
        sigma,
        strength=strength,
        gmres_arguments={"maxiter": 64},
        dofdesc=dofdesc,
    )
    logger.info(
        "sigma: [%.5e, %.5e]",
        actx.to_numpy(actx.np.min(sigma_smooth)),
        actx.to_numpy(actx.np.max(sigma_smooth)),
    )

    from pystopt import bind, sym

    h_max = bind(places, sym.h_max(places.ambient_dim))(actx)
    h_min = bind(places, sym.h_min(places.ambient_dim))(actx)
    logger.info("h: [%.5e, %.5e]", actx.to_numpy(h_min), actx.to_numpy(h_max))

    # }}}

    if not visualize:
        return

    from pystopt.visualization import make_visualizer

    vis = make_visualizer(actx, density_discr)

    filename = filenamer.with_suffix(f"filter_yukawa_{case.name}_values")
    vis.write_file(
        filename,
        [
            (r"\sigma", sigma),
            (r"\epsilon", sigma_noisy),
            (r"\hat{\sigma}", sigma_smoothed),
        ],
        markers=["-", "-", "k-"],
        overwrite=True,
    )

    if places.ambient_dim == 2:
        from pystopt.mesh.fourier import visualize_fourier_modes

        filename = filenamer.with_suffix(f"filter_yukawa_{case.name}_filter")
        visualize_fourier_modes(
            filename,
            actx,
            density_discr,
            [
                (r"\sigma", sigma),
                (r"\hat{\sigma}", sigma_smoothed),
                (r"\epsilon", sigma_noisy),
            ],
            markers=["-", "-", "k-"],
            overwrite=True,
        )

        filename = filenamer.with_suffix(f"filter_yukawa_{case.name}_smooth")
        visualize_fourier_modes(
            filename,
            actx,
            density_discr,
            [
                (r"\sigma", sigma),
                (r"\hat{\sigma}", sigma_smooth),
            ],
            overwrite=True,
        )
    else:
        raise NotImplementedError


# }}}


# {{{ test_fourier_filter


@pytest.mark.parametrize("method", ["ideal", "trapezoidal", "butterworth"])
def test_fourier_filter(actx_factory, method, visualize):
    actx = get_cl_array_context(actx_factory)

    target_order = 3
    case = etd.FourierEllipseV2TestCase(target_order=target_order)
    discr = case.get_discretization(actx, case.resolutions[-2])

    logger.info("ndofs:     %d", discr.ndofs)
    logger.info("nelements: %d", discr.mesh.nelements)

    # {{{

    from pystopt.filtering import apply_filter_spectral

    f = actx.thaw(discr.nodes()[0])
    fhat = apply_filter_spectral(actx, discr, f, method=method)

    # }}}

    if not visualize:
        return

    from pystopt.mesh.fourier import visualize_fourier_modes

    filename = filenamer.with_suffix(f"filter_fourier_{method}")
    visualize_fourier_modes(
        filename,
        actx,
        discr,
        [
            ("f", f),
            (r"\hat{f}", fhat),  # noqa: RUF027
        ],
        overwrite=True,
    )


# }}}


# {{{ test_spharm_filter


@pytest.mark.parametrize("method", ["ideal", "tikhonov", "tikhonov_opt"])
def test_spharm_filter(actx_factory, method, visualize):
    actx = get_cl_array_context(actx_factory)

    target_order = 3
    case = etd.SPHUrchinTestCase(target_order=target_order)
    discr = case.get_discretization(actx, case.resolutions[-1])

    logger.info("ndofs:     %d", discr.ndofs)
    logger.info("nelements: %d", discr.mesh.nelements)

    # {{{

    from pystopt.filtering import apply_filter_spectral

    f = actx.thaw(discr.nodes()[0])
    if method == "ideal":
        fhat = apply_filter_spectral(actx, discr, f, method=method, kmax=6)
    elif method == "tikhonov":  # noqa: SIM114
        fhat = apply_filter_spectral(actx, discr, f, method=method)
    elif method == "tikhonov_opt":
        fhat = apply_filter_spectral(actx, discr, f, method=method)
    else:
        raise ValueError(method)

    # }}}

    if not visualize:
        return

    from pystopt.mesh.spharm import visualize_spharm_modes

    filename = filenamer.with_suffix(f"filter_spharm_{method}_before")
    visualize_spharm_modes(filename, discr, [("f", f)], overwrite=True)

    filename = filenamer.with_suffix(f"filter_spharm_{method}_after")
    visualize_spharm_modes(filename, discr, [("f", fhat)], overwrite=True)

    from pystopt.visualization import make_visualizer

    vis = make_visualizer(actx, discr, vis_order=target_order)

    filename = filenamer.with_suffix(f"filter_spharm_{method}")
    vis.write_file(filename, [], overwrite=True)


# }}}


# {{{ test_spharm_tikhonov_opt


@pytest.mark.parametrize("noisy", [True, False])
def test_spharm_tikhonov_opt(actx_factory, noisy, visualize):
    actx = get_cl_array_context(actx_factory)

    from dataclasses import replace

    target_order = 3
    case = etd.SPHSphereTestCase(
        target_order=target_order,
        mesh_arguments={"transform_matrix": np.array([1.0, 1.0, 2.0])},
    )

    resolution = 64
    discr = case.get_discretization(actx, resolution)

    mesh_arguments = case.mesh_arguments.copy()
    mesh_arguments["use_default_mesh_unit_nodes"] = False
    equi_discr = replace(
        case,
        group_factory_cls=mpoly.InterpolatoryEquidistantGroupFactory,
        mesh_arguments=mesh_arguments,
    ).get_discretization(actx, resolution)

    logger.info("ndofs:     %d", discr.ndofs)
    logger.info("nelements: %d", discr.mesh.nelements)

    # {{{ filter interpolation error

    from pystopt import bind, sym

    f = bind(discr, sym.normal(discr.ambient_dim).as_vector()[0])(actx)
    fequi = bind(equi_discr, sym.normal(discr.ambient_dim).as_vector()[0])(actx)

    if noisy:
        from pystopt.dof_array import uniform_like

        amplitude = 0.1
        f = f + amplitude * uniform_like(actx, f)

    from pystopt.filtering import apply_filter_spectral

    fk = discr.to_spectral_conn(f)
    fhat = apply_filter_spectral(actx, discr, fk, method="tikhonov_opt", p=2)

    # }}}

    # {{{ check gcv gradient

    p = 2
    alpha = 0.1

    from pystopt.filtering.spharm import _tikhonov_gcv, _tikhonov_gcv_gradient

    gcv_f = _tikhonov_gcv(actx, discr, fk, p=p, alpha=alpha)
    gcv_g = _tikhonov_gcv_gradient(actx, discr, fk, p=p, alpha=alpha)

    from pystopt.measure import EOCRecorder, stringify_eoc

    eoc = EOCRecorder(name="gcv")

    eps = 10.0 ** (-np.arange(2, 10))
    for i in range(eps.size):
        gcv_f_eps = _tikhonov_gcv(actx, discr, fk, p=p, alpha=alpha + eps[i])
        gcv_g_eps = (gcv_f_eps - gcv_f) / eps[i]

        error = abs(gcv_g_eps - gcv_g) / abs(gcv_g)
        eoc.add_data_point(eps[i], error)
        logger.info(
            "eps %.5e value %.5e expected %.5e error %.5e",
            eps[i],
            gcv_g_eps,
            gcv_g,
            error,
        )

    logger.info("\n%s", stringify_eoc(eoc))
    assert eoc.satisfied(order=1.0)

    if visualize:
        from pystopt.measure import visualize_eoc

        filename = filenamer.with_suffix("filter_spharm_tikhonov_opt_convergence")
        visualize_eoc(filename, eoc, order=1, overwrite=True)

    # }}}

    # {{{ evaluate gcv cost

    if visualize:
        p = np.array([1, 2, 3])
        alpha = np.logspace(-9, 1, 256)

        gcv = np.empty((p.size, alpha.size))
        for i in range(p.size):
            for j in range(alpha.size):
                gcv[i, j] = _tikhonov_gcv(actx, discr, fk, p=p[i], alpha=alpha[j])

    # }}}

    if not visualize:
        return

    from pystopt.mesh.spharm import visualize_spharm_modes

    filename = filenamer.with_suffix("filter_spharm_tikhonov_opt_equidistant")
    visualize_spharm_modes(
        filename, equi_discr, [("f", fequi)], mode="byorder", overwrite=True
    )

    filename = filenamer.with_suffix("filter_spharm_tikhonov_opt_before")
    visualize_spharm_modes(filename, discr, [("f", f)], mode="byorder", overwrite=True)

    filename = filenamer.with_suffix("filter_spharm_tikhonov_opt_after")
    visualize_spharm_modes(
        filename, discr, [("f", fhat)], mode="byorder", overwrite=True
    )

    from pystopt.visualization.matplotlib import subplots

    filename = filenamer.with_suffix("filter_spharm_tikhonov_opt_cost")
    with subplots(filename, overwrite=True) as fig:
        ax = fig.gca()

        for i in range(p.size):
            ax.loglog(alpha, gcv[i], label=f"$p = {p[i]}$")

        ax.set_xlabel(r"$\alpha$")
        ax.set_ylabel("$GCV$")
        ax.legend(
            bbox_to_anchor=(0, 1.02, 1.0, 0.2),
            loc="lower left",
            mode="expand",
            borderaxespad=0,
            ncol=3,
        )


# }}}


# {{{ test_spectral_filter_on_noise


@pytest.mark.parametrize("ambient_dim", [2, 3])
def test_spectral_filter_on_noise(actx_factory, ambient_dim, visualize):
    actx = get_cl_array_context(actx_factory)

    # {{{ geometry

    target_order = 3

    if ambient_dim == 2:
        case = etd.FourierCircleTestCase(target_order=target_order)
        geom = etd.FourierStarfishTestCase(target_order=target_order)
    elif ambient_dim == 3:
        case = etd.SPHSphereTestCase(target_order=target_order)
        geom = etd.SPHUFOTestCase(target_order=target_order)
    else:
        raise ValueError(ambient_dim)

    discr = case.get_discretization(actx, case.resolutions[-1])
    test_discr = geom.get_discretization(actx, case.resolutions[-1])

    # }}}

    # {{{ filter test data

    # create a density to smooth
    nodes = actx.thaw(test_discr.nodes())
    sigma = nodes[-1]

    # add some noise
    from pystopt.dof_array import uniform_like

    amplitude = 0.25
    noise = uniform_like(actx, sigma, a=-amplitude, b=amplitude, seed=42)

    sigma_noisy = sigma + noise

    # add some filtering
    from pystopt.filtering import apply_filter_spectral

    sigma_filter = apply_filter_spectral(actx, discr, sigma_noisy, method="ideal")

    # }}}

    # {{{ check

    if not visualize:
        return

    from pystopt.visualization import make_visualizer

    vis = make_visualizer(actx, discr, vis_order=target_order)

    filename = filenamer.with_suffix(f"spectral_filter_{ambient_dim}d")
    vis.write_file(
        filename,
        [
            (r"\sigma_n", sigma_noisy),
            (r"\sigma", sigma),
            (r"\sigma_f", sigma_filter),
            # ("error", actx.np.log10(actx.np.abs(sigma - sigma_filter))),
        ],
        overwrite=True,
    )

    # }}}


# }}}


if __name__ == "__main__":
    import sys

    import pyopencl as cl  # noqa: F401

    if len(sys.argv) > 1:
        exec(sys.argv[1])
    else:
        pytest.main([__file__])
