# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

import numpy as np
import pytest

from pytential import GeometryCollection

from pystopt import bind
from pystopt.dof_array import dof_array_norm, dof_array_rnorm
from pystopt.stokes import sti
from pystopt.tools import (
    get_cl_array_context,
    get_default_logger,
    pytest_generate_tests_for_array_contexts,
)

import extra_test_data as etd

logger = get_default_logger(__file__)
filenamer = etd.get_filename_generator(__file__)
pytest_generate_tests = pytest_generate_tests_for_array_contexts([
    "pystopt.pyopencl",
])


# {{{ analytic


@pytest.mark.parametrize("solution_name", ["hadamard", "taylor"])
@pytest.mark.parametrize("viscosity_ratio", [1.0, 5.0])
def test_analytic_solution(actx_factory, solution_name, viscosity_ratio, visualize):
    actx = get_cl_array_context(actx_factory)

    # {{{ setup

    radius = 1.0
    target_order = 3
    rel_tol = 4.0e-8
    context = {"viscosity_ratio": viscosity_ratio, "ca": 0.05}

    if solution_name == "hadamard":
        from pystopt.stokes import HadamardRybczynski

        solution = HadamardRybczynski(radius=radius)
    elif solution_name == "taylor":
        from pystopt.stokes import Taylor

        solution = Taylor(radius=radius)
    else:
        raise ValueError(f"unknown solution: {solution_name}")

    case = etd.SPHSphereTestCase(
        target_order=target_order,
        qbx_order=target_order,
        source_ovsmp=2,
        mesh_arguments={"radius": solution.radius},
    )

    logger.info("\n%s", str(case))

    # }}}

    # {{{ geometry

    discr = case.get_discretization(actx, case.resolutions[-1])
    places = GeometryCollection(discr)

    logger.info("ndofs: %d", discr.ndofs)
    logger.info("nelements: %d", discr.mesh.nelements)

    # }}}

    # {{{ check

    from functools import partial

    import pystopt.symbolic.sympy_interop as sp

    to_pytential = partial(sp.to_pytential, 3)

    normal = bind(places, to_pytential(solution.normal))(actx)

    # {{{ divergence free

    def _check_divu(side):
        divu_sym = sp.divergence(solution[side].velocity)
        divu = bind(places, to_pytential(divu_sym))(actx, **context)
        error = dof_array_norm(divu)
        # assert error < 1.0e-13, error

        return actx.to_numpy(error)

    error_divu_ext = _check_divu(+1)
    logger.info("error  'div(u_ext)'                    %.15e", error_divu_ext)

    error_divu_int = _check_divu(-1)
    logger.info("error  'div(u_int)'                    %.15e", error_divu_int)

    # }}}

    # {{{ momentum

    def _check_stokes(side):
        stokes_sym = -sp.gradient(3, solution[side].pressure) + sp.vector_laplacian(
            solution[side].velocity
        )
        stokes = bind(places, to_pytential(stokes_sym))(actx, **context)
        error = dof_array_norm(stokes)
        # assert error < 1.0e-13, error

        return error if np.isscalar(error) else actx.to_numpy(error)

    error_stk_ext = _check_stokes(+1)
    logger.info("error  '-grad(p_ext) + lap(u_ext)'     %.15e", error_stk_ext)

    error_stk_int = _check_stokes(-1)
    logger.info("error  '-grad(p_int) + lap(u_int)'     %.15e", error_stk_int)

    # }}}

    # {{{ continuity of velocity

    u_ext = bind(places, sti.velocity(solution, side=+1))(actx, **context)
    u_int = bind(places, sti.velocity(solution, side=-1))(actx, **context)

    u_error = actx.to_numpy(dof_array_rnorm(u_ext, u_int))
    logger.info("error  'jump(u)'                       %.15e", u_error)

    # }}}

    # {{{ jump in normal stress

    f_ext = bind(places, sti.traction(solution, side=+1))(actx, **context)
    f_int = bind(places, sti.traction(solution, side=-1))(actx, **context)
    f_jmp = bind(places, sti.deltaf(solution))(actx, **context)

    fn_ext = f_ext.dot(normal)
    fn_int = f_int.dot(normal)
    fn_jmp = f_jmp.dot(normal)

    fn_error = actx.to_numpy(dof_array_rnorm(fn_ext, viscosity_ratio * fn_int))
    logger.info("error  'jump(fn)'                      %.15e", fn_error)

    jp_error = actx.to_numpy(dof_array_rnorm(fn_ext - viscosity_ratio * fn_int, fn_jmp))
    logger.info("error  'jump(fn) - deltaf'             %.15e", jp_error)

    # }}}

    # {{{ jump in pressure

    p_ext = bind(places, sti.pressure(solution, side=+1))(actx, **context)
    p_int = bind(places, sti.pressure(solution, side=-1))(actx, **context)

    p_jmp = -(p_ext - viscosity_ratio * p_int)
    p_error = actx.to_numpy(dof_array_rnorm(p_jmp, fn_jmp))
    logger.info("error  'jump(p) - jump(fn)'            %.15e", p_error)

    # }}}

    # {{{ jump in tangential stress

    ft_ext = f_ext - (f_ext @ normal) * normal
    ft_int = f_int - (f_int @ normal) * normal

    ft_error = actx.to_numpy(dof_array_rnorm(ft_ext, viscosity_ratio * ft_int))
    logger.info("error  'jump(ft)'                      %.15e", ft_error)

    # }}}

    # }}}

    # {{{ plotting

    if visualize:
        filename = filenamer.with_suffix(f"{solution_name}_variables")
        fields_and_names = [
            ("u_ext", u_ext),
            ("u_int", u_int),
            ("f_ext", f_ext),
            ("f_int", f_int),
            ("p_ext", p_ext),
            ("p_int", p_int),
            ("ft_jmp", ft_ext - viscosity_ratio * ft_int),
        ]

        from pystopt.visualization import make_visualizer

        vis = make_visualizer(actx, discr, case.target_order)
        vis.write_file(filename, fields_and_names, overwrite=True)

    # }}}

    assert u_error < 2 * 1000 * rel_tol
    assert fn_error > rel_tol
    assert jp_error < rel_tol

    # FIXME: this should definitely not be this big??
    assert ft_error < 2 * 10000 * rel_tol

    if abs(viscosity_ratio - 1.0) < 1.0e-14:
        assert p_error < 2 * 100 * rel_tol
    else:
        assert p_error > rel_tol


# }}}


# {{{ boundary conditions


@pytest.mark.parametrize(
    ("boundary_name", "boundary_kwargs"),
    [
        ("extensional", {"alpha": 1.0}),
        ("uniform", {"uinf": 1.0}),
        ("solid_body_rotation", {"omega": 1.0}),
        ("helix", {"tmax": 1.0}),
    ],
)
def test_boundary_conditions(actx_factory, boundary_name, boundary_kwargs, visualize):
    actx = get_cl_array_context(actx_factory)

    # {{{ geometry

    ambient_dim = 3

    from pystopt.stokes import get_farfield_boundary_from_name

    bc = get_farfield_boundary_from_name(ambient_dim, boundary_name, **boundary_kwargs)

    if ambient_dim == 2:
        case = etd.FourierStarfishTestCase()
    elif ambient_dim == 3:
        case = etd.SPHUFOTestCase()
    else:
        raise ValueError(f"unsupported dimension: {ambient_dim}d")

    logger.info("\n%s", case)

    discr = case.get_discretization(actx, case.resolutions[-1])
    places = GeometryCollection(discr)

    # }}}

    # {{{ check

    velocity = bind(places, sti.velocity(bc))(actx)
    pressure = bind(places, sti.pressure(bc))(actx)
    traction = bind(places, sti.traction(bc))(actx)
    tangent_traction = bind(places, sti.tangent_traction(bc, 0))(actx)
    normal_velocity_gradient = bind(places, sti.normal_velocity_gradient(bc))(actx)
    tangent_velocity_gradient = bind(places, sti.tangent_velocity_gradient(bc, 0))(actx)
    normal_velocity_laplacian = bind(places, sti.normal_velocity_laplacian(bc))(actx)
    normal_velocity_hessian = bind(places, sti.normal_velocity_hessian(bc))(actx)

    assert dof_array_norm(pressure) < 1.0e-12

    if boundary_name == "uniform":
        assert dof_array_norm(normal_velocity_gradient) < 1.0e-12
        assert dof_array_norm(tangent_velocity_gradient) < 1.0e-12
    else:
        assert dof_array_norm(normal_velocity_gradient) > 1.0e-12
        assert dof_array_norm(tangent_velocity_gradient) > 1.0e-12

    assert dof_array_norm(normal_velocity_laplacian) < 1.0e-12
    assert dof_array_norm(normal_velocity_hessian) < 1.0e-12

    # }}}

    # {{{ plot

    if not visualize:
        return

    from pystopt.visualization import make_visualizer

    vis = make_visualizer(actx, discr, case.target_order)

    vis.write_file(
        filenamer.with_suffix(boundary_name),
        [
            ("velocity", velocity),
            ("pressure", pressure),
            ("traction", traction),
            ("tangent_traction", tangent_traction),
            ("normal_velocity_gradient", normal_velocity_gradient),
            ("tangent_velocity_gradient", tangent_velocity_gradient),
            ("normal_velocity_laplacian", normal_velocity_laplacian),
            ("normal_velocity_hessian", normal_velocity_hessian),
        ],
        show_separate="fields",
        overwrite=True,
    )

    # }}}


if __name__ == "__main__":
    import sys

    if len(sys.argv) > 1:
        exec(sys.argv[1])
    else:
        pytest.main([__file__])
