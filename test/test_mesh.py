# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

import numpy as np
import numpy.linalg as la
import pytest

import meshmode.discretization.poly_element as mpoly
from arraycontext import flatten
from meshmode.dof_array import DOFArray
from meshmode.mesh import SimplexElementGroup, TensorProductElementGroup
from pytools.obj_array import make_obj_array

from pystopt.tools import (
    get_cl_array_context,
    get_default_logger,
    pytest_generate_tests_for_array_contexts,
)

import extra_test_data as etd

logger = get_default_logger(__file__)
filenamer = etd.get_filename_generator(__file__)
pytest_generate_tests = pytest_generate_tests_for_array_contexts([
    "pystopt.pyopencl",
])


# {{{ test_reconstruct_discr


@pytest.mark.parametrize("ambient_dim", [3])
@pytest.mark.parametrize(
    "group_factory",
    [
        mpoly.InterpolatoryQuadratureGroupFactory,
        # mpoly.InterpolatoryEquidistantGroupFactory,
        # mpoly.InterpolatoryEdgeClusteredGroupFactory,
    ],
)
def test_reconstruct_discr(actx_factory, ambient_dim, group_factory, visualize):
    actx = get_cl_array_context(actx_factory)

    # {{{ geometry

    if ambient_dim == 2:
        target_order = 4
        case = etd.FourierCircleTestCase(
            resolutions=[64],
            qbx_order=target_order,
            mesh_arguments={
                "target_order": target_order,
                "group_factory_cls": group_factory,
            },
        )
    elif ambient_dim == 3:
        target_order = 3
        case = etd.SPHSphereTestCase(
            resolutions=[16],
            qbx_order=target_order,
            mesh_arguments={
                "target_order": target_order,
                "group_factory_cls": group_factory,
            },
        )
    else:
        raise ValueError(f"unsupported dimension: {ambient_dim}")

    discr_orig = case.get_discretization(actx, case.resolutions[-1]).copy(xlm=None)

    logger.info("factory:   %s", group_factory.__name__)
    logger.info("nelements: %d", discr_orig.mesh.nelements)
    logger.info("ndofs:    %d", discr_orig.ndofs)

    # }}}

    from pystopt.dof_array import dof_array_rnorm

    def transformation(x, *, as_numpy=False):
        cos = np.cos if as_numpy else actx.np.cos
        return make_obj_array([
            x[0] + 0.1 * cos(2.0 * np.pi * x[1]),
            x[1] + 0.1 * cos(2.0 * np.pi * x[0]),
            x[2] if ambient_dim == 3 else 0.0,
        ])[:ambient_dim]

    # transform mesh
    from pystopt.mesh import update_discr_from_nodes

    nodes = transformation(actx.thaw(discr_orig.nodes()))

    discr_new = update_discr_from_nodes(
        actx,
        discr_orig,
        nodes,
        group_factory=group_factory(order=target_order),
        keep_vertices=True,
    )

    # check nodes are transformed correctly
    nodes_new = actx.thaw(discr_new.nodes())
    node_error = actx.to_numpy(dof_array_rnorm(nodes, nodes_new))
    logger.info("node error: %.5e", node_error)
    assert node_error < 1.0e-13

    # check vertices are transformed correctly
    vertices_new = discr_new.mesh.vertices
    if discr_orig.mesh.vertices.shape == vertices_new.shape:
        vertices = discr_orig.mesh.vertices
    else:
        from pystopt.mesh.processing import duplicate_mesh_vertices

        vertices = duplicate_mesh_vertices(discr_orig.mesh)

    vertices = np.vstack(transformation(vertices, as_numpy=True))

    vertex_error = la.norm(vertices_new - vertices) / la.norm(vertices)
    logger.info("vertex error: %.5e", vertex_error)

    # check repeated transformations don't mess anything up
    for _ in range(32):
        nodes_new = actx.thaw(discr_new.nodes())
        discr_new = update_discr_from_nodes(
            actx,
            discr_new,
            nodes_new,
            group_factory=group_factory(order=target_order),
        )

    nodes_new = actx.thaw(discr_new.nodes())
    node_error = actx.to_numpy(dof_array_rnorm(nodes, nodes_new))
    logger.info("node error: %.5e", node_error)
    assert node_error < 2.0e-13

    if visualize:
        from pystopt.visualization import make_visualizer

        for name, discr in zip(["orig", "new"], [discr_orig, discr_new], strict=False):
            vis = make_visualizer(actx, discr, target_order)

            filename = filenamer.with_suffix(f"reconstruct_{name}_high")
            vis.write_file(filename, [], overwrite=True)

            vis = make_visualizer(actx, discr, 1)

            filename = filenamer.with_suffix(f"reconstruct_{name}_low")
            vis.write_file(filename, [], overwrite=True)

    if group_factory is mpoly.InterpolatoryQuadratureGroupFactory:
        if ambient_dim == 2:
            assert vertex_error < 7.0e-8
        else:
            assert vertex_error < 5.0e-4
    else:
        assert vertex_error < 1.0e-13


# }}}


# {{{ test_spectral_reconstruction


@pytest.mark.parametrize("ambient_dim", [2, 3])
def test_spectral_reconstruction(actx_factory, ambient_dim, visualize):
    actx = get_cl_array_context(actx_factory)

    target_order = 3
    mesh_arguments = {}
    if ambient_dim == 2:
        from_case = etd.FourierCircleTestCase(mesh_arguments=mesh_arguments)
        to_case = etd.FourierStarfishTestCase(mesh_arguments=mesh_arguments)
    elif ambient_dim == 3:
        from_case = etd.SPHSphereTestCase(mesh_arguments=mesh_arguments)
        to_case = etd.SPHUFOTestCase(mesh_arguments=mesh_arguments)
    else:
        raise ValueError("unsupported dimension")

    logger.info("\n%s", to_case)

    # {{{ geometry

    resolution = from_case.resolutions[-1]

    from_discr = from_case.get_discretization(actx, resolution)
    to_discr = to_case.get_discretization(actx, resolution)
    ylm = actx.thaw(to_discr.xlm)

    logger.info("ndofs:     %d", from_discr.ndofs)
    logger.info("nelements: %d", from_discr.mesh.nelements)

    # }}}

    # {{{ reconstruct

    from pystopt.mesh import update_discr_from_spectral

    to_recon = update_discr_from_spectral(actx, from_discr, ylm)

    to_nodes = actx.thaw(to_discr.nodes())
    recon_nodes = actx.thaw(to_recon.nodes())

    from pystopt.dof_array import dof_array_rnorm

    error = actx.to_numpy(dof_array_rnorm(to_nodes, recon_nodes))
    logger.info("error: %.5e", error)
    assert error < 1.0e-13

    # }}}

    if not visualize:
        return

    from pystopt.visualization import make_visualizer

    vis = make_visualizer(actx, to_discr, vis_order=target_order)
    vis.write_file(
        filenamer.with_suffix("reconstruction_target"),
        [("x", to_nodes)],
        overwrite=True,
    )

    vis = make_visualizer(actx, to_recon, vis_order=target_order)
    vis.write_file(
        filenamer.with_suffix("reconstruction_recon"),
        [("x", recon_nodes)],
        overwrite=True,
    )


# }}}


# {{{ test_vertex_connection


@pytest.mark.parametrize("ambient_dim", [2, 3])
def test_vertex_connection(actx_factory, ambient_dim, visualize):
    actx = get_cl_array_context(actx_factory)

    # {{{ geometry

    target_order = 3

    if ambient_dim == 2:
        cls = etd.FourierCircleTestCase
    elif ambient_dim == 3:
        cls = etd.SPHSphereTestCase
    else:
        raise ValueError(f"unsupported dimension: {ambient_dim}")

    case = cls(
        target_order=target_order,
        group_factory_cls=mpoly.InterpolatoryEdgeClusteredGroupFactory,
        qbx_order=target_order,
    )
    logger.info("\n%s", case)

    discr = case.get_discretization(actx, case.resolutions[1])
    mesh = discr.mesh

    logger.info("nelements: %d", discr.mesh.nelements)
    logger.info("ndofs:    %d", discr.ndofs)

    # }}}

    # {{{ test

    from pystopt.mesh import make_unique_mesh_vertex_connections

    to_vertex, from_vertex = make_unique_mesh_vertex_connections(discr)

    nodes = actx.thaw(discr.nodes())
    vertices = actx.to_numpy(flatten(to_vertex(nodes), actx)).reshape(nodes.size, -1)

    dv = np.abs(vertices - mesh.vertices)
    error = la.norm(dv) / la.norm(mesh.vertices)
    logger.info("error: %.5e", error)

    # NOTE: this works to machine precision when using PolynomialWarpAndBlend
    assert error < 2.0e-15

    c_nodes = make_obj_array([from_vertex(to_vertex(x)) for x in nodes])
    for _ in range(64):
        c_nodes = make_obj_array([from_vertex(to_vertex(x)) for x in c_nodes])

    # }}}

    if not visualize:
        return

    from pystopt.visualization import make_visualizer

    vis = make_visualizer(actx, discr, target_order)

    if ambient_dim == 2:
        filename = filenamer.with_suffix(f"vertex_connection_{ambient_dim}d")
        vis.write_file(
            filename,
            [("y", c_nodes)],
            write_mode="geometry",
            show_vertices=True,
            legend=False,
        )

    filename = filenamer.with_suffix(f"vertex_connection_{ambient_dim}d_error")
    vis.write_file(filename, [("dy", c_nodes - nodes)], legend=False)


# }}}


# {{{ test_discr_to_mesh_projection


@pytest.mark.parametrize("node_type", ["equi", "warp"])
@pytest.mark.parametrize("group_cls", [SimplexElementGroup, TensorProductElementGroup])
def test_discr_to_mesh_projection(actx_factory, node_type, group_cls, visualize):
    actx = get_cl_array_context(actx_factory)

    # {{{ geometry

    dim = 2
    target_order = 4
    mesh_order = target_order + 1
    group_factory = mpoly.InterpolatoryEdgeClusteredGroupFactory(order=target_order)

    if node_type == "equi":
        import modepy as mp

        if group_cls is SimplexElementGroup:
            equi_nodes = mp.equidistant_nodes(dim, mesh_order)
        else:
            equi_nodes = mp.equidistant_nodes(1, mesh_order)[0]
            equi_nodes = mp.tensor_product_nodes(dim, equi_nodes)

    def make_discr(nelements):
        from meshmode.mesh.generation import generate_regular_rect_mesh

        mesh = generate_regular_rect_mesh(
            a=(-1.0,) * dim,
            b=(1.0,) * dim,
            nelements_per_axis=(nelements,) * dim,
            group_cls=group_cls,
            order=mesh_order,
        )

        if node_type == "equi":
            # TODO: just pass unit_nodes to mesh generator?
            from meshmode.mesh.generation import make_group_from_vertices

            grp = make_group_from_vertices(
                mesh.vertices,
                mesh.groups[0].vertex_indices,
                order=mesh_order,
                group_cls=group_cls,
                unit_nodes=equi_nodes,
            )
            mesh = mesh.copy(groups=[grp])
        elif node_type == "warp":
            pass
        else:
            raise ValueError(f"unsupported node type: '{node_type}'")

        from meshmode.discretization import Discretization

        return Discretization(actx, mesh, group_factory)

    # }}}

    # {{{ project and check

    def func(x):
        return actx.np.sin(np.pi * x[0]) * actx.np.cos(np.pi * x[1] * (1 - x[0]))

    from pystopt.measure import EOCRecorder, stringify_eoc

    eoc_to = EOCRecorder(name="to")
    eoc_from = EOCRecorder(name="from")

    for nelements in [8, 16, 32, 64]:
        from pystopt.mesh import make_from_mesh_connection, make_to_mesh_connection

        discr = make_discr(nelements)
        to_mesh = make_to_mesh_connection(actx, discr)
        from_mesh = make_from_mesh_connection(actx, discr)

        # to_mesh
        nodes = actx.thaw(discr.nodes())

        f_discr = func(nodes)
        f_to_mesh = to_mesh(f_discr)

        # from_mesh
        nodes = make_obj_array([
            DOFArray(
                actx,
                tuple(actx.from_numpy(grp.nodes[iaxis]) for grp in discr.mesh.groups),
            )
            for iaxis in range(dim)
        ])

        f_mesh = func(nodes)
        f_from_mesh = from_mesh(f_mesh)

        # errors
        from pystopt.dof_array import dof_array_rnorm

        to_error = actx.to_numpy(dof_array_rnorm(f_to_mesh, f_mesh))
        from_error = actx.to_numpy(dof_array_rnorm(f_from_mesh, f_discr))
        logger.info("error from %.6e to %.6e", from_error, to_error)

        eoc_to.add_data_point(1 / nelements, to_error)
        eoc_from.add_data_point(1 / nelements, from_error)

    logger.info("error\n%s", stringify_eoc(eoc_to, eoc_from))

    # }}}

    if not visualize:
        return

    el_type = "simplex" if group_cls is SimplexElementGroup else "tensor"
    filename = filenamer.with_suffix(f"discr_from_mesh_{node_type}_{el_type}")

    from pystopt.visualization import make_visualizer

    vis = make_visualizer(actx, discr, vis_order=target_order, vis_type="vtkhdf")

    error = actx.np.log10(actx.np.abs(f_discr - f_from_mesh) + 1.0e-16)
    vis.write_file(
        filename,
        [("f", f_discr), ("g", f_from_mesh), ("error", error)],
        overwrite=True,
    )


# }}}


# {{{ test_generate_balls


@pytest.mark.parametrize("ambient_dim", [2, 3])
@pytest.mark.parametrize("is_random", [True, False])
def test_generate_balls(actx_factory, ambient_dim, is_random, visualize):
    actx = get_cl_array_context(actx_factory)

    resolution = 32
    target_order = 3

    if ambient_dim == 2:
        cls = etd.FourierCircleTestCase
    elif ambient_dim == 3:
        cls = etd.SPHSphereTestCase
    else:
        raise ValueError(f"unsupported dimension: {ambient_dim}")

    case = cls(
        target_order=target_order,
        group_factory_cls=mpoly.InterpolatoryEdgeClusteredGroupFactory,
        qbx_order=target_order,
    )
    logger.info("\n%s", case)

    kwargs = {"resolution": resolution, "radius": 1.0, "gap": 0.15}
    if is_random:
        from pystopt.mesh import generate_random_discr_array

        balls = generate_random_discr_array(
            actx, case.mesh_generator, 3**ambient_dim, **kwargs
        )
    else:
        from pystopt.mesh import generate_uniform_discr_array

        balls = generate_uniform_discr_array(
            actx, case.mesh_generator, shape=(3, 3, 3)[:ambient_dim], **kwargs
        )

    if not visualize:
        return

    from pystopt.visualization import make_visualizer

    vis = make_visualizer(actx, balls)

    method = "random" if is_random else "uniform"
    vis.write_file(
        filenamer.with_suffix(f"balls_{ambient_dim}d_{method}"),
        [],
        overwrite=True,
        aspect="equal",
    )


# }}}


# {{{ test_group_volumes


@pytest.mark.parametrize("ambient_dim", [2, 3])
def test_group_volumes(actx_factory, ambient_dim, visualize):
    actx = get_cl_array_context(actx_factory)

    # {{{ geometry

    target_order = 3

    if ambient_dim == 2:
        cls = etd.FourierCircleTestCase
    elif ambient_dim == 3:
        cls = etd.SPHSphereTestCase
    else:
        raise ValueError(f"unsupported dimension: {ambient_dim}")

    case = cls(
        target_order=target_order,
        group_factory_cls=mpoly.InterpolatoryQuadratureGroupFactory,
        qbx_order=target_order,
    )
    logger.info("\n%s", case)

    from pytential import GeometryCollection

    discr = case.get_discretization(actx, case.resolutions[-1])
    places = GeometryCollection(discr, auto_where=case.name)

    logger.info("nelements: %d", discr.mesh.nelements)
    logger.info("ndofs:    %d", discr.ndofs)

    # }}}

    # {{{

    rng = np.random.default_rng(42)
    vd = 10.0 + 5.0 * rng.random()
    if ambient_dim == 2:
        vr = np.pi
    else:
        vr = 4.0 / 3.0 * np.pi

    from pystopt.mesh.processing import rescale_discretization

    discr = rescale_discretization(actx, places, vd=vd)

    from pystopt import bind, sym

    vs = actx.to_numpy(bind(places, sym.surface_volume(discr.ambient_dim))(actx))
    logger.info("vs %.12e error %.12e", vs, abs(vs - vr) / vr)

    vs = actx.to_numpy(bind(discr, sym.surface_volume(discr.ambient_dim))(actx))
    logger.info("vd %.12e vs %.12e error %.12e", vd, vs, abs(vs - vd) / abs(vd))
    assert abs(vs - vd) / abs(vd) < 1.0e-14

    # }}}


# }}}


if __name__ == "__main__":
    import sys

    import pyopencl as cl  # noqa: F401

    if len(sys.argv) > 1:
        exec(sys.argv[1])
    else:
        pytest.main([__file__])
