# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

from dataclasses import dataclass, field

import numpy as np
import numpy.linalg as la
import pytest

from arraycontext import ArrayContext

from pystopt.tools import (
    get_cl_array_context,
    get_default_logger,
    pytest_generate_tests_for_array_contexts,
)

import extra_test_data as etd

logger = get_default_logger(__file__)
filenamer = etd.get_filename_generator(__file__)
pytest_generate_tests = pytest_generate_tests_for_array_contexts([
    "pystopt.pyopencl",
])


# {{{ test flattening and unflattening


@pytest.mark.parametrize(
    "cls",
    [
        etd.FourierCircleTestCase,
        etd.SPHSphereTestCase,
    ],
)
def test_flatten_unflatten(actx_factory, cls, visualize):
    actx = get_cl_array_context(actx_factory)

    # {{{ geometry

    target_order = 3
    case = cls(mesh_arguments={"target_order": target_order})
    discr = case.get_discretization(actx, case.resolutions[-1])

    # }}}

    # {{{ check

    from pytools.obj_array import make_obj_array

    from pystopt.dof_array import uniform_like

    other = discr.zeros(actx)

    def make_dof_array():
        return uniform_like(actx, other)

    def make_obj_dof_array():
        return make_obj_array([make_dof_array() for _ in range(discr.ambient_dim)])

    def make_complex_dof_array():
        return uniform_like(actx, other) + 1j * uniform_like(actx, other)

    def make_complex_obj_dof_array():
        return make_obj_array([
            make_complex_dof_array() for _ in range(discr.ambient_dim)
        ])

    from pystopt.optimize import flatten_to_numpy, unflatten_from_numpy_like

    for make_array_fn, dtype in [
        (make_dof_array, np.float64),
        (make_complex_dof_array, np.complex128),
        (make_obj_dof_array, np.float64),
        (make_complex_obj_dof_array, np.complex128),
    ]:
        ary = make_array_fn()
        ary_flat = flatten_to_numpy(actx, ary)
        ary_roundtrip = unflatten_from_numpy_like(actx, ary_flat, ary)

        assert isinstance(ary_flat, np.ndarray)
        assert ary_flat.dtype == dtype
        assert ary_flat.shape == (ary_flat.size,)

        from pystopt.dof_array import dof_array_rnorm

        assert dof_array_rnorm(ary, ary_roundtrip) < 1.0e-15

    # }}}


# }}}


# {{{ test_structured_vdot


@pytest.mark.parametrize("args", ["host", "device", "container"])
@pytest.mark.parametrize("dtype", [np.float64, np.complex128])
def test_structured_vdot(actx_factory, args, dtype):
    actx = get_cl_array_context(actx_factory)

    from pystopt.tools import randn

    rng = np.random.default_rng(42)

    if args == "host":
        x = randn((256, 7), dtype, rng=rng)
        y = randn((256, 7), dtype, rng=rng)
    elif args == "device":
        x = actx.from_numpy(randn((256, 7), dtype, rng=rng))
        y = actx.from_numpy(randn((256, 7), dtype, rng=rng))
    elif args == "container":
        from meshmode.dof_array import DOFArray

        x = DOFArray(
            actx,
            tuple([
                actx.from_numpy(randn(shape, dtype, rng=rng))
                for shape in [(128, 7), (128, 11)]
            ]),
        )
        y = DOFArray(
            actx,
            tuple([
                actx.from_numpy(randn(shape, dtype, rng=rng))
                for shape in [(128, 7), (128, 11)]
            ]),
        )
    else:
        raise ValueError(f"unsupported 'args' value: {args}")

    from pystopt.optimize.cg_utils import get_vdot_for_container

    vdot = get_vdot_for_container(x, actx=actx)
    r = vdot(x, y)
    assert np.array(r).dtype.kind == "f"


# }}}


# {{{ test_steepest_descent


def _optim_convex(n, dd=4.0, rng=None):
    if rng is None:
        rng = np.random.default_rng(42)

    b = rng.random(size=(n,))
    A = rng.random(size=(n, n))

    # NOTE: a larger dd make it more "diagonally dominant" -> faster convergence
    A = (A + A.T) / 2 + dd * np.eye(n)

    def fun(x):
        return x.dot(A @ x) / 2 - x.dot(b)

    def jac(x):
        return A @ x - b

    return fun, jac, la.solve(A, b)


def _optim_rosenbrock(n, a=1.0, b=2.0):
    # https://en.wikipedia.org/wiki/Rosenbrock_function
    assert n % 2 == 0

    def fun(x):
        return sum(
            b * (x[2 * i + 1] - x[2 * i] ** 2) ** 2 + (x[2 * i] - a) ** 2
            for i in range(n // 2)
        )

    def jac(x):
        return np.hstack([
            [
                2.0 * (x[2 * i] - a)
                - 4.0 * b * (x[2 * i + 1] - x[2 * i] ** 2) * x[2 * i],
                2.0 * b * (x[2 * i + 1] - x[2 * i] ** 2),
            ]
            for i in range(n // 2)
        ])

    return fun, jac, None


@dataclass(frozen=True)
class HistoryCallback:
    f: list[float] = field(default_factory=list)
    gnorm2: list[float] = field(default_factory=list)
    alpha: list[float] = field(default_factory=list)

    x: list[np.ndarray] = field(default_factory=list)
    g: list[np.ndarray] = field(default_factory=list)

    def __call__(self, x, info, **kwargs):
        self.f.append(info.f)
        self.gnorm2.append(la.norm(info.g))
        self.alpha.append(info.alpha)

        self.x.append(info.x)
        self.g.append(info.g)

        return 1


@pytest.mark.parametrize(
    ("problem", "size"),
    [
        (_optim_convex, 16),
        (_optim_rosenbrock, 2),
    ],
)
@pytest.mark.parametrize(
    ("line_search_name", "line_search_kwargs"),
    [
        # NOTE: small step size is required for Rosenbrock
        ("fixed", {"alpha": 0.05}),
        ("backtracking", {"factor": 0.75}),
        ("stabilized-barzilai-borwein", {}),
        # NOTE: this method seems a bit more sensitive for nonlinear functions
        ("modified-barzilai-borwein", {"factor": 1.0}),
    ],
)
@pytest.mark.parametrize(
    ("descent_name", "descent_kwargs"),
    [
        ("steepest", {}),
        # ("hestenes-stiefel", {}),
        # ("fletcher-reeves", {}),
        ("polak-ribiere", {}),
        # ("fletcher", {}),
        # ("liu-storey", {}),
        # ("dai-yuan", {}),
        # ("hager-zhang", {}),
    ],
)
def test_steepest_descent(
    problem,
    size,
    line_search_name,
    line_search_kwargs,
    descent_name,
    descent_kwargs,
    visualize,
):
    force_contour_plot = False

    if descent_name == "hestenes-stiefel" and line_search_name != "backtracking":
        pytest.skip("'hestenes-stiefel' needs a backtracking line search")

    if line_search_name == "fixed":
        if descent_name == "dai-yuan":
            line_search_kwargs["alpha"] = 0.01

        if descent_name == "hager-zhang":
            line_search_kwargs["alpha"] = 0.025

    if line_search_name == "stabilized-barzilai-borwein":
        if descent_name in {"steepest", "dai-yuan"}:
            line_search_kwargs["delta_factor"] = 0.25

        if descent_name == "polak-ribiere":
            line_search_kwargs["delta_factor"] = 0.05

    # {{{ get problem parameters

    rng = np.random.default_rng(42)

    fun, jac, x = problem(size)
    x0 = rng.random(size)
    logger.info("x0: %s", x0)

    rtol = 1.0e-4
    if x is None:
        import scipy.optimize as so

        r = so.minimize(fun, x0, jac=jac, method="L-BFGS-B", tol=1.0e-2 * rtol)
        x = r.x

        logger.info("scipy: x %s f(x) = %.5e", x, fun(x))

    # }}}

    # {{{ optimize

    from pystopt.optimize.riemannian import CartesianEucledeanSpace

    manifold = CartesianEucledeanSpace(vdot=np.vdot)

    from pystopt.optimize import get_line_search_from_name

    ls = get_line_search_from_name(line_search_name, fun=fun, **line_search_kwargs)

    from pystopt.optimize import get_descent_direction_from_name

    ds = get_descent_direction_from_name(descent_name, **descent_kwargs)

    from pystopt.optimize.steepest import minimize

    callback = HistoryCallback()
    result = minimize(
        fun,
        jac,
        x0,
        options={
            "maxit": 1024,
            "rtol": rtol,
            "ftol": rtol,
            "linesearch": ls,
            "descent": ds,
            "manifold": manifold,
        },
        callback=callback,
    )
    logger.info("status:\n%s", result.pretty())

    error = la.norm(result.x - x) / la.norm(x)
    logger.info("error: %s: %.5e", problem.__name__, error)
    assert (
        callback.f[-1] < 1.75 * callback.f[0] * rtol
        or callback.gnorm2[-1] < 1.75 * callback.gnorm2[0] * rtol
    )

    # }}}

    if False:
        np.savez(
            f"optim_steepest_{line_search_name}.npz".replace("-", "_"),
            f=np.array(callback.f),
            alpha=np.array(callback.alpha),
            x=np.array(callback.x),
            g=np.array(callback.g),
        )

    if not visualize:
        return

    f = np.abs(np.array(callback.f))
    f = f / f[0]

    g = np.array(callback.gnorm2)
    g = g / g[0]

    from pystopt.visualization.matplotlib import subplots

    filename = filenamer.with_suffix(f"{descent_name}_{line_search_name}_cost")
    with subplots(filename, overwrite=True) as fig:
        ax = fig.gca()

        ax.semilogy(f)
        ax.set_xlabel("$Iteration$")
        ax.set_ylabel("$f$")

    filename = filenamer.with_suffix(f"{descent_name}_{line_search_name}_gradient")
    with subplots(filename, overwrite=True) as fig:
        ax = fig.gca()

        ax.semilogy(g)
        ax.axhline(rtol, ls="--", color="k")

        ax.set_ylim([1.0e-2 * rtol, 10.0])
        ax.set_xlabel("$Iteration$")
        ax.set_ylabel(r"$\|\nabla f\|_2$")

    filename = filenamer.with_suffix(f"{descent_name}_{line_search_name}_alpha")
    with subplots(filename, overwrite=True) as fig:
        ax = fig.gca()

        ax.semilogy(np.array(callback.alpha))
        ax.set_xlabel("$Iteration$")
        ax.set_ylabel(r"$\alpha$")

    if force_contour_plot and size == 2:
        filename = filenamer.with_suffix(f"{descent_name}_{line_search_name}_contour")

        x, y = np.array(callback.x).T

        xmin, xmax = np.min(x), np.max(x)
        xabs = max(abs(xmin), abs(xmax))
        ymin, ymax = np.min(y), np.max(y)
        yabs = max(abs(ymin), abs(ymax))

        xc = np.linspace(xmin - 0.25 * xabs, xmax + 0.25 * xabs, 256)
        yc = np.linspace(ymin - 0.25 * yabs, ymax + 0.25 * yabs, 256)
        xc, yc = np.meshgrid(xc, yc)
        zc = fun([xc, yc])

        with subplots(filename, overwrite=True) as fig:
            ax = fig.gca()

            ax.grid(visible=False)
            im = ax.contourf(xc, yc, zc, levels=64, zorder=0)
            ax.contour(xc, yc, zc, colors="k", linewidths=(1,), levels=64, zorder=1)
            fig.colorbar(im, ax=ax)

            ax.plot(x, y, "wo-")
            ax.plot(x[0], y[0], "bo")
            ax.plot(x[-1], y[-1], "go")


# }}}


# {{{ test_pycgdescent


@dataclass
class DOFArrayHistoryCallback:
    actx: ArrayContext
    norm: object = None

    f: list[float] = field(default_factory=list)
    g: list[float] = field(default_factory=list)

    def __call__(self, x, info, **kwargs):
        from pystopt.dof_array import dof_array_norm

        self.f.append(info.f)
        self.g.append(self.actx.to_numpy(dof_array_norm(info.g, ord=2)))

        return 1


def test_pycgdescent_dof_array(actx_factory):
    actx = get_cl_array_context(actx_factory)

    # {{{ geometry

    rtol = 1.0e-4

    case = etd.FourierCircleTestCase()
    discr = case.get_discretization(actx, case.resolutions[0])
    x0 = actx.thaw(discr.nodes())

    logger.info("ndofs: %d", discr.ndofs)

    # }}}

    # {{{ functions

    from pystopt.dof_array import uniform_like

    mat = uniform_like(actx, x0[0])

    def fun(x):
        return actx.to_numpy(actx.np.sum(x @ (mat * x)) / 2)

    def jac(x):
        return x

    def funjac(x):
        return fun(x), x

    # }}}

    # {{{ optimize

    import pycgdescent as cg

    options = cg.OptimizeOptions()
    history = DOFArrayHistoryCallback(actx)

    from pystopt.optimize import minimize_cgdescent

    result = minimize_cgdescent(
        fun=fun,
        x0=x0,
        jac=jac,
        funjac=funjac,
        rtol=rtol,
        options=options,
        callback=history,
    )

    logger.info("result:\n%s", result.pretty())
    logger.info("history[f]: %s", history.f)
    logger.info("history[g]: %s", history.g)

    # }}}


# }}}


if __name__ == "__main__":
    import sys

    import pyopencl as cl  # noqa: F401

    if len(sys.argv) > 1:
        exec(sys.argv[1])
    else:
        pytest.main([__file__])
