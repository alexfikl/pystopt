# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

import pytest

from pystopt import sym
from pystopt.stokes import sti
from pystopt.tools import get_default_logger, pytest_generate_tests_for_array_contexts

logger = get_default_logger(__file__)
pytest_generate_tests = pytest_generate_tests_for_array_contexts([
    "pystopt.pyopencl",
])


@pytest.mark.parametrize("ambient_dim", [2, 3])
def test_stokes_kernel_interface(ambient_dim):
    from functools import partial

    methods = [
        sti.pressure,
        sti.velocity,
        sti.traction,
        partial(sti.tangent_traction, tangent_idx=0),
        sti.normal_velocity_gradient,
        partial(sti.tangent_velocity_gradient, tangent_idx=0),
        sti.normal_velocity_laplacian,
        sti.normal_velocity_hessian,
    ]

    from pystopt.stokes import StokesletJumpRelations, make_stokeslet

    stokeslet = make_stokeslet(ambient_dim)
    jumps = StokesletJumpRelations(ambient_dim)
    density = sym.make_sym_vector("q", ambient_dim)

    for m in methods:
        m(stokeslet, density, qbx_forced_limit="avg")
        m(jumps, density, side=+1)


if __name__ == "__main__":
    import sys

    if len(sys.argv) > 1:
        exec(sys.argv[1])
    else:
        pytest.main([__file__])
