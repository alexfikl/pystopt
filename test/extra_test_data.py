# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

import pathlib
from dataclasses import dataclass, field
from typing import Any

import meshmode.discretization.poly_element as mpoly

from pystopt.mesh.generation import MeshGenerator


def get_filename_generator(caller):
    caller = pathlib.Path(caller)

    from pystopt.paths import get_filename, make_dirname

    dirname = make_dirname("_output", cwd=caller.parent)
    return get_filename(caller.stem, cwd=dirname)


def _set_up_formatting():
    import shutil

    import numpy as np

    linewidth = shutil.get_terminal_size((85, 20))[0] - 7
    np.set_printoptions(linewidth=linewidth, edgeitems=7)

    from pystopt.visualization.matplotlib import setup_matplotlib

    setup_matplotlib()


_set_up_formatting()


# {{{ base test case


@dataclass(frozen=True)
class TestCase:
    name: str = "test_case"

    # mesh
    mesh_generator_cls: type[MeshGenerator] | None = None
    mesh_arguments: dict[str, Any] = field(default_factory=dict)
    default_mesh_arguments: dict[str, Any] = field(default_factory=dict)

    # NOTE: these overwrite `default_mesh_arguments`
    target_order: int = 3
    group_factory_cls: type[mpoly.ElementGroupFactory] = (
        mpoly.InterpolatoryQuadratureGroupFactory
    )

    # qbx
    qbx_order: int = None
    source_ovsmp: int = 3
    fmm_order: int | bool = False
    disable_refinement: bool = True

    # convergence
    resolutions: list[int] = field(default_factory=list)

    def __post_init__(self):
        if self.qbx_order is None:
            object.__setattr__(self, "qbx_order", self.target_order)

    def __getattr__(self, name):
        if hasattr(self.mesh_generator, name):
            return getattr(self.mesh_generator, name)

        raise AttributeError

    def get_mesh_arguments(self):
        mesh_arguments = self.default_mesh_arguments.copy()
        mesh_arguments["target_order"] = self.target_order
        mesh_arguments["group_factory_cls"] = self.group_factory_cls

        mesh_arguments.update(self.mesh_arguments)
        return mesh_arguments

    @property
    def mesh_generator(self):
        mesh_generator_cls = self.mesh_generator_cls
        if mesh_generator_cls is None:
            from pystopt.mesh.generation import get_mesh_generator_class_from_name

            mesh_generator_cls = get_mesh_generator_class_from_name(self.name)

        if mesh_generator_cls is None:
            raise AttributeError

        return mesh_generator_cls(**self.get_mesh_arguments())

    @property
    def ambient_dim(self):
        return self.mesh_generator.ambient_dim

    def get_discretization(self, actx, resolution):
        from pystopt.mesh import generate_discretization

        return generate_discretization(self.mesh_generator, actx, resolution)

    def get_layer_potential_for_discr(self, pre_density_discr, **kwargs):
        fmm_backend = "sumpy" if self.fmm_order else None

        from pystopt.qbx import QBXLayerPotentialSource

        qbx = QBXLayerPotentialSource(
            pre_density_discr,
            fine_order=self.source_ovsmp * self.target_order,
            qbx_order=self.qbx_order,
            fmm_order=self.fmm_order,
            fmm_backend=fmm_backend,
            _disable_refinement=self.disable_refinement,
            **kwargs,
        )

        return qbx

    def get_layer_potential(self, actx, resolution, **kwargs):
        pre_density_discr = self.get_discretization(actx, resolution)
        return self.get_layer_potential_for_discr(pre_density_discr, **kwargs)

    def __str__(self):
        from pystopt.tools import dc_stringify

        return "{}\n{}".format(dc_stringify(self), dc_stringify(self.mesh_generator))


# }}}


# {{{ geometries


@dataclass(frozen=True)
class FourierCircleTestCase(TestCase):
    name: str = "fourier_circle"
    resolutions: list[int] = field(default_factory=lambda: [16, 32, 64, 128])


@dataclass(frozen=True)
class FourierEllipseTestCase(TestCase):
    name: str = "fourier_ellipse"
    resolutions: list[int] = field(default_factory=lambda: [16, 32, 64, 128])


@dataclass(frozen=True)
class FourierEllipseV2TestCase(TestCase):
    name: str = "fourier_ellipse_v2"
    resolutions: list[int] = field(default_factory=lambda: [16, 32, 64, 128])


@dataclass(frozen=True)
class FourierStarfishTestCase(TestCase):
    name: str = "fourier_starfish"
    resolutions: list[int] = field(default_factory=lambda: [16, 32, 64, 128])


@dataclass(frozen=True)
class FourierUFOTestCase(TestCase):
    name: str = "fourier_ufo"
    resolutions: list[int] = field(default_factory=lambda: [16, 32, 64, 128])


@dataclass(frozen=True)
class SPHSphereTestCase(TestCase):
    name: str = "spharm_sphere"
    resolutions: list[int] = field(default_factory=lambda: [8, 12, 16, 24])


@dataclass(frozen=True)
class SPHSpheroidTestCase(TestCase):
    name: str = "spharm_spheroid"
    resolutions: list[int] = field(default_factory=lambda: [8, 12, 16, 24])


@dataclass(frozen=True)
class SPHUFOTestCase(TestCase):
    name: str = "spharm_ufo"
    resolutions: list[int] = field(default_factory=lambda: [8, 12, 16, 24])


@dataclass(frozen=True)
class SPHUrchinTestCase(TestCase):
    name: str = "spharm_urchin"
    resolutions: list[int] = field(default_factory=lambda: [8, 12, 16, 24])


@dataclass(frozen=True)
class SPHHarmonicTestCase(TestCase):
    name: str = "spharm_urchin"
    resolutions: list[int] = field(default_factory=lambda: [8, 12, 16, 24])


# }}}
