# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

import numpy as np
import numpy.linalg as la
import pytest

import meshmode.discretization.poly_element as mpoly
from pytools.obj_array import make_obj_array

from pystopt.dof_array import dof_array_rnorm
from pystopt.tools import (
    get_cl_array_context,
    get_default_logger,
    pytest_generate_tests_for_array_contexts,
)

import extra_test_data as etd

logger = get_default_logger(__file__)
filenamer = etd.get_filename_generator(__file__)
pytest_generate_tests = pytest_generate_tests_for_array_contexts([
    "pystopt.pyopencl",
])


# {{{ test_merge_disjoint_discretizations


@pytest.mark.parametrize("ambient_dim", [2, 3])
def test_merge_disjoint_discretizations(actx_factory, ambient_dim, visualize):
    actx = get_cl_array_context(actx_factory)

    target_order = 1
    mesh_arguments = {
        "target_order": target_order,
        "group_factory_cls": mpoly.InterpolatoryEdgeClusteredGroupFactory,
        "radius": 0.75,
    }

    if ambient_dim == 2:
        case = etd.FourierCircleTestCase(mesh_arguments=mesh_arguments)
    elif ambient_dim == 3:
        case = etd.SPHSphereTestCase(mesh_arguments=mesh_arguments)
    else:
        raise ValueError("unsupported dimension")

    logger.info("\n%s", case)

    # {{{

    orig_discr = case.get_discretization(actx, case.resolutions[2])

    from itertools import product

    from meshmode.mesh.processing import affine_map

    discretizations = []
    for i, j in product(range(3), repeat=2):
        mesh = affine_map(
            orig_discr.mesh, b=np.array([2 * i, 2 * j, 0.0])[:ambient_dim]
        )
        discr = orig_discr.copy(mesh=mesh)

        nodes = actx.thaw(discr.nodes())
        x_roundtrip = discr.from_spectral_conn(discr.to_spectral_conn(nodes[0]))
        error = actx.to_numpy(dof_array_rnorm(nodes[0], x_roundtrip))
        logger.info("roundtrip error[%d, %d]: %.5e", i, j, error)
        assert error < 1.0e-14

        discretizations.append(discr)

    from pystopt.mesh import merge_disjoint_spectral_discretizations

    discr = merge_disjoint_spectral_discretizations(actx, discretizations)

    nodes = actx.thaw(discr.nodes())
    x_roundtrip = discr.from_spectral_conn(discr.to_spectral_conn(nodes[0]))
    error = actx.to_numpy(dof_array_rnorm(nodes[0], x_roundtrip))
    logger.info("roundtrip error: %.5e", error)
    assert error < 1.0e-14

    # }}}

    if not visualize:
        return

    from pystopt.visualization import make_visualizer

    vis = make_visualizer(actx, discr, vis_order=target_order)
    vis.write_file(filenamer.with_suffix("merge_disjoint"), [], overwrite=True)


# }}}


# {{{ test_same_mesh_and_discr_nodes


@pytest.mark.parametrize(
    "case_cls",
    [
        etd.FourierCircleTestCase,
        etd.SPHSphereTestCase,
    ],
)
@pytest.mark.parametrize(
    "group_factory_cls",
    [
        mpoly.InterpolatoryEquidistantGroupFactory,
        mpoly.InterpolatoryEdgeClusteredGroupFactory,
        mpoly.InterpolatoryQuadratureGroupFactory,
    ],
)
@pytest.mark.parametrize("target_order", [3, 5])
def test_same_mesh_and_discr_nodes(
    actx_factory, case_cls, group_factory_cls, target_order, visualize
):
    actx = get_cl_array_context(actx_factory)

    case = case_cls(
        mesh_arguments={
            "target_order": target_order,
            "group_factory_cls": group_factory_cls,
            "use_spectral_derivative": True,
            "use_default_mesh_unit_nodes": True,
        },
    )
    logger.info("\n%s", case)

    # {{{

    from pystopt.measure import EOCRecorder, stringify_eoc

    eoc = EOCRecorder(name="interp", expected_order=target_order + 1)

    for r in case.resolutions:
        discr = case.get_discretization(actx, r)

        if case.mesh_arguments["use_default_mesh_unit_nodes"]:
            for grp in discr.groups:
                grp_unit_nodes = grp.unit_nodes.reshape(-1)
                meg_unit_nodes = grp.mesh_el_group.unit_nodes.reshape(-1)

                error = la.norm(grp_unit_nodes - meg_unit_nodes) / la.norm(
                    meg_unit_nodes
                )
                assert error < 1.0e-14

        f = actx.thaw(discr.nodes()[0])
        f_roundtrip = discr.from_spectral_conn(discr.to_spectral_conn(f))

        error = actx.to_numpy(dof_array_rnorm(f, f_roundtrip, ord=2))

        eoc.add_data_point(1 / r, error)
        logger.info("h %3d error %.5e", r, error)

    logger.info("error\n%s", stringify_eoc(eoc))
    assert eoc.satisfied()

    max_error = eoc.max_error()
    assert max_error < 1.0e-12

    # }}}

    if not visualize:
        return

    from pystopt.visualization import make_visualizer

    vis = make_visualizer(actx, discr)
    vis.write_file(
        filenamer.with_suffix(f"same_nodes_{case.name}"),
        [("f", f - f_roundtrip)],
        overwrite=True,
    )


# }}}


# {{{ test_spectral_derivative


class EllipseGeometry:
    def __init__(self, mesh):
        self.radius = mesh.radius
        self.aspect_ratio = mesh.aspect_ratio

    def normal(self, discr, nodes):
        actx = nodes[0].array_context

        a = self.radius
        b = self.radius / self.aspect_ratio

        sin_t = nodes[1] / b
        cos_t = nodes[0] / a
        jac = actx.np.sqrt((a * sin_t) ** 2 + (b * cos_t) ** 2)

        return make_obj_array([
            cos_t * b / jac,
            sin_t * a / jac,
        ])

    def curvature(self, discr, nodes):
        actx = nodes[0].array_context

        a = self.radius
        b = self.radius / self.aspect_ratio

        sin_t = nodes[1] / b
        cos_t = nodes[0] / a
        jac = actx.np.sqrt((a * sin_t) ** 2 + (b * cos_t) ** 2)

        return a * b / jac**3

    def area(self, discr, nodes, *, capped=False):
        # https://docs.scipy.org/doc/scipy/reference/generated/scipy.special.ellipe.html
        eccentricity = 1.0 - (1 / self.aspect_ratio) ** 2

        from scipy.special import ellipe  # pylint: disable=no-name-in-module

        ellip_e = ellipe(eccentricity)

        return 4.0 * self.radius * ellip_e


class SpheroidGeometry:
    def __init__(self, mesh):
        self.radius = mesh.radius
        self.aspect_ratio = mesh.aspect_ratio

    def normal(self, discr, nodes):
        actx = nodes[0].array_context

        a = self.radius
        b = self.radius * self.aspect_ratio

        cos_theta = nodes[2] ** 2 / b**2 - (nodes[0] ** 2 + nodes[1] ** 2) / a**2
        sqrt_cos = actx.np.sqrt(a**2 + b**2 + (a**2 - b**2) * cos_theta)

        # unit normal vector
        return make_obj_array([
            (nodes[0] / a) * np.sqrt(2) * b / sqrt_cos,
            (nodes[1] / a) * np.sqrt(2) * b / sqrt_cos,
            (nodes[2] / b) * np.sqrt(2) * a / sqrt_cos,
        ])

    def curvature(self, discr, nodes):
        actx = nodes[0].array_context

        a = self.radius
        b = self.radius * self.aspect_ratio

        cos_theta = nodes[2] ** 2 / b**2 - (nodes[0] ** 2 + nodes[1] ** 2) / a**2
        sqrt_cos = actx.np.sqrt(a**2 + b**2 + (a**2 - b**2) * cos_theta)

        return b / (np.sqrt(2) * a) * (2 * a**2 + sqrt_cos**2) / sqrt_cos**3

    def area(self, discr, nodes, *, capped=True):
        a = self.radius
        c = self.radius * self.aspect_ratio

        if abs(a - c) < 1.0e-14:
            area = 4.0 * np.pi * a**2
        elif a < c:
            e = np.sqrt(1.0 - (a / c) ** 2)
            area = 2.0 * np.pi * a**2 * (1.0 + (c / a) / e * np.arcsin(e))
        else:
            e = np.sqrt(1.0 - (c / a) ** 2)
            area = 2.0 * np.pi * a**2 * (1 + (c / a) ** 2 / e * np.arctanh(e))

        if capped:
            # NOTE: our spherical harmonic mesh does not actually include a
            # spherical cap around the pole, so this is meant to subtract that
            # to get the exact area

            assert len(discr.specgroups) == 1
            theta = discr.specgroups[0].theta

            d = abs(theta[1] - theta[0]) / 2.0

            if abs(a - c) < 1.0e-14:
                cap = 2.0 * a**2 * np.pi * (1.0 - np.cos(d))
            else:
                # NOTE: could not convince mathematica to integrate this
                from scipy.integrate import quad

                cap, _ = quad(
                    lambda theta: 2.0
                    * np.pi
                    * np.sqrt(
                        (a**2 + c**2 + (a**2 - c**2) * np.cos(2.0 * theta))
                        * a**2
                        * np.sin(theta) ** 2
                        / 2.0
                    ),
                    0.0,
                    d,
                    epsabs=1.0e-14,
                )

            area = area - 2.0 * cap

        return area


@pytest.mark.parametrize("ambient_dim", [2, 3])
@pytest.mark.parametrize("target_order", [3, 4])
def test_spectral_derivative(actx_factory, ambient_dim, target_order, visualize):
    actx = get_cl_array_context(actx_factory)

    # NOTE: these pass for aspect_ratio != 1 with different tolerances
    radius = 1.0
    aspect_ratio = 1.0
    mesh_arguments = {
        "group_factory_cls": mpoly.InterpolatoryEquidistantGroupFactory,
        "mesh_unit_nodes": False if ambient_dim == 3 else None,
        "radius": radius,
        "aspect_ratio": aspect_ratio,
    }

    if ambient_dim == 2:
        case = etd.FourierEllipseTestCase(
            target_order=target_order,
            resolutions=[32, 40, 56, 64, 72],
            mesh_arguments=mesh_arguments,
        )
        geometry = EllipseGeometry(case.mesh_generator)
    elif ambient_dim == 3:
        case = etd.SPHSpheroidTestCase(
            target_order=target_order,
            resolutions=[(i, i) for i in range(16, 56, 8)],
            # resolutions=[(24, 16)],
            mesh_arguments=mesh_arguments,
        )
        geometry = SpheroidGeometry(case.mesh_generator)
    else:
        raise ValueError("unsupported dimension")

    logger.info("\n%s", case)

    # {{{ convergence

    from pystopt.measure import EOCRecorder, stringify_eoc, verify_eoc

    eoc_apoly = EOCRecorder(name="area", expected_order=target_order + 1)
    eoc_aspecv0 = EOCRecorder(name="area", expected_order=target_order + 1)
    eoc_aspecv1 = EOCRecorder(name="areac", expected_order=target_order + 1)

    eoc_npoly = EOCRecorder(name="normal", expected_order=target_order)
    eoc_nspec = EOCRecorder(name="normal", expected_order=target_order)

    eoc_kpoly = EOCRecorder(name="kappa", expected_order=target_order - 1)
    eoc_kspec = EOCRecorder(name="kappa", expected_order=target_order)

    from pytools import ProcessTimer

    for nelements in case.resolutions:
        # {{{ geometry

        with ProcessTimer() as p:
            discr = case.get_discretization(actx, nelements)

        logger.info("ndofs:     %d", discr.ndofs)
        logger.info("nelements: %d", discr.mesh.nelements)
        logger.info("time:      %s", p)

        assert len(discr.specgroups) == 1
        nspec = discr.specgroups[0].nspec
        nspec = 1

        # }}}

        # {{{ analytical

        nodes = actx.thaw(discr.nodes())
        normal = geometry.normal(discr, nodes)
        kappa = geometry.curvature(discr, nodes)
        areav0 = geometry.area(discr, nodes, capped=True)
        areav1 = geometry.area(discr, nodes, capped=False)

        # }}}

        # {{{ check spectral derivatives

        from pytential import GeometryCollection

        discr.use_spectral_derivative = True
        places = GeometryCollection(discr)

        from pystopt import bind, sym

        h_max = actx.to_numpy(bind(places, sym.h_max_from_volume(ambient_dim))(actx))

        normal_spec = bind(places, sym.normal(ambient_dim).as_vector())(actx)
        kappa_spec = bind(places, sym.mean_curvature(ambient_dim))(actx)
        waa_spec = bind(places, sym.area_element(discr.ambient_dim))(actx)
        area_specv0 = bind(
            places, sym.integral(discr.ambient_dim, discr.dim, sym.Ones())
        )(actx)
        area_specv1 = bind(
            places, sym.sintegral(sym.Ones(), discr.ambient_dim, discr.dim) / nspec
        )(actx)

        error_n = actx.to_numpy(dof_array_rnorm(normal_spec, normal))
        error_k = actx.to_numpy(dof_array_rnorm(kappa, kappa_spec))
        error_av0 = actx.to_numpy(abs(areav0 - area_specv0) / abs(areav0))
        error_av1 = actx.to_numpy(abs(areav1 - area_specv1) / abs(areav1))

        eoc_aspecv0.add_data_point(h_max, error_av0)
        eoc_aspecv1.add_data_point(h_max, error_av1)
        eoc_nspec.add_data_point(h_max, error_n)
        eoc_kspec.add_data_point(h_max, error_k)
        logger.info(
            "error[spec]: nres %s normal %.5e kappa %.5e area %.5e / %.5e",
            nelements,
            error_n,
            error_k,
            error_av0,
            error_av1,
        )

        # }}}

        # {{{ check polynomial derivatives

        # NOTE: create another collection to clear caches
        discr.use_spectral_derivative = False
        places = GeometryCollection(discr)

        normal_poly = bind(places, sym.normal(ambient_dim).as_vector())(actx)
        kappa_poly = bind(places, sym.mean_curvature(ambient_dim))(actx)
        waa_poly = bind(places, sym.area_element(discr.ambient_dim))(actx)
        area_poly = bind(
            places, sym.integral(discr.ambient_dim, discr.dim, sym.Ones())
        )(actx)

        error_n = actx.to_numpy(dof_array_rnorm(normal_poly, normal))
        error_k = actx.to_numpy(dof_array_rnorm(kappa, kappa_poly))
        error_a = actx.to_numpy(abs(areav0 - area_poly) / abs(areav0))

        eoc_apoly.add_data_point(h_max, error_a)
        eoc_npoly.add_data_point(h_max, error_n)
        eoc_kpoly.add_data_point(h_max, error_k)
        logger.info(
            "error[poly]: nres %s normal %.12e kappa %.12e area %.12e",
            nelements,
            error_n,
            error_k,
            error_a,
        )

        logger.info(
            "area: exact %2.12e poly %2.12e spec %2.12e",
            areav0,
            actx.to_numpy(area_poly),
            actx.to_numpy(area_specv0),
        )
        logger.info(
            "area: exact %2.12e spec %2.12e", areav1, actx.to_numpy(area_specv1)
        )

        # }}}

    logger.info("error[poly]:\n%s", stringify_eoc(eoc_apoly, eoc_npoly, eoc_kpoly))
    logger.info(
        "error[spec]:\n%s",
        stringify_eoc(eoc_aspecv0, eoc_aspecv1, eoc_nspec, eoc_kspec),
    )

    if ambient_dim == 2:
        expected_order = target_order
    else:
        expected_order = min(grp.order for grp in discr.groups) - 0.2

    verify_eoc(eoc_apoly, expected_order + 1.0)
    verify_eoc(eoc_npoly, expected_order + 0.0, atol=1.0e-13)
    verify_eoc(eoc_kpoly, expected_order - 1.0, atol=1.0e-11)

    verify_eoc(eoc_aspecv0, expected_order + 1.0, atol=4.0e-11)
    verify_eoc(eoc_aspecv1, expected_order + 1.0, atol=4.0e-11)
    verify_eoc(eoc_nspec, expected_order, atol=2.0e-12)
    verify_eoc(eoc_kspec, expected_order, atol=7.0e-11)

    # }}}

    if not visualize:
        return

    filename = filenamer.with_suffix(f"derivative_{target_order:02d}")
    error_kpoly = actx.np.log10(actx.np.fabs(kappa - kappa_poly) + 1.0e-16)
    error_kspec = actx.np.log10(actx.np.fabs(kappa - kappa_spec) + 1.0e-16)
    error_waa = actx.np.log10(actx.np.fabs(waa_spec - waa_poly) + 1.0e-16)

    from pystopt.visualization import make_visualizer

    vis = make_visualizer(actx, discr, vis_order=target_order)

    vis.write_file(
        filename,
        [
            ("waa_poly", waa_poly),
            ("waa_spec", waa_spec),
            ("waa_error", error_waa),
            ("kappa", kappa),
            ("kappa_poly", kappa_poly),
            ("normal_poly", normal_poly),
            ("kappa_spec", kappa_spec),
            ("error_kappa_poly", error_kpoly),
            ("error_kappa_spec", error_kspec),
        ],
        overwrite=True,
    )


# }}}


# {{{ test_spectral_integral


@pytest.mark.parametrize("ambient_dim", [2, 3])
def test_spectral_integral(actx_factory, ambient_dim, visualize):
    actx = get_cl_array_context(actx_factory)

    # {{{ geometry

    kwargs = {}
    if ambient_dim == 2:
        case = etd.FourierStarfishTestCase(**kwargs)
    else:
        case = etd.SPHUFOTestCase(**kwargs)

    from pytential import GeometryCollection

    qbx = case.get_layer_potential(actx, case.resolutions[-1] + 16)
    places = GeometryCollection(qbx, auto_where=case.name)
    dofdesc = places.auto_source

    density_discr = places.get_discretization(dofdesc.geometry)
    logger.info("nelements:     %d", density_discr.mesh.nelements)
    logger.info("ndofs:         %d", density_discr.ndofs)
    logger.info("nspec:         %d", density_discr.nspec)

    # }}}

    # {{{

    from pystopt import bind, sym

    nodes = sym.nodes(ambient_dim).as_vector()
    normal = sym.normal(ambient_dim).as_vector()
    kappa = sym.summed_curvature(ambient_dim)

    sym_expr_1 = sym.Ones()
    sym_expr_2 = (nodes @ normal) ** 2
    sym_expr_3 = np.pi * sum(nodes) + (1 + nodes[0] / (1 + kappa**2)) ** 2

    for sym_expr in (sym_expr_1, sym_expr_2, sym_expr_3):
        operand = actx.to_numpy(bind(places, sym.NodeMax(sym_expr))(actx))
        logger.info("max %.12e", operand)

        expr_quad = actx.to_numpy(
            bind(places, sym.integral(ambient_dim, ambient_dim - 1, sym_expr))(actx)
        )
        expr_spec = actx.to_numpy(
            bind(places, sym.sintegral(sym_expr, ambient_dim, ambient_dim - 1))(actx)
        )

        error = abs(expr_quad - expr_spec) / abs(expr_quad)
        logger.info("quad %.12e spec %.12e error: %.12e", expr_quad, expr_spec, error)

        assert error < 5.0e-4

    # }}}


# }}}


# {{{ test_groupwise_reductions


@pytest.mark.parametrize(
    "case_cls",
    [
        etd.FourierCircleTestCase,
        etd.SPHSphereTestCase,
    ],
)
def test_groupwise_reductions(actx_factory, case_cls, visualize):
    actx = get_cl_array_context(actx_factory)

    case = case_cls()
    logger.info("\n%s", case)

    offset_p = np.array([1, 2, 0][: case.ambient_dim])
    case_p = case_cls(mesh_arguments={**case.mesh_arguments, "offset": offset_p})

    offset_m = np.array([-3, -4, 0][: case.ambient_dim])
    case_m = case_cls(mesh_arguments={**case.mesh_arguments, "offset": offset_m})

    from pystopt.measure import EOCRecorder, stringify_eoc, verify_eoc

    eoc_p = EOCRecorder(name="p")
    eoc_m = EOCRecorder(name="m")

    from pystopt.mesh import merge_disjoint_spectral_discretizations

    for r in case.resolutions:
        discr_p = case_p.get_discretization(actx, r)
        discr_m = case_m.get_discretization(actx, r)
        discr = merge_disjoint_spectral_discretizations(actx, [discr_p, discr_m])

        # {{{ test convergence

        from pystopt import bind, sym

        xc = bind(discr, sym.surface_centroid(discr.ambient_dim, groupwise=True))(actx)
        xc = actx.to_numpy(xc)

        h_max = actx.to_numpy(bind(discr, sym.h_max(discr.ambient_dim))(actx))
        error_p = la.norm([xc[0][0] - offset_p[0], xc[1][0] - offset_p[1]]) / la.norm(
            offset_p
        )
        error_m = la.norm([xc[0][1] - offset_m[0], xc[1][1] - offset_m[1]]) / la.norm(
            offset_m
        )
        logger.info("h_max %.5e error p %.12e m %.12e", h_max, error_p, error_m)

        eoc_p.add_data_point(h_max, error_p)
        eoc_m.add_data_point(h_max, error_m)

        # }}}

        # {{{ test math

        xc = sym.surface_centroid(discr.ambient_dim, groupwise=True)
        x = sym.nodes(discr.ambient_dim).as_vector()
        n = sym.normal(discr.ambient_dim).as_vector()

        xc_dot_xc = sym.NodeSum(xc @ xc)
        xc_dot_n = (xc @ (x - xc)) * n

        xc_dot_xc = bind(discr, xc_dot_xc)(actx)
        xc_dot_n = bind(discr, xc_dot_n)(actx)

        # }}}

    logger.info("\n%s", stringify_eoc(eoc_p, eoc_m))
    verify_eoc(eoc_p, case.target_order)
    verify_eoc(eoc_m, case.target_order)


# }}}


if __name__ == "__main__":
    import sys

    import pyopencl as cl  # noqa: F401

    if len(sys.argv) > 1:
        exec(sys.argv[1])
    else:
        pytest.main([__file__])
