# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

from dataclasses import dataclass, replace

import numpy as np
import pytest

from arraycontext import ArrayContext
from pytential import GeometryCollection

import pystopt.callbacks as cb
from pystopt import sym
from pystopt.checkpoint import IteratorCheckpointManager
from pystopt.tools import (
    get_cl_array_context,
    get_default_logger,
    pytest_generate_tests_for_array_contexts,
)

import extra_test_data as etd

logger = get_default_logger(__file__)
filenamer = etd.get_filename_generator(__file__)
pytest_generate_tests = pytest_generate_tests_for_array_contexts([
    "pystopt.pyopencl",
])


# {{{ test_checkpoint_writer


@pytest.mark.parametrize("name", ["h5"])
def test_checkpoint_output(actx_factory, name):
    actx = get_cl_array_context(actx_factory)

    def _get_reader_writer(name):
        if name == "h5":
            from pystopt.checkpoint import make_hdf_checkpoint

            return make_hdf_checkpoint(replace(filenamer, ext=".h5")("writer"))
        else:
            raise ValueError(f"unknown writer name: {name}")

    rw = _get_reader_writer(name)
    checkpoint = IteratorCheckpointManager(rw=rw, pattern="writer{i}")

    from pystopt.checkpoint.hdf import array_context_for_pickling

    with array_context_for_pickling(actx):
        for i in range(10):
            checkpoint.write({"i": i}, overwrite=True)

        del checkpoint

        out = _get_reader_writer(name)
        assert out.read("writer2") == {"i": 2}

        with pytest.raises(ValueError, match="already exists"):
            out.write("writer0", {"i": 0}, overwrite=False)


# }}}


# {{{ test_checkpoint_restart


@pytest.mark.parametrize("name", ["h5"])
@pytest.mark.parametrize("ncheckpoints", [0, 1, 2, 10])
def test_checkpoint_restart(actx_factory, name, ncheckpoints):
    actx = get_cl_array_context(actx_factory)
    basename = replace(filenamer, ext=".h5")

    def _get_reader_writer(name, suffix, *, keep=False):
        if name == "h5":
            filename = basename.with_suffix(f"restart_{suffix}")
            if not keep and filename.exists():
                filename.unlink()

            from pystopt.checkpoint import make_hdf_checkpoint

            return make_hdf_checkpoint(filename)
        else:
            raise ValueError(f"unknown writer name: {name}")

    # {{{ test advance with metadata

    pattern = "writer{i}"
    rw = _get_reader_writer(name, "metadata")
    checkpoint = IteratorCheckpointManager(rw=rw, pattern=pattern)

    from pystopt.checkpoint.hdf import array_context_for_pickling

    with array_context_for_pickling(actx):
        for i in range(ncheckpoints):
            checkpoint.write({"i", i}, overwrite=True)

        prev_key = checkpoint.prev_key
        next_key = checkpoint.next_key
        ncalls = checkpoint._ncalls

        checkpoint.done()

    rw = _get_reader_writer(name, "metadata", keep=True)
    checkpoint = IteratorCheckpointManager(rw=rw, pattern=pattern)
    checkpoint.advance()

    assert prev_key == checkpoint.prev_key, (prev_key, checkpoint.prev_key)
    assert next_key == checkpoint.next_key, (next_key, checkpoint.next_key)
    assert ncalls == checkpoint._ncalls

    # }}}

    # {{{ check advance without metadata

    rw = _get_reader_writer(name, "bare")
    checkpoint = IteratorCheckpointManager(rw=rw, pattern=pattern)

    with array_context_for_pickling(actx):
        for i in range(ncheckpoints):
            checkpoint.write({"i", i}, overwrite=True)

        prev_key = checkpoint.prev_key
        next_key = checkpoint.next_key
        ncalls = checkpoint._ncalls

    rw = _get_reader_writer(name, "bare", keep=True)
    checkpoint = IteratorCheckpointManager(rw=rw, pattern=pattern)
    assert "_chk_metadata" not in checkpoint.rw

    checkpoint.advance()
    assert prev_key == checkpoint.prev_key, (prev_key, checkpoint.prev_key)
    assert next_key == checkpoint.next_key, (next_key, checkpoint.next_key)
    assert ncalls == checkpoint._ncalls

    # }}}

    # {{{ check manual advance

    rw = _get_reader_writer(name, "manual")
    checkpoint = IteratorCheckpointManager(rw=rw, pattern=pattern)

    with array_context_for_pickling(actx):
        for i in range(ncheckpoints):
            checkpoint.write({"i", i}, overwrite=True)
        checkpoint.done()

    rw = _get_reader_writer(name, "manual", keep=True)
    checkpoint = IteratorCheckpointManager(rw=rw, pattern=pattern)
    checkpoint.advance(ncheckpoints)

    assert checkpoint.next_key == f"writer{ncheckpoints}", checkpoint.next_key
    assert checkpoint._ncalls == ncheckpoints, checkpoint._ncalls

    # }}}


# }}}


# {{{ test_checkpointing_mesh


@pytest.mark.parametrize(
    "case_cls",
    [
        etd.FourierCircleTestCase,
        etd.SPHSphereTestCase,
    ],
)
def test_checkpointing_mesh(actx_factory, case_cls, visualize):
    actx = get_cl_array_context(actx_factory)

    # {{{ geometry

    target_order = 3
    case = case_cls(target_order=target_order)
    logger.info("\n%s", case)

    discr = case.get_discretization(actx, case.resolutions[1])

    # }}}

    # {{{ pickle roundtrip

    h5filenamer = replace(filenamer, ext=".h5")

    from pystopt.checkpoint.hdf import array_context_for_pickling, dump, load

    filename = h5filenamer("mesh")

    with array_context_for_pickling(actx):
        dump(discr, filename)
        discr_out = load(filename)

    # }}}

    # {{{ check

    from pystopt.dof_array import dof_array_rnorm

    # check that the mesh is the same
    assert discr.mesh == discr_out.mesh

    # check that the nodes are the same
    nodes_in = actx.thaw(discr.nodes())
    nodes_out = actx.thaw(discr_out.nodes())

    error = actx.to_numpy(dof_array_rnorm(nodes_in, nodes_out))
    logger.info("error[nodes]: %.5e", error)
    assert error < 1.0e-15

    # check the spectral part is the same by doing a roundtrip
    x = nodes_in[0]
    x_in = discr.from_spectral_conn(discr.to_spectral_conn(x))
    x_out = discr_out.from_spectral_conn(discr_out.to_spectral_conn(x))

    error = actx.to_numpy(dof_array_rnorm(x_in, x_out))
    logger.info("error[spectral]: %.5e", error)
    assert error < 1.0e-15

    # }}}


# }}}


# {{{ test_checkpointing_callbacks


@dataclass(frozen=True)
class CheckpointState:
    array_context: ArrayContext


@dataclass(frozen=True)
class GeometryCheckpointWrangler:
    places: GeometryCollection
    dofdesc: sym.DOFDescriptor


@dataclass(frozen=True)
class GeometryCheckpointState(CheckpointState):
    wrangler: GeometryCheckpointWrangler

    def get_geometry_collection(self):
        return self.wrangler.places


@dataclass(frozen=True)
class CheckpointInfo:
    it: int
    alpha: float
    f: float
    g: np.ndarray
    d: np.ndarray | None


def _callback_checkpoint_roundtrip(actx, obj, name, fields=None, *, overwrite=True):
    from pystopt.checkpoint import make_hdf_checkpoint

    filename = replace(filenamer, ext=".h5").with_suffix(name)
    if filename.exists():
        filename.unlink()

    from pystopt.checkpoint.hdf import array_context_for_pickling

    with array_context_for_pickling(actx):
        rw = make_hdf_checkpoint(filename)
        with IteratorCheckpointManager(rw) as checkpoint:
            checkpoint.write_to(name, obj, overwrite=overwrite)

        rw = make_hdf_checkpoint(filename)
        with IteratorCheckpointManager(rw) as checkpoint:
            result = checkpoint.read_from(name)

    if fields:
        result = replace(result, **fields)

    return result


def test_checkpointing_optimization_log_callback(actx_factory):
    actx = get_cl_array_context(actx_factory)
    rng = np.random.default_rng(42)

    callback_old = cb.OptimizationLogCallback(norm=np.linalg.norm, log=logger.info)
    nruns = 10

    for i in range(nruns):
        state = CheckpointState(array_context=actx)
        info = CheckpointInfo(
            it=i,
            alpha=rng.random(),
            f=rng.random(),
            g=rng.random(128),
            d=rng.random(128),
        )

        callback_old(state, info=info)

    callback_new = _callback_checkpoint_roundtrip(
        actx,
        callback_old,
        "optimization_log_cb",
        fields={"norm": callback_old.norm, "log": callback_old.log},
    )

    for i in range(nruns):
        state = CheckpointState(array_context=actx)
        info = CheckpointInfo(
            it=i,
            alpha=rng.random(),
            f=rng.random(),
            g=rng.random(128),
            d=rng.random(128),
        )

        callback_new(state, info=info)


@pytest.mark.parametrize(
    ("cls", "name"),
    [
        (cb.PerformanceHistoryCallback, "performance_history_cb"),
        (cb.OptimizationHistoryCallback, "optimization_history_cb"),
    ],
)
def test_checkpointing_history_callback(actx_factory, cls, name):
    if issubclass(cls, cb.PerformanceHistoryCallback):
        pytest.importorskip("psutil")

    # {{{ setup

    actx = get_cl_array_context(actx_factory)
    callback_old = cls(norm=np.linalg.norm)

    from pystopt.tools import dc_items

    histories = [
        key
        for key, value in dc_items(callback_old, init_only=False)
        if isinstance(value, list)
    ]

    # }}}

    # {{{ check add data

    rng = np.random.default_rng(42)
    nruns = 10

    for i in range(nruns):
        state = CheckpointState(array_context=actx)
        info = CheckpointInfo(
            it=i,
            alpha=rng.random(),
            f=rng.random(),
            g=rng.random(128),
            d=rng.random(128),
        )

        callback_old(state, info=info)

    # check the desired number of histories were added
    for history in histories:
        assert len(getattr(callback_old, history)) == nruns

    # }}}

    # {{{ check checkpoint

    callback_new = _callback_checkpoint_roundtrip(
        actx, callback_old, name, fields={"norm": np.linalg.norm}
    )

    # check the checkpointed data has the same number of histories
    for history in histories:
        assert len(getattr(callback_new, history)) == nruns

    # check that the histories are the same
    for history in histories:
        assert np.allclose(
            getattr(callback_old, history),
            getattr(callback_new, history),
            atol=1.0e-15,
        )

    for i in range(nruns):
        state = CheckpointState(array_context=actx)
        info = CheckpointInfo(
            it=i,
            alpha=rng.random(),
            f=rng.random(),
            g=rng.random(128),
            d=rng.random(128),
        )

        callback_new(state, info=info)

    # check that the new histories were correctly added
    for history in histories:
        assert len(getattr(callback_new, history)) == 2 * nruns

    # check that we can remove them
    callback_new.restart_from_iteration(nruns)
    for history in histories:
        assert len(getattr(callback_new, history)) == nruns

    for history in histories:
        assert np.allclose(
            getattr(callback_old, history),
            getattr(callback_new, history),
            atol=1.0e-15,
        )

    # }}}


def test_checkpointing_checkpoint_callback(actx_factory):
    # {{{ setup

    actx = get_cl_array_context(actx_factory)

    pattern = "Checkpoint{i:09d}"
    filename = replace(filenamer, ext=".h5").with_suffix("callback_output")
    if filename.exists():
        filename.unlink()

    from pystopt.checkpoint import make_hdf_checkpoint_manager

    checkpoint = replace(make_hdf_checkpoint_manager(filename), pattern=pattern)
    callback_old = cb.CheckpointCallback(
        norm=np.linalg.norm, checkpoint=checkpoint, overwrite=True
    )

    # }}}

    # {{{ check add data

    rng = np.random.default_rng(42)

    nruns = 10
    fields = {}

    from pystopt.checkpoint.hdf import array_context_for_pickling

    with array_context_for_pickling(actx):
        for _ in range(nruns):
            state = CheckpointState(array_context=actx)
            fields["data"] = cb.OutputField.checkpoint(
                "data", actx.from_numpy(rng.random(128))
            )

            callback_old(state, fields=fields)

        # check that all the keys were written in the right order
        h5 = callback_old.checkpoint.rw.h5root  # pylint: disable=no-member
        assert set(h5.keys()) == {pattern.format(i=i) for i in range(nruns)}

        assert callback_old.checkpoint.prev_key == pattern.format(i=nruns - 1)
        assert callback_old.checkpoint.next_key == pattern.format(i=nruns)

    # }}}

    # {{{ check checkpoint

    callback_new = _callback_checkpoint_roundtrip(
        actx, callback_old, "checkpoint_cb", fields={"norm": np.linalg.norm}
    )

    # check that all the keys were retrieved in the right order
    h5 = callback_new.checkpoint.rw.h5root
    assert set(h5.keys()) == {pattern.format(i=i) for i in range(nruns)}

    assert callback_new.checkpoint.prev_key == pattern.format(i=nruns - 1)
    assert callback_new.checkpoint.next_key == pattern.format(i=nruns)

    with array_context_for_pickling(actx):
        for _ in range(nruns):
            state = CheckpointState(array_context=actx)
            fields["data"] = cb.OutputField.checkpoint(
                "data", actx.from_numpy(rng.random(128))
            )

            callback_new(state, fields=fields)

        # check that the new keys were all added in the right order
        h5 = callback_new.checkpoint.rw.h5root
        assert set(h5.keys()) == {pattern.format(i=i) for i in range(2 * nruns)}

        assert callback_new.checkpoint.prev_key == pattern.format(i=2 * nruns - 1)
        assert callback_new.checkpoint.next_key == pattern.format(i=2 * nruns)

    callback_new.restart_from_iteration(nruns - 1)
    assert callback_new.checkpoint.prev_key == pattern.format(i=nruns - 1)
    assert callback_new.checkpoint.next_key == pattern.format(i=nruns)

    # }}}


def test_checkpointing_visualize_callback(actx_factory):
    pytest.importorskip("pyvisfile")

    # {{{ setup

    actx = get_cl_array_context(actx_factory)

    from pystopt.paths import generate_filename_series

    callback_old = cb.VisualizeCallback(
        norm=np.linalg.norm,
        visualize_file_series=generate_filename_series(
            filenamer.stem, suffix="visualize_output", cwd=filenamer.cwd, ext=None
        ),
        overwrite=True,
    )

    case = etd.SPHSphereTestCase()
    discr = case.get_discretization(actx, case.resolutions[0])

    places = GeometryCollection(discr, auto_where="callback")
    dofdesc = places.auto_source

    # }}}

    # {{{ check add data

    nruns = 10
    fields = {}

    for i in range(nruns):
        wrangler = GeometryCheckpointWrangler(places=places, dofdesc=dofdesc)
        state = GeometryCheckpointState(array_context=actx, wrangler=wrangler)

        fields["data"] = cb.OutputField.visualize("data", i + discr.zeros(actx))

        callback_old(state, fields=fields)

    assert (
        callback_old._previous_filename.aspath().name
        == f"{filenamer.stem}_visualize_output_{nruns - 1:09d}"
    )

    # }}}

    # {{{ check checkpoint

    callback_new = _callback_checkpoint_roundtrip(
        actx, callback_old, "visualize_cb", fields={"norm": np.linalg.norm}
    )

    for i in range(nruns):
        wrangler = GeometryCheckpointWrangler(places=places, dofdesc=dofdesc)
        state = GeometryCheckpointState(array_context=actx, wrangler=wrangler)
        fields["data"] = cb.OutputField.visualize("data", i + discr.zeros(actx))

        callback_new(state, fields=fields)

    assert (
        callback_new._previous_filename.aspath().name
        == f"{filenamer.stem}_visualize_output_{2 * nruns - 1:09d}"
    )

    # }}}


# }}}


if __name__ == "__main__":
    import sys

    import pyopencl as cl  # noqa: F401

    if len(sys.argv) > 1:
        exec(sys.argv[1])
    else:
        pytest.main([__file__])
