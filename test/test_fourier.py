# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

import numpy as np
import numpy.linalg as la
import pytest

import meshmode.discretization.poly_element as mpoly
from arraycontext import flatten

from pystopt import bind, sym
from pystopt.dof_array import dof_array_rnorm
from pystopt.tools import (
    get_cl_array_context,
    get_default_logger,
    pytest_generate_tests_for_array_contexts,
)

import extra_test_data as etd

logger = get_default_logger(__file__)
filenamer = etd.get_filename_generator(__file__)
pytest_generate_tests = pytest_generate_tests_for_array_contexts([
    "pystopt.pyopencl",
])


# {{{ test_np_fft_compatibility


def test_np_fft_compatibility(actx_factory, visualize):
    actx = get_cl_array_context(actx_factory)

    # {{{ geometry

    radius = 1.0
    target_order = 3

    case = etd.FourierCircleTestCase(
        mesh_arguments={
            "target_order": target_order,
            "group_factory_cls": mpoly.PolynomialEquidistantSimplexGroupFactory,
            "radius": radius,
        }
    )

    logger.info("\n%s", case)

    discr = case.get_discretization(actx, case.resolutions[1])

    assert len(discr.specgroups) == 1
    grp = discr.specgroups[0]

    # }}}

    # {{{ test connection

    x = actx.thaw(discr.nodes())
    f = actx.np.exp(x[0])

    f_hat = discr.to_spectral_conn(f)
    f_roundtrip = discr.from_spectral_conn(f_hat)

    error = actx.to_numpy(dof_array_rnorm(f_roundtrip, f))
    logger.info("error[roundtrip]:  %.5e", error)
    assert error < 1.0e-13

    # }}}

    # {{{ test derivative

    # NOTE:
    #   x[0] == cos(theta), x[1] == sin(theta) => x'[0] = -x[1]
    # then
    #   d/dtheta exp(cos(theta)) = -sin(theta) * exp(cos(theta))
    df = -x[1] * f

    # NOTE: the discr nodes are not uniform, so we need to compute the
    # jacobian honestly to make this work
    from meshmode.discretization import num_reference_derivative

    jac_num = actx.np.sqrt(
        num_reference_derivative(discr, [0], x[0]) ** 2
        + num_reference_derivative(discr, [0], x[1]) ** 2
    )

    from pystopt.mesh.spectral import spectral_num_reference_derivative

    jac_fft = actx.np.sqrt(
        spectral_num_reference_derivative(discr, [0], x[0]) ** 2
        + spectral_num_reference_derivative(discr, [0], x[1]) ** 2
    )
    error_jac = actx.to_numpy(dof_array_rnorm(jac_fft, grp.jacobian))
    logger.info("error[jac]:        %.5e", error_jac)
    assert error_jac < 1.0e-13

    df_num = 1.0 / jac_num * num_reference_derivative(discr, [0], f)
    df_fft = 1.0 / jac_fft * spectral_num_reference_derivative(discr, [0], f)

    error_num = actx.to_numpy(dof_array_rnorm(df, df_num))
    error_fft = actx.to_numpy(dof_array_rnorm(df, df_fft))
    error = actx.to_numpy(dof_array_rnorm(df_num, df_fft))
    logger.info("error[deriv]:      %.5e %.5e (%.5e)", error_num, error_fft, error)
    assert error_fft < 5.0e-5

    # }}}

    if not visualize:
        return

    from pystopt.visualization import make_visualizer

    vis = make_visualizer(actx, discr, vis_order=target_order)

    filename = filenamer.with_suffix("fourier_derivative")
    vis.write_file(
        filename, [("df", df), ("num", df_num), ("fft", df_fft)], overwrite=True
    )

    e_num = actx.np.log10(actx.np.fabs(df - df_num) + 1.0e-16)
    e_fft = actx.np.log10(actx.np.fabs(df - df_fft) + 1.0e-16)

    filename = filenamer.with_suffix("fourier_derivative_error")
    vis.write_file(filename, [("num", e_num), ("fft", e_fft)], overwrite=True)


# }}}


# {{{ test_fourier_interpolation


@pytest.mark.parametrize(
    "cls",
    [
        etd.FourierCircleTestCase,
        etd.FourierEllipseTestCase,
    ],
)
def test_fourier_interpolation(actx_factory, cls, visualize):
    actx = get_cl_array_context(actx_factory)

    target_order = 3
    to_case = cls(mesh_arguments={"target_order": target_order})
    from_case = etd.FourierCircleTestCase(
        mesh_arguments={"target_order": target_order, "radius": 3.0}
    )

    logger.info("\n%s", to_case)

    # {{{ geometry

    to_discr = to_case.get_discretization(actx, to_case.resolutions[-1])
    from_discr = from_case.get_discretization(actx, to_case.resolutions[-1])

    from pystopt.mesh.fourier import FourierThetaInterpolateConnection

    interp = FourierThetaInterpolateConnection(from_discr, to_discr)

    # }}}

    # {{{ check interpolation

    def fn(theta):
        return actx.np.cos(theta) ** 2

    from pystopt.mesh.fourier import polar_angle

    to_theta = polar_angle(actx.thaw(to_discr.nodes()))
    from_theta = polar_angle(actx.thaw(from_discr.nodes()))

    to_f = fn(to_theta)
    from_f = fn(from_theta)
    interp_f = interp(from_f)

    logger.info("error: %.5e", actx.to_numpy(actx.np.min(interp_f)))

    # }}}

    if not visualize:
        return

    from pystopt.visualization import make_visualizer

    vis = make_visualizer(actx, to_discr, vis_order=target_order)

    vis.write_file(
        filenamer.with_suffix("fourier_interp"),
        [
            ("f_f", from_f),
            ("f_t", to_f),
            ("f_i", interp_f),
        ],
        overwrite=True,
        markers=["-", "--", ":"],
    )


# }}}


# {{{ test_fourier_interpolation_error


@pytest.mark.parametrize(
    "group_factory_cls",
    [
        mpoly.InterpolatoryQuadratureGroupFactory,
        mpoly.InterpolatoryEdgeClusteredGroupFactory,
    ],
)
@pytest.mark.parametrize(
    "target_order",
    [
        3,
        5,
        # 4, 6      # likely redundant
    ],
)
def test_fourier_interpolation_error(
    actx_factory, group_factory_cls, target_order, visualize
):
    actx = get_cl_array_context(actx_factory)

    geometry = "ellipse"
    if geometry == "starfish":
        case = etd.FourierStarfishTestCase(
            target_order=target_order,
            group_factory_cls=group_factory_cls,
            mesh_arguments={"use_default_mesh_unit_nodes": True},
        )
    elif geometry == "ellipse":
        case = etd.FourierEllipseTestCase(
            target_order=target_order,
            group_factory_cls=group_factory_cls,
            mesh_arguments={
                "use_default_mesh_unit_nodes": True,
                "aspect_ratio": 1.25,
            },
        )
    else:
        raise ValueError

    from dataclasses import replace

    equi = replace(case, group_factory_cls=mpoly.InterpolatoryEquidistantGroupFactory)

    # {{{ geometry

    resolution = case.resolutions[-2]
    discr = case.get_discretization(actx, resolution).copy(xlm=None)
    equi_discr = equi.get_discretization(actx, resolution).copy(xlm=None)

    from pytential import GeometryCollection

    places = GeometryCollection(
        {case.name: discr, f"{case.name}_equi": equi_discr}, auto_where=case.name
    )

    logger.info("ndofs:     %d", discr.ndofs)
    logger.info("nelements: %d", discr.mesh.nelements)

    poly_dd = places.auto_source
    equi_dd = poly_dd.copy(geometry=f"{case.name}_equi")

    # }}}

    # {{{ test function

    def nodes(dofdesc, iaxis=0):
        return bind(
            places,
            sym.nodes(places.ambient_dim, dofdesc=dofdesc).as_vector()[iaxis],
            auto_where=dofdesc,
        )(actx)

    def normal(dofdesc, iaxis=0):
        return bind(
            places,
            sym.normal(places.ambient_dim, dofdesc=dofdesc).as_vector()[iaxis],
            auto_where=dofdesc,
        )(actx)

    def curvature(dofdesc, iaxis=0):
        return bind(
            places,
            sym.mean_curvature(places.ambient_dim, dofdesc=dofdesc),
            auto_where=dofdesc,
        )(actx)

    fn = normal

    # }}}

    # {{{ check

    # check that the interpolation introduces additional high frequency modes
    x = fn(poly_dd)
    xlm = actx.to_numpy(flatten(discr.to_spectral_conn(x), actx))

    equi_x = fn(equi_dd)
    equi_xlm = actx.to_numpy(flatten(equi_discr.to_spectral_conn(equi_x), actx))

    n_modes = np.sum(np.abs(xlm) > 1.0e-13)
    n_equi_modes = np.sum(np.abs(equi_xlm) > 1.0e-13)
    logger.info("modes: %2d equi %2d", n_modes, n_equi_modes)

    # assert the magnitude of the extra modes
    xlm_error = la.norm(xlm - equi_xlm, ord=np.inf)
    logger.info("error[poly xlm]: %.5e", xlm_error)

    # }}}

    # {{{ estimate frequencies

    h_min = actx.to_numpy(bind(discr, sym.h_min_from_volume(discr.ambient_dim))(actx))
    h_max = actx.to_numpy(bind(discr, sym.h_max_from_volume(discr.ambient_dim))(actx))
    error_h_max = h_max ** (target_order + 1)

    h_ref = 2.0 / resolution
    error_ref_max = h_ref ** (target_order + 1)

    logger.info(
        "h %.5e %.5e error %.5e ref_error %.5e",
        h_min,
        h_max,
        error_h_max,
        error_ref_max,
    )
    # assert xlm_error < 20 * h_max

    from pystopt.mesh.fourier import find_modes_for_tolerance

    n_min_modes = find_modes_for_tolerance(actx, equi.curve_fn, rtol=h_min)
    logger.info("modes: %d", n_min_modes)

    # assert error_ref_max < xlm_error < error_h_max

    # }}}

    # {{{ plot

    if not visualize:
        return

    basename = replace(
        filenamer,
        stem=f"{filenamer.stem}_fourier_interp_{resolution}_{target_order}",
    )

    from pystopt.mesh.fourier import visualize_fourier_modes
    from pystopt.visualization.matplotlib import subplots

    name = group_factory_cls.__name__[7:17]
    filename = basename.with_suffix(f"{name}")
    with subplots(filename, overwrite=True) as fig:
        visualize_fourier_modes(
            fig,
            actx,
            discr,
            [
                (r"\hat{x}", x),  # noqa: RUF027
                # ("y", c_equi_xlm),
            ],
            overwrite=True,
        )

        ax = fig.gca()
        # ax.axhline(y=error_h_max, ls=":", color="k")
        ax.axhline(y=error_ref_max, ls="--", color="k")
        # ax.axvline(x=n_min_modes, color="b")

        k_max = np.arange(0, discr.specgroups[0].noutputs // 2 + 1, resolution)
        k_max = [2.0 / h_min, 2.0 / h_max]
        # k_max = [h_max / h_min / h_ref, h_min / h_max / h_ref]

        for k in k_max:
            ax.axvline(x=int(k), color="k")
            ax.axvline(x=-int(k), color="k")

    filename = basename.with_suffix("equidistant")
    with subplots(filename, overwrite=True) as fig:
        visualize_fourier_modes(
            fig,
            actx,
            equi_discr,
            [
                (r"\hat{x}", equi_x),  # noqa: RUF027
                # ("y", c_equi_xlm),
            ],
            overwrite=True,
        )

        # ax = fig.gca()
        # ax.axhline(y=error_h_max, ls=":", color="k")
        # ax.axhline(y=error_ref_max, ls="--", color="k")
        # ax.axvline(x=n_min_modes, color="b")

    # }}}


# }}}


# {{{ test_fourier_qbx_errors


@pytest.mark.slowtest
@pytest.mark.parametrize("qbx_order", [3, 5, 7])
@pytest.mark.parametrize("source_ovsmp", [3, 6, 9])
def test_fourier_qbx_error(actx_factory, qbx_order, source_ovsmp, visualize):
    actx = get_cl_array_context(actx_factory)

    target_order = 4
    viscosity_ratio = 1.0
    capillary_number = 1.0

    case_name = "ellipse"
    farfield_name = "uniform"

    if case_name == "circle":
        cls = etd.FourierCircleTestCase
    elif case_name == "ellipse":
        cls = etd.FourierEllipseTestCase
    else:
        raise ValueError(case_name)

    case = cls(
        target_order=target_order,
        qbx_order=qbx_order,
        source_ovsmp=source_ovsmp,
        mesh_arguments={"rfft": False},
    )

    # {{{ geometry

    resolution = case.resolutions[2]

    from pytential import GeometryCollection

    qbx = case.get_layer_potential(actx, resolution)
    places = GeometryCollection(qbx, auto_where=case.name)
    dofdesc = places.auto_source

    density_discr = places.get_discretization(case.name)
    logger.info("ndofs:     %d", density_discr.ndofs)
    logger.info("nelements: %d", density_discr.mesh.nelements)

    # }}}

    # {{{ optim

    from pystopt.cost import NormalVelocityFunctional

    cost = NormalVelocityFunctional(
        u=sym.make_sym_vector("u", places.ambient_dim),
        ud=0,
    )

    from pystopt import stokes

    if farfield_name == "uniform":
        bc = stokes.get_uniform_farfield(
            places.ambient_dim, uinf=1.0, capillary_number_name="ca"
        )
    elif farfield_name == "extensional":
        bc = stokes.get_extensional_farfield(
            places.ambient_dim, alpha=1.0, capillary_number_name="ca"
        )
    else:
        raise ValueError(farfield_name)
    op = stokes.TwoPhaseSingleLayerStokesRepresentation(bc)

    from pystopt.simulation.staticstokes import make_stokes_shape_optimization_wrangler

    wrangler = make_stokes_shape_optimization_wrangler(
        places,
        cost,
        op,
        viscosity_ratio=viscosity_ratio,
        capillary_number=capillary_number,
        context={},
        is_spectral=False,
    )
    state = wrangler.get_initial_state(actx)

    # }}}

    # {{{ values

    r = state._get_stokes_solution()
    u = state.get_stokes_velocity()
    grad = state.evaluate_shape_gradient()

    dudn = stokes.sti.normal_velocity_gradient(r, side=None, qbx_forced_limit="avg")
    hess = stokes.sti.normal_velocity_hessian(r, side=None, qbx_forced_limit="avg")
    grad_dot_n = bind(places, sym.n_dot(places.ambient_dim), auto_where=dofdesc)(
        actx, x=grad
    )

    # }}}

    # {{{ visualize

    if not visualize:
        return

    k_max = np.arange(0, density_discr.specgroups[0].noutputs // 2 + 1, resolution)

    from pystopt.mesh.fourier import visualize_fourier_modes
    from pystopt.visualization.matplotlib import subplots

    order = f"qbx_{qbx_order:02d}_ovsmp_{source_ovsmp:02d}"
    case = f"{case_name}_{farfield_name[:3]}"
    basename = filenamer.prepend(f"qbx_error_vr_{viscosity_ratio}_{order}_{case}")

    names_and_fields = [
        ("du_x", dudn[0]),
        ("du_y", dudn[1]),
        ("u_x", u[0]),
        ("u_y", u[1]),
        (r"hess", hess),
        ("g_x", grad[0]),
        ("g_y", grad[1]),
        ("g_n", grad_dot_n),
    ]

    for name, field in names_and_fields:
        filename = basename.with_suffix(name)
        with subplots(filename, overwrite=True) as fig:
            ax = fig.gca()  # pylint: disable=no-member

            visualize_fourier_modes(
                fig, actx, density_discr, [(name, field)], overwrite=True
            )

            for k in k_max:
                ax.axvline(x=k, color="k")
                ax.axvline(x=-k, color="k")

    # }}}


# }}}


if __name__ == "__main__":
    import sys

    import pyopencl as cl  # noqa: F401

    if len(sys.argv) > 1:
        exec(sys.argv[1])
    else:
        pytest.main([__file__])
