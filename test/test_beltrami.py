# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

from dataclasses import dataclass, field
from typing import Any

import pytest

from pytential import GeometryCollection

from pystopt import bind, sym
from pystopt.tools import (
    get_cl_array_context,
    get_default_logger,
    pytest_generate_tests_for_array_contexts,
)

import extra_test_data as etd

logger = get_default_logger(module=__file__)
filenamer = etd.get_filename_generator(__file__)
pytest_generate_tests = pytest_generate_tests_for_array_contexts([
    "pystopt.pyopencl",
])


# {{{ solutions


@dataclass(frozen=True)
class LaplaceBeltramiSolution:
    name: str = "laplace"
    radius: float = 1.0
    m: int = 1
    n: int = 3

    @property
    def eig(self):
        return -self.n * (self.n + 1) / self.radius**2

    @property
    def context(self):
        return {}

    def source(self, actx, discr):
        from pystopt.mesh.spharm import evaluate_spharm_from_coordinates

        y_mn = evaluate_spharm_from_coordinates(actx, discr, self.m, self.n)
        return self.eig * y_mn

    def exact(self, actx, discr):
        from pystopt.mesh.spharm import evaluate_spharm_from_coordinates

        return evaluate_spharm_from_coordinates(actx, discr, self.m, self.n)


class YukawaBeltramiSolution(LaplaceBeltramiSolution):
    name: str = "yukawa"
    k: float = 1.0

    @property
    def context(self):
        return {"k": self.k}

    def source(self, actx, discr):
        from pystopt.mesh.spharm import evaluate_spharm_from_index

        y_mn = evaluate_spharm_from_index(actx, discr, self.m, self.n)
        return (self.k**2 - self.eig) * y_mn


# }}}


def run_beltrami(actx, op, solution, case, r, *, visualize=False):
    # {{{ discretization

    qbx = case.get_layer_potential(actx, r)
    places = GeometryCollection(qbx, auto_where=case.name)

    density_discr = places.get_discretization(case.name)
    logger.info("ndofs: %d", density_discr.ndofs)
    logger.info("nelements: %d", density_discr.mesh.nelements)

    # }}}

    # {{{ beltrami

    # analytic solutions
    exact_sol = solution.exact(actx, density_discr)
    rhs = solution.source(actx, density_discr)

    # solve
    from pystopt.operators import solve_beltrami

    approx_sol = solve_beltrami(
        actx, places, op, rhs, dofdesc=case.name, context=solution.context
    )
    approx_sol = actx.np.real(approx_sol)

    # check
    from pystopt.dof_array import dof_array_rnorm

    error = actx.to_numpy(dof_array_rnorm(approx_sol, exact_sol))
    h_max = actx.to_numpy(bind(places, sym.h_max_from_volume(case.ambient_dim))(actx))

    logger.info("hmax %.5e error %.5e", h_max, error)

    # }}}

    # {{{ visualize

    if visualize:
        r = r if isinstance(r, int) else r[0]
        filename = filenamer.with_suffix(f"{case.name}_{solution.name}_{r:04d}")

        from pystopt.visualization import make_visualizer

        vis = make_visualizer(actx, density_discr, case.target_order)
        vis.write_file(
            filename,
            [
                ("solution", approx_sol),
                ("analytic", exact_sol),
                (
                    "error",
                    actx.np.log10(actx.np.abs(approx_sol - exact_sol) + 1.0e-16),
                ),
            ],
            overwrite=True,
        )

    # }}}

    return h_max, error


@dataclass(frozen=True)
class BeltramiTestCase(etd.SPHSphereTestCase):
    context: dict[str, Any] = field(default_factory=dict)


@pytest.mark.skip("broken")
@pytest.mark.slowtest
@pytest.mark.parametrize("precond", ["left", "right"])
def test_laplace_beltrami(actx_factory, precond, visualize):
    actx = get_cl_array_context(actx_factory)

    radius = 1.0
    target_order = 5

    solution = LaplaceBeltramiSolution(radius)
    case = BeltramiTestCase(
        target_order=target_order,
        qbx_order=5,
        source_ovsmp=4,
        fmm_order=10,
        mesh_arguments={"radius": radius},
    )

    logger.info("\n%s", str(case))

    from pystopt.operators import LaplaceBeltramiRepresentation

    op = LaplaceBeltramiRepresentation(case.ambient_dim, precond=precond)

    from pystopt.measure import EOCRecorder, stringify_eoc

    eoc = EOCRecorder()

    for r in list(case.resolutions):
        h, err = run_beltrami(actx, op, solution, case, r, visualize=visualize)
        eoc.add_data_point(h, err)

    logger.info("\n%s", stringify_eoc(eoc))
    assert eoc.satisfied(order=min(case.qbx_order, case.target_order) - 2.5)


@pytest.mark.skip("broken")
@pytest.mark.slowtest
def test_yukawa_beltrami(actx_factory, visualize):
    actx = get_cl_array_context(actx_factory)

    radius = 1.0
    target_order = 5

    solution = YukawaBeltramiSolution(radius)
    case = BeltramiTestCase(
        target_order=target_order,
        qbx_order=5,
        source_ovsmp=4,
        fmm_order=10,
        mesh_arguments={"radius": radius},
    )

    logger.info("\n%s", str(case))

    from pystopt.operators import YukawaBeltramiRepresentation

    op = YukawaBeltramiRepresentation(case.ambient_dim, yukawa_lambda_name="k")

    from pystopt.measure import EOCRecorder, stringify_eoc

    eoc = EOCRecorder()

    for r in list(case.resolutions):
        h, err = run_beltrami(actx, op, solution, case, r, visualize=visualize)
        eoc.add_data_point(h, err)

    logger.info("\n%s", stringify_eoc(eoc))
    assert eoc.satisfied(order=min(case.qbx_order, case.target_order) - 2.5)


if __name__ == "__main__":
    import sys

    import pyopencl as cl  # noqa: F401

    if len(sys.argv) > 1:
        exec(sys.argv[1])
    else:
        pytest.main([__file__])
