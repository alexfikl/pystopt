# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

from dataclasses import replace

import numpy as np
import numpy.linalg as la
import pytest

import meshmode.discretization.poly_element as mpoly
from arraycontext import flatten
from pytools.obj_array import make_obj_array

from pystopt import bind, sym
from pystopt.dof_array import dof_array_rnorm
from pystopt.tools import (
    get_cl_array_context,
    get_default_logger,
    pytest_generate_tests_for_array_contexts,
)

import extra_test_data as etd

logger = get_default_logger(__file__)
filenamer = etd.get_filename_generator(__file__)
pytest_generate_tests = pytest_generate_tests_for_array_contexts([
    "pystopt.pyopencl",
])


# {{{ test_shtns_assumptions


@pytest.mark.parametrize("odd", [False, True])
def test_shtns_assumptions(odd, visualize):
    # {{{ parameters

    radius = 1.0

    nlon = 32
    nlat = nlon // 2 + odd
    lmax = 7
    mmax = lmax // 2
    logger.info("nlon %d nlat %d lmax %d mmax %d", nlon, nlat, lmax, mmax)

    # }}}

    # {{{ create spharm grid

    from pystopt.mesh.spharm import _make_shtns

    sh = _make_shtns(lmax, mmax, nlat, nlon, polar_opt=1.0e-10)

    poles = [0.0, np.pi]
    theta = np.arccos(sh.cos_theta)
    phi = (2.0 * np.pi / nlon) * np.arange(nlon)

    if odd:
        m_phi, m_theta = np.meshgrid(phi, theta)
    else:
        m_phi, m_theta = np.meshgrid(phi, theta)

    # }}}

    # {{{ test equidistance

    dt = abs(theta[1] - theta[0])
    error_dt = la.norm(np.abs(np.diff(theta)) - dt) / dt
    error_pole = (
        abs(abs(theta[0] - poles[0]) - dt / 2.0)
        + abs(abs(theta[-1] - poles[1]) - dt / 2.0)
    ) / dt
    logger.info("error[theta]: int %.6e pole %.6e", error_dt, error_pole)
    assert error_dt < 2.5e-14
    assert error_pole < 1.5e-14

    dp = abs(phi[1] - phi[0])
    error_dp = la.norm(np.diff(phi) - dp) / abs(dp)
    error_pole = abs((2.0 * np.pi - phi[-1]) - dp) / abs(dp)
    logger.info("error[phi]: int %.6e pole %.6e", error_dt, error_pole)
    assert error_dt < 2.5e-14
    assert error_pole < 1.5e-14

    # }}}

    # {{{ check theta / phi

    # NOTE: Y10 = N10 * cos(theta), so this should allow us to check we're
    # getting theta back properly from shtns (and by extension phi)
    n, m = 1, 0

    import math

    ylm = sh.spec_array()
    ylm[sh.idx(n, m)] = np.sqrt(
        4.0 * np.pi / (2 * n + 1) * math.factorial(n + m) / math.factorial(n - m)
    )

    theta_roundtrip = np.arccos(sh.synth(ylm))
    assert la.norm(theta_roundtrip - theta.reshape(-1, 1)) / la.norm(theta) < 2.0e-15

    # }}}

    # {{{ test roundtrip

    assert m_phi.shape == sh.spat_shape

    a = 2.0 * radius
    b = radius
    x = a * np.sin(m_theta) * np.cos(m_phi)
    y = a * np.sin(m_theta) * np.sin(m_phi)
    z = b * np.cos(m_theta)

    xlm = sh.analys(x)
    ylm = sh.analys(y)
    zlm = sh.analys(z)
    assert xlm.shape == (sh.nlm,)  # pylint: disable=no-member

    x_roundtrip = sh.synth(xlm)
    y_roundtrip = sh.synth(ylm)
    z_roundtrip = sh.synth(zlm)
    assert x_roundtrip.shape == sh.spat_shape  # pylint: disable=no-member

    error_x = la.norm(x - x_roundtrip) / la.norm(x)
    error_y = la.norm(y - y_roundtrip) / la.norm(y)
    error_z = la.norm(z - z_roundtrip) / la.norm(z)
    logger.info("error[to]: x %.5e y %.5e z %.5e", error_x, error_y, error_z)
    assert error_x < 1.0e-14
    assert error_y < 1.0e-14
    assert error_z < 1.0e-14

    # }}}

    # {{{ check gradient

    name = "x"
    if name == "x":
        flm = sh.analys(x)
        dt = +a * np.cos(m_theta) * np.cos(m_phi)
        dp = -a * np.sin(m_theta) * np.sin(m_phi)
    elif name == "y":
        flm = sh.analys(y)
        dt = +a * np.cos(m_theta) * np.sin(m_phi)
        dp = +a * np.sin(m_theta) * np.cos(m_phi)
    else:  # == "z"
        flm = sh.analys(z)
        dt = -b * np.sin(m_theta)
        dp = 0.0

    dp_norm = la.norm(dp)
    if dp_norm < 1.0e-14:
        dp_norm = 1.0

    # check that SHTNS computes
    #   df/dtheta
    #   1/sin(theta) df/dphi
    dt_num = sh.spat_array()
    dp_num = sh.spat_array()
    sh.SHsph_to_spat(flm, dt_num, dp_num)

    dp_num *= np.sin(m_theta)

    error_dt = la.norm(dt_num - dt) / la.norm(dt)
    error_dp = la.norm(dp_num - dp) / dp_norm
    logger.info("error[grad]: dt %.5e dp %.5e", error_dt, error_dp)
    assert error_dt < 1.0e-14
    assert error_dp < 1.0e-14

    # check simpler dphi derivative
    dp_num = sh.synth(1.0j * sh.m * flm)

    error_dp = la.norm(dp_num - dp) / dp_norm
    logger.info("error[grad]: dp %.5e", error_dp)
    assert error_dp < 1.0e-14

    # }}}

    if not visualize:
        return

    # {{{ plot coordinates

    filename = filenamer.with_suffix("shtns_sanity_check")

    import matplotlib.pyplot as plt

    error = np.log10(np.abs(y - y_roundtrip) + 1.0e-16)

    fig, (ax1, ax2) = plt.subplots(2, 1, figsize=(12, 14))

    ax1.grid(zorder=1)
    ax1.tricontourf(m_theta.flat, m_phi.flat, y.flat, levels=32, zorder=0)
    ax1.set_xlabel(r"$\theta$")
    ax1.set_ylabel(r"$\phi$")
    ax1.set_aspect("equal")

    ax2.grid(zorder=1)
    ax2.tricontourf(m_theta.flat, m_phi.flat, error.flat, levels=32, zorder=0)
    ax2.set_xlabel(r"$\theta$")
    ax2.set_ylabel(r"$\phi$")
    ax2.set_aspect("equal")

    fig.savefig(filename)
    logger.info("output: %s", filename)
    plt.close(fig)

    # }}}

    # {{{ plot wireframe

    from mpl_toolkits.mplot3d import Axes3D  # noqa: F401

    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1, projection="3d")

    ax.plot_wireframe(x, y, z, linewidths=0.5)
    ax.set_xlim([-a - 0.1, a + 0.1])
    ax.set_ylim([-a - 0.1, a + 0.1])
    ax.set_zlim([-b - 0.1, b + 0.1])
    ax.view_init(90, 0)

    # NOTE: hides most of the z axis
    ax.zaxis.line.set_lw(0.0)
    ax.set_zticks([])

    filename = filenamer.with_suffix("shtns_surface_wireframe")
    fig.savefig(filename)
    # plt.show()
    logger.info("output: %s", filename)

    # }}}


# }}}


# {{{ test_spharm_construction


@pytest.mark.parametrize("target_order", [3, 4])
def test_spharm_construction(actx_factory, target_order, visualize):
    actx = get_cl_array_context(actx_factory)

    # {{{ setup

    lmax = 11
    mmax = lmax

    case = etd.SPHSphereTestCase(
        target_order=target_order,
        group_factory_cls=mpoly.InterpolatoryEquidistantGroupFactory,
        # resolutions=[(i + 8, i) for i in range(32, 88, 8)],
        mesh_arguments={
            "use_default_mesh_unit_nodes": False,
            "mesh_unit_nodes": False,
            "radius": 1.0,
            "lmax": lmax,
            "mmax": mmax,
        },
    )

    logger.info("\n%s", case)

    # }}}

    # {{{ geometry

    nlon, nlat = case.resolutions[0] + 4, case.resolutions[0]
    logger.info("nlon %d nlat %d lmax %d", nlon, nlat, lmax)

    discr = case.get_discretization(actx, (nlon, nlat))

    logger.info("ndofs:     %d", discr.ndofs)
    logger.info("nelements: %d", discr.mesh.nelements)

    assert len(discr.specgroups) == 1
    grp = discr.specgroups[0]

    phi, theta = np.meshgrid(grp.phi, grp.theta)
    x, y, z = case.mesh_generator.surface_fn(theta, phi)
    nodes = actx.thaw(discr.nodes())

    # }}}

    # {{{ check roundtrip

    def _to_spectral_error(ary_spat, ary_discr):
        xlm = grp.sh.analys(ary_spat)
        ylm = discr.to_spectral_conn(ary_discr)[0].squeeze()

        return la.norm(xlm - actx.to_numpy(ylm)) / la.norm(xlm)

    error_x = _to_spectral_error(x, nodes[0])
    error_y = _to_spectral_error(y, nodes[1])
    error_z = _to_spectral_error(z, nodes[2])
    logger.info("error[spec]: x %.5e y %.5e z %.5e", error_x, error_y, error_z)
    assert error_x < 2.0e-15, error_x
    assert error_y < 2.0e-15, error_y
    assert error_z < 2.0e-15, error_z

    def _from_spectral_error(ary):
        ary_roundtrip = discr.from_spectral_conn(discr.to_spectral_conn(ary))

        ary = actx.to_numpy(flatten(ary, actx))
        ary_roundtrip = actx.to_numpy(flatten(ary_roundtrip, actx))

        return la.norm(ary - ary_roundtrip, np.inf) / la.norm(ary, np.inf)

    error_x = _from_spectral_error(nodes[0])
    error_y = _from_spectral_error(nodes[1])
    error_z = _from_spectral_error(nodes[2])
    logger.info("error[trip]: x %.5e y %.5e z %.5e", error_x, error_y, error_z)
    assert error_x < 1.0e-14, error_x
    assert error_y < 1.0e-14, error_y
    assert error_z < 1.0e-14, error_z

    # }}}

    # {{{ check normal orientation

    normal = bind(discr, sym.normal(discr.ambient_dim).as_vector())(actx)
    n_dot_x = actx.to_numpy(flatten(normal @ nodes, actx))
    assert (n_dot_x > 0.0).all()

    # }}}

    if not visualize:
        return

    nodes_roundtrip = make_obj_array([
        discr.from_spectral_conn(discr.to_spectral_conn(nodes[i]))
        for i in range(discr.ambient_dim)
    ])
    error = make_obj_array([
        actx.np.log10(actx.np.fabs(xi - yi) + 1.0e-16)
        for xi, yi in zip(nodes, nodes_roundtrip, strict=True)
    ])

    filename = filenamer.with_suffix(f"construction_error_{target_order:02d}")

    from pystopt.visualization import make_visualizer

    vis = make_visualizer(actx, discr, vis_order=target_order)

    vis.write_file(
        filename,
        [("normal", normal), ("error", error), ("error_x", error[0]), ("x", nodes)],
        overwrite=True,
    )


# }}}


# {{{ test_spharm_interpolation_error


@pytest.mark.parametrize(
    "group_factory_cls",
    [
        mpoly.InterpolatoryQuadratureGroupFactory,
        # mpoly.InterpolatoryEdgeClusteredGroupFactory,
    ],
)
@pytest.mark.parametrize("target_order", [3, 4])
@pytest.mark.parametrize("resolution", [24, 48])
def test_spharm_interpolation_error(
    actx_factory,
    group_factory_cls,
    target_order,
    resolution,
    visualize,
):
    actx = get_cl_array_context(actx_factory)

    spharm_m = 0
    spharm_n = 5

    mesh_order = target_order
    case = etd.SPHSphereTestCase(
        target_order=target_order,
        group_factory_cls=group_factory_cls,
        mesh_arguments={
            "mesh_order": mesh_order,
            "mesh_unit_nodes": True,
            "use_default_mesh_unit_nodes": True,
        },
    )

    mesh_arguments = case.mesh_arguments.copy()
    mesh_arguments["use_default_mesh_unit_nodes"] = False
    equi = replace(
        case,
        group_factory_cls=mpoly.InterpolatoryEquidistantGroupFactory,
        mesh_arguments=mesh_arguments,
    )

    logger.info("%s\n", case)

    # {{{ geometry

    resolution = case.resolutions[-1] if resolution is None else resolution
    discr = case.get_discretization(actx, resolution)
    equi_discr = equi.get_discretization(actx, resolution)

    logger.info("ndofs:     %d", discr.ndofs)
    logger.info("nelements: %d", discr.mesh.nelements)

    # }}}

    # {{{ get values to check

    name = "spharm"
    if name == "spharm":
        from pystopt.mesh.spharm import (
            evaluate_spharm_from_coordinates,
            evaluate_spharm_from_index,
        )

        x = evaluate_spharm_from_coordinates(actx, discr, m=spharm_m, n=spharm_n)
        equi_x = evaluate_spharm_from_index(actx, equi_discr, spharm_m, spharm_n)

        y = evaluate_spharm_from_index(actx, discr, m=spharm_m, n=spharm_n)
        error = actx.to_numpy(dof_array_rnorm(x, y, ord=np.inf))
        logger.info("error %.5e", error)
    elif name == "coords":
        x = actx.thaw(discr.nodes()[0])
        equi_x = actx.thaw(equi_discr.nodes()[0])
    else:
        raise ValueError(name)

    # }}}

    # {{{ compute spherical harmonic coefficients

    xlm = discr.to_spectral_conn(x)
    equi_xlm = equi_discr.to_spectral_conn(equi_x)

    h = 2.0 / resolution
    h_error_estimate = h ** (mesh_order + 1)

    error = actx.to_numpy(dof_array_rnorm(xlm, equi_xlm, ord=np.inf))
    logger.info("h %.5e error %.5e %.5e", h, error, h_error_estimate)

    # }}}

    # {{{ plot

    if not visualize:
        return

    basename = replace(
        filenamer,
        stem=(
            f"{filenamer.stem}_interp_{resolution}_{target_order}_"
            f"y_{spharm_m}_{spharm_n}"
        ),
    )
    name = group_factory_cls.__name__[:-12]

    # {{{ spharm

    from pystopt.mesh.spharm import visualize_spharm_modes

    filename = basename.with_suffix(name)
    visualize_spharm_modes(filename, discr, [("x", xlm)], mode="imshow", overwrite=True)

    filename = basename.with_suffix("equidistant")
    visualize_spharm_modes(
        filename, equi_discr, [("x", equi_xlm)], mode="imshow", overwrite=True
    )

    filename = basename.with_suffix(f"{name}_error")
    visualize_spharm_modes(
        filename, discr, [("Error", xlm - equi_xlm)], mode="byorder", overwrite=True
    )

    # }}}

    # {{{ physical

    from pystopt.visualization import make_visualizer

    vis = make_visualizer(actx, discr, vis_order=target_order)
    vis.write_file(
        basename.with_suffix("geometry"),
        [
            ("f", x),
            # ("fidx", y),
            # ("theta", theta), ("phi", phi),
        ],
        overwrite=True,
    )

    # }}}

    # }}}


# }}}


# {{{ test_spharm_interpolation_error_estimate


@pytest.mark.parametrize(
    "group_factory_cls",
    [
        mpoly.InterpolatoryQuadratureGroupFactory,
        # mpoly.InterpolatoryEdgeClusteredGroupFactory,
    ],
)
@pytest.mark.parametrize("target_order", [3, 4])
def test_spharm_interpolation_error_estimate(
    actx_factory, group_factory_cls, target_order, visualize
):
    actx = get_cl_array_context(actx_factory)

    case = etd.SPHUrchinTestCase(
        target_order=target_order,
        resolutions=[24, 32, 40, 48, 64],
        group_factory_cls=group_factory_cls,
        mesh_arguments={"use_default_mesh_unit_nodes": True},
    )
    equi = replace(
        case,
        group_factory_cls=mpoly.InterpolatoryEquidistantGroupFactory,
    )

    iaxis = 0

    # {{{ convergence

    from pystopt.measure import EOCRecorder, stringify_eoc

    eoc_interp = EOCRecorder(name="interp", expected_order=target_order + 1)
    eoc_unique = EOCRecorder(name="unique", expected_order=target_order + 1)
    eoc_smodes = EOCRecorder(name="smodes", expected_order=target_order + 1)
    eoc_estimate = EOCRecorder(name="Estimate")

    for r in case.resolutions:
        # {{{ geometry

        discr = case.get_discretization(actx, r).copy(xlm=None)
        equi_discr = equi.get_discretization(actx, r).copy(xlm=None)

        from pystopt.mesh.connection import make_same_layout_connection

        to_equi_conn = make_same_layout_connection(actx, equi_discr, discr)
        to_unique_conn = discr.to_spectral_conn.connections[0]

        # }}}

        # {{{ functions

        x = actx.thaw(discr.nodes()[iaxis])
        xlm = discr.to_spectral_conn(x)

        equi_x = actx.thaw(equi_discr.nodes()[iaxis])
        equi_xlm = equi_discr.to_spectral_conn(equi_x)

        h = actx.to_numpy(bind(discr, sym.h_min_from_volume(discr.ambient_dim))(actx))
        h = 2.0 / np.sqrt(discr.mesh.nelements)

        # }}}

        # {{{ errors

        f_ref = equi_x
        f = to_equi_conn(x)
        error_interp = actx.to_numpy(dof_array_rnorm(f, f_ref))
        eoc_interp.add_data_point(h, error_interp)

        f_ref = equi_discr.to_spectral_conn.connections[0](equi_x)
        f = to_unique_conn(x)
        error_unique = actx.to_numpy(dof_array_rnorm(f, f_ref))
        eoc_unique.add_data_point(h, error_unique)

        f_ref = equi_xlm
        f = xlm
        error_smodes = actx.to_numpy(dof_array_rnorm(xlm, equi_xlm))
        eoc_smodes.add_data_point(h, error_smodes)

        h_error_estimate = h ** (target_order + 1)
        eoc_estimate.add_data_point(h, h_error_estimate)

        # }}}

        logger.info(
            "ndofs %5d h %.5e interp %.5e unique %.5e modes %.5e estimate %.5e",
            discr.ndofs,
            h,
            error_interp,
            error_unique,
            error_smodes,
            h_error_estimate,
        )

    logger.info("\n%s", stringify_eoc(eoc_interp, eoc_unique, eoc_smodes, eoc_estimate))

    assert eoc_interp.satisfied()
    assert eoc_unique.satisfied()
    assert eoc_smodes.satisfied()

    # }}}

    order_estimate = eoc_estimate.order_estimate()
    order_smodes = eoc_smodes.order_estimate()
    assert abs(order_estimate - order_smodes) < 1.0

    if not visualize:
        return

    basename = replace(filenamer, stem=f"{filenamer.stem}_estimate_{target_order}")
    name = group_factory_cls.__name__[:-12]

    from pystopt.measure import visualize_eoc

    visualize_eoc(
        basename.with_suffix(f"{name}_order"),
        eoc_interp,
        eoc_unique,
        eoc_smodes,
        eoc_estimate,
        overwrite=True,
    )


# }}}


# {{{ test_spharm_qbx_errors


@pytest.mark.slowtest
@pytest.mark.parametrize("qbx_order", [3, 5, 7])
@pytest.mark.parametrize("source_ovsmp", [3, 6, 9])
def test_spharm_qbx_error(actx_factory, qbx_order, source_ovsmp, visualize):
    actx = get_cl_array_context(actx_factory)

    viscosity_ratio = 1.0
    capillary_number = 1.0

    case_name = "sphere"
    if case_name == "sphere":
        cls = etd.SPHSphereTestCase
    elif case_name == "spheroid":
        cls = etd.SPHSpheroidTestCase
    else:
        raise ValueError(case_name)

    target_order = 3
    case = cls(
        target_order=target_order,
        qbx_order=qbx_order,
        source_ovsmp=source_ovsmp,
    )

    mesh_arguments = case.mesh_arguments.copy()
    mesh_arguments["use_default_mesh_unit_nodes"] = False
    equi_case = replace(
        case,
        group_factory_cls=mpoly.InterpolatoryEquidistantGroupFactory,
        mesh_arguments=mesh_arguments,
    )

    # {{{ geometry

    resolution = case.resolutions[-1]
    discr = equi_case.get_discretization(actx, resolution)

    from pytential.target import PointsTarget

    points = discr.from_spectral_conn.connections[0](actx.thaw(discr.xlm))
    points = PointsTarget(actx.freeze(make_obj_array([p[0].ravel() for p in points])))

    from pytential import GeometryCollection

    qbx = case.get_layer_potential(
        actx,
        resolution,
        target_association_tolerance=0.05,
    )

    places = GeometryCollection(
        {
            case.name: qbx,
            "target": discr,
            "points": points,
        },
        auto_where=case.name,
    )
    source_dd = places.auto_source
    target_dd = sym.as_dofdesc("target")
    qbx_target_dd = sym.as_dofdesc(case.name)

    density_discr = places.get_discretization(source_dd.geometry)
    logger.info("ndofs:     %d", density_discr.ndofs)
    logger.info("nelements: %d", density_discr.mesh.nelements)

    (sgrp,) = density_discr.specgroups
    logger.info("lmax %4d mmax %4d nlm %4d", sgrp.sh.lmax, sgrp.sh.mmax, sgrp.sh.nlm)

    # }}}

    # {{{ optim

    from pystopt.cost import NormalVelocityFunctional

    cost = NormalVelocityFunctional(
        u=sym.make_sym_vector("u", places.ambient_dim),
        ud=0,
    )

    from pystopt import stokes

    taylor = stokes.Taylor(
        radius=1,
        alpha=1,
        capillary_number_name="ca",
        viscosity_ratio_name="viscosity_ratio",
    )
    op = stokes.TwoPhaseSingleLayerStokesRepresentation(taylor.farfield)

    from pystopt.simulation.staticstokes import make_stokes_shape_optimization_wrangler

    wrangler = make_stokes_shape_optimization_wrangler(
        places,
        cost,
        op,
        viscosity_ratio=viscosity_ratio,
        capillary_number=capillary_number,
        context={},
        is_spectral=False,
    )

    # }}}

    # {{{ values

    if qbx_target_dd == source_dd:
        qbx_forced_limit = {+1: +1, "avg": "avg"}
    else:
        qbx_forced_limit = {+1: None, "avg": None}

    from pystopt.stokes import single_layer_solve

    solution = single_layer_solve(
        actx,
        places,
        op,
        lambdas=wrangler.lambdas,
        sources=[source_dd.geometry],
        context=wrangler.context,
        gmres_arguments={"callback": True},
    )

    # velocity
    taylor_u = bind(places, stokes.sti.velocity(taylor, side=+1), auto_where=target_dd)(
        actx, **wrangler.context
    )
    u = stokes.sti.velocity(
        solution,
        side=+1,
        qbx_forced_limit=qbx_forced_limit[+1],
        dofdesc=qbx_target_dd,
    )

    # hessian
    taylor_hess = bind(
        places,
        0.5
        * (
            stokes.sti.normal_velocity_hessian(taylor, side=+1)
            + stokes.sti.normal_velocity_hessian(taylor, side=-1)
        ),
        auto_where=target_dd,
    )(actx, **wrangler.context)
    # hess = stokes.sti.normal_velocity_hessian(
    #         solution, side=None, qbx_forced_limit=qbx_forced_limit["avg"],
    #         dofdesc=qbx_target_dd)

    # }}}

    # {{{ visualize

    if not visualize:
        return

    from pystopt.mesh.spharm import visualize_spharm_modes

    order = f"order_{target_order:02d}_qbx_{qbx_order:02d}_ovsmp_{source_ovsmp:02d}"
    basename = filenamer.prepend(f"qbx_error_{order}_{case_name}")

    names_and_fields = [
        # qbx + quadrature
        ("u_x", u[0], qbx_target_dd),
        ("u_y", u[1], qbx_target_dd),
        # (r"hess_u", hess, qbx_target_dd),
        # taylor + equidistant
        ("ut_x", taylor_u[0], target_dd),
        ("ut_y", taylor_u[1], target_dd),
        (r"hess_ut", taylor_hess, target_dd),
    ]

    from meshmode.discretization import Discretization
    from meshmode.dof_array import DOFArray

    for name, field, dd in names_and_fields:
        r_discr = places.get_discretization(dd.geometry, dd.discr_stage)
        if isinstance(r_discr, Discretization):
            r_field = field
        else:
            r_discr = discr
            r_field = discr.to_spectral_conn.connections[-1](
                DOFArray(actx, (field.reshape(-1, 1),))
            )

        filename = basename.with_suffix(f"{name}")
        visualize_spharm_modes(
            filename, r_discr, [(name, r_field)], mode="imshow", overwrite=True
        )

        # filename = basename.with_suffix(f"{name}_line")
        # with subplots(filename, nrows=1, ncols=2, overwrite=True) as fig:
        #     visualize_spharm_modes(
        #             fig, discr, [(name, field)],
        #             mode="linen", overwrite=True)

    # }}}


# }}}


# {{{ test_spharm_visualize_modes


def test_spharm_visualize_modes(actx_factory, visualize):
    pytest.importorskip("matplotlib")
    actx = get_cl_array_context(actx_factory)

    # {{{ geometry

    case = etd.SPHSphereTestCase()
    logger.info("\n%s", case)

    discr = case.get_discretization(actx, case.resolutions[-1])

    # }}}

    from pystopt.mesh.spharm import visualize_spharm_modes

    f = actx.thaw(discr.xlm[0])

    # NOTE: this used to blow up memory for some reason, keep it just in case
    filename = filenamer.with_suffix("visualize_modes_imshow")
    for _ in range(12):
        visualize_spharm_modes(
            filename, discr, [("f", f)], mode="imshow", overwrite=True
        )

    filename = filenamer.with_suffix("visualize_modes_bydegree")
    for _ in range(12):
        visualize_spharm_modes(
            filename, discr, [("f", f)], mode="bydegree", overwrite=True
        )

    filename = filenamer.with_suffix("visualize_modes_byorder")
    for _ in range(12):
        visualize_spharm_modes(
            filename, discr, [("f", f)], mode="byorder", overwrite=True
        )


# }}}


# {{{ test_spherical_angles


@pytest.mark.parametrize(
    ("cls", "offset"),
    [
        (etd.SPHSphereTestCase, np.array([0.0, 0.0, 0.0])),
        (etd.SPHSphereTestCase, np.array([1.0, 0.0, 0.0])),
        (etd.SPHSphereTestCase, np.array([0.0, 0.0, 1.0])),
    ],
)
def test_spherical_angles(actx_factory, cls, offset, visualize):
    actx = get_cl_array_context(actx_factory)
    case = cls(mesh_arguments={"offset": offset, "radius": 1.0})

    from pytential import GeometryCollection

    discr = case.get_discretization(actx, case.resolutions[-1])
    places = GeometryCollection(discr)

    sym_theta, sym_phi = sym.spherical_angles(discr.ambient_dim)
    theta = bind(places, sym_theta)(actx)
    phi = bind(places, sym_phi)(actx)

    xo = actx.thaw(discr.nodes())
    xa = make_obj_array([
        offset[0] + actx.np.sin(theta) * actx.np.cos(phi),
        offset[1] + actx.np.sin(theta) * actx.np.sin(phi),
        offset[2] + actx.np.cos(theta),
    ])

    error = actx.to_numpy(dof_array_rnorm(xa, xo))
    logger.info("error: %.12e", error)


# }}}


if __name__ == "__main__":
    import sys

    import pyopencl as cl  # noqa: F401

    if len(sys.argv) > 1:
        exec(sys.argv[1])
    else:
        pytest.main([__file__])
