# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

from functools import singledispatch

import numpy as np
import pytest

from pytential import GeometryCollection
from pytools.obj_array import make_obj_array

import pystopt.stokes as stk
import pystopt.stokes.boundary_conditions as sbc
from pystopt import bind, sym
from pystopt.tools import (
    get_cl_array_context,
    get_default_logger,
    pytest_generate_tests_for_array_contexts,
)

import extra_test_data as etd

logger = get_default_logger(__file__)
filenamer = etd.get_filename_generator(__file__)
pytest_generate_tests = pytest_generate_tests_for_array_contexts([
    "pystopt.pyopencl",
])


# {{{ test_gradient_fourier_modes


def _make_geometry_wrangler(
    places: GeometryCollection,
    *,
    dofdesc: sym.DOFDescriptor,
):
    from pystopt.cost import GeometryTrackingFunctional

    cost = GeometryTrackingFunctional(
        xd=make_obj_array([0, 0, 0][: places.ambient_dim]),
        t=sym.var("t"),
        dofdesc=dofdesc,
        allow_tangential_gradient=True,
    )

    from pystopt.simulation.unconstrained import (
        make_unconstrained_shape_optimization_wrangler,
    )

    return make_unconstrained_shape_optimization_wrangler(
        places, cost, dofdesc=dofdesc, is_spectral=False
    )


def _make_stokes_wrangler(
    places: GeometryCollection,
    *,
    dofdesc: sym.DOFDescriptor,
    viscosity_ratio: float = 1.0,
    capillary_number: float = 0.1,
):
    from pystopt.cost import NormalVelocityFunctional

    cost = NormalVelocityFunctional(
        u=sym.make_sym_vector("u", places.ambient_dim),
        ud=0,
    )

    bc = stk.get_extensional_farfield(
        places.ambient_dim, alpha=1.0, capillary_number_name="ca"
    )
    op = stk.TwoPhaseSingleLayerStokesRepresentation(bc)

    from pystopt.simulation.staticstokes import make_stokes_shape_optimization_wrangler

    return make_stokes_shape_optimization_wrangler(
        places,
        cost,
        op,
        viscosity_ratio=viscosity_ratio,
        capillary_number=capillary_number,
        dofdesc=dofdesc,
        is_spectral=False,
    )


@pytest.mark.slowtest
@pytest.mark.parametrize(
    "cost_name",
    [
        "geometry",
        # "stokes",
    ],
)
@pytest.mark.parametrize("target_order", [3, 5])
def test_gradient_fourier_modes(actx_factory, cost_name, target_order, visualize):
    actx = get_cl_array_context(actx_factory)
    case = etd.FourierCircleTestCase(
        target_order=target_order, mesh_arguments={"rfft": False}
    )

    # {{{ geometry

    resolution = case.resolutions[2]
    sigma = 1.0e-1

    qbx = case.get_layer_potential(actx, resolution)
    places = GeometryCollection(qbx, auto_where=case.name)
    dofdesc = places.auto_source

    density_discr = places.get_discretization(case.name)
    logger.info("ndofs:     %d", density_discr.ndofs)
    logger.info("nelements: %d", density_discr.mesh.nelements)

    # }}}

    # {{{ optimization

    if cost_name == "geometry":
        wrangler = _make_geometry_wrangler(places, dofdesc=dofdesc)
    elif cost_name == "stokes":
        wrangler = _make_stokes_wrangler(places, dofdesc=dofdesc)
    else:
        raise ValueError(f"unknown cost name: '{cost_name}'")

    state = wrangler.get_initial_state(actx)
    grad_ad = bind(places, sym.n_dot(places.ambient_dim), auto_where=dofdesc)(
        actx, x=state.evaluate_shape_gradient()
    )

    from pystopt.finite import compare_finite_difference_shape_gradient

    result = compare_finite_difference_shape_gradient(
        state, eps=1.0e-3, sigma=sigma, grad=grad_ad
    )

    # }}}

    # {{{ look at fourier modes

    if not visualize:
        return

    h_max = 2.0 / resolution
    error_h_max = h_max ** (case.target_order + 1)
    k_max = np.arange(0, density_discr.specgroups[0].noutputs // 2 + 1, resolution)

    from dataclasses import replace

    basename = replace(
        filenamer,
        stem=(
            f"{filenamer.stem}_{cost_name}_fourier_gradient_{resolution}_{target_order}"
        ),
    )

    from pystopt.visualization import make_visualizer

    vis = make_visualizer(actx, density_discr)
    vis.write_file(
        basename.with_suffix("value"),
        [
            ("g", grad_ad),
            ("g_{AD}", result.grad_ad),
            ("g_{FD}", result.grad_fd),
        ],
        markers=[":", "-", "--"],
        overwrite=True,
    )

    from pystopt.mesh.fourier import visualize_fourier_modes
    from pystopt.visualization.matplotlib import subplots

    filename = basename.with_suffix("adjoint")
    with subplots(filename, overwrite=True) as fig:
        visualize_fourier_modes(
            fig,
            actx,
            density_discr,
            [
                ("g", grad_ad),
                (r"\hat{g}", result.grad_ad),
            ],
            overwrite=True,
        )

        ax = fig.gca()
        ax.axhline(y=error_h_max, ls=":", color="k")

        for k in k_max:
            ax.axvline(x=k, color="k")
            ax.axvline(x=-k, color="k")

    filename = basename.with_suffix("finite")
    with subplots(filename, overwrite=True) as fig:
        visualize_fourier_modes(
            fig,
            actx,
            density_discr,
            [
                (r"\hat{g}", result.grad_fd),
            ],
            overwrite=True,
        )

        ax = fig.gca()
        ax.axhline(y=error_h_max, ls=":", color="k")

        for k in k_max:
            ax.axvline(x=k, color="k")
            ax.axvline(x=-k, color="k")

    # }}}


# }}}


# {{{ test_stokes_finite_difference_gradient


@pytest.mark.slowtest
@pytest.mark.parametrize("ambient_dim", [2, 3])
@pytest.mark.parametrize("viscosity_ratio", [1])
@pytest.mark.parametrize("capillary_number", [1.0])
def test_stokes_finite_difference_gradient(
    actx_factory, ambient_dim, viscosity_ratio, capillary_number, visualize
):
    actx = get_cl_array_context(actx_factory)

    # {{{ parameters

    kwargs = {
        "target_order": 3,
        "qbx_order": 4,
        "source_ovsmp": 3,
        "fmm_order": 10,
    }
    gamma = 1

    if ambient_dim == 2:
        resolution = 512

        epsilons = 10.0 ** (-np.arange(2, 8))
        sigma = 5.0e-2
    elif ambient_dim == 3:
        resolution = 64

        epsilons = 10.0 ** (-np.arange(1, 6))
        sigma = 7.5e-1
    else:
        raise ValueError(f"unsupported dimension: {ambient_dim}")

    # }}}

    # {{{ geometry

    if ambient_dim == 2:
        case = etd.FourierEllipseTestCase(**kwargs)
    else:
        case = etd.SPHSphereTestCase(**kwargs)

    qbx = case.get_layer_potential(actx, resolution)
    places = GeometryCollection(qbx, auto_where=case.name)
    dofdesc = places.auto_source

    density_discr = places.get_discretization(dofdesc.geometry)
    logger.info("nelements:     %d", density_discr.mesh.nelements)
    logger.info("ndofs:         %d", density_discr.ndofs)
    logger.info("nspec:         %d", density_discr.nspec)

    # }}}

    # {{{ stokes

    bc = stk.get_extensional_farfield(
        places.ambient_dim, alpha=1.0, axis=1, capillary_number_name="ca"
    )
    op = stk.TwoPhaseSingleLayerStokesRepresentation(bc)

    from pystopt.cost import NormalVelocityFunctional, VolumeTrackingFunctional

    sym_u = sym.make_sym_vector("u", places.ambient_dim)
    cost = gamma * NormalVelocityFunctional(u=sym_u, ud=0, dofdesc=dofdesc) + (
        1 - gamma
    ) * VolumeTrackingFunctional(vd=sym.var("vd"))
    context = {"vd": np.pi}

    from pystopt.simulation.staticstokes import make_stokes_shape_optimization_wrangler

    wrangler = make_stokes_shape_optimization_wrangler(
        places,
        cost,
        op,
        viscosity_ratio=viscosity_ratio,
        capillary_number=capillary_number,
        is_spectral=False,
        dofdesc=dofdesc,
        context=context,
    )
    state = wrangler.get_initial_state(actx)

    # }}}

    # {{{ finite difference comparison

    grad = state.evaluate_shape_gradient()
    grad_dot_n = bind(places, sym.n_dot(places.ambient_dim), auto_where=dofdesc)(
        actx, x=grad
    )

    if visualize:
        from pystopt.visualization import make_visualizer

        vis = make_visualizer(actx, density_discr)
        vis.write_file(
            filenamer.with_suffix(f"gradient_{case.name}"),
            [
                ("g", grad),
                ("g_dot_n", grad_dot_n),
            ],
            overwrite=True,
        )

    from pystopt.finite import (
        compare_finite_difference_shape_gradient,
        generate_random_dofs,
    )
    from pystopt.visualization.matplotlib import subplots

    if ambient_dim == 2:
        rng = np.random.default_rng(72)
        indices = generate_random_dofs(density_discr, 5, rng=rng)
    else:
        # FIXME: not the happiest situation, but it seems some points don't
        # converge nicely in 3D, so this just hard codes a couple
        indices = [
            (0, 3652, 2),
            (0, 3574, 9),
            (0, 2723, 7),
            (0, 1614, 3),
            (0, 322, 1),
        ]

    from pystopt.measure import EOCRecorder, stringify_eoc

    eocs = [EOCRecorder(name=f"{iel:04d}_{idof:04d}") for _, iel, idof in indices]
    eoc = EOCRecorder()

    results = []
    for epsilon in epsilons:
        r = compare_finite_difference_shape_gradient(
            state,
            grad=grad_dot_n,
            eps=epsilon,
            sigma=sigma,
            indices=indices,
        )

        grad_ad = actx.to_numpy(r.grad_ad)
        grad_fd = actx.to_numpy(r.grad_fd)
        results.append(grad_fd)

        for i in range(grad_ad.size):
            error = abs(grad_ad[i] - grad_fd[i]) / abs(grad_fd[i])
            eocs[i].add_data_point(epsilon, error)

        logger.info("error: %s", abs(grad_ad - grad_fd) / abs(grad_fd))
        eoc.add_data_point(epsilon, r.error)

    logger.info("\n%s", stringify_eoc(*eocs))
    logger.info("\n%s", stringify_eoc(eoc))

    from dataclasses import replace

    suffix = f"{case.name}_vr_{viscosity_ratio:02.2f}_ca_{capillary_number:02.2f}"
    filename = replace(filenamer, ext=".npz", suffix=f"convergence_{suffix}")

    np.savez(
        filename,
        viscosity_ratio=viscosity_ratio,
        capillary_number=capillary_number,
        ambient_dim=ambient_dim,
        resolution=resolution,
        indices=indices,
        grad_fd=results,
        grad_ad=grad_ad,
        epsilons=epsilons,
        sigma=sigma,
    )

    # }}}

    if not visualize:
        return

    from pystopt.measure import visualize_eoc

    filename = filenamer.with_suffix(f"convergence_{case.name}")
    visualize_eoc(
        filename,
        eoc,
        order=1,
        abscissa=r"\epsilon",
        enable_legend=False,
        overwrite=True,
    )

    filename = filenamer.with_suffix(f"gradient_{case.name}_error")
    with subplots(filename, overwrite=True) as fig:
        ax = fig.gca()

        for grad_fd in results:
            ax.semilogy(grad_fd - grad_ad, "o-")
        # ax.plot(grad_ad, "v-", color="k")

        ax.set_ylabel(r"$\nabla J$")


# }}}


# {{{ test_boundary_condition_gradient


@singledispatch
def get_perturbed_bc_kwargs(bc, *, eps):
    raise NotImplementedError


@get_perturbed_bc_kwargs.register(sbc.UniformFlowBoundaryCondition)
def _get_perturbed_uniform(bc, *, eps: float = 1.0e-6):
    for i in range(bc.ambient_dim):
        uinf = bc.uinf.copy()
        uinf[i] += eps
        yield {"uinf": uinf}


@get_perturbed_bc_kwargs.register(sbc.SolidBodyRotationBoundaryCondition)
def _get_perturbed_sbr(bc, *, eps: float = 1.0e-6):
    if bc.ambient_dim == 2:
        yield {"omega": bc.omega + eps}
    else:
        for i in range(bc.ambient_dim):
            omega = bc.omega.copy()
            omega[i] += eps
            yield {"omega": omega}


@get_perturbed_bc_kwargs.register(sbc.HelicalFlowBoundaryCondition)
def _get_perturbed_helix(bc, *, eps: float = 1.0e-6):
    yield {"omega": bc.omega + eps, "height": bc.height}
    yield {"omega": bc.omega, "height": bc.height + eps}


@pytest.mark.parametrize(
    ("ambient_dim", "name"),
    [
        (2, "uniform"),  # (3, "uniform"),
        (2, "solid_body_rotation"),
        (3, "solid_body_rotation"),
        # (2, "bezier_uniform"), (3, "bezier_uniform"),
        (3, "helix"),
    ],
)
def test_boundary_condition_gradient(actx_factory, ambient_dim, name, visualize):
    actx = get_cl_array_context(actx_factory)

    # {{{ geometry

    kwargs = {}
    if ambient_dim == 2:
        case = etd.FourierStarfishTestCase(**kwargs)
    else:
        case = etd.SPHUFOTestCase(**kwargs)

    qbx = case.get_layer_potential(actx, case.resolutions[-1])
    places = GeometryCollection(qbx, auto_where=case.name)
    dofdesc = places.auto_source

    density_discr = places.get_discretization(dofdesc.geometry)
    logger.info("nelements:     %d", density_discr.mesh.nelements)
    logger.info("ndofs:         %d", density_discr.ndofs)
    logger.info("nspec:         %d", density_discr.nspec)

    # }}}

    # {{{ setup up operators

    if name == "uniform":
        sym_bc_kwargs = {"uinf": sym.make_sym_vector("uinf", ambient_dim)}
        bc_kwargs = {"uinf": np.array([1.2, 1.5, 4.3][:ambient_dim])}
    elif name == "solid_body_rotation":
        if ambient_dim == 2:
            sym_bc_kwargs = {"omega": sym.var("omega")}
            bc_kwargs = {"omega": np.pi}
        else:
            sym_bc_kwargs = {"omega": sym.make_sym_vector("omega", ambient_dim)}
            bc_kwargs = {"omega": np.array([1 + np.pi, np.pi / 7, np.pi**2])}
    elif name == "helix":
        sym_bc_kwargs = {
            "omega": sym.var("omega"),
            "height": sym.var("height"),
            "tmax": 1.0,
        }
        bc_kwargs = {"omega": 3.4 * np.pi, "height": 1.7, "tmax": 1.0}
    else:
        raise ValueError(f"unknown bc: '{name}'")

    from pystopt.cost import NormalVelocityFunctional

    cost = NormalVelocityFunctional(
        u=sym.make_sym_vector("u", places.ambient_dim),
        ud=0,
    )

    bc = stk.get_farfield_boundary_from_name(
        places.ambient_dim, name, capillary_number_name="ca", **sym_bc_kwargs
    )
    op = stk.TwoPhaseSingleLayerStokesRepresentation(bc)

    bcstar = stk.make_unsteady_adjoint_boundary_conditions(
        places.ambient_dim, cost, xstar_name="xstar"
    )
    ad = stk.TwoPhaseSingleLayerStokesRepresentation(bcstar)

    import pystopt.operators as ops

    q = ops.make_sym_density(op, "q")
    qstar = ops.make_sym_density(ad, "qstar")

    from arraycontext import flatten

    nodes = sym.nodes(ambient_dim).as_vector()
    normal = sym.normal(ambient_dim).as_vector()
    kappa = sym.summed_curvature(ambient_dim)
    xstar = bind(places, np.pi + sum(nodes) ** 2 + sum(normal) / (1 + kappa**2))(actx)

    from pystopt.dof_array import dof_array_norm

    logger.info("xstar: %.12e", actx.to_numpy(dof_array_norm(xstar)))

    # }}}

    # {{{ check against finite difference

    def fun(bc):
        from pystopt.stokes import sti

        sym_xstar_uinf = sym.sintegral(
            sym.var("xstar") * sym.n_dot(sti.velocity(bc)),
            ambient_dim,
            ambient_dim - 1,
        )

        return actx.to_numpy(
            bind(places, sym_xstar_uinf)(actx, xstar=xstar, **bc_kwargs)
        )

    import pystopt.derivatives as grad

    sym_grad_ad = grad.farfield(bc, op, q, ad, qstar)
    logger.info("grad_ad:\n%s", sym.pretty(sym_grad_ad))

    grad_ad = actx.to_numpy(
        flatten(bind(places, sym_grad_ad)(actx, xstar=xstar, **bc_kwargs), actx)
    )
    logger.info("grad_ad: %s", grad_ad)

    from pystopt.measure import EOCRecorder

    eoc = EOCRecorder()

    for p in range(2, 5):
        eps = 10.0 ** (-p)

        grad_fd = []
        f = fun(bc)
        for perturb_bc_kwargs in get_perturbed_bc_kwargs(bc, eps=eps):
            all_perturb_bc_kwargs = {**bc_kwargs, **perturb_bc_kwargs}

            perturb_bc = stk.get_farfield_boundary_from_name(
                places.ambient_dim,
                name,
                capillary_number_name="ca",
                **all_perturb_bc_kwargs,
            )

            feps = fun(perturb_bc)
            grad_fd.append((feps - f) / eps)
            logger.info("grad_fd: %.12e", grad_fd[-1])

        import numpy.linalg as la

        error = la.norm(grad_ad - grad_fd) / la.norm(grad_fd)
        logger.info("error: %.12e", error)
        # assert error < eps

        eoc.add_data_point(eps, error)

    logger.info("\n%s", eoc)

    # }}}


# }}}


if __name__ == "__main__":
    import sys

    import pyopencl as cl  # noqa: F401

    if len(sys.argv) > 1:
        exec(sys.argv[1])
    else:
        pytest.main([__file__])
