# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

import numpy as np
import pytest

import meshmode.discretization.poly_element as mpoly

from pystopt import bind, sym
from pystopt.dof_array import dof_array_norm, dof_array_rnorm
from pystopt.tools import (
    get_cl_array_context,
    get_default_logger,
    pytest_generate_tests_for_array_contexts,
)

import extra_test_data as etd

logger = get_default_logger(__file__)
filenamer = etd.get_filename_generator(__file__)
pytest_generate_tests = pytest_generate_tests_for_array_contexts([
    "pystopt.pyopencl",
])


# {{{ test_spharm_princial_directions


def test_spharm_princial_directions(actx_factory, visualize):
    actx = get_cl_array_context(actx_factory)

    target_order = 3
    case = etd.SPHSpheroidTestCase(
        target_order=target_order,
        resolutions=[(i + 8, i) for i in range(16, 56, 8)],
        mesh_arguments={
            "group_factory_cls": mpoly.InterpolatoryEquidistantGroupFactory,
            "aspect_ratio": 2.0,
        },
    )

    nelements = case.resolutions[-1]
    discr = case.get_discretization(actx, nelements)

    from pytential import GeometryCollection

    places = GeometryCollection(discr, auto_where=case.name)

    # {{{ compute principal directions

    # NOTE: computed once to warm up the caches for the fundamental forms
    kappa = bind(
        places,
        sym.summed_curvature(places.ambient_dim),
        auto_where=places.auto_source,
    )(actx)

    from pytools import ProcessTimer

    with ProcessTimer() as p:
        kappa1, kappa2 = bind(
            places,
            sym.principal_curvature(places.ambient_dim),
            auto_where=places.auto_source,
        )(actx)
    logger.info("principal_curvature: %s", p)

    with ProcessTimer() as p:
        d1, d2 = bind(
            places,
            sym.extrinsic_principal_directions(places.ambient_dim),
            auto_where=places.auto_source,
        )(actx)
    logger.info("extrinsic_principal_directions: %s", p)

    with ProcessTimer() as p:
        gradn = bind(
            places,
            sym.extrinsic_shape_operator(places.ambient_dim),
            auto_where=places.auto_source,
        )(actx)
    logger.info("extrinsic_shape_operator: %s", p)

    with ProcessTimer() as p:
        q1, q2 = bind(
            places,
            sym.principal_directions(places.ambient_dim),
            auto_where=places.auto_source,
        )(actx)
        actx.queue.finish()
    logger.info("principal_directions: %s", p)

    with ProcessTimer() as p:
        q1, q2 = bind(
            places,
            sym.principal_directions(places.ambient_dim),
            auto_where=places.auto_source,
        )(actx)
        actx.queue.finish()

    logger.info("principal_directions: %s", p)

    form1 = bind(
        places,
        sym.first_fundamental_form(places.ambient_dim),
        auto_where=places.auto_source,
    )(actx)

    # }}}

    # {{{ check principal curvatures

    kappa_err = actx.to_numpy(dof_array_rnorm(kappa1 + kappa2, kappa))
    logger.info("kappa %.5e", kappa_err)
    assert kappa_err < 1.0e-13

    # }}}

    # {{{ check principal direction orthonormality

    q1_dot_q1_err = actx.to_numpy(dof_array_norm(q1 @ (form1 @ q1) - 1))
    q2_dot_q2_err = actx.to_numpy(dof_array_norm(q2 @ (form1 @ q2) - 1))
    q1_dot_q2_err = actx.to_numpy(dof_array_norm(q1 @ (form1 @ q2)))

    logger.info(
        "q1.q1 %.5e q2.q2 %.5e q1.q2 %.5e",
        q1_dot_q1_err,
        q2_dot_q2_err,
        q1_dot_q2_err,
    )
    assert q1_dot_q1_err < 1.0e-13
    assert q2_dot_q2_err < 1.0e-13
    assert q1_dot_q2_err < 1.0e-13

    # }}}

    # {{{ check extrinsic principal direction orthogonality

    d1_dot_d1_err = actx.to_numpy(dof_array_norm(d1 @ d1 - 1, ord=np.inf))
    d2_dot_d2_err = actx.to_numpy(dof_array_norm(d2 @ d2 - 1, ord=np.inf))
    d1_dot_d2_err = actx.to_numpy(dof_array_norm(d1 @ d2, ord=np.inf))

    logger.info(
        "d1.d1 %.5e d2.d2 %.5e d1.d2 %.5e",
        d1_dot_d1_err,
        d2_dot_d2_err,
        d1_dot_d2_err,
    )
    assert d1_dot_d1_err < 1.0e-13
    assert d2_dot_d2_err < 1.0e-13
    assert d1_dot_d2_err < 1.0e-13

    # }}}

    # {{{ check extrinsic shape operator eigenvectors

    gradn_dot_d1_err = actx.to_numpy(
        dof_array_rnorm(gradn @ d1, kappa1 * d1, ord=np.inf)
    )
    gradn_dot_d2_err = actx.to_numpy(
        dof_array_rnorm(gradn @ d2, kappa2 * d2, ord=np.inf)
    )

    # NOTE: gradn is computed differently form the shape operator, so this
    # has a slightly bigger error
    logger.info("gradn.d1 %.5e gradn.d2 %.5e", gradn_dot_d1_err, gradn_dot_d2_err)
    assert gradn_dot_d1_err < 2.0e-12
    assert gradn_dot_d2_err < 5.0e-11

    # }}}

    if not visualize:
        return

    from pystopt.visualization import make_visualizer

    vis = make_visualizer(actx, discr, vis_order=target_order)

    vis.write_file(
        filenamer.with_suffix("principal_directions"),
        [
            ("d1", d1),
            ("d2", d2),
            ("d1d2", actx.np.log10(actx.np.abs(d1 @ d2) + 1.0e-16)),
            ("kappa1", kappa1 * d1),
            ("kappa2", kappa2 * d1),
            ("gradn_d1", gradn @ d1),
            ("gradn_d2", gradn @ d2),
        ],
        overwrite=True,
    )


# }}}


# {{{ test_extrinsic_shape_operator


@pytest.mark.parametrize(
    "cls",
    [
        etd.FourierCircleTestCase,
        etd.FourierEllipseTestCase,
        etd.SPHSpheroidTestCase,
    ],
)
def test_extrinsic_shape_operator(actx_factory, cls, visualize):
    actx = get_cl_array_context(actx_factory)

    target_order = 5
    case = cls(mesh_arguments={"target_order": target_order})

    nelements = case.resolutions[-1]
    discr = case.get_discretization(actx, nelements)

    from pytential import GeometryCollection

    places = GeometryCollection(discr, auto_where=case.name)

    # {{{ check we can recover the summed curvature

    gradn = bind(
        places,
        sym.extrinsic_shape_operator(places.ambient_dim),
        auto_where=places.auto_source,
    )(actx)

    tangents = bind(
        places, sym.tangential_onb(places.ambient_dim), auto_where=places.auto_source
    )(actx)

    kappa = bind(
        places,
        sym.summed_curvature(places.ambient_dim),
        auto_where=places.auto_source,
    )(actx)

    kappa_gradn = sum(t @ (gradn @ t) for t in tangents.T)

    error = actx.to_numpy(dof_array_rnorm(kappa_gradn, kappa))
    logger.info("error: %.12e", error)
    assert error < 1.0e-14

    # }}}


# }}}


if __name__ == "__main__":
    import sys

    import pyopencl as cl  # noqa: F401

    if len(sys.argv) > 1:
        exec(sys.argv[1])
    else:
        pytest.main([__file__])
