# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

from dataclasses import dataclass

import numpy as np
import pytest
import sympy as sp

from pytools.obj_array import make_obj_array

from pystopt import bind, sym
from pystopt.dof_array import dof_array_rnorm
from pystopt.tools import (
    get_cl_array_context,
    get_default_logger,
    pytest_generate_tests_for_array_contexts,
)

import extra_test_data as etd

logger = get_default_logger(__file__)
filenamer = etd.get_filename_generator(__file__)
pytest_generate_tests = pytest_generate_tests_for_array_contexts([
    "pystopt.pyopencl",
])


# {{{ test_sympy_mapper


@pytest.mark.parametrize("ambient_dim", [2, 3])
def test_sympy_mapper(actx_factory, ambient_dim, visualize):
    actx = get_cl_array_context(actx_factory)

    # coordinates
    from pystopt.symbolic.sympy_interop import cart_coords

    x = cart_coords(ambient_dim)
    r = sp.sqrt(sum(x**2))
    theta = sp.atan2(x[1], x[0])

    # function
    psi = sp.simplify(2 / r**2 * sp.cos(theta))
    grad_psi = make_obj_array([psi.diff(x[i]) for i in range(ambient_dim)])
    logger.info("\n%s", "\n".join(f"{i}: {grad_psi[i]}" for i in range(ambient_dim)))

    from pystopt.symbolic.mappers import SympyToPytentialMapper

    psi_ = SympyToPytentialMapper(ambient_dim)(psi)
    grad_psi_ = SympyToPytentialMapper(ambient_dim)(grad_psi)
    logger.info("\n%s", "\n".join(f"{i}: {grad_psi_[i]}" for i in range(ambient_dim)))

    # evaluate
    if ambient_dim == 2:
        discr = etd.FourierCircleTestCase().get_discretization(actx, 128)
    else:
        discr = etd.SPHSphereTestCase().get_discretization(actx, 16)

    psi = bind(discr, psi_)(actx)
    grad_psi = bind(discr, grad_psi_)(actx)

    # plot
    if visualize:
        from pystopt.visualization import make_visualizer

        target_order = 4
        vis = make_visualizer(actx, discr, target_order)

        filename = filenamer.with_suffix("sympy_psi")
        vis.write_file(filename, [(r"\psi", psi)], overwrite=True)

        filename = filenamer.with_suffix("sympy_psi_grad")
        vis.write_file(filename, [(r"\nabla \psi", grad_psi)], overwrite=True)


# }}}


# {{{ test_surface_derivatives


@dataclass(frozen=True)
class _SurfaceConvergenceResult:
    h_max: float
    error_div: float
    error_grd: float
    error_lap: float


def _run_surface_convergence(actx_factory, case, r, *, visualize=False):
    actx = get_cl_array_context(actx_factory)

    # {{{ geometry

    from pytential import GeometryCollection

    discr = case.get_discretization(actx, r)
    places = GeometryCollection(discr, auto_where=case.name)

    ambient_dim = places.ambient_dim
    x = actx.thaw(discr.nodes())

    # }}}

    # {{{ symbolic

    sym_normal = sym.normal(ambient_dim).as_vector()
    sym_kappa = sym.summed_curvature(ambient_dim)

    sym_f = sym.var("f")
    sym_v = sym.make_sym_vector("v", ambient_dim)
    sym_dv = sym.make_sym_vector("dv", ambient_dim**2).reshape(ambient_dim, -1)

    # }}}

    # {{{ test surface divergence

    v = make_obj_array(
        [
            x[0] + actx.np.sin(2.0 * np.pi * x[1]),
            x[1] ** 2,
            (x[2] ** 3 + 2.0) if ambient_dim == 3 else 0.0,
        ][:ambient_dim]
    )
    div_v = 1.0 + 2.0 * x[1] + ((3.0 * x[2] ** 2) if ambient_dim == 3 else 0.0)
    grad_v = (
        make_obj_array([
            1.0,
            2.0 * np.pi * actx.np.cos(2.0 * np.pi * x[1]),
            0.0,
            0.0,
            2.0 * x[1],
            0.0,
            0.0,
            0.0,
            (3.0 * x[2] ** 2) if ambient_dim == 3 else 0.0,
        ])
        .reshape(3, 3)[:ambient_dim, :ambient_dim]
        .reshape(-1)
    )

    sym_sdiv = sym.surface_divergence(ambient_dim, sym_v)
    sym_adiv = sym_f - (sym_dv @ sym_normal) @ sym_normal

    sdiv = bind(places, sym_sdiv)(actx, v=v)
    adiv = bind(places, sym_adiv)(actx, f=div_v, dv=grad_v)

    error_div = actx.to_numpy(dof_array_rnorm(sdiv, adiv))
    logger.info("error[div]: %.5e", error_div)

    # }}}

    # {{{ test surface gradient

    f = (
        actx.np.cos(2.0 * np.pi * x[0])
        + actx.np.sin(np.pi * x[1] ** 2)
        + (x[2] ** 3 if ambient_dim == 3 else 0.0)
    )
    grad_f = make_obj_array(
        [
            -2.0 * np.pi * actx.np.sin(2.0 * np.pi * x[0]),
            2.0 * np.pi * x[1] * actx.np.cos(np.pi * x[1] ** 2),
            (3.0 * x[2] ** 2) if ambient_dim == 3 else 0.0,
        ][:ambient_dim]
    )

    sym_sgrad = sym.surface_gradient(ambient_dim, sym_f)
    sym_agrad = sym_v - (sym_v @ sym_normal) * sym_normal

    sgrad = bind(places, sym_sgrad)(actx, f=f)
    agrad = bind(places, sym_agrad)(actx, v=grad_f)

    error_grd = actx.to_numpy(dof_array_rnorm(sgrad, agrad))
    logger.info("error[grd]: %.5e", error_grd)

    # }}}

    # {{{ test surface laplacian

    lap_f = (
        -4.0 * np.pi**2 * actx.np.cos(2.0 * np.pi * x[0])
        - 4.0 * np.pi**2 * x[1] ** 2 * actx.np.sin(np.pi * x[1] ** 2)
        + 2.0 * np.pi * actx.np.cos(np.pi * x[1] ** 2)
        + ((6.0 * x[2]) if ambient_dim == 3 else 0.0)
    )
    grad_grad_f = (
        make_obj_array([
            -4.0 * np.pi**2 * actx.np.cos(2.0 * np.pi * x[0]),
            0.0,
            0.0,
            0.0,
            2.0 * np.pi * actx.np.cos(np.pi * x[1] ** 2)
            - 4.0 * np.pi**2 * x[1] ** 2 * actx.np.sin(np.pi * x[1] ** 2),
            0.0,
            0.0,
            0.0,
            ((6.0 * x[2]) if ambient_dim == 3 else 0.0),
        ])
        .reshape(3, 3)[:ambient_dim, :ambient_dim]
        .reshape(-1)
    )

    sym_slap = sym.surface_laplace_beltrami(ambient_dim, sym_f)
    sym_alap = (
        sym_f - sym_kappa * (sym_v @ sym_normal) - (sym_dv @ sym_normal) @ sym_normal
    )

    slap = bind(places, sym_slap)(actx, f=f)
    alap = bind(places, sym_alap)(actx, f=lap_f, v=grad_f, dv=grad_grad_f)

    error_lap = actx.to_numpy(dof_array_rnorm(slap, alap))
    logger.info("error[lap]: %.5e", error_lap)

    # }}}

    if visualize:
        from pystopt.visualization import make_visualizer

        vis = make_visualizer(actx, discr, vis_order=case.target_order)

        if ambient_dim == 2:
            vis.write_file(
                filenamer.with_suffix("surface_divergence"),
                [
                    ("sdiv", sdiv),
                    ("adiv", adiv),
                ],
                overwrite=True,
            )
            vis.write_file(
                filenamer.with_suffix("surface_gradient"),
                [
                    ("sgrad", sgrad),
                    ("agrad", agrad),
                ],
                overwrite=True,
                markers=["-", "--"],
            )
        elif ambient_dim == 3:
            vis.write_file(
                filenamer.with_suffix("surface_derivatives"),
                [
                    ("sdiv", sdiv),
                    ("adiv", adiv),
                    ("sgrad", sgrad),
                    ("agrad", agrad),
                    ("slap", slap),
                    ("alap", alap),
                ],
                overwrite=True,
            )

    h_max = bind(places, sym.h_max_from_volume(ambient_dim))(actx)
    return _SurfaceConvergenceResult(
        h_max=actx.to_numpy(h_max),
        error_div=error_div,
        error_grd=error_grd,
        error_lap=error_lap,
    )


@pytest.mark.parametrize("ambient_dim", [2, 3])
def test_surface_derivatives(actx_factory, ambient_dim, visualize):
    # {{{

    target_order = 3
    if ambient_dim == 2:
        case = etd.FourierEllipseTestCase(
            mesh_arguments={"target_order": target_order},
        )
    elif ambient_dim == 3:
        case = etd.SPHSpheroidTestCase(
            resolutions=[8, 12, 16, 24, 32, 40],
            mesh_arguments={"target_order": target_order},
        )
    else:
        raise ValueError(f"unsupported dimension: '{ambient_dim}'")

    # }}}

    logger.info("\n%s", str(case))
    from pystopt.measure import EOCRecorder, stringify_eoc

    eoc_div = EOCRecorder(name="div", expected_order=target_order)
    eoc_grd = EOCRecorder(name="grd", expected_order=target_order)
    eoc_lap = EOCRecorder(name="lap", expected_order=target_order)

    for i, r in enumerate(case.resolutions):
        result = _run_surface_convergence(
            actx_factory,
            case,
            r,
            visualize=visualize and (i == len(case.resolutions) - 1),
        )

        eoc_div.add_data_point(result.h_max, result.error_div)
        eoc_grd.add_data_point(result.h_max, result.error_grd)
        eoc_lap.add_data_point(result.h_max, result.error_lap)

    logger.info("errors:\n%s", stringify_eoc(eoc_div, eoc_grd, eoc_lap))


# }}}


# {{{ test_custom_execution


def test_custom_execution(actx_factory, visualize):
    actx = get_cl_array_context(actx_factory)

    # {{{ geometry

    target_order = 3
    case = etd.FourierEllipseTestCase(target_order=target_order)
    discr = case.get_discretization(actx, case.resolutions[1])

    from pytential import GeometryCollection

    places = GeometryCollection(discr, auto_where=case.name)

    logger.info("ndofs:     %d", discr.ndofs)
    logger.info("nelements: %d", discr.mesh.nelements)

    # }}}

    # {{{

    xlm = bind(places, sym.snodes(places.ambient_dim))(actx)
    assert xlm is not None

    error = actx.to_numpy(dof_array_rnorm(xlm, actx.thaw(discr.xlm)))
    logger.info("error: %.5e", error)
    assert error < 1.0e-15

    # }}}


# }}}


if __name__ == "__main__":
    import sys

    import pyopencl as cl  # noqa: F401

    if len(sys.argv) > 1:
        exec(sys.argv[1])
    else:
        pytest.main([__file__])
