PYTHON := "python -X dev"

_default:
    @just --list

# {{{ formatting

alias fmt: format

[doc("Reformat all source code")]
format: isort black pyproject justfmt

[doc("Run ruff isort fixes over the source code")]
isort:
    ruff check --fix --select=I src test examples experiments scripts docs
    ruff check --fix --select=RUF022 src
    @echo -e "\e[1;32mruff isort clean!\e[0m"

[doc("Run ruff format over the source code")]
black:
    ruff format src test examples experiments scripts docs
    @echo -e "\e[1;32mruff format clean!\e[0m"

[doc("Run pyproject-fmt over the configuration")]
pyproject:
    {{ PYTHON }} -m pyproject_fmt \
        --indent 4 --max-supported-python "3.13" \
        pyproject.toml
    @echo -e "\e[1;32mpyproject clean!\e[0m"

[doc("Run just --fmt over the justfiles")]
justfmt:
    just --unstable --fmt
    just -f docs/justfile --unstable --fmt
    @echo -e "\e[1;32mjust --fmt clean!\e[0m"

# }}}
# {{{ linting

[doc("Run all linting checks over the source code")]
lint: typos reuse ruff doc8 pylint

[doc("Run typos over the source code and documentation")]
typos:
    typos --sort
    @echo -e "\e[1;32mtypos clean!\e[0m"

[doc("Check REUSE license compliance")]
reuse:
    {{ PYTHON }} -m reuse lint
    @echo -e "\e[1;32mREUSE compliant!\e[0m"

[doc("Run ruff checks over the source code")]
ruff:
    ruff check src test examples experiments scripts docs
    @echo -e "\e[1;32mruff clean!\e[0m"

[doc("Run doc8 checks over the documentation")]
doc8:
    {{ PYTHON }} -m doc8 src docs
    @echo -e "\e[1;32mdoc8 clean!\e[0m"

[doc("Run sphinx linkcheck over the documentation")]
linkcheck:
    @just docs/build linkcheck
    @echo -e "\e[1;32mlinkcheck clean!\e[0m"

[doc("Run pylint checks over the source code")]
pylint:
    @{{ PYTHON }} -c 'from pytential.symbolic.pde import system_utils'
    {{ PYTHON }} -m pylint --py-version 3.10 src test examples docs
    @echo -e "\e[1;32mpylint clean!\e[0m"

[doc("Run mypy checks over the source code")]
mypy:
    {{ PYTHON }} -m mypy src test examples experiments
    @echo -e "\e[1;32mmypy clean!\e[0m"

# }}}
# {{{ pin

[private]
requirements_dev_txt:
    uv pip compile --upgrade --universal --python-version "3.10" \
        --extra git --extra dev --extra docs \
        -o requirements-dev.txt pyproject.toml

[private]
requirements_txt:
    uv pip compile --upgrade --universal --python-version "3.10" \
        --extra git \
        -o requirements.txt pyproject.toml

[doc("Pin dependency versions to requirements.txt")]
pin: requirements_txt requirements_dev_txt

# }}}
# {{{ develop

[doc("Install project in editable mode")]
develop:
    @rm -rf build
    @rm -rf dist
    {{ PYTHON }} -m pip install \
        --verbose \
        --no-build-isolation \
        --editable .

[doc("Editable install using pinned dependencies from requirements-dev.txt")]
pip-install:
    {{ PYTHON }} -m pip install --upgrade \
        pip wheel editables cython \
        hatchling setuptools
    {{ PYTHON }} -m pip install \
        --verbose \
        --requirement requirements-dev.txt \
        --no-build-isolation \
        --editable .

[doc("Remove various build artifacts")]
clean:
    rm -rf *.png
    rm -rf build dist
    rm -rf docs/build.sphinx

[doc("Remove various temporary files and caches")]
purge: clean
    rm -rf .ruff_cache .pytest_cache .pytest-cache .mypy_cache tags

[doc("Regenerate ctags")]
ctags:
    ctags --recurse=yes \
        --tag-relative=yes \
        --exclude=.git \
        --exclude=docs \
        --python-kinds=-i \
        --language-force=python

# }}}
# {{{ tests

[doc("Run pytest tests")]
pytest *PYTEST_ADDOPTS:
    {{ PYTHON }} -m pytest \
        --junit-xml=pytest-results.xml \
        -rswx --durations=25 -v -s -m 'not slowtest' \
        {{ PYTEST_ADDOPTS }}

[doc("Run examples with default options")]
examples:
    @for ex in optim-boundary-uniform.py optimi-boundary-helix; do \
        echo "::group::Running experiments/${ex}"; \
        {{ PYTHON }} experiments/${ex}; \
        echo "::endgroup::"; \
    done

# }}}
