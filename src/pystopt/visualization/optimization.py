# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT


import numpy as np

from pystopt.callbacks import HistoryCallback
from pystopt.paths import FileParts
from pystopt.tools import get_default_logger

logger = get_default_logger(__name__)


# {{{ visualize_performance_history


def visualize_performance_history(
    basename: FileParts,
    history: HistoryCallback | dict[str, list[np.ndarray]],
    *,
    overwrite: bool = False,
) -> None:
    import matplotlib.pyplot as mp

    from pystopt.visualization.matplotlib import subplots

    fig = mp.figure()

    if isinstance(history, HistoryCallback):
        history = history.to_numpy()
    history = {k: v if isinstance(v, list) else [v] for k, v in history.items()}

    # {{{ memory

    if "memory" in history:
        filename = basename.with_suffix("memory")
        with subplots(fig, filename=filename, overwrite=overwrite):
            ax = fig.gca()

            from pytools.convergence import estimate_order_of_convergence

            for memory, timestamp in zip(
                history["memory"], history["timestamp"], strict=False
            ):
                # NOTE: timestamp is in nanoseconds
                timestamp_h = timestamp / 1_000_000_000 / 60

                _, o = estimate_order_of_convergence(timestamp_h, memory)
                ax.plot(timestamp_h, memory, label=rf"$\mathcal{{O}}(t^{{{o:.2f}}})$")

            ax.set_xlabel("$Process~ Time~ (m)$")
            ax.set_ylabel("$Memory~ (RSS)~ (Mib)$")
            ax.legend(
                bbox_to_anchor=(0, 1.02, 1.0, 0.2),
                loc="lower left",
                mode="expand",
                borderaxespad=0,
                ncol=len(history["memory"]),
            )

    # }}}


# }}}


# {{{ visualize_optimization_history


def visualize_optimization_history(
    basename: FileParts,
    history: HistoryCallback | dict[str, list[np.ndarray]],
    *,
    overwrite: bool = False,
) -> None:
    """Plot fields from :class:`~pystopt.callbacks.OptimizationHistoryCallback`.

    :arg history: a :class:`dict` with the same fields as
        :class:`~pystopt.callbacks.OptimizationHistoryCallback`.
    """
    import matplotlib.pyplot as mp

    from pystopt.visualization.matplotlib import subplots

    fig = mp.figure()

    if isinstance(history, HistoryCallback):
        history = history.to_numpy()
    history = {k: v if isinstance(v, list) else [v] for k, v in history.items()}

    visualize_performance_history(basename, history, overwrite=overwrite)

    # {{{ cost

    if "f" in history:
        filename = basename.with_suffix("cost")
        with subplots(fig, filename=filename, overwrite=overwrite):
            ax = fig.gca()
            for f in history["f"]:
                logf = np.abs(f / f[0])
                ax.semilogy(logf + 1.0e-16, "-")

            ax.set_xlabel("$Iteration$")
            ax.set_ylabel(r"$|f/f^0|$")

        filename = basename.with_suffix("cost_relative")
        with subplots(fig, filename=filename, overwrite=overwrite):
            ax = fig.gca()
            for f in history["f"]:
                relf = np.abs(f[1:] - f[:-1]) / f[0]
                ax.semilogy(relf + 1.0e-16, "-")

            ax.set_xlabel("$Iteration$")
            ax.set_ylabel(r"$|f^{k + 1} - f^k| / f^0$")

    # }}}

    # {{{ gradient

    if "g" in history:
        filename = basename.with_suffix("gradient")
        with subplots(fig, filename=filename, overwrite=overwrite):
            ax = fig.gca()
            for g, d in zip(history["g"], history["d"], strict=False):
                log_g = np.abs(g / g[0])
                log_d = np.abs(d / d[0])

                # line, = ax.semilogy(g + 1.0e-16, "-")
                (line,) = ax.semilogy(log_g + 1.0e-16, "-")
                ax.semilogy(log_d + 1.0e-16, ":", color=line.get_color())

            ax.set_xlabel("$Iteration$")
            ax.set_ylabel(r"$\|\mathbf{g}\| / \|\mathbf{g}_0\|$")  # noqa: RUF027

    # }}}

    # {{{ step size

    if "alpha" in history:
        filename = basename.with_suffix("step_size")
        with subplots(fig, filename=filename, overwrite=overwrite):
            ax = fig.gca()
            for alpha in history["alpha"]:
                ax.semilogy(alpha)

            ax.set_xlabel("$Iteration$")
            ax.set_ylabel(r"$\alpha$")

    # }}}

    mp.close(fig)


# }}}


# {{{ visualize_stokes_evolution_history


def visualize_stokes_evolution_history(
    basename: FileParts,
    history: HistoryCallback | dict[str, list[np.ndarray]],
    *,
    expected_deformation: float | None = None,
    overwrite: bool = True,
) -> None:
    """Plot fields from :class:`~pystopt.simulation.StokesEvolutionHistoryCallback`.

    :arg history: a :class:`dict` with the same fields as
        :class:`~pystopt.simulation.StokesEvolutionHistoryCallback`.
    """
    import matplotlib.pyplot as mp

    from pystopt.visualization.matplotlib import subplots

    fig = mp.figure()

    if isinstance(history, HistoryCallback):
        history = history.to_numpy()
    history = {k: v if isinstance(v, list) else [v] for k, v in history.items()}

    if "time" in history:
        time = history["time"]
    else:
        key = next(iter(history.keys()))
        time = [np.arange(v.size) for v in history[key]]

    visualize_performance_history(basename, history, overwrite=overwrite)

    # {{{ velocity

    if "u_dot_n" in history:
        filename = basename.with_suffix("velocity")
        with subplots(fig, filename=filename, overwrite=overwrite):
            ax = fig.gca()

            for t, u_dot_n in zip(time, history["u_dot_n"], strict=False):
                ax.semilogy(t, u_dot_n / u_dot_n[0] + 1.0e-16)

            ax.set_xlabel("$Iteration$")
            ax.set_ylabel(r"$\|\mathbf{u} \cdot \mathbf{n}\|_\infty$")

    # }}}

    # {{{ volume

    if "volume" in history:
        filename = basename.with_suffix("volume")
        with subplots(fig, filename=filename, overwrite=overwrite):
            ax = fig.gca()

            for t, volume in zip(time, history["volume"], strict=False):
                ax.plot(t, volume / volume[0])
                ax.axhline(1, ls="--", color="k")

            ax.set_xlabel("$Iteration$")
            ax.set_ylabel(r"$V / V_0$")

        filename = basename.with_suffix("volume_relative")
        with subplots(fig, filename=filename, overwrite=overwrite):
            ax = fig.gca()
            for t, volume in zip(time, history["volume"], strict=False):
                p_volume = np.abs(volume[1:] - volume[0]) / volume[0]
                ax.semilogy(t[1:], p_volume + 1.0e-16)

            ax.set_xlabel("$Iteration$")
            ax.set_ylabel(r"$|V - V_0| / V_0$")

    # }}}

    # {{{ area

    if "area" in history:
        filename = basename.with_suffix("area")
        with subplots(fig, filename=filename, overwrite=overwrite):
            ax = fig.gca()
            for t, area in zip(time, history["area"], strict=False):
                ax.plot(t, area / area[0])

            ax.set_xlabel("$Iteration$")
            ax.set_ylabel(r"$A / A_0$")

        filename = basename.with_suffix("area_relative")
        with subplots(fig, filename=filename, overwrite=overwrite):
            ax = fig.gca()
            for t, area in zip(time, history["area"], strict=False):
                p_area = np.abs(area[1:] - area[0]) / area[0]
                ax.semilogy(t[1:], p_area + 1.0e-16)

            ax.set_xlabel("$Iteration$")
            ax.set_ylabel(r"$|A - A_0| / A_0$")

    # }}}

    # {{{ deformation

    if "deformation" in history:
        filename = basename.with_suffix("deformation")
        with subplots(fig, filename=filename, overwrite=overwrite):
            ax = fig.gca()
            for t, deformation in zip(time, history["deformation"], strict=False):
                ax.plot(t, deformation)

            if expected_deformation is not None:
                ax.axhline(expected_deformation, ls="--", color="k")

            ax.set_xlabel("$Iteration$")
            ax.set_ylabel(r"$D$")

    # }}}

    mp.close(fig)


# }}}
