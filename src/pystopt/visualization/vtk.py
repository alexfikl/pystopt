# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

import pathlib
from collections.abc import Sequence
from dataclasses import dataclass, field
from typing import Any

from arraycontext import ArrayContext
from meshmode.discretization import Discretization
from meshmode.discretization.visualization import Visualizer

from pystopt.tools import get_default_logger
from pystopt.visualization import VisualizerBase

logger = get_default_logger(__name__)


@dataclass(frozen=True)
class VTKVisualizer(VisualizerBase):
    use_high_order_vtk: bool = True

    # NOTE: internal attribute to avoid recreating the connectivity on copies
    _vis: Visualizer | None = None

    def __post_init__(self):
        if self._vis is None:
            vis = Visualizer(self.conn, is_equidistant=self.use_high_order_vtk)
            object.__setattr__(self, "_vis", vis)

        if self.use_high_order_vtk:
            from meshmode.discretization.poly_element import (
                EquidistantTensorProductElementGroup,
                PolynomialEquidistantSimplexElementGroup,
            )

            # FIXME: this is not a great check
            for grp in self.to_discr.groups:
                if not isinstance(  # noqa: UP038
                    grp,
                    (
                        PolynomialEquidistantSimplexElementGroup,
                        EquidistantTensorProductElementGroup,
                    ),
                ):
                    raise ValueError("high-order VTK requires equidistant groups")

    def copy_with_same_connectivity(
        self, actx: ArrayContext, discr: Discretization, *, skip_tests: bool = False
    ) -> "VTKVisualizer":
        from pystopt.mesh import check_discr_same_connectivity

        if not skip_tests and not check_discr_same_connectivity(discr, self._vis.discr):
            raise ValueError("'discr' does not have matching group structures")

        from pystopt.visualization import replace_connection_discr

        vis_discr = self._vis.vis_discr.copy(actx=actx, mesh=discr.mesh)
        conn = replace_connection_discr(self._vis.connection, discr, vis_discr)

        vis = type(self._vis)(
            conn,
            element_shrink_factor=self._vis.element_shrink_factor,
            is_equidistant=self._vis.is_equidistant,
            _vtk_linear_connectivity=self._vis._cached_vtk_linear_connectivity,
            _vtk_lagrange_connectivity=self._vis._cached_vtk_lagrange_connectivity,
        )

        from dataclasses import replace

        return replace(
            self,
            conn=vis.connection,
            use_high_order_vtk=self.use_high_order_vtk,
            _vis=vis,
        )

    def write_vtk_file(
        self,
        filename: pathlib.Path,
        names_and_fields: Sequence[tuple[str, Any]],
        *,
        overwrite: bool = True,
        **kwargs: Any,
    ) -> None:
        raise NotImplementedError

    def write_file(
        self,
        basename: str,
        names_and_fields: Sequence[tuple[str, Any]],
        *,
        overwrite: bool = True,
        **kwargs: Any,
    ) -> None:
        filename = pathlib.Path(str(basename)).with_suffix(f".{self.ext}")
        if filename.exists():
            if overwrite:
                filename.unlink()
            else:
                raise FileExistsError(f"output file '{filename}' already exists")

        from pystopt.visualization.matplotlib import preprocess_latex

        names_and_fields_clean = [
            (preprocess_latex(name, disable_latex=True), f)
            for name, f in names_and_fields
        ]

        self.write_vtk_file(
            filename, names_and_fields_clean, overwrite=overwrite, **kwargs
        )

        from pystopt.paths import relative_to

        logger.info("output: '%s'", relative_to(filename))


@dataclass(frozen=True)
class VTKXMLVisualizer(VTKVisualizer):
    """Visualizer for unstructured VTK files in the ``VTU`` XML format.

    Based on :class:`meshmode.discretization.visualization.Visualizer`.
    """

    @property
    def ext(self) -> str:
        return "vtu"

    def write_vtk_file(
        self,
        filename: pathlib.Path,
        names_and_fields: Sequence[tuple[str, Any]],
        *,
        overwrite: bool = True,
        **kwargs: Any,
    ) -> None:
        self._vis.write_vtk_file(
            filename,
            names_and_fields,
            overwrite=overwrite,
            use_high_order=self.use_high_order_vtk,
        )


@dataclass(frozen=True)
class VTKHDFVisualizer(VTKVisualizer):
    """Visualizer for unstructured VTK files in the ``VTKHDF`` format.

    Based on :class:`meshmode.discretization.visualization.Visualizer`.
    """

    h5_file_options: dict[str, Any] = field(default_factory=dict)
    h5_dset_options: dict[str, Any] = field(default_factory=dict)

    @property
    def ext(self):
        return "hdf"

    def write_vtk_file(
        self,
        filename: pathlib.Path,
        names_and_fields: Sequence[tuple[str, Any]],
        *,
        overwrite: bool = True,
        **kwargs: Any,
    ) -> None:
        h5_file_options = kwargs.get("h5_file_options")
        if h5_file_options is None:
            h5_file_options = self.h5_file_options

        h5_dset_options = kwargs.get("h5_dset_options")
        if h5_dset_options is None:
            h5_dset_options = self.h5_dset_options

        self._vis.write_vtkhdf_file(
            filename,
            names_and_fields,
            overwrite=overwrite,
            use_high_order=self.use_high_order_vtk,
            h5_file_options=h5_file_options,
            dset_options=h5_dset_options,
        )
