# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

"""
.. currentmodule:: pystopt.visualization

Visualization
-------------

.. autoclass:: VisualizerBase

.. autofunction:: make_visualizer
.. autofunction:: make_same_connectivity_visualizer
"""

from collections.abc import Callable, Sequence
from dataclasses import dataclass
from typing import Any

from arraycontext import ArrayContext
from meshmode.discretization import Discretization
from meshmode.discretization.connection import DiscretizationConnection

from pystopt.paths import PathLike


def make_visualizer_connection(
    actx: ArrayContext, discr: Discretization, *, vis_order: int, vis_type: str
) -> DiscretizationConnection:
    high_order = vis_type in {"vtkxml_lagrange", "vtkhdf_lagrange"}

    if high_order:
        from meshmode.discretization.poly_element import (
            InterpolatoryEquidistantGroupFactory as GroupFactory,
        )
    else:
        from meshmode.discretization.poly_element import (
            InterpolatoryEdgeClusteredGroupFactory as GroupFactory,
        )

    if vis_order > 0 or high_order:
        if vis_order < 0:
            order = max(grp.order for grp in discr.groups)
        else:
            order = vis_order

        from meshmode.discretization.connection import make_same_mesh_connection

        vis_discr = discr.copy(group_factory=GroupFactory(order=order))
        return make_same_mesh_connection(actx, vis_discr, discr)
    else:
        from meshmode.discretization.connection import IdentityDiscretizationConnection

        return IdentityDiscretizationConnection(discr)


def replace_connection_discr(
    conn: DiscretizationConnection,
    from_discr: Discretization,
    to_discr: Discretization,
) -> DiscretizationConnection:
    from meshmode.discretization import connection

    if isinstance(conn, connection.DirectDiscretizationConnection):
        return connection.DirectDiscretizationConnection(
            from_discr,
            to_discr,
            groups=conn.groups,
            is_surjective=conn.is_surjective,
        )
    elif isinstance(conn, connection.IdentityDiscretizationConnection):
        return connection.IdentityDiscretizationConnection(from_discr)
    elif isinstance(conn, connection.ChainedDiscretizationConnection):
        assert not conn.connections
        return connection.ChainedDiscretizationConnection([], from_discr)
    else:
        raise TypeError(f"connection type: '{type(conn).__name__}'")


def flatten_array_container(
    ary: Any, *, key_func: Callable[[str], str], map_func: Callable[[Any], Any]
) -> Sequence[tuple[str, Any]]:
    from arraycontext import NotAnArrayContainerError, serialize_container
    from meshmode.dof_array import DOFArray

    results = []

    def _rec(subary, key):
        if isinstance(ary, DOFArray):
            results.append((key_func(key), map_func(subary)))
            return

        try:
            iterable = serialize_container(subary)
        except NotAnArrayContainerError as exc:
            raise TypeError(
                f"unsupported array type: '{type(subary).__name__}'"
            ) from exc
        else:
            for ikey, iary in iterable:
                _rec(iary, ikey)

    _rec(ary, "")
    return results


def preprocess_visualization_fields(
    names_and_fields: Sequence[tuple[str, Any]],
    *,
    map_func: Callable[[str, Any], tuple[str, Any]] | None = None,
    flatten_containers: bool = True,
    disable_latex: bool = False,
) -> Sequence[tuple[str, Any]]:
    if map_func is None:

        def default_map_func(field):
            return field

        map_func = default_map_func

    def preprocess_name(name, key=""):
        from pystopt.visualization.matplotlib import preprocess_latex

        name = name if not key else f"{name}_{key}"
        return preprocess_latex(name, disable_latex=disable_latex)

    from dataclasses import is_dataclass

    results = []
    for name, field in names_and_fields:
        if flatten_containers:
            try:
                results.extend(
                    flatten_array_container(
                        field,
                        map_func=map_func,
                        key_func=lambda key, name=name: preprocess_name(name, key),
                    )
                )
            except TypeError:
                if is_dataclass(field):
                    from pystopt.tools import dc_items

                    results.extend([
                        (preprocess_name(name, key), map_func(value))
                        for key, value in dc_items(field)
                    ])
                else:
                    results.append((preprocess_name(name), map_func(field)))
        else:
            results.append((preprocess_name(name), map_func(field)))


@dataclass(frozen=True)
class VisualizerBase:
    """Encapsulated visualization methods.

    .. attribute:: ext

        Default extension used by the visualizer.

    .. attribute:: conn

        A :class:`~meshmode.discretization.connection.DiscretizationConnection`
        between the incoming data and the data to be outputted.

    .. attribute:: from_discr

        Original discretization on which the incoming data lives on.

    .. attribute:: to_discr

        Output visualization discretization, i.e. all incoming data is first
        projected on this discretization before writing.

    .. automethod:: write_file
    """

    conn: DiscretizationConnection

    @property
    def ext(self):
        raise NotImplementedError

    @property
    def from_discr(self) -> Discretization:
        return self.conn.from_discr

    @property
    def to_discr(self) -> Discretization:
        return self.conn.to_discr

    def write_file(
        self,
        basename: PathLike,
        names_and_fields: Sequence[tuple[str, Any]],
        *,
        overwrite: bool = True,
        **unused_kwargs: Any,
    ) -> None:
        """
        :arg basename: basename of the file to be written. The exact extension
            is chosen by the visualizer.
        :arg names_and_fields: a list of tuples ``(name, field)``, where
            *field* is an :class:`~pyopencl.array.Array` or a
            :class:`numpy.ndarray` of :class:`~pyopencl.array.Array`,
            representing scalar or vector fields on
            :attr:`~meshmode.discretization.connection.DiscretizationConnection.from_discr`.
        :arg overwrite: flag to overwrite existing files.
        :arg kwargs: additional supported arguments.
        """
        raise NotImplementedError


def make_visualizer(
    actx: ArrayContext,
    discr: Discretization,
    vis_order: int | None = None,
    vis_type: str | None = None,
    **kwargs: Any,
) -> VisualizerBase:
    """Simple interface for visualization. In most cases, the following
    will do the expected

    .. code::

        vis = make_visualizer(actx, discr, vis_order=order)
        vis.write_file(basename, [
            ("velocity", u),
            ("pressure", p),
            ("vorticity", omega)
        ], overwrite=True)

    where ``u`` and ``omega`` are an *object* arrays
    (see :mod:`pytools.obj_array`) and ``p`` is a scalar.

    Currently supported visualizers (selected using *vis_type*) are

    * ``"vtkxml"``: writes XML VTK files with first-order elements.
    * ``"vtkxml_lagrange"``: writes XML VTK files with high-order Lagrange elements.
    * ``"vtkhdf"``: writes HDF5 VTK files with first-order elements.
    * ``"vtkhdf_lagrange"``: writes HDF5 VTK files with high-order Lagrange elements.
    * ``"matplotlib"``: writes binary (e.g. PNG) files using ``matplotlib``.
      Only available in 2D.

    :arg actx: a :class:`~arraycontext.ArrayContext`.
    :arg discr: a :class:`~meshmode.discretization.Discretization`.
    :arg vis_order: resampling for output, e.g. can give a lower order than the
        one *discr* was constructed with to cut down on the number of DOFs.
    :arg vis_type: force selection of one of the visualizers.

    :returns: an appropriate :class:`VisualizerBase` that depends on the provided
        *discr*.
    """

    if vis_order is None:
        vis_order = -1

    if vis_type is None:
        if discr.ambient_dim <= 2 and discr.dim <= 1:
            vis_type = "matplotlib"
        elif discr.dim <= 3:
            vis_type = "vtkhdf_lagrange"
        else:
            raise ValueError("cannot determine a default visualizer. set 'vis_type'")

    conn = make_visualizer_connection(
        actx, discr, vis_order=vis_order, vis_type=vis_type
    )

    if vis_type == "matplotlib":
        from pystopt.visualization.matplotlib import MatplotlibVisualizer

        return MatplotlibVisualizer(conn, **kwargs)
    elif vis_type == "vtkxml":
        from pystopt.visualization.vtk import VTKXMLVisualizer

        return VTKXMLVisualizer(conn, use_high_order_vtk=False, **kwargs)
    elif vis_type == "vtkxml_lagrange":
        from pystopt.visualization.vtk import VTKXMLVisualizer

        return VTKXMLVisualizer(conn, use_high_order_vtk=True, **kwargs)
    elif vis_type == "vtkhdf":
        from pystopt.visualization.vtk import VTKHDFVisualizer

        return VTKHDFVisualizer(conn, use_high_order_vtk=False, **kwargs)
    elif vis_type == "vtkhdf_lagrange":
        from pystopt.visualization.vtk import VTKHDFVisualizer

        return VTKHDFVisualizer(conn, use_high_order_vtk=True, **kwargs)
    else:
        raise ValueError(f"unsupported visualizer: '{vis_type}'")


def make_same_connectivity_visualizer(
    actx: ArrayContext,
    vis: VisualizerBase,
    discr: Discretization,
    *,
    skip_tests: bool = False,
) -> VisualizerBase:
    """
    :arg vis: an existing :class:`VisualizerBase`.
    :arg discr: a new :class:`~meshmode.discretization.Discretization` with
        the same connectivity as :attr:`~VisualizerBase.from_discr`.
    :arg skip_tests: If *True*, no checks are performed that the two
        discretization actually have the same connectivity.

    :returns: a visualizer of the same type as the input with the base
        discretization exchanged for *discr*.
    """
    from pystopt.mesh import check_discr_same_connectivity

    from_discr = vis.conn.from_discr
    if not skip_tests and not check_discr_same_connectivity(discr, from_discr):
        raise ValueError("'discr' does not have the same connectivity")

    from pystopt.visualization.vtk import VTKVisualizer

    if isinstance(vis, VTKVisualizer):
        return vis.copy_with_same_connectivity(actx, discr, skip_tests=True)

    from pystopt.visualization.matplotlib import MatplotlibVisualizer

    if isinstance(vis, MatplotlibVisualizer):
        to_discr = vis.conn.to_discr.copy(actx=actx, mesh=discr.mesh)
        conn = replace_connection_discr(vis.conn, discr, to_discr)

        from dataclasses import replace

        return replace(vis, conn=conn)

    raise TypeError(f"unsupported visualizer: '{type(vis).__name__}'")
