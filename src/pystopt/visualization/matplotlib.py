# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

from __future__ import annotations

import os
import pathlib
from collections.abc import Iterator
from contextlib import contextmanager
from dataclasses import dataclass, field
from typing import TYPE_CHECKING, Any

import numpy as np

from pytools import memoize_method
from pytools.obj_array import make_obj_array

from pystopt.tools import get_default_logger
from pystopt.visualization import VisualizerBase

logger = get_default_logger(__name__)

if TYPE_CHECKING:
    import matplotlib.pyplot as mp

    IS_AVAILABLE_MATPLOTLIB = True
else:
    try:
        import matplotlib.pyplot as mp

        IS_AVAILABLE_MATPLOTLIB = True
    except ImportError:
        IS_AVAILABLE_MATPLOTLIB = False

# {{{ matplotlib helpers


def setup_matplotlib(**kwargs: Any) -> None:
    """Wrapper around :func:`matplotlib.rc` with some biased defaults. Sets
    all incoming options by simple calls to

        .. code::

            matplotlib.rc(key, **kwargs[key])

    :arg kwargs: keyword arguments with the format ``Dict[str, Dict[str, Any]]``.
    :returns: if `"defaults"` is in *kwargs*, then the default options are
        returned without any changes. Otherwise, an updated defaults
        :class:`dict` with values from *kwargs* is returned.
    """

    if not IS_AVAILABLE_MATPLOTLIB:
        return

    from pystopt.tools import is_mpl_latex_available

    defaults = {
        "figure": {"figsize": (8, 8), "dpi": 300},
        "text": {"usetex": is_mpl_latex_available()},
        "legend": {"fontsize": 32},
        "lines": {"linewidth": 2, "markersize": 10},
        "axes": {"labelsize": 32, "titlesize": 32, "grid": True},
        "xtick": {"labelsize": 24, "direction": "inout"},
        "ytick": {"labelsize": 24, "direction": "inout"},
        "axes.grid": {"axis": "both", "which": "both"},
        "xtick.major": {"size": 6.5, "width": 1.5},
        "ytick.major": {"size": 6.5, "width": 1.5},
        "xtick.minor": {"size": 4.0},
        "ytick.minor": {"size": 4.0},
    }

    if "style" in kwargs:
        mp.style.use(kwargs["style"])
    else:
        # NOTE: preserve existing colors (the ones in "science" are ugly)
        defaults["axes"]["prop_cycle"] = mp.rcParams["axes.prop_cycle"]

        if "science" in mp.style.available:
            mp.style.use(["science", "ieee"])
        elif "seaborn-v0_8-white" in mp.style.available:
            # for matplotlib >=3.6
            mp.style.use("seaborn-v0_8-white")
        else:
            mp.style.use("seaborn-white")

    def update(d, u):
        for k, v in u.items():
            new_v = v
            if isinstance(new_v, dict):
                new_v = update(d.get(k, {}), new_v)

            if new_v is not None:
                d[k] = new_v

        return d if len(d) > 0 else None

    update(defaults, kwargs)
    for group, params in defaults.items():
        if not isinstance(params, dict):
            logger.error("ValueError: %s is not a dict", group)
            continue

        try:
            mp.rc(group, **params)
        except KeyError as e:
            logger.error(e)


def preprocess_latex(label: str, *, disable_latex: bool = False) -> str:
    """Preprocess a plot label for LaTeX support.

    :arg disable_latex: force disabling LaTeX support regardless of the
        ``"usetex"`` option in the :mod:`matplotlib` configuration.
    :returns: a version of *label* with LaTeX commands removed if :mod:`matplotlib`
        does not support it. If it does, this ensures that the string is
        inside a math environment.
    """
    if not isinstance(label, str):
        raise ValueError(f"label is not a string: {type(label)}")

    if IS_AVAILABLE_MATPLOTLIB:
        usetex = mp.rcParams["text.usetex"] and not disable_latex
    else:
        usetex = False

    if usetex:
        if "$" in label:
            return label

        return f"${label}$"
    else:
        # remove some common latex things
        for cmd in ("hat", "mathbf", "mathcal", "tilde", "texttt"):
            label = label.replace(f"\\{cmd}", "")

        # remove any other annoying non-ascii characters
        from pystopt.tools import slugify

        return slugify(label, separator="_")


def process_plot_format(fmt: str) -> dict[str, str]:
    """
    :returns: a :class:`dict` of values for the line style, marker style and
        color defined in *fmt*, which can be directly passed on to line plots.
    """
    if fmt is None:
        return {}

    from matplotlib.axes._base import _process_plot_format

    linestyle, marker, color = _process_plot_format(fmt)

    fmt = {}
    if linestyle is not None:
        fmt["linestyle"] = linestyle
    if marker is not None:
        fmt["marker"] = marker
    if color is not None:
        fmt["color"] = color

    return fmt


def with_matplotlib_ext(basename: os.PathLike, ext: str | None = None) -> pathlib.Path:
    """
    :returns: a path with the extension specified by :mod:`matplotlib` in the
        ``"savefig.format"`` configuration option.
    """
    if ext is None:
        ext = mp.rcParams["savefig.format"]

    return pathlib.Path(basename).with_suffix(f".{ext}")


@contextmanager
def subplots(
    fig_or_filename: mp.Figure | str,
    nrows: int = 1,
    ncols: int = 1,
    *,
    filename: os.PathLike | None = None,
    projection: str | None = None,
    figsize: tuple[int, int] | None = None,
    close: bool = True,
    overwrite: bool = False,
) -> Iterator["mp.Figure"]:
    if isinstance(fig_or_filename, mp.Figure):
        fig = fig_or_filename
    else:
        assert filename is None

        fig = mp.figure()
        for i in range(nrows * ncols):
            fig.add_subplot(nrows, ncols, i + 1, projection=projection)
        filename = fig_or_filename

        if figsize is None:
            figsize = (10 * ncols, 10 * nrows)
        fig.set_size_inches(*figsize)

    if filename is not None:
        filename = with_matplotlib_ext(filename)
        filename = filename.resolve()

    if filename is not None and filename.exists() and not overwrite:
        raise FileExistsError(f"output file '{filename}' already exists")

    try:
        yield fig
    finally:
        if filename is not None:
            bbox_extra_artists = []
            for ax in fig.axes:
                legend = ax.get_legend()
                if legend is not None:
                    bbox_extra_artists.append(legend)

            fig.savefig(
                filename,
                bbox_extra_artists=tuple(bbox_extra_artists),
                bbox_inches="tight",
            )

            from pystopt.paths import relative_to

            logger.info("output: %s", relative_to(filename))

            fig.clf()

            if close and not isinstance(fig_or_filename, mp.Figure):
                mp.close(fig)


setup_matplotlib()

# }}}


# {{{ visualizer


@dataclass(frozen=True)
class MatplotlibVisualizer(VisualizerBase):
    """Visualizer based on :mod:`matplotlib.pyplot`.

    .. automethod:: __init__
    .. automethod:: write_file
    """

    fig: "mp.Figure" = field(init=False, repr=False)

    def __post_init__(self) -> None:
        from_discr = self.conn.from_discr
        if from_discr.dim != 1 or from_discr.ambient_dim != 2:
            raise ValueError("only curves in two-dimensions are supported")

        object.__setattr__(self, "fig", mp.figure())

    @property
    def ext(self) -> str:
        return mp.rcParams["savefig.format"]

    def __del__(self):
        mp.close(self.fig)

    @property
    def _setup_actx(self):
        return self.to_discr._setup_actx

    @memoize_method
    def nodes(self) -> np.ndarray:
        actx = self._setup_actx
        from arraycontext import flatten

        return actx.to_numpy(flatten(self.to_discr.nodes(), actx)).reshape(
            self.to_discr.ambient_dim, -1
        )

    @memoize_method
    def theta(self, igrp):
        # FIXME: use the equidistant theta instead
        def _mean(vec):
            return actx.to_numpy(actx.np.sum(vec)) / vec.size

        actx = self._setup_actx
        x, y = make_obj_array([
            actx.np.ravel(xi[igrp] - _mean(xi[igrp]))
            for xi in actx.thaw(self.to_discr.nodes())
        ])

        return actx.to_numpy(2.0 * np.pi * (y < 0) + actx.np.arctan2(y, x))

    @memoize_method
    def ind(self, igrp):
        return np.argsort(self.theta(igrp))

    def gca(self, write_mode, **kwargs):
        ax = self.fig.gca()

        if write_mode == "data":
            kwargs["xlabel"] = "${}$".format(kwargs.get("xlabel", r"\theta"))
        elif write_mode in {"geometry", "quiver"}:
            kwargs["xlabel"] = "$x$"
            kwargs["ylabel"] = "$y$"
        else:
            raise ValueError(f"unsupported write_mode: {write_mode}")

        for attr, value in kwargs.items():
            setter = getattr(ax, f"set_{attr}", None)
            if setter is None:
                continue

            setter(value)

        ax.grid(visible=True)
        return ax

    def _plot_scalar(self, name, var, marker=None, getter_xy=None):
        if marker is None:
            marker = {}
        node_nr_base = 0

        ax = self.fig.gca()
        for igrp, grp in enumerate(self.to_discr.groups):
            g_ind = self.ind(igrp)
            g_x, g_y = getter_xy(igrp, var, node_nr_base)
            # g_ind = g_ind[:g_ind.size]

            ax.plot(g_x[g_ind], g_y[g_ind], **marker, label=preprocess_latex(name))

            node_nr_base += grp.ndofs

    def _add_legend(self, grp=None, ncols=2, write_mode="data"):
        if grp is not None:
            ncols = len(grp.names_and_fields) + int(write_mode == "geometry")
            if grp.names_and_fields:
                f = grp.names_and_fields[0][1]
                if ncols == 1 and isinstance(f, np.ndarray) and f.dtype.char == "O":
                    ncols = f.size
                else:
                    ncols = int(np.sqrt(ncols)) + 1

        ax = self.fig.gca()
        return ax.legend(
            bbox_to_anchor=(0, 1.02, 1.0, 0.2),
            loc="lower left",
            mode="expand",
            borderaxespad=0,
            ncol=ncols,
        )

    def _savefig(self, filename, legend=None):
        if legend is not None:
            bbox_extra_artists = (legend,)
        else:
            bbox_extra_artists = None

        self.fig.savefig(
            filename, bbox_extra_artists=bbox_extra_artists, bbox_inches="tight"
        )

        from pystopt.paths import relative_to

        logger.info("output: %s", relative_to(filename))

    # {{{ write_data_file

    def write_data_file(self, names_and_fields, markers):
        def getter_xy(igrp, var, node_nr_base):
            grp = self.to_discr.groups[igrp]
            return self.theta(igrp), var[node_nr_base : node_nr_base + grp.ndofs]

        for n, (name, f) in enumerate(names_and_fields):
            fmt = process_plot_format(markers[n])

            if isinstance(f, np.ndarray) and f.dtype.char == "O":
                for axis, fi in zip(["x", "y", "z"], f, strict=False):
                    self._plot_scalar(
                        f"{{{name}}}_{axis}", fi, marker=fmt, getter_xy=getter_xy
                    )
            else:
                self._plot_scalar(name, f, marker=fmt, getter_xy=getter_xy)

    # }}}

    # {{{ write_geometry_file

    def write_geometry_file(
        self, names_and_fields, markers, *, show_vertices=False, show_sorted=False
    ):
        def getter_xy(igrp, var, node_nr_base):
            grp = self.to_discr.groups[igrp]
            return (
                var[0][node_nr_base : node_nr_base + grp.ndofs],
                var[1][node_nr_base : node_nr_base + grp.ndofs],
            )

        ax = self.fig.gca()
        found_self_nodes = False
        for n, (name, f) in enumerate(names_and_fields):
            fname = name
            if fname == "__self_x":
                fname = r"\mathbf{x}"
                found_self_nodes = True

            fmt = process_plot_format(markers[n])
            self._plot_scalar(fname, f, marker=fmt, getter_xy=getter_xy)

        if not found_self_nodes:
            fmt = {"linestyle": "-", "color": None}
            self._plot_scalar(
                r"\mathbf{x}", self.nodes(), marker=fmt, getter_xy=getter_xy
            )

        if show_vertices and self.conn.to_discr.mesh.vertices is not None:
            x, y = self.conn.to_discr.mesh.vertices
            ax.plot(x, y, "ko", ms=3)

    # }}}

    # {{{ write_quiver_file

    def write_quiver_file(self, names_and_fields, markers):
        assert all(
            isinstance(f, np.ndarray) and f.dtype.char == "O"
            for _, f in names_and_fields
        )

        x, y = self.nodes()
        ax = self.fig.gca()

        for n, (name, f) in enumerate(names_and_fields):
            fmt = process_plot_format(markers[n])
            fx, fy = f

            node_nr_base = 0
            for igrp, grp in enumerate(self.to_discr.groups):
                g_ind = self.ind(igrp)
                g_fx = fx[node_nr_base : node_nr_base + grp.ndofs]
                g_fy = fy[node_nr_base : node_nr_base + grp.ndofs]
                g_x = x[node_nr_base : node_nr_base + grp.ndofs]
                g_y = y[node_nr_base : node_nr_base + grp.ndofs]

                (line,) = ax.plot(g_x[g_ind], g_y[g_ind], ls="-")

                g_fmt = fmt.copy()
                if "color" not in g_fmt:
                    g_fmt["color"] = line.get_color()

                ax.quiver(
                    g_x[g_ind],
                    g_y[g_ind],
                    g_fx[g_ind],
                    g_fy[g_ind],
                    **g_fmt,
                    angles="xy",
                    scale_units="xy",
                    label=preprocess_latex(name),
                )

                node_nr_base += grp.ndofs

        ax.margins(0.05, 0.05)

    # }}}

    def write_file(
        self,
        basename,
        names_and_fields,
        *,
        overwrite=True,
        markers=None,
        add_legend=None,
        write_mode=None,
        show_vertices=False,
        show_separate=None,
        **kwargs,
    ):
        r"""
        The *write_mode* can be one of:

        * ``"data"``: plots all *names_and_fields* on the same plot as
          a function of :math:`\theta`, i.e. the angle of a node ``(x, y)``.
          If the interface is not single-valued with-respect to the
          angle, this will result in ill-defined plots.
        * ``"geometry"``: will only plot the geometry. This is automatically
          activate if *names_and_fields* is empty. It can also be forced
          by this flag, in case every :class:`tuple` in *names_and_fields*
          is assumed to be an object array and define a geometry.
        * ``"quiver"``: useful when *names_and_fields* contains vectors
          that can be plotted against the geometry.

        The *show_separate* can be one of:

        * ``None``: plot all the given fields in a single file.
        * ``"fields"``: each field results in a separate file being written, with the
          name of the field as a suffix.
        * ``"components"``: write three files for scalar fields, the :math:`x`
          component and :math:`y` component of vector fields.

        :arg markers: a list of string markers and line styles of the same size as
            *names_and_fields*.
        :arg add_legend: a :class:`bool` that shows the legend if *True*. The legend
            is always shown outside the axis at the top.
        :arg write_mode: selects some default styling for the plot, as described
            above.
        :arg show_separate: additionally separates the fields in
            *names_and_fields* as described above.
        :arg show_vertices: a :class:`bool` that adds the vertices to the
            plot if *write_mode* is "geometry".
        :arg kwargs: additional arguments can be passed that are setters on
            :class:`matplotlib.axes.Axes`. For example, the aspect can be set by
            passing ``aspect=1`` which translates to a call ``ax.set_aspect(1)``.
        """

        # {{{ options

        is_geometry_only = not names_and_fields

        # write mode
        if write_mode is None:
            write_mode = "geometry" if is_geometry_only else "data"
        write_mode = write_mode.lower()

        # markers
        if markers is None:
            markers = [None] * len(names_and_fields)

        # legend
        if add_legend is None:
            add_legend = not is_geometry_only or len(names_and_fields) > 1

        if len(names_and_fields) == 1 and "ylabel" not in kwargs:
            kwargs["ylabel"] = preprocess_latex(names_and_fields[0][0])

        # show separate
        if isinstance(show_separate, str):
            show_separate = show_separate.lower()

        # }}}

        # {{{ separate names_and_fields

        def _get_filename(suffix=""):
            if suffix:
                from pystopt.tools import slugify

                suffix = "_" + slugify(suffix, separator="_")

            return f"{basename}{suffix}.{self.ext}"

        from collections import namedtuple

        DataGroup = namedtuple("DataGroup", ["filename", "names_and_fields", "markers"])

        from meshmode.discretization.visualization import _resample_to_numpy

        names_and_fields = [
            (name, _resample_to_numpy(self.conn, self.conn.to_discr, field))
            for name, field in names_and_fields
            if field is not None
        ]

        if show_separate == "fields":
            groups = [
                DataGroup(_get_filename(f"_{name}"), [(name, field)], [marker])
                for (name, field), marker in zip(
                    names_and_fields, markers, strict=False
                )
            ]
        elif show_separate == "components":
            groups = [
                DataGroup(_get_filename("scalar"), [], []),
                DataGroup(_get_filename("vx"), [], []),
                DataGroup(_get_filename("vy"), [], []),
            ]

            import pyopencl as cl

            for (name, f), marker in zip(names_and_fields, markers, strict=False):
                fa = f
                if isinstance(fa, cl.array.Array) and len(fa.shape) == 2:
                    fa = make_obj_array(fa)

                if isinstance(fa, np.ndarray) and fa.dtype.char == "O":
                    groups[1].names_and_fields.append((name, fa[0]))
                    groups[1].markers.append(marker)
                    groups[2].names_and_fields.append((name, fa[1]))
                    groups[2].markers.append(marker)
                else:
                    groups[0].names_and_fields.append((name, fa))
                    groups[0].markers.append(marker)

            groups = [grp for grp in groups if grp[1]]
        else:
            groups = [DataGroup(_get_filename(), names_and_fields, markers)]

        # }}}

        # {{{ iterate over groups

        for grp in groups:
            if not overwrite and os.path.exists(grp.filename):
                raise FileExistsError(f"output file '{grp.filename}' already exists")

            # {{{ plot

            if kwargs.get("force_clf", True):
                self.fig.clf()

            self.gca(write_mode, **kwargs)

            if write_mode == "data":
                self.write_data_file(grp.names_and_fields, grp.markers)
            elif write_mode == "geometry":
                self.write_geometry_file(
                    grp.names_and_fields, grp.markers, show_vertices=show_vertices
                )
            elif write_mode == "quiver":
                self.write_quiver_file(grp.names_and_fields, grp.markers)
            else:
                raise ValueError(f"unknown write_mode: {write_mode}")

            # }}}

            # {{{ write

            if add_legend:
                legend = self._add_legend(
                    grp=grp, write_mode=write_mode, ncols=kwargs.get("ncols", 2)
                )
            else:
                legend = None

            self._savefig(grp.filename, legend=legend)

            # }}}

        # }}}

        if kwargs.get("force_clf", True):
            self.fig.clf()

        # NOTE: for some reason matplotlib seems to be allocating a lot of memory
        # (even though the figures are cleared?) and this seems to help with that
        import gc

        gc.collect()

    # }}}


# }}}
