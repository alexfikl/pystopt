# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

"""
.. currentmodule:: pystopt.stokes

.. autofunction:: make_static_adjoint_boundary_conditions
.. autofunction:: make_unsteady_adjoint_boundary_conditions

.. autoclass:: AdjointTractionJumpCondition
.. autoclass:: UnsteadyAdjointTractionJumpCondition
.. autoclass:: AdjointBoundaryCondition
"""

from dataclasses import dataclass
from functools import partial
from typing import TYPE_CHECKING

import numpy as np

from pytools.obj_array import make_obj_array

import pystopt.stokes.interface as sti
from pystopt import grad, sym
from pystopt.stokes.boundary_conditions import (
    BezierUniformFlowBoundaryCondition,
    GravityJumpCondition,
    HelicalFlowBoundaryCondition,
    SolidBodyRotationBoundaryCondition,
    TractionJumpCondition,
    TwoPhaseStokesBoundaryCondition,
    UniformFlowBoundaryCondition,
    VariableYoungLaplaceJumpCondition,
    YoungLaplaceJumpCondition,
)
from pystopt.stokes.representations import TwoPhaseStokesRepresentation
from pystopt.tools import get_default_logger

logger = get_default_logger(__name__)
if TYPE_CHECKING:
    import pystopt.cost


# {{{ adjoint traction jump boundary conditions


@dataclass(frozen=True)
class AdjointTractionJumpCondition(TractionJumpCondition):
    r"""Defines adjoint boundary conditions for the static Stokes system.

    In the static case, the jump conditions are entirely defined by the
    cost function. In particular, we have that

    .. math::

        [\![ \mathbf{f}^* ]\!] = \nabla_{\mathbf{u}} J.

    .. attribute:: cost
    """

    cost: "pystopt.cost.ShapeFunctional"

    def __call__(self):
        return grad.velocity(self.cost, self.ambient_dim)


@dataclass(frozen=True)
class UnsteadyAdjointTractionJumpCondition(AdjointTractionJumpCondition):
    r"""Defines adjoint boundary conditions for the quasi-static Stokes system.

    In the quasi-static case, the jump conditions are defined by the
    cost function and the adjoint variable constraining the forward kinematic
    condition. We have that

    .. math::

        [\![ \mathbf{f}^* ]\!] = x^* \mathbf{n} + \nabla_{\mathbf{u}} J,

    under the assumptions that the forward kinematic condition is given by

    .. math::

        \dot{\mathbf{x}} = (\mathbf{u} \cdot \mathbf{n}) \mathbf{n}.

    .. attribute:: xstar_name
    .. attribute:: xstar
    """

    # FIXME: should this also take a `StokesEvolutionOperator` thing?
    # If it has tangential components, that's still not enough to define
    # what the shape derivative of those would be + we won't really want to
    # count them anyway.

    xstar_name: str = "xstar"

    @property
    def xstar(self):
        return sym.var(self.xstar_name)

    def __call__(self):
        deltaf = sym.n_times(self.ambient_dim, self.xstar)

        if self.cost is not None:
            deltaf += grad.velocity(self.cost, self.ambient_dim)

        return deltaf


# }}}


# {{{ adjoint boundary conditions


def make_static_adjoint_boundary_conditions(
    ambient_dim: int,
    cost: "pystopt.cost.ShapeFunctional",
) -> "AdjointBoundaryCondition":
    """Construct a :class:`AdjointBoundaryCondition` based on the static
    jump conditions :class:`AdjointTractionJumpCondition`.
    """

    deltaf = AdjointTractionJumpCondition(ambient_dim=ambient_dim, cost=cost)
    return AdjointBoundaryCondition(ambient_dim=ambient_dim, deltafs=(deltaf,))


def make_unsteady_adjoint_boundary_conditions(
    ambient_dim: int,
    cost: "pystopt.cost.ShapeFunctional",
    *,
    xstar_name: str = "xstar",
) -> "AdjointBoundaryCondition":
    """Construct a :class:`AdjointBoundaryCondition` based on the quasi-static
    jump conditions :class:`UnsteadyAdjointTractionJumpCondition`.
    """

    deltaf = UnsteadyAdjointTractionJumpCondition(
        ambient_dim=ambient_dim, cost=cost, xstar_name=xstar_name
    )
    return AdjointBoundaryCondition(ambient_dim=ambient_dim, deltafs=(deltaf,))


@dataclass(frozen=True)
class AdjointBoundaryCondition(TwoPhaseStokesBoundaryCondition):
    """Adjoint boundary conditions for a static Stokes system.

    Implements the interface of :class:`~pystopt.stokes.StokesInterface`
    with the signature::

        sti.velocity(adjoint_bc)

    .. attribute:: ambient_dim
    .. attribute:: cost

    .. automethod:: __init__
    """

    @property
    def cost(self):
        (deltaf,) = self.deltafs
        assert isinstance(deltaf, AdjointTractionJumpCondition)
        return deltaf.cost


@sti.pressure.register(AdjointBoundaryCondition)
def _bc_adjoint_pressure(self: AdjointBoundaryCondition):
    return 0


@sti.velocity.register(AdjointBoundaryCondition)
def _bc_adjoint_velocity(self: AdjointBoundaryCondition):
    return make_obj_array([0, 0, 0][: self.ambient_dim])


@sti.traction.register(AdjointBoundaryCondition)
def _bc_adjoint_traction(self: AdjointBoundaryCondition):
    return make_obj_array([0, 0, 0][: self.ambient_dim])


@sti.tangent_traction.register(AdjointBoundaryCondition)
def _bc_adjoint_tangent_traction(self: AdjointBoundaryCondition, *, tangent_idx):
    return make_obj_array([0, 0, 0][: self.ambient_dim])


@sti.normal_velocity_gradient.register(AdjointBoundaryCondition)
def _bc_adjoint_normal_velocity_gradient(self: AdjointBoundaryCondition):
    return make_obj_array([0, 0, 0][: self.ambient_dim])


@sti.tangent_velocity_gradient.register(AdjointBoundaryCondition)
def _bc_adjoint_tangent_velocity_gradient(
    self: AdjointBoundaryCondition, *, tangent_idx
):
    return make_obj_array([0, 0, 0][: self.ambient_dim])


@sti.normal_velocity_hessian.register(AdjointBoundaryCondition)
def _bc_adjoint_normal_velocity_hessian(self: AdjointBoundaryCondition):
    return 0


@sti.normal_velocity_laplacian.register(AdjointBoundaryCondition)
def _bc_adjoint_normal_velocity_laplacian(self: AdjointBoundaryCondition):
    return 0


@sti.deltau.register(AdjointBoundaryCondition)
def _bc_adjoint_deltau(self: AdjointBoundaryCondition):
    return grad.traction(self.cost, self.ambient_dim)


# }}}


# {{{ boundary condition derivatives


@grad.velocity.register(TwoPhaseStokesBoundaryCondition)
def _grad_velocity_bc(
    self: TwoPhaseStokesBoundaryCondition, op, density, ad, adjoint_density
):
    return sum(
        grad.velocity(deltaf, op, density, ad, adjoint_density)
        for deltaf in self.detalfs
    )


@grad.traction.register(TwoPhaseStokesBoundaryCondition)
def _grad_traction_bc(
    self: TwoPhaseStokesBoundaryCondition, op, density, ad, adjoint_density
):
    return sum(
        grad.traction(deltaf, op, density, ad, adjoint_density)
        for deltaf in self.deltafs
    )


@grad.shape.register(TwoPhaseStokesBoundaryCondition)
def _grad_shape_bc(
    self: TwoPhaseStokesBoundaryCondition, op, density, ad, adjoint_density
):
    return sum(
        grad.shape(deltaf, op, density, ad, adjoint_density) for deltaf in self.deltafs
    )


@grad.capillary.register(TwoPhaseStokesBoundaryCondition)
def _grad_capillary_bc(
    self: TwoPhaseStokesBoundaryCondition, op, density, ad, adjoint_density
):
    return sum(
        grad.capillary(deltaf, op, density, ad, adjoint_density)
        for deltaf in self.deltafs
    )


# }}}


# {{{ farfield derivatives

# {{{ uniform flow derivatives

# FIXME: xstar is used to constrain the evolution equation, so this feels
# like the wrong place to put it to begin with? The unsteady adjoint BCs
# should probably take the `StokesEvolutionOperator`
#
# FIXME: what if there's more jump conditions? that xstar should not be
# an attribute of the jump conditions, but of the full BC?


@grad.farfield.register(UniformFlowBoundaryCondition)
def _grad_farfield_bc_uniform(
    self: UniformFlowBoundaryCondition, op, density, ad, adjoint_density
):
    from pymbolic import ExpressionNode

    (deltafstar,) = ad.bc.deltafs

    xstar = deltafstar.xstar
    normal = sym.normal(deltafstar.ambient_dim).as_vector()

    result = make_obj_array([0, 0, 0][: self.ambient_dim])
    for i in range(result.size):
        if not isinstance(self.uinf[i], ExpressionNode):
            continue

        result[i] = sym.sintegral(
            xstar * normal[i], self.ambient_dim, self.ambient_dim - 1
        )

    return result


# }}}


# {{{ solid body rotation derivatives


@grad.farfield.register(SolidBodyRotationBoundaryCondition)
def _grad_farfield_bc_solid_body_rotation(
    self: SolidBodyRotationBoundaryCondition, op, density, ad, adjoint_density
):
    (deltafstar,) = ad.bc.deltafs

    xstar = deltafstar.xstar
    normal = sym.normal(deltafstar.ambient_dim).as_vector()
    x = sym.nodes(self.ambient_dim).as_vector()

    # FIXME: the BC `b times x` is parametrized by `b`, but we should only be
    # taking derivatives if `b[i]` is a `sym.Variable`? The BCs should keep
    # a reference to these parameters, so we can check?

    operand = xstar * np.cross(x, normal)
    if self.ambient_dim == 2:
        operand = operand[()]

    return sym.sintegral(operand, self.ambient_dim, self.ambient_dim - 1)


# }}}


# {{{ bezier uniform derivatives


@grad.farfield.register(BezierUniformFlowBoundaryCondition)
def _grad_farfield_bc_bezier_uniform(
    self: BezierUniformFlowBoundaryCondition, op, density, ad, adjoint_density
):
    (deltafstar,) = ad.bc.deltafs
    assert self.npoints == deltafstar.cost.npoints

    from pystopt.stokes.boundary_conditions import get_parametrized_bezier_basis

    xstar = deltafstar.xstar
    normal = sym.normal(deltafstar.ambient_dim).as_vector()
    basis = get_parametrized_bezier_basis(deltafstar.cost.tau, self.order)

    from pymbolic.mapper.differentiator import differentiate

    result = np.zeros(self.npoints, dtype=object)
    for i in range(self.npoints):
        db = differentiate(basis[i], self.tau)
        result[i] = sym.sintegral(
            xstar * db * normal, self.ambient_dim, self.ambient_dim - 1
        )

    return result


# }}}


# {{{ helical derivatives


@grad.farfield.register(HelicalFlowBoundaryCondition)
def _grad_farfield_bc_helical(
    self: HelicalFlowBoundaryCondition, op, density, ad, adjoint_density
):
    if self.ambient_dim != 3:
        raise ValueError(f"invalid 'ambient_dim': expected 3, got {self.ambient_dim}")

    (deltafstar,) = ad.bc.deltafs
    xstar = deltafstar.xstar
    normal = sym.normal(deltafstar.ambient_dim).as_vector()
    x = sym.nodes(self.ambient_dim).as_vector()

    # NOTE: we need to take the derivative of uinf in
    #   uinf @ n = [-omega/T * y, omega/T * x, height/T] @ n
    #   uinf @ n = (W(omega) @ x + b(height)) @ n
    # where
    #   W(omega) = [[0, -omega/T, 0], [omega/T, 0, 0], [0, 0, 0]]
    #   b(height) = [0, 0, height/T]
    # with respect to omega and height

    drot = np.array([[0, -1 / self.tmax, 0], [1 / self.tmax, 0, 0], [0, 0, 0]])
    domega = (drot @ x) @ normal

    doffset = make_obj_array([0, 0, 1 / self.tmax])
    dheight = doffset @ normal

    return make_obj_array([
        sym.sintegral(xstar * domega, self.ambient_dim, self.ambient_dim - 1),
        sym.sintegral(xstar * dheight, self.ambient_dim, self.ambient_dim - 1),
    ])


# }}}

# }}}


# {{{ jump conditions derivatives


@grad.velocity.register(TractionJumpCondition)
def _grad_velocity_traction_jump(
    self: TractionJumpCondition, op, density, ad, adjoint_density
):
    return make_obj_array([0, 0, 0][: self.ambient_dim])


@grad.traction.register(TractionJumpCondition)
def _grad_traction_traction_jump(
    self: TractionJumpCondition, op, density, ad, adjoint_density
):
    return make_obj_array([0, 0, 0][: self.ambient_dim])


# {{{ YoungLaplaceJumpCondition derivatives

# {{{ helpers


def _young_laplace_shape_derivative(
    bc: YoungLaplaceJumpCondition,
    ad: TwoPhaseStokesRepresentation,
    qstar: np.ndarray,
) -> dict[str, sym.var]:
    """Implements the shape derivative of the Young-Laplace jump conditions
    based on the boundary integral representation of the adjoint system.
    """
    # FIXME: pass this on to bc?
    dim = bc.ambient_dim - 1

    t = sym.tangential_onb(bc.ambient_dim).T
    n = sym.normal(bc.ambient_dim).as_vector()
    kappa = sym.summed_curvature(bc.ambient_dim)

    gradn = sym.extrinsic_shape_operator(bc.ambient_dim, dim=dim)
    t_dot_gradn = [gradn @ t[i] for i in range(dim)]

    # {{{ velocity gradients

    # NOTE: `side` is set to `None` because the normal gradient is averaged
    n_dot_gradustar = sti.normal_velocity_gradient(
        ad, qstar, side=None, qbx_forced_limit="avg"
    )

    # NOTE: `side` is set to `None` because the tangential grad has no jumps
    t_dot_gradustar = [
        sti.tangent_velocity_gradient(
            ad, qstar, tangent_idx=i, side=None, qbx_forced_limit=+1
        )
        for i in range(dim)
    ]

    # }}}

    # {{{ velocity hessians

    # NOTE: `side` is set to None because lapl and hess have the same jumps,
    # so they cancel out when we add them together. yey!
    n_dot_lapstar = sti.normal_velocity_laplacian(
        ad, qstar, side=None, qbx_forced_limit="avg"
    )
    n_dot_hesstar = sti.normal_velocity_hessian(
        ad, qstar, side=None, qbx_forced_limit="avg"
    )

    # }}}

    # FIXME: why are all the signs reversed?

    if bc.ambient_dim == 2:
        t0 = -(n_dot_lapstar - n_dot_hesstar)
        t1 = 3 * kappa * (n_dot_gradustar @ n)

        return {"t0": t0, "t1": t1}
    elif bc.ambient_dim == 3:
        t0 = -(n_dot_lapstar - n_dot_hesstar)
        t1 = -2 * sum(t_dot_gradustar[i] @ t_dot_gradn[i] for i in range(dim))
        t2 = kappa * (n_dot_gradustar @ n)

        return {"t0": t0, "t1": t1, "t2": t2}
    else:
        raise ValueError(f"unsupported dimension: {bc.ambient_dim}")


def _young_laplace_shape_derivative_ref(
    bc: YoungLaplaceJumpCondition,
    ad: TwoPhaseStokesRepresentation,
    qstar: np.ndarray,
) -> sym.var:
    """Implements the shape derivative of the Young-Laplace jump conditions
    based on directly taking surface derivatives of the adjoint velocity.
    """
    ambient_dim = bc.ambient_dim
    dim = bc.ambient_dim - 1

    n = sym.normal(ambient_dim).as_vector()
    kappa = sym.summed_curvature(ambient_dim)
    kappa_sq = sym.squared_curvature(ambient_dim, dim=dim)

    ustar = sti.velocity(ad, qstar, side=None, qbx_forced_limit=+1)
    n_dot_gradustar = sti.normal_velocity_gradient(
        ad, qstar, side=None, qbx_forced_limit="avg"
    )

    ustar_dot_n = sym.cse(ustar @ n)
    kappa_ustar = sym.cse(kappa * ustar)

    # {{{ adjoint normal

    div_ustar = sym.surface_divergence(ambient_dim, kappa_ustar, dim=dim)
    adjoint_normal_op = div_ustar - kappa**2 * ustar_dot_n

    # }}}

    # {{{ adjoint curvature

    laplace_ustar = sym.surface_laplace_beltrami(ambient_dim, ustar_dot_n, dim=dim)
    adjoint_curvature_op = -laplace_ustar - kappa_sq * ustar_dot_n

    # }}}

    return {
        "t0": adjoint_normal_op,
        "t1": adjoint_curvature_op,
        "t2": kappa * (n_dot_gradustar @ n) + kappa**2 * ustar_dot_n,
    }


def _young_laplace_shape_derivative_ref_v2(
    bc: YoungLaplaceJumpCondition,
    ad: TwoPhaseStokesRepresentation,
    qstar: np.ndarray,
) -> sym.var:
    """Implements the shape derivative of the Young-Laplace jump conditions
    based on directly taking surface derivatives of the adjoint velocity.
    """
    ambient_dim = bc.ambient_dim
    ambient_dim = bc.ambient_dim
    dim = bc.ambient_dim - 1

    n = sym.normal(ambient_dim).as_vector()
    t = sym.tangential_onb(ambient_dim, dim=dim).T
    gradn = sym.extrinsic_shape_operator(bc.ambient_dim, dim=dim)
    t_dot_gradn = [gradn @ t[i] for i in range(dim)]

    # NOTE: `side` is set to `None` because the tangential grad has no jumps
    # ustar = sti.velocity(ad, qstar, side=None, qbx_forced_limit=+1)
    ustar = sym.make_sym_vector("ustar", ambient_dim)
    t_dot_gradustar = [
        sti.tangent_velocity_gradient(
            ad, qstar, tangent_idx=i, side=None, qbx_forced_limit=+1
        )
        for i in range(dim)
    ]

    laplace_beltrami_ustar = 0
    for i in range(ambient_dim):
        laplace_beltrami_ustar -= n[i] * sym.surface_laplace_beltrami(
            ambient_dim, ustar[i], dim=dim
        )
    gradustar_dot_gradn = -2 * sum(
        t_dot_gradustar[i] @ t_dot_gradn[i] for i in range(dim)
    )

    return {
        "t0": laplace_beltrami_ustar,
        "t1": gradustar_dot_gradn,
    }


# }}}


@grad.capillary.register(YoungLaplaceJumpCondition)
def _grad_capillary_young_laplace(
    self: YoungLaplaceJumpCondition, op, density, ad, adjoint_density
):
    ustar = sti.velocity(ad, adjoint_density, qbx_forced_limit=+1)

    # NOTE: deltaf already contains a `1/ca`, so we just multiply by `-1/ca` to
    # get the full derivative, which is `-1/ca^2`
    return (
        -1
        / self.capillary_number
        * sym.sintegral(ustar @ self(), self.ambient_dim, self.ambient_dim - 1)
    )


@grad.shape.register(YoungLaplaceJumpCondition)
def _grad_shape_young_laplace(
    self: YoungLaplaceJumpCondition, op, density, ad, adjoint_density
):
    # terms = _young_laplace_shape_derivative(self, ad, adjoint_density)
    terms = _young_laplace_shape_derivative(self, ad, adjoint_density)

    normal = sym.normal(self.ambient_dim).as_vector()
    return 1 / self.capillary_number * sum(terms.values()) * normal


# }}}


# {{{ VariableYoungLaplaceJumpCondition


def _variable_young_laplace_shape_derivative(
    bc: VariableYoungLaplaceJumpCondition,
    ad: TwoPhaseStokesRepresentation,
    qstar: np.ndarray,
) -> dict[str, sym.var]:
    raise NotImplementedError


@grad.capillary.register(VariableYoungLaplaceJumpCondition)
def _grad_capillary_variable_young_laplace(
    self: VariableYoungLaplaceJumpCondition, op, density, ad, adjoint_density
):
    ustar = sti.velocity(ad, adjoint_density, qbx_forced_limit=+1)

    return (
        -1.0
        / self.bond_number
        * sym.sintegral(ustar @ self(), self.ambient_dim, self.ambient_dim - 1)
    )


@grad.shape.register(VariableYoungLaplaceJumpCondition)
def _grad_shape_variable_young_laplace(
    self: VariableYoungLaplaceJumpCondition, op, density, ad, adjoint_density
):
    terms = _variable_young_laplace_shape_derivative(self, ad, adjoint_density)

    normal = sym.normal(self.ambient_dim).as_vector()
    return 1.0 / self.bond_number * sum(terms.values()) * normal


# }}}


# {{{ GravityJumpCondition


def _gravity_shape_derivative(
    bc: YoungLaplaceJumpCondition,
    ad: TwoPhaseStokesRepresentation,
    qstar: np.ndarray,
) -> sym.var:
    n = sym.normal(bc.ambient_dim).as_vector()
    axis = bc.axis

    ustar = sti.velocity(ad, qstar, qbx_forced_limit=+1)
    t_ustar = ustar - (ustar @ n) * n
    return {
        "t0": (ustar @ n) * (n @ axis),
        "t1": t_ustar @ axis,
    }


@grad.capillary.register(GravityJumpCondition)
def _grad_capillary_gravity(
    self: GravityJumpCondition, op, density, ad, adjoint_density
):
    ustar = sti.velocity(ad, adjoint_density, qbx_forced_limit=+1)

    return (
        -1.0
        / self.bond_number
        * sym.sintegral(ustar @ self(), self.ambient_dim, self.ambient_dim - 1)
    )


@grad.shape.register(GravityJumpCondition)
def _grad_shape_gravity(self: GravityJumpCondition, op, density, ad, adjoint_density):
    terms = _gravity_shape_derivative(self, ad, adjoint_density)

    normal = sym.normal(self.ambient_dim).as_vector()
    return 1.0 / self.bond_number * sum(terms.values()) * normal


# }}}

# }}}


# {{{ two-phase Stokes derivatives

# {{{ helpers


def _stokes_bulk_shape_derivative(
    op: TwoPhaseStokesRepresentation,
    q: np.ndarray,
    ad: TwoPhaseStokesRepresentation,
    qstar: np.ndarray,
    vlambda: sym.var,
) -> dict[str, sym.var]:
    ambient_dim = op.ambient_dim
    dim = ambient_dim - 1

    # {{{ variables

    # forward
    f = partial(sti.traction, op, q, qbx_forced_limit="avg")
    n_dot_gradu = partial(sti.normal_velocity_gradient, op, q, qbx_forced_limit="avg")
    t_dot_gradu = [
        partial(
            sti.tangent_velocity_gradient,
            op,
            q,
            tangent_idx=i,
            qbx_forced_limit="avg",
        )
        for i in range(dim)
    ]

    # adjoint
    n_dot_gradustar = partial(
        sti.normal_velocity_gradient, ad, qstar, qbx_forced_limit="avg"
    )
    tstar = [
        partial(sti.tangent_traction, ad, qstar, tangent_idx=i, qbx_forced_limit="avg")
        for i in range(dim)
    ]
    deltafstar = sti.deltaf(ad.bc)

    # }}}

    def avg(func, weight=1):
        return (func(side=+1) + weight * func(side=-1)) / 2

    def jmp(func, weight=1):
        return func(side=+1) - weight * func(side=-1)

    # FIXME: why are the signs reversed in the paper?
    t0 = -avg(f, vlambda) @ jmp(n_dot_gradustar)
    t1 = deltafstar @ avg(n_dot_gradu)
    t2 = sum(jmp(tstar[i], vlambda) @ avg(t_dot_gradu[i]) for i in range(dim))

    return {"t0": t0, "t1": t1, "t2": t2}


# }}}


@grad.farfield.register(TwoPhaseStokesRepresentation)
def _grad_farfield_stokes(
    self: TwoPhaseStokesRepresentation, op, density, ad, adjoint_density
):
    return make_obj_array([0, 0, 0][: self.ambient_dim])


@grad.capillary.register(TwoPhaseStokesRepresentation)
def _grad_capillary_stokes(
    self: TwoPhaseStokesRepresentation, op, density, ad, adjoint_density
):
    return 0


@grad.velocity.register(TwoPhaseStokesRepresentation)
def _grad_velocity_stokes(
    self: TwoPhaseStokesRepresentation, op, density, ad, adjoint_density
):
    return make_obj_array([0, 0, 0][: self.ambient_dim])


@grad.traction.register(TwoPhaseStokesRepresentation)
def _grad_traction_stokes(
    self: TwoPhaseStokesRepresentation, op, density, ad, adjoint_density
):
    return make_obj_array([0, 0, 0][: self.ambient_dim])


@grad.shape.register(TwoPhaseStokesRepresentation)
def _grad_shape_stokes(
    self: TwoPhaseStokesRepresentation,
    op,
    density,
    ad,
    adjoint_density,
    viscosity_ratio_name="viscosity_ratio",
):
    assert self is op, "representation does not match 'op'"

    vlambda = sym.var(viscosity_ratio_name)
    normal = sym.normal(self.ambient_dim).as_vector()
    terms = _stokes_bulk_shape_derivative(op, density, ad, adjoint_density, vlambda)

    return sum(terms.values()) * normal


# }}}
