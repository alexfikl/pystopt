# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

"""
.. currentmodule:: pystopt.stokes

The :class:`TwoPhaseStokesRepresentation` implements
:class:`pystopt.stokes.StokesInterface` with the following arguments

.. code:: python

    @velocity.register(TwoPhaseStokesRepresentation)
    def _velocity_repr(self, density, *, side=None, qbx_forced_limit=None):
        pass

and similar for the other variables. It also implements the functionality
for :class:`pystopt.operators.RepresentationInterface`.

.. autoclass:: TwoPhaseStokesRepresentation
.. autoclass:: TwoPhaseSingleLayerStokesRepresentation
"""

from functools import partial
from typing import Any

import numpy as np

import pystopt.stokes.interface as sti
import pystopt.stokes.kernels as stk
from pystopt import sym
from pystopt.operators import (
    RepresentationInterface,
    make_sym_density,
    make_sym_operator,
    prepare_sym_rhs,
)
from pystopt.stokes.boundary_conditions import TwoPhaseStokesBoundaryCondition
from pystopt.tools import get_default_logger

logger = get_default_logger(__name__)


# {{{ variable names

_SIDE_TO_NAME: dict[int, str] = {+1: "ext", -1: "int"}
_QBX_LIMIT_TO_NAME: dict[str, str | int] = {"avg": "avg", +1: "ext", -1: "int"}


def _make_var_name(
    name: str, side: int, qbx_forced_limit: int | str, prefix: str = ""
) -> str:
    name = f"{prefix}{name}"
    if side is not None:
        name = f"{name}_{_SIDE_TO_NAME[side]}"

    if qbx_forced_limit is not None:
        name = f"{name}_{_QBX_LIMIT_TO_NAME[qbx_forced_limit]}"

    return name


# }}}


# {{{ two-phase Stokes representation


class TwoPhaseStokesRepresentation(RepresentationInterface, sti.StokesInterface):
    """
    .. automethod:: __init__
    """

    def __init__(
        self,
        knl: stk.StokesKernel,
        jmp: stk.StokesJumpRelations,
        bc: TwoPhaseStokesBoundaryCondition,
        kernel_arguments: dict[str, Any] | None = None,
    ) -> None:
        """
        :arg knl: a Stokes kernel-like :class:`~pystopt.stokes.Stokeslet`.
        :arg jmp: jump relations for the kernels in *knl*.
        :arg bc: farfield boundary conditions.
        """

        super().__init__(knl.ambient_dim, kernel_arguments=kernel_arguments)

        self.knl = knl
        self.jmp = jmp
        self.bc = bc


def _stokes_representation(self, func, density, side, qbx_forced_limit, name=None):
    if name is None:
        name = func.__name__

    if side not in {-1, +1, None}:
        raise ValueError(f"invalid 'side': {side}")

    if qbx_forced_limit not in {-2, -1, +1, +2, "avg", None}:
        raise ValueError(f"invalid 'qbx_forced_limit': {qbx_forced_limit}")

    bc = func(self.bc)
    if np.isscalar(density) and density == 0:
        lpot = jmp = 0
    else:
        lpot = func(self.knl, density, qbx_forced_limit=qbx_forced_limit)

        if side in {-1, +1}:
            jmp = func(self.jmp, density, side=side)
        else:
            jmp = 0

    return sym.cse(bc + jmp + lpot, prefix=_make_var_name(name, side, qbx_forced_limit))


@make_sym_density.register(TwoPhaseStokesRepresentation)
def _make_sym_density_stokes(
    self: TwoPhaseStokesRepresentation, name: str = "q"
) -> np.ndarray:
    return sym.make_sym_vector(name, self.ambient_dim)


@sti.pressure.register(TwoPhaseStokesRepresentation)
def _pressure_stokes(self, density, *, side=None, qbx_forced_limit=None):
    return _stokes_representation(self, sti.pressure, density, side, qbx_forced_limit)


@sti.velocity.register(TwoPhaseStokesRepresentation)
def _velocity_stokes(self, density, *, side=None, qbx_forced_limit=None):
    return _stokes_representation(self, sti.velocity, density, side, qbx_forced_limit)


@sti.traction.register(TwoPhaseStokesRepresentation)
def _traction_stokes(self, density, *, side=None, qbx_forced_limit=None):
    return _stokes_representation(self, sti.traction, density, side, qbx_forced_limit)


@sti.tangent_traction.register(TwoPhaseStokesRepresentation)
def _tangent_traction_stokes(
    self, density, *, tangent_idx, side=None, qbx_forced_limit=None
):
    return _stokes_representation(
        self,
        partial(sti.tangent_traction, tangent_idx=tangent_idx),
        density,
        side,
        qbx_forced_limit,
        name=f"tangent_traction_{tangent_idx}",
    )


@sti.normal_velocity_gradient.register(TwoPhaseStokesRepresentation)
def _normal_velocity_gradient_stokes(
    self, density, *, side=None, qbx_forced_limit=None
):
    return _stokes_representation(
        self, sti.normal_velocity_gradient, density, side, qbx_forced_limit
    )


@sti.tangent_velocity_gradient.register(TwoPhaseStokesRepresentation)
def _tangent_velocity_gradient_stokes(
    self, density, *, tangent_idx, side=None, qbx_forced_limit=None
):
    return _stokes_representation(
        self,
        partial(sti.tangent_velocity_gradient, tangent_idx=tangent_idx),
        density,
        side,
        qbx_forced_limit,
        name=f"tangent_velocity_gradient_{tangent_idx}",
    )


@sti.normal_velocity_laplacian.register(TwoPhaseStokesRepresentation)
def _normal_velocity_laplacian_stokes(
    self, density, *, side=None, qbx_forced_limit=None
):
    return _stokes_representation(
        self, sti.normal_velocity_laplacian, density, side, qbx_forced_limit
    )


@sti.normal_velocity_hessian.register(TwoPhaseStokesRepresentation)
def _normal_velocity_hessian_stokes(self, density, *, side=None, qbx_forced_limit=None):
    return _stokes_representation(
        self, sti.normal_velocity_hessian, density, side, qbx_forced_limit
    )


# }}}


# {{{ single-layer


class TwoPhaseSingleLayerStokesRepresentation(TwoPhaseStokesRepresentation):
    r"""Constructs a complete representation (layer potentials and jumps)
    for a single-layer (Stokeslet) representation of the velocity

    .. math::

            u_i(\mathbf{x}) = \int_\Sigma G_{ij}(\mathbf{x}, \mathbf{y})
                q_j(\mathbf{y}) \,\mathrm{d}S.

    and all the variables described by :class:`~pystopt.stokes.kernels.Stokeslet`.
    The construction largely corresponds to Section 5.3 from [Pozrikidis1992]_.
    """

    def __init__(
        self,
        bc: TwoPhaseStokesBoundaryCondition,
        kernel_arguments: dict[str, Any] | None = None,
    ) -> None:
        knl = stk.make_stokeslet(bc.ambient_dim)
        jmp = stk.StokesletJumpRelations(bc.ambient_dim)

        super().__init__(knl, jmp, bc, kernel_arguments=kernel_arguments)


@make_sym_operator.register(TwoPhaseSingleLayerStokesRepresentation)
def _make_sym_operator_single_layer(
    self, density, *, viscosity_ratio_name="viscosity_ratio"
):
    r"""Constructs Fredholm integral operator for the density.
    The operator is obtained by taking surface limits of the traction
    and subtracting both sides of the interface to introduce the jumps.
    This gives

    .. math::

        \frac{1}{2} (1 + \lambda) \mathbf{q}
            + (1 - \lambda) \mathcal{T}[\mathbf{q}]
            = [\![ \mathbf{f} ]\!] - (1 - \lambda) \mathbf{f}^\infty,

    where the right-hand side is constructed by the caller.
    """
    viscosity_ratio = sym.var(viscosity_ratio_name)
    lpot = sti.traction(self.knl, density, qbx_forced_limit="avg")

    return 0.5 * (1 + viscosity_ratio) * density + (1 - viscosity_ratio) * lpot


@prepare_sym_rhs.register(TwoPhaseSingleLayerStokesRepresentation)
def _prepare_sym_rhs_single_layer(self, b, *, viscosity_ratio_name="viscosity_ratio"):
    viscosity_ratio = sym.var(viscosity_ratio_name)

    return b + sti.deltaf(self.bc) - (1 - viscosity_ratio) * sti.traction(self.bc)


# }}}
