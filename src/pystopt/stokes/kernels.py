# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

"""
.. currentmodule:: pystopt.stokes

All of the classes in this section implement :class:`pystopt.stokes.StokesInterface`
with the arguments

.. code:: python

    @velocity.register(Wrapper)
    def _wrapper_velocity(
            self: Wrapper, density, *, qbx_forced_limit=None):
        pass

and similar for the other variables. The only departure from this interface
are :func:`pystopt.stokes.tangent_traction` and
:func:`pystopt.stokes.tangent_velocity_gradient` which have

.. code:: python

    @tangent_traction.register(Wrapper)
    def _wrapper_traction(
            self: Wrapper, density, *, tangent_idx, qbx_forced_limit=None):
        pass

to specific which tangent vector in the basis to use. The tangent basis
is obtained by :func:`pytential.symbolic.primitives.tangential_onb`.

.. autoclass:: Stresslet
    :no-show-inheritance:

.. autoclass:: Stokeslet
    :no-show-inheritance:

.. autoclass:: StokesletJumpRelations
    :no-show-inheritance:

.. autoclass:: StokesKernel
.. autoclass:: StokesJumpRelations

"""

from typing import Any

import numpy as np

from pytools.obj_array import make_obj_array

import pystopt.stokes.interface as sti
from pystopt import sym
from pystopt.tools import get_default_logger

try:
    # FIXME: better way to check that we're on the new stokes branch?
    from pytential.symbolic.stokes import StressletWrapperTornberg  # noqa: F401

    FAST_STOKES_AVAILABLE = True
except ImportError:
    FAST_STOKES_AVAILABLE = False

logger = get_default_logger(__name__)


def generate_kernel_components(
    ambient_dim: int, kernel_dict: dict[tuple[int, ...], Any], repeat: int = 2
) -> tuple[tuple[int, ...], Any]:
    """
    :returns: a tuple of ``(mi, knl)`` with the multi-index ``mi`` that
        represents the kernel :math:`K_{mi}` component given by ``knl.``
    """
    key = np.empty((ambient_dim,), dtype=np.int32)

    from itertools import product

    for mi in product(range(ambient_dim), repeat=repeat):
        key.fill(0)
        np.add.at(key, list(mi), +1)
        knl = kernel_dict[tuple(key)]

        yield mi, knl


def make_stokeslet(ambient_dim: int, method: str | None = None) -> sti.StokesInterface:
    if FAST_STOKES_AVAILABLE:
        if method is None:
            if ambient_dim <= 2:
                method = "biharmonic"
            elif ambient_dim == 3:
                method = "laplace"
            else:
                raise ValueError(f"unsupported dimension: '{ambient_dim}'")

        stokeslet = ModernStokeslet(ambient_dim, method=method)
    else:
        stokeslet = Stokeslet(ambient_dim)

    return stokeslet


# {{{ base


class StokesKernel(sti.StokesInterface):
    def __init__(self, ambient_dim: int, base_kernel: Any) -> None:
        super().__init__(ambient_dim)

        # NOTE: all the equations are meant to be non-dimensional, so we do
        # not use the viscosity at all
        self.base = base_kernel

    @property
    def mu(self):
        return 1

    def get_jump_relations(self):
        return StokesletJumpRelations(self.ambient_dim)


# }}}


# {{{ stresslet wrapper


class Stresslet(StokesKernel):
    r"""Wrapper for :class:`sumpy.kernel.StressletKernel` and
    ``pytential.symbolic.stokes.StressletWrapper``.

    The wrapper handles the application of the Stresslet kernel :math:`T_{ijk}`,
    the associated pressure kernel :math:`\Pi_{ij}` and associated stress
    kernel :math:`\Theta_{ijkl}` to a given density. The notation and
    definitions can be found in [Pozrikidis1992]_.

    .. attribute:: ambient_dim
    """

    def __init__(self, ambient_dim: int) -> None:
        from pytential.symbolic.stokes import StressletWrapper

        if FAST_STOKES_AVAILABLE:
            base = StressletWrapper(dim=ambient_dim, mu_sym=1, method="naive")
        else:
            base = StressletWrapper(dim=ambient_dim)

        super().__init__(ambient_dim, base)


@sti.pressure.register(Stresslet)
def _stresslet_pressure(self: Stresslet, density, *, qbx_forced_limit=None):
    r"""Apply the associated Stresslet pressure kernel to a given density.
    Constructs the operator

        .. math::

            \int_\Sigma \Pi_{ij}(\mathbf{x}, \mathbf{y})
                d_i(\mathbf{y}) q_j(\mathbf{y}) \,\mathrm{d}S.

    :arg density: a symbolic expression of the density as an
        array of size :attr:`~Stresslet.ambient_dim`.
    """

    normal = sym.normal(self.ambient_dim).as_vector()

    if FAST_STOKES_AVAILABLE:
        return self.base.apply_pressure(density, normal, qbx_forced_limit)
    else:
        return self.base.apply_pressure(density, normal, self.mu, qbx_forced_limit)


@sti.velocity.register(Stresslet)
def _stresslet_velocity(self: Stresslet, density, *, qbx_forced_limit=None):
    r"""Apply the Stresslet kernel to a given density.
    Constructs the operator

        .. math::

            \int_\Sigma T_{ijk}(\mathbf{x}, \mathbf{y})
                d_k(\mathbf{y}) q_j(\mathbf{y}) \,\mathrm{d}S.
    """

    normal = sym.normal(self.ambient_dim).as_vector()

    if FAST_STOKES_AVAILABLE:
        return self.base.apply(density, normal, qbx_forced_limit)
    else:
        return self.base.apply(density, normal, self.mu, qbx_forced_limit)


@sti.traction.register(Stresslet)
def _stresslet_traction(self: Stresslet, density, *, qbx_forced_limit=None):
    r"""Apply the associated Stresslet traction kernel to a given density.
    Constructs the operator

        .. math::

            n_l(\mathbf{x}) \int_\Sigma \Theta_{ijkl}(\mathbf{x}, \mathbf{y})
                d_k(\mathbf{y}) q_j(\mathbf{y}) \,\mathrm{d}S.
    """
    normal = sym.normal(self.ambient_dim).as_vector()

    if FAST_STOKES_AVAILABLE:
        return self.base.apply_stress(density, normal, normal, qbx_forced_limit)
    else:
        mu = self.mu
        return self.base.apply_stress(density, normal, normal, mu, qbx_forced_limit)


# }}}


# {{{ stokeslet wrapper


class Stokeslet(StokesKernel):
    """Wrapper for :class:`sumpy.kernel.StokesletKernel` and
    ``pytential.symbolic.stokes.StokesletWrapper``.

    The wrapper handles the application of the Stokeslet kernel :math:`G_{ij}`,
    the associated pressure kernel :math:`P_{i}` and associated stress
    kernel :math:`T_{ijk}` to a given density. The notation and definitions
    can be found in [Pozrikidis1992]_.
    """

    def __init__(self, ambient_dim):
        from warnings import warn

        warn(
            f"'{type(self).__name__}' is deprecated. Use 'ModernStokeslet' instead",
            DeprecationWarning,
            stacklevel=2,
        )
        from pytential.symbolic.stokes import StokesletWrapper

        if FAST_STOKES_AVAILABLE:
            base = StokesletWrapper(dim=ambient_dim, mu_sym=1, method="naive")
        else:
            base = StokesletWrapper(dim=ambient_dim)

        super().__init__(ambient_dim, base)


@sti.pressure.register(Stokeslet)
def _stokeslet_pressure(self: Stokeslet, density, *, qbx_forced_limit=None):
    r"""Apply the associated Stokeslet pressure kernel to a given density.
    Constructs the operator

        .. math::

            \int_\Sigma P_{j}(\mathbf{x}, \mathbf{y})
                q_j(\mathbf{y}) \,\mathrm{d}S.
    """

    if FAST_STOKES_AVAILABLE:
        return self.base.apply_pressure(density, qbx_forced_limit)
    else:
        return self.base.apply_pressure(density, self.mu, qbx_forced_limit)


@sti.velocity.register(Stokeslet)
def _stokeslet_velocity(self: Stokeslet, density, *, qbx_forced_limit=None):
    r"""Apply the Stokeslet kernel to a given density.
    Constructs the operator

        .. math::

            \int_\Sigma G_{ij}(\mathbf{x}, \mathbf{y})
                q_j(\mathbf{y}) \,\mathrm{d}S.
    """
    if FAST_STOKES_AVAILABLE:
        return self.base.apply(density, qbx_forced_limit)
    else:
        return self.base.apply(density, self.mu, qbx_forced_limit)


@sti.traction.register(Stokeslet)
def _stokeslet_traction(self: Stokeslet, density, *, qbx_forced_limit=None):
    r"""Apply the associated Stokeslet traction kernel to a given density.
    Constructs the operator

        .. math::

            n_k(\mathbf{x}) \int_\Sigma T_{ijk}(\mathbf{x}, \mathbf{y})
                q_j(\mathbf{y}) \,\mathrm{d}S.
    """
    if qbx_forced_limit not in {None, +1, -1, "avg"}:
        raise ValueError("cannot evaluate traction off-surface")

    normal = sym.normal(self.ambient_dim).as_vector()

    if FAST_STOKES_AVAILABLE:
        return self.base.apply_stress(density, normal, qbx_forced_limit)
    else:
        return self.base.apply_stress(density, normal, self.mu, qbx_forced_limit)


@sti.tangent_traction.register(Stokeslet)
def _stokeslet_tangent_traction(
    self: Stokeslet, density, *, tangent_idx, qbx_forced_limit=None
):
    r"""Apply the associated Stokeslet shear kernel to a given density.
    Constructs the operator

        .. math::

            t^\alpha_k(\mathbf{x}) \int_\Sigma T_{ijk}(\mathbf{x}, \mathbf{y})
                q_j(\mathbf{y}) \,\mathrm{d}S.
    """
    if qbx_forced_limit not in {None, +1, -1, "avg"}:
        raise ValueError("cannot evaluate shear off-surface")

    if not 0 <= tangent_idx < self.ambient_dim - 1:
        raise ValueError(f"'tangent_idx' out of bounds: {tangent_idx}")

    onb = sym.tangential_onb(self.ambient_dim).T
    tangent = onb[tangent_idx]

    return self.base.apply_stress(density, tangent, self.mu, qbx_forced_limit)


@sti.normal_velocity_gradient.register(Stokeslet)
def _stokeslet_normal_velocity_gradient(
    self: Stokeslet, density, *, qbx_forced_limit=None
):
    r"""Apply the Stokeslet normal derivative kernel to a given density.
    Constructs the operator

        .. math::

            n_k(\mathbf{x}) \int_\Sigma
                \frac{\partial G_{ij}}{\partial x_k}(\mathbf{x}, \mathbf{y})
                q_j(\mathbf{y}) \,\mathrm{d}S.
    """
    if qbx_forced_limit not in {None, +1, -1, "avg"}:
        raise ValueError("cannot evaluate normal derivative off-surface")

    normal = sym.normal(self.ambient_dim).as_vector()
    expr = 0
    for i in range(self.ambient_dim):
        expr += normal[i] * self.base.apply_derivative(
            i, density, self.mu, qbx_forced_limit
        )

    return expr


@sti.tangent_velocity_gradient.register(Stokeslet)
def _stokeslet_tangent_velocity_gradient(
    self: Stokeslet, density, *, tangent_idx, qbx_forced_limit=None
):
    r"""Apply the Stokeslet tangent derivative kernel to a given density.
    Constructs the operator

        .. math::

            t^\alpha_k(\mathbf{x}) \int_\Sigma
                \frac{\partial G_{ij}}{\partial x_k}(\mathbf{x}, \mathbf{y})
                q_j(\mathbf{y}) \,\mathrm{d}S.
    """
    if qbx_forced_limit not in {None, +1, -1, "avg"}:
        raise ValueError("cannot evaluate tangent derivative off-surface")

    if not 0 <= tangent_idx < self.ambient_dim - 1:
        raise ValueError(f"'tangent_idx' out of bounds: {tangent_idx}")

    onb = sym.tangential_onb(self.ambient_dim).T
    tangent = onb[tangent_idx]

    expr = 0
    for i in range(self.ambient_dim):
        expr += tangent[i] * self.base.apply_derivative(
            i, density, self.mu, qbx_forced_limit
        )

    return expr


def _velocity_laplacian_from_pressure(
    self: StokesKernel,
    density: np.ndarray,
    direction: np.ndarray,
    *,
    qbx_forced_limit: Any | None = None,
) -> sym.var:
    from sumpy.kernel import LaplaceKernel

    kernel = LaplaceKernel(dim=self.ambient_dim)

    from pystopt.symbolic.mappers import DerivativeTaker

    dd = [DerivativeTaker(i) for i in range(3)]

    from itertools import product

    from pytential.symbolic.primitives import int_g_vec

    expr = 0
    for i, j in product(range(self.ambient_dim), repeat=2):
        p_j = int_g_vec(kernel, density[j], qbx_forced_limit=qbx_forced_limit)

        expr += 1 / self.mu * direction[i] * dd[i](dd[j](p_j))

    return expr


@sti.normal_velocity_laplacian.register(Stokeslet)
def _stokeslet_normal_velocity_laplacian(
    self: Stokeslet, density, *, qbx_forced_limit=None, from_pressure=True
):
    r"""Apply the normal Stokeslet Laplacian kernel to a given density.
    Constructs the operator

        .. math::

            \int_\Sigma n_k(\mathbf{x})
                \frac{\partial^2 G_{ij}}{\partial x_k^2}(\mathbf{x}, \mathbf{y})
                q_j(\mathbf{y}) \,\mathrm{d}S.
    """
    if qbx_forced_limit not in {None, +1, -1, "avg"}:
        raise ValueError("cannot evaluate normal derivative off-surface")

    from pystopt.symbolic.mappers import DerivativeTaker

    dd = [DerivativeTaker(i) for i in range(3)]
    normal = sym.normal(self.ambient_dim).as_vector()

    from pytential.symbolic.primitives import int_g_vec

    if from_pressure:
        expr = _velocity_laplacian_from_pressure(
            self, density, normal, qbx_forced_limit=qbx_forced_limit
        )
    else:
        expr = 0
        for (i, j), knl_ij in generate_kernel_components(
            self.ambient_dim, self.base.kernel_dict, repeat=2
        ):
            for m in range(self.ambient_dim):
                u_i = int_g_vec(
                    knl_ij, density[j], mu=self.mu, qbx_forced_limit=qbx_forced_limit
                )

                expr += normal[i] * dd[m](dd[m](u_i))

    return expr


@sti.normal_velocity_hessian.register(Stokeslet)
def _stokeslet_normal_velocity_hessian(
    self: Stokeslet, density, *, qbx_forced_limit=None
):
    r"""Apply the Stokeslet second normal derivative kernel to a given density.
    Constructs the operator

        .. math::

            n_i(\mathbf{x}) n_k(\mathbf{x}) n_l(\mathbf{x}) \int_\Sigma
                \frac{\partial^2 G_{ij}}{\partial x_k \partial x_l}
                    (\mathbf{x}, \mathbf{y})
                q_j(\mathbf{y}) \,\mathrm{d}S.
    """
    if qbx_forced_limit not in {None, +1, -1, "avg"}:
        raise ValueError("cannot evaluate normal derivative off-surface")

    from pytential.symbolic.mappers import DerivativeTaker

    dd = [DerivativeTaker(i) for i in range(3)]
    normal = sym.normal(self.ambient_dim).as_vector()

    from itertools import product

    expr = 0

    from pytential.symbolic.primitives import int_g_vec

    for (i, j), knl_ij in generate_kernel_components(
        self.ambient_dim, self.base.kernel_dict, repeat=2
    ):
        for m, n in product(range(self.ambient_dim), repeat=2):
            u_i = int_g_vec(
                knl_ij, density[j], mu=self.mu, qbx_forced_limit=qbx_forced_limit
            )

            expr += normal[i] * normal[m] * normal[n] * dd[m](dd[n](u_i))

    return expr


# }}}


# {{{ biharmonic Stokes wrappers


class ModernStokeslet(StokesKernel):
    def __init__(self, ambient_dim: int, *, method: str | None = None) -> None:
        if method is None:
            if ambient_dim == 2:
                method = "biharmonic"
            elif ambient_dim == 3:
                method = "laplace"
            else:
                raise ValueError(f"unsupported dimension: {ambient_dim}")

        try:
            if method == "naive":
                from pytential.symbolic.stokes import (
                    StokesletWrapperNaive as StokesletWrapper,
                )
            elif method == "biharmonic":
                from pytential.symbolic.stokes import (
                    StokesletWrapperBiharmonic as StokesletWrapper,
                )
            elif method == "laplace":
                if ambient_dim == 3:
                    from pytential.symbolic.stokes import (
                        StokesletWrapperTornberg as StokesletWrapper,
                    )
                else:
                    raise ValueError("'laplace' method only available in 3d")
            else:
                raise ValueError(f"unknown Stokeslet construction method: '{method}'")
        except ImportError as exc:
            raise RuntimeError(f"cannot use '{type(self).__name__}'") from exc

        base = StokesletWrapper(dim=ambient_dim, mu_sym=1, nu_sym=0.5)
        super().__init__(ambient_dim, base)

    @property
    def mu(self):
        return self.base.mu

    @classmethod
    def is_available(cls) -> bool:
        try:
            from pytential.symbolic.stokes import StokesletWrapperTornberg  # noqa

            return True
        except ImportError:
            return False


@sti.pressure.register(ModernStokeslet)
def _modern_stokeslet_pressure(
    self: ModernStokeslet,
    density: np.ndarray,
    *,
    qbx_forced_limit: Any | None = None,
) -> np.ndarray:
    return self.base.apply_pressure(density, qbx_forced_limit)


@sti.velocity.register(ModernStokeslet)
def _modern_stokeslet_velocity(
    self: ModernStokeslet,
    density: np.ndarray,
    *,
    qbx_forced_limit: Any | None = None,
) -> np.ndarray:
    return self.base.apply(density, qbx_forced_limit)


@sti.traction.register(ModernStokeslet)
def _modern_stokeslet_traction(
    self: ModernStokeslet,
    density: np.ndarray,
    *,
    qbx_forced_limit: Any | None = None,
) -> np.ndarray:
    if qbx_forced_limit not in {None, +1, -1, "avg"}:
        raise ValueError("cannot evaluate traction off-surface")

    normal = sym.normal(self.ambient_dim).as_vector()
    return self.base.apply_stress(density, normal, qbx_forced_limit)


@sti.tangent_traction.register(ModernStokeslet)
def _modern_stokeslet_tangent_traction(
    self: ModernStokeslet,
    density: np.ndarray,
    *,
    tangent_idx: int,
    qbx_forced_limit: Any | None = None,
) -> np.ndarray:
    if qbx_forced_limit not in {None, +1, -1, "avg"}:
        raise ValueError("cannot evaluate shear off-surface")

    if not 0 <= tangent_idx < self.ambient_dim - 1:
        raise ValueError(f"'tangent_idx' out of bounds: {tangent_idx}")

    onb = sym.tangential_onb(self.ambient_dim).T
    tangent = onb[tangent_idx]
    assert tangent.size == self.ambient_dim

    return self.base.apply_stress(density, tangent, qbx_forced_limit)


@sti.normal_velocity_gradient.register(ModernStokeslet)
def _modern_stokeslet_normal_velocity_gradient(
    self: ModernStokeslet,
    density: np.ndarray,
    *,
    qbx_forced_limit: Any | None = None,
) -> np.ndarray:
    if qbx_forced_limit not in {None, +1, -1, "avg"}:
        raise ValueError("cannot evaluate normal derivative off-surface")

    normal = sym.normal(self.ambient_dim).as_vector()
    expr = 0
    for i in range(self.ambient_dim):
        expr += normal[i] * self.base.apply(
            density, qbx_forced_limit, extra_deriv_dirs=(i,)
        )

    return expr


@sti.tangent_velocity_gradient.register(ModernStokeslet)
def _modern_stokeslet_tangent_velocity_gradient(
    self: ModernStokeslet,
    density: np.ndarray,
    *,
    tangent_idx: int,
    qbx_forced_limit: Any | None = None,
) -> np.ndarray:
    if qbx_forced_limit not in {None, +1, -1, "avg"}:
        raise ValueError("cannot evaluate normal derivative off-surface")

    if not 0 <= tangent_idx < self.ambient_dim - 1:
        raise ValueError(f"'tangent_idx' out of bounds: {tangent_idx}")

    onb = sym.tangential_onb(self.ambient_dim).T
    tangent = onb[tangent_idx]
    assert tangent.size == self.ambient_dim

    expr = 0
    for i in range(self.ambient_dim):
        expr += tangent[i] * self.base.apply(
            density, qbx_forced_limit, extra_deriv_dirs=(i,)
        )

    return expr


@sti.normal_velocity_laplacian.register(ModernStokeslet)
def _modern_stokeslet_normal_velocity_laplacian(
    self: ModernStokeslet,
    density: np.ndarray,
    *,
    from_pressure: bool | None = None,
    qbx_forced_limit: Any | None = None,
) -> np.ndarray:
    if qbx_forced_limit not in {None, +1, -1, "avg"}:
        raise ValueError("cannot evaluate normal derivative off-surface")

    if from_pressure is None:
        # NOTE:
        # * in 2D, we use StokesletWrapperBiharmonic, which uses the Biharmonic
        #   kernel, so we prefer writing the Laplacian as is to just keep
        #   one kernel in the expressions (pressure uses the Laplace kernel)
        # * in 3D, we use StokesletWrapperTornberg, which uses a Laplace kernel
        #   so defining this in terms of the pressure still just keeps one
        #   kernel around

        from_pressure = self.ambient_dim == 3

    normal = sym.normal(self.ambient_dim).as_vector()
    expr = 0

    if from_pressure:
        expr = _velocity_laplacian_from_pressure(
            self, density, normal, qbx_forced_limit=qbx_forced_limit
        )
    else:
        for i in range(self.ambient_dim):
            expr += normal @ self.base.apply(
                density, qbx_forced_limit, extra_deriv_dirs=(i, i)
            )

    return expr


@sti.normal_velocity_hessian.register(ModernStokeslet)
def _modern_stokeslet_normal_velocity_hessian(
    self: ModernStokeslet,
    density: np.ndarray,
    *,
    qbx_forced_limit: Any | None = None,
) -> np.ndarray:
    if qbx_forced_limit not in {None, +1, -1, "avg"}:
        raise ValueError("cannot evaluate normal derivative off-surface")

    normal = sym.normal(self.ambient_dim).as_vector()
    expr = 0

    from itertools import product

    for i, j in product(range(self.ambient_dim), repeat=2):
        expr += (
            normal[i]
            * normal[j]
            * (
                normal
                @ self.base.apply(density, qbx_forced_limit, extra_deriv_dirs=(i, j))
            )
        )

    return expr


# }}}


# {{{ jump relations


class StokesJumpRelations(sti.StokesInterface):
    """Generic Stokes jump relations."""


class StokesletJumpRelations(StokesJumpRelations):
    """Jump relations for all the layer potential operators constructed in
    :class:`Stokeslet`.
    """


@sti.pressure.register(StokesletJumpRelations)
def _stokeslet_jump_pressure(self, density, *, side, normal=None):
    if normal is None:
        normal = sym.normal(self.ambient_dim).as_vector()

    return -0.5 * side * (normal @ density)


@sti.velocity.register(StokesletJumpRelations)
def _stokeslet_jump_velocity(self, density, *, side):
    return make_obj_array([0, 0, 0])[: self.ambient_dim]


@sti.traction.register(StokesletJumpRelations)
def _stokeslet_jump_traction(self, density, *, side):
    return 0.5 * side * density


@sti.tangent_traction.register(StokesletJumpRelations)
def _stokeslet_jump_tangent_traction(self, density, *, tangent_idx, side):
    if not 0 <= tangent_idx < self.ambient_dim - 1:
        raise ValueError(f"'tangent_idx' out of bounds: {tangent_idx}")

    onb = sym.tangential_onb(self.ambient_dim).T
    tangent = onb[tangent_idx]
    normal = sym.normal(self.ambient_dim).as_vector()
    assert tangent.size == normal.size

    return 0.5 * side * ((density @ normal) * tangent + (density @ tangent) * normal)


@sti.normal_velocity_gradient.register(StokesletJumpRelations)
def _stokeslet_jump_normal_velocity_gradient(self, density, *, side, normal=None):
    if normal is None:
        normal = sym.normal(self.ambient_dim).as_vector()

    return 0.5 * side * (density - (density @ normal) * normal)


@sti.tangent_velocity_gradient.register(StokesletJumpRelations)
def _stokeslet_jump_tangent_velocity_gradient(self, density, *, tangent_idx, side):
    return make_obj_array([0, 0, 0])[: self.ambient_dim]


@sti.normal_velocity_laplacian.register(StokesletJumpRelations)
def _stokeslet_jump_normal_velocity_laplacian(self, density, *, side, normal=None):
    if normal is None:
        normal = sym.normal(self.ambient_dim).as_vector()

    kappa = sym.summed_curvature(self.ambient_dim)
    return (
        0.5
        * side
        * (
            kappa * (density @ normal)
            - sym.surface_divergence(self.ambient_dim, density)
        )
    )


@sti.normal_velocity_hessian.register(StokesletJumpRelations)
def _stokeslet_jump_normal_velocity_hessian(self, density, *, side, normal=None):
    return sti.normal_velocity_laplacian(self, density, side=side, normal=normal)


# }}}
