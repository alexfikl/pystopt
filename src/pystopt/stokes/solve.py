# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

"""
.. currentmodule:: pystopt.stokes

The :class:`StokesSolution` class implements :class:`pystopt.stokes.StokesInterface`
functions with the arguments

.. code:: python

    @velocity.register(StokesSolution)
    def _solution_velocity(self: StokesSolution, *,
            side=None, qbx_forced_limit=None, dofdesc=None):
        pass

and similar for the other variables.

.. autoclass:: StokesSolution
.. autoclass:: TwoPhaseSingleLayerStokesSolver
    :no-show-inheritance:

.. autofunction:: single_layer_solve
"""

from dataclasses import dataclass
from functools import partial, singledispatch
from typing import Any

import numpy as np

from arraycontext import ArrayContext
from meshmode.dof_array import DOFArray
from pytential import GeometryCollection
from pytools import memoize_in, memoize_method

import pystopt.stokes.interface as sti
from pystopt import bind, sym
from pystopt.stokes.representations import (
    TwoPhaseSingleLayerStokesRepresentation,
    TwoPhaseStokesRepresentation,
)
from pystopt.tools import get_default_logger

logger = get_default_logger(__name__)


# {{{ solution


@dataclass(frozen=True)
class StokesSolution:
    """Represents the result of solving a Stokes layer potential, e.g. by
    :func:`single_layer_solve`.

    .. attribute:: density

        Solved density for the operator :attr:`op`.

    .. attribute:: op
    """

    actx: ArrayContext
    source: sym.DOFDescriptor
    places: GeometryCollection
    density: np.ndarray
    op: TwoPhaseStokesRepresentation
    context: dict[str, Any]

    density_name: str = "q"

    @property
    def ambient_dim(self):
        return self.places.ambient_dim

    @property
    @memoize_method
    def density_norm(self):
        from pystopt.dof_array import dof_array_norm

        return dof_array_norm(self.density)


def _eval_solution(self, func, side, qbx_forced_limit, dofdesc):
    if dofdesc is None:
        dofdesc = self.source
    auto_where = (self.source, dofdesc)

    if self.density_name in self.context:
        raise ValueError("density '{self.density_name}' cannot be in context")

    from pystopt.operators import make_sym_density

    if self.density_norm < 1.0e-14:
        density = 0
    else:
        density = make_sym_density(self.op, self.density_name)

    expr = func(self.op, density, side=side, qbx_forced_limit=qbx_forced_limit)

    from pytential.symbolic.execution import _prepare_auto_where

    auto_where = _prepare_auto_where(auto_where, self.places)

    density = {self.density_name: self.density}
    return bind(self.places, expr, auto_where=auto_where)(
        self.actx, **density, **self.context
    )


@sti.pressure.register(StokesSolution)
def _evaluate_pressure(
    self: StokesSolution, *, side=None, qbx_forced_limit=None, dofdesc=None
):
    return _eval_solution(self, sti.pressure, side, qbx_forced_limit, dofdesc)


@sti.velocity.register(StokesSolution)
def _evaluate_velocity(
    self: StokesSolution, *, side=None, qbx_forced_limit=None, dofdesc=None
):
    return _eval_solution(self, sti.velocity, side, qbx_forced_limit, dofdesc)


@sti.traction.register(StokesSolution)
def _evaluate_traction(
    self: StokesSolution, *, side=None, qbx_forced_limit=None, dofdesc=None
):
    return _eval_solution(self, sti.traction, side, qbx_forced_limit, dofdesc)


@sti.tangent_traction.register(StokesSolution)
def _evaluate_tangent_traction(
    self: StokesSolution,
    *,
    tangent_idx,
    side=None,
    qbx_forced_limit=None,
    dofdesc=None,
):
    return _eval_solution(
        self,
        partial(sti.tangent_traction, tangent_idx=tangent_idx),
        side,
        qbx_forced_limit,
        dofdesc,
    )


@sti.normal_velocity_gradient.register(StokesSolution)
def _evaluate_normal_velocity_gradient(
    self: StokesSolution, *, side=None, qbx_forced_limit=None, dofdesc=None
):
    return _eval_solution(
        self, sti.normal_velocity_gradient, side, qbx_forced_limit, dofdesc
    )


@sti.tangent_velocity_gradient.register(StokesSolution)
def _evaluate_tangent_velocity_gradient(
    self: StokesSolution,
    *,
    tangent_idx,
    side=None,
    qbx_forced_limit=None,
    dofdesc=None,
):
    return _eval_solution(
        self,
        partial(sti.tangent_velocity_gradient, tangent_idx=tangent_idx),
        side,
        qbx_forced_limit,
        dofdesc,
    )


@sti.normal_velocity_laplacian.register(StokesSolution)
def _evaluate_normal_velocity_laplacian(
    self: StokesSolution, *, side=None, qbx_forced_limit=None, dofdesc=None
):
    return _eval_solution(
        self, sti.normal_velocity_laplacian, side, qbx_forced_limit, dofdesc
    )


@sti.normal_velocity_hessian.register(StokesSolution)
def _evaluate_normal_velocity_hessian(
    self: StokesSolution, *, side=None, qbx_forced_limit=None, dofdesc=None
):
    return _eval_solution(
        self, sti.normal_velocity_hessian, side, qbx_forced_limit, dofdesc
    )


# }}}


# {{{ solve


def _ensure_dof_array(
    actx: ArrayContext,
    places: GeometryCollection,
    b: np.ndarray,
    dofdesc: sym.DOFDescriptor,
) -> np.ndarray:
    density_discr = places.get_discretization(dofdesc.geometry, dofdesc.discr_stage)
    x = actx.thaw(density_discr.nodes()[0])

    from pytools.obj_array import make_obj_array

    return make_obj_array([
        bi if isinstance(bi, DOFArray) else actx.np.full_like(x, bi) for bi in b
    ])


@singledispatch
def gmres_initial_guess(self, b, *, viscosity_ratio: float):
    raise NotImplementedError(type(self).__name__)


@gmres_initial_guess.register(TwoPhaseSingleLayerStokesRepresentation)
def _guess_single_layer(
    self: TwoPhaseSingleLayerStokesRepresentation, b, *, viscosity_ratio: float
):
    return 2 * b / (1 + viscosity_ratio)


class TwoPhaseSingleLayerStokesSolver:
    """Simple wrapper around :func:`single_layer_solve`.

    .. automethod:: __init__
    .. automethod:: __call__
    """

    def __init__(
        self,
        sym_op: TwoPhaseStokesRepresentation,
        *,
        viscosity_ratio,
        context=None,
        gmres_arguments=None,
    ):
        if context is None:
            context = {}

        if gmres_arguments is None:
            gmres_arguments = {}

        if "callback" not in gmres_arguments:
            gmres_arguments["callback"] = False

        self.sym_op = sym_op
        self.viscosity_ratio = viscosity_ratio

        self.context = context
        self.gmres_arguments = gmres_arguments

    def __call__(self, actx, places, *, source=None):
        """Solve the Stokes integral equation on the given *source*.

        :returns: a :class:`StokesSolution`.
        """
        return single_layer_solve(
            actx,
            places,
            self.sym_op,
            lambdas=self.viscosity_ratio,
            sources=[source],
            context=self.context,
            gmres_arguments=self.gmres_arguments,
        )


def single_layer_solve(
    actx, places, op, *, lambdas, sources=None, context=None, gmres_arguments=None
):
    """Solves a two-phase Stokes problem.

    :arg places: a :class:`~pytential.collection.GeometryCollection` or any
        input that can be passed to its constructor.
    :arg op: a :class:`~pystopt.stokes.TwoPhaseStokesRepresentation`
        instance that describes the integral equation.
    :arg sources: a list of identifiers in *places* on which to solve.
    :arg lambdas: a list of viscosity ratios for each source domain.

    :returns: a :class:`StokesSolution`.
    """

    # {{{ argument handling

    if not isinstance(places, GeometryCollection):
        places = GeometryCollection(places)

    if sources is None:
        sources = [places.auto_source]

    if not isinstance(sources, list):
        sources = [sources]

    sources = [sym.as_dofdesc(s) for s in sources]
    assert len(lambdas) == len(sources)

    if len(sources) != 1:
        raise ValueError("only a single geometry is supported at the moment")

    ((viscosity_ratio_name, viscosity_ratio),) = lambdas.items()

    if context is None:
        context = {}

    context = context.copy()
    context.update(lambdas)

    if gmres_arguments is None:
        gmres_arguments = {}

    all_gmres_arguments = {
        "rtol": 1.0e-10,
        "callback": True,
        "stall_iterations": 0,
        "hard_failure": True,
    }
    all_gmres_arguments.update(gmres_arguments)

    # }}}

    # {{{ problem setup

    # {{{ symbolic

    @memoize_in(op, (single_layer_solve, "make_sym_operator"))
    def _make_sym_operator(viscosity_ratio_name, dofdesc):
        from pystopt.operators import make_sym_density, make_sym_operator

        density = make_sym_density(op, "q")
        expr = make_sym_operator(op, density, viscosity_ratio_name=viscosity_ratio_name)

        from pystopt.symbolic.execution import prepare_expr

        return prepare_expr(places, expr, dofdesc)

    @memoize_in(op, (single_layer_solve, "prepare_sym_rhs"))
    def _prepare_sym_rhs(viscosity_ratio_name, dofdesc):
        from pystopt.operators import prepare_sym_rhs

        expr = prepare_sym_rhs(op, 0, viscosity_ratio_name=viscosity_ratio_name)

        from pystopt.symbolic.execution import prepare_expr

        return prepare_expr(places, expr, dofdesc)

    sym_op = _make_sym_operator(viscosity_ratio_name, sources[0])
    sym_rhs = _prepare_sym_rhs(viscosity_ratio_name, sources[0])

    # }}}

    # {{{ bind

    b = bind(places, sym_rhs, _skip_prepare=True)(actx, **context)
    b = _ensure_dof_array(actx, places, b, dofdesc=sources[0])

    scipy_op = bind(places, sym_op, _skip_prepare=True).scipy_op(
        actx, "q", b[0].entry_dtype, **context
    )

    x0 = None
    if "x0" not in all_gmres_arguments:
        x0 = gmres_initial_guess(op, b, viscosity_ratio=viscosity_ratio)
        all_gmres_arguments["x0"] = x0

    # }}}

    # {{{ solve

    if abs(viscosity_ratio - 1) < 1.0e-14:
        density = gmres_initial_guess(op, b, viscosity_ratio=viscosity_ratio)
    else:
        from pystopt.optimize.gmres import gmres

        result = gmres(scipy_op, b, **all_gmres_arguments)
        density = result.solution

    # }}}

    return StokesSolution(
        actx=actx,
        source=sources[0],
        places=places,
        density=density,
        op=op,
        context=context,
    )


# }}}

# vim: foldmethod=marker
