# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

"""
.. currentmodule:: pystopt.stokes

All subclasses of :class:`TwoPhaseStokesBoundaryCondition` implement
:class:`pystopt.stokes.StokesInterface` with the arguments

.. code:: python

    @svelocity.register(TwoPhaseStokesBoundaryCondition)
    def _bc_velocity(self: TwoPhaseStokesBoundaryCondition):
        pass

and similar for the remamining variables.

.. autoclass:: TractionJumpCondition
    :no-show-inheritance:

.. autoclass:: YoungLaplaceJumpCondition
.. autoclass:: VariableYoungLaplaceJumpCondition
.. autoclass:: GravityJumpCondition

.. autoclass:: TwoPhaseStokesBoundaryCondition
.. autoclass:: CapillaryBoundaryCondition
.. autoclass:: FarfieldCapillaryBoundaryCondition

.. autofunction:: get_farfield_boundary_from_name
.. autofunction:: get_extensional_farfield
.. autofunction:: get_uniform_farfield
.. autofunction:: get_bezier_uniform_farfield
.. autofunction:: get_shear_farfield
.. autofunction:: get_solid_body_rotation_farfield
.. autofunction:: get_helical_farfield
"""

from dataclasses import dataclass
from typing import Any

import numpy as np

from pytools import memoize_method
from pytools.obj_array import make_obj_array

import pystopt.stokes.interface as sti
from pystopt import sym
from pystopt.tools import get_default_logger

logger = get_default_logger(__name__)


# {{{ traction jump conditions


@dataclass(frozen=True)
class TractionJumpCondition:
    """
    .. attribute:: ambient_dim

    .. automethod:: __call__
    """

    ambient_dim: int

    def __call__(self):
        """Symbolic expression of the jump condition."""
        raise NotImplementedError


@dataclass(frozen=True)
class YoungLaplaceJumpCondition(TractionJumpCondition):
    r"""Constant surface tension jump condition. It is given by

    .. math::

        [\![ \mathbf{f} ]\!] = \frac{1}{\mathrm{Ca}} \kappa \mathbf{n}.

    .. attribute:: capillary_number_name

        Name of the symbolic variable representing the Capillary number.

    .. attribute:: capillary_number

        A :class:`~pymbolic.primitives.Variable`.

    .. automethod:: __init__
    """

    capillary_number_name: str = "ca"

    @property
    def capillary_number(self):
        return sym.var(self.capillary_number_name)

    def __call__(self):
        normal = sym.normal(self.ambient_dim).as_vector()
        kappa = sym.summed_curvature(self.ambient_dim)

        return 1 / self.capillary_number * kappa * normal


@dataclass(frozen=True)
class VariableYoungLaplaceJumpCondition(YoungLaplaceJumpCondition):
    r"""Variable surface tension jump condition. It is given by

    .. math::

        [\![ \mathbf{f} ]\!] = \frac{1}{\mathrm{Ca}}
            (\gamma \kappa \mathbf{n} + \nabla_\Sigma \gamma),

    where :math:`\nabla_\Sigma` is the tangential gradient on the surface
    :math:`\Sigma`.

    .. attribute:: surface_tension_name

        Name of the symbolic variable representing the surface tension
        coefficient.

    .. attribute:: surface_tension

        A :class:`~pymbolic.primitives.Variable`.

    .. automethod:: __init__
    """

    surface_tension_name: str = "gamma"

    @property
    def surface_tension(self):
        return sym.var(self.surface_tension_name)

    def __call__(self):
        normal = sym.normal(self.ambient_dim).as_vector()
        kappa = sym.summed_curvature(self.ambient_dim)

        ca = self.capillary_number
        gamma = self.surface_tension

        return (
            1
            / ca
            * (gamma * kappa * normal + sym.surface_gradient(self.ambient_dim, gamma))
        )


@dataclass(frozen=True)
class GravityJumpCondition(TractionJumpCondition):
    r"""Gravity-based jump condition. It is given by

    .. math::

        [\![ \mathbf{f} ]\!] = \frac{1}{\mathrm{Bo}} x_d \mathbf{n}.

    .. attribute:: direction

        Integer indicating the axis direction in which gravity actx.

    .. attribute:: bond_number_name

        Name of the symbolic variable representing the Bond number.

    .. attribute:: bond_number

        A :class:`~pymbolic.primitives.Variable`.

    .. automethod:: __init__
    """

    direction: int
    bond_number_name: str = "bo"

    def __post_init__(self):
        if self.direction < 0 or self.direction >= self.ambient_dim:
            raise ValueError(
                f"'direction' must be in [0, ambient_dim): given {self.direction}"
            )

    @property
    def bond_number(self):
        return sym.var(self.bond_number_name)

    @property
    @memoize_method
    def axis(self):
        axis = make_obj_array([0, 0, 0][: self.ambient_dim])
        axis[self.direction] = 1

        return axis

    def __call__(self):
        normal = sym.normal(self.ambient_dim).as_vector()
        nodes = sym.nodes(self.ambient_dim).as_vector()

        bo = self.bond_number

        return 1 / bo * (self.axis @ nodes) * normal


# }}}


# {{{ base classes


@dataclass(frozen=True)
class TwoPhaseStokesBoundaryCondition:
    """Boundary conditions for a generic two-phase Stokes flow.

    .. attribute:: ambient_dim
    .. attribute:: deltafs

        A :class:`tuple` of :class:`TractionJumpCondition` that are summed
        together to obtain the full jump condition.

    .. automethod:: __init__
    """

    ambient_dim: int
    deltafs: tuple[TractionJumpCondition, ...]


@sti.deltaf.register(TwoPhaseStokesBoundaryCondition)
def _bc_stokes_deltaf(self: TwoPhaseStokesBoundaryCondition):
    return sum(deltaf() for deltaf in self.deltafs)


@dataclass(frozen=True)
class CapillaryBoundaryCondition(TwoPhaseStokesBoundaryCondition):
    """Boundary conditions for a two-phase Stokes flow in the presence
    of constant surface tension at the interface.

    .. attribute:: capillary_number

        Symbolic variable representing the Capillary number.
    """

    @property
    def capillary_number(self):
        (deltaf,) = (
            d for d in self.deltafs if isinstance(d, YoungLaplaceJumpCondition)
        )

        return deltaf.capillary_number


# }}}


# {{{ farfield boundary conditions interface


def get_farfield_boundary_from_name(
    ambient_dim: int, name: str, **kwargs: Any
) -> "FarfieldCapillaryBoundaryCondition":
    if name == "uniform":
        return get_uniform_farfield(ambient_dim, **kwargs)
    elif name == "bezier_uniform":
        return get_bezier_uniform_farfield(ambient_dim, **kwargs)
    elif name == "extensional":
        return get_extensional_farfield(ambient_dim, **kwargs)
    elif name == "shear":
        return get_shear_farfield(ambient_dim, **kwargs)
    elif name == "solid_body_rotation":
        return get_solid_body_rotation_farfield(ambient_dim, **kwargs)
    elif name == "helix":
        return get_helical_farfield(ambient_dim, **kwargs)
    else:
        raise ValueError(f"unknown farfield boundary name: '{name}'")


@dataclass(frozen=True)
class FarfieldCapillaryBoundaryCondition(CapillaryBoundaryCondition):
    """Farfield boundary conditions for constant surface tension Stokes flow."""

    uinf: np.ndarray
    pinf: sym.var
    finf: np.ndarray
    tinf: np.ndarray
    dudninf: np.ndarray
    dudtinf: np.ndarray
    ddudninf: sym.var
    dduinf: sym.var


@sti.pressure.register(FarfieldCapillaryBoundaryCondition)
def _bc_capillary_pressure(self: FarfieldCapillaryBoundaryCondition):
    return self.pinf


@sti.velocity.register(FarfieldCapillaryBoundaryCondition)
def _bc_capillary_velocity(self: FarfieldCapillaryBoundaryCondition):
    return self.uinf


@sti.traction.register(FarfieldCapillaryBoundaryCondition)
def _bc_capillary_traction(self: FarfieldCapillaryBoundaryCondition):
    return self.finf


@sti.tangent_traction.register(FarfieldCapillaryBoundaryCondition)
def _bc_capillary_tangent_traction(
    self: FarfieldCapillaryBoundaryCondition, tangent_idx
):
    return self.tinf[tangent_idx]


@sti.normal_velocity_gradient.register(FarfieldCapillaryBoundaryCondition)
def _bc_capillary_normal_velocity_gradient(self: FarfieldCapillaryBoundaryCondition):
    return self.dudninf


@sti.tangent_velocity_gradient.register(FarfieldCapillaryBoundaryCondition)
def _bc_capillary_tangent_velocity_gradient(
    self: FarfieldCapillaryBoundaryCondition, tangent_idx
):
    return self.dudtinf[tangent_idx]


@sti.normal_velocity_hessian.register(FarfieldCapillaryBoundaryCondition)
def _bc_capillary_normal_velocity_hessian(self: FarfieldCapillaryBoundaryCondition):
    return self.ddudninf


@sti.normal_velocity_laplacian.register(FarfieldCapillaryBoundaryCondition)
def _bc_capillary_normal_velocity_laplacian(
    self: FarfieldCapillaryBoundaryCondition,
):
    return self.dduinf


# }}}


# {{{ farfield boundary conditions


def _make_farfield_with_constant_pressure(
    uinf, pinf, capillary_number_name, normal=None, tangent=None, cls=None, **kwargs
):
    assert isinstance(capillary_number_name, str)

    import pystopt.symbolic.sympy_interop as spi

    pinf = spi.to_sympy_variable(pinf)
    if not spi.is_variable(pinf):
        raise ValueError("pressure is expected to be constant")

    if cls is None:
        cls = FarfieldCapillaryBoundaryCondition

    ambient_dim = len(uinf)
    if normal is None:
        normal = spi.normal(ambient_dim)

    finf = spi.traction(pinf, uinf, n=normal)
    tinf = spi.tangent_traction(pinf, uinf, t=tangent)
    dudninf = spi.normal_vector_gradient(uinf, n=normal)
    dudtinf = spi.tangent_vector_gradient(uinf, t=tangent)
    ddudninf = spi.normal_vector_hessian(uinf, n=normal).dot(normal)
    dduinf = spi.vector_laplacian(uinf).dot(normal)

    assert len(dudtinf) == ambient_dim - 1
    assert len(tinf) == ambient_dim - 1

    deltaf = YoungLaplaceJumpCondition(
        ambient_dim=ambient_dim, capillary_number_name=capillary_number_name
    )

    return cls(
        ambient_dim=ambient_dim,
        uinf=spi.to_pytential(ambient_dim, uinf),
        pinf=spi.to_pytential(ambient_dim, pinf),
        finf=spi.to_pytential(ambient_dim, finf),
        tinf=spi.to_pytential(ambient_dim, tinf),
        dudninf=spi.to_pytential(ambient_dim, dudninf),
        dudtinf=spi.to_pytential(ambient_dim, dudtinf),
        ddudninf=spi.to_pytential(ambient_dim, ddudninf),
        dduinf=spi.to_pytential(ambient_dim, dduinf),
        deltafs=(deltaf,),
        **kwargs,
    )


# {{{ extensional flow


@dataclass(frozen=True)
class ExtensionalFlowBoundaryCondition(FarfieldCapillaryBoundaryCondition):
    alpha: float | sym.var


def get_extensional_farfield(
    ambient_dim: int,
    *,
    alpha: float = 0.5,
    pinf: float = 0.0,
    axis: int = 2,
    capillary_number_name: str = "ca",
) -> FarfieldCapillaryBoundaryCondition:
    r"""Extensional farfield flow with constant surface tension

    .. math::

        \mathbf{u}_\infty =
        \begin{cases}
        (\alpha x, -\alpha y), & \quad d = 2, \\
        (\alpha x, \alpha y, -2 \alpha z), & \quad d = 3,
        \end{cases}

    where the location of the :math:`-2 \alpha` can be controlled by the
    *axis* argument. Setting *axis* to :math:`-1` extends the 2D case to 3D
    by padding with zero.

    :arg alpha: strain rate.
    :arg pinf: constant farfield pressure.
    :arg axis: can define the plane in which the extensional flow acts for a
        three-dimensional flow, but is ignored in two dimensions.

    :returns: a :class:`FarfieldCapillaryBoundaryCondition`.
    """
    import pystopt.symbolic.sympy_interop as spi

    x = spi.cart_coords(ambient_dim)
    alpha = spi.to_sympy_variable(alpha)

    if not spi.is_variable(alpha):
        raise ValueError(f"'alpha' is not constant: {alpha}")

    if ambient_dim == 2:
        uinf = make_obj_array([alpha * x[0], -alpha * x[1]])
    elif ambient_dim == 3:
        if axis == -1:
            uinf = make_obj_array([
                alpha * x[0],
                -alpha * x[1],
                spi.to_sympy_variable(0.0),
            ])
        elif 0 <= axis < ambient_dim:
            uinf = alpha * make_obj_array([x[0], x[1], x[2]])
            uinf[axis] = -2.0 * uinf[axis]
        else:
            raise ValueError(f"unsupported axis '{axis}' in {ambient_dim}d")
    else:
        raise ValueError(f"unsupported dimension: '{ambient_dim}'")

    return _make_farfield_with_constant_pressure(
        uinf,
        pinf,
        capillary_number_name=capillary_number_name,
        cls=ExtensionalFlowBoundaryCondition,
        alpha=alpha,
    )


# }}}


# {{{ uniform flow


@dataclass(frozen=True)
class UniformFlowBoundaryCondition(FarfieldCapillaryBoundaryCondition):
    pass


def get_uniform_farfield(
    ambient_dim: int,
    *,
    uinf: float | np.ndarray = 1.0,
    pinf: float = 0.0,
    capillary_number_name: str = "ca",
) -> FarfieldCapillaryBoundaryCondition:
    r"""Uniform farfield flow with constant surface tension.

    .. math::

        \mathbf{u}^\infty = (U, V, W)

    :arg uinf: an :class:`~numpy.ndarray` of size *ambient_dim* or a single
        number, in which case the velocity field is set to be
        :math:`(U_\infty, 0, 0)`.
    :arg pinf: constant farfield pressure.
    :returns: a :class:`FarfieldCapillaryBoundaryCondition`.
    """
    if isinstance(uinf, np.ndarray):
        assert uinf.size == ambient_dim
    else:
        uinf = make_obj_array([uinf, 0, 0])

    import pystopt.symbolic.sympy_interop as spi

    uinf = spi.to_sympy_variable(uinf)
    if not all(spi.is_variable(u) for u in uinf):
        raise ValueError(f"found non-constant component in 'uinf': {uinf}")

    return _make_farfield_with_constant_pressure(
        uinf[:ambient_dim],
        pinf,
        capillary_number_name=capillary_number_name,
        cls=UniformFlowBoundaryCondition,
    )


# }}}


# {{{ time-dependent Bezier-based uniform flow


@dataclass(frozen=True)
class BezierUniformFlowBoundaryCondition(FarfieldCapillaryBoundaryCondition):
    tau: sym.var
    position: sym.var
    points: tuple[np.ndarray, ...]

    @property
    def npoints(self):
        return len(self.points)

    @property
    def order(self):
        return len(self.points) - 1


def get_parametrized_bezier_basis(t: np.ndarray, n: int) -> tuple[np.ndarray, ...]:
    from math import comb

    basis = []
    for i in range(n + 1):
        b = comb(n, i) * t**i * (1 - t) ** (n - i)

        basis.append(b)

    return tuple(basis)


def get_bezier_uniform_farfield(
    ambient_dim: int,
    points_or_order: int | tuple[np.ndarray, ...],
    *,
    tmax: float | sym.var,
    pinf: float = 0.0,
    tau: str | sym.var = "tau",
    capillary_number_name: str = "ca",
) -> FarfieldCapillaryBoundaryCondition:
    r"""Time-dependent uniform farfield flow with constant surface tension.

    The control points in *ps* define a curve

    .. math::

        \mathbf{p}(\tau) = \sum \mathbf{p}_i \phi_i(\tau),

    where :math:`\phi_i(\tau)`, for the normalized time variable
    :math:`\tau \in [0, 1]`, are the Bézier curve basis functions for the
    respective order. The velocity field is then given by

    .. math::

        \mathbf{u}^\infty(\tau) = \frac{\mathrm{d} \mathbf{p}}{\mathrm{d} \tau}.

    :arg points_or_order: a set of control points used to form the Bézier curve.
        The number of points also defines the order of the curve.
        If this is a single integer, the points are defined symbolically and
        named ``"p{i}"``.
    :arg pinf: constant farfield pressure.
    :arg tau: variable name for the parametrization of the Bézier curve.
        This is expected to be normalized to :math:`[0, 1]` by the user when
        the expression is eventually evaluated.

    :returns: a :class:`FarfieldCapillaryBoundaryCondition`.
    """
    import pystopt.symbolic.sympy_interop as spi

    if isinstance(points_or_order, int):
        if points_or_order > 3:
            raise ValueError(
                f"Bézier curves of order {points_or_order} are not supported"
            )

        points = tuple([
            spi.make_sym_vector(f"bezier_p{i}", ambient_dim)
            for i in range(points_or_order + 1)
        ])
        order = points_or_order
    elif isinstance(points_or_order, tuple):
        if not all(p.size == ambient_dim for p in points_or_order):
            raise ValueError("'points' have a different dimension to 'ambient_dim'")

        points = tuple([spi.to_sympy_variable(p) for p in points_or_order])
        order = len(points) - 1
    else:
        raise TypeError(
            "'points_or_order' has unknown type "
            f"'{type(points_or_order).__name__}': {points_or_order}"
        )

    if order == 0:
        raise ValueError(
            "zero order Bezier velocity fields are not supported: "
            "use 'get_uniform_farfield' instead"
        )

    tmax = spi.to_sympy_variable(tmax)
    tau = spi.to_sympy_variable(tau)

    # get the position at each time
    basis = get_parametrized_bezier_basis(tau / tmax, order)
    p = sum(b * p for b, p in zip(basis, points, strict=False))

    import sympy as sp

    uinf = make_obj_array([sp.diff(p_i, tau) for p_i in p])
    return _make_farfield_with_constant_pressure(
        uinf,
        pinf,
        capillary_number_name=capillary_number_name,
        cls=BezierUniformFlowBoundaryCondition,
        # parameters
        tau=spi.to_pytential(ambient_dim, tau),
        position=spi.to_pytential(ambient_dim, p),
        points=spi.to_pytential(ambient_dim, points),
    )


# }}}


# {{{ shear flow


@dataclass(frozen=True)
class ShearFlowBoundaryCondition(FarfieldCapillaryBoundaryCondition):
    alpha: float | sym.var


def get_shear_farfield(
    ambient_dim: int,
    *,
    alpha: float = 1.0,
    pinf: float = 0.0,
    capillary_number_name: str = "ca",
) -> FarfieldCapillaryBoundaryCondition:
    r"""Shear farfield flow with constant surface tension.

    .. math::

        \mathbf{u}^\infty = (\alpha y, 0, 0)

    :arg alpha: shear strength.
    """
    import pystopt.symbolic.sympy_interop as spi

    x = spi.cart_coords(ambient_dim)
    alpha = spi.to_sympy_variable(alpha)

    if not spi.is_variable(alpha):
        raise ValueError(f"'alpha' is not constant: {alpha}")

    uinf = make_obj_array([alpha * x[1], 0, 0])
    return _make_farfield_with_constant_pressure(
        uinf[:ambient_dim],
        pinf,
        capillary_number_name=capillary_number_name,
        cls=ShearFlowBoundaryCondition,
        # parameters
        alpha=alpha,
    )


# }}}


# {{{ solid body rotation


@dataclass(frozen=True)
class SolidBodyRotationBoundaryCondition(FarfieldCapillaryBoundaryCondition):
    omega: float | sym.var | np.ndarray


def get_solid_body_rotation_farfield(
    ambient_dim: int,
    *,
    omega: float | np.ndarray = 1.0,
    pinf: float = 0.0,
    capillary_number_name: str = "ca",
) -> FarfieldCapillaryBoundaryCondition:
    r"""Solid body rotation flow with constant surface tension.

    .. math::

        \mathbf{u}^\infty = \mathbf{\omega} \times \mathbf{x},

    where :math:`\mathbf{omega}` is the axis of the solid body rotation. If
    a scalar is given, the axis is set to :math:`(0, 0, \omega)`. For the 2D
    case, we *omega* can only be a scalar and the rotation is always in the
    :math:`x-y` plane.

    :arg omega: magnitude or axis of rotation.
    """
    import pystopt.symbolic.sympy_interop as spi

    x = spi.cart_coords(ambient_dim)
    omega = spi.to_sympy_variable(omega)

    if ambient_dim == 2:
        if spi.is_variable(omega):
            omega = make_obj_array([omega, omega])
        else:
            assert omega.size == ambient_dim

        uinf = make_obj_array([-omega[0] * x[1], omega[1] * x[0]])
    elif ambient_dim == 3:
        if spi.is_variable(omega):
            omega = make_obj_array([1.0e-16, 1.0e-16, omega])
        else:
            assert omega.size == ambient_dim

        uinf = np.cross(omega, x)
    else:
        raise ValueError(f"unsupported dimension: '{ambient_dim}'")

    return _make_farfield_with_constant_pressure(
        uinf,
        pinf,
        capillary_number_name=capillary_number_name,
        cls=SolidBodyRotationBoundaryCondition,
        # parameters
        omega=omega,
    )


# }}}


# {{{ get_helical_farfield


@dataclass(frozen=True)
class HelicalFlowBoundaryCondition(FarfieldCapillaryBoundaryCondition):
    tmax: sym.var
    height: sym.var
    omega: sym.var


def get_helical_farfield(
    ambient_dim: int,
    *,
    tmax: float | sym.var,
    height: float | sym.var = 1.0,
    omega: float | sym.var = 2.0 * np.pi,
    pinf: float | sym.var = 0.0,
    capillary_number_name: str = "ca",
) -> FarfieldCapillaryBoundaryCondition:
    if ambient_dim != 3:
        raise ValueError(f"invalid 'ambient_dim': expected 3, got {ambient_dim}")

    import pystopt.symbolic.sympy_interop as spi

    height = spi.to_sympy_variable(height)
    omega = spi.to_sympy_variable(omega)
    tmax = spi.to_sympy_variable(tmax)

    x = spi.cart_coords(ambient_dim)
    uinf = make_obj_array([
        -omega / tmax * x[1],
        omega / tmax * x[0],
        height / tmax,
    ])

    return _make_farfield_with_constant_pressure(
        uinf,
        pinf,
        capillary_number_name=capillary_number_name,
        cls=HelicalFlowBoundaryCondition,
        # parameters
        tmax=spi.to_pytential(ambient_dim, tmax),
        height=spi.to_pytential(ambient_dim, height),
        omega=spi.to_pytential(ambient_dim, omega),
    )


def find_bezier_helix_approximation(
    omega: float,
    height: float,
    *,
    n: int = 7,
    npoints: int = 256,
    x0: np.ndarray | None = None,
) -> tuple[np.ndarray, ...]:
    if x0 is None:
        x0 = np.array([1, 0, 0])

    t = np.linspace(0.0, 1.0, npoints)
    mat = np.stack(get_parametrized_bezier_basis(t, n))
    b = np.stack([
        np.cos(omega * t) * x0[0] - np.sin(omega * t) * x0[1] - x0[0],
        np.sin(omega * t) * x0[0] + np.cos(omega * t) * x0[1] - x0[1],
        height * t + x0[2],
    ])

    import scipy.linalg as sla

    p, _, _, _ = sla.lstsq(mat.T, b.T)
    p[0, :] = 0.0

    return tuple(p)


# }}}

# }}}
