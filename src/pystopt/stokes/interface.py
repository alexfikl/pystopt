# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

r"""
.. currentmodule:: pystopt.stokes

.. autoclass:: StokesInterface
    :no-show-inheritance:

The various functionality associated with the :class:`StokesInterface` is
implemented through a :func:`~functools.singledispatch` setup. Basically all
interested parties can implement a subset of the following functions by
registering it, e.g. using

.. code:: python

    @velocity.register(MyType)
    def _velocity_interface(self: MyType, *args, **kwargs):
        # compute some symbolic or explicit value related to the velocity
        return u

All the classes in this section implement a subset of this interface with
different input arguments.

.. function:: streamfunction(self: StokesInterface, *args, **kwargs)

.. function:: velocity(self: StokesInterface, *args, **kwargs)
.. function:: pressure(self: StokesInterface, *args, **kwargs)
.. function:: traction(self: StokesInterface, *args, **kwargs)

    The traction is the normal component of the stress tensor, i.e.
    :math:`\mathbf{n} \cdot \sigma \triangleq n_i \sigma_{ij}`.

.. function:: tangent_traction(self: StokesInterface, *args, **kwargs)

    This is the tangential component of the stress tensor, i.e.
    :math:`\mathbf{t}^\alpha \cdot \sigma \triangleq t_i^\alpha \sigma_{ij}`,
    where :math:`\alpha \in \{1, \dots, d - 1\}` is a basis for the
    tangent space.

.. function:: normal_velocity_gradient(self: StokesInterface, *args, **kwargs)

    This is the normal component of the velocity gradient, i.e.
    :math:`\mathbf{n} \cdot \nabla \mathbf{u} \triangleq n_i \nabla_i u_j`.

.. function:: tangent_velocity_gradient(self: StokesInterface, *args, **kwargs)

    This is the tangential component of the velocity gradient, i.e.
    :math:`\mathbf{t}^\alpha \cdot \nabla \mathbf{u} \triangleq
    t_i^\alpha \nabla_i u_j`.

.. function:: normal_velocity_laplacian(self: StokesInterface, *args, **kwargs)

    This is the normal component of the velocity Laplacian, i.e.
    :math:`\mathbf{n} \cdot \Delta \mathbf{u} \triangleq n_j \nabla_i \nabla_i u_j`.

.. function:: normal_velocity_hessian(self: StokesInterface, *args, **kwargs)

    This is the normal component of the velocity Hessian, i.e.
    :math:`\mathbf{n} \cdot \nabla \nabla \mathbf{u} \cdot \mathbf{n}
    \triangleq n_i n_k \nabla_i \nabla_k u_j`.

.. function:: deltaf(self: StokesInterface, *args, **kwargs)

    This is the jump in traction at the interface. In non-dimensional
    variables, for a viscosity ratio :math:`\lambda`, it is

    .. math::

        [[\mathbf{n} \cdot \sigma]] =
            \mathbf{n} \cdot \sigma_+ - \lambda \mathbf{n} \cdot \sigma_+.

.. function:: deltau(self: StokesInterface, *args, **kwargs)

    This is the jump in velocity at the interface. In all current applications,
    this is zero.
"""

from dataclasses import dataclass
from functools import singledispatch


@dataclass
class StokesInterface:
    """Generic interface for solutions and representations of Stokes flow.

    .. autoattribute:: ambient_dim
    .. automethod:: __init__
    """

    ambient_dim: int
    """Dimension of the ambient space."""


@singledispatch
def streamfunction(self: StokesInterface, *args, **kwargs):
    raise NotImplementedError(type(self).__name__)


@singledispatch
def pressure(self: StokesInterface, *args, **kwargs):
    raise NotImplementedError(type(self).__name__)


@singledispatch
def velocity(self: StokesInterface, *args, **kwargs):
    raise NotImplementedError(type(self).__name__)


@singledispatch
def traction(self: StokesInterface, *args, **kwargs):
    raise NotImplementedError(type(self).__name__)


@singledispatch
def tangent_traction(self: StokesInterface, *args, **kwargs):
    raise NotImplementedError(type(self).__name__)


@singledispatch
def normal_velocity_gradient(self: StokesInterface, *args, **kwargs):
    raise NotImplementedError(type(self).__name__)


@singledispatch
def tangent_velocity_gradient(self: StokesInterface, *args, **kwargs):
    raise NotImplementedError(type(self).__name__)


@singledispatch
def normal_velocity_laplacian(self: StokesInterface, *args, **kwargs):
    raise NotImplementedError(type(self).__name__)


@singledispatch
def normal_velocity_hessian(self: StokesInterface, *args, **kwargs):
    raise NotImplementedError(type(self).__name__)


@singledispatch
def deltaf(self: StokesInterface, *args, **kwargs):
    raise NotImplementedError(type(self).__name__)


@singledispatch
def deltau(self: StokesInterface, *args, **kwargs):
    return 0
