# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

"""
.. currentmodule:: pystopt.stokes

All subclasses of :class:`AnalyticStokesSolution` implement
:class:`pystopt.stokes.StokesInterface` with the arguments

.. code:: python

    @svelocity.register(AnalyticStokesSolution)
    def _velocity_solution(self: AnalyticStokesSolution, side: int):
        pass

and similar for the remamining variables. In the case of the jumps, the side
can be omitted for obvious reasons.

.. autoclass:: SympySolution
    :no-show-inheritance:
.. autoclass:: AnalyticStokesSolution
.. autoclass:: SphericalStokesSolution

.. autoclass:: PolarStokesSolution
.. autoclass:: HadamardRybczynski
.. autoclass:: Taylor
"""

from dataclasses import dataclass

import numpy as np
import sympy as sp

from pymbolic.interop.sympy import PymbolicToSympyMapper
from pytools import memoize_method
from pytools.obj_array import make_obj_array

import pystopt.stokes.interface as sti
from pystopt.stokes.boundary_conditions import TwoPhaseStokesBoundaryCondition
from pystopt.symbolic.sympy_interop import to_pytential

# {{{ base


def _make_sympy_solution_from_primitive(
    p: sp.Expr,
    u: np.ndarray,
    psi: sp.Expr | None = None,
    *,
    normal: np.ndarray | None = None,
    tangent: np.ndarray | None = None,
) -> "SympySolution":
    import pystopt.symbolic.sympy_interop as spi

    ambient_dim = len(u)

    if normal is None:
        normal = spi.normal(ambient_dim)

    f = spi.traction(p, u, n=normal)
    dudn = spi.normal_vector_gradient(u, n=normal)
    ddu = spi.vector_laplacian(u).dot(normal)
    ddudn = spi.normal_vector_hessian(u, n=normal).dot(normal)

    t = spi.tangent_traction(p, u, t=tangent)
    dudt = spi.tangent_vector_gradient(u, t=tangent)

    return SympySolution(
        pressure=p,
        velocity=u,
        traction=f,
        normal_velocity_gradient=dudn,
        normal_velocity_laplacian=ddu,
        normal_velocity_hessian=ddudn,
        tangent_traction=t,
        tangent_velocity_gradient=dudt,
        streamfunction=psi,
    )


@dataclass(frozen=True)
class SympySolution:
    """
    .. automethod:: __init__
    """

    # variables
    pressure: sp.Expr
    velocity: np.ndarray
    # first derivatives
    traction: np.ndarray
    normal_velocity_gradient: np.ndarray
    # tangents
    tangent_traction: np.ndarray
    tangent_velocity_gradient: np.ndarray
    # second derivatives
    normal_velocity_laplacian: sp.Expr
    normal_velocity_hessian: sp.Expr

    streamfunction: sp.Expr = None

    @property
    def ambient_dim(self):
        return self.velocity.size


class AnalyticStokesSolution(sti.StokesInterface):
    r"""Analytical solution to a two-phase Stokes flow.

    .. autoattribute:: viscosity_ratio_name
    .. autoattribute:: capillary_number_name

    .. autoproperty:: farfield
    .. autoproperty:: as_sympy

    .. automethod:: __getitem__
    """

    def __init__(
        self,
        ambient_dim: int,
        *,
        viscosity_ratio_name: str = "viscosity_ratio",
        capillary_number_name: str = "ca",
    ) -> None:
        super().__init__(ambient_dim)

        self.viscosity_ratio_name: str = viscosity_ratio_name
        """A name for the viscosity ratio symbolic variable."""
        self.capillary_number_name: str = capillary_number_name
        """A name for the capillary number symbolic variable."""

    @property
    def farfield(self) -> TwoPhaseStokesBoundaryCondition:
        """The farfield boundary conditions for this solution."""
        raise NotImplementedError

    @property
    def sympy_solution(self) -> tuple[None, SympySolution, SympySolution]:
        """A tuple of ``(None, ext, int)``, where ``ext`` and ``int`` are
        the exterior and interior solutions as :mod:`sympy` expressions.
        For the curious, the extra *None* is simply there so we can into the
        tuple with ±1.
        """
        raise NotImplementedError

    @property
    @memoize_method
    def as_sympy(self) -> PymbolicToSympyMapper:
        """A mapper used to translate an expression to :mod:`sympy`."""
        return PymbolicToSympyMapper()

    def __getitem__(self, side: int) -> SympySolution:
        """
        :arg side: interior (``-1``) or exterior (``+1``) solution.
        :returns: a :class:`SympySolution` with the solution on the respective
            side.
        """

        if side not in {-1, +1}:
            raise IndexError(f"'side' not in {{-1, +1}}: {side}")

        return self.sympy_solution[side]


@sti.streamfunction.register(AnalyticStokesSolution)
def _streamfunction_sympy_solution(self: AnalyticStokesSolution, side: int):
    return to_pytential(self.ambient_dim, self[side].streamfunction)


@sti.velocity.register(AnalyticStokesSolution)
def _velocity_sympy_solution(self: AnalyticStokesSolution, side: int):
    return to_pytential(self.ambient_dim, self[side].velocity)


@sti.pressure.register(AnalyticStokesSolution)
def _pressure_sympy_solution(self: AnalyticStokesSolution, side: int):
    return to_pytential(self.ambient_dim, self[side].pressure)


@sti.traction.register(AnalyticStokesSolution)
def _traction_sympy_solution(self: AnalyticStokesSolution, side: int):
    return to_pytential(self.ambient_dim, self[side].traction)


@sti.tangent_traction.register(AnalyticStokesSolution)
def _tangent_traction_sympy_solution(
    self: AnalyticStokesSolution, tangent_idx: int, side: int
):
    return to_pytential(self.ambient_dim, self[side].tangent_traction[tangent_idx])


@sti.normal_velocity_gradient.register(AnalyticStokesSolution)
def _normal_velocity_gradient_sympy_solution(self: AnalyticStokesSolution, side: int):
    return to_pytential(self.ambient_dim, self[side].normal_velocity_gradient)


@sti.tangent_velocity_gradient.register(AnalyticStokesSolution)
def _tangent_velocity_gradient_sympy_solution(
    self: AnalyticStokesSolution, tangent_idx: int, side: int
):
    return to_pytential(
        self.ambient_dim, self[side].tangent_velocity_gradient[tangent_idx]
    )


@sti.normal_velocity_laplacian.register(AnalyticStokesSolution)
def _normal_velocity_laplacian_sympy_solution(self: AnalyticStokesSolution, side: int):
    return to_pytential(self.ambient_dim, self[side].normal_velocity_laplacian)


@sti.normal_velocity_hessian.register(AnalyticStokesSolution)
def _normal_velocity_hessian_sympy_solution(self: AnalyticStokesSolution, side: int):
    return to_pytential(self.ambient_dim, self[side].normal_velocity_hessian)


@sti.deltaf.register(AnalyticStokesSolution)
def _deltaf_sympy_solution(self: AnalyticStokesSolution, side: int = 0):
    viscosity_ratio = sp.Symbol(self.viscosity_ratio_name)
    f_ext = self[+1].traction
    f_int = self[-1].traction

    return to_pytential(self.ambient_dim, f_ext - viscosity_ratio * f_int)


# }}}


# {{{ spherical solutions


def _polar_velocity(pr, c):
    from pystopt.symbolic.sympy_interop import polar_coords, sph_from_cart_coords

    r, theta = polar_coords()
    r_, theta_ = sph_from_cart_coords(pr.ambient_dim)
    to_cart = [(r, r_), (theta, theta_)]

    # streamfunction
    psi = (c[0] / r + c[1] * r + c[2] * r * sp.log(r) + c[3] * r**3) * sp.sin(theta)

    # velocity field by ways of the streamfunction
    u_r = psi.diff(theta) / r
    u_theta = -psi.diff(r)

    # rotate from spherical to cartesian
    from pystopt.symbolic.sympy_interop import cylindrical_to_cartesian_rotation

    rot = cylindrical_to_cartesian_rotation()
    u_x, u_y, _ = rot @ make_obj_array([u_r, u_theta, 0])

    return (
        psi.subs(to_cart),
        make_obj_array([u_x.subs(to_cart), u_y.subs(to_cart)]),
    )


def _polar_farfield(pr):
    vlambda = sp.Symbol(pr.viscosity_ratio_name)
    c = 2 * pr.alpha * (1 + vlambda) / vlambda
    uinf = make_obj_array([pr.uinf + c, sp.Number(0)])

    from pystopt.stokes.boundary_conditions import _make_farfield_with_constant_pressure

    farfield = _make_farfield_with_constant_pressure(
        uinf,
        pr.pinf,
        capillary_number_name=pr.capillary_number_name,
        normal=pr.normal,
        tangent=pr.tangent,
    )

    from dataclasses import replace

    return replace(farfield, deltafs=(lambda: sti.deltaf(pr),))


class PolarStokesSolution(AnalyticStokesSolution):
    """Two-phase Stokes solutions on a circle.

    .. autoattribute:: radius
    .. autoattribute:: uinf
    .. autoattribute:: pinf
    .. autoattribute:: alpha

    .. autoproperty:: normal
    .. autoproperty:: tangent
    .. autoproperty:: curvature
    """

    def __init__(
        self,
        *,
        radius: float = 1.0,
        uinf: float = 1.0,
        pinf: float = 0.0,
        alpha: float | None = None,
        viscosity_ratio_name: str = "viscosity_ratio",
        capillary_number_name: str = "ca",
    ):
        if abs(radius - 1.0) > 1.0e-14:
            raise NotImplementedError("dimensional solution with R != 1")

        super().__init__(
            ambient_dim=2,
            viscosity_ratio_name=viscosity_ratio_name,
            capillary_number_name=capillary_number_name,
        )

        if alpha is None:
            alpha = -uinf

        self.radius: float = radius
        """Radius of the circle representing the droplet interface."""
        self.uinf: float = uinf
        """Farfield velocity field."""
        self.pinf: float = pinf
        """Farfield pressure value."""
        self.alpha: float = alpha
        """Additional parameter used to determine a unique solution. A special
        value of this parameter leads to a steady state solution.
        """

    @property
    def normal(self) -> np.ndarray:
        """Exterior normal vector on the circle."""
        from pystopt.symbolic.sympy_interop import cart_coords

        return cart_coords(self.ambient_dim) / self.radius

    @property
    def tangent(self) -> np.ndarray:
        """Tangent vector on the circle (counterclockwise)."""
        # NOTE: needs to match pystopt.symbolic.primitives.tangential_onb
        nx, ny = self.normal
        return make_obj_array([ny, -nx])

    @property
    def curvature(self) -> float:
        """Curvature of the circle (constant based on the radius)."""
        return (self.ambient_dim - 1) / self.radius

    @property
    @memoize_method
    def farfield(self):
        return _polar_farfield(self)

    @property
    @memoize_method
    def sympy_solution(self):
        # transform user-provided variables to sympy symbols
        alpha = self.as_sympy(self.alpha)
        vlambda = sp.Symbol(self.viscosity_ratio_name)
        capillary_number = sp.Symbol(self.capillary_number_name)

        # jump in normal traction (this is a constant on the circle)
        gamma = self.curvature / capillary_number

        # coordinates
        from pystopt.symbolic.sympy_interop import sph_from_cart_coords

        r, theta = sph_from_cart_coords(self.ambient_dim)

        # {{{ exterior solution

        # A_+ B_+ C_+ D_+
        c_ext = [alpha, self.uinf, 2 * alpha * (1 + vlambda) / vlambda, 0]

        psi_ext, u_ext = _polar_velocity(self, c_ext)
        p_ext = self.pinf - 2 * c_ext[2] / r * sp.cos(theta)
        sext = _make_sympy_solution_from_primitive(
            p_ext, u_ext, psi_ext, normal=self.normal, tangent=self.tangent
        )

        # }}}

        # {{{ interior solution

        # A_- B_- C_- D_-
        c_int = [
            0,
            self.uinf - alpha * (1 - vlambda) / vlambda,
            0,
            alpha / vlambda,
        ]

        sint = {}
        psi_int, u_int = _polar_velocity(self, c_int)
        p_int = (self.pinf + gamma) / vlambda + 8 * c_int[3] * r * sp.cos(theta)
        sint = _make_sympy_solution_from_primitive(
            p_int, u_int, psi_int, normal=self.normal, tangent=self.tangent
        )

        # }}}

        return None, sext, sint


# }}}


# {{{ spherical solutions


class SphericalStokesSolution(AnalyticStokesSolution):
    """Two-phase Stokes solutions on a sphere.

    .. autoproperty:: normal
    .. autoproperty:: summed_curvature
    """

    def __init__(
        self,
        *,
        radius=1.0,
        viscosity_ratio_name="viscosity_ratio",
        capillary_number_name="ca",
    ):
        super().__init__(
            ambient_dim=3,
            viscosity_ratio_name=viscosity_ratio_name,
            capillary_number_name=capillary_number_name,
        )

        self.radius = radius

    @property
    def normal(self) -> np.ndarray:
        r"""Exterior normal vector on the sphere:

        .. math::

            \mathbf{n} = \frac{\mathbf{x}}{R}.
        """
        from pystopt.symbolic.sympy_interop import cart_coords

        return cart_coords(self.ambient_dim) / self.radius

    @property
    def summed_curvature(self) -> float:
        r"""Summed curvature of the sphere (sum of principal curvatures)

        .. math::

            \kappa = \frac{d - 1}{R}.
        """

        return (self.ambient_dim - 1) / self.radius


# }}}


# {{{ Hadamard-Rybczynski


def _hr_velocity(hr, c):
    from pystopt.symbolic.sympy_interop import polar_coords, sph_from_cart_coords

    r, theta = polar_coords()
    r_, theta_, _ = sph_from_cart_coords(ambient_dim=hr.ambient_dim)
    to_cart = [(r, r_), (theta, theta_)]

    # streamfunction
    psi = (c[0] / r + c[1] * r + c[2] * r**2 + c[3] * r**4) * sp.sin(theta) ** 2

    # velocity field by ways of the Stokes streamfunction
    u_r = +1.0 / (r**2 * sp.sin(theta)) * psi.diff(theta)
    u_theta = -1.0 / (r * sp.sin(theta)) * psi.diff(r)

    # rotate from spherical to cartesian
    from pystopt.symbolic.sympy_interop import sph_to_cartesian_rotation

    rot = sph_to_cartesian_rotation()
    u_x, u_y, u_z = rot @ make_obj_array([u_r, u_theta, 0])

    return psi.subs(to_cart), make_obj_array([
        u_x.subs(to_cart),
        u_y.subs(to_cart),
        u_z.subs(to_cart),
    ])


class HadamardRybczynski(SphericalStokesSolution):
    r"""Hadamard-Rybczynski solution.

    The Hadamard-Rybczynski solution is an axisymmetric solution in a
    uniform farfield flow. It allows stream function solutions of the form

    .. math::

        \psi_\pm = \left(
        \frac{A_\pm}{r} + B_\pm r + C_\pm r^2 + D_\pm r^4
        \right) \sin^2 \theta,

    where the constants are determined (as a one-parameter family) by the
    boundary conditions

    * mass conservation at the interface
      :math:`[\![ \mathbf{u} ]\!] = 0`.
    * jump in normal traction at the interface
      :math:`[\![ \mathbf{f} \cdot \mathbf{n} ]\!] = \kappa / \mathrm{Ca}`.
    * jump in tangential traction at the interface
      :math:`[\![ \mathbf{f} \cdot \mathbf{t} ]\!] = 0`.
    * finite solutions at the origin and at infinity.

    For details, see Equation 3-7 in [Clift1978]_ and the surrounding
    discussion.


    .. automethod:: __init__
    """

    def __init__(
        self,
        *,
        radius=1.0,
        alpha=None,
        uinf=1.0,
        pinf=0.0,
        capillary_number_name="ca",
        viscosity_ratio_name="viscosity_ratio",
    ):
        """
        :arg alpha: parameter for the solution. If *None*, the classic solution
            from [Clift1978]_ is computed.
        :arg uinf: magnitude of the :math:`x` component of farfield velocity field.
        :arg pinf: farfield pressure field.
        """
        super().__init__(
            radius=radius,
            capillary_number_name=capillary_number_name,
            viscosity_ratio_name=viscosity_ratio_name,
        )

        if alpha is None:
            from pystopt import sym

            vlambda = sym.var(self.viscosity_ratio_name)
            alpha = self.radius**3 / 4 * vlambda / (1 + vlambda)

        self.alpha = alpha
        self.uinf = uinf
        self.pinf = pinf

    @property
    @memoize_method
    def farfield(self):
        from pystopt.stokes.boundary_conditions import get_uniform_farfield

        farfield = get_uniform_farfield(
            self.ambient_dim,
            uinf=self.uinf,
            pinf=self.pinf,
            capillary_number_name=self.capillary_number_name,
        )

        from dataclasses import replace

        return replace(farfield, deltafs=(lambda: sti.deltaf(self),))

    @property
    @memoize_method
    def sympy_solution(self):
        # transform user-provided variables to sympy symbols
        radius = self.as_sympy(self.radius)
        alpha = self.as_sympy(self.alpha)
        vlambda = sp.Symbol(self.viscosity_ratio_name)
        capillary_number = sp.Symbol(self.capillary_number_name)

        # jump in normal traction (this is a constant on the sphere)
        gamma = 1.0 / capillary_number * self.summed_curvature

        # coordinates
        from pystopt.symbolic.sympy_interop import sph_from_cart_coords

        r, theta, _ = sph_from_cart_coords(self.ambient_dim)

        # {{{ exterior solution

        c_ext = [
            alpha,
            -alpha * (2 + 3 * vlambda) / vlambda / radius**2,
            self.uinf / 2,
            0,
        ]

        psi_ext, u_ext = _hr_velocity(self, c_ext)
        p_ext = self.pinf + 2 * c_ext[1] / r**2 * sp.cos(theta)
        sext = _make_sympy_solution_from_primitive(
            p_ext, u_ext, psi_ext, normal=self.normal, tangent=None
        )

        # }}}

        # {{{ interior solution

        c_int = [
            0,
            0,
            0.5 - alpha * (3 + 2 * vlambda) / vlambda / radius**3,
            alpha / vlambda / radius**5,
        ]

        sint = {}
        psi_int, u_int = _hr_velocity(self, c_int)
        p_int = (self.pinf + gamma) / vlambda + 20 * c_int[3] * r * sp.cos(theta)
        sint = _make_sympy_solution_from_primitive(
            p_int, u_int, psi_int, normal=self.normal, tangent=None
        )

        # }}}

        return None, sext, sint


# }}}


# {{{ Taylor


class Taylor(SphericalStokesSolution):
    """Taylor solution.

    This is a fully three-dimensional solution in an extensional flow.
    For details see [Taylor1932]_.

    .. automethod:: __init__
    """

    def __init__(
        self,
        *,
        radius=1.0,
        alpha=1.0,
        pinf=0.0,
        capillary_number_name="ca",
        viscosity_ratio_name="viscosity_ratio",
    ):
        """
        :arg alpha: shear strength.
        :arg pinf: farfield pressure field.
        """
        super().__init__(
            radius=radius,
            capillary_number_name=capillary_number_name,
            viscosity_ratio_name=viscosity_ratio_name,
        )

        self.alpha = alpha
        self.pinf = pinf

    @property
    @memoize_method
    def farfield(self):
        from pystopt.stokes.boundary_conditions import get_extensional_farfield

        farfield = get_extensional_farfield(
            self.ambient_dim,
            alpha=self.alpha / 2.0,
            pinf=self.pinf,
            axis=-1,
            capillary_number_name=self.capillary_number_name,
        )

        from dataclasses import replace

        return replace(farfield, deltafs=(lambda: sti.deltaf(self),))

    @property
    @memoize_method
    def sympy_solution(self):
        # transform user-provided variables to sympy symbols
        radius = self.as_sympy(self.radius)
        alpha = self.as_sympy(self.alpha)
        vlambda = sp.Symbol(self.viscosity_ratio_name)
        capillary_number = sp.Symbol(self.capillary_number_name)

        # jump in normal traction (this is a constant on the sphere)
        gamma = 1.0 / capillary_number * self.summed_curvature

        # coordinates
        from pystopt.symbolic.sympy_interop import cart_coords

        x, y, z = cart_coords(self.ambient_dim)
        r = sp.sqrt(x**2 + y**2 + z**2)

        # {{{ exterior solution

        a1 = radius**3 * (-alpha / 2.0 * (2.0 + 5.0 * vlambda) / (1.0 + vlambda))
        a2 = radius**5 * (-alpha / 4.0 * vlambda / (1.0 + vlambda))

        u = (
            0.5 * alpha * x
            + 0.5 * a1 * x * (x**2 - y**2) / r**5
            + a2 * (-5.0 * x * (x**2 - y**2) / r**7 + 2.0 * x / r**5)
        )
        v = (
            -0.5 * alpha * y
            + 0.5 * a1 * y * (x**2 - y**2) / r**5
            + a2 * (-5.0 * y * (x**2 - y**2) / r**7 - 2.0 * y / r**5)
        )
        w = 0.5 * a1 * z * (x**2 - y**2) / r**5 + a2 * (-5.0 * z * (x**2 - y**2) / r**7)

        p_ext = self.pinf + a1 * (x**2 - y**2) / r**5
        u_ext = make_obj_array([u, v, w])
        sext = _make_sympy_solution_from_primitive(
            p_ext, u_ext, normal=self.normal, tangent=None
        )

        # }}}

        # {{{ interior solution

        b1 = 21.0 / 4.0 * alpha / (1.0 + vlambda) / radius**2
        b2 = -3.0 / 8.0 * alpha / (1.0 + vlambda)

        u = b1 * (5.0 / 21.0 * x * r**2 - 2.0 / 21.0 * x * (x**2 - y**2)) + 2.0 * b2 * x
        v = (
            b1 * (-5.0 / 21.0 * y * r**2 - 2.0 / 21.0 * y * (x**2 - y**2))
            - 2.0 * b2 * y
        )
        w = b1 * (-2.0 / 21.0 * z * (x**2 - y**2))

        p_int = (self.pinf + gamma) / vlambda + b1 * (x**2 - y**2)
        u_int = make_obj_array([u, v, w])
        sint = _make_sympy_solution_from_primitive(
            p_int, u_int, normal=self.normal, tangent=None
        )

        # }}}

        return None, sext, sint


# }}}

# vim: fdm=marker
