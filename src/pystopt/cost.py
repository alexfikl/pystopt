# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

"""
All the cost functionals here implement the derivatives defined in
:mod:`pystopt.derivatives`. All the implementations have the signature

.. function:: func(self: ShapeFunctional, ambient_dim, dim, dofdesc)

Specific functionals can be obtained programmatically using

.. autofunction:: cost_functional_from_name

All cost functionals should inherit from

.. autoclass:: ShapeFunctional
.. autoclass:: AXPBYFunctional
.. autoclass:: GeometryShapeFunctional

Some example geometry-based shape functionals (that have all zero derivatives
with respect to the Stokes variables). First, we have a set of geometry
tracking cost functionals in various (possibly time-dependent) flows.

.. autoclass:: GeometryTrackingFunctional
.. autoclass:: UniformGeometryTrackingFunctional
.. autoclass:: SolidBodyRotationGeometryTrackingFunctional
.. autoclass:: BezierGeometryTrackingFunctional
.. autoclass:: HelixGeometryTrackingFunctional

Similarly, we have a set of centroid tracking cost functionals.

.. autoclass:: CentroidTrackingFunctional
.. autoclass:: UniformCentroidTrackingFunctional
.. autoclass:: SolidBodyRotationCentroidTrackingFunctional
.. autoclass:: ExtensionalCentroidTrackingFunctional
.. autoclass:: HelixCentroidTrackingFunctional

Other examples of cost functionals that only depend on the geometry are also
given.

.. autoclass:: AreaTrackingFunctional
.. autoclass:: VolumeTrackingFunctional
.. autoclass:: EnergyFunctional
.. autoclass:: TangentialGradientFunctional

Finally, some example Stokes-based shape functionals are given.

.. autoclass:: NormalVelocityFunctional
.. autofunction:: get_desired_normal_velocity
.. autofunction:: make_normal_velocity_functional_from_stokes

The following cost functionals are provided for testing only (used to
verify the shape gradients of each quantity).

.. autoclass:: NormalTestFunctional
.. autoclass:: CurvatureTestFunctional
.. autoclass:: CurvatureVectorTestFunctional
"""

from dataclasses import dataclass
from numbers import Real
from typing import Any

import numpy as np

from arraycontext import ArrayContext
from pytential import GeometryCollection
from pytools.obj_array import make_obj_array

from pystopt import grad, sym
from pystopt.stokes import TwoPhaseStokesRepresentation
from pystopt.tools import get_default_logger

logger = get_default_logger(__name__)


# {{{ utils


def cost_functional_from_name(name: str, **kwargs: Any) -> "ShapeFunctional":
    """
    :arg name: name of the cost functional. The stringified name is just the
        name without the `Functional` part joined by underscores. For example,
        for :class:`GeometryTrackingFunctional` is ``"geometry_tracking"``.
    """
    name_to_cls: dict[str, type[ShapeFunctional]] = {
        "geometry_tracking": GeometryTrackingFunctional,
        "uniform_geometry_tracking": UniformGeometryTrackingFunctional,
        "sbr_geometry_tracking": SolidBodyRotationGeometryTrackingFunctional,
        "bezier_geometry_tracking": BezierGeometryTrackingFunctional,
        "helix_geometry_tracking": HelixGeometryTrackingFunctional,
        "centroid_tracking": CentroidTrackingFunctional,
        "uniform_centroid_tracking": UniformCentroidTrackingFunctional,
        "sbr_centroid_tracking": SolidBodyRotationCentroidTrackingFunctional,
        "extensional_centroid_tracking": ExtensionalCentroidTrackingFunctional,
        "helix_centroid_tracking": HelixCentroidTrackingFunctional,
        "area_tracking": AreaTrackingFunctional,
        "volume_tracking": VolumeTrackingFunctional,
        "energy": EnergyFunctional,
        "tangential_gradient": TangentialGradientFunctional,
        "normal_velocity": NormalVelocityFunctional,
    }

    if isinstance(name, str):
        if name not in name_to_cls:
            raise ValueError(f"no known cost functional with name '{name}'")

        cls = name_to_cls[name]
        if __debug__:
            from dataclasses import fields

            attrs = {f.name for f in fields(cls)}
            for key in kwargs:
                if key not in attrs:
                    raise ValueError(f"'{key}' not an attribute of '{cls.__name__}'")
    else:
        assert issubclass(cls, ShapeFunctional)

    return cls(**kwargs)  # type: ignore[call-arg]


# }}}


# {{{ interface


@dataclass(frozen=True)
class ShapeFunctional:
    """A generic shape functional.

    Shape functionals also support basic arithmetic, i.e. multiplication /
    division by numeric constants and addition / subtraction between
    functionals. These operations result in a :class:`AXPBYFunctional`.

    .. automethod:: __call__
    """

    # {{{ arithmetic

    def __mul__(self, other):
        if not isinstance(other, Real):
            raise NotImplementedError

        if other == 0:
            return 0

        if other == 1:
            return self

        if isinstance(self, AXPBYFunctional):
            func = self.func
            factor = [other * f for f in self.factor]  # pylint: disable=E1101
        else:
            func, factor = [self], [other]

        return AXPBYFunctional(tuple(func), tuple(factor))

    def __rmul__(self, other):
        return self.__mul__(other)

    def __truediv__(self, other):
        if not isinstance(other, Real):
            raise NotImplementedError

        return self.__mul__(1 / other)

    def __rtruediv__(self, other):
        if not isinstance(other, Real):
            raise NotImplementedError

        return self.__mul__(1 / other)

    def __add__(self, other):
        if other == 0:
            return self

        if not isinstance(other, ShapeFunctional):
            raise NotImplementedError

        if isinstance(self, AXPBYFunctional):
            func = self.func
            factor = self.factor
        else:
            func, factor = (self,), (1,)

        if isinstance(other, AXPBYFunctional):
            func = func + other.func
            factor = factor + other.factor
        else:
            func = (*func, other)
            factor = (*factor, 1)

        return AXPBYFunctional(func, factor)

    def __radd__(self, other):
        return self.__add__(other)

    def __sub__(self, other):
        return self.__add__((-1) * other)

    # }}}

    def __call__(
        self,
        ambient_dim: int,
        *,
        dim: int | None = None,
        dofdesc: sym.DOFDescriptorLike | None = None,
    ) -> sym.ArithmeticExpression:
        raise NotImplementedError


@dataclass(frozen=True)
class AXPBYFunctional(ShapeFunctional):
    r"""A linear combination of shape functionals of the form

    .. math::

        \mathcal{J} = \sum f_i \mathcal{J}_i,

    where :math:`f_i` are given by :attr:`factor` and :math:`J_i` are given by
    :attr:`func`.

    .. autoattribute:: func
    .. autoattribute:: factor
    """

    func: tuple[ShapeFunctional, ...]
    r"""A tuple of :class:`ShapeFunctional`\ s."""
    factor: tuple[Real, ...]
    """A tuple of real numbers multiplying the functionals in :attr:`func`."""

    # {{{

    # FIXME: this is horribly hacky!

    @property
    def dofdesc(self):
        (dofdesc,) = (func.dofdesc for func in self.func if hasattr(func, "dofdesc"))
        return dofdesc

    def nodes(self):
        (xd,) = (func.nodes() for func in self.func if hasattr(func, "nodes"))
        return xd

    # }}}

    def __call__(
        self,
        ambient_dim: int,
        dim: int | None = None,
        dofdesc: sym.DOFDescriptorLike | None = None,
    ) -> sym.ArithmeticExpression:
        return sum(
            factor * func(ambient_dim, dim=dim, dofdesc=dofdesc)
            for factor, func in zip(self.factor, self.func, strict=False)
        )


@grad.shape.register(AXPBYFunctional)
def _grad_shape_axpby(
    self: AXPBYFunctional,
    ambient_dim: int,
    *,
    dim: int | None = None,
    dofdesc: sym.DOFDescriptorLike | None = None,
) -> np.ndarray:
    return sum(
        (
            factor * grad.shape(func, ambient_dim, dim=dim, dofdesc=dofdesc)
            for factor, func in zip(self.factor, self.func, strict=False)
        ),
        np.array(0),
    )


@grad.velocity.register(AXPBYFunctional)
def _grad_velocity_axpby(
    self: AXPBYFunctional,
    ambient_dim: int,
    *,
    dim: int | None = None,
    dofdesc: sym.DOFDescriptorLike | None = None,
) -> np.ndarray:
    return sum(
        (
            factor * grad.velocity(func, ambient_dim, dim=dim, dofdesc=dofdesc)
            for factor, func in zip(self.factor, self.func, strict=False)
        ),
        np.array(0),
    )


@grad.traction.register(AXPBYFunctional)
def _grad_traction_axpby(
    self: AXPBYFunctional,
    ambient_dim: int,
    *,
    dim: int | None = None,
    dofdesc: sym.DOFDescriptorLike | None = None,
) -> np.ndarray:
    return sum(
        (
            factor * grad.traction(func, ambient_dim, dim=dim, dofdesc=dofdesc)
            for factor, func in zip(self.factor, self.func, strict=False)
        ),
        np.array(0),
    )


@grad.capillary.register(AXPBYFunctional)
def _grad_capillary_axpby(
    self: AXPBYFunctional,
    ambient_dim: int,
    *,
    dim: int | None = None,
    dofdesc: sym.DOFDescriptorLike | None = None,
) -> sym.ArithmeticExpression:
    return sum(
        factor * grad.capillary(func, ambient_dim, dim=dim, dofdesc=dofdesc)
        for factor, func in zip(self.factor, self.func, strict=False)
    )


@grad.farfield.register(AXPBYFunctional)
def _grad_farfield_axpby(
    self: AXPBYFunctional,
    ambient_dim: int,
    *,
    dim: int | None = None,
    dofdesc: sym.DOFDescriptorLike | None = None,
) -> sym.ArithmeticExpression:
    return sum(
        factor * grad.farfield(func, ambient_dim, dim=dim, dofdesc=dofdesc)
        for factor, func in zip(self.factor, self.func, strict=False)
    )


# }}}


# {{{ geometry


@dataclass(frozen=True)
class GeometryShapeFunctional(ShapeFunctional):
    """Shape functional that only depends on the geometry.

    For example, the Stokes velocity field (or other variables) does not
    appear in the cost functional. This then sets the relevant derivatives to
    zero.
    """


@grad.velocity.register(GeometryShapeFunctional)
def _grad_velocity_geometry(
    self: GeometryShapeFunctional,
    ambient_dim: int,
    *,
    dim: int | None = None,
    dofdesc: sym.DOFDescriptorLike | None = None,
) -> np.ndarray:
    return make_obj_array([0, 0, 0][:ambient_dim])


@grad.traction.register(GeometryShapeFunctional)
def _grad_traction_geometry(
    self: GeometryShapeFunctional,
    ambient_dim: int,
    *,
    dim: int | None = None,
    dofdesc: sym.DOFDescriptorLike | None = None,
) -> np.ndarray:
    return make_obj_array([0, 0, 0][:ambient_dim])


@grad.capillary.register(GeometryShapeFunctional)
def _grad_capillary_geometry(
    self: GeometryShapeFunctional,
    ambient_dim: int,
    *,
    dim: int | None = None,
    dofdesc: sym.DOFDescriptorLike | None = None,
) -> sym.ArithmeticExpression:
    return 0


@grad.farfield.register(GeometryShapeFunctional)
def _grad_farfield_geometry(
    self: GeometryShapeFunctional,
    ambient_dim: int,
    *,
    dim: int | None = None,
    dofdesc: sym.DOFDescriptorLike | None = None,
) -> np.ndarray:
    return np.array(0.0)


# }}}


# {{{ geometry tracking


@dataclass(frozen=True)
class GeometryTrackingFunctional(GeometryShapeFunctional):
    r"""Implements the evaluation and gradient for the cost functional

    .. math::

        \mathcal{J} = \frac{1}{2} \int_{\Sigma(t)}
            \|\mathbf{X}(t) - \mathbf{X}_d(t)\|^2 \,\mathrm{d}S.

    where :math:`t` is a parameter for the interface (in practice, this is
    time).

    .. attribute:: ambient_dim

    .. attribute:: t

        A time-like parameter for the geometry, if any.

    .. attribute:: xd

        Reference desired geometry coordinates. To get the actual coordinates
        for use :meth:`nodes`, as they can depend on other parameters in the
        class.

    .. attribute:: dofdesc

        A :class:`~pytential.symbolic.dof_desc.DOFDescriptor` for the
        geometry to which :attr:`xd` belongs to, if any.

    .. attribute:: allow_tangential_gradient

        If *False*, only the normal component of the gradient is used. Otherwise,
        a tangential component is present, that can allow better matching
        discrete points.

    .. automethod:: nodes
    """

    xd: np.ndarray

    t: sym.var | None
    dofdesc: sym.DOFDescriptor | None
    allow_tangential_gradient: bool

    @property
    def ambient_dim(self):
        return self.xd.size

    def nodes(self) -> np.ndarray:
        """
        :returns: an expression for the nodes based on :attr:`xd` and other
            parameters describing the desired geometry.
        """
        return self.xd

    def __call__(
        self,
        ambient_dim: int,
        *,
        dim: int | None = None,
        dofdesc: sym.DOFDescriptorLike | None = None,
    ) -> sym.ArithmeticExpression:
        if dim is None:
            dim = ambient_dim - 1

        f = sym.cse(sym.nodes(ambient_dim, dofdesc=dofdesc).as_vector() - self.nodes())

        return sym.sintegral((f @ f) / 2, ambient_dim, dim, dofdesc=dofdesc)


@grad.shape.register(GeometryTrackingFunctional)
def _grad_shape_geometry_tracking(
    self: GeometryTrackingFunctional,
    ambient_dim: int,
    *,
    dim: int | None = None,
    dofdesc: sym.DOFDescriptorLike | None = None,
) -> sym.ArithmeticExpression:
    f = sym.cse(sym.nodes(ambient_dim, dofdesc=dofdesc).as_vector() - self.nodes())

    n = sym.normal(ambient_dim, dim=dim, dofdesc=dofdesc).as_vector()
    kappa = sym.summed_curvature(ambient_dim, dim=dim, dofdesc=dofdesc)

    if self.allow_tangential_gradient:
        return f + kappa * (f @ f) / 2 * n
    else:
        return (f @ n + kappa * (f @ f) / 2) * n


# }}}


# {{{ time-dependent geometry tracking


@dataclass(frozen=True)
class UniformGeometryTrackingFunctional(GeometryTrackingFunctional):
    r"""Implements time-dependent geometry tracking in a uniform flow.

    The desired geometry :math:`\mathbf{X}_d` is parametrized by a time-like
    variable. In this case, the desired geometry is given by

    .. math::

        \mathbf{X}_d(t) \triangleq \mathbf{x}_d + t \mathbf{u}^\infty,

    where :math:`\mathbf{u}^\infty` is a uniform farfield velocity field that
    translates the given geometry.

    See also :func:`~pystopt.stokes.get_uniform_farfield`.

    .. attribute:: uinf_d

        Uniform velocity field that translates the geometry
        :attr:`~GeometryTrackingFunctional.xd`.
    """

    uinf_d: np.ndarray

    def __post_init__(self):
        if self.uinf_d.shape != self.xd.shape:
            raise ValueError(
                "'uinf_d' and 'xd' shapes must match: "
                f"uinf_d {self.uinf_d.shape} != xd {self.xd.shape}"
            )

    def nodes(self):
        return self.xd + self.t * self.uinf_d


def _get_solid_body_rotation_matrix(
    ambient_dim: int, t: sym.var, omega_d: np.ndarray
) -> np.ndarray:
    rot = np.empty((ambient_dim, ambient_dim), dtype=object)
    if ambient_dim == 2:
        sqrt_a = sym.sqrt(omega_d[0]) / sym.sqrt(omega_d[1])
        sqrt_b = sym.sqrt(omega_d[0] * omega_d[1])

        rot[0, 0] = sym.cos(sqrt_b * t)
        rot[0, 1] = -sqrt_a * sym.sin(sqrt_b * t)
        rot[1, 0] = sym.sin(sqrt_b * t) / sqrt_a
        rot[1, 1] = sym.cos(sqrt_b * t)
    elif ambient_dim == 3:
        a, b, c = omega_d
        abc = a**2 + b**2 + c**2
        ct = sym.cos(sym.sqrt(abc) * t)
        st = sym.sin(sym.sqrt(abc) * t)

        # NOTE: obtained by solving
        #   x_d'(t) = omega_d \times x_d(t)

        rot[0, 0] = a**2 / abc + b**2 * ct / abc + c**2 * ct / abc
        rot[0, 1] = a * b / abc - a * b * ct / abc - c * st / sym.sqrt(abc)
        rot[0, 2] = a * c / abc - a * c * ct / abc + b * st / sym.sqrt(abc)

        rot[1, 0] = a * b / abc - a * b * ct / abc + c * st / sym.sqrt(abc)
        rot[1, 1] = b**2 / abc + a**2 * ct / abc + c**2 * ct / abc
        rot[1, 2] = b * c / abc - b * c * ct / abc - a * st / sym.sqrt(abc)

        rot[2, 0] = a * c / abc - a * c * ct / abc - b * st / sym.sqrt(abc)
        rot[2, 1] = b * c / abc - b * c * ct / abc + a * st / sym.sqrt(abc)
        rot[2, 2] = c**2 / abc + a**2 * ct / abc + b**2 * ct / abc
    else:
        raise ValueError(f"unsupported dimension: {ambient_dim}")

    return rot


@dataclass(frozen=True)
class SolidBodyRotationGeometryTrackingFunctional(GeometryTrackingFunctional):
    r"""Implements time-dependent geometry tracking in a solid body rotation flow.

    In this case, the desired geometry is given by

    .. math::

        \dot{\mathbf{X}}(t) = \omega_d \times \mathbf{X}

    with the usual restriction of the cross product to the two-dimensional case.
    In the 2D case, this is just a rotation in the :math:`xy` plane, but in
    3D it can be any arbitrary rotation around a given axis :math:`\omega_d`.

    See also :func:`~pystopt.stokes.get_solid_body_rotation_farfield`.

    .. attribute:: omega_d

        Axis of the rotation used to define the position of the desired
        geometry at a given time :attr:`~GeometryTrackingFunctional.t`.
    """

    omega_d: np.ndarray

    def __post_init__(self):
        if not isinstance(self.omega_d, np.ndarray):
            if self.ambient_dim == 2:
                omega_d = make_obj_array([self.omega_d, self.omega_d])
            else:
                omega_d = make_obj_array([0, 0, self.omega_d])

            object.__setattr__(self, "omega_d", omega_d)

        if self.omega_d.shape != self.xd.shape:
            raise ValueError(
                "'omega_d' and 'xd' shapes must match: "
                f"omega_d {self.omega_d.shape} != xd {self.xd.shape}"
            )

    def nodes(self):
        rot = _get_solid_body_rotation_matrix(self.ambient_dim, self.t, self.omega_d)
        return rot @ self.xd


@dataclass(frozen=True)
class BezierGeometryTrackingFunctional(GeometryTrackingFunctional):
    r"""Implements time-dependent geometry tracking in a uniform flow.

    In this case, the desired geometry is given by

    .. math::

        \dot{\mathbf{X}}(t) = \sum P_i \phi_i'(t)

    where :math:`\phi_i` are the Bernstein polynomials of order :attr:`order`.
    The actual position of the interface is then just obtained by integration
    using :attr:`GeometryTrackingFunctional.xd` as an initial condition.

    See also :func:`~pystopt.stokes.get_bezier_uniform_farfield`.

    .. attribute:: npoints

        Number of control points in the Bézier curve.

    .. attribute:: order

        Order of the Bézier curve.

    .. attribute:: points_d

        Control points that define the Bézier curve for the velocity field.
    """

    points_d: tuple[np.ndarray, ...]

    def __post_init__(self):
        if not all(self.xd.shape == p.shape for p in self.points_d):
            raise ValueError("'points_d' and 'xd' shapes must match")

    @property
    def npoints(self):
        return len(self.points_d)

    @property
    def order(self):
        return len(self.points_d) - 1

    def nodes(self):
        from pystopt.stokes.boundary_conditions import get_parametrized_bezier_basis

        basis = get_parametrized_bezier_basis(self.t, self.order)

        return self.xd + sum(b * p for b, p in zip(basis, self.points_d, strict=False))


@dataclass(frozen=True)
class HelixGeometryTrackingFunctional(GeometryTrackingFunctional):
    r"""Implements time-dependent geometry tracking in a helical flow.

    In this case, the desired geometry is given by

    .. math::

        \begin{cases}
        x(t) = R \cos (\omega_d t), \\
        y(t) = R \sin (\omega_d t), \\
        z(t) = H_d t
        \end{cases}

    where :math:`R` is the :attr:`radius`, :math:`\omega_d` is the :attr:`omega_d`,
    :math:`H` is the :attr:`height_d` and :math:`\t` is a time-like variable
    in :math:`[0, 1]`.

    See also :func:`~pystopt.stokes.get_helical_farfield`.

    .. attribute:: radius

        Radius of the helix.

    .. attribute:: omega_d

        Wavenumber that, together with the :attr:`height_d`, determines the
        number of turns in the helix.

    .. attribute:: height_d

        The height of the helix.

    """

    omega_d: float | sym.var
    height_d: float | sym.var

    def nodes(self):
        t = self.t
        rot = np.array(
            [
                [sym.cos(self.omega_d * t), -sym.sin(self.omega_d * t), 0],
                [sym.sin(self.omega_d * t), sym.cos(self.omega_d * t), 0],
                [0, 0, 1],
            ],
            dtype=object,
        )
        offset = np.array([0, 0, self.height_d * t])

        return rot @ self.xd + offset


# }}}


# {{{ centroid tracking


@dataclass(frozen=True)
class CentroidTrackingFunctional(GeometryShapeFunctional):
    r"""Implements the evaluation and gradient for the cost functional

    .. math::

        \mathcal{J} = \frac{1}{2} \|\mathbf{x}_c(t) - \mathbf{x}_d(t)\|^2,

    where :math:`\mathbf{x}_c` is the centroid and :math:`\mathbf{x}_d` is
    a desired position.

    .. attribute:: ambient_dim

    .. attribute:: t

        A time-like parameter for the geometry, if any.

    .. attribute:: xd

        Reference desired geometry centroid. To get the actual coordinates
        for use :meth:`nodes`, as they can depend on other parameters in the
        class.

    .. automethod:: nodes
    """

    xd: np.ndarray
    t: sym.var | None

    @property
    def _groupwise(self):
        # NOTE: this is here to allow testing computing centroids per group
        return True

    @property
    def ambient_dim(self):
        return self.xd.size

    def nodes(self) -> np.ndarray:
        """
        :returns: an expression for the centroid based on :attr:`xd` and other
            parameters describing the desired geometry.
        """
        return self.xd

    def __call__(
        self,
        ambient_dim: int,
        *,
        dim: int | None = None,
        dofdesc: sym.DOFDescriptorLike | None = None,
    ) -> sym.ArithmeticExpression:
        xc = sym.surface_centroid(
            ambient_dim, dim=dim, dofdesc=dofdesc, groupwise=self._groupwise
        )
        f = sym.cse(xc - self.nodes(), "centroid_diff")

        return sym.NodeSum(f @ f) / 2


@grad.shape.register(CentroidTrackingFunctional)
def _grad_shape_centroid_tracking(
    self: CentroidTrackingFunctional,
    ambient_dim: int,
    *,
    dim: int | None = None,
    dofdesc: sym.DOFDescriptorLike | None = None,
) -> np.ndarray:
    x = sym.nodes(ambient_dim, dofdesc=dofdesc).as_vector()
    xc = sym.surface_centroid(
        ambient_dim, dim=dim, dofdesc=dofdesc, groupwise=self._groupwise
    )
    f = sym.cse(xc - self.nodes(), "centroid_diff")

    v = sym.surface_volume(
        ambient_dim, dim=dim, dofdesc=dofdesc, groupwise=self._groupwise
    )
    n = sym.normal(ambient_dim, dim=dim, dofdesc=dofdesc).as_vector()

    return (f @ (x - xc)) / v * n


# }}}


# {{{ time-dependent centroid tracking


@dataclass(frozen=True)
class UniformCentroidTrackingFunctional(CentroidTrackingFunctional):
    uinf_d: np.ndarray

    def __post_init__(self):
        if self.uinf_d.shape != self.xd.shape:
            raise ValueError(
                "'uinf_d' and 'xd' shapes must match: "
                f"uinf_d {self.uinf_d.shape} != xd {self.xd.shape}"
            )

    def nodes(self):
        return self.xd + self.t * self.uinf_d


@dataclass(frozen=True)
class SolidBodyRotationCentroidTrackingFunctional(CentroidTrackingFunctional):
    omega_d: np.ndarray

    def __post_init__(self):
        if not isinstance(self.omega_d, np.ndarray):
            if self.ambient_dim == 2:
                omega_d = make_obj_array([self.omega_d, self.omega_d])
            else:
                omega_d = make_obj_array([0, 0, self.omega_d])

            object.__setattr__(self, "omega_d", omega_d)

        if self.omega_d.shape != self.xd.shape:
            raise ValueError(
                "'omega_d' and 'xd' shapes must match: "
                f"omega_d {self.omega_d.shape} != xd {self.xd.shape}"
            )

    def nodes(self):
        rot = _get_solid_body_rotation_matrix(self.ambient_dim, self.t, self.omega_d)
        return rot @ self.xd


@dataclass(frozen=True)
class ExtensionalCentroidTrackingFunctional(CentroidTrackingFunctional):
    alpha_d: float | sym.var

    def nodes(self):
        if self.ambient_dim == 2:
            return make_obj_array([
                self.xd[0] * sym.exp(self.alpha_d * self.t),
                self.xd[1] * sym.exp(-self.alpha_d * self.t),
            ])
        else:
            return make_obj_array([
                self.xd[0] * sym.exp(self.alpha_d * self.t),
                self.xd[1] * sym.exp(self.alpha_d * self.t),
                self.xd[2] * sym.exp(-2 * self.alpha_d * self.t),
            ])


@dataclass(frozen=True)
class HelixCentroidTrackingFunctional(CentroidTrackingFunctional):
    height_d: float | sym.var
    omega_d: float | sym.var

    def nodes(self):
        t = self.t
        rot = np.array(
            [
                [sym.cos(self.omega_d * t), -sym.sin(self.omega_d * t), 0],
                [sym.sin(self.omega_d * t), sym.cos(self.omega_d * t), 0],
                [0, 0, 1],
            ],
            dtype=object,
        )
        offset = np.array([0, 0, self.height_d * t])

        return rot @ self.xd + offset


# }}}


# {{{


@dataclass(frozen=True)
class AreaTrackingFunctional(GeometryShapeFunctional):
    r"""Implements the evaluation and gradient for the cost functional

    .. math::

        \mathcal{J} = \frac{1}{2} \left(\int_\Sigma\,\mathrm{d}S - A_d\right)^2

    where :math:`A_d` is a desired area.
    """

    ad: sym.var

    def __call__(
        self,
        ambient_dim: int,
        *,
        dim: int | None = None,
        dofdesc: sym.DOFDescriptorLike | None = None,
    ) -> sym.ArithmeticExpression:
        a = sym.surface_area(ambient_dim, dim=dim, dofdesc=dofdesc)

        return (a - self.ad) ** 2 / 2


@grad.shape.register(AreaTrackingFunctional)
def _grad_shape_area_tracking(
    self: AreaTrackingFunctional,
    ambient_dim: int,
    *,
    dim: int | None = None,
    dofdesc: sym.DOFDescriptorLike | None = None,
) -> np.ndarray:
    n = sym.normal(ambient_dim, dim=dim, dofdesc=dofdesc).as_vector()
    kappa = sym.summed_curvature(ambient_dim, dim=dim, dofdesc=dofdesc)
    a = sym.surface_area(ambient_dim, dim=dim, dofdesc=dofdesc)

    return kappa * (a - self.ad) ** 2 / 2 * n


# }}}


# {{{ volume tracking


@dataclass(frozen=True)
class VolumeTrackingFunctional(GeometryShapeFunctional):
    r"""Implements the evaluation and gradient for the cost functional

    .. math::

        \mathcal{J} = \frac{1}{2} \left(\int_\Omega\,\mathrm{d}V - V_d\right)^2

    where :math:`V_d` is a desired volume. The volume in question here is
    interior to a closed surface :math:`\Sigma`, so that we can use the
    Divergence Theorem to easily compute it.
    """

    vd: sym.var

    def __call__(
        self,
        ambient_dim: int,
        *,
        dim: int | None = None,
        dofdesc: sym.DOFDescriptorLike | None = None,
    ) -> sym.ArithmeticExpression:
        v = sym.surface_volume(ambient_dim, dim=dim, dofdesc=dofdesc)

        return (v - self.vd) ** 2 / 2


@grad.shape.register(VolumeTrackingFunctional)
def _grad_shape_volume_tracking(
    self: VolumeTrackingFunctional,
    ambient_dim: int,
    *,
    dim: int | None = None,
    dofdesc: sym.DOFDescriptorLike | None = None,
) -> np.ndarray:
    n = sym.normal(ambient_dim, dim=dim, dofdesc=dofdesc).as_vector()
    v = sym.surface_volume(ambient_dim, dim=dim, dofdesc=dofdesc)

    return (v - self.vd) * n


# }}}


# {{{ scalar energy functional


@dataclass(frozen=True)
class EnergyFunctional(GeometryShapeFunctional):
    r"""Implements the evaluation and gradient for the cost functional

    .. math::

        \mathcal{J} = \frac{1}{2} \int_\Sigma f^2 \,\mathrm{d}S.

    .. attribute:: f

        Scalar function used in cost functional.

    .. attribute:: gradf

        If the scalar :attr:`f` is defined in the ambient space and not just
        on the surface :math:`\Sigma`, the full gradient is also required
        to compute the cost gradient.
    """

    f: sym.var
    gradf: np.ndarray | None = None

    def __call__(
        self,
        ambient_dim: int,
        *,
        dim: int | None = None,
        dofdesc: sym.DOFDescriptorLike | None = None,
    ) -> sym.ArithmeticExpression:
        return sym.sintegral(self.f**2 / 2, ambient_dim, dim, dofdesc=dofdesc)


@grad.shape.register(EnergyFunctional)
def _grad_shape_energy(
    self: EnergyFunctional,
    ambient_dim: int,
    *,
    dim: int | None = None,
    dofdesc: sym.DOFDescriptorLike | None = None,
) -> np.ndarray:
    n = sym.normal(ambient_dim, dim=dim, dofdesc=dofdesc).as_vector()
    kappa = sym.summed_curvature(ambient_dim, dim=dim, dofdesc=dofdesc)

    df = kappa * self.f**2 / 2.0
    if self.gradf is not None:
        df = df + self.gradf @ n

    return df * n


# }}}


# {{{ scalar tangential gradient functional


@dataclass(frozen=True)
class TangentialGradientFunctional(GeometryShapeFunctional):
    r"""Implements the evaluation and gradient for the cost functional

    .. math::

        \mathcal{J} = \frac{1}{2} \int_\Sigma
            \nabla_\Sigma f \cdot \nabla_\Sigma f \,\mathrm{d}S.

    .. attribute:: f

        Scalar function used in cost functional.

    .. attribute:: gradf

        Ambient gradient of :attr:`f`. If not provided, the gradient
        is computed numerically.
    """

    f: sym.var
    gradf: np.ndarray | None = None
    hessf: np.ndarray | None = None

    def __call__(
        self,
        ambient_dim: int,
        *,
        dim: int | None = None,
        dofdesc: sym.DOFDescriptorLike | None = None,
    ) -> sym.ArithmeticExpression:
        gradf = self.gradf
        if gradf is None:
            sgradf = sym.surface_gradient(ambient_dim, self.f, dim=dim, dofdesc=dofdesc)
        else:
            n = sym.normal(ambient_dim, dim=dim, dofdesc=dofdesc).as_vector()
            sgradf = gradf - (gradf @ n) * n

        return sym.sintegral(sgradf @ sgradf / 2, ambient_dim, dim, dofdesc=dofdesc)


@grad.shape.register(TangentialGradientFunctional)
def _grad_shape_tangential_gradient(
    self: TangentialGradientFunctional,
    ambient_dim: int,
    *,
    dim: int | None = None,
    dofdesc: sym.DOFDescriptorLike | None = None,
) -> np.ndarray:
    n = sym.normal(ambient_dim, dim=dim, dofdesc=dofdesc).as_vector()
    kappa = sym.summed_curvature(ambient_dim, dim=dim, dofdesc=dofdesc)

    gradf = self.gradf
    if gradf is None:
        sgradf = sym.surface_gradient(ambient_dim, self.f, dim=dim, dofdesc=dofdesc)
    else:
        sgradf = gradf - (gradf @ n) * n

    gradf_dot_n = sym.cse(gradf @ n)
    gradf_d = (
        sym.surface_divergence(
            ambient_dim, gradf_dot_n * gradf, dim=dim, dofdesc=dofdesc
        )
        - kappa * (gradf_dot_n) ** 2
    )

    return (-gradf_d + n @ (self.hessf @ sgradf) + kappa * sgradf @ sgradf / 2) * n


# }}}


# {{{ normal velocity tracking


@dataclass(frozen=True)
class NormalVelocityFunctional(ShapeFunctional):
    r"""Implements the evaluation and gradients for the cost functional

    .. math::

        \mathcal{J} = \frac{1}{2} \int_\Sigma
            |\mathbf{u} \cdot \mathbf{n} - u_d|^2 \,\mathrm{d}S.

    .. attribute:: u

        Velocity field used in the cost functional.

    .. attribute:: ud

        Desired normal component of the velocity field.

    .. attribute:: dofdesc

        A :class:`~pytential.symbolic.dof_desc.DOFDescriptor` for the
        geometry on which the desired velocity was computed, if any.

    .. attribute:: gradu_dot_t

        Tangential components of the velocity gradient. They are only required to
        compute the shape gradient. If not provided, this is computed
        numerically.

    .. attribute:: gradud

        Surface gradient of :attr:`ud`. If :attr:`ud` is constant, this is
        determined automatically, otherwise it is computed, e.g., using
        :func:`~pystopt.symbolic.primitives.surface_gradient`.
    """

    u: np.ndarray
    ud: sym.var

    dofdesc: sym.DOFDescriptor | None = None
    gradu_dot_t: np.ndarray | None = None
    gradud: np.ndarray | None = None
    divu: sym.var | None = None

    def __post_init__(self):
        from pymbolic.primitives import is_constant

        if self.gradud is None and is_constant(self.ud):
            ambient_dim = len(self.u)
            object.__setattr__(self, "gradud", make_obj_array([0, 0, 0][:ambient_dim]))

        if self.divu is None:
            object.__setattr__(self, "divu", 0)

        object.__setattr__(self, "dofdesc", sym.as_dofdesc(self.dofdesc))

    def __call__(
        self,
        ambient_dim: int,
        dim: int | None = None,
        dofdesc: sym.DOFDescriptorLike | None = None,
    ) -> sym.ArithmeticExpression:
        n = sym.normal(ambient_dim, dim=dim, dofdesc=dofdesc).as_vector()

        return sym.sintegral(
            (self.u @ n - self.ud) ** 2 / 2, ambient_dim, dim, dofdesc=dofdesc
        )


def get_desired_normal_velocity(
    actx: ArrayContext,
    places: GeometryCollection,
    op: TwoPhaseStokesRepresentation,
    *,
    context: dict[str, Any] | None = None,
    lambdas: dict[str, float] | None = None,
    dofdesc: sym.DOFDescriptorLike | None = None,
) -> dict[str, Any]:
    """Solves the Stokes system on a desired geometry in *places* described by
    *dofdesc*.

    :arg context: a :class:`dict` of additional variables required to solve
        the Stokes problem described by *op*.
    :arg lambdas: viscosity ratios for the desired geometry.

    :returns: a :class:`dict` which contains the required desired
        velocity field for :class:`NormalVelocityFunctional`. The keys in
        the dictionary match the attribute names.
    """

    if context is None:
        context = {}

    if lambdas is None:
        lambdas = {}

    dofdesc = sym.as_dofdesc(dofdesc)

    from pystopt.stokes import single_layer_solve

    result = single_layer_solve(
        actx,
        places,
        op,
        gmres_arguments={"rtol": 1.0e-8},
        lambdas=lambdas,
        sources=[dofdesc.geometry],
        context=context,
    )

    from pystopt.operators import make_sym_density

    density = make_sym_density(op, "q")

    from pystopt.stokes import sti

    sym_n = sym.normal(places.ambient_dim, dofdesc=dofdesc).as_vector()
    sym_ud = sti.velocity(op, density, side=+1, qbx_forced_limit=+1)

    from pystopt import bind

    ud_dot_n = bind(places, sym_ud @ sym_n, auto_where=dofdesc)(actx, q=result.density)

    discr_d = places.get_discretization(dofdesc.geometry)
    return {
        "xd": actx.thaw(discr_d.nodes()),
        "ud": ud_dot_n,
    }


def make_normal_velocity_functional_from_stokes(
    op: TwoPhaseStokesRepresentation,
    dofdesc: sym.DOFDescriptorLike | None = None,
) -> NormalVelocityFunctional:
    return NormalVelocityFunctional(
        u=sym.make_sym_vector("u", op.ambient_dim),
        gradu_dot_t=None,
        ud=sym.var("ud"),
        gradud=None,
        dofdesc=dofdesc,
    )


def _normal_velocity_functional_gradient(
    self: NormalVelocityFunctional,
    ambient_dim: int,
    *,
    dim: int | None = None,
    dofdesc: sym.DOFDescriptor | None = None,
) -> dict[str, Any]:
    n = sym.normal(ambient_dim, dim=dim, dofdesc=dofdesc).as_vector()
    kappa = sym.summed_curvature(ambient_dim, dim=dim, dofdesc=dofdesc)

    u_dot_n = sym.cse(self.u @ n)
    f = sym.cse(u_dot_n - self.ud)

    if self.gradud is None:
        gradf_dot_u = self.u @ sym.surface_gradient(ambient_dim, f, dofdesc=dofdesc)
    else:
        gradu_dot_n = sym.surface_gradient(ambient_dim, u_dot_n, dofdesc=dofdesc)
        gradf_dot_u = self.u @ (gradu_dot_n - self.gradud)

    return {
        r"\mathbf{u} \cdot \nabla_\Sigma j": gradf_dot_u + f * self.divu,
        r"\kappa j^2": -kappa * f * u_dot_n + kappa * f**2 / 2,
    }


@grad.shape.register(NormalVelocityFunctional)
def _grad_shape_normal_velocity(
    self: NormalVelocityFunctional,
    ambient_dim: int,
    *,
    dim: int | None = None,
    dofdesc: sym.DOFDescriptorLike | None = None,
) -> np.ndarray:
    n = sym.normal(ambient_dim, dim=dim, dofdesc=dofdesc).as_vector()
    return (
        sum(
            _normal_velocity_functional_gradient(
                self, ambient_dim, dim=dim, dofdesc=dofdesc
            ).values()
        )
        * n
    )


@grad.velocity.register(NormalVelocityFunctional)
def _grad_velocity_normal_velocity(
    self: NormalVelocityFunctional,
    ambient_dim: int,
    *,
    dim: int | None = None,
    dofdesc: sym.DOFDescriptorLike | None = None,
) -> np.ndarray:
    n = sym.normal(ambient_dim, dim=dim, dofdesc=dofdesc).as_vector()
    return sym.cse((sym.cse(self.u @ n) - self.ud) * n)


@grad.traction.register(NormalVelocityFunctional)
def _grad_traction_normal_velocity(
    self: NormalVelocityFunctional,
    ambient_dim: int,
    *,
    dim: int | None = None,
    dofdesc: sym.DOFDescriptorLike | None = None,
) -> np.ndarray:
    return make_obj_array([0, 0, 0][:ambient_dim])


@grad.capillary.register(NormalVelocityFunctional)
def _grad_capillary_normal_velocity(
    self: NormalVelocityFunctional,
    ambient_dim: int,
    *,
    dim: int | None = None,
    dofdesc: sym.DOFDescriptorLike | None = None,
) -> sym.ArithmeticExpression:
    return 0


@grad.farfield.register(NormalVelocityFunctional)
def _grad_farfield_normal_velocity(
    self: NormalVelocityFunctional,
    ambient_dim: int,
    *,
    dim: int | None = None,
    dofdesc: sym.DOFDescriptorLike | None = None,
) -> np.ndarray:
    return np.array(0.0)


# }}}


# {{{ test shape functionals

# {{{ normal


@dataclass(frozen=True)
class NormalTestFunctional(GeometryShapeFunctional):
    r"""The functional is given by

    .. math::

        \mathcal{J} = \int_\Sigma \mathbf{f} \cdot \mathbf{n} \,\mathrm{d}S.

    with an (Eulerian) shape gradient given by

    .. math::

        \nabla_{\mathbf{X}} \mathcal{J} = \nabla \cdot \mathbf{f}

    .. attribute:: f
    .. attribute:: divf
    """

    f: np.ndarray
    divf: sym.var

    def __call__(
        self,
        ambient_dim: int,
        *,
        dim: int | None = None,
        dofdesc: sym.DOFDescriptorLike | None = None,
    ) -> sym.ArithmeticExpression:
        n = sym.normal(ambient_dim, dim=dim, dofdesc=dofdesc)
        return sym.sintegral(self.f @ n, ambient_dim, dim, dofdesc=dofdesc)


@grad.shape.register(NormalTestFunctional)
def _grad_shape_normal_test(
    self: NormalTestFunctional,
    ambient_dim: int,
    *,
    dim: int | None = None,
    dofdesc: sym.DOFDescriptorLike | None = None,
) -> np.ndarray:
    n = sym.normal(ambient_dim, dim=dim, dofdesc=dofdesc).as_vector()

    from pystopt.symbolic.derivatives import adjoint_normal_shape_gradient

    df = adjoint_normal_shape_gradient(
        ambient_dim, f=self.f, divf=self.divf, dim=dim, dofdesc=dofdesc
    )

    return df * n


# }}}


# {{{ adjoint curvature test


@dataclass(frozen=True)
class CurvatureTestFunctional(GeometryShapeFunctional):
    r"""The functional is given by

    .. math::

        \mathcal{J} = \int_\Sigma \kappa f \,\mathrm{d}S.

    .. attribute:: f
    .. attribute:: divf
    .. attribute:: hessf
    """

    f: sym.var
    gradf: np.ndarray
    hessf: np.ndarray

    def __call__(
        self,
        ambient_dim: int,
        *,
        dim: int | None = None,
        dofdesc: sym.DOFDescriptorLike | None = None,
    ) -> sym.ArithmeticExpression:
        kappa = sym.summed_curvature(ambient_dim, dim=dim, dofdesc=dofdesc)

        return sym.sintegral(kappa * self.f, ambient_dim, dim, dofdesc=dofdesc)


@grad.shape.register(CurvatureTestFunctional)
def _grad_shape_curvature_test(
    self: CurvatureTestFunctional,
    ambient_dim: int,
    *,
    dim: int | None = None,
    dofdesc: sym.DOFDescriptorLike | None = None,
) -> np.ndarray:
    n = sym.normal(ambient_dim, dim=dim, dofdesc=dofdesc).as_vector()

    from pystopt.symbolic.derivatives import adjoint_curvature_shape_gradient

    df = adjoint_curvature_shape_gradient(
        ambient_dim,
        f=self.f,
        gradf_dot_n=self.gradf @ n,
        lapf=np.trace(self.hessf),
        hessf_dot_n=(n @ self.hessf) @ n,
        dim=dim,
        dofdesc=dofdesc,
    )

    return df * n


# }}}


# {{{ adjoint curvature vector test


@dataclass(frozen=True)
class CurvatureVectorTestFunctional(GeometryShapeFunctional):
    r"""The functional is given by

    .. math::

        \mathcal{J} = \int_\Sigma \kappa \mathbf{f} \cdot \mathbf{n} \,\mathrm{d}x.

    .. attribute:: f
    .. attribute:: divf
    .. attribute:: hessf
    """

    f: np.ndarray
    gradf: sym.var
    hessf: np.ndarray

    def __call__(
        self,
        ambient_dim: int,
        *,
        dim: int | None = None,
        dofdesc: sym.DOFDescriptorLike | None = None,
    ) -> sym.ArithmeticExpression:
        n = sym.normal(ambient_dim, dim=dim, dofdesc=dofdesc).as_vector()
        kappa = sym.summed_curvature(ambient_dim, dim=dim, dofdesc=dofdesc)

        return sym.sintegral(kappa * (self.f @ n), ambient_dim, dim, dofdesc=dofdesc)


@grad.shape.register(CurvatureVectorTestFunctional)
def _grad_shape_curvature_vector(
    self: CurvatureVectorTestFunctional,
    ambient_dim: int,
    *,
    dim: int | None = None,
    dofdesc: sym.DOFDescriptorLike | None = None,
) -> np.ndarray:
    if dim is None:
        dim = ambient_dim - 1

    n = sym.normal(ambient_dim, dim=dim, dofdesc=dofdesc).as_vector()
    t = sym.tangential_onb(ambient_dim, dim=dim, dofdesc=dofdesc).T

    from pystopt.symbolic.derivatives import adjoint_curvature_vector_shape_gradient

    df = adjoint_curvature_vector_shape_gradient(
        ambient_dim,
        f=self.f,
        divf=np.trace(self.gradf),
        gradf_dot_n=(n @ self.gradf) @ n,
        gradf_dot_t=[t[i] @ self.gradf for i in range(dim)],
        lapf_dot_n=np.trace(self.hessf @ n),
        hessf_dot_n=(n @ (n @ self.hessf)) @ n,
        dim=dim,
        dofdesc=dofdesc,
    )

    return df * n


# }}}

# }}}
