# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

"""
Finite Difference Approximations
--------------------------------

This module contains helpers to compute finite difference-type approximation
for first-order shape derivatives.

.. autoclass:: FinitePerturbation
.. autoclass:: FiniteGradientResult

.. autofunction:: make_perturbations
.. autofunction:: convolve_bump_function
.. autofunction:: compare_finite_difference_shape_gradient
"""

from collections.abc import Iterator
from dataclasses import dataclass

import numpy as np

from meshmode.dof_array import DOFArray
from pytools.obj_array import make_obj_array

from pystopt import bind, sym
from pystopt.simulation import ShapeOptimizationState
from pystopt.tools import get_default_logger

logger = get_default_logger(__name__)


@dataclass(frozen=True)
class FinitePerturbation:
    """
    .. attribute:: bump

        Bump function used for finite difference perturbations.

    .. attribute:: tilde

        Full geometry perturbation, constructed for first-order perturbations
        so it only has a normal component.

    .. attribute:: igrp
    .. attribute:: iel
    .. attribute:: idof
    """

    bump: np.ndarray
    tilde: np.ndarray

    igrp: int
    iel: int
    idof: int

    @property
    def index(self):
        return self.igrp, self.iel, self.idof


@dataclass(frozen=True)
class FiniteGradientResult:
    """Container for the finite difference gradient comparison.

    .. attribute:: grad

        Known gradient.

    .. attribute:: grad_fd

        Finite difference approximated gradient.

    .. attribute:: grad_ad

        Known gradient convoluted with the bump functions used to compute
        :attr:`grad_fd`.

    .. attribute:: eps
    .. attribute:: hmax
    .. attribute:: error
    """

    grad: np.ndarray
    grad_ad: DOFArray
    grad_fd: DOFArray

    eps: float
    hmax: float
    error: float


def generate_random_dofs(
    discr, n: int, *, rng: np.random.Generator | None = None
) -> list[int]:
    if rng is None:
        rng = np.random.default_rng()

    ndofs = set()
    while len(ndofs) < n:
        igrp = rng.integers(0, len(discr.groups))
        iel = rng.integers(0, discr.groups[igrp].nelements)
        idof = rng.integers(0, discr.groups[igrp].nunit_dofs)

        ndofs = ndofs | {(igrp, iel, idof)}

    return list(ndofs)


def nddofs(discr) -> Iterator[tuple[int, int, int]]:
    r"""Iterator over the degrees of freedom in *discr*.

    :returns: a :class:`tuple` of ``(igrp, iel, idof)`` for each degree of
        freedom in *discr*.
    """
    for igrp, grp in enumerate(discr.groups):
        for iel, idof in np.ndindex(grp.nelements, grp.nunit_dofs):
            yield igrp, iel, idof


def _make_sym_perturbation(ambient_dim, *, dim=None, dofdesc=None):
    h = sym.gaussian_bump_function(ambient_dim, sigma_name="sigma", normalized=False)

    return h


def make_perturbations(
    state: ShapeOptimizationState,
    sigma: float,
    indices: list[int] | int | None = None,
) -> Iterator[FinitePerturbation]:
    r"""
    :arg sigma: width of the bump function used in generating perturbations,
        see :func:`~pystopt.symbolic.primitives.gaussian_bump_function`.
    :arg indices: a :class:`list` of :class:`tuple`\ s of ``(igrp, iel, idof)``
        of degrees of freedom in the discretization given by *dofdesc* that
        should be perturbed. If *None*, all degrees of freedom are perturbed.
    """
    actx = state.array_context
    places = state.get_geometry_collection()
    dofdesc = state.wrangler.dofdesc

    discr = places.get_discretization(dofdesc.geometry, dofdesc.discr_stage)
    dx = make_obj_array([xi.copy() for xi in state.x])

    normal = bind(places, sym.normal(places.ambient_dim).as_vector())(actx)
    bump = bind(
        places,
        _make_sym_perturbation(places.ambient_dim, dofdesc=dofdesc),
        auto_where=dofdesc,
    )

    indices = nddofs(discr) if indices is None else indices
    for igrp, iel, idof in indices:
        # NOTE: computes (x - x_i), where i = (igrp, iel, idof)
        for idim in range(dx.size):
            dx[idim] -= actx.to_numpy(state.x[idim][igrp][iel, idof])

        h = bump(actx, y=dx, sigma=sigma)

        # NOTE: we can revert the chance once the bump function is computed
        for idim in range(dx.size):
            dx[idim] += actx.to_numpy(state.x[idim][igrp][iel, idof])

        from dataclasses import replace

        yield FinitePerturbation(
            bump=h,
            tilde=replace(state, x=h * normal),
            igrp=igrp,
            iel=iel,
            idof=idof,
        )


def convolve_bump_function(
    state: ShapeOptimizationState,
    f: DOFArray,
    *,
    sigma: float,
    dofdesc: sym.DOFDescriptorLike | None = None,
):
    """Convolve *f* with the bump functions used in
    :func:`compare_finite_difference_shape_gradient`.

    :arg sigma: width of the bump functions used in perturbing the geometry,
        see :func:`make_perturbations`.
    """

    actx = state.array_context
    dot = bind(
        state.get_geometry_collection(),
        sym.real_scalar_dot(state.ambient_dim, dofdesc=dofdesc),
        auto_where=dofdesc,
    )

    fhat = actx.np.zeros_like(f)
    perturbations = make_perturbations(state, sigma=sigma)
    for p in perturbations:
        fhat[p.igrp][p.iel, p.idof] = dot(actx, x=f, y=p.bump)

    return fhat


def compare_finite_difference_shape_gradient(
    state: ShapeOptimizationState,
    *,
    eps: float,
    sigma: float,
    grad: DOFArray | None,
    indices: list[int] | int | None = None,
) -> FiniteGradientResult:
    r"""Compute the finite difference-based shape gradient of *cost_fn* and
    compare to the one given by *grad*.

    The finite difference gradient is obtained by

    .. math::

        \nabla_i \mathcal{J} = \frac{1}{\epsilon}
            (\mathcal{J}(\mathbf{x} - \epsilon \mathbf{h}_i)
            - \mathcal{J}(\mathbf{x})),

    where :math:`\mathbf{h}_i` is a normal perturbation centered around the
    :math:`i`-th degree of freedom in *dofdesc*. As the perturbation
    is not a Dirac delta, the value that is obtained from the above
    computation is actually the inner product

    .. math::

        (\nabla \mathcal{J}, \mathbf{h}_i \cdot \mathbf{n})_\Sigma.

    To be able to compare this result to *grad*, we also convolve it with the
    bump functions :math:`\mathbf{h}_i`, using :func:`convolve_bump_function`.

    :arg cost_fn: a callable returning the value of the cost functional.
    :arg eps: finite difference step size.
    :arg sigma: width of the bump functions used in perturbing the geometry,
        see :func:`make_perturbations`.
    :arg grad: an optional :class:`DOFArray` containing the known gradient.
    :arg indices: see :func:`make_perturbations`.
    """

    # {{{ setup

    if indices is not None and not isinstance(indices, list):
        indices = [indices]

    actx = state.array_context
    places = state.get_geometry_collection()
    dofdesc = state.wrangler.dofdesc

    dot = bind(
        places,
        sym.real_scalar_dot(places.ambient_dim, dofdesc=dofdesc),
        auto_where=dofdesc,
    )

    discr = places.get_discretization(dofdesc.geometry, dofdesc.discr_stage)

    grad_ad = None
    if indices is None:
        grad_fd = discr.zeros(state.array_context)
        if grad is not None:
            grad_ad = discr.zeros(state.array_context)
    else:
        grad_fd = actx.np.zeros(len(indices), dtype=np.float64)
        if grad is not None:
            grad_ad = actx.np.zeros(len(indices), dtype=np.float64)

    # }}}

    # {{{ compute finite difference

    perturbations = make_perturbations(state, sigma=sigma, indices=indices)
    cost_0 = state.evaluate_shape_cost()

    for i, p in enumerate(perturbations):
        xtilde = state + eps * p.tilde
        cost_eps = xtilde.evaluate_shape_cost()

        i_grad_fd = (cost_eps - cost_0) / eps
        i_grad_ad = np.nan
        if grad is not None:
            i_grad_ad = dot(actx, x=grad, y=p.bump)

        if indices is None:
            grad_fd[p.igrp][p.iel, p.idof] = i_grad_fd
            if grad is not None:
                grad_ad[p.igrp][p.iel, p.idof] = i_grad_ad
        else:
            grad_fd[i] = i_grad_fd
            if grad is not None:
                grad_ad[i] = i_grad_ad

        if grad is not None:
            i_grad_ad = actx.to_numpy(i_grad_ad)

        logger.info(
            "grad[%4d, %2d] fd %+.5e ad %+.5e", p.iel, p.idof, i_grad_fd, i_grad_ad
        )

    # }}}

    # {{{ errors

    error = np.nan
    if grad is not None:
        # FIXME: what gradient to divide with in the relative norm?
        if indices is None:
            from pystopt.dof_array import dof_array_rnorm

            error = dof_array_rnorm(grad_ad, grad_fd)
        else:
            error = actx.np.linalg.norm(grad_ad - grad_fd) / actx.np.linalg.norm(
                grad_fd
            )

        error = actx.to_numpy(error)

    hmax = bind(
        places,
        sym.h_max_from_volume(places.ambient_dim, dofdesc=dofdesc),
        auto_where=dofdesc,
    )(actx)

    r = FiniteGradientResult(
        grad=grad,
        grad_fd=grad_fd,
        grad_ad=grad_ad,
        eps=eps,
        hmax=actx.to_numpy(hmax),
        error=error,
    )

    logger.info("eps %.5e hmax %.5e rel error %.5e", eps, r.hmax, r.error)

    # }}}

    return r
