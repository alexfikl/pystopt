# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

"""
.. currentmodule:: pystopt.evolution

.. autoclass:: TimeWrangler
.. autoclass:: TimestepMethodWrapper

.. autofunction:: make_adjoint_time_stepper
"""

from collections.abc import Callable, Iterator
from dataclasses import dataclass
from typing import Any

from pytools import T

from pystopt.checkpoint import CheckpointManager
from pystopt.evolution.adjoint import TimestepEvent
from pystopt.measure import TicTocTimer
from pystopt.paths import PathLike
from pystopt.tools import get_default_logger

logger = get_default_logger(__name__)


# {{{ wrangler


def _make_checkpoint(dirname: PathLike | None) -> CheckpointManager | None:
    """Constructs a checkpoint in the given *dirname* directory.

    By default, this constructs a HDF5-based checkpointer using
    :func:`pystopt.checkpoint.make_hdf_checkpoint_manager`.
    """
    if dirname is None:
        return None

    import pathlib

    dirname = pathlib.Path(dirname)
    if not dirname.exists():
        dirname.mkdir()

    from pystopt.paths import get_filename

    checkpoint_file_name = get_filename("ts", ext=".h5", cwd=dirname)

    from pystopt.checkpoint import make_hdf_checkpoint_manager

    return make_hdf_checkpoint_manager(checkpoint_file_name)


@dataclass(frozen=True)
class TimeWrangler:
    """A class that holds all the parameters required to construct a
    :class:`TimestepMethodWrapper`.

    .. attribute:: time_step_name
    .. attribute:: maxit
    .. attribute:: tmax
    .. attribute:: dt

        Initial guess for the time step.

    .. attribute:: theta

        Courant number that can be used for time step estimation.

    .. attribute:: estimate_time_step

        A callable used to estimate the time step. Setting it to *False* can be
        used to disable adaptive time stepping.

    .. automethod:: make_stepper
    """

    time_step_name: str

    maxit: int | None = None
    tmax: float | None = None
    dt: float | None = None
    theta: float | None = None

    estimate_time_step: Callable[[float, Any], float] | bool | None = None
    state_filter: Callable[[Any], Any] | None = None

    def make_stepper(
        self,
        state0: T,
        source: Callable[[float, T], T],
        estimate: Callable[[float, T], float] | None = None,
        **kwargs: Any,
    ) -> "TimestepMethodWrapper":
        """Construct a time stepper starting from the initial condition *state0*
        and the right-hand side *source*.

        :arg estimate: a callable used for time step estimation that can be used
            to overwrite the default one in :attr:`estimate_time_step`.
        """
        if estimate is None:
            estimate = self.estimate_time_step

        if self.estimate_time_step is False:
            estimate = None

        component_id = kwargs.pop("component_id", "state")
        checkpoint_directory_name = kwargs.pop("checkpoint_directory_name", None)

        checkpoint = _make_checkpoint(checkpoint_directory_name)
        stepper = make_adjoint_time_stepper(
            self.time_step_name,
            component_id,
            state0,
            source,
            dt_start=self.dt,
            t_start=0.0,
            checkpoint=checkpoint,
            state_filter=self.state_filter,
            **kwargs,
        )

        if estimate is not None:
            from dataclasses import replace

            stepper = replace(stepper, estimate_time_step=estimate)

        return stepper


# }}}


# {{{ integrator


def _log_time_step_end(
    event: TimestepEvent,
    tt: TicTocTimer,
    *,
    tmax: float | None,
    maxit: int | None,
) -> None:
    from pystopt.measure import estimate_wall_time_from_timestep

    t_elapsed, t_remaining = estimate_wall_time_from_timestep(
        tt, n=event.n, t=event.t, dt=event.dt, tmax=tmax, maxit=maxit
    )

    from pystopt.measure import format_seconds
    from pystopt.tools import c

    if maxit is None:
        it_string = f"{event.n:04d}"
    else:
        it_string = f"{event.n:04d}/{maxit - 1:04d}"

    if tmax is None:
        t_string = f"t = {event.t:.5e}"
    else:
        t_string = f"t = {event.t:.5e}/{tmax:.5e}"

    if tmax is None and maxit is None:
        timer_string = f"elapsed {format_seconds(t_elapsed)}"
    else:
        timer_string = "elapsed {} remaining {}".format(
            format_seconds(t_elapsed),
            format_seconds(t_remaining),
        )

    logger.info(
        c.wrap("[%s] %s dt = %.5e ", c.LightGreen) + c.wrap("(%s %s)", c.Cyan),
        it_string,
        t_string,
        event.dt,
        tt,
        timer_string,
    )


@dataclass(frozen=True)
class TimestepMethodWrapper:
    """
    .. attribute:: leap

        Underlying time stepper that is wrapped.

    .. attribute:: estimate_time_step

        A callbable that takes the state variable and returns an estimated
        time step. Note that, if the callable is provided,
        :func:`~pystopt.evolution.update_time_step_from_estimate` is used to
        additionally ensure the time step is not varying too quickly and remains
        within bounds at the final time.

    .. attribute:: dt_max

        If :attr:`estimate_time_step` is provided, this value can be used to
        additionally limit the time step size. In general, it is recommended
        that :attr:`estimate_time_step` does the preprocessing itself.

    .. automethod:: run
    """

    leap: Any
    estimate_time_step: Callable[[float, Any], float] | None = None
    dt_max: float | None = None

    def make_adjoint(
        self,
        adjoint_id: str,
        state0: T,
        fun: Callable[[float, T, T], T],
    ) -> "TimestepMethodWrapper":
        """
        :arg state0: value of the adjoint at the final time. The final time is
            determined from the checkpoints of the forward evolution.
        """
        from pystopt.evolution.adjoint import AdjointTimeStepMethod, TimeStepMethod

        if isinstance(self.leap, TimeStepMethod):
            leap = self.leap.make_adjoint(
                adjoint_id,
                state0,
                fun,
            )
        elif isinstance(self.leap, AdjointTimeStepMethod):
            raise TypeError(
                f"'{type(self.leap).__name__}' is already an adjoint method"
            )
        else:
            raise TypeError(f"'{type(self.leap).__name__}' does not support adjointing")

        from dataclasses import replace

        return replace(self, leap=leap, estimate_time_step=None)

    @property
    def initial_state(self):
        return self.leap.state0

    def run(
        self,
        *,
        tmax: float | None = None,
        maxit: int | None = None,
        verbose: bool = True,
    ) -> Iterator[TimestepEvent]:
        """Run time stepper for a given period of time.

        :arg tmax: final time for the time stepping.
        :arg maxit: maximum number of iterations.
        """
        tt = TicTocTimer()
        tt.tic()

        from pystopt.evolution.adjoint import StepFailedEvent
        from pystopt.tools import c

        # NOTE: account for the fact that we're counting from 0
        max_steps = maxit if maxit is None else maxit - 1

        for event in self.leap.run(t_end=tmax, max_steps=max_steps):
            if isinstance(event, StepFailedEvent):
                if maxit is None:
                    logger.info(
                        c.wrap("%s[%05d] t = %.5e (Stopped)%s", c.LightRed),
                        event.n,
                        event.t,
                    )
                else:
                    logger.info(
                        c.wrap("%s[%05d/%05d] t = %.5e (Stopped)%s", c.LightRed),
                        event.n,
                        maxit - 1,
                        event.t,
                    )
                break

            yield event

            from pystopt.evolution.estimates import update_time_step_from_estimate

            if self.estimate_time_step is not None:
                dt_new = self.estimate_time_step(event.t, event.state_component)
                if dt_new is not None:
                    dt_new = update_time_step_from_estimate(
                        self.leap.dt, dt_new, self.dt_max
                    )

                    if tmax is not None:
                        dt_new = min(dt_new, abs(tmax - event.t))

                    self.leap.dt = dt_new

            tt.toc()
            if verbose:
                _log_time_step_end(event, tt, tmax=tmax, maxit=maxit)

            tt.tic()

        logger.info("time: %s", tt.stats())


# }}}


# {{{


def make_adjoint_time_stepper(
    method: str,
    component_id: str,
    component: T,
    rhs: Callable[[float, T], T],
    *,
    dt_start: float,
    t_start: float = 0.0,
    state_filter: Callable[[T], T] | None = None,
    checkpoint: CheckpointManager | None = None,
    **kwargs: Any,
) -> TimestepMethodWrapper:
    """Generate a time integrator with adjoint capabilities.

    Currently supported methods are:

    * ``"aeuler"``: classic first-order forward Euler method.
    * ``"assprk22"``: classic two-stage second-order SSP RK method.
    * ``"assprk33"``: classic three-stage third-order SSP RK method.
    * ``"assprk34"``: classic four-stage third-order SSP RK method.

    :arg component_id: a string name for the variable.
    :arg component: an initial condition for the time stepping.
    :arg rhs: a :class:`~collections.abc.Callable` taking ``(t, component)``.
    :arg dt: fixed time step.
    """

    import pystopt.evolution.adjoint as ad

    cls = {
        "aeuler": ad.ForwardEulerMethod,
        "assprk22": ad.SSPRK22Method,
        "assprk33": ad.SSPRK33Method,
        "assprk34": ad.SSPRK34Method,
    }.get(method)
    if cls is None:
        raise ValueError(f"unknown method: '{method}'")

    leap = cls(
        state_id=component_id,
        state0=component,
        function=rhs,
        filter=state_filter,
        dt=dt_start,
        t_start=t_start,
        checkpoint=checkpoint,
    )

    return TimestepMethodWrapper(leap=leap, **kwargs)


# }}}
