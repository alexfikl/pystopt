# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

"""
.. currentmodule:: pystopt.evolution

.. autofunction:: update_time_step_from_estimate
.. autofunction:: fixed_time_step_from
.. autofunction:: capillary_time_step_estimation
.. autofunction:: capillary_time_step_brackbill
.. autofunction:: capillary_time_step_loewenberg
.. autofunction:: capillary_time_step_zinchenko2000
.. autofunction:: capillary_time_step_zinchenko2006
"""

from collections.abc import Sequence

import numpy as np

from arraycontext import Array, ArrayContext
from pytential import GeometryCollection

from pystopt import bind, sym
from pystopt.tools import get_default_logger

logger = get_default_logger(__name__)


# {{{ update_time_step_from_estimate


def update_time_step_from_estimate(
    dt_old: float,
    dt_new: float,
    dt_max: float | None = None,
) -> float:
    """Smooth out time step estimates based on immediate history.

    :arg dt_old: previous time step.
    :arg dt_new: estimate for the next time step.
    :arg dt_max: maximum allowed time step.

    :returns: an improved estimate for the next time step that does not
        grow too fast compared to *dt_old*, e.g. it shouldn't be more than
        twice as large as *dt_old*.
    """

    dt = dt_new

    # ensure dt isn't growing too fast
    dt = min(dt, 1.25 * dt_old)

    # smooth dt out a bit anyway
    dt = min(dt, 0.5 * (dt + dt_old))

    # ensure dt isn't bigger than the maximum requested
    if dt_max is not None:
        dt = min(dt, dt_max)

    return dt


# }}}


# {{{ fixed_time_step_from


def fixed_time_step_from(
    *,
    maxit: int | None = None,
    tmax: float | None = None,
    timestep: float | None = None,
) -> tuple[int, float, float]:
    """Harmonize the values of *maxit*, *tmax* and *timestep*.

    This function can be used to ensure that the values are consistent in the
    sense that::

        tmax = maxit * timestep

    In the case all the values are given, i.e. not *None*, then the time step
    is kept as is and the other values are changed to match the formula above.
    """

    if (
        (maxit is None and tmax is None)  # noqa: PLR0916
        or (maxit is None and timestep is None)
        or (tmax is None and timestep is None)
    ):
        raise ValueError("must provide 2 out of 3 from 'maxit', 'tmax' or 'timestep'")

    if maxit is None:
        assert tmax is not None
        assert timestep is not None
        if tmax == np.inf:
            # NOTE: this is infinite enough for everybody!
            maxit = np.iinfo(np.int32).max
        else:
            maxit = int(tmax / timestep)
            timestep = tmax / (maxit - 1)
    elif tmax is None:
        assert maxit is not None
        assert timestep is not None
        tmax = maxit * timestep
        timestep = tmax / (maxit - 1)
    elif timestep is None:
        assert tmax is not None
        assert maxit is not None
        timestep = tmax / (maxit - 1)
    else:
        # FIXME: if all the values are given, just leave them alone?
        timestep = tmax / (maxit - 1)

    # NOTE: this adds some small noise to the time step so that it goes over
    # tmax when iterating and no additional checking is needed
    timestep = timestep + 5 * np.finfo(np.float64).eps

    return maxit, tmax, timestep


# }}}


# {{{ adaptive time step estimation


def _max_velocity(
    actx: ArrayContext,
    places: GeometryCollection,
    *,
    velocity: np.ndarray | None = None,
    dofdesc: sym.DOFDescriptor | None = None,
) -> Array:
    if velocity is None:
        return 1.0

    # NOTE: takes inf norm of the velocity
    v = sym.make_sym_vector("v", places.ambient_dim)
    return bind(places, sym.NodeMax(sym.sqrt(v @ v)))(actx, v=velocity)


def _min_vertex_distance(actx: ArrayContext, to_vertex_conn) -> Array:
    from arraycontext import make_loopy_program
    from pytools import memoize_in

    @memoize_in(actx, (_min_vertex_distance, "min_vertex_distance"))
    def prg():
        import loopy as lp

        t_unit = make_loopy_program(
            """
                {[iel, iface, idim]:
                    0 <= iel < nelements and
                    0 <= iface < nfaces and
                    0 <= idim < ndim}
                """,
            """
                for iel, iface
                    <> i = vi[iel, fvi[iface, 0]]
                    <> j = vi[iel, fvi[iface, 1]]
                    <> dx = sqrt(sum(idim, (vertices[idim, i] - vertices[idim, j])**2))

                    vertex_distance[i] = \
                            vertex_distance[i] if vertex_distance[i] < dx else dx \
                            {id=insn0, nosync=insn1, atomic}
                    vertex_distance[j] = \
                            vertex_distance[j] if vertex_distance[j] < dx else dx \
                            {id=insn1, nosync=insn0, atomic}
                end
                """,
            kernel_data=[
                lp.GlobalArg("vi", shape=("nelements", "nelvertices")),
                lp.GlobalArg("vertex_distance", for_atomic=True),
                lp.GlobalArg("vertices", shape=("ndim", "nvertices")),
                lp.ValueArg("nvertices", np.int32),
                lp.ValueArg("nelvertices", np.int32),
                "...",
            ],
            name="min_vertex_distance",
        )

        from meshmode.transform_metadata import ConcurrentElementInameTag

        t_unit = lp.tag_array_axes(t_unit, "vi", "stride:auto,stride:auto")
        return lp.tag_inames(
            t_unit,
            {"iel": ConcurrentElementInameTag()},
        )

    discr = to_vertex_conn.from_discr
    vertex_distance = actx.np.zeros(
        discr.mesh.nvertices, dtype=discr.mesh.vertices.dtype
    )
    vertex_distance.fill(np.inf)
    vertices = actx.from_numpy(discr.mesh.vertices.copy())

    from pystopt.stabilization.zinchenko_util import get_face_vertex_indices

    for grp in to_vertex_conn.groups:
        for batch in grp.batches:
            mgrp = discr.groups[batch.from_group_index].mesh_el_group
            fvi = np.array(get_face_vertex_indices(mgrp, triangulate=False))

            actx.call_loopy(
                prg(),
                vi=batch.node_indices,
                fvi=actx.from_numpy(fvi),
                vertices=vertices,
                vertex_distance=vertex_distance,
            )

    return vertex_distance


def capillary_time_step_loewenberg(
    actx: ArrayContext,
    places: GeometryCollection,
    *,
    capillary_number: float,
    viscosity_ratio: float,
    velocity: np.ndarray | None = None,
    dofdesc: sym.DOFDescriptorLike | None = None,
) -> Array:
    """Time step estimation based on [loewenberg-2001]_ Section 4.1.1.

    The length scale in [loewenberg-2001]_ was based on the smallest edge
    length. In our case, we use a length scale based on the smallest stretch
    of an element in :func:`~pystopt.symbolic.primitives.h_min`.

    .. [loewenberg-2001] V. Cristini, J. Bławzdziewicz, M. Loewenberg,
        `An Adaptive Mesh Algorithm for Evolving Surfaces: Simulations of Drop
        Breakup and Coalescence`,
        Journal of Computational Physics, Vol. 168, pp. 445--463, 2001,
        `DOI <https://doi.org/10.1006/jcph.2001.6713>`__.
    """
    dofdesc = places.auto_source if dofdesc is None else sym.as_dofdesc(dofdesc)

    hmin = bind(places, sym.h_min(places.ambient_dim), auto_where=dofdesc)(actx)
    vmax = _max_velocity(actx, places, velocity=velocity, dofdesc=dofdesc)

    return capillary_number * hmin / vmax


def capillary_time_step_brackbill(
    actx: ArrayContext,
    places: GeometryCollection,
    *,
    capillary_number: float,
    viscosity_ratio: float | None = None,
    velocity: np.ndarray | None = None,
    dofdesc: sym.DOFDescriptorLike | None = None,
) -> Array:
    """Time step estimation based on [brackbill-1992]_ Equation 61.

    The length scale in [brackbill-1992]_ was based on a uniform rectangular
    mesh. In our case, we use :func:`~pystopt.symbolic.primitives.h_min`.

    .. [brackbill-1992] J. U. Brackbill, D. B. Kothe, C. Zemach,
        `A Continuum Method for Modeling Surface Tension,
        Journal of Computational Physics`, Vol. 100, pp. 335--354, 1992,
        `DOI <https://doi.org/10.1016/0021-9991(92)90240-y>`__.
    """
    dofdesc = places.auto_source if dofdesc is None else sym.as_dofdesc(dofdesc)

    hmin = bind(places, sym.h_min(places.ambient_dim), auto_where=dofdesc)(actx)
    vmax = _max_velocity(actx, places, velocity=velocity, dofdesc=dofdesc)

    return actx.np.sqrt(capillary_number / vmax * hmin**3 / np.pi)


def capillary_time_step_zinchenko2006(
    actx: ArrayContext,
    places: GeometryCollection,
    *,
    capillary_number: float,
    viscosity_ratio: float | None = None,
    velocity: np.ndarray | None = None,
    dofdesc: sym.DOFDescriptorLike | None = None,
) -> Array:
    """Time step estimation based on [zinchenko-2006]_ Equation 3.26.

    A small modification is made to the results from [zinchenko-2006]_, since we
    take the maximum of the mean curvature instead the maximum of the individual
    principal curvatures. This should result in a more stringent time step.

    .. [zinchenko-2006] A. Z. Zinchenko, R. H. Davis,
        `A Boundary-Integral Study of a Drop Squeezing Through Interparticle
        Constrictions`,
        Journal of Fluid Mechanics, Vol. 564, pp. 227, 2006,
        `DOI <https://doi.org/10.1017/s0022112006001479>`__.
    """
    dofdesc = places.auto_source if dofdesc is None else sym.as_dofdesc(dofdesc)

    from pystopt.mesh import make_to_unique_mesh_vertex_connection

    discr = places.get_discretization(dofdesc.geometry, dofdesc.discr_stage)
    to_vertex = make_to_unique_mesh_vertex_connection(discr)
    assert len(to_vertex.groups) == 1

    mean_curvature = bind(
        places, sym.abs(sym.mean_curvature(places.ambient_dim)), auto_where=dofdesc
    )(actx)

    kappa = to_vertex(mean_curvature)[0].squeeze()
    hmin = _min_vertex_distance(actx, to_vertex)
    vmax = _max_velocity(actx, places, velocity=velocity, dofdesc=dofdesc)

    return capillary_number / vmax * actx.np.min(hmin / kappa)


def capillary_time_step_zinchenko2000(
    actx: ArrayContext,
    places: GeometryCollection,
    *,
    capillary_number: float,
    viscosity_ratio: float | None = None,
    velocity: np.ndarray | None = None,
    dofdesc: sym.DOFDescriptorLike | None = None,
) -> Array:
    """Time step estimation based on [zinchenko-2000]_ Appendix C.

    This is very similar to :func:`capillary_time_step_zinchenko2006`, but also
    involves the viscosity ratio.

    .. [zinchenko-2000] A. Z. Zinchenko, R. H. Davis,
        `An Efficient Algorithm for Hydrodynamical Interaction of Many
        Deformable Drops`,
        Journal of Computational Physics, Vol. 157, pp. 539--587, 2000,
        `DOI <https://doi.org/10.1006/jcph.1999.6384>`__.
    """
    if viscosity_ratio is None:
        viscosity_ratio = 1.0

    dt = capillary_time_step_zinchenko2006(
        actx,
        places,
        capillary_number=capillary_number,
        viscosity_ratio=viscosity_ratio,
        velocity=velocity,
        dofdesc=dofdesc,
    )

    return (1.0 + viscosity_ratio) * dt


def capillary_time_step_siqueira(
    actx: ArrayContext,
    places: GeometryCollection,
    *,
    capillary_number: float,
    viscosity_ratio: float | None = None,
    velocity: np.ndarray | None = None,
    dofdesc: sym.DOFDescriptorLike | None = None,
) -> Array:
    """Time step estimation based on [siqueira-2017]_ Section 3.5.

    .. [siqueira-2017] I. R. Siqueira, R. B. Rebouças, T. F. Oliveira, F. R. Cunha,
        `A New Mesh Relaxation Approach and Automatic Time-Step Control Method
        for Boundary Integral Simulations of a Viscous Drop`,
        International Journal for Numerical Methods in Fluids, Vol. 84,
        pp. 221--238, 2017,
        `DOI <https://doi.org/10.1002/fld.4346>`__.
    """
    dofdesc = places.auto_source if dofdesc is None else sym.as_dofdesc(dofdesc)

    if places.ambient_dim == 2:
        basis = [[(0, 0)], [(1, 1)], [(0, 1), (1, 0)]]
    elif places.ambient_dim == 3:
        basis = [
            [(0, 0)],
            [(1, 1)],
            [(2, 2)],
            [(0, 1), (1, 0)],
            [(0, 2), (2, 0)],
            [(1, 2), (2, 1)],
        ]
    else:
        raise ValueError(f"unsupported: dimension: {places.ambient_dim}")

    # {{{ construct system

    xc = bind(
        places,
        sym.nodes(places.ambient_dim) - sym.surface_centroid(places.ambient_dim),
        auto_where=dofdesc,
    )(actx)

    # FIXME: not clear what they mean in the paper by the droplet radius?
    g = actx.np.sqrt(xc @ xc) - 1

    nbasis = len(basis)
    A = np.empty((nbasis, nbasis))
    b = np.empty(nbasis)
    y = np.empty(nbasis, dtype=object)

    for i in range(nbasis):
        # FIXME: does paper assume points are always centered at 0?
        y[i] = sum(xc[j] * xc[k] for j, k in basis[i])

        sym_b = sym.sintegral(g * y[i], places.ambient_dim, places.ambient_dim - 1)
        b[i] = actx.to_numpy(bind(places, sym_b, auto_where=dofdesc)(actx))

    from itertools import product

    for i, j in product(range(nbasis), repeat=2):
        sym_a = sym.sintegral(y[i] * y[j], places.ambient_dim, places.ambient_dim - 1)
        A[i, j] = actx.to_numpy(bind(places, sym_a, auto_where=dofdesc)(actx))

    # }}}

    # FIXME: not clear that this can be used this way?
    c = np.linalg.solve(A, b)
    h = (c[0] - c[1]) / 2
    vmax = _max_velocity(actx, places, velocity=velocity, dofdesc=dofdesc)

    return capillary_number * h / vmax


def capillary_time_step_estimation(
    actx: ArrayContext,
    places: GeometryCollection,
    velocity: np.ndarray,
    *,
    method: str,
    capillary_number: float,
    viscosity_ratio: float = 1.0,
    sources: Sequence[sym.DOFDescriptorLike] | None = None,
) -> float:
    """Time step estimation based on capillary waves.

    List of methods

    * ``'brackbill'``: uses :func:`capillary_time_step_brackbill`.
    * ``'loewenberg'``: uses :func:`capillary_time_step_loewenberg`.
    * ``'zinchenko2000'``: uses :func:`capillary_time_step_zinchenko2000`.
    * ``'zinchenko2006'``: uses :func:`capillary_time_step_zinchenko2006`.

    :arg sources: a single or list of identifiers for geometries in *places*.
        The returned time step will be a minimum value over all the sources.
    """

    if sources is None:
        sources = [places.auto_source]

    if not isinstance(sources, list | tuple):
        sources = [sources]

    # FIXME: only one source supported at the moment
    assert len(sources) == 1

    method = method.lower()
    if method == "brackbill":
        fn = capillary_time_step_brackbill
    elif method == "loewenberg":
        fn = capillary_time_step_loewenberg
    elif method == "zinchenko2000":
        fn = capillary_time_step_zinchenko2000
    elif method == "zinchenko2006":
        fn = capillary_time_step_zinchenko2006
    else:
        raise ValueError(f"unknown method: '{method}'")

    dt = np.inf
    for dofdesc in sources:
        current_dt = fn(
            actx,
            places,
            capillary_number=capillary_number,
            viscosity_ratio=viscosity_ratio,
            velocity=velocity,
            dofdesc=dofdesc,
        )

        dt = min(dt, actx.to_numpy(current_dt))

    return dt


# }}}
