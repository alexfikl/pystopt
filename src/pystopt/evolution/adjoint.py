# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

"""
.. currentmodule:: pystopt.evolution

.. class:: pystopt.evolution.adjoint.T

.. autoclass:: TimestepEvent
.. autoclass:: StepFailedEvent
.. autoclass:: StateComputedEvent
.. autoclass:: InitialStateEvent

.. autoclass:: TimeStepMethodBase
.. autoclass:: TimeStepMethod

.. autoclass:: AdjointTimeStepMethod

.. autoclass:: ForwardEulerMethod
.. autoclass:: AdjointForwardEulerMethod

.. autoclass:: SSPRK22Method
.. autoclass:: AdjointSSPRK22Method

.. autoclass:: SSPRK33Method
.. autoclass:: AdjointSSPRK33Method

.. autoclass:: SSPRK34Method
"""

from collections.abc import Callable, Iterator
from dataclasses import dataclass
from typing import Any, Generic, TypeVar

from pystopt.checkpoint import IteratorCheckpointManager
from pystopt.tools import get_default_logger

logger = get_default_logger(__name__)


# The way this is evisioned to work is something like this.
#
# 1. Forward Evolution
#
# y[0] = [initial value]
# [give control to the user with y[0]]
# checkpoint(y[0])
#
# for n = 0, ..., N
#   y[n + 1] = step(t[n], y[n])
#   [give control to the user with y[n + 1]]
#   checkpoint(y[n + 1])
#
# 2. Adjoint Evolution
#
# y[N] = checkpoint(N)
# p[N] = [initial value from y[N]]
# [give control to the user with (y[N], p[N])]
#
# for n = N - 1, ..., 1
#   p[n] = step(t[n], y[n], p[n + 1])
#   [give control back to the user with (y[n], p[n])]
#
# 3. Adjoint State
#
# The adjoint state is considered to be the pair `(y[n], p[n])` and this
# does not get to be reused much, unfortunately. For example, consider the
# Forward Euler scheme and two consecutive steps
#
# (y[n], p[n]) = p[n + 1] + dt * f(t[n], (y[n], p[n + 1]))
# (y[n - 1], p[n - 1]) = p[n] + dt * f(t[n - 1], (y[n - 1], p[n])
#
# we can see here that each pair `(y[m], p[n])` only appears once. This is also
# the case with more complicated time integration method with multiple stages
# (true for AB too? Hm..)
#
# Therefore, doing any sort of caching at the level of the pair `(y[n], p[n])`
# is not going to be super helpful :( On the other hand, `y[n]` should be able
# to cache any auxiliary results, since it's used multiple times.


# {{{ events


class StepFailedError(RuntimeError):
    pass


@dataclass(frozen=True)
class TimestepEvent:
    r"""A generic event that can be emitted by :class:`TimeStepMethodBase`.

    .. attribute:: n

        Time step iteration count.

    .. attribute:: t

        Time at iteration :attr:`n`.

    .. attribute:: dt

        Time step at iteration :attr:`n`, such that :math:`t - \Delta t` gives
        the previous iteration time.
    """

    n: int
    t: float
    dt: float


@dataclass(frozen=True)
class StepFailedEvent(TimestepEvent):
    """An event used to signal that a step update failed.

    .. attribute:: state_component

        Value of the states components that were used to cause the failure.
    """

    state_component: Any


@dataclass(frozen=True)
class StateComputedEvent(TimestepEvent):
    """An event used to emit the successful update of the state.

    .. attribute:: state_component

        Value of the state components at iteration :attr:`TimestepEvent.n`.

    .. attribute:: adjoint_component

        When performing the adjoint evolution, this will contain the
        adjoint state components. Otherwise, it is expected to be *None*.
    """

    state_component: Any
    adjoint_component: Any


@dataclass(frozen=True)
class InitialStateEvent(StateComputedEvent):
    """An event used to emit the initial state of the evolution."""


# }}}


# {{{ base time stepper

# NOTE: pytype complains with the TypeVar from `pytools`. Bug?
T = TypeVar("T")


@dataclass
class TimeStepMethodBase(Generic[T]):
    """
    .. attribute:: state_id

        Name of the state variable in :attr:`TimeStepMethod.function`
        or :attr:`AdjointTimeStepMethod.function`.

    .. attribute:: state0

        Initial value for the time stepping variable.

    .. attribute:: dt

        Initial time step, which can be changed by overwriting.

    .. attribute:: filter

        A callable that is used to "filter" the state variables after an update.

    .. attribute:: checkpoint

        A :class:`~pystopt.checkpoint.CheckpointManager` that can read and write
        the :attr:`state0` type object.

    .. automethod:: run
    """

    state_id: str
    state0: T
    dt: float

    filter: Callable[[T], T] | None
    checkpoint: IteratorCheckpointManager | None

    # {{{ outside world interface

    def run(
        self,
        t_end: float | None = None,
        max_steps: int | None = None,
    ) -> Iterator[TimestepEvent]:
        """
        :arg t_end: if *None*, the final time is assumed to be infinite.
        :arg max_steps: if *None*, an infinite number of time steps is assumed.
        """
        raise NotImplementedError

    # }}}

    # {{{ checkpointing helpers

    def write_stages(self, stages: dict[str, T]) -> None:
        if self.checkpoint is None:
            return

        self.checkpoint.write_to(
            f"{self.checkpoint.next_key}_TS_Stages", stages, overwrite=True
        )

    def read_stages(self) -> dict[str, T]:
        return self.checkpoint.read_from(f"{self.checkpoint.prev_key}_TS_Stages")

    # }}}


# }}}


# {{{ forward


@dataclass
class TimeStepMethod(TimeStepMethodBase[T]):
    """Family of standard forward time stepping methods.

    .. attribute:: function

        Right-hand side function used in time stepping.

    .. attribute:: t_start

        Initial time.

    .. automethod:: make_adjoint
    """

    function: Callable[[float, T], T]
    t_start: float

    def call_filter(self, ary: T) -> T:
        if self.filter is None:
            return ary

        return self.filter(ary)

    def call_fun(self, t: float, state: T) -> T:
        return self.function(t, state)

    def run_single_step(self, t: float, state: T) -> T:
        raise NotImplementedError

    def run(
        self,
        t_end: float | None = None,
        max_steps: int | None = None,
    ) -> Iterator[TimestepEvent]:
        n = 0
        t = self.t_start

        # FIXME: should we filter the initial state?
        state = self.call_filter(self.state0)
        yield InitialStateEvent(
            n=n,
            t=t,
            dt=self.dt,
            state_component=state,
            adjoint_component=None,
        )

        while True:
            if t_end is not None and t >= t_end:
                break

            if max_steps is not None and n >= max_steps:
                break

            # {{{ step

            try:
                next_state = self.run_single_step(t, state)
            except StepFailedError:
                yield StepFailedEvent(n=n, t=t, dt=self.dt, state_component=state)
                next_state = state
                continue
            else:
                n = n + 1
                t_next = t + self.dt

                yield StateComputedEvent(
                    n=n,
                    t=t_next,
                    dt=self.dt,
                    state_component=next_state,
                    adjoint_component=None,
                )

            # NOTE: the checkpointing needs to happen after the call to
            # `run_single_step` so that `state` has all the required values
            if self.checkpoint is not None:
                self.checkpoint.write(
                    {"n": n - 1, "t": t, self.state_id: state}, overwrite=True
                )

            t = t_next
            state = next_state

            # }}}

        # NOTE: checkpoint the last iteration as well as well
        if self.checkpoint is not None:
            self.checkpoint.write(
                {"n": n, "t": t, self.state_id: state}, overwrite=True
            )

    # {{{ adjoint

    def get_adjoint_cls(self) -> type:
        raise NotImplementedError

    def make_adjoint(
        self,
        adjoint_id: str,
        adjoint0: T,
        fun: Callable[[float, T], T],
    ) -> "AdjointTimeStepMethod":
        r"""Construct the corresponding discrete adjoint to the time stepper.

        :arg adjoint_id: identifier of the adjoint variable.
        :arg adjoint0: value of the adjoint variable at the final time.
        :arg fun: right-hand side in the adjoint equation.
        """

        if self.checkpoint is None:
            raise ValueError("adjoint functionality requires checkpoints")

        cls = self.get_adjoint_cls()
        return cls(
            state_id=self.state_id,
            adjoint_id=adjoint_id,
            state0=adjoint0,
            function=fun,
            dt=self.dt,
            filter=None,
            checkpoint=self.checkpoint,
        )

    # }}}


# }}}


# {{{ adjoint


@dataclass
class AdjointTimeStepMethod(TimeStepMethodBase[T]):
    r"""Family of adjoint time stepping methods.

    The adjoint equation is assumed to be of the form

    .. math::

        -\frac{p}{t} = f^*(t, p; y)

    where :math:`y` is the corresponding forward variable, which appears
    in the adjoint equation if the forward ODE was nonlinear. The adjoint
    equation is solved backwards in time based on the checkpointed values
    of the forward problem.

    .. attribute:: adjoint_id
    .. attribute:: function

        Right-hand side for the adjoint evolution equation that takes
        ``(t, state, adjoint)``.
    """

    adjoint_id: str
    function: Callable[[float, Any, T], T]

    def call_filter(self, ary: T) -> T:
        if self.filter is None:
            return ary

        return self.filter(ary)

    def call_fun(self, t: float, state: Any, adjoint: T) -> T:
        return self.function(t, state, adjoint)

    def run_adjoint_single_step(self, t: float, state: Any, adjoint: T) -> T:
        raise NotImplementedError

    def run(
        self,
        t_end: float | None = None,
        max_steps: int | None = None,
    ) -> Iterator[TimestepEvent]:
        if self.checkpoint is None:
            raise ValueError("adjoint methods require a checkpoint")

        # {{{ get final time values

        self.checkpoint.reverse()
        chk = self.checkpoint.read()

        n = chk["n"]
        t = chk["t"]
        state = chk[self.state_id]

        # }}}

        adjoint = self.call_filter(self.state0)
        yield InitialStateEvent(
            n=n, t=t, dt=self.dt, state_component=state, adjoint_component=adjoint
        )

        while n > 0:
            # {{{ retrieve checkpoint state

            chk = self.checkpoint.read()
            assert chk["n"] == n - 1

            self.dt = t - chk["t"]
            state = chk[self.state_id]

            n = n - 1
            t = chk["t"]

            # }}}

            # {{{ step

            try:
                adjoint = self.run_adjoint_single_step(t, state, adjoint)
            except StepFailedError:
                yield StepFailedEvent(n=n, t=t, dt=self.dt, state_component=adjoint)
            else:
                yield StateComputedEvent(
                    n=n,
                    t=t,
                    dt=self.dt,
                    state_component=state,
                    adjoint_component=adjoint,
                )

            # }}}


# }}}


# {{{ forward euler


@dataclass
class ForwardEulerMethod(TimeStepMethod[T]):
    """Forward Euler time stepper."""

    def run_single_step(self, t: float, state: T) -> T:
        return self.call_filter(state + self.dt * self.call_fun(t, state))

    def get_adjoint_cls(self) -> type:
        return AdjointForwardEulerMethod


@dataclass
class AdjointForwardEulerMethod(AdjointTimeStepMethod[T]):
    """Adjoint of :class:`ForwardEulerMethod`."""

    def run_adjoint_single_step(self, t: float, state: Any, adjoint: T) -> T:
        return self.call_filter(adjoint + self.dt * self.call_fun(t, state, adjoint))


# }}}


# {{{ ssprk22


@dataclass
class SSPRK22Method(TimeStepMethod[T]):
    """Classic second-order SSP Runge-Kutta method with 2 stages."""

    def run_single_step(self, t: float, state: T) -> T:
        k1 = self.call_filter(state + self.dt * self.call_fun(t, state))
        result = self.call_filter(
            state / 2.0 + k1 / 2.0 + self.dt / 2.0 * self.call_fun(t + self.dt, k1)
        )

        # NOTE: checkpoint after computation so that all values are cached
        self.write_stages({"k1": k1})

        return result

    def get_adjoint_cls(self) -> type:
        return AdjointSSPRK22Method


@dataclass
class AdjointSSPRK22Method(AdjointTimeStepMethod[T]):
    """Adjoint of :class:`SSPRK22Method`."""

    def run_adjoint_single_step(self, t: float, state: Any, adjoint: T) -> T:
        stages = self.read_stages()
        k1star = self.call_filter(
            adjoint + self.dt * self.call_fun(t + self.dt, stages["k1"], adjoint)
        )

        return self.call_filter(
            adjoint / 2.0
            + k1star / 2.0
            + self.dt / 2.0 * self.call_fun(t, state, k1star)
        )


# }}}


# {{{ ssprk33


@dataclass
class SSPRK33Method(TimeStepMethod[T]):
    """Classic third-order SSP Runge-Kutta method with 3 stages."""

    def run_single_step(self, t: float, state: T) -> T:
        k1 = self.call_filter(state + self.dt * self.call_fun(t, state))
        k2 = self.call_filter(
            3.0 / 4.0 * state
            + 1.0 / 4.0 * k1
            + 1.0 / 4.0 * self.dt * self.call_fun(t + self.dt, k1)
        )
        result = self.call_filter(
            1.0 / 3.0 * state
            + 2.0 / 3.0 * k2
            + 2.0 / 3.0 * self.dt * self.call_fun(t + self.dt / 2.0, k2)
        )

        # NOTE: checkpoint after computation so that all values are cached
        self.write_stages({"k1": k1, "k2": k2})

        return result

    def get_adjoint_cls(self) -> type:
        return AdjointSSPRK33Method


@dataclass
class AdjointSSPRK33Method(AdjointTimeStepMethod[T]):
    """Adjoint of :class:`SSPRK33Method`."""

    def run_adjoint_single_step(self, t: float, state: Any, adjoint: T) -> T:
        stages = self.read_stages()

        k2star = self.call_filter(
            adjoint + self.dt * self.call_fun(t + self.dt / 2.0, stages["k2"], adjoint)
        )
        k1star = self.call_filter(
            k2star + self.dt * self.call_fun(t + self.dt, stages["k1"], k2star)
        )

        return self.call_filter(
            1.0 / 3.0 * adjoint
            + 1.0 / 2.0 * k2star
            + 1.0 / 6.0 * k1star
            + self.dt / 6.0 * self.call_fun(t, state, k1star)
        )


# }}}


# {{{ ssprk34


@dataclass
class SSPRK34Method(TimeStepMethod[T]):
    """A third-order SSP Runge-Kutta method with 4 stages."""

    def run_single_step(self, t: float, state: T) -> T:
        # FIXME: this keeps all the stages in memory!
        k1 = self.call_filter(state + self.dt / 2.0 * self.call_fun(t, state))
        k2 = self.call_filter(k1 + self.dt / 2.0 * self.call_fun(t + self.dt / 2.0, k1))
        k3 = self.call_filter(
            2.0 / 3.0 * state
            + 1.0 / 3.0 * k2
            + self.dt / 6.0 * self.call_fun(t + self.dt, k2)
        )
        result = self.call_filter(
            k3 + self.dt / 2.0 * self.call_fun(t + self.dt / 2.0, k3)
        )

        # NOTE: checkpoint after computation so that all values are cached
        self.write_stages({"k1": k1, "k2": k2, "k3": k3})

        return result

    def get_adjoint_cls(self) -> type:
        return AdjointSSPRK34Method


@dataclass
class AdjointSSPRK34Method(AdjointTimeStepMethod[T]):
    """Adjoint of :class:`SSPRK34Method`."""

    def run_adjoint_single_step(self, t: float, state: Any, adjoint: T) -> T:
        stages = self.read_stages()
        k1, k2, k3 = stages["k1"], stages["k2"], stages["k3"]

        k3star = self.call_filter(
            adjoint + self.dt / 2.0 * self.call_fun(t + self.dt / 2.0, k3, adjoint)
        )
        k2star = self.call_filter(
            k3star / 3.0 + self.dt / 6.0 * self.call_fun(t + self.dt, k2, k3star)
        )
        k1star = self.call_filter(
            k2star + self.dt / 2.0 * self.call_fun(t + self.dt / 2.0, k1, k2star)
        )

        return self.call_filter(
            k1star
            + 2.0 / 3.0 * k3star
            + self.dt / 2.0 * self.call_fun(t, state, k1star)
        )


# }}}
