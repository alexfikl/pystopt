# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

"""
.. currentmodule:: pystopt.evolution

.. autoclass:: StokesEvolutionOperator
"""

from dataclasses import dataclass

import numpy as np

from pystopt import sym
from pystopt.tools import get_default_logger

logger = get_default_logger(__name__)


@dataclass(frozen=True)
class StokesEvolutionOperator:
    r"""Evolves an interface in Stokes flow.

    The evolution equation is given by

    .. math::

        \frac{\mathrm{d} \mathbf{X}}{\mathrm{d} t} =
        (\mathbf{u} \cdot \mathbf{n}) \mathbf{n}
        + (I - \mathbf{n} \otimes \mathbf{n}) \mathbf{u}_\tau
        + \mathbf{s},

    where the first term is purely normal, the second term is purely
    tangential and :math:`\mathbf{s}` is an additional user-provided source
    term (usually :math:`0`). If :attr:`force_normal_velocity` is *False*, the
    first term is replaces by :math:`\mathbf{u}`. If
    :attr:`force_tangential_velocity` is *False*, the second term is omitted, even
    if it is provided to :meth:`get_sym_operator`.

    When binding the operator from :meth:`get_sym_operator`, the user must
    provide the Stokes velocity field :math:`\mathbf{u}` and the tangential
    velocity field :math:`\mathbf{u}_\tau`.

    .. attribute:: force_normal_velocity

    .. attribute:: force_tangential_velocity

    .. automethod:: __init__
    .. automethod:: get_sym_operator
    """

    force_normal_velocity: bool = True
    force_tangential_velocity: bool = False
    force_volume_conservation: bool = False

    def get_sym_operator(
        self,
        u: np.ndarray,
        w: np.ndarray | None = None,
        dim: int | None = None,
        dofdesc: sym.DOFDescriptorLike | None = None,
    ) -> np.ndarray:
        ambient_dim = len(u)
        if dim is None:
            dim = ambient_dim - 1

        normal = sym.normal(ambient_dim, dim=dim, dofdesc=dofdesc).as_vector()
        u_dot_n = sym.cse(u @ normal)

        expr = 0
        if self.force_normal_velocity:
            expr = expr + u_dot_n * normal
        else:
            expr = expr + u

        if self.force_tangential_velocity:
            assert w is not None
            assert w.size == ambient_dim

            # remove any normal components from the "tangential velocity", to
            # be extra safe
            w = w - (w @ normal) * normal
            expr = expr + w

        if self.force_volume_conservation:
            area = sym.surface_area(ambient_dim, dim=dim, dofdesc=dofdesc)
            xi = -sym.sintegral(u_dot_n, ambient_dim, dim, dofdesc=dofdesc) / area

            expr = expr + xi * normal

        return expr
