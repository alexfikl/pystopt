# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

from pytential.qbx import QBXLayerPotentialSource as QBXLayerPotentialSourceBase


def count_fmms_in_bound_exprs(expr):
    from pytential.symbolic.compiler import ComputePotential

    return len([
        insn for insn in expr.code.instructions if isinstance(insn, ComputePotential)
    ])


class QBXLayerPotentialSource(QBXLayerPotentialSourceBase):
    def preprocess_optemplate(self, name, discretizations, expr):
        """
        :arg name: The symbolic name for *self*, which the preprocessor
            should use to find which expressions it is allowed to modify.
        """
        from pystopt.symbolic.mappers import QBXPreprocessor

        return QBXPreprocessor(name, discretizations)(expr)
