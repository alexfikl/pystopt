# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

"""
.. currentmodule:: pystopt.optimize

.. autofunction:: flatten_to_numpy
.. autofunction:: unflatten_from_numpy_like
.. autofunction:: get_vdot_for_container
.. autofunction:: get_norm_for_container

.. autoclass:: CGResult
    :members:
.. autoclass:: CGCallbackInfo
    :members:
.. autoclass:: CGLogCallback
    :members:
.. autoclass:: CGHistoryCallback
    :members:
"""

from collections.abc import Callable
from dataclasses import dataclass, field
from typing import Any

import numpy as np

from arraycontext import (  # noqa: F401
    ArrayContainer,
    ArrayContext,
    ArrayOrContainer,
    ArrayOrContainerT,
)

from pystopt.callbacks import HistoryCallback, LogCallback
from pystopt.tools import get_default_logger

logger = get_default_logger(__name__)


# {{{ flatten and unflatten


def flatten_to_numpy(actx: ArrayContext | None, ary: ArrayOrContainer) -> np.ndarray:
    """Flatten and transfer an array container to :mod:`numpy`.

    This is a composition of :meth:`~arraycontext.ArrayContext.to_numpy` and
    :func:`~arraycontext.flatten`.
    """
    if isinstance(ary, np.ndarray) and ary.dtype.char != "O":
        return ary

    if actx is None:
        raise ValueError("'actx' is required for device containers")

    from arraycontext import flatten

    return actx.to_numpy(flatten(ary, actx))


def unflatten_from_numpy_like(
    actx: ArrayContext | None, ary: np.ndarray, prototype: ArrayOrContainerT
) -> ArrayOrContainerT:
    """A reverse operator to :func:`flatten_to_numpy`."""
    if isinstance(prototype, np.ndarray) and prototype.dtype.char != "O":
        return ary

    if actx is None:
        raise ValueError("'actx' is required for device containers")

    from arraycontext import unflatten

    return unflatten(prototype, actx.from_numpy(ary), actx)


# }}}


# {{{ structured_vdot


def _vdot_real(
    actx: ArrayContext,
    x: ArrayOrContainerT,
    y: ArrayOrContainerT,
) -> float:
    return actx.to_numpy(actx.np.vdot(x, y))


def _vdot_complex(
    actx: ArrayContext,
    x: ArrayOrContainerT,
    y: ArrayOrContainerT,
) -> float:
    def _cdot(ix, iy):
        # computes `ix.real * iy.real + ix.imag + iy.imag`
        return actx.np.sum(actx.np.real(ix * actx.np.conj(iy)))

    from arraycontext import rec_multimap_reduce_array_container

    return actx.to_numpy(rec_multimap_reduce_array_container(sum, _cdot, x, y))


def get_vdot_for_container(
    x: ArrayOrContainerT,
    actx: ArrayContext | None = None,
) -> Callable[[ArrayOrContainerT, ArrayOrContainerT], float]:
    """Uses heuristics to get a :func:`~numpy.vdot` callable for the type of *x*."""
    if isinstance(x, np.ndarray) and x.dtype.char != "O":
        if x.dtype.kind == "f":
            return np.vdot
        elif x.dtype.kind == "c":
            return lambda ary0, ary1: np.sum(np.real(ary0 * np.conj(ary1)))
        else:
            raise ValueError(f"unsupported container dtype: {x.dtype}")

    if actx is None:
        from pystopt.dof_array import get_container_context_recursively

        actx = get_container_context_recursively(x)

    if actx is None:
        raise ValueError("array context must be provided for device arrays")

    from arraycontext import rec_map_reduce_array_container

    dtypes = set()
    dtypes = rec_map_reduce_array_container(
        lambda args: dtypes.union(*args), lambda ary: {ary.dtype}, x
    )
    common_dtype = np.result_type(*dtypes)

    from functools import partial

    if common_dtype.kind == "f":
        return partial(_vdot_real, actx)
    elif common_dtype.kind == "c":
        return partial(_vdot_complex, actx)
    else:
        raise ValueError(f"unsupported container dtype: {common_dtype}")


def get_norm_for_container(
    x: ArrayOrContainerT,
    actx: ArrayContext | None = None,
) -> Callable[[ArrayOrContainerT], float]:
    """Uses heuristics to get a :func:`~numpy.linalg.norm` for the type of *x*."""
    if isinstance(x, np.ndarray) and x.dtype.char != "O":
        return np.linalg.norm

    if actx is None:
        from pystopt.dof_array import get_container_context_recursively

        actx = get_container_context_recursively(x)

    if actx is None:
        raise ValueError("array context must be provided for device arrays")

    from pystopt.dof_array import dof_array_norm

    return lambda ary: actx.to_numpy(dof_array_norm(ary))


# }}}


# {{{ cg result


@dataclass(frozen=True)
class CGResult:
    """Based on :class:`scipy.optimize.OptimizeResult`."""

    x: np.ndarray
    """Solution of the optimization."""
    success: bool
    """Flag to denote a successful exit."""
    status: int
    """Termination status of the optimize."""
    message: str
    """Description of the termination status in :attr:`status`."""
    fun: float
    """Function value at the end of the optimization."""
    jac: float
    """Norm of the gradient (Jacobian) at the end of the optimization."""
    nfev: int
    """Number of function evaluations."""
    njev: int
    """Number of gradient (Jacobian) evaluation."""
    nit: int
    """Number of iterations performed by the optimizer."""

    def pretty(self) -> str:
        """A fancier stringify of the results."""
        from pystopt.tools import dc_stringify

        return dc_stringify(self)


def combine_cg_results(
    current: CGResult, previous: CGResult, *, n: int = 0
) -> CGResult:
    if isinstance(n, bool):
        n = 0
    else:
        n = n - previous.nfev

    from dataclasses import replace

    return replace(
        current,
        nfev=current.nfev + previous.nfev + n,
        njev=current.njev + previous.njev + n,
        nit=current.nit + previous.nit + n,
    )


# }}}


# {{{ callback info


@dataclass(frozen=True)
class CGCallbackInfo:
    it: int
    """Current iteration."""
    alpha: float
    r"""Current step size, such that :math:`x = x_p + \alpha d`, where
    :math:`x_p` is the previous iterate.
    """
    x: np.ndarray
    """Solution at current iteration."""
    f: float
    """Function value at current iteration."""
    g: np.ndarray
    """Gradient at the current iteration."""
    d: np.ndarray
    """Descent direction at the current iteration. This will usually not
    be the same as the gradient and can be used for debugging.
    """


# }}}


# {{{ log callback


@dataclass(frozen=True)
class CGLogCallback(LogCallback):
    """
    .. automethod:: __call__
    """

    _f_prev: float = field(default=np.inf, init=False, repr=False)
    _g_prev: float = field(default=np.inf, init=False, repr=False)

    def __call__(self, *args: Any, **kwargs: Any) -> int:
        """
        :arg info: an instance of :class:`CGCallbackInfo`.
        """
        info = kwargs["info"]
        assert isinstance(info.g, np.ndarray)

        from pystopt.tools import c

        if self.norm is None:
            s_gnorm = s_dnorm = "<unknown>"
        else:
            gnorm = self.norm(info.g)
            s_gnorm = c.message(f"{gnorm:.7e}", success=gnorm < self._g_prev)

            dnorm = gnorm if info.d is None else self.norm(info.d)
            s_dnorm = c.message(f"{dnorm:.7e}", success=gnorm < self._g_prev)

            object.__setattr__(self, "_g_prev", gnorm)
        object.__setattr__(self, "_f_prev", info.f)

        self.log(
            "IT %-8d f %s |g| %s |d| %s alpha %.7e",
            info.it,
            c.message(f"{info.f:.7e}", success=info.f < self._f_prev),
            s_gnorm,
            s_dnorm,
            info.alpha,
        )

        return 1


# }}}


# {{{ history callback


@dataclass(frozen=True)
class CGHistoryCallback(HistoryCallback):
    """Store values from :class:`CGCallbackInfo` from all iterations.

    The values stored as the function values, the gradient norm and the
    descent direction norm.

    .. automethod:: __call__
    """

    f: list[float] = field(default_factory=list, init=False, repr=False)
    """History of function values."""
    g: list[float] = field(default_factory=list, init=False, repr=False)
    r"""History of :math:`\ell^2` norms of the gradient."""
    d: list[float] = field(default_factory=list, init=False, repr=False)
    r"""History of :math:`\ell^2` norms of the descent direction."""
    alpha: list[float] = field(default_factory=list, init=False, repr=False)
    """History of step sizes."""

    def __call__(self, info, *args, **kwargs):
        """
        :arg info: an instance of :class:`CGCallbackInfo`.
        """
        if getattr(info, "f", None) is not None:
            self.f.append(info.f)

        if getattr(info, "g", None) is not None:
            self.g.append(np.linalg.norm(info.g, ord=2))

        if getattr(info, "d", None) is not None:
            self.d.append(np.linalg.norm(info.d, ord=2))

        if getattr(info, "alpha", None) is not None:
            self.alpha.append(info.alpha)

        return 1


# }}}
