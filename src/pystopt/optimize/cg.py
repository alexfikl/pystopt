# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

from collections.abc import Callable
from dataclasses import replace
from functools import partial
from typing import Any

import numpy as np
import pycgdescent as cg

from pystopt.tools import dc_asdict, get_default_logger

logger = get_default_logger(__name__)


# {{{ minimize


def unflatten_info(unflatten: Callable[[Any], Any], *args: Any):
    (info,) = args
    info = replace(info, x=unflatten(info.x), g=unflatten(info.g), d=None)

    from pystopt.optimize.cg_utils import CGCallbackInfo

    return CGCallbackInfo(**dc_asdict(info))


def flatten_to_real(actx, ary, *, return_type: bool = False):
    from pystopt.optimize.cg_utils import flatten_to_numpy

    result = flatten_to_numpy(actx, ary)
    dtype = result.dtype
    if dtype.char == "c":
        result = np.concatenate(result.real, result.imag)

    if return_type:
        return result, dtype

    return result


def unflatten_from_real(actx, ary, prototype, *, force_complex: bool = False):
    if force_complex:
        assert ary.size % 2 == 0
        cary = 1j * ary[ary.size // 2 :]
        cary += ary[: ary.size // 2]
    else:
        cary = ary

    from pystopt.optimize.cg_utils import unflatten_from_numpy_like

    return unflatten_from_numpy_like(actx, cary, prototype)


def minimize(
    fun: cg.FunType,
    x0: np.ndarray,
    *,
    jac: cg.GradType,
    funjac: cg.FunGradType | None = None,
    rtol: float | None = None,
    options: cg.OptimizeOptions | None = None,
    callback: cg.CallbackType = False,
    work: np.ndarray | None = None,
) -> cg.OptimizeResult:
    """Wrapper around :func:`pycgdescent.minimize`."""
    from pystopt.dof_array import get_container_context_recursively

    actx = get_container_context_recursively(x0)
    x0_flat, dtype = flatten_to_real(actx, x0, return_type=True)

    from pystopt.callbacks import make_default_optimize_callback
    from pystopt.optimize.cg_utils import CGLogCallback, get_norm_for_container

    callback = make_default_optimize_callback(
        callback=callback,
        log_callback_factory=CGLogCallback(
            norm=get_norm_for_container(x0, actx), log=logger.info
        ),
    )

    # {{{ wrappers

    flatten = partial(flatten_to_real, actx)
    unflatten = partial(
        unflatten_from_real, actx, prototype=x0, force_complex=dtype.char == "c"
    )

    def _fun_wrap(x):
        return fun(unflatten(x))

    def _jac_wrap(g, x):
        g[...] = flatten(jac(unflatten(x)))

    def _funjac_wrap(g, x):
        feval, geval = funjac(unflatten(x))
        g[...] = flatten(geval)

        return feval

    def _callback_wrap(*args):
        info = unflatten_info(unflatten, *args)
        return callback(info.x, info=info)

    # }}}

    r = cg.minimize(
        fun=_fun_wrap,
        x0=x0_flat,
        jac=_jac_wrap,
        funjac=_funjac_wrap,
        tol=rtol,
        options=options,
        callback=_callback_wrap,
        work=work,
    )

    # NOTE: r.x is the only vector in cg.OptimizeResult, everyone else is a scalar
    r = dc_asdict(replace(r, x=unflatten(r.x)))
    del r["nsubspaceit"], r["nsubspaces"]

    from pystopt.optimize.cg_utils import CGResult

    return CGResult(**r)


# }}}
