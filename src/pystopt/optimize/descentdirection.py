# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

"""
.. currentmodule:: pystopt.optimize

.. note::

    Some descent directions should exclusively be used with a specific
    implementation of :class:`pystopt.optimize.LineSearch`.

All descent directions can be obtained programmatically using

.. autofunction:: get_descent_direction_from_name

Descent directions must be subclasses of

.. autoclass:: DescentDirection
    :no-show-inheritance:

and implement the following function through :func:`functools.singledispatch`

.. autofunction:: descent_direction

Concrete implementations include

.. autoclass:: SteepestDescentDirection
.. autoclass:: HestenesStiefelDirection
.. autoclass:: FletcherReevesDirection
.. autoclass:: PolakRibiereDirection
.. autoclass:: FletcherDirection
.. autoclass:: LiuStoreyDirection
.. autoclass:: DaiYuanDirection
.. autoclass:: HagerZhangDirection
"""

import sys
from dataclasses import dataclass, field
from functools import singledispatch
from typing import Generic

import pystopt.optimize.riemannian as riem
from pystopt.tools import get_default_logger

logger = get_default_logger(__name__)

if getattr(sys, "_BUILDING_SPHINX_DOCS", False):
    from typing import TypeVar

    ArrayOrContainerT = TypeVar("ArrayOrContainerT")
else:
    from arraycontext import ArrayOrContainerT


# {{{ interface

_DESCENT_DIRECTION_REGISTRY = None


def get_descent_direction_from_name(name: str, **kwargs) -> "DescentDirection":
    """
    :arg name: a string identifier for the descent direction method. The
        identifiers follow the naming convention ``"word-word-..."```,
        where each word is the lowercase part of the class name. For example,
        :class:`FletcherReevesDirection` is identified by ``"fletcher-reeves"``.

    :arg kwargs: keyword arguments passed directly to the class constructor.
    """
    global _DESCENT_DIRECTION_REGISTRY  # noqa: PLW0603
    if _DESCENT_DIRECTION_REGISTRY is None:
        _DESCENT_DIRECTION_REGISTRY = {
            "steepest": SteepestDescentDirection,
            "hestenes-stiefel": HestenesStiefelDirection,
            "fletcher-reeves": FletcherReevesDirection,
            "polak-ribiere": PolakRibiereDirection,
            "fletcher": FletcherDirection,
            "liu-storey": LiuStoreyDirection,
            "dai-yuan": DaiYuanDirection,
            "hager-zhang": HagerZhangDirection,
        }

    if name not in _DESCENT_DIRECTION_REGISTRY:
        raise ValueError(f"unknown descent direction: '{name}'")

    cls = _DESCENT_DIRECTION_REGISTRY[name]

    if __debug__:
        from dataclasses import fields

        known_fields = {f.name for f in fields(cls)}
        given_fields = set(kwargs)
        unknown_fields = given_fields - known_fields
        if unknown_fields:
            raise AssertionError(f"unknown fields: {unknown_fields}")

    return cls(**kwargs)


@dataclass
class DescentDirection(Generic[ArrayOrContainerT]):
    r"""The descent direction is computed as

    .. math::

        \mathbf{d}^{(k + 1)} = -\mathbf{g}^{(k + 1)} + \beta^{(k)} \mathbf{d}^{(k)}

    where the CG update parameter :math:`\beta` depends on

    .. math::

        \beta^{(k)} \equiv \beta^{(k)}(
            \mathbf{g}^{(k + 1)}, \mathbf{g}^{(k)}, \mathbf{d}^{(k)}
            )
    """

    _x_prev: ArrayOrContainerT | None = field(default=None, init=False, repr=False)
    _g_prev: ArrayOrContainerT | None = field(default=None, init=False, repr=False)
    _d_prev: ArrayOrContainerT | None = field(default=None, init=False, repr=False)

    def force_update(
        self, x: ArrayOrContainerT, g: ArrayOrContainerT, d: ArrayOrContainerT
    ) -> None:
        self._x_prev = x
        self._g_prev = g
        self._d_prev = d

        return d


@singledispatch
def descent_direction(
    ds: DescentDirection,
    m: riem.RiemannianManifold,
    x: ArrayOrContainerT,
    g: ArrayOrContainerT,
) -> ArrayOrContainerT:
    r"""Constructs :math:`\mathbf{d}^{(k + 1)}` from :math:`\mathbf{g}^{(k + 1)}`.
    and the datum at the previous step in :class:`DescentDirection`.
    """
    raise NotImplementedError(type(ds).__name__)


# }}}


# {{{ steepest descent


@dataclass
class SteepestDescentDirection(DescentDirection):
    pass


@descent_direction.register(SteepestDescentDirection)
def _update_steepest(
    ds: SteepestDescentDirection,
    m: riem.RiemannianManifold,
    x: ArrayOrContainerT,
    g: ArrayOrContainerT,
) -> ArrayOrContainerT:
    # update descent direction
    d = -g

    return ds.force_update(x, g, d)


# }}}


# {{{ hestenes-stiefel


@dataclass
class HestenesStiefelDirection(DescentDirection):
    pass


@descent_direction.register(HestenesStiefelDirection)
def _update_hestenes_stiefel(
    ds: HestenesStiefelDirection,
    m: riem.RiemannianManifold,
    x: ArrayOrContainerT,
    g: ArrayOrContainerT,
) -> ArrayOrContainerT:
    # transport descent directions to new points
    g_prev = riem.transport(m, ds._x_prev, x, ds._g_prev)
    d_prev = riem.transport(m, ds._x_prev, x, ds._d_prev)

    # compute cg coefficient
    y = g - g_prev
    beta = riem.metric(m, x, g, y) / riem.metric(m, x, d_prev, y)

    # update descent direction
    d = -g + beta * d_prev

    return ds.force_update(x, g, d)


# }}}


# {{{ fletcher-reeves


@dataclass
class FletcherReevesDirection(DescentDirection):
    pass


@descent_direction.register(FletcherReevesDirection)
def _update_fletcher_reeves(
    ds: FletcherReevesDirection,
    m: riem.RiemannianManifold,
    x: ArrayOrContainerT,
    g: ArrayOrContainerT,
) -> ArrayOrContainerT:
    # transport descent directions to new points
    g_prev = riem.transport(m, ds._x_prev, x, ds._g_prev)
    d_prev = riem.transport(m, ds._x_prev, x, ds._d_prev)

    # compute cg coefficient
    # NOTE: can we compute the second metric on x_prev? and not transport g_prev
    beta = riem.metric(m, x, g, g) / riem.metric(m, x, g_prev, g_prev)

    # update descent direction
    d = -g + beta * d_prev

    return ds.force_update(x, g, d)


# }}}


# {{{ polak-ribiere


@dataclass
class PolakRibiereDirection(DescentDirection):
    pass


@descent_direction.register(PolakRibiereDirection)
def _update_polak_ribiere(
    ds: PolakRibiereDirection,
    m: riem.RiemannianManifold,
    x: ArrayOrContainerT,
    g: ArrayOrContainerT,
) -> ArrayOrContainerT:
    # transport descent directions to new points
    g_prev = riem.transport(m, ds._x_prev, x, ds._g_prev)
    d_prev = riem.transport(m, ds._x_prev, x, ds._d_prev)

    # compute cg coefficient
    y = g - g_prev
    beta = riem.metric(m, x, g, y) / riem.metric(m, x, g_prev, g_prev)

    # update descent direction
    d = -g + beta * d_prev

    return ds.force_update(x, g, d)


# }}}


# {{{ fletcher


@dataclass
class FletcherDirection(DescentDirection):
    pass


@descent_direction.register(FletcherDirection)
def _update_fletcher(
    ds: FletcherDirection,
    m: riem.RiemannianManifold,
    x: ArrayOrContainerT,
    g: ArrayOrContainerT,
) -> ArrayOrContainerT:
    # transport descent directions to new points
    g_prev = riem.transport(m, ds._x_prev, x, ds._g_prev)
    d_prev = riem.transport(m, ds._x_prev, x, ds._d_prev)

    # compute cg coefficient
    beta = -riem.metric(m, x, g, g) / riem.metric(m, x, g_prev, d_prev)

    # update descent direction
    d = -g + beta * d_prev

    return ds.force_update(x, g, d)


# }}}


# {{{ liu-storey


@dataclass
class LiuStoreyDirection(DescentDirection):
    pass


@descent_direction.register(LiuStoreyDirection)
def _update_liu_storey(
    ds: LiuStoreyDirection,
    m: riem.RiemannianManifold,
    x: ArrayOrContainerT,
    g: ArrayOrContainerT,
) -> ArrayOrContainerT:
    # transport descent directions to new points
    g_prev = riem.transport(m, ds._x_prev, x, ds._g_prev)
    d_prev = riem.transport(m, ds._x_prev, x, ds._d_prev)

    # compute cg coefficient
    y = g - g_prev
    beta = -riem.metric(m, x, g, y) / riem.metric(m, x, g_prev, d_prev)

    # update descent direction
    d = -g + beta * d_prev

    return ds.force_update(x, g, d)


# }}}


# {{{ dai-yuan


@dataclass
class DaiYuanDirection(DescentDirection):
    pass


@descent_direction.register(DaiYuanDirection)
def _update_dai_yuan(
    ds: DaiYuanDirection,
    m: riem.RiemannianManifold,
    x: ArrayOrContainerT,
    g: ArrayOrContainerT,
) -> ArrayOrContainerT:
    # transport descent directions to new points
    g_prev = riem.transport(m, ds._x_prev, x, ds._g_prev)
    d_prev = riem.transport(m, ds._x_prev, x, ds._d_prev)

    # compute cg coefficient
    y = g - g_prev
    beta = riem.metric(m, x, g, g) / riem.metric(m, x, y, d_prev)

    # update descent direction
    d = -g + beta * d_prev

    return ds.force_update(x, g, d)


# }}}


# {{{ hager-zhang


@dataclass
class HagerZhangDirection(DescentDirection):
    pass


@descent_direction.register(HagerZhangDirection)
def _update_hager_zhang(
    ds: DaiYuanDirection,
    m: riem.RiemannianManifold,
    x: ArrayOrContainerT,
    g: ArrayOrContainerT,
) -> ArrayOrContainerT:
    # transport descent directions to new points
    g_prev = riem.transport(m, ds._x_prev, x, ds._g_prev)
    d_prev = riem.transport(m, ds._x_prev, x, ds._d_prev)

    # compute cg coefficient
    y = g - g_prev
    d_dot_y = riem.metric(m, x, d_prev, y)
    y_dot_y = riem.metric(m, x, y, y)
    beta = riem.metric(m, x, y - 2 * y_dot_y / d_dot_y * d_prev, g) / d_dot_y

    # update descent direction
    d = -g + beta * d_prev

    return ds.force_update(x, g, d)


# }}}
