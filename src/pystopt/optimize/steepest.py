# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

import enum
import sys
from collections.abc import Callable
from dataclasses import dataclass, field
from typing import Any, Generic, TypeVar

import numpy as np

from pystopt.optimize.cg_utils import CGResult
from pystopt.tools import get_default_logger

logger = get_default_logger(__name__)
T = TypeVar("T")

if getattr(sys, "_BUILDING_SPHINX_DOCS", False):
    ArrayOrContainerT = TypeVar("ArrayOrContainerT")
else:
    from arraycontext import ArrayOrContainerT


# {{{ interface


def _dof_array_size(ary):
    from meshmode.dof_array import DOFArray

    if isinstance(ary, np.ndarray) and ary.dtype.char == "O":
        return _dof_array_size(ary[0])
    elif isinstance(ary, DOFArray):
        return sum(iary.size for iary in ary)
    else:
        return ary.size


def minimize(
    fun: Callable[[T], float],
    jac: Callable[[T], T],
    x0: T,
    *,
    funjac: Callable[[T], tuple[float, T]] | None = None,
    rtol: float | None = None,
    options: dict[str, Any] | None = None,
    callback: Callable[..., int] | bool = False,
) -> CGResult:
    r"""Minimize scalar functions by gradient descent methods.

    The standard update formula for conjugate gradient algorithms is given by

    .. math::

        \begin{aligned}
            \mathbf{x}^{(k + 1)} = \,\, & \mathbf{x}^{(k)}
                + \alpha^{(k)} \mathbf{d}^{(k)}, \\
            \mathbf{d}^{(k + 1)} = \,\, & -\mathbf{g}^{(k + 1)}
                + \beta^{(k)} \mathbf{d}^{(k)},
        \end{aligned}

    where :math:`\alpha` is the step size, determined by a
    :class:`~pystopt.optimize.LineSearch`, :math:`\mathbf{d}` is the
    descent direction and :math:`\beta` is the CG update parameter, determined
    by :class:`~pystopt.optimize.descentdirection.DescentDirection`.

    Different classic CG methods correspond to different choices of the
    :math:`\beta` parameter. For example taking :math:`\beta = 0`, we obtain
    the standard steepest descent method.

    Available options:
    * `maxit` (default `np.inf`): maximum number of CG iterations.
    * `rtol` (default `1.0e-6`): relative decrease in gradient magnitude.
    * `ftol` (default `1.0e-8`): relative decrease in function magnitude.
    * `increase_iterations` (default `5`): maximum number of consecutive
      iterations over which the function values can increase.
    * `alpha_stall_iterations` (default `3`): maximum number of consecutive
      iterations over which the step size can stall.
    * `restart` (default `np.inf`): number of iterations before a restart.
      Restarts can also happen automatically if the descent direction no
      longer points in the direction of the gradient.
    * `linesearch`: a :class:`~pystopt.optimize.LineSearch` instance
      describing the line search method.
    * `descent`: a :class:`~pystopt.optimize.descentdirection.DescentDirection`
      instance describing the CG descent direction.
    * `manifold`: a :class:`~pystopt.optimize.riemann.RiemannianManifold`
      that is used to compute gradient magnitudes and transport.

    :arg fun: a callable that returns the function values at a given point *x*.
    :arg jac: a callable that returns the gradient at a given point *x*.
    :arg x0: initial guess.
    :arg funjac: a callable that returns both the function value at the
        gradient at a given point. This is used in cases where both of them are
        needed and can speed up computations by avoiding repeated setup.
    :arg rtol: relative decrease in gradient magnitude.
    :arg options: a :class:`dict` of options as defined above.
    :arg callback: a callable that is called at the end of each iteration.
        A helper wrapper is given in :class:`~pystopt.optimize.CallbackManager`.
    :returns: a :class:`~pystopt.optimize.CGResult`.
    """

    # {{{ handle options

    if options is None:
        options = {}

    maxit = options.get("maxit", np.iinfo(np.int64).max)
    rtol = options.get("rtol", rtol)
    ftol = options.get("ftol", 1.0e-2 * rtol)
    increase_iterations = options.get("increase_iterations", 5)
    alpha_stall_iterations = options.get("alpha_stall_iterations", 3)
    restart = options.get("restart", maxit)
    linesearch = options.get("linesearch", None)
    descent = options.get("descent", None)
    manifold = options.get("manifold", None)

    if linesearch is None:
        from pystopt.optimize.linesearch import BacktrackingLineSearch

        linesearch = BacktrackingLineSearch(fun=fun)

    if descent is None:
        from pystopt.optimize.descentdirection import SteepestDescentDirection

        descent = SteepestDescentDirection()

    if manifold is None:
        from pystopt.dof_array import get_container_context_recursively

        actx = get_container_context_recursively(x0)

        from pystopt.optimize.cg_utils import get_vdot_for_container

        vdot = get_vdot_for_container(x0, actx=actx)

        from pystopt.optimize.riemannian import CartesianEucledeanSpace

        manifold = CartesianEucledeanSpace(vdot=vdot)

    from pystopt.callbacks import make_default_optimize_callback
    from pystopt.optimize.cg_utils import CGLogCallback
    from pystopt.optimize.riemannian import norm

    callback = make_default_optimize_callback(
        callback=callback,
        log_callback_factory=CGLogCallback,
        norm=lambda x: norm(manifold, x, x),
    )

    # }}}

    # {{{ optimize

    ss = _SteepestState(fun=fun, jac=jac, funjac=funjac, callback=callback)
    x, iteration, flag, message = _steepest(
        ss,
        linesearch,
        descent,
        manifold,
        x0,
        maxit=maxit,
        rtol=rtol,
        ftol=ftol,
        increase_iterations=increase_iterations,
        alpha_stall_iterations=alpha_stall_iterations,
        restart=restart,
    )

    # }}}

    import pystopt.optimize.riemannian as riem

    return CGResult(
        x=x,
        success=flag == _Flag.CONVERGED,
        status=flag.value,
        message=message,
        fun=ss.f_curr,
        jac=riem.norm(manifold, x, ss.g_curr),
        nfev=ss.nfev + linesearch._nfev,
        njev=ss.njev + linesearch._njev,
        nit=iteration,
    )


# }}}


# {{{ dataclasses


class _Flag(enum.IntEnum):
    CONVERGED = 0
    REACHED_MAXIT = 1
    USER_STOP = 2
    FUNC_NANINF = 10
    FUNC_INCREASE = 11
    ALPHA_STALL = 12
    JAC_NANINF = 13


@dataclass
class _SteepestState(Generic[ArrayOrContainerT]):
    # functions
    fun: Callable[[Any], float]
    jac: Callable[[Any], Any]
    funjac: Callable[[Any], tuple[float, Any]]
    callback: Callable[..., int] | None

    # loop
    nfev: int = field(default=0, init=False)
    njev: int = field(default=0, init=False)

    # call history
    f_curr: float | None = field(default=None, init=False)
    f_prev: float | None = field(default=None, init=False)
    g_curr: ArrayOrContainerT | None = field(default=None, init=False)
    g_prev: ArrayOrContainerT | None = field(default=None, init=False)

    def call_fun(self, x):
        self.nfev += 1
        self.f_prev = self.f_curr

        self.f_curr = self.fun(x)
        return self.f_curr

    def call_jac(self, x):
        self.njev += 1
        self.g_prev = self.g_curr

        self.g_curr = self.jac(x)
        return self.g_curr

    def call_funjac(self, x):
        self.nfev, self.njev = self.nfev + 1, self.njev + 1
        self.f_prev, self.g_prev = self.f_curr, self.g_curr

        if self.funjac is None:
            self.f_curr = self.fun(x)
            self.g_curr = self.jac(x)
        else:
            self.f_curr, self.g_curr = self.funjac(x)

        return self.f_curr, self.g_curr

    def call_callback(self, iteration, x, alpha, f, g, d):
        if self.callback is None:
            return 1

        from pystopt.optimize.cg_utils import CGCallbackInfo

        info = CGCallbackInfo(it=iteration, alpha=alpha, x=x, f=f, g=g, d=d)

        return self.callback(x, info=info)

    def pretty(self):
        from pystopt.tools import dc_stringify

        return dc_stringify(self)


# }}}


# {{{ steepest descent


def _steepest(
    ss,
    ls,
    ds,
    m,
    x0,
    *,
    maxit: int,
    rtol: float,
    ftol: float,
    increase_iterations: int,
    alpha_stall_iterations: int,
    restart: int,
):
    import pystopt.optimize.riemannian as riem
    from pystopt.optimize.descentdirection import descent_direction
    from pystopt.optimize.linesearch import line_search

    # {{{ init
    # first call
    x = x0
    f, g = ss.call_funjac(x)
    d = ds.force_update(x, g, -g)

    # update relative tolerances
    gnorm = riem.norm(m, x, g)
    rtol = rtol * gnorm
    ftol = ftol * abs(f)

    logger.info("relative tolerance: ftol %.12e gtol %.12e", ftol, rtol)

    # }}}

    # {{{ loop

    from pytools import ProcessTimer

    iteration = 0
    irestart = 0
    f_increase = 0

    flag = None
    message = None

    while iteration < maxit:
        # {{{ update solution

        with ProcessTimer() as p:
            alpha, xp = line_search(ls, m, x, f, g, d)
        logger.info("elapsed[alphas]: %s", p)

        with ProcessTimer() as p:
            user_stop = ss.call_callback(iteration, x, alpha, f, g, d)
        logger.info("elapsed[output]: %s", p)

        with ProcessTimer() as p:
            if xp is None:
                x = riem.retract(m, x, alpha * d)
            else:
                x = xp

            f, g = ss.call_funjac(x)
        logger.info("elapsed[funjac]: %s", p)

        gnorm = riem.norm(m, x, g)

        # }}}

        # {{{ update descent direction

        d = descent_direction(ds, m, x, g)
        d_dot_g = riem.metric(m, x, d, g)

        # NOTE: if dot(d, g) > 0, we lost orthogonality, so it needs to restart
        if irestart == restart or d_dot_g > 0:
            from warnings import warn

            warn(
                "Loss of orthogonality in descent direction; Restarting",
                UserWarning,
                stacklevel=2,
            )

            # force update to -g to maintain orthogonality
            d = ds.force_update(x, g, -g)
            irestart = 0

        # }}}

        # {{{ check stopping conditions

        iteration += 1
        irestart += 1

        # user
        if user_stop == 0:
            flag = _Flag.USER_STOP
            message = "user requested stopping condition"
            break

        # invalid function
        if not np.isfinite(f):
            flag = _Flag.FUNC_NANINF
            message = f"function values not finite (f = {f})"
            break

        # function increase
        if ss.f_prev < f:
            f_increase += 1
        else:
            f_increase = 0

        if f_increase >= increase_iterations:
            flag = _Flag.FUNC_INCREASE
            message = f"function increased over {f_increase} iterations"
            break

        # alpha stall
        if hasattr(ls, "alpha_min"):
            alpha = np.mean(list(ls.history)[:alpha_stall_iterations])
            if abs(alpha - ls.alpha_min) < 1.0e-8:
                flag = _Flag.ALPHA_STALL
                message = f"alpha stalled over {alpha_stall_iterations} iterations"
                break

        # gradient tolerance
        if not np.isfinite(gnorm):
            flag = _Flag.JAC_NANINF
            message = f"gradient values not finite (gnorm = {gnorm})"
            break

        if gnorm < rtol:
            flag = _Flag.CONVERGED
            message = f"reached desired rtol with gnorm {gnorm:.5e} < {rtol:.5e}"
            break

        # function tolerance
        if abs(f) < ftol or f == 0:
            flag = _Flag.CONVERGED
            message = f"reached desired ftol with |f| {f:.5e} < {ftol:.5e}"
            break

        # }}}

    # }}}

    if flag is None:
        flag = _Flag.REACHED_MAXIT
        message = f"reached maxit with gnorm {gnorm:.5e} > {rtol:.5e}"

    return x, iteration, flag, message


# }}}
