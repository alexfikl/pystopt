# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

"""
.. currentmodule:: pystopt.optimize

All line searches can be accessed programmatically using

.. autofunction:: get_line_search_from_name

Line searches must be subclasses of :class:`LineSearch` and implemented using
:func:`functools.singledispatch` on :func:`line_search`.

.. autoclass:: LineSearch
    :no-show-inheritance:

.. autofunction:: line_search

Concrete implementations include

.. autoclass:: FixedLineSearch
.. autoclass:: BacktrackingLineSearch
.. autoclass:: GrowingBacktrackingLineSearch
.. autoclass:: StabilizedBarzilaiBorweinLineSearch
.. autoclass:: ModifiedBarzilaiBorweinLineSearch

In the context of shape optimization, the following methods can be used to
get an estimate of the maximum allowable step size.

.. autofunction:: estimate_line_search_step_dogan
.. autofunction:: estimate_line_search_step_feppon
"""

import sys
from collections import deque
from collections.abc import Callable
from dataclasses import dataclass, field
from functools import partial, singledispatch
from typing import Any, Generic

import numpy as np

import pystopt.optimize.riemannian as riem
from pystopt import sym
from pystopt.tools import get_default_logger

logger = get_default_logger(__name__)

if getattr(sys, "_BUILDING_SPHINX_DOCS", False):
    from typing import TypeVar

    ArrayOrContainerT = TypeVar("ArrayOrContainerT")
else:
    from arraycontext import ArrayOrContainerT


# {{{ interface

_LINE_SEARCH_REGISTRY = None


def get_line_search_from_name(name: str, **kwargs: Any) -> "LineSearch":
    """
    :arg name: a string identifier for the line search method. The
        identifiers follow the naming convention ``"word-word-..."``, where
        each word is a lowercase part of the class name without ``"LineSearch"``.
        For example, for :class:`StabilizedBarzilaiBorweinLineSearch`, the
        identifier is ``"stabilized-barzilai-borwein"``.

    :arg kwargs: keyword arguments passed directly to the class constructor.
    """
    global _LINE_SEARCH_REGISTRY  # noqa: PLW0603
    if _LINE_SEARCH_REGISTRY is None:
        _LINE_SEARCH_REGISTRY = {
            "fixed": FixedLineSearch,
            "backtracking": BacktrackingLineSearch,
            "growing-backtracking": GrowingBacktrackingLineSearch,
            "stabilized-barzilai-borwein": StabilizedBarzilaiBorweinLineSearch,
            "modified-barzilai-borwein": ModifiedBarzilaiBorweinLineSearch,
        }

    if name not in _LINE_SEARCH_REGISTRY:
        raise ValueError(f"unknown line search: '{name}'")

    cls = _LINE_SEARCH_REGISTRY[name]

    if __debug__:
        from dataclasses import fields

        known_fields = {f.name for f in fields(cls)}
        given_fields = set(kwargs)
        unknown_fields = given_fields - known_fields
        if unknown_fields:
            raise AssertionError(f"unknown fields: {unknown_fields}")

    return cls(**kwargs)


@dataclass
class LineSearch(Generic[ArrayOrContainerT]):
    """Generic line search algorithm for steepest descent algorithms.

    .. autoattribute:: fun
    .. autoattribute:: jac
    """

    fun: Callable[[ArrayOrContainerT], float]
    """Functional that is being optimized."""
    jac: Callable[[ArrayOrContainerT], ArrayOrContainerT] | None = None
    """Gradient of :attr:`fun`."""

    _nfev: int = field(default=0, init=False, repr=False)
    _njev: int = field(default=0, init=False, repr=False)

    def call_fun(self, x: ArrayOrContainerT) -> float:
        """Wrapper around :attr:`fun` that also collects statistics, as necessary."""
        self._nfev += 1
        return self.fun(x)

    def call_jac(self, x: ArrayOrContainerT) -> ArrayOrContainerT:
        """Wrapper around :attr:`jac` that also collects statistics, as necessary."""
        self._njev += 1
        return self.jac(x)


@singledispatch
def line_search(
    ls: LineSearch,
    m: riem.RiemannianManifold,
    x: ArrayOrContainerT,
    f: float,
    g: ArrayOrContainerT,
    d: ArrayOrContainerT,
) -> tuple[float, ArrayOrContainerT]:
    r"""Approximation of the steepest descent step size.

    The update is expected to be

    .. math::

        x^{(k + 1)} = x^{(k)} + \alpha^{(k)} d^{(k)},

    where :math:`\alpha^{(k)}` is the step size approximated by this function.
    The descent direction *d* is expected to be a descent direction, i.e.
    :math:`\operatorname{dot}(d, g) < 0`. The manifold *m* is used to
    provide the specific meaning to all the operations above.

    :arg x: previous point at which to evaluate functions.
    :arg f: function value evaluated at *x*.
    :arg g: gradient value evaluated at *x*.
    :arg d: descent direction evaluated at *x*.

    :returns: an approximation of the step size and, optionally, the next iterate.
    """
    raise NotImplementedError(type(ls).__name__)


# }}}


# {{{ fixed step size


@dataclass
class FixedLineSearch(LineSearch):
    r"""Basic fixed step size without an actual line search.

    If not provided, the step size is approximated by the following

    .. math::

        \alpha = \begin{cases}
        \frac{\|x^{(0)}\|}{\|g^{(0)}\|}, & \quad \|x^{(0)}\| > 0, \\
        2 \frac{|f^{(0)}|}{\|g^{(0)}\|}, & \quad \text{otherwise}.
        \end{cases}

    .. autoattribute:: alpha
    """

    alpha: float | None = None
    """A fixed given step size to return during optimization."""


def _initial_step_cgdescent(
    ls: LineSearch,
    m: riem.RiemannianManifold,
    x: ArrayOrContainerT,
    f: float,
    g: ArrayOrContainerT,
) -> float:
    # NOTE: this is the initial guess implement in CG_DESCENT if none is given

    # FIXME: taking the norm of x does not make much sense here, since it's a
    # point, not a vector. CG_DESCENT uses Linfty norms here too.
    xnorm = riem.norm(m, x, x)
    gnorm = riem.norm(m, x, g)
    if abs(xnorm) < 1.0e-13:
        return 2.0 * abs(f) / gnorm
    else:
        return xnorm / gnorm


@line_search.register(FixedLineSearch)
def _line_search_fixed(
    ls: FixedLineSearch,
    m: riem.RiemannianManifold,
    x: ArrayOrContainerT,
    f: float,
    g: ArrayOrContainerT,
    d: ArrayOrContainerT,
) -> float:
    if ls.alpha is None:
        # NOTE: at first iteration -g == d, so it doesn't matter which one is passed
        ls.alpha = _initial_step_cgdescent(ls, m, x, f, g)

    return ls.alpha, None


# }}}


# {{{ backtracking line search


@dataclass
class BacktrackingLineSearch(LineSearch):
    r"""Implements the classic backtracking line search based on the
    Armijo-Goldstein condition.

    The algorithm decreases the step size by :attr:`factor` while

    .. math::

        f(\mathbf{x} + \alpha \mathbf{d}) - f(\mathbf{x}) >
            \alpha \beta (\mathbf{d} \cdot \mathbf{g}),

    where :math:`\mathbf{g}` is is the gradient and :math:`\mathbf{d}`
    is the descent direction at :math:`\mathbf{x}`. If an :math:`\alpha`
    is not found after :attr:`maxit` iterations, it just returns the last
    value.

    The initial guess for the upper bound is given by

    .. math::

        \tilde{\alpha}_{max} = \frac{1}{N_{history}} \sum_{k} \alpha^{(k)},

    i.e. an average over the last :attr:`nhistory` terms. As it
    is an average, it is guaranteed to never exceed :attr:`max_alpha`.

    .. autoattribute:: nhistory
    .. autoattribute:: maxit
    .. autoattribute:: max_alpha
    .. autoattribute:: min_alpha
    .. autoattribute:: beta
    .. autoattribute:: factor
    """

    # line search bounds

    maxit: int = 64
    """Maximum number of iterations of line search to be performed."""
    max_alpha: float = 1.0
    """Upper bound for the step size."""
    min_alpha: float = 1.0e-8
    """Lower bound for the step size."""

    # line search parameters
    # NOTE: default values from Nocedal and Wright

    beta: float = 1.0e-4
    """Parameter used in the line search Armijo condition."""
    factor: float = 0.85
    """Decrease in step size if the Armijo condition is not satisfied."""

    # history
    nhistory: int = 5
    """Number of step sizes to keep as history for the average."""
    history: deque[float] | None = field(default=None, init=False, repr=False)
    """Step size history."""

    def __post_init__(self):
        if self.nhistory > 0:
            self.history = deque(maxlen=self.nhistory)

    def init_alpha_from_direction(
        self, m: riem.RiemannianManifold, x: ArrayOrContainerT, d: ArrayOrContainerT
    ) -> float:
        return np.mean([self.max_alpha, *list(self.history)])


@dataclass
class GrowingBacktrackingLineSearch(BacktrackingLineSearch):
    r"""A growing adaptive version of :class:`BacktrackingLineSearch`.

    The adaptation is in terms of the initial upper bound used to start the
    line search. it uses an upper bound given by

    .. math::

        \tilde{\alpha}_{max} = \min \left(
                c \alpha_{history},
                \frac{\alpha_{max}}{\min(1, \|d\|)}
                \right)

    where :math:`\alpha_{history}` is the upper bound from
    :class:`BacktrackingLineSearch`.

    .. autoattribute:: factor_max_alpha
    """

    factor_max_alpha: float = 5.0
    """A factor multiplying :attr:`~BacktrackingLineSearch.max_alpha` to
    allow larger steps in some scenarios, but still control the magnitute.
    """

    def init_alpha_from_direction(
        self, m: riem.RiemannianManifold, x: ArrayOrContainerT, d: ArrayOrContainerT
    ) -> float:
        alpha = super().init_alpha_from_direction(m, x, d)
        return min(
            # NOTE: this allows some growth in alpha, but nothing crazy
            self.factor_max_alpha * self.max_alpha,
            alpha / min(1, riem.norm(m, x, d)),
        )


@line_search.register(BacktrackingLineSearch)
def _line_search_backtracking(
    ls: BacktrackingLineSearch,
    m: riem.RiemannianManifold,
    x: ArrayOrContainerT,
    f: float,
    g: ArrayOrContainerT,
    d: ArrayOrContainerT,
) -> float:
    d_dot_g = riem.metric(m, x, d, g)
    assert d_dot_g < 0, "direction is not a descent direction"

    alpha = ls.init_alpha_from_direction(m, x, d)
    for _ in range(ls.maxit):
        if alpha < ls.min_alpha:
            break

        xp = riem.retract(m, x, alpha * d)
        fa = ls.call_fun(xp)
        if (fa - f) < (alpha * ls.beta * d_dot_g):
            break

        alpha = max(ls.factor * alpha, ls.min_alpha)
        logger.debug("alpha = %.5e fa - f = %.5e d_dot_g %.5e", alpha, fa - f, d_dot_g)

    ls.history.append(alpha)
    return alpha, xp


# }}}


# {{{


@dataclass
class StabilizedBarzilaiBorweinLineSearch(LineSearch):
    """Implements the step size estimate from [Burdakov2019]_.

    .. [Burdakov2019] O. Burdakov, Y.-H. Dai, N. Huang,
        *Stabilized Barzilai-Borwein Method*,
        `ARXIV <https://arxiv.org/abs/1907.06409v3>`__.

    According to the discussion in Section 4 of [Burdakov2019], the stabilization
    is more important when using method ``BB1``. We use method ``BB2`` with
    stabilization by default to provide best results at no additional cost.

    .. autoattribute:: method
    .. autoattribute:: first_alpha
    .. autoattribute:: max_alpha

    .. autoattribute:: delta
    .. autoattribute:: delta_factor

    .. autoattribute:: nhistory
    .. autoattribute:: history

    .. autoattribute:: require_monotonicity
    """

    method: int = 2
    """Version of the Barzilai-Borwein estimate, see [Burdakov2019]_ for
    explicit formulae. This is an integer value in ``{±1, ±2}``,
    where negative values use the selected method but disable stabilization.
    """

    first_alpha: float | None = None
    """Guess for the initial step size. If not provided, it is computed from
    the available data.
    """
    max_alpha: float = 1.0
    """Maximum value of the step size, if the predicted step size grows out
    of control. This can happen as the optimum is reached.
    """

    delta: float | None = None
    """Parameter in stabilization formula. If not provided, it is estimated
    from the value of the first :attr:`nhistory` terms.
    """
    delta_factor: float = 0.5
    """Factor used in the estimate of :attr:`delta` based on the history
    (named :math:`c` in [Burdakov2019]_).
    """

    nhistory: int = 5
    """Number of items in the history of iterates that can be used to estimate
    :attr:`delta`.
    """
    history: list[float] = field(default_factory=list)
    """A list of the initial :attr:`nhistory` step sizes used to estimate
    :attr:`delta`.
    """

    require_monotonicity: bool = False
    """A flag to control if the method requires monotonicity in the function
    values. Since the step sizes are approximated, this does not always
    guarantee a decrease in the function values, so when *True*, we also
    perform a simple backtracking line search.
    """

    _x_prev: ArrayOrContainerT | None = field(default=None, init=False, repr=False)
    _d_prev: ArrayOrContainerT | None = field(default=None, init=False, repr=False)
    _alpha_prev: float | None = field(default=None, init=False, repr=False)

    def __post_init__(self):
        if abs(self.method) not in {1, 2}:
            raise ValueError(f"'method' must be one of ±1 or ±2, got {self.method}")


def _estimate_initial_alpha(
    ls: LineSearch,
    m: riem.RiemannianManifold,
    x: ArrayOrContainerT,
    f: float,
    g: ArrayOrContainerT,
) -> float:
    if ls.first_alpha is None:
        return _initial_step_cgdescent(ls, m, x, f, g)

    return ls.first_alpha


def _estimate_delta(ls: LineSearch, s: float) -> float:
    if ls.method < 0:
        return 1.0e6

    if ls.delta is not None:
        return ls.delta

    if len(ls.history) < ls.nhistory:
        ls.history.append(s)
        return 1.0e6

    ls.delta = ls.delta_factor * min(ls.history)
    return ls.delta


def _estimate_alpha_stab_bb(
    ls: LineSearch,
    m: riem.RiemannianManifold,
    x: ArrayOrContainerT,
    f: float,
    d: ArrayOrContainerT,
) -> float:
    # transport previous descent direction to current tangent space
    d_prev = riem.transport(m, ls._x_prev, x, ls._d_prev)

    # NOTE: the standard BB method does s = x_{k + 1} - x_k; as we're using
    # an update formula of the form
    #
    #   x_{k + 1} = R_{x_k} (alpha_k d_k) ~ x_k + alpha_k d_k
    #
    # we can use the approximation
    #
    #   s_k = x_{k + 1} - x_k ~ T_{x_k, x_{k + 1}}(alpha_k d_k)
    #
    # we can also use the transported d_k as an approximation of that difference
    # For flat manifolds, this will always be the same though.
    s = ls._alpha_prev * d_prev

    # NOTE: these are on the same point on the manifold, so they can be subtracted
    y = d_prev - d

    s_norm = riem.norm(m, x, s)
    y_norm = riem.norm(m, x, y)

    # estimate alpha using classic Barzilai-Borwein, 1988
    if abs(ls.method) == 1:
        alpha_bb = s_norm**2 / riem.metric(m, x, s, y)
    else:
        alpha_bb = riem.metric(m, x, s, y) / y_norm**2

    # account for negative values using fix from Dai-Al-Baali-Yang, 2015
    if alpha_bb < 0.0:
        alpha_bb = s_norm / y_norm

    # compute stabilization from Burdakov-Dai-Huang, 2019
    delta = _estimate_delta(ls, s_norm)
    alpha_stab = delta / y_norm

    return min(alpha_bb, alpha_stab)


@line_search.register(StabilizedBarzilaiBorweinLineSearch)
def _line_search_barzilai_borwein(
    ls: StabilizedBarzilaiBorweinLineSearch,
    m: riem.RiemannianManifold,
    x: ArrayOrContainerT,
    f: float,
    g: ArrayOrContainerT,
    d: ArrayOrContainerT,
) -> float:
    if ls._x_prev is None:
        alpha = _estimate_initial_alpha(ls, m, x, f, d)
    else:
        alpha = _estimate_alpha_stab_bb(ls, m, x, f, d)

    alpha_next = min(ls.max_alpha, alpha)
    xp = None
    if ls.require_monotonicity:
        factor = 0.5
        beta = 1.0e-4
        alpha_min = 1.0e-8

        d_dot_g = riem.metric(m, x, d, g)
        assert d_dot_g < 0 or d_dot_g == 0, (
            f"direction is not a descent direction: {d_dot_g}"
        )

        for i in range(16):
            if alpha_next < alpha_min or f == 0:
                break

            xp = riem.retract(m, x, alpha_next * d)
            fa = ls.call_fun(xp)
            if fa < f or (fa - f) < (alpha_next * beta * d_dot_g):
                break

            alpha_next = max(factor * alpha_next, alpha_min)
            logger.info(
                "[%02d] alpha = %.5e fa - f = %.5e d_dot_g %.5e",
                i,
                alpha_next,
                fa - f,
                d_dot_g,
            )

    ls._x_prev = x
    ls._d_prev = d
    ls._alpha_prev = alpha_next

    return alpha_next, xp


# }}}


# {{{


@dataclass
class ModifiedBarzilaiBorweinLineSearch(LineSearch):
    """Modified stabilized version of Barzilai-Borwein from [Wang2021]_.

    .. [Wang2021] TODO

    This method is very similar to :class:`StabilizedBarzilaiBorweinLineSearch`.
    The main difference is a simplification that removes the :math:`Delta`
    parameter and simply uses the previous step for an estimate.

    In general, this method is expected to work equally well.

    .. autoattribute:: max_alpha
    .. autoattribute:: first_alpha
    .. autoattribute:: factor
    """

    max_alpha: float = np.inf
    """Maximum value of the step size, if the predicted step size grows out
    of control. This can happen as the optimum is reached.
    """
    first_alpha: float | None = None
    """Step size at the first step. If not given, it is approximated from the
    available data.
    """
    factor: float = 1.0
    """A factor that can is multiplied into the estimated step size. Can be
    used to control the magnitude of the step.
    """

    _x_prev: ArrayOrContainerT | None = field(default=None, init=False, repr=False)
    _d_prev: ArrayOrContainerT | None = field(default=None, init=False, repr=False)


def _estimate_alpha_mod_bb(
    ls: LineSearch,
    m: riem.RiemannianManifold,
    x: ArrayOrContainerT,
    f: float,
    d: ArrayOrContainerT,
) -> float:
    # transport previous descent direction to current tangent space
    d_prev = riem.transport(m, ls._x_prev, x, ls._d_prev)

    delta_x = ls._alpha_prev * d_prev
    delta_d = d_prev - d
    delta_x_norm = riem.norm(m, x, delta_x)
    delta_d_norm = riem.norm(m, x, delta_d)

    # compute preliminary estimates
    alpha_1 = riem.metric(m, x, delta_x, delta_d) / delta_d_norm**2
    alpha_2 = delta_x_norm / delta_d_norm
    alpha_3 = delta_x_norm / riem.norm(m, x, d)

    # get smallest positive estimate for the step size
    if alpha_1 > 0:
        return min(alpha_1, alpha_3)
    else:
        return min(alpha_2, alpha_3)


@line_search.register(ModifiedBarzilaiBorweinLineSearch)
def _line_search_modified_barzilai_borwein(
    ls: ModifiedBarzilaiBorweinLineSearch,
    m: riem.RiemannianManifold,
    x: ArrayOrContainerT,
    f: float,
    g: ArrayOrContainerT,
    d: ArrayOrContainerT,
) -> float:
    if ls._x_prev is None:
        alpha = _estimate_initial_alpha(ls, m, x, f, d)
    else:
        alpha = _estimate_alpha_mod_bb(ls, m, x, f, d)

    ls._x_prev = x
    ls._d_prev = d
    ls._alpha_prev = alpha
    return min(ls.max_alpha, ls.factor * alpha), None


# }}}


# {{{ geometry-based line search step estimates


def estimate_line_search_step_dogan(
    ambient_dim: int,
    g: sym.ArithmeticExpression | np.ndarray,
    dim: int | None = None,
    dofdesc: sym.DOFDescriptorLike | None = None,
) -> sym.ArithmeticExpression:
    """An estimate for the CG step size based on Section 4.1 in [Dogan2007]_.

    .. [Dogan2007] G. Doǧan, P. Morin, R. H. Nochetto, M. Verani,
        *Discrete Gradient Flows for Shape Optimization and Applications*,
        Computer Methods in Applied Mechanics and Engineering, Vol. 196,
        pp. 3898--3914, 2007,
        `DOI <https://doi.org/10.1016/j.cma.2006.10.046>`__.

    :arg g: shape gradient or only its normal component.
    :returns: a symbolic expression for the estimate of the step size.
    """
    grad = partial(sym.surface_gradient, ambient_dim, dim=dim, dofdesc=dofdesc)

    if isinstance(g, np.ndarray):
        assert ambient_dim == g.size
        grad_g = [sym.cse(grad(g[i])) for i in range(ambient_dim)]
        norm_grad_g = sym.sqrt(sum(g @ g for g in grad_g))
    else:
        grad_g = sym.cse(grad(g))
        norm_grad_g = sym.sqrt(sym.NodeSum(grad_g @ grad_g))

    return 1 / norm_grad_g


def estimate_line_search_step_feppon(
    ambient_dim: int,
    g: sym.ArithmeticExpression | np.ndarray,
    dim: int | None = None,
    dofdesc: sym.DOFDescriptorLike | None = None,
) -> sym.ArithmeticExpression:
    """An estimate for the CG step size based on Section 5.3.2 in [Feppon2020]_.

    .. [Feppon2020] F. Feppon, G. Allaire, C. Dapogny,
        *Null Space Gradient Flows for Constrained Optimization With
        Applications to Shape Optimization*,
        ESAIM: Control, Optimisation and Calculus of Variations, Vol. 26, pp.
        90, 2020,
        `DOI <https://doi.org/10.1051/cocv/2020015>`__.

    :arg g: shape gradient or only its normal component.
    :returns: a symbolic expression for the estimate of the step size.
    """

    h_min = sym.h_min_from_volume(ambient_dim, dim=dim, dofdesc=dofdesc)

    if isinstance(g, np.ndarray):
        assert ambient_dim == g.size

        from pymbolic.primitives import Max

        norm_g = Max(sym.NodeMax(g))
    else:
        norm_g = sym.NodeMax(sym.abs(g))

    return h_min / norm_g


# }}}
