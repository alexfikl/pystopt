# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

"""
.. currentmodule:: pystopt.optimize

This module implements some helpers to handle Riemannian optimization in
shape spaces. For an introduction to these concepts see [RingWirth2012]_.

.. [RingWirth2012]
    W. Ring, B. Wirth,
    *Optimization Methods on Riemannian Manifolds and Their Application to
    Shape Space*, SIAM Journal on Optimization, Vol. 22, pp. 596--627, 2012,
    `DOI <https://doi.org/10.1137/11082885X>`__.

The abstract classes used to represent the manifold are

.. autoclass:: RiemannianManifold
.. autoclass:: ShapeSpace

They implement the following methods through a
:func:`functools.singledispatch` mechanism.

.. autofunction:: norm
.. autofunction:: metric
.. autofunction:: representation
.. autofunction:: retract
.. autofunction:: transport

Explicit implementations of the :class:`RiemannianManifold` class are given
below.

.. autoclass:: CartesianEucledeanSpace
.. autoclass:: L2ShapeSpace
.. autoclass:: WeightedL2ShapeSpace
"""

from collections.abc import Callable
from dataclasses import dataclass
from functools import singledispatch

import numpy as np

from arraycontext import Array, ArrayOrContainerT
from pytential import GeometryCollection
from pytools import memoize_method

from pystopt import bind, sym
from pystopt.tools import get_default_logger

logger = get_default_logger(__name__)


# {{{ interface


@dataclass(frozen=True)
class RiemannianManifold:
    """Generic Riemannian manifold.

    .. attribute:: ambient_dim
        :type: int

        Dimension of the ambient embedding space.

    .. attribute:: dim
        :type: int

        Dimension of the manifold.
    """


@singledispatch
def norm(m: RiemannianManifold, x: ArrayOrContainerT, u: ArrayOrContainerT) -> Array:
    """
    :arg x: a point on the manifold *m*;
    :arg u: a vector in the tangent space of *m* at *x*.

    :returns: the norm of the vector *u* using the :func:`metric` on *m*.
    """
    raise NotImplementedError(type(m).__name__)


@singledispatch
def metric(
    m: RiemannianManifold,
    x: ArrayOrContainerT,
    u: ArrayOrContainerT,
    v: ArrayOrContainerT,
) -> Array:
    """
    :arg x: a point on the manifold *m*;
    :arg u: a vector in the tangent space of *m* at *x*.
    :arg v: a vector in the tangent space of *m* at *x*.

    :returns: the value of :math:`g_x(u, v)`, where :math:`g_x` is the metric
        at a point :math:`x`. This essentially reduces to an inner product in
        most cases.
    """
    raise NotImplementedError(type(m).__name__)


@singledispatch
def representation(
    m: RiemannianManifold, x: ArrayOrContainerT, u: ArrayOrContainerT
) -> ArrayOrContainerT:
    """
    :arg x: a point on the manifold *m*;
    :arg u: a vector in the tangent space of *m* at *x*.

    :returns: a representation of the vector *u* in the manifold. This
        essentially corresponds to "raising the index" in tensor calculus.
    """
    raise NotImplementedError(type(m).__name__)


@singledispatch
def retract(
    m: RiemannianManifold, x: ArrayOrContainerT, u: ArrayOrContainerT
) -> ArrayOrContainerT:
    r"""Computes the retraction of the vector *u* from the tangent space at *x*.

    A retraction :math:`R_x` is a map from the tangent space :math:`T_x M`
    back to the manifold :math:`M`. This map must satisfy

    .. math::

        \begin{cases}
        R_x(0) = x, \\
        D R_x(0)[u] = u,
        \end{cases}

    so that the zero vector is just mapped to the point itself and the
    directional derivative in the direction of the vector *u* is just the
    vector itself (i.e. the directional derivative at :math:`0` is the
    identity map).

    In the context of optimization, the retraction is used to generalize the
    update formula

    .. math::

        x^{(k + 1)} = x^{(k)} + \alpha d^{(k)}

    to

    .. math::

        x^{(k + 1)} = R_{x^{(k)}}(\alpha d^{(k)}).

    :arg x: a point on the manifold *m*;
    :arg u: a vector in the tangent space of *m* at *x*.

    :returns: the retraction of the vector *u* from the tangent space at *x*.
    """
    raise NotImplementedError(type(m).__name__)


@singledispatch
def transport(
    m: RiemannianManifold,
    x: ArrayOrContainerT,
    y: ArrayOrContainerT,
    v: ArrayOrContainerT,
) -> ArrayOrContainerT:
    r"""Transports the vector *v* from the tangent space at *x* to *y*.

    A transport :math:`T_{x, y}` is a map from the tangent space :math:`T_x M`
    to the tangent space :math:`T_y M`. In general, this should be
    consistent with the chosen retraction in :func:`retract`.

    In the context of optimization, the transport map is used to more
    advanced conjugate gradient algorithms, where we have a gradient
    and a descent direction. For example, the standard expression

    .. math::

        d^{(k + 1)} = -g^{(k + 1)} + \beta d^{(k)}

    to

    .. math::

        d^{(k + 1)} = -g^{(k + 1)} + T_{x^{(k)}, x^{(k + 1)}}(d^{(k)}),

    since :math:`g^{(k + 1)}` and :math:`d^{(k)}` are in two different
    tangent spaces and cannot be meaningfully added for curved manifolds.

    :arg x: a point on the manifold *m*;
    :arg y: a point on the manifold *m*;
    :arg v: a vector in the tangent space of *m* at *x*.

    :returns: the vector *v* transported to the tangent space at *y*.
    """
    raise NotImplementedError(type(m).__name__)


# }}}


# {{{ shape space


@dataclass(frozen=True)
class ShapeSpace(RiemannianManifold):
    """
    .. autoattribute:: places
    .. autoattribute:: dofdesc
    """

    places: GeometryCollection
    """A geometry collection containing the surfaces."""
    dofdesc: sym.DOFDescriptor
    """A descriptor for the surface considered in the shape space."""

    @property
    def ambient_dim(self):
        return self.discr.ambient_dim

    @property
    def dim(self):
        return self.discr.dim

    @property
    @memoize_method
    def discr(self):
        return self.places.get_discretization(
            self.dofdesc.geometry, self.dofdesc.discr_stage
        )


@norm.register(ShapeSpace)
def _norm_shape_space(
    m: ShapeSpace, x: ArrayOrContainerT, u: ArrayOrContainerT
) -> Array:
    return np.sqrt(metric(m, x, u, u))


@retract.register(ShapeSpace)
def _retract_shape_space(
    m: ShapeSpace, x: ArrayOrContainerT, u: ArrayOrContainerT
) -> ArrayOrContainerT:
    return x + u


@transport.register(ShapeSpace)
def _transport_l2_shape_space(
    m: ShapeSpace, x: ArrayOrContainerT, y: ArrayOrContainerT, v: ArrayOrContainerT
) -> ArrayOrContainerT:
    if not m.transported:
        return v

    actx = x.array_context
    ambient_dim = x.ambient_dim

    # NOTE: the normals are likely already cached in each collection, so this
    # should be a fairly cheap operation once we get going
    nx = bind(x.places, sym.normal(ambient_dim).as_vector(), auto_where=m.dofdesc)(actx)
    ny = bind(y.places, sym.normal(ambient_dim).as_vector(), auto_where=m.dofdesc)(actx)
    return (v @ nx) * ny


# }}}


# {{{ euclidean space


@dataclass(frozen=True)
class CartesianEucledeanSpace(RiemannianManifold):
    """A flat Eucledean manifold in a Cartesian coordinate system."""

    vdot: Callable[[ArrayOrContainerT, ArrayOrContainerT], Array]


@norm.register(CartesianEucledeanSpace)
def _norm_eucledean(
    m: CartesianEucledeanSpace, x: ArrayOrContainerT, u: ArrayOrContainerT
) -> Array:
    return np.sqrt(metric(m, x, u, u))


@metric.register(CartesianEucledeanSpace)
def _metric_eucledean(
    m: CartesianEucledeanSpace,
    x: ArrayOrContainerT,
    u: ArrayOrContainerT,
    v: ArrayOrContainerT,
) -> Array:
    return m.vdot(u, v)


@representation.register(CartesianEucledeanSpace)
def _representation_eucledean(
    m: CartesianEucledeanSpace, x: ArrayOrContainerT, u: ArrayOrContainerT
) -> ArrayOrContainerT:
    return u


@retract.register(CartesianEucledeanSpace)
def _retract_eucledean(
    m: CartesianEucledeanSpace, x: ArrayOrContainerT, u: ArrayOrContainerT
) -> ArrayOrContainerT:
    return x + u


@transport.register(CartesianEucledeanSpace)
def _transport_euclidean(
    m: CartesianEucledeanSpace,
    x: ArrayOrContainerT,
    y: ArrayOrContainerT,
    v: ArrayOrContainerT,
) -> ArrayOrContainerT:
    return v


# }}}


# {{{ l2 shape space


@dataclass(frozen=True)
class L2ShapeSpace(ShapeSpace):
    r"""A flat shape space using the :math:`L^2` inner product.

    This space only works on :class:`~pystopt.simulation.GeometryState`\ s.
    The inner product on this space is given by

    .. math::

        g(u, v) = \int_{\mathbb{S}^d} u \cdot v \,\mathrm{d}S,

    where :math:`\mathbb{S}^d` is the :math:`d`-dimensional sphere. Note that
    this choice is not sufficient to describe a manifold (see [RingWirth2012]_).

    Besides that, it implements a non-trivial transport. Following [RingWirth2012]_,
    each point in the manifold is a closed surface and the tangent vectors
    are isomorphic to scalings of the normal vector field. Then, we set

    .. math::

        T_{x, y}[\mathbb{v}] = (\mathbb{v} \cdot \mathbf{n}_x) \mathbf{n}_y,

    where :math:`\mathbf{n}` are the normal vectors on the surfaces represented
    by :math:`x` and :math:`y`.

    .. autoattribute:: transported
    """

    transported: bool
    """If *False*, the tangent vector transport to the normal direction
    on the given shape is not performed.
    """


@metric.register(L2ShapeSpace)
def _metric_l2_shape_space(
    m: L2ShapeSpace, x: ArrayOrContainerT, u: ArrayOrContainerT, v: ArrayOrContainerT
) -> Array:
    assert type(u) is type(v), f"got {(type(u), type(v))}"

    from pystopt.simulation import GeometryState

    assert isinstance(u, GeometryState), f"got {type(u)}"
    assert u.array_context is v.array_context
    assert u.wrangler.is_spectral is v.wrangler.is_spectral

    if not u.wrangler.is_spectral:
        u = m.discr.to_spectral(u)
        v = m.discr.to_spectral(v)

    actx = u.array_context
    return actx.to_numpy(actx.np.vdot(u, v))


# }}}


# {{{ weighted L2 shape space


@dataclass(frozen=True)
class WeightedL2ShapeSpace(L2ShapeSpace):
    r"""A shape space using a weighted :math:`L^2` inner product.

    Unlike :class:`L2ShapeSpace`, this manifold is no longer flat
    (see [RingWirth2012]_). The inner product is given by

    .. math::

        g(u, v) = \int_{\mathbb{S}^d} (1 + \alpha \kappa) u \cdot v \,\mathrm{d}S,

    where :math:`\alpha` is a real coefficient and :math:`\kappa` is the
    sum of the principal curvatures. Setting :math:`\alpha = 0` recovers
    the degenerate case of :class:`L2ShapeSpace`.

    .. autoattribute:: alpha
    """

    alpha: float
    """Weight used for the curvature-based term."""


@metric.register(WeightedL2ShapeSpace)
def _metric_weighted_l2_shape(
    m: WeightedL2ShapeSpace,
    x: ArrayOrContainerT,
    u: ArrayOrContainerT,
    v: ArrayOrContainerT,
) -> ArrayOrContainerT:
    kappa = bind(
        x.places,
        sym.summed_curvature(m.ambient_dim, dim=m.dim, dofdesc=m.dofdesc),
        auto_where=m.dofdesc,
    )(x.array_context)
    if x.wrangler.is_spectral:
        u = m.discr.from_spectral(u)
        v = m.discr.from_spectral(v)

    operand = (1 + m.alpha * sym.var("kappa") ** 2) * sym.var("uv")
    return bind(
        x.places,
        sym.sintegral(operand, m.ambient_dim, m.dim, dofdesc=m.dofdesc),
        auto_where=m.dofdesc,
    )(x.array_context, uv=u @ v, kappa=kappa)


@representation.register(WeightedL2ShapeSpace)
def _representation_weighted_l2_shape(
    m: WeightedL2ShapeSpace, x: ArrayOrContainerT, u: ArrayOrContainerT
) -> ArrayOrContainerT:
    kappa = bind(
        x.places,
        sym.summed_curvature(m.ambient_dim, dim=m.dim, dofdesc=m.dofdesc),
        auto_where=m.dofdesc,
    )(x.array_context)
    return u / (1 + m.alpha * kappa**2)


# }}}
