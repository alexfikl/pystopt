# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

from collections.abc import Callable
from dataclasses import dataclass, field
from typing import Any

import numpy as np

from arraycontext import ArrayOrContainerT

from pystopt.callbacks import HistoryCallback, LogCallback
from pystopt.tools import get_default_logger

logger = get_default_logger(__name__)


# {{{ callbacks


@dataclass(frozen=True)
class GMRESLogCallback(LogCallback):
    ncalls: int = field(default=0, init=False, repr=False)

    def __call__(self, *args: Any, **kwargs: Any) -> int:
        (residual,) = args

        if residual is None:
            self.log(f"IT {self.ncalls:<8d}")
        else:
            if self.norm is not None:
                rnorm = self.norm(residual)
            else:
                rnorm = float("nan")

            from pystopt.tools import c

            self.log(f"IT {self.ncalls:<8d} {c.White}{rnorm:.12e}{c.Normal}")

        object.__setattr__(self, "ncalls", self.ncalls + 1)
        return 1


@dataclass(frozen=True)
class GMRESHistoryCallback(HistoryCallback):
    r: list[float] = field(default_factory=list, repr=False)

    def __call__(self, *args: Any, **kwargs: Any) -> int:
        (residual,) = args
        self.r.append(np.linalg.norm(residual))

        return 1


# }}}


def gmres(
    op,
    rhs: ArrayOrContainerT,
    x0: ArrayOrContainerT | None = None,
    *,
    inner_product=None,
    callback: Callable | bool | None = False,
    restart: int | None = None,
    rtol: float | None = None,
    maxiter: int | None = None,
    hard_failure: bool | None = None,
    stall_iterations: int | None = None,
    no_progress_factor: float | None = None,
    require_monotonicity: bool = True,
) -> ArrayOrContainerT:
    """Wrapper around :func:`pytential.solve.gmres`.

    :arg op: a linear operator following the interface of
        :class:`scipy.sparse.linalg.LinearOperator`.
    :arg rhs: right-hand side vector.
    :arg x0: initial guess.
    :arg inner_product: an inner product with an interface compatible with
        :func:`numpy.vdot` that returns a scalar.
    :arg callback: a callable that is called at each iteration with the
        residual.
    :arg restart: maximum number of iterations after which the algorithm is
        restarted, but it can be restarted sooner too.
    :arg rtol: relative tolerance of the residual.
    :arg maxiter: maximum number of iterations.
    :arg hard_failure: if *True*, raises an exception when the algorithm
        fails to converge for whatever reason.
    :arg stall_iterations: number of iterations for which the residual
        decrease is allowed to stall. The reasonable decrease is prescribed by
        *no_progress_factor*.
    :arg no_progress_factor: minimum relative residual decrease.
    :arg require_monotonicity: if *True*, an error is raised if the
        residual increases.

    :returns: a :class:`pytential.solve.GMRESResult`.
    """
    # {{{ setup

    if inner_product is None:
        from pystopt.optimize.cg_utils import get_vdot_for_container

        inner_product = get_vdot_for_container(rhs, actx=op.array_context)

    if callable(callback):
        pass
    elif isinstance(callback, bool):
        if callback:
            callback = GMRESLogCallback(
                log=logger.info, norm=lambda x: abs(inner_product(x, x)) ** 0.5
            )
    else:
        raise TypeError(f"unsupported callback type: {type(callback)}")

    # }}}

    from pytential.linalg.gmres import gmres as _gmres

    return _gmres(
        op,
        rhs,
        restart=restart,
        tol=rtol,
        x0=x0,
        inner_product=inner_product,
        maxiter=maxiter,
        hard_failure=hard_failure,
        no_progress_factor=no_progress_factor,
        stall_iterations=stall_iterations,
        callback=callback,
        require_monotonicity=require_monotonicity,
    )
