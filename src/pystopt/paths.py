# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

"""
.. class:: PathLike

    Type alias to be for file names.

.. autoclass:: FileParts

.. autofunction:: get_dirname
.. autofunction:: make_dirname
.. autofunction:: get_filename
.. autofunction:: generate_filename_series
"""

import io
import os
import pathlib
from collections.abc import Iterator
from dataclasses import dataclass, replace
from typing import Union

from pytools import memoize_method

# https://github.com/python/mypy/issues/5667
PathLike = Union[str, bytes, "os.PathLike[str]", io.IOBase]


@dataclass(frozen=True)
class FileParts:
    """Constructs filenames of the form::

        [cwd]/[stem]_[suffix]_[index].[ext]

    .. automethod:: with_suffix
    .. automethod:: aspath
    """

    stem: str
    ext: str | None
    suffix: str | None
    index: str | None
    cwd: pathlib.Path | None

    def prepend(self, stem: str) -> "FileParts":
        from pystopt.tools import slugify

        return replace(self, stem=slugify(f"{self.stem}_{stem}"))

    def with_suffix(self, suffix: str) -> os.PathLike:
        """
        :returns: a :class:`pathlib.Path` with the given suffix.
        """
        from pystopt.tools import slugify

        return replace(self, suffix=slugify(suffix)).aspath()

    def __call__(self, suffix: str) -> os.PathLike:
        return self.with_suffix(suffix)

    @memoize_method
    def aspath(self) -> os.PathLike:
        stem = "_".join(p for p in [self.stem, self.suffix, self.index] if p)

        path = pathlib.Path(stem)
        if self.ext is not None:
            path = path.with_suffix(self.ext)

        if self.cwd is not None:
            path = self.cwd / path

        return path

    def __str__(self) -> str:
        return str(self.aspath())

    def __fspath__(self) -> str:
        return str(self)


def relative_to(filename: PathLike, cwd: PathLike | None = None) -> pathlib.Path:
    filename = pathlib.Path(filename).resolve()
    if cwd is None:
        cwd = pathlib.Path.cwd()

    try:
        return filename.relative_to(cwd)
    except ValueError:
        # NOTE: can happen if the files aren't relative to each other at all
        return filename


# {{{ directory name generator


def get_dirname(
    stem: str, *, today: bool | str = True, cwd: os.PathLike | None = None
) -> pathlib.Path:
    """Create canonical directory names of the form::

        [cwd]/[date]_[stem]

    The resulting directory name is then slugified to ensure only ascii
    alphanumeric characters are present.

    :arg today: if *True*, use the current date ``%Y-%m-%d`` as the prefix.
    :returns: absolute path to the directory.
    """
    if cwd is None:
        cwd = pathlib.Path(
            os.environ.get("PYSTOPT_DEFAULT_OUTPUT_PATH", pathlib.Path.cwd())
        ).absolute()
    else:
        cwd = pathlib.Path(cwd).absolute()

    if os.path.exists(stem):
        stem = pathlib.Path(stem).stem

    if today:
        from datetime import datetime

        if isinstance(today, bool):
            s_time = datetime.now().strftime("%Y_%m_%d")
        else:
            s_time = datetime.now().strftime(today)
    else:
        s_time = ""

    from pystopt.tools import slugify

    return cwd / "_".join(slugify(p) for p in [s_time, stem] if p)


def make_dirname(
    stem: str, *, today: bool | str = True, cwd: os.PathLike | None = None
) -> pathlib.Path:
    """Same as :func:`get_dirname`, but also ensures the directory exists."""

    dirname = get_dirname(stem, today=today, cwd=cwd)
    if not dirname.exists():
        dirname.mkdir()

    return dirname


# }}}


# {{{ filename generator


def make_file_parts(
    stem: str, suffix: str, index: str | None, ext: str, cwd: str | None
) -> FileParts:
    if os.path.exists(stem):
        stem = pathlib.Path(stem).stem

    if not cwd:
        cwd = os.getcwd()
    cwd = pathlib.Path(cwd).absolute()

    if ext is not None:
        ext = f".{ext}" if ext[0] != "." else ext

    from pystopt.tools import slugify

    return FileParts(
        stem=slugify(stem),
        suffix=slugify(suffix),
        index=slugify(index),
        ext=ext,
        cwd=cwd,
    )


def get_filename(
    stem: str,
    *,
    suffix: str | None = None,
    cwd: os.PathLike | None = None,
    ext: str | None = None,
) -> FileParts:
    """Create a canonically named file of the form::

        [dirname]/[stem]_[suffix].[ext]

    The resulting file name is then slugified to ensure only ascii
    alphanumeric characters are present.
    """
    return make_file_parts(stem=stem, suffix=suffix, index=None, ext=ext, cwd=cwd)


def generate_filename_series(
    stem: str,
    *,
    suffix: str | None = None,
    cwd: PathLike | None = None,
    ext: str | None = None,
    start: int = 0,
    idxfmt: str | None = None,
) -> Iterator[FileParts]:
    """Create a set of canonically named file names of the form::

        [cwd]/[stem]_[suffix]_[NNNNN].[ext]

    This is a generator that can be used as

    .. code:: python

        filenames = generate_filename_series("series", ext=".out")
        for i in range(10):
            filename = next(filenames)
            with open(filename.aspath(), "w") as f:
                f.write(str(i))

    :param start: starting index for the series.
    :param idxfmt: format string for the series index, by default set to
        ``{:09d}``.

    :returns: a generator for filenames.
    """
    if idxfmt is None:
        idxfmt = "{:09d}"

    parts = make_file_parts(stem, suffix=suffix, index=None, ext=ext, cwd=cwd)
    n = start

    while True:
        yield replace(parts, index=idxfmt.format(n))

        n += 1


def generate_dirname_series(
    stem: str,
    *,
    cwd: PathLike | None = None,
    create: bool = False,
    start: int = 0,
    idxfmt: str | None = None,
) -> Iterator[pathlib.Path]:
    if idxfmt is None:
        idxfmt = "{:09d}"

    n = start
    while True:
        dirname = get_dirname(f"{stem}_{idxfmt.format(n)}", today=False, cwd=cwd)
        if create and not dirname.exists():
            dirname.mkdir()

        yield dirname
        n += 1


# }}}
