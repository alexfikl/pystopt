# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

import numpy as np
import sympy as sp

from pytools.obj_array import make_obj_array

from pystopt.tools import get_default_logger

logger = get_default_logger(__name__)


# {{{ helpers


def is_sympy_constant(x):
    import pymbolic.primitives as prim

    return prim.is_constant(x) or isinstance(x, sp.Number | sp.Symbol | sp.Indexed)


def is_pymbolic_constant(x):
    import pymbolic.primitives as prim

    return prim.is_constant(x) or isinstance(x, prim.Variable | prim.Subscript)


def is_variable(v):
    if is_sympy_constant(v):
        return True
    elif isinstance(v, sp.Add | sp.Mul):
        return all(is_variable(vi) for vi in v.args)

    return False


def to_sympy_variable(v, **kwargs):
    if is_variable(v):
        return v

    from pymbolic.primitives import Product, Subscript, Sum, Variable

    from pystopt.symbolic.primitives import Ones

    if isinstance(v, str):
        return sp.Symbol(v, **kwargs)
    elif isinstance(v, Variable):
        return sp.Symbol(v.name, **kwargs)
    elif isinstance(v, np.ndarray) and v.dtype.char == "O":
        return make_obj_array([to_sympy_variable(vi, **kwargs) for vi in v])
    elif isinstance(v, Subscript):
        assert isinstance(v.aggregate, Variable)
        return sp.Indexed(v.aggregate.name, v.index)
    elif isinstance(v, Sum):
        return sum(to_sympy_variable(vi) for vi in v.children)
    elif isinstance(v, Product):
        from pytools import product

        return product(to_sympy_variable(vi) for vi in v.children)
    elif isinstance(v, Ones):
        return 1.0
    else:
        raise TypeError(f"unsupported variable type: '{type(v).__name__}'")


def to_pytential(ambient_dim, expr, dofdesc=None, *, simplify=False):
    if simplify:
        if isinstance(expr, np.ndarray):
            expr = make_obj_array([sp.simplify(comp) for comp in expr])
        else:
            expr = sp.simplify(expr)

    from pystopt.symbolic.mappers import SympyToPytentialMapper

    expr = SympyToPytentialMapper(ambient_dim)(expr)

    from pystopt import sym

    if isinstance(expr, np.ndarray):
        expr = make_obj_array([
            (comp * sym.Ones()) if is_pymbolic_constant(comp) else comp for comp in expr
        ])
    elif isinstance(expr, list | tuple):
        expr = type(expr)([
            (comp * sym.Ones()) if is_pymbolic_constant(comp) else comp for comp in expr
        ])
    else:
        expr = (expr * sym.Ones()) if is_pymbolic_constant(expr) else expr

    if dofdesc is None:
        return expr

    from pystopt.symbolic.mappers import DOFDescriptorReplaceMapper

    return DOFDescriptorReplaceMapper(dofdesc)(expr)


def make_sym_vector(name, ambient_dim):
    return make_obj_array([sp.Indexed(name, i) for i in range(ambient_dim)])


def gradient(ambient_dim, f):
    x = cart_coords(ambient_dim)
    return make_obj_array([sp.diff(f, x[i]) for i in range(ambient_dim)])


def divergence(u):
    ambient_dim = len(u)
    x = cart_coords(ambient_dim)

    return sum(sp.diff(u[i], x[i]) for i in range(ambient_dim))


def hessian(ambient_dim, f):
    x = cart_coords(ambient_dim)
    hessf = np.empty((ambient_dim, ambient_dim), dtype=object)

    from itertools import product

    for i, j in product(range(ambient_dim), repeat=2):
        hessf[i, j] = sp.diff(f, x[i], x[j])

    return hessf


def vector_laplacian(u):
    ambient_dim = len(u)
    x = cart_coords(ambient_dim)

    return make_obj_array([
        sum(sp.diff(u[i], (x[j], 2)) for j in range(ambient_dim))
        for i in range(ambient_dim)
    ])


# }}}


# {{{ coordinates


def nodes(ambient_dim):
    return cart_coords(ambient_dim)


def cart_coords(ambient_dim):
    from pystopt.symbolic.mappers import SYMPY_COORD_NAME

    return make_obj_array([sp.Indexed(SYMPY_COORD_NAME, i) for i in range(ambient_dim)])


def polar_coords():
    return sp.Symbol("r", nonnegative=True), sp.Symbol("theta")


def sph_from_cart_coords(ambient_dim):
    x = cart_coords(ambient_dim)
    r = sp.sqrt(sum(x**2))

    if ambient_dim == 2:
        theta = sp.atan2(x[1], x[0])
        return make_obj_array([r, theta])
    elif ambient_dim == 3:
        # https://en.wikipedia.org/wiki/Spherical_coordinate_system
        theta = sp.acos(x[2] / r)
        phi = sp.atan2(x[1], x[0])
        return make_obj_array([r, theta, phi])
    else:
        raise ValueError(f"unsupported dimension: {ambient_dim}")


def cylindrical_to_cartesian_rotation():
    _, theta = polar_coords()

    return np.array(
        [
            [sp.cos(theta), sp.sin(theta), 0],
            [-sp.sin(theta), sp.cos(theta), 0],
            [0, 0, 1],
        ],
        dtype=object,
    ).T


def sph_to_cartesian_rotation():
    _, theta, phi = sph_from_cart_coords(ambient_dim=3)

    return np.array(
        [
            [
                sp.sin(theta) * sp.cos(phi),
                sp.sin(theta) * sp.sin(phi),
                +sp.cos(theta),
            ],
            [
                sp.cos(theta) * sp.cos(phi),
                sp.cos(theta) * sp.sin(phi),
                -sp.sin(theta),
            ],
            [-sp.sin(phi), sp.cos(phi), 0],
        ],
        dtype=object,
    ).T


def normal(ambient_dim):
    from pystopt.symbolic.mappers import SYMPY_NORMAL_NAME

    return make_sym_vector(SYMPY_NORMAL_NAME, ambient_dim)


def tangential_onb(ambient_dim, dim=None):
    if dim is None:
        dim = ambient_dim - 1

    from pystopt.symbolic.mappers import SYMPY_TANGENT_NAME

    return tuple(
        make_sym_vector(SYMPY_TANGENT_NAME[i], ambient_dim) for i in range(dim)
    )


# }}}


# {{{ fluids


def _directional_stress(p, u, direction):
    ambient_dim = len(u)
    x = cart_coords(ambient_dim)

    return make_obj_array([
        -p * direction[i]
        + sum(
            (sp.diff(u[i], x[j]) + sp.diff(u[j], x[i])) * direction[j]
            for j in range(ambient_dim)
        )
        for i in range(ambient_dim)
    ])


def _directional_vector_gradient(u, direction):
    ambient_dim = len(u)
    x = cart_coords(ambient_dim)

    return make_obj_array([
        sum(sp.diff(u[i], x[j]) * direction[j] for j in range(ambient_dim))
        for i in range(ambient_dim)
    ])


def traction(p, u, n=None):
    if n is None:
        n = normal(len(u))

    return _directional_stress(p, u, n)


def tangent_traction(p, u, t=None):
    ambient_dim = len(u)

    if t is None:
        return tuple(
            _directional_stress(p, u, ti) for ti in tangential_onb(ambient_dim)
        )
    elif isinstance(t, int):
        return (_directional_stress(p, u, tangential_onb(ambient_dim)[t]),)
    else:
        return (_directional_stress(p, u, t),)


def normal_vector_gradient(u, n=None):
    if n is None:
        n = normal(len(u))

    return _directional_vector_gradient(u, n)


def tangent_vector_gradient(u, t=None):
    ambient_dim = len(u)

    if t is None:
        return tuple(
            _directional_vector_gradient(u, ti) for ti in tangential_onb(ambient_dim)
        )
    elif isinstance(t, int):
        return (_directional_vector_gradient(u, tangential_onb(ambient_dim)[t]),)
    else:
        return (_directional_vector_gradient(u, t),)


def normal_vector_hessian(u, n=None):
    ambient_dim = len(u)
    x = cart_coords(ambient_dim)
    if n is None:
        n = normal(ambient_dim)

    from itertools import product

    return make_obj_array([
        sum(
            sp.diff(u[i], x[j], x[k]) * n[j] * n[k]
            for j, k in product(range(ambient_dim), repeat=2)
        )
        for i in range(ambient_dim)
    ])


# }}}
