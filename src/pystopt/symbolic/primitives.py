# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

"""
All the functions from :mod:`pytential.symbolic.primitives` are exported
from here. Additionally, the following primitives are implemented.

.. class:: ExpressionNode

    See :class:`pymbolic.primitives.ExpressionNode`.

.. class:: ArithmeticExpression

    See :data:`pymbolic.ArithmeticExpression`.

.. class:: DOFDescriptorLike

    See :class:`pytential.symbolic.dof_desc.DOFDescriptorLike`.

.. autofunction:: first_fundamental_form
.. autofunction:: inverse_first_fundamental_form
.. autofunction:: second_fundamental_form
.. autofunction:: shape_operator
.. autofunction:: extrinsic_shape_operator

.. autofunction:: summed_curvature
.. autofunction:: mean_curvature
.. autofunction:: gaussian_curvature
.. autofunction:: squared_curvature

.. autofunction:: principal_curvature
.. autofunction:: principal_directions
.. autofunction:: extrinsic_principal_directions

.. autofunction:: surface_gradient
.. autofunction:: surface_divergence
.. autofunction:: surface_laplace_beltrami

.. autofunction:: surface_volume
.. autofunction:: surface_centroid

.. autofunction:: h_max_from_volume
.. autofunction:: h_min_from_volume
.. autofunction:: h_min

.. autofunction:: gaussian_bump_function
.. autofunction:: spherical_bump_function

.. autofunction:: norm
.. autofunction:: relative_norm

.. autofunction:: n_dot
"""

from __future__ import annotations

from collections.abc import Hashable
from functools import partial
from itertools import product
from typing import Any
from warnings import warn

import numpy as np

from pymbolic.geometric_algebra import MultiVector  # noqa: F401
from pymbolic.primitives import (  # noqa: F401
    Comparison,
    If,
    Variable,
    expr_dataclass,
)
from pymbolic.typing import ArithmeticExpression
from pytential import GeometryCollection
from pytential.symbolic.primitives import (  # noqa: F401
    GRANULARITY_CENTER,
    GRANULARITY_ELEMENT,
    GRANULARITY_NODE,
    QBX_SOURCE_QUAD_STAGE2,
    QBX_SOURCE_STAGE1,
    QBX_SOURCE_STAGE2,
    D,
    DiscretizationProperty,
    DOFDescriptor,
    DOFDescriptorLike,
    ElementwiseMax,
    ElementwiseMin,
    ElementwiseSum,
    IntG,
    NodeCoordinateComponent,
    NodeMax,
    NodeMin,
    NodeSum,
    NumReferenceDerivative,
    Ones,
    Operand,
    QWeight,
    S,
    SingleScalarOperandExpressionWithWhere,
    _mapping_max_stretch_factor,
    abs,  # noqa: A004
    arccos,
    arccosh,
    arcsin,
    arcsinh,
    arctan,
    arctan2,
    arctanh,
    area_element,
    as_dofdesc,
    conj,
    cos,
    cosh,
    cse,
    cse_scope,
    exp,
    expansion_centers,
    expansion_radii,
    for_each_expression,
    grad,
    h_max,
    imag,
    int_g_vec,
    log,
    make_sym_vector,
    nodes,
    normal,
    num_reference_derivative,
    real,
    sin,
    sinh,
    sqrt,
    tan,
    tanh,
    var,
    weights_and_area_elements,
)
from pytools.obj_array import make_obj_array


def prepare_dofdesc(
    places: GeometryCollection, dofdesc: DOFDescriptorLike
) -> DOFDescriptor:
    return places.auto_source if dofdesc is None else as_dofdesc(dofdesc)


def pretty(expr):
    from pystopt.symbolic.mappers import PrettyStringifyMapper

    stringify_mapper = PrettyStringifyMapper()

    from pymbolic.mapper.stringifier import PREC_NONE

    result = stringify_mapper(expr, PREC_NONE)

    cse_strs = stringify_mapper.get_cse_strings()
    if cse_strs:
        cse_str = "\n".join(cse_strs)
        result = f"{cse_str}\n{'=' * 75}\n{result}"

    return result


@expr_dataclass()
class GroupwiseSum(SingleScalarOperandExpressionWithWhere):
    pass


# {{{ spectral


@expr_dataclass(init=False)
class SpectralNodeCoordinateComponent(NodeCoordinateComponent):
    pass


@expr_dataclass(init=False)
class SpectralNumReferenceDerivative(NumReferenceDerivative):
    pass


@for_each_expression
def spectral_num_reference_derivative(
    expr: ArithmeticExpression,
    ref_axes: tuple[tuple[int, int], ...] | int,
    dofdesc: DOFDescriptorLike | None,
) -> NumReferenceDerivative:
    """Take a derivative of *expr* with respect to the the element reference
    coordinates.

    See :class:`~pystopt.symbolic.primitives.SpectralNumReferenceDerivative`.
    """

    if isinstance(ref_axes, int):
        ref_axes = ((ref_axes, 1),)

    return SpectralNumReferenceDerivative(ref_axes, expr, as_dofdesc(dofdesc))


@expr_dataclass()
class SpectralProjection(DiscretizationProperty):
    operand: Operand
    """Expression to project to or from the spectral coefficients."""
    forward: bool
    """Direction of the projection."""

    def __post_init__(self) -> None:
        if not isinstance(self.dofdesc, DOFDescriptor):
            warn(
                "Passing a 'dofdesc' that is not a 'DOFDescriptor' to "
                f"{type(self).__name__!r} is deprecated and will stop working "
                "in 2025. Use 'as_dofdesc' to convert the descriptor.",
                DeprecationWarning,
                stacklevel=2,
            )

            object.__setattr__(self, "dofdesc", as_dofdesc(self.dofdesc))


@for_each_expression
def to_spectral(operand: Any, dofdesc: DOFDescriptorLike | None = None):
    return SpectralProjection(
        operand=operand, forward=True, dofdesc=as_dofdesc(dofdesc)
    )


@for_each_expression
def from_spectral(operand: Any, dofdesc: DOFDescriptorLike | None = None):
    return SpectralProjection(
        operand=operand, forward=False, dofdesc=as_dofdesc(dofdesc)
    )


def snodes(ambient_dim: int, dofdesc: DOFDescriptorLike | None = None) -> np.ndarray:
    return make_obj_array([
        SpectralNodeCoordinateComponent(i, as_dofdesc(dofdesc))
        for i in range(ambient_dim)
    ])


@expr_dataclass()
class _SpectralConjugateScaledAreaElement(DiscretizationProperty):
    mapper_method = "map_spectral_conjugate_scaled_area_element"


@for_each_expression
def sintegral(
    operand: ArithmeticExpression,
    ambient_dim: int,
    dim: int,
    *,
    dofdesc: DOFDescriptorLike | None = None,
    groupwise: bool = False,
) -> ArithmeticExpression:
    dofdesc = as_dofdesc(dofdesc)

    reduction = partial(GroupwiseSum, dofdesc=dofdesc) if groupwise else NodeSum

    return real(
        reduction(
            to_spectral(operand, dofdesc=dofdesc)
            * _SpectralConjugateScaledAreaElement(dofdesc),
        )
    )


# }}}


# {{{ filtering


@expr_dataclass()
class FilterOperator(DiscretizationProperty):
    operand: Operand
    """Expression to filter."""
    context: dict[str, Hashable]
    """Parameters for the filtering function."""


@for_each_expression
def filter_expression(
    operand: Any,
    context: dict[str, Hashable] | None = None,
    dofdesc: DOFDescriptorLike | None = None,
) -> FilterOperator:
    if context is None:
        context = {}

    return FilterOperator(operand, context, as_dofdesc(dofdesc))


# }}}


# {{{ fundamental forms


def parametrization_derivative_matrix(ambient_dim, dim, dofdesc=None):
    """
    :returns: an :class:`~numpy.ndarray` representing the derivative of the
        reference-to-global parametrization.
    """
    dofdesc = as_dofdesc(dofdesc)

    xlm = make_obj_array([
        SpectralNodeCoordinateComponent(i, dofdesc) for i in range(ambient_dim)
    ])
    jac = np.zeros((ambient_dim, dim), object)
    for i in range(ambient_dim):
        func_component = xlm[i]
        for j in range(dim):
            jac[i, j] = SpectralNumReferenceDerivative(
                ((j, 1),), func_component, dofdesc
            )

    return cse(jac, "pd_matrix", cse_scope.DISCRETIZATION)


def _pder_tangential_onb(ambient_dim, dim=None, dofdesc=None):
    """
    :returns: a matrix of shape ``(ambient_dim, dim)`` with orthonormal columns
        spanning the tangential space of the surface of *dofdesc*.
    """
    dofdesc = as_dofdesc(dofdesc)

    if dim is None:
        dim = ambient_dim - 1

    if ambient_dim == 2 and dim == 1:
        n = normal(ambient_dim, dim=dim, dofdesc=dofdesc).as_vector()
        return make_obj_array([n[1], -n[0]]).reshape(-1, 1)

    pd_mat = parametrization_derivative_matrix(ambient_dim, dim=dim, dofdesc=dofdesc)

    # {{{ Gram-Schmidt

    orth_pd_mat = np.zeros_like(pd_mat)
    for k in range(pd_mat.shape[1]):
        avec = pd_mat[:, k]
        q = avec
        for j in range(k):
            q = q - (avec @ orth_pd_mat[:, j]) * orth_pd_mat[:, j]
        q = cse(q, f"q{k}")

        orth_pd_mat[:, k] = cse(
            q / sqrt(sum(q**2)), f"orth_pd_vec{k}_", cse_scope.DISCRETIZATION
        )

    # }}}

    return orth_pd_mat


def tangential_onb(
    ambient_dim: int,
    *,
    dim: int | None = None,
    dofdesc: DOFDescriptorLike | None = None,
    from_principal_directions: bool = False,
) -> ArithmeticExpression:
    dofdesc = as_dofdesc(dofdesc)

    if from_principal_directions:
        return extrinsic_principal_directions(ambient_dim, dim=dim, dofdesc=dofdesc)
    else:
        return _pder_tangential_onb(ambient_dim, dim=dim, dofdesc=dofdesc)


def first_fundamental_form(
    ambient_dim: int,
    *,
    dim: int | None = None,
    dofdesc: DOFDescriptorLike | None = None,
) -> ArithmeticExpression:
    r"""Computes the matrix elements of the first fundamental form.

    The elements are given by

    .. math::

        g_{ij} \triangleq \frac{\partial X_k}{\partial \xi_i}
                 \frac{\partial X_k}{\partial \xi_j},

    where :math:`\mathbf{X}(\xi)` is the surface parametrization and
    :math:`(i, j) \in \{1, d\}` for :math:`d` = *dim*.
    """
    dofdesc = as_dofdesc(dofdesc)

    if dim is None:
        dim = ambient_dim - 1

    mder = parametrization_derivative_matrix(ambient_dim, dim=dim, dofdesc=dofdesc)
    return cse(mder.T @ mder, "form1_mat", cse_scope.DISCRETIZATION)


def inverse_first_fundamental_form(
    ambient_dim: int,
    *,
    dim: int | None = None,
    dofdesc: DOFDescriptorLike | None = None,
) -> ArithmeticExpression:
    """Computes the matrix elements of the inverse of the first fundamental
    form given in :func:`first_fundamental_form`.
    """
    dofdesc = as_dofdesc(dofdesc)

    if dim is None:
        dim = ambient_dim - 1

    if ambient_dim == dim:
        raise ValueError("volumes are not supported")

    if dim <= 2:
        from pytential.symbolic.primitives import _small_mat_inverse

        form1 = first_fundamental_form(ambient_dim, dim=dim, dofdesc=dofdesc)
        inv_form1 = _small_mat_inverse(form1)
    else:
        raise ValueError(f"{dim}D surfaces not supported")

    return cse(inv_form1, "inv_form1_mat", cse_scope.DISCRETIZATION)


def second_fundamental_form(
    ambient_dim: int,
    *,
    dim: int | None = None,
    dofdesc: DOFDescriptorLike | None = None,
) -> ArithmeticExpression:
    r"""Computes the matrix elements of the second fundamental form.

    They are given by

    .. math::

        h_{ij} \triangleq -\frac{\partial n_k}{\partial \xi_i}
                 \frac{\partial X_k}{\partial \xi_j}
               = n_k \frac{\partial^2 X_k}{\partial \xi_i \partial \xi_j},

    where :math:`\mathbf{n}` is the unit normal vector and
    :math:`(i, j) \in \{1, d\}` for :math:`d` = *dim*. Here the first
    formula is implemented, while
    :func:`pytential.symbolic.primitives.second_fundamental_form` implements
    the second formula.
    """
    dofdesc = as_dofdesc(dofdesc)

    if dim is None:
        dim = ambient_dim - 1

    if ambient_dim == dim:
        raise ValueError("volumes are not supported")

    r = snodes(ambient_dim, dofdesc=dofdesc)
    n = to_spectral(
        normal(ambient_dim, dim=dim, dofdesc=dofdesc).as_vector(), dofdesc=dofdesc
    )

    ds = partial(spectral_num_reference_derivative, dofdesc=dofdesc)
    ref_axes = [((i, 1),) for i in range(dim)]

    form2_mat = np.empty((dim, dim), dtype=object)
    for (i, dn_axis), (j, dr_axis) in product(enumerate(ref_axes), repeat=2):
        if i > j:
            continue

        nu = ds(n, dn_axis)
        rv = ds(r, dr_axis)

        form2_mat[i, j] = form2_mat[j, i] = cse(
            -nu @ rv, f"form2_mat_{i}_{j}", cse_scope.DISCRETIZATION
        )

    return cse(form2_mat, "form2_mat", cse_scope.DISCRETIZATION)


def shape_operator(
    ambient_dim: int,
    *,
    dim: int | None = None,
    dofdesc: DOFDescriptorLike | None = None,
) -> ArithmeticExpression:
    r"""Computes the matrix elements of the (intrinsic) shape operator.

    They are given by

    .. math::

        S_{ij} \triangleq -h_{ij} g^{ij}.
    """
    dofdesc = as_dofdesc(dofdesc)

    if dim is None:
        dim = ambient_dim - 1

    inv_form1 = inverse_first_fundamental_form(ambient_dim, dim=dim, dofdesc=dofdesc)
    form2 = second_fundamental_form(ambient_dim, dim=dim, dofdesc=dofdesc)

    return cse(-form2 @ inv_form1, "shape_operator", cse_scope.DISCRETIZATION)


def extrinsic_shape_operator(
    ambient_dim: int,
    *,
    dim: int | None = None,
    dofdesc: DOFDescriptorLike | None = None,
) -> ArithmeticExpression:
    r"""Computes the matrix elements of the (extrinsic) shape operator,
    sometimes called the Weingarten map.

    Its components form a :math:`n \times n` matrix, where :math:`n` =
    *ambient_dim*. It is given by

    .. math::

        W_{ij} \triangleq g^{kl}
            \frac{\partial n_j}{\partial \xi_k}
            \frac{\partial X_i}{\partial \xi_l}

    i.e. simply the surface gradient of the normal
    :math:`W \triangleq \nabla_\Sigma \mathbf{n}`. It has the property that

    .. math::

        \frac{\partial X_i}{\partial \xi_k} W_{ij} =
        \frac{\partial n_j}{\partial \xi_k}.

    Dotting with the parametrization derivative again retrieves the components
    of the (intrinsic) shape operator.
    """
    dofdesc = as_dofdesc(dofdesc)

    if ambient_dim == 2:
        kappa = summed_curvature(ambient_dim, dim=dim, dofdesc=dofdesc)
        (tangent,) = tangential_onb(ambient_dim, dim=dim, dofdesc=dofdesc).T

        wmap = kappa * np.outer(tangent, tangent)
    else:
        n = normal(ambient_dim, dim=dim, dofdesc=dofdesc).as_vector()

        wmap = np.empty((ambient_dim, ambient_dim), dtype=object)
        for i in range(ambient_dim):
            wmap[i, :] = surface_gradient(ambient_dim, n[i], dim=dim, dofdesc=dofdesc)

    return cse(wmap, "weingarten", cse_scope.DISCRETIZATION)


# }}}


# {{{ curvature


def summed_curvature(
    ambient_dim: int,
    *,
    dim: int | None = None,
    dofdesc: DOFDescriptorLike | None = None,
) -> ArithmeticExpression:
    r"""Sum of the principal curvatures or trace of the :func:`shape_operator`

    .. math::

        \kappa \triangleq \operatorname{tr} S = \sum_{i = 0}^{d - 1} \kappa_i.
    """
    dofdesc = as_dofdesc(dofdesc)

    if dim is None:
        dim = ambient_dim - 1

    if ambient_dim == 1:
        return 0

    if ambient_dim == dim:
        return 0

    op = shape_operator(ambient_dim, dim=dim, dofdesc=dofdesc)
    return cse(np.trace(op), "summed_curvature", cse_scope.DISCRETIZATION)


def mean_curvature(
    ambient_dim: int,
    *,
    dim: int | None = None,
    dofdesc: DOFDescriptorLike | None = None,
) -> ArithmeticExpression:
    r"""Average of the principal curvatures from :func:`summed_curvature`.

    .. math::

        H = \frac{\kappa}{d} = \frac{1}{d - 1} \operatorname{tr} S.
    """
    if dim is None:
        dim = ambient_dim - 1

    return 1.0 / dim * summed_curvature(ambient_dim, dim=dim, dofdesc=dofdesc)


def gaussian_curvature(
    ambient_dim: int,
    *,
    dim: int | None = None,
    dofdesc: DOFDescriptorLike | None = None,
) -> ArithmeticExpression:
    r"""Product of the principal curvatures or determinant of the
    :func:`shape_operator`

    .. math::

        \kappa_G \triangleq \operatorname{det} S = \prod_{i = 1}^d \kappa_i.

    For one dimensional surfaces, the determinant is :math:`0` and the
    Gaussian curvature vanishes.
    """
    dofdesc = as_dofdesc(dofdesc)

    if dim is None:
        dim = ambient_dim - 1

    if ambient_dim == dim:
        return 0

    if dim == 1:
        return 0

    if ambient_dim == 3 and dim == 2:
        s_op = shape_operator(ambient_dim, dim=dim, dofdesc=dofdesc)

        # NOTE: kappa = det(s_op)
        return cse(
            s_op[0, 0] * s_op[1, 1] - s_op[0, 1] * s_op[1, 0],
            "gaussian_curvature",
            cse_scope.DISCRETIZATION,
        )
    else:
        raise NotImplementedError(
            f"not available in {ambient_dim}D for {dim}D surfaces"
        )


def squared_curvature(
    ambient_dim: int,
    *,
    dim: int | None = None,
    dofdesc: DOFDescriptorLike | None = None,
) -> ArithmeticExpression:
    r"""Sum of squares of the principal curvatures

    .. math::

        K \triangleq \sum_{i = 1}^d \kappa_i^2.

    For 2D surfaces in a 3D ambient space, this can also be written
    in terms of the summed curvature and Gaussian curvature

    .. math::

        K = \kappa^2 - 2 \kappa_G.
    """
    dofdesc = as_dofdesc(dofdesc)

    if dim is None:
        dim = ambient_dim - 1

    kappa = summed_curvature(ambient_dim, dim=dim, dofdesc=dofdesc)

    if dim == 1:
        return kappa**2
    elif dim == 2:
        kappa_g = gaussian_curvature(ambient_dim, dim=dim, dofdesc=dofdesc)
        return kappa**2 - 2 * kappa_g
    else:
        raise NotImplementedError(
            f"not available in {ambient_dim}D for {dim}D surfaces"
        )


# }}}


# {{{ principal directions


def principal_curvature(
    ambient_dim: int,
    *,
    dim: int | None = None,
    dofdesc: DOFDescriptorLike | None = None,
) -> ArithmeticExpression:
    """Principal curvatures are the eigenvalues of the :func:`shape_operator`.

    :return: an :class:`~numpy.ndarray` of size *dim* with the expressions of
        all principal curvatures.
    """
    dofdesc = as_dofdesc(dofdesc)

    if dim is None:
        dim = ambient_dim - 1

    if ambient_dim == 1:
        return 0

    if ambient_dim == dim:
        return 0

    s_op = shape_operator(ambient_dim, dim=dim, dofdesc=dofdesc)

    if dim <= 2:
        from pytential.symbolic.primitives import _small_sym_mat_eigenvalues

        eigs = _small_sym_mat_eigenvalues(s_op)

        # FIXME: for some reason these are reversed ??
        return make_obj_array(
            [
                cse(eigs[i], f"principal_curvature_{i}", cse_scope.DISCRETIZATION)
                for i in range(dim)
            ][::-1]
        )
    else:
        raise NotImplementedError(
            f"not available in {ambient_dim}D for {dim}D surfaces"
        )


def principal_directions(
    ambient_dim: int,
    *,
    dim: int | None = None,
    dofdesc: DOFDescriptorLike | None = None,
) -> np.ndarray:
    r"""Principal directions are the eigenvectors of the :func:`shape_operator`.

    The principal directions returned by this function solve the
    generalized eigenvalue problem

    .. math::

        -h q_i = \kappa_i g q_i,

    where :math:`\kappa_i` are the principal curvatures, :math:`h` is the
    second fundamental form and :math:`g` is the first fundamental form.
    To get the actual eigenvectors of the shape operator, it suffices to
    multiply by the first fundamental form, since

    .. math::

        -h g^{-1} d_i = S d_i = \kappa_i d_i,

    where :math:`d_i \triangleq g q_i`.
    """
    dofdesc = as_dofdesc(dofdesc)

    if dim is None:
        dim = ambient_dim - 1

    if ambient_dim == 1:
        return make_obj_array([1])

    if ambient_dim == dim:
        from pytools import wandering_element

        return make_obj_array([
            np.array(axis) for axis in wandering_element(ambient_dim)
        ])

    # solve -h q_i = k g q_i such that q_i g q_i = 1

    form1 = first_fundamental_form(ambient_dim, dim=dim, dofdesc=dofdesc)
    form2 = second_fundamental_form(ambient_dim, dim=dim, dofdesc=dofdesc)

    if dim == 1:
        return make_obj_array([np.array([1 / sqrt(form1)])])
    elif dim == 2:
        g11, g12, _, g22 = form1.flat
        h11, h12, _, h22 = form2.flat
        k1, k2 = principal_curvature(ambient_dim, dim=dim, dofdesc=dofdesc)

        # NOTE: as both h and g are symmetric matrices, q_i are orthogonal
        q1 = cse(make_obj_array([h12 + k1 * g12, -(h11 + k1 * g11)]))
        q2 = cse(make_obj_array([-(h22 + k2 * g22), h12 + k2 * g12]))

        # NOTE: make sure they're also normalized wrt to the metric
        return make_obj_array([
            cse(
                q1 / sqrt(q1 @ (form1 @ q1)),
                "principal_direction_1",
                cse_scope.DISCRETIZATION,
            ),
            cse(
                q2 / sqrt(q2 @ (form1 @ q2)),
                "principal_direction_2",
                cse_scope.DISCRETIZATION,
            ),
        ])
    else:
        raise NotImplementedError(
            f"not available in {ambient_dim}D for {dim}D surfaces"
        )


def extrinsic_principal_directions(
    ambient_dim: int,
    *,
    dim: int | None = None,
    dofdesc: DOFDescriptorLike | None = None,
) -> np.ndarray:
    r"""Principal directions in ambient coordinates, which correspond to the
    eigenvectors of :func:`extrinsic_shape_operator`.

    They are obtained by

    .. math::

        D_{i, j} \triangleq \frac{\partial X_j}{\xi_k} d_{i, k}

    where :math:`d_i` are the intrinsic principal directions from
    :func:`principal_directions`. Note that the principal directions form an
    orthonormal basis of the tangent space, similar to
    :func:`~pytential.symbolic.primitives.tangential_onb`.
    """
    dofdesc = as_dofdesc(dofdesc)

    if dim is None:
        dim = ambient_dim - 1

    q = principal_directions(ambient_dim, dim=dim, dofdesc=dofdesc)

    # Walker 3.52
    mder = parametrization_derivative_matrix(ambient_dim, dim=dim, dofdesc=dofdesc)
    return make_obj_array([
        cse(
            mder @ q[i],
            f"extrinsic_principal_direction_{i}",
            cse_scope.DISCRETIZATION,
        )
        for i in range(q.size)
    ])


# }}}


# {{{ surface derivatives


def surface_gradient(
    ambient_dim: int,
    operand: ArithmeticExpression,
    *,
    dim: int | None = None,
    dofdesc: DOFDescriptorLike | None = None,
    is_spectral: bool = False,
) -> ArithmeticExpression:
    r"""Computes the surface gradient of *operand*. In terms of the ambient
    gradient it is given as

    .. math::

        \nabla_\Sigma f = \nabla f - (\mathbf{n} \cdot \nabla f) \mathbf{n},

    but it can also be written intrinsically as

    .. math::

        (\nabla_\Sigma f)_k = g^{ij}
            \frac{\partial f}{\partial \xi_i}
            \frac{\partial X_k}{\partial \xi_j}.
    """
    dofdesc = as_dofdesc(dofdesc)

    if dim is None:
        dim = ambient_dim - 1

    if is_spectral:
        d = partial(spectral_num_reference_derivative, dofdesc=dofdesc)
    else:
        d = partial(num_reference_derivative, dofdesc=dofdesc)

    inv_form1 = inverse_first_fundamental_form(ambient_dim, dim=dim, dofdesc=dofdesc)
    mder = parametrization_derivative_matrix(ambient_dim, dim=dim, dofdesc=dofdesc)

    # Walker 4.6
    return make_obj_array([
        sum(
            inv_form1[j, k] * d(operand, ((j, 1),)) * mder[i, k]
            for j, k in product(range(dim), repeat=2)
        )
        for i in range(ambient_dim)
    ])


def surface_vector_gradient(
    ambient_dim: int,
    operand: np.ndarray,
    *,
    dim: int | None = None,
    dofdesc: DOFDescriptorLike | None = None,
):
    """Computes the surface gradient of *operand*, where *operand* is a
    vector. The vector surface gradient is defined by applying
    :func:`surface_gradient` componentwise.
    """
    dofdesc = as_dofdesc(dofdesc)

    # Walker 4.7
    result = np.empty((ambient_dim, ambient_dim), dtype=object)
    for i in range(ambient_dim):
        result[i] = surface_gradient(ambient_dim, operand[i], dim=dim, dofdesc=dofdesc)

    return result


def surface_divergence(
    ambient_dim: int,
    operand: np.ndarray,
    *,
    dim: int | None = None,
    dofdesc: DOFDescriptorLike | None = None,
):
    r"""Computes the surface divergence of *operand*. In terms of the ambient
    gradient it is given as

    .. math::

        \nabla_\Sigma \cdot \mathbf{f} =
            \nabla \cdot \mathbf{f} -
            (\mathbf{n} \cdot \nabla \mathbf{f}) \cdot \mathbf{n},

    but it can also be written intrinsically as

    .. math::

        \nabla_\Sigma \cdot \mathbf{f} = g^{ij}
            \frac{\partial f_k}{\partial \xi_i}
            \frac{\partial X_k}{\partial \xi_j}.
    """
    if len(operand) != ambient_dim:
        raise ValueError("operand must be a vector of size 'ambient_dim'")

    dofdesc = as_dofdesc(dofdesc)

    if dim is None:
        dim = ambient_dim - 1

    d = partial(NumReferenceDerivative, dofdesc=dofdesc)
    inv_form1 = inverse_first_fundamental_form(ambient_dim, dim=dim, dofdesc=dofdesc)
    mder = parametrization_derivative_matrix(ambient_dim, dim=dim, dofdesc=dofdesc)

    # Walker 4.20
    return sum(
        inv_form1[i, j]
        * sum(d(((i, 1),), operand[k]) * mder[k, j] for k in range(ambient_dim))
        for i, j in product(range(dim), repeat=2)
    )


def surface_laplace_beltrami(
    ambient_dim: int,
    operand: ArithmeticExpression,
    *,
    dim: int | None = None,
    dofdesc: DOFDescriptorLike | None = None,
):
    r"""Computes the surface Laplace-Beltrami operator. In terms of the ambient
    gradient it is given as

    .. math::

        \Delta_\Sigma f =
            \Delta f - \kappa (\mathbf{n} \cdot \nabla f) -
            (\mathbf{n} \cdot \nabla \nabla f) \cdot \mathbf{n},

    but it can also be written intrinsically as

    .. math::

        \Delta_\Sigma f = g^{ij} \frac{\partial}{\partial \xi_i} \left(
            g^{kl}
            \frac{\partial f}{\partial \xi_k}
            \frac{\partial X_k}{\partial \xi_l}
            \right) \frac{\partial X_k}{\partial \xi_j}.
    """
    dofdesc = as_dofdesc(dofdesc)

    if dim is None:
        dim = ambient_dim - 1

    # Walker 4.23
    # NOTE: don't be tempted to express these in a more compact form;
    # tried that and it didn't work because the intermediate values are not
    # always periodic in the right ways for the spherical harmonic representation
    return surface_divergence(
        ambient_dim,
        surface_gradient(ambient_dim, operand, dim=dim, dofdesc=dofdesc),
        dim=dim,
        dofdesc=dofdesc,
    )


# }}}


# {{{ closed surface


def surface_area(
    ambient_dim: int,
    *,
    dim: int | None = None,
    dofdesc: DOFDescriptorLike | None = None,
):
    dofdesc = as_dofdesc(dofdesc)

    if dim is None:
        dim = ambient_dim - 1

    return cse(
        sintegral(1, ambient_dim, dim, dofdesc=dofdesc),
        "surface_area",
        cse_scope.DISCRETIZATION,
    )


def surface_volume(
    ambient_dim: int,
    *,
    dim: int | None = None,
    dofdesc: DOFDescriptorLike | None = None,
    groupwise: bool = False,
):
    r"""Volume (or area in 2D) enclosed by a closed surface :math:`Sigma`

    .. math::

            V \triangleq \int_{\Sigma} \frac{1}{d}
                \mathbf{x} \cdot \mathbf{n} \,\mathrm{d}S.
    """
    dofdesc = as_dofdesc(dofdesc)

    if dim is None:
        dim = ambient_dim - 1

    x = nodes(ambient_dim, dofdesc=dofdesc).as_vector()
    n = normal(ambient_dim, dim=dim, dofdesc=dofdesc).as_vector()

    return cse(
        sintegral(
            1.0 / ambient_dim * (x @ n),
            ambient_dim,
            dim,
            dofdesc=dofdesc,
            groupwise=groupwise,
        ),
        "surface_volume",
        cse_scope.DISCRETIZATION,
    )


def surface_centroid(
    ambient_dim: int,
    *,
    dim: int | None = None,
    dofdesc: DOFDescriptorLike | None = None,
    groupwise: bool = False,
) -> np.ndarray:
    r"""Centroid of a closed surface

    .. math::

        \mathbf{c} = \frac{1}{|\Omega|} \int_{\Omega} \mathb{x} \,\mathrm{d}S,

    where :math:`|\Omega|` is the volume enclosed by the surface.
    """
    dofdesc = as_dofdesc(dofdesc)

    if dim is None:
        dim = ambient_dim - 1

    x = nodes(ambient_dim, dofdesc=dofdesc).as_vector()
    n = normal(ambient_dim, dim=dim, dofdesc=dofdesc).as_vector()

    # NOTE: this uses the equality
    #   x = 1/2 \nabla (x @ x)
    # and then the divergence theorem to reduce the centroid to the surface.
    volume = surface_volume(ambient_dim, dim=dim, dofdesc=dofdesc, groupwise=groupwise)
    x_integral = sintegral(
        1.0 / 2.0 * (x @ x) * n,
        ambient_dim,
        dim,
        dofdesc=dofdesc,
        groupwise=groupwise,
    )

    return cse(x_integral / volume, "surface_centroid", cse_scope.DISCRETIZATION)


def integral(ambient_dim, dim, operand, dofdesc=None):
    """A surface integral of *operand*."""
    dofdesc = as_dofdesc(dofdesc)

    return NodeSum(
        area_element(ambient_dim, dim=dim, dofdesc=dofdesc) * QWeight(dofdesc) * operand
    )


# }}}


# {{{ element sizes


def element_area(
    ambient_dim: int,
    *,
    dim: int | None = None,
    dofdesc: DOFDescriptorLike | None = None,
    granularity: type | None = GRANULARITY_ELEMENT,
):
    dofdesc = as_dofdesc(dofdesc)
    waa = weights_and_area_elements(ambient_dim, dim=dim, dofdesc=dofdesc)

    dofdesc = dofdesc.copy(granularity=granularity)
    return cse(
        ElementwiseSum(waa, dofdesc=dofdesc),
        "element_area",
        cse_scope.DISCRETIZATION,
    )


def _h_from_volume(ambient_dim, dim=None, dofdesc=None):
    dofdesc = as_dofdesc(dofdesc)

    if dim is None:
        dim = ambient_dim - 1

    return cse(
        ElementwiseSum(weights_and_area_elements(ambient_dim, dim=dim), dofdesc=dofdesc)
        ** (1 / dim),
        "_element_size_from_volume",
        cse_scope.DISCRETIZATION,
    )


def h_max_from_volume(
    ambient_dim: int,
    *,
    dim: int | None = None,
    dofdesc: DOFDescriptorLike | None = None,
):
    r"""Computes a characteristic (largest) length scale from element areas

    .. math::

        h_{max} \triangleq \max_{k}
            \left|\int_{E_k} \,\mathrm{d}S\right|^{1/d}.

    Note that this metric is mostly suited for meshes where each element is
    mostly isotropic, i.e. stretches the same in all directions. For a more
    general metric on simplices, see :func:`pytential.symbolic.primitives.h_max`.
    """
    dofdesc = as_dofdesc(dofdesc)

    return cse(
        NodeMax(_h_from_volume(ambient_dim, dim=dim, dofdesc=dofdesc)),
        "h_max_from_volume",
        cse_scope.DISCRETIZATION,
    )


def h_min_from_volume(
    ambient_dim: int,
    *,
    dim: int | None = None,
    dofdesc: DOFDescriptorLike | None = None,
):
    r"""Computes a characteristic (smallest) length scale from element areas

    .. math::

        h_{min} \triangleq \min_{k}
            \left|\int_{E_k} \,\mathrm{d}S\right|^{1/d}.
    """
    dofdesc = as_dofdesc(dofdesc)

    return cse(
        NodeMin(_h_from_volume(ambient_dim, dim=dim, dofdesc=dofdesc)),
        "h_min_from_volume",
        cse_scope.DISCRETIZATION,
    )


def h_min(
    ambient_dim: int,
    *,
    dim: int | None = None,
    dofdesc: DOFDescriptorLike | None = None,
):
    """Computes a characteristic (smallest) length scale from element.

    Like :func:`pytential.symbolic.primitives.h_max`, this based on looking at
    the stretching of an element.
    """
    from pytential.symbolic.primitives import _quad_resolution

    dofdesc = as_dofdesc(dofdesc).copy(granularity=GRANULARITY_ELEMENT)
    r = _quad_resolution(ambient_dim, dim=dim, dofdesc=dofdesc)

    return cse(NodeMin(r), "h_min", cse_scope.DISCRETIZATION)


def element_length(
    ambient_dim: int,
    *,
    dim: int | None = None,
    dofdesc: DOFDescriptorLike | None = None,
    granularity: Any = GRANULARITY_ELEMENT,
):
    """Computes a characteristic length for each element.

    Used for :func:`pystopt.symbolic.primitives.h_min`.
    """
    from pytential.symbolic.primitives import _quad_resolution

    dofdesc = as_dofdesc(dofdesc).copy(granularity=granularity)
    r = _quad_resolution(ambient_dim, dim=dim, dofdesc=dofdesc)

    return cse(r, "element_length", cse_scope.DISCRETIZATION)


# }}}


# {{{ bump functions


def gaussian_bump_function(
    ambient_dim: int,
    *,
    dim: int | None = None,
    dofdesc: DOFDescriptorLike | None = None,
    normalized: bool = True,
    sigma_name: str = "sigma",
):
    r"""Constructs a simple Gaussian bump function

    .. math::

        h(\mathbf{x}) = \frac{1}{\sqrt{2 \pi \sigma^2}}
            \exp\left(-\frac{\mathbf{x} \cdot \mathbf{x}}{2 \sigma^2}\right),

    where :math:`\sigma` is the variance of the Gaussian. If *normalized* is
    *True*, the bump function is normalized with respect to the
    :math:`\mathrm{L}^\infty` norm, i.e. the prefactor is removed.
    """

    if dim is None:
        dim = ambient_dim - 1

    sigma = var(sigma_name)
    y = make_sym_vector("y", ambient_dim)

    h_fac = 1 if normalized else (1 / (sqrt(2 * np.pi * sigma**2)))
    return cse(h_fac * exp(-(y @ y) / (2.0 * sigma**2)))


def spherical_bump_function(
    ambient_dim: int, h: ArithmeticExpression, *, normalized=True
):
    r"""Constructs a bump function

    .. math::

        \tilde{h}(\mathbf{h}) = \sin \theta h(\mathbf{x}),

    where *h* is a given bump function and the angle :math:`theta` is defined
    as

    .. math::

        \theta = \arccos \frac{z}{r}.

    This ensures that no issues arise at the poles :\math:`\theta = 0, \pi`,
    as the bump function goes to zero.
    """

    if ambient_dim == 2:
        return h

    if ambient_dim == 3:
        x = nodes(ambient_dim).as_vector()
        theta = arccos(x[2] / sqrt(x @ x))
        fac = sin(theta)
        h = cse(fac * h)

        if normalized:
            h = h / NodeMax(abs(h))
        return h

    raise ValueError(f"unsupported dimension: {ambient_dim}")


# }}}


# {{{ norm


def real_scalar_dot(
    ambient_dim: int,
    *,
    dim: int | None = None,
    dofdesc: DOFDescriptorLike | None = None,
):
    if dim is None:
        dim = ambient_dim - 1

    return sintegral(var("x") * var("y"), ambient_dim, dim, dofdesc=dofdesc)


def norm(
    ambient_dim: int,
    operand: ArithmeticExpression,
    *,
    p: float = 2,
    dim: int | None = None,
    dofdesc: DOFDescriptorLike | None = None,
):
    if dim is None:
        dim = ambient_dim - 1

    if p == 2:
        if isinstance(operand, np.ndarray) and operand.dtype.char == "O":
            p_operand = operand @ operand
        else:
            p_operand = operand**2

        return sqrt(sintegral(p_operand, ambient_dim, dim, dofdesc=dofdesc))

    if p == np.inf:
        result = NodeMax(abs(operand))

        if isinstance(result, np.ndarray) and result.dtype.char == "O":  # pylint: disable=no-member
            from pymbolic.primitives import Max

            result = Max(tuple(result))

        return result

    if p == -np.inf:
        result = NodeMin(abs(operand))

        if isinstance(result, np.ndarray) and result.dtype.char == "O":  # pylint: disable=no-member
            from pymbolic.primitives import Min

            result = Min(tuple(result))

        return result

    from numbers import Number

    if isinstance(p, Number):
        if isinstance(operand, np.ndarray) and operand.dtype.char == "O":
            p_operand = sum(c**p for c in operand)
        else:
            p_operand = operand**p

        return sintegral(p_operand, ambient_dim, dim, dofdesc=dofdesc) ** (1.0 / p)

    raise TypeError(f"unsupported norm type: '{p}'")


def relative_norm(
    ambient_dim: int,
    x: ArithmeticExpression,
    y: ArithmeticExpression,
    *,
    p: float = 2,
    atol: float = 1.0e-14,
    dim: int | None = None,
    dofdesc: DOFDescriptorLike | None = None,
):
    dofdesc = as_dofdesc(dofdesc)

    norm_y = cse(norm(ambient_dim, y, p=p, dim=dim, dofdesc=dofdesc))
    norm_d = norm(ambient_dim, x - y, p=p, dim=dim, dofdesc=dofdesc)
    norm_y = If(Comparison(norm_y, "<", atol), 1, norm_y)

    return norm_d / norm_y


# }}}


# {{{ misc


def spherical_angles(
    ambient_dim: int,
    *,
    dim: int | None = None,
    dofdesc: DOFDescriptorLike | None = None,
) -> tuple[ArithmeticExpression, ArithmeticExpression]:
    r"""Recover spherical coordinates on the geometry.

    The transformation is given by

    .. math::

        \begin{cases}
        x = \sin \theta \cos \phi, \\
        y = \sin \theta \sin \phi, \\
        z = \cos \theta,
        \end{cases}

    where :math:`\theta \in [0, \pi]` and :math:`\phi \in [0, 2 \pi]`.
    """
    dofdesc = as_dofdesc(dofdesc)

    assert ambient_dim == 3
    assert dim is None or dim == 2

    x = cse(
        nodes(ambient_dim, dofdesc=dofdesc).as_vector()
        - surface_centroid(ambient_dim, dim=dim, dofdesc=dofdesc),
        "centered_nodes",
        cse_scope.DISCRETIZATION,
    )

    theta = arctan2(sqrt(x[:2] @ x[:2]), x[2])
    phi = arctan2(x[1], x[0])

    return theta, phi


def n_dot(
    ambient_dim_or_expr: int | ArithmeticExpression,
    *,
    dim: int | None = None,
    dofdesc: DOFDescriptorLike | None = None,
    name: str = "x",
) -> ArithmeticExpression:
    if isinstance(ambient_dim_or_expr, int):
        ambient_dim = ambient_dim_or_expr
        expr = make_sym_vector(name, ambient_dim)
    else:
        ambient_dim = len(ambient_dim_or_expr)
        expr = ambient_dim_or_expr

    n = normal(ambient_dim, dim=dim, dofdesc=dofdesc).as_vector()

    return n @ expr


def n_times(
    ambient_dim: int,
    expr: ArithmeticExpression | None = None,
    *,
    dim: int | None = None,
    dofdesc: DOFDescriptorLike | None = None,
    name: str = "x",
) -> np.ndarray:
    if expr is None:
        expr = var(name)

    n = normal(ambient_dim, dim=dim, dofdesc=dofdesc).as_vector()
    return expr * n


def project_normal(
    ambient_dim_or_expr: int | np.ndarray,
    *,
    dim: int | None = None,
    dofdesc: DOFDescriptorLike | None = None,
    name: str = "x",
) -> np.ndarray:
    dofdesc = as_dofdesc(dofdesc)

    if isinstance(ambient_dim_or_expr, int):
        ambient_dim = ambient_dim_or_expr
        expr = make_sym_vector(name, ambient_dim)
    else:
        ambient_dim = len(ambient_dim_or_expr)
        expr = ambient_dim_or_expr

    n = normal(ambient_dim, dim=dim, dofdesc=dofdesc).as_vector()
    return cse(n_dot(expr, dim=dim, dofdesc=dofdesc)) * n


def project_tangent(
    ambient_dim_or_expr: int | np.ndarray,
    *,
    dim: int | None = None,
    dofdesc: DOFDescriptorLike | None = None,
    name: str = "x",
) -> np.ndarray:
    dofdesc = as_dofdesc(dofdesc)

    if isinstance(ambient_dim_or_expr, int):
        ambient_dim = ambient_dim_or_expr
        expr = make_sym_vector(name, ambient_dim)
    else:
        ambient_dim = len(ambient_dim_or_expr)
        expr = ambient_dim_or_expr

    n = normal(ambient_dim, dim=dim, dofdesc=dofdesc).as_vector()
    return expr - cse(n_dot(expr, dim=dim, dofdesc=dofdesc)) * n


# }}}
