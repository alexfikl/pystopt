# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

import numpy as np

from pymbolic.geometric_algebra.mapper import DerivativeBinder as DerivativeBinderBase
from pymbolic.interop.sympy import SympyToPymbolicMapper
from pymbolic.mapper.dependency import DependencyMapper as DependencyMapperBase
from pymbolic.mapper.stringifier import (
    PREC_NONE,
    PREC_PRODUCT,
    CSESplittingStringifyMapperMixin,
)
from pytools.obj_array import make_obj_array

from pytential.symbolic.mappers import (  # isort:skip
    Collector as CollectorBase,
    CombineMapper,
    DerivativeSourceAndNablaComponentCollector as DerivativeSourceAndNablaComponentCollectorBase,  # noqa: E501
    DerivativeSourceFinder as DerivativeSourceFinderBase,
    DerivativeTaker,
    DiscretizationStageTagger as DiscretizationStageTaggerBase,
    EvaluationMapper as EvaluationMapperBase,
    IdentityMapper as IdentityMapperBase,
    InterpolationPreprocessor as InterpolationPreprocessorBase,
    LocationTagger as LocationTaggerBase,
    NablaComponentToUnitVector as NablaComponentToUnitVectorBase,
    QBXPreprocessor as QBXPreprocessorBase,
    StringifyMapper as StringifyMapperBase,
)

# {{{ execution


class IdentityMapper(IdentityMapperBase):
    def map_spectral_node_coordinate_component(self, expr):
        return self.map_node_coordinate_component(expr)

    def map_spectral_num_reference_derivative(self, expr):
        return self.map_num_reference_derivative(expr)

    def map_spectral_projection(self, expr):
        operand = self.rec(expr.operand)
        if operand is expr.operand:
            return expr

        return type(expr)(operand=operand, forward=expr.forward, dofdesc=expr.dofdesc)

    def map_filter_operator(self, expr):
        operand = self.rec(expr.operand)
        if operand is expr.operand:
            return expr

        return type(expr)(operand, context=expr.context, dofdesc=expr.dofdesc)

    def map_spectral_conjugate_scaled_area_element(self, expr):
        return expr

    def map_groupwise_sum(self, expr):
        operand = self.rec(expr.operand)
        if operand is expr.operand:
            return expr

        return type(expr)(operand, dofdesc=expr.dofdesc)


class Collector(CollectorBase, CombineMapper):
    def map_spectral_node_coordinate_component(self, expr):
        return self.map_node_coordinate_component(expr)

    def map_spectral_num_reference_derivative(self, expr):
        return self.map_num_reference_derivative(expr)

    def map_spectral_projection(self, expr):
        return self.rec(expr.operand)

    def map_filter_operator(self, expr):
        return self.rec(expr.operand)

    def map_spectral_conjugate_scaled_area_element(self, expr):
        return set()

    def map_groupwise_sum(self, expr):
        return self.rec(expr.operand)


class OperatorCollector(Collector):
    def map_int_g(self, expr):
        return {expr} | Collector.map_int_g(self, expr)


class DependencyMapper(DependencyMapperBase, Collector):
    pass


class EvaluationMapper(EvaluationMapperBase):
    def map_spectral_node_coordinate_component(self, expr):
        return self.map_node_coordinate_component(expr)

    def map_spectral_num_reference_derivative(self, expr):
        return self.map_num_reference_derivative(expr)

    def map_spectral_projection(self, expr):
        operand = self.rec(expr.operand)
        if operand is expr.operand:
            return expr

        return type(expr)(operand, forward=expr.forward, dofdesc=expr.dofdesc)

    def map_filter_operator(self, expr):
        operand = self.rec(expr.operand)
        if operand is expr.operand:
            return expr

        return type(expr)(operand, context=expr.context, dofdesc=expr.dofdesc)

    def map_spectral_conjugate_scaled_area_element(self, expr):
        return expr

    def map_groupwise_sum(self, expr):
        return self.rec(expr.operand)


class DerivativeSourceAndNablaComponentCollector(
    Collector, DerivativeSourceAndNablaComponentCollectorBase
):
    pass


class NablaComponentToUnitVector(EvaluationMapper, NablaComponentToUnitVectorBase):
    pass


class DerivativeSourceFinder(EvaluationMapper, DerivativeSourceFinderBase):
    pass


class DerivativeBinder(DerivativeBinderBase, IdentityMapper):
    derivative_source_and_nabla_component_collector = (
        DerivativeSourceAndNablaComponentCollector
    )
    nabla_component_to_unit_vector = NablaComponentToUnitVector
    derivative_source_finder = DerivativeSourceFinder

    def take_derivative(self, ambient_axis, expr):
        return DerivativeTaker(ambient_axis)(expr)


class LocationTagger(LocationTaggerBase):
    def map_spectral_node_coordinate_component(self, expr):
        return self.map_node_coordinate_component(expr)

    def map_spectral_num_reference_derivative(self, expr):
        return self.map_num_reference_derivative(expr)

    def map_spectral_projection(self, expr):
        operand = self.rec(expr.operand)
        dofdesc = self._default_dofdesc(expr.dofdesc)
        if operand is expr.operand and dofdesc is expr.dofdesc:
            return expr

        return type(expr)(operand=operand, forward=expr.forward, dofdesc=dofdesc)

    def map_filter_operator(self, expr):
        operand = self.rec(expr.operand)
        dofdesc = self._default_dofdesc(expr.dofdesc)
        if operand is expr.operand and dofdesc is expr.dofdesc:
            return expr

        return type(expr)(operand, context=expr.context, dofdesc=dofdesc)

    def map_spectral_conjugate_scaled_area_element(self, expr):
        dofdesc = self._default_dofdesc(expr.dofdesc)
        if dofdesc is expr.dofdesc:
            return expr

        return type(expr)(dofdesc=dofdesc)

    def map_groupwise_sum(self, expr):
        operand = self.rec(expr.operand)
        dofdesc = self._default_dofdesc(expr.dofdesc)
        if operand is expr.operand and dofdesc is expr.dofdesc:
            return expr

        return type(expr)(operand, dofdesc=dofdesc)


class ToTargetTagger(LocationTagger):
    def __init__(self, default_source, default_target):
        super().__init__(default_target, default_source=default_source)
        self.operand_rec = LocationTagger(default_source, default_source=default_source)


class DiscretizationStageTagger(DiscretizationStageTaggerBase, IdentityMapper):
    def map_spectral_node_coordinate_component(self, expr):
        return self.map_node_coordinate_component(expr)

    def map_spectral_num_reference_derivative(self, expr):
        return self.map_num_reference_derivative(expr)


class InterpolationPreprocessor(InterpolationPreprocessorBase, IdentityMapper):
    def __init__(self, places, from_discr_stage=None):
        if from_discr_stage is None:
            # NOTE: stage1 should always be the spectral discretization
            from pystopt import sym

            from_discr_stage = sym.QBX_SOURCE_STAGE1

        super().__init__(places, from_discr_stage=from_discr_stage)
        self.tagger = DiscretizationStageTagger(self.from_discr_stage)

    def map_num_reference_derivative(self, expr):
        from pystopt.mesh import SpectralDiscretization

        discr = self.places.get_discretization(
            expr.dofdesc.geometry, self.from_discr_stage
        )
        assert isinstance(discr, SpectralDiscretization)

        return super().map_num_reference_derivative(expr)

    def map_int_g(self, expr):
        from pystopt.mesh import SpectralDiscretization

        discr = self.places.get_discretization(
            expr.source.geometry, self.from_discr_stage
        )
        assert isinstance(discr, SpectralDiscretization)

        return super().map_int_g(expr)


class QBXPreprocessor(QBXPreprocessorBase, IdentityMapper):
    pass


# }}}


# {{{ stringifier


class StringifyMapper(StringifyMapperBase):
    def map_groupwise_sum(self, expr, enclosing_prec):
        from pytential.symbolic.mappers import stringify_where

        return "GrSum[{}]({})".format(
            stringify_where(expr.dofdesc), self.rec(expr.operand, PREC_NONE)
        )

    def map_spectral_node_coordinate_component(self, expr, enclosing_prec):
        from pytential.symbolic.mappers import stringify_where

        return "xhat{}[{}]".format(expr.ambient_axis, stringify_where(expr.dofdesc))

    def map_spectral_num_reference_derivative(self, expr, enclosing_prec):
        from pytential.symbolic.mappers import stringify_where

        diff_op = " ".join(
            f"d/ds{axis}" if mult == 1 else f"d/ds{axis}^{mult}"
            for axis, mult in expr.ref_axes
        )

        result = "{}[{}] {}".format(
            diff_op,
            stringify_where(expr.dofdesc),
            self.rec(expr.operand, PREC_PRODUCT),
        )

        if enclosing_prec >= PREC_PRODUCT:
            return f"({result})"
        else:
            return result

    def map_spectral_projection(self, expr, enclosing_prec):
        from pytential.symbolic.mappers import stringify_where

        return "{}[{}]({})".format(
            "Spec" if expr.forward else "InvSpec",
            stringify_where(expr.dofdesc),
            self.rec(expr.operand, PREC_PRODUCT),
        )

    def map_spectral_conjugate_scaled_area_element(self, expr, enclosing_prec):
        from pytential.symbolic.mappers import stringify_where

        return "SpecAreaElement[{}]".format(stringify_where(expr.dofdesc))


class PrettyStringifyMapper(CSESplittingStringifyMapperMixin, StringifyMapper):
    pass


# }}}


# {{{ sympy

SYMPY_COORD_NAME = "_sp_x"
SYMPY_NORMAL_NAME = "_sp_normal"
SYMPY_TANGENT_NAME = ("_sp_tangent_0", "_sp_tangent_1")


class SympyToPytentialMapper(SympyToPymbolicMapper):
    def __init__(self, ambient_dim, dim=None):
        super().__init__()
        if dim is None:
            dim = ambient_dim - 1

        self.ambient_dim = ambient_dim
        self.dim = dim

    def rec(self, expr, *args, **kwargs):
        from pystopt import sym

        if isinstance(expr, list | tuple | np.ndarray | sym.MultiVector):
            array = [self.rec(x) for x in expr]

            if isinstance(expr, sym.MultiVector):
                return type(expr)(make_obj_array(array))
            elif isinstance(expr, np.ndarray):
                return np.array(array, dtype=expr.dtype)
            return type(expr)(array)

        return super().rec(expr, *args, **kwargs)

    def map_object(self, expr):
        if isinstance(expr, int | float | complex | np.number):
            return expr

        return self.not_supported(expr)

    # {{{ geometry

    def _map_node_coordinate_component(self, axis):
        from pystopt import sym

        if 0 <= axis <= self.ambient_dim - 1:
            return sym.NodeCoordinateComponent(axis, sym.as_dofdesc(None))
        else:
            raise IndexError("'nodes' index out of range")

    def _map_normal(self, axis):
        from pystopt import sym

        if 0 <= axis <= self.ambient_dim - 1:
            return sym.normal(self.ambient_dim, dim=self.dim).as_vector()[axis]
        else:
            raise IndexError("'normal' index out of range")

    def _map_tangential_onb(self, idx, axis):
        from pystopt import sym

        if 0 <= axis <= self.ambient_dim - 1 and 0 <= idx <= self.dim:
            onb = sym.tangential_onb(self.ambient_dim, dim=self.dim).T
            return onb[idx][axis]
        else:
            raise IndexError("'tangential_onb' index out of range")

    def map_Indexed(self, expr):  # noqa: N802
        if len(expr.args) == 2:
            base = expr.args[0]
            axis = int(self.rec(expr.args[1]))

            if base.name == SYMPY_COORD_NAME:
                return self._map_node_coordinate_component(axis)
            elif base.name == SYMPY_NORMAL_NAME:
                return self._map_normal(axis)
            elif base.name in SYMPY_TANGENT_NAME:
                idx = SYMPY_TANGENT_NAME.index(base.name)
                return self._map_tangential_onb(idx, axis)

        return super().map_Indexed(expr)

    # }}}

    def map_DenseMatrix(self, expr):  # noqa: N802
        result = np.empty(expr.shape, dtype=object)
        for i, v in np.ndenumerate(expr):
            result[i] = self.rec(v)

        return result

    def not_supported(self, expr):
        if getattr(expr, "is_Function", False):
            import pytential.symbolic.primitives as prim

            from pystopt import sym

            name = self.function_name(expr)
            fn = getattr(sym, name, None)
            if isinstance(fn, prim.NumpyMathFunction):
                return fn(self.rec(expr.args[0]))

        return super().not_supported(expr)


def to_sympy(ambient_dim: int, expr, dim=None):
    return SympyToPytentialMapper(ambient_dim, dim=dim)(expr)


# }}}


# {{{ dofdesc replacer


class DOFDescriptorReplaceMapper(IdentityMapper):
    def __init__(self, dofdesc):
        super().__init__()
        self.dofdesc = dofdesc

    def map_node_coordinate_component(self, expr):
        return type(expr)(expr.ambient_axis, dofdesc=self.dofdesc)


# }}}


# {{{


def map_math_derivative(i, func, args, allowed_nonsmoothness="none"):
    import pytential.symbolic.primitives as prim

    if not isinstance(func, prim.Function):
        from pymbolic.mapper.differentiator import map_math_functions_by_name

        return map_math_functions_by_name(
            i, func, args, allowed_nonsmoothness=allowed_nonsmoothness
        )

    from pystopt import sym

    if func.name == "sin":
        return sym.cos(*args)
    elif func.name == "cos":
        return -sym.sin(*args)
    elif func.name == "exp":
        return sym.exp(*args)
    elif func.name == "log":
        return 1 / args[0]
    else:
        raise RuntimeError(f"cannot differentiate unrecognized function: '{func}'")


def differentiate(
    expression,
    variable,
    func_mapper=map_math_derivative,
    allowed_nonsmoothness="none",
):
    import pymbolic.primitives as prim

    if not isinstance(variable, prim.Variable | prim.Subscript):
        variable = prim.make_variable(variable)

    from pymbolic.mapper.differentiator import DifferentiationMapper

    return DifferentiationMapper(
        variable, func_mapper, allowed_nonsmoothness=allowed_nonsmoothness
    )(expression)


# }}}
