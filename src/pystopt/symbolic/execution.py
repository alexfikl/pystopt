# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

from __future__ import annotations

import numpy as np

from arraycontext import PyOpenCLArrayContext
from pytential.symbolic.compiler import OperatorCompiler as OperatorCompilerBase
from pytential.symbolic.execution import BoundExpression as BoundExpressionBase
from pytential.symbolic.execution import EvaluationMapper as EvaluationMapperBase
from pytools import memoize_method

from pystopt.symbolic.mappers import DependencyMapper, IdentityMapper
from pystopt.tools import get_default_logger

logger = get_default_logger(__name__)


# {{{ bind


def prepare_expr(places, expr, auto_where=None):
    from pytential.symbolic.execution import _prepare_auto_where

    auto_source, auto_target = _prepare_auto_where(auto_where, places=places)

    from pystopt.symbolic.mappers import DerivativeBinder, ToTargetTagger

    expr = ToTargetTagger(auto_source, auto_target)(expr)
    expr = DerivativeBinder()(expr)

    from pytential.source import LayerPotentialSourceBase

    for name, place in places.places.items():
        if isinstance(place, LayerPotentialSourceBase):
            expr = place.preprocess_optemplate(name, places, expr)

    from pystopt.symbolic.mappers import InterpolationPreprocessor

    expr = InterpolationPreprocessor(places)(expr)

    return expr


def merge_int_gs(places, expr):
    from pytential.qbx import QBXLayerPotentialSource

    fmmlib = any(
        value.fmm_backend == "fmmlib"
        for value in places.places.values()
        if isinstance(value, QBXLayerPotentialSource)
    )

    if not fmmlib:
        from pymbolic.primitives import Expression
        from pytential.symbolic.pde.system_utils import merge_int_g_exprs

        if isinstance(expr, np.ndarray | list | tuple):
            expr = np.array(merge_int_g_exprs(list(expr)), dtype=object)
        elif isinstance(expr, Expression):
            expr = merge_int_g_exprs([expr])[0]
    else:
        print("not merging!!!")

    return expr


def bind(
    places,
    expr,
    *,
    auto_where=None,
    _skip_prepare: bool = False,
    _skip_merge: bool = True,
):
    from pytential.collection import GeometryCollection

    if not _skip_prepare:
        if not isinstance(places, GeometryCollection):
            places = GeometryCollection(places, auto_where=auto_where)
            auto_where = places.auto_where

        expr = prepare_expr(places, expr, auto_where=auto_where)
        if not _skip_merge:
            expr = merge_int_gs(places, expr)
    else:
        from pystopt.symbolic.mappers import InterpolationPreprocessor

        expr = InterpolationPreprocessor(places)(expr)

    assert isinstance(places, GeometryCollection)
    return BoundExpression(places, expr)


# }}}


# {{{ compiler


class OperatorCompiler(OperatorCompilerBase, IdentityMapper):
    def __init__(self, places, prefix="_expr"):
        super().__init__(places, prefix=prefix)
        self.group_to_operators = None

        self.dep_mapper = DependencyMapper(
            # include_operator_bindings=False,
            include_lookups=False,
            include_subscripts=False,
            include_calls="descend_args",
        )

    # {{{ copy pasted wholesale from pytential

    def __call__(self, expr):
        # {{{ collect operators by operand

        from pytential.symbolic.primitives import IntG

        from pystopt.symbolic.mappers import OperatorCollector

        operators = [op for op in OperatorCollector()(expr) if isinstance(op, IntG)]

        self.group_to_operators = {}
        for op in operators:
            features = self.op_group_features(op)
            self.group_to_operators.setdefault(features, set()).add(op)

        # }}}

        # Traverse the expression, generate code.

        result = IdentityMapper.__call__(self, expr)

        # Put the toplevel expressions into variables as well.

        from pytools.obj_array import obj_array_vectorize

        result = obj_array_vectorize(self.assign_to_new_var, result)

        from pytential.symbolic.compiler import Code, _compute_schedule

        inputs, schedule = _compute_schedule(self.dep_mapper, self.code, result)
        return Code(inputs, schedule, result)

    # }}}


# }}}


# {{{ evaluation


class EvaluationMapper(EvaluationMapperBase):
    def map_node_sum(self, expr):
        return self.array_context.np.sum(self.rec(expr.operand))

    def map_groupwise_sum(self, expr):
        actx = self.array_context
        operand = self.rec(expr.operand)

        def _sum(ary):
            if isinstance(ary, actx.array_types):
                return actx.np.sum(ary)
            else:
                # FIXME: this shouldn't hardcode a DOFArray
                from pystopt.dof_array import DOFArray

                return DOFArray(actx, tuple([actx.np.sum(subary) for subary in ary]))

        from meshmode.dof_array import rec_map_dof_array_container

        return rec_map_dof_array_container(_sum, operand)

    def map_spectral_node_coordinate_component(self, expr):
        discr = self.places.get_discretization(
            expr.dofdesc.geometry, expr.dofdesc.discr_stage
        )

        from pystopt.mesh import SpectralDiscretization

        if isinstance(discr, SpectralDiscretization):
            if discr.xlm is not None:
                return self.array_context.thaw(discr.xlm[expr.ambient_axis])

            nodes = self.array_context.thaw(discr.nodes()[expr.ambient_axis])
            return discr.to_spectral_conn(nodes)
        else:
            return super().map_node_coordinate_component(expr)

    def map_num_reference_derivative(self, expr):
        # NOTE: this is only here to use discr.num_reference_derivative
        ref_axes = [axis for axis, mult in expr.ref_axes for _ in range(mult)]
        discr = self.places.get_discretization(
            expr.dofdesc.geometry, expr.dofdesc.discr_stage
        )

        from pystopt.mesh import SpectralDiscretization

        if isinstance(discr, SpectralDiscretization):
            return discr.num_reference_derivative(ref_axes, self.rec(expr.operand))
        else:
            return super().map_num_reference_derivative(expr)

    def map_spectral_num_reference_derivative(self, expr):
        discr = self.places.get_discretization(
            expr.dofdesc.geometry, expr.dofdesc.discr_stage
        )

        from pystopt.mesh import SpectralDiscretization

        if isinstance(discr, SpectralDiscretization):
            ref_axes = [axis for axis, mult in expr.ref_axes for _ in range(mult)]

            from pystopt.dof_array import SpectralDOFArray

            vec = self.rec(expr.operand)

            if discr.use_spectral_derivative:
                assert isinstance(vec, SpectralDOFArray)
                return discr.spectral_num_reference_derivative(ref_axes, vec)
            else:
                if isinstance(vec, SpectralDOFArray):
                    vec = discr.from_spectral_conn(vec)

                return discr.num_reference_derivative(ref_axes, vec)
        else:
            return super().map_num_reference_derivative(expr)

    def map_spectral_projection(self, expr):
        discr = self.places.get_discretization(
            expr.dofdesc.geometry, expr.dofdesc.discr_stage
        )
        operand = self.rec(expr.operand)

        from pystopt.mesh import SpectralDiscretization
        from pystopt.mesh.spectral import from_spectral, to_spectral

        if isinstance(discr, SpectralDiscretization):
            if expr.forward:
                return to_spectral(discr, operand, strict=False)
            else:
                return from_spectral(discr, operand, strict=False)
        else:
            raise TypeError(f"{type(discr).__name__}")

    def map_filter_operator(self, expr):
        operand = self.rec(expr.operand)
        discr = self.places.get_discretization(
            expr.dofdesc.geometry, expr.dofdesc.discr_stage
        )

        from pystopt.filtering import apply_filter_spectral

        return apply_filter_spectral(self.array_context, discr, operand, **expr.context)

    def map_spectral_conjugate_scaled_area_element(self, expr):
        from pystopt.mesh import SpectralDiscretization

        discr = self.places.get_discretization(
            expr.dofdesc.geometry, expr.dofdesc.discr_stage
        )
        assert isinstance(discr, SpectralDiscretization)

        return self.array_context.thaw(discr._conj_scaled_area_element())

    def map_if(self, expr):
        actx = self.array_context
        cond = self.rec(expr.condition)

        from meshmode.dof_array import DOFArray

        if isinstance(cond, DOFArray):
            return actx.np.where(cond, self.rec(expr.then), self.rec(expr.else_))
        elif isinstance(cond, bool | np.bool_):
            if cond:
                return self.rec(expr.then)
            else:
                return self.rec(expr.else_)
        else:
            raise TypeError(f"unsupported type: {type(cond).__name__}")


class BoundExpression(BoundExpressionBase):
    @property
    @memoize_method
    def code(self):
        return OperatorCompiler(self.places)(self.sym_op_expr)

    def eval(
        self,
        context=None,
        timing_data=None,
        array_context: PyOpenCLArrayContext | None = None,
    ):
        if context is None:
            context = {}

        # NOTE: avoid compiling any code if the expression is long lived
        # and already nicely cached in the collection from a previous run
        import pymbolic.primitives as prim
        import pytential.symbolic.primitives as sym

        if (
            isinstance(self.sym_op_expr, prim.CommonSubexpression)
            and self.sym_op_expr.scope == sym.cse_scope.DISCRETIZATION
        ):
            from pytential.symbolic.execution import EvaluationMapperCSECacheKey

            cache = self.places._get_cache(EvaluationMapperCSECacheKey)

            from numbers import Number

            expr = self.sym_op_expr
            if expr.child in cache:
                value = cache[expr.child]
                if expr.scope == sym.cse_scope.DISCRETIZATION and not isinstance(
                    value, Number
                ):
                    value = array_context.thaw(value)

                return value

        from pytential.symbolic.execution import (
            _find_array_context_from_args_in_context,
            execute,
        )

        array_context = _find_array_context_from_args_in_context(context, array_context)

        mapper = EvaluationMapper(
            self, array_context, context=context, timing_data=timing_data
        )
        return execute(self.code, mapper)


# }}}
