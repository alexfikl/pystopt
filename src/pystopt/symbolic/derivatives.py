# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

"""
.. autofunction:: adjoint_normal_shape_derivative
.. autofunction:: adjoint_normal_shape_gradient
.. autofunction:: adjoint_curvature_shape_derivative
.. autofunction:: adjoint_curvature_shape_gradient
.. autofunction:: adjoint_curvature_vector_shape_derivative
.. autofunction:: adjoint_curvature_vector_shape_gradient
"""

from pystopt import sym

# {{{ normal shape derivatives


def _adjoint_normal_shape_derivative(ambient_dim, f, divf, dim=None, dofdesc=None):
    n = sym.normal(ambient_dim, dim=dim, dofdesc=dofdesc).as_vector()
    kappa = sym.summed_curvature(ambient_dim, dim=dim, dofdesc=dofdesc)

    return divf - kappa * (f @ n)


def adjoint_normal_shape_derivative(ambient_dim, f, *, dim=None, dofdesc=None):
    r"""Computes the adjoint of the Eulerian shape derivative of the normal
    applied to the vector *f* with respect to the :math:`L^2` inner product.

    From Lemma 5.5 in [Walker2015]_, we have that the Eulerian shape derivative
    of the normal is given by

    .. math::

        \mathbf{n}'[\mathbf{V}] = -\nabla_\Sigma (\mathbf{V} \cdot \mathbf{n})

    so, using the surface Divergence Theorem, we get that its adjoint is

    .. math::

        \mathbf{n}^*[\mathbf{f}] =
            (\nabla_\Sigma \cdot \mathbf{f}
            - \kappa \mathbf{f} \cdot \mathbf{n}) \mathbf{n}.
    """
    divf = sym.surface_divergence(ambient_dim, f, dim=dim, dofdesc=dofdesc)

    return _adjoint_normal_shape_derivative(
        ambient_dim, f, divf, dim=dim, dofdesc=dofdesc
    )


def adjoint_normal_shape_derivative_ambient(
    ambient_dim, f, *, divf, gradf_dot_n, dim=None, dofdesc=None
):
    r"""Variant of :func:`adjoint_normal_shape_derivative` where *f* is
    defined in the whole ambient space. In this case, we know that

    .. math::

        \nabla_\Sigma \cdot \mathbf{f} = \nabla \cdot \mathbf{f}
            - (\mathbf{n} \cdot \nabla \mathbf{f}) \cdot \mathbf{n},

    so the adjoint normal operator can be computed using these quantities,
    if known.

    :arg divf: ambient divergence of the field *f*.
    :arg gradf_dot_n: normal gradient of the field *f*.
    """
    n = sym.normal(ambient_dim, dim=dim, dofdesc=dofdesc).as_vector()
    divf = divf - gradf_dot_n @ n

    return _adjoint_normal_shape_derivative(
        ambient_dim, f, divf, dim=dim, dofdesc=dofdesc
    )


def adjoint_normal_shape_gradient(ambient_dim, f, *, divf, dim=None, dofdesc=None):
    r"""Shape gradient of the functional

    .. math::

        \int_\Sigma \mathbf{f} \cdot \mathbf{n} \,\mathrm{d}S
        = \langle \nabla g, \mathbf{V} \cdot \mathbf{n}\rangle.
    """
    return divf


# }}}


# {{{ curvature shape derivatives


def _adjoint_curvature_shape_derivative(
    ambient_dim, f, surface_laplacian_f, *, dim=None, dofdesc=None
):
    kappa = sym.summed_curvature(ambient_dim, dim=dim, dofdesc=dofdesc)
    gappa = sym.gaussian_curvature(ambient_dim, dim=dim, dofdesc=dofdesc)

    return -surface_laplacian_f - (kappa**2 - 2 * gappa) * f


def adjoint_curvature_shape_derivative(ambient_dim, f, *, dim=None, dofdesc=None):
    r"""Computes the adjoint of the Eulerian shape derivative of the curvature
    applied to the field *f*, with respect to the :math:`L^2` inner product.

    From Lemma 5.6 in [Walker2015]_, we have that the Eulerian shape derivative
    of the curvature is given by

    .. math::

        \kappa'[\mathbf{V}] = -\Delta_\Sigma (\mathbf{V} \cdot \mathbf{n})
            - (\kappa^2 - 2 \kappa_G) (\mathbf{V} \cdot \mathbf{n})

    so, using the surface Divergence Theorem, we get that its adjoint is

    .. math::

        \kappa^*[f] = -\Delta_\Sigma f - (\kappa^2 - 2 \kappa_G) f
    """
    surface_laplacian_f = sym.surface_laplace_beltrami(
        ambient_dim, f, dim=dim, dofdesc=dofdesc
    )

    return _adjoint_curvature_shape_derivative(
        ambient_dim, f, surface_laplacian_f, dim=dim, dofdesc=dofdesc
    )


def adjoint_curvature_shape_derivative_ambient(
    ambient_dim, f, *, gradf_dot_n, lapf, hessf_dot_n, dim=None, dofdesc=None
):
    r"""Variant of :func:`adjoint_curvature_shape_derivative` where *f* is
    defined in the whole ambient space. In this case, we know that

    .. math::

        \Delta_\Sigma f = \Delta f
            - \kappa * (\mathbf{n} \cdot \nabla f)
            - (\mathbf{n} \cdot \nabla \nabla f) \cdot \mathbf{n}

    so the adjoint normal operator can be computed using these quantities,
    if known.

    :arg gradf_dot_n: normal component of the ambient gradient of the field *f*.
    :arg lapf: ambient Laplacian of the field *f*.
    :arg hessf_dot_n: normal component of the ambient Hessian of *f*.
    """
    kappa = sym.summed_curvature(ambient_dim, dim=dim, dofdesc=dofdesc)
    surface_laplacian_f = lapf - kappa * gradf_dot_n - hessf_dot_n

    return _adjoint_curvature_shape_derivative(
        ambient_dim, f, surface_laplacian_f, dim=dim, dofdesc=dofdesc
    )


def adjoint_curvature_shape_gradient(
    ambient_dim, f, *, gradf_dot_n, lapf, hessf_dot_n, dim=None, dofdesc=None
):
    r"""Shape gradient of the functional

    .. math::

        \int_\Sigma \kappa f \,\mathrm{d}S
        = \langle \nabla g, \mathbf{V} \cdot \mathbf{n}\rangle.
    """
    kappa = sym.summed_curvature(ambient_dim, dim=dim, dofdesc=dofdesc)
    gappa = sym.gaussian_curvature(ambient_dim, dim=dim, dofdesc=dofdesc)

    return -lapf + hessf_dot_n + 2 * kappa * gradf_dot_n + 2 * gappa * f


# }}}


# {{{ curvature vector shape derivatives


def _adjoint_curvature_vector_shape_derivative(ambient_dim, f):
    pass


def adjoint_curvature_vector_shape_derivative(
    ambient_dim, f, *, dim=None, dofdesc=None
):
    pass


def adjoint_curvature_vector_shape_derivative_ambient(
    ambient_dim,
    f,
    *,
    divf,
    gradf_dot_n,
    gradf_dot_t,
    lapf_dot_n,
    hessf_dot_n,
    dim=None,
    dofdesc=None,
):
    n = sym.normal(ambient_dim, dim=dim, dofdesc=dofdesc).as_vector()
    kappa = sym.summed_curvature(ambient_dim, dim=dim, dofdesc=dofdesc)

    if ambient_dim == 2:
        remainder = 2 * kappa * gradf_dot_n - kappa * divf
    elif ambient_dim == 3:
        gradn = sym.extrinsic_shape_operator(ambient_dim, dim=dim, dofdesc=dofdesc)
        t = sym.tangential_onb(ambient_dim, dim=dim, dofdesc=dofdesc).T

        remainder = kappa * divf - sum(
            gradf_dot_t[i] @ (t[i] @ gradn) for i in range(dim)
        )
    else:
        raise ValueError(f"unsupported dimension: {ambient_dim}")

    return -lapf_dot_n + hessf_dot_n - kappa**2 * (f @ n) + remainder


def adjoint_curvature_vector_shape_gradient(
    ambient_dim,
    *,
    f,
    divf,
    gradf_dot_n,
    gradf_dot_t,
    lapf_dot_n,
    hessf_dot_n,
    dim=None,
    dofdesc=None,
):
    if dim is None:
        dim = ambient_dim - 1

    kappa = sym.summed_curvature(ambient_dim, dim=dim, dofdesc=dofdesc)

    if ambient_dim == 2:
        remainder = 3 * kappa * gradf_dot_n - kappa * divf
    elif ambient_dim == 3:
        gradn = sym.extrinsic_shape_operator(ambient_dim, dim=dim, dofdesc=dofdesc)
        t = sym.tangential_onb(ambient_dim, dim=dim, dofdesc=dofdesc).T

        remainder = (
            kappa * divf
            + kappa * gradf_dot_n
            - 2 * sum(gradf_dot_t[i] @ (t[i] @ gradn) for i in range(dim))
        )
    else:
        raise ValueError(f"unsupported dimension: {ambient_dim}")

    return -lapf_dot_n + hessf_dot_n + remainder


# }}}
