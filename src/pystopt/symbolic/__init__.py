# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

import os

import pystopt.symbolic.primitives as sym
from pystopt.symbolic.execution import bind

# {{{


# TODO: This is extremely hacky. Need to figure out a better way to hook into
# the pymbolic / pytential symbolic code


def _monkey_patch_primitives():
    import pytential.symbolic.primitives

    for to_name, from_name in [
        # NOTE: sets all integrals to be computed through the Parseval's Theorem
        ("integral", "sintegral"),
        # NOTE: uses spectral derivatives
        ("parametrization_derivative_matrix", "parametrization_derivative_matrix"),
        # NOTE: some hardcoding for the curve case
        ("tangential_onb", "tangential_onb"),
        # NOTE: uses spectral derivatives
        ("first_fundamental_form", "first_fundamental_form"),
        # NOTE: takes only first derivatives (for SPHARM support)
        ("second_fundamental_form", "second_fundamental_form"),
        # NOTE: takes only first derivatives (for SPHARM support)
        ("shape_operator", "shape_operator"),
        # NOTE: takes only first derivatives (for SPHARM support)
        ("mean_curvature", "mean_curvature"),
    ]:
        setattr(
            pytential.symbolic.primitives,
            f"_monkey_{to_name}",
            getattr(pytential.symbolic.primitives, to_name),
        )
        setattr(pytential.symbolic.primitives, to_name, getattr(sym, from_name))


def _monkey_patch_mappers():
    import pytential.symbolic.mappers

    import pystopt.symbolic.mappers as prim

    for to_name, from_name in [
        ("IdentityMapper", "IdentityMapper"),
        ("Collector", "Collector"),
        ("EvaluationMapper", "EvaluationMapper"),
        ("LocationTagger", "LocationTagger"),
        ("DiscretizationStageTagger", "DiscretizationStageTagger"),
        ("StringifyMapper", "StringifyMapper"),
    ]:
        setattr(
            pytential.symbolic.mappers,
            f"_Monkey{to_name}",
            getattr(pytential.symbolic.mappers, to_name),
        )
        setattr(pytential.symbolic.mappers, to_name, getattr(prim, from_name))


def _monkey_patch_pytential_symbolic():
    import pytential.symbolic.execution

    pytential.symbolic.execution.bind = bind

    _monkey_patch_primitives()
    _monkey_patch_mappers()


PYTENTIAL_NO_MONKEY_PATCH = "PYTENTIAL_NO_MONKEY_PATCH" in os.environ
if not PYTENTIAL_NO_MONKEY_PATCH:
    _monkey_patch_pytential_symbolic()

# }}}

__all__ = (
    "bind",
    "sym",
)
