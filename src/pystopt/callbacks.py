# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

"""
.. autoclass:: OutputType
   :members:

.. autoclass:: OutputField

.. autoclass:: OutputFieldCallable
.. autoclass:: OutputFieldVisualizeCallable

.. autoclass:: CallbackCallable
.. autoclass:: CallbackManager

.. autoclass:: LogCallback
.. autoclass:: OptimizationLogCallback

.. autoclass:: HistoryCallback
.. autoclass:: PerformanceHistoryCallback
.. autoclass:: OptimizationHistoryCallback

.. autoclass:: CheckpointCallback
.. autoclass:: VisualizeCallback
"""

import enum
import pathlib
from collections.abc import Callable, Iterator
from dataclasses import dataclass, field, replace
from typing import Any, Protocol

import numpy as np

from arraycontext import ArrayContext
from pytential import GeometryCollection

from pystopt import sym
from pystopt.checkpoint import IteratorCheckpointManager
from pystopt.measure import TicTocTimer
from pystopt.paths import FileParts, PathLike
from pystopt.tools import get_default_logger
from pystopt.visualization import VisualizerBase

logger = get_default_logger(__name__)


# {{{ output fields


@enum.unique
class OutputType(enum.IntFlag):
    CHECKPOINT = 1
    """Set if the output field should be checkpointed."""
    VISUALIZED = 2
    """Set if the output field should be visualized."""
    GENERIC = CHECKPOINT | VISUALIZED
    """Set if the output field should be both checkpointed and visualized."""


@dataclass(frozen=True)
class OutputField:
    """
    .. attribute:: label

        A name for the field that is used in the output, e.g. as the key name
        in checkpointing or dataset name in visualization.

    .. attribute:: value
    .. attribute:: flag

        A :class:`OutputType` denoting the type of the output field.
    """

    label: str
    value: Any
    flag: OutputType = OutputType.GENERIC

    def __iter__(self):
        return iter((self.label, self.value))

    def __hash__(self):
        return hash((type(self), self.label))

    @classmethod
    def checkpoint(cls, label: str, value: Any) -> "OutputField":
        return cls(label=label, value=value, flag=OutputType.CHECKPOINT)

    @classmethod
    def visualize(cls, label: str, value: Any) -> "OutputField":
        return cls(label=label, value=value, flag=OutputType.VISUALIZED)


def extract_fields(
    fields: dict[str, "OutputField"], names: tuple[str, ...]
) -> list[tuple[str, Any]]:
    return [
        (fields[name].label, fields[name].value) for name in names if name in fields
    ]


class OutputFieldCallable(Protocol):
    """
    .. automethod:: __call__
    """

    def __call__(self, state: Any, **kwargs: Any) -> dict[str, OutputField]: ...


class OutputFieldVisualizeCallable(Protocol):
    """
    .. automethod:: __call__
    """

    def __call__(
        self,
        vis: "VisualizeCallback",
        basename: FileParts,
        state: Any,
        fields: dict[str, OutputField],
        **kwargs: Any,
    ) -> None: ...


# }}}


# {{{ callback interface


def install_signal_handler(callback):
    import signal

    def handler(signum, frame):
        if signum == signal.SIGUSR1:
            callback()

    signal.signal(signal.SIGUSR1, handler)


@dataclass(frozen=True)
class CallbackCallable:
    """Generic callable base class for callbacks.

    .. attribute:: norm

        A :class:`~collections.abc.Callable` that computes the norm of an
        array or array container.

    .. automethod:: restart_from_iteration
    .. automethod:: __call__
    """

    norm: Callable[[Any], float] | None

    def restart_from_iteration(self, n: int) -> None:
        """For callbacks that keep an internal state based on the iteration
        count, this function should return them to the iteration *n*.
        """

    def __call__(self, *args: Any, **kwargs: Any) -> int:
        """
        :returns: `1` if no error was encountered and `0` otherwise.
        """
        raise NotImplementedError(type(self).__name__)


@dataclass
class CallbackManager:
    """A wrapper for various callbacks used during an iterative process.

    The main function of this wrapper is to call all the callbacks in
    :attr:`callbacks` and handle their return values. Any callback can request
    a termination of the iteration by returning :math:`0`. Besides that,
    two other methods are provided to stop the iteration externally:

    * sending the signal ``SIGUSR1`` to the process.
    * creating the file :attr:`stop_file_name`.

    Note that all the termination conditions are checked and all the callbacks
    are called, i.e. they are not shortcircuited once one is found to
    return :math:`0`.

    .. attribute:: stop_file_name

        The name of a file that, when it is found to exist, the iteration
        should be stopped.

    .. attribute:: short_circuit

        If *True*, the return values from the callbacks are shortcircuited.
        This means that, if a callback returns :math:`0`, no subsequent
        callback will be called. The :attr:`stop_file_name` and signal
        are checked first.

    .. attribute:: callbacks

        Additional user provided callbacks.

    .. automethod:: restart_from_iteration
    .. automethod:: __call__
    """

    stop_file_name: pathlib.Path | None = None
    callbacks: dict[str, CallbackCallable] = field(default_factory=dict, repr=False)

    short_circuit: bool = False

    _force_gc: bool = True
    _ncalls: int = field(default=0, init=False, repr=False)
    _stop_signal: int = field(default=1, init=False, repr=False)

    def __post_init__(self) -> None:
        if not isinstance(self.callbacks, dict):
            raise TypeError("'callbacks' should be a dictionary.")

        # {{{ signal handler

        def callback() -> None:
            self._stop_signal = 0

        install_signal_handler(callback)

        # }}}

        # {{{ stop file

        if self.stop_file_name is not None:
            self.stop_file_name = pathlib.Path(self.stop_file_name)
            if self.stop_file_name.exists():
                self.stop_file_name.unlink()

        # }}}

    def restart_from_iteration(self, ncalls: int) -> None:
        for cb in self.callbacks.values():
            cb.restart_from_iteration(ncalls)

        object.__setattr__(self, "_ncalls", ncalls)

    def __contains__(self, key: str) -> bool:
        return key in self.callbacks

    def __getitem__(self, key: str) -> CallbackCallable:
        return self.callbacks[key]

    def __setitem__(self, key: str, value: CallbackCallable) -> None:
        assert isinstance(value, CallbackCallable)
        self.callbacks[key] = value

    def _run_callbacks(self, *args: Any, **kwargs: Any) -> int:
        from pystopt.tools import c

        ret = self._stop_signal
        if self.short_circuit and ret == 0:
            logger.info(c.wrap("Stopped by signal 'SIGUSR1'", c.LightRed))

            return ret

        if self.stop_file_name is not None and self.stop_file_name.exists():
            from pystopt.paths import relative_to

            logger.info(
                c.wrap("Stopped by stop file: '%s'", c.LightRed),
                relative_to(self.stop_file_name),
            )

            self.stop_file_name.unlink()
            ret = 0

            if self.short_circuit:
                return ret

        retc = 1
        for name, callback in self.callbacks.items():
            retc = retc & callback(*args, ncalls=self._ncalls - 1, **kwargs)

            if self.short_circuit and retc == 0:
                logger.info(c.wrap("Stopped by callback '%s'", c.LightRed), name)
                return retc

        if retc == 0:
            logger.info(c.wrap("Stopped by callback", c.LightRed))

        return ret & retc

    def get_output_field_getters(self) -> tuple[OutputFieldCallable, ...]:
        r"""
        :returns: a :class:`tuple` of callables that return a mapping of
            :class:`OutputField`\ s. Subclasses should customize this for
            their needs.
        """
        return ()

    def __call__(self, *args: Any, **kwargs: Any) -> int:
        """Calls all the callbacks in :attr:`~pystopt.callbacks.CallbackManager.callbacks`
        and aggregates their return values.

        In addition to the input arguments, the manager also inserts the
        keyword *ncalls*, with the number of calls to this class.

        :returns: :math:`0` to stop the optimization and another integer
            otherwise.
        """  # noqa: E501
        # {{{ gather output fields

        (state,) = args
        fields = {}
        labels = set()

        from pystopt.visualization.matplotlib import preprocess_latex

        for func in self.get_output_field_getters():
            new_fields = func(state, **kwargs)

            for name, f in new_fields.items():
                if name in fields:
                    raise KeyError(
                        f"field '{name}' already in output: second appearance "
                        f" in '{func}' outputs"
                    )

                label = f.label
                if state.wrangler.ambient_dim == 3:
                    label = preprocess_latex(label, disable_latex=True)

                if label in labels:
                    raise ValueError(
                        f"field '{name}' label '{label}' already in output: "
                        f"second appearance in '{func}' outputs"
                    )

                labels = labels | {label}

            fields.update(new_fields)

        # }}}

        self._ncalls += 1
        ret = self._run_callbacks(*args, fields=fields, **kwargs)

        if self._ncalls % 10 == 0 and self._force_gc:
            # NOTE: this is usually called at the end of an iteration of some
            # sort, when we throw away all the geometry and caches associated
            # with it, so it's a good time to flush the memory too

            # FIXME: this is a hack because the memory was growing quite a bit.
            # It looks like a bug somewhere in the stack, see
            #   https://github.com/inducer/pytential/issues/59
            # but it could also just be the OS not releasing memory unprompted.

            from pystopt.tools import gc_collect

            gc_collect()

        return ret


# }}}


# {{{ log


@dataclass(frozen=True)
class LogCallback(CallbackCallable):
    """
    .. attribute:: log

        User-provided logging function that takes the same arguments as
        :meth:`logging.Logger.info` and friends using the old school percent-based
        syntax for string formatting.
    """

    log: Callable[..., Any] | None


@dataclass(frozen=True)
class OptimizationLogCallback(LogCallback):
    _pt: TicTocTimer | None = field(default=None, init=False, repr=False)
    _f_prev: float = field(default=np.inf, init=False, repr=False)
    _g_prev: float = field(default=np.inf, init=False, repr=False)

    def __call__(self, *args: Any, **kwargs: Any) -> int:
        (state,) = args
        info = kwargs.get("info")
        if info is None:
            return 1

        if self.log is None:
            return 1

        if self._pt is None:
            object.__setattr__(self, "_pt", TicTocTimer())
            self._pt.tic()

        self._pt.toc()
        actx = state.array_context

        it = kwargs.get("ncalls")
        if it is None:
            it = info.it
        else:
            assert it == info.it

        from pystopt.tools import c

        if self.norm is None:
            s_gnorm = s_dnorm = "<unknown>"
        else:
            gnorm = actx.to_numpy(self.norm(info.g))
            s_gnorm = c.message(f"{gnorm:.7e}", success=gnorm < self._g_prev)

            dnorm = gnorm if info.d is None else actx.to_numpy(self.norm(info.d))
            s_dnorm = c.message(f"{dnorm:.7e}", success=gnorm < self._g_prev)

            object.__setattr__(self, "_g_prev", gnorm)
        object.__setattr__(self, "_f_prev", info.f)

        self.log(
            "[%8d] f %s |g| %s |d| %s alpha %.7e (%s)",
            it,
            c.message(f"{info.f:.7e}", success=info.f < self._f_prev),
            s_gnorm,
            s_dnorm,
            info.alpha,
            c.wrap(self._pt, c.Cyan),
        )

        object.__setattr__(self, "_f_prev", info.f)
        object.__setattr__(self, "_g_prev", gnorm)

        self._pt.tic()

        return 1


# }}}


# {{{ history


@dataclass(frozen=True)
class HistoryCallback(CallbackCallable):
    """A callable that collects history for various variables.

    All fields should be lists or support the `append` method.

    .. automethod:: to_numpy
    .. automethod:: from_numpy
    """

    def to_numpy(self) -> dict[str, np.ndarray]:
        r"""
        :returns: a :class:`dict` with the history as :class:`numpy.ndarray`\ s.
        """

        from pystopt.tools import dc_items

        return {
            k: np.asarray(v)
            for k, v in dc_items(self, init_only=False)
            if isinstance(v, list)
        }

    def from_numpy(self, history: dict[str, np.ndarray]) -> None:
        """Takes the output of :meth:`to_numpy` and appends it to the current
        histories in the callback.
        """
        # copy over histories
        for key, values in history.items():
            if not hasattr(self, key):
                raise KeyError(f"{type(self).__name__} has no attribute '{key}'")

            getattr(self, key).extend(values)

        # make sure all the entries have the same number of histories
        from pytools import is_single_valued

        from pystopt.tools import dc_items

        if not is_single_valued(
            len(v) for _, v in dc_items(self, init_only=False) if isinstance(v, list)
        ):
            raise ValueError("not all the provided histories have the same length")

    def restart_from_iteration(self, n: int) -> None:
        """Only keep the first *nmax* items in the history.

        :param nmax: a positive integer.
        """
        assert n > 0

        from pystopt.tools import dc_items

        for k, v in dc_items(self, init_only=False):
            if not isinstance(v, list):
                continue

            object.__setattr__(self, k, v[:n])


@dataclass(frozen=True)
class PerformanceHistoryCallback(HistoryCallback):
    """Gather basic performance data using ``psutil``.

    .. attribute:: memory

        Memory used at the time of calling in ``Mib``.

    .. attribute:: timestamp

        Process time at the time of calling.
    """

    memory: list[int] = field(default_factory=list, repr=False)
    timestamp: list[int] = field(default_factory=list, repr=False)

    process: Any = field(default=None, init=False, repr=False)

    def __call__(self, *args: Any, **kwargs: Any) -> int:
        try:
            import psutil
        except ImportError:
            from warnings import warn

            warn("'psutil' library is not available", stacklevel=2)
            return 1

        if self.process is None:
            import os

            object.__setattr__(self, "process", psutil.Process(os.getpid()))

        # NOTE: store memory in Mib (mebibytes)
        self.memory.append(self.process.memory_info().rss // 1024**2)

        import time

        self.timestamp.append(time.process_time_ns())

        return 1


@dataclass(frozen=True)
class OptimizationHistoryCallback(PerformanceHistoryCallback):
    """
    .. attribute:: f

        Cost functional values at each iteration.

    .. attribute:: g

        Norm of the cost gradient at each iteration, computed using
        :attr:`~CallbackCallable.norm`.

    .. attribute:: d

        Norm of the descent direction used in the gradient descent algorithm,
        computed using :attr:`~CallbackCallable.norm`. This is not the same as
        :attr:`g` if, e.g., a nonlinear conjugate gradient method is used.

    .. attribute:: alpha

        Gradient descent step size at each iteration.
    """

    f: list[float] = field(default_factory=list, repr=False)
    g: list[float] = field(default_factory=list, repr=False)
    d: list[float] = field(default_factory=list, repr=False)
    alpha: list[float] = field(default_factory=list, repr=False)

    def __call__(self, *args: Any, **kwargs: Any) -> int:
        r = super().__call__(*args, **kwargs)
        if "info" not in kwargs:
            raise ValueError("missing keyword argument 'info'")

        (state,) = args
        info = kwargs["info"]
        actx = state.array_context

        self.alpha.append(info.alpha)
        self.f.append(info.f)
        self.g.append(actx.to_numpy(self.norm(info.g)))

        if info.d is not None:
            self.d.append(actx.to_numpy(self.norm(info.d)))

        return r


# }}}


# {{{ checkpoint


@dataclass(frozen=True)
class CheckpointCallback(CallbackCallable):
    checkpoint: IteratorCheckpointManager
    overwrite: bool = False

    def restart_from_iteration(self, n: int) -> None:
        self.checkpoint.advance(n + 1)

    def __call__(self, *args: Any, **kwargs: Any) -> int:
        (state,) = args
        names_and_fields = {
            # NOTE: this uses name instead of f.label on purpose!
            name: f.value
            for name, f in kwargs["fields"].items()
            if f.flag & OutputType.CHECKPOINT
        }

        if names_and_fields:
            actx = state.array_context
            assert actx is not None, "'state' is frozen"

            from meshmode.discretization import Discretization

            frozen_names_and_fields = {}
            for name, f in names_and_fields.items():
                frozen_f = f
                if not (np.isscalar(f) or isinstance(f, Discretization)):
                    frozen_f = actx.freeze(f)

                frozen_names_and_fields[name] = frozen_f

            self.checkpoint.write(frozen_names_and_fields, overwrite=self.overwrite)

        return 1


# }}}


# {{{ visualize


def default_visualize_writer(
    vis: "VisualizeCallback",
    basename: FileParts,
    state: Any,
    fields: dict[str, OutputField],
    **kwargs: Any,
) -> None:
    from meshmode.dof_array import (
        DOFArray,
        InconsistentDOFArray,
        check_dofarray_against_discr,
    )

    xd = None

    names_and_fields = []
    discr = vis.from_discr
    for label, f in fields.items():
        if label == "x0":
            xd = f
            continue

        try:
            if isinstance(f.value, np.ndarray):
                for subary in f.value:
                    if isinstance(subary, DOFArray):
                        check_dofarray_against_discr(discr, subary)
            else:
                check_dofarray_against_discr(discr, f.value)

            names_and_fields.append((f.label, f.value))
        except InconsistentDOFArray:
            from warnings import warn

            warn(
                f"field '{f.label}' is not consistent with the visualization "
                "discretization and is skipped!",
                stacklevel=2,
            )
        except TypeError:
            from pystopt.dof_array import SpectralDOFArray

            assert isinstance(f.value, SpectralDOFArray)

    vis.write_file(basename, names_and_fields)

    if xd is not None:
        # FIXME: this is super hacky
        wrangler = getattr(state.wrangler, "forward", state.wrangler)
        cost = getattr(wrangler, "cost", None)

        from pystopt.cost import GeometryTrackingFunctional

        if isinstance(cost, GeometryTrackingFunctional):
            dofdesc = cost.dofdesc
            orig_discr_d = wrangler.places.get_discretization(
                dofdesc.geometry, dofdesc.discr_stage
            )

            from pystopt.simulation.state import reconstruct_discretization

            discr_d = reconstruct_discretization(
                state.array_context, orig_discr_d, xd.value
            )

            from pystopt.visualization import make_same_connectivity_visualizer

            vis = make_same_connectivity_visualizer(
                state.array_context, vis.vis, discr_d
            )
            vis.write_file(basename.with_suffix("geometry_desired"), [])


def matplotlib_visualize_geometry_writer(
    vis: "VisualizeCallback",
    basename: FileParts,
    state: Any,
    fields: dict[str, OutputField],
    **kwargs: Any,
) -> None:
    visualize_kwargs = vis.get_geometry_kwargs(state)
    visualize_kwargs.update({"write_mode": "geometry"})

    filename = basename.with_suffix("geometry")
    vis.write_file(filename, extract_fields(fields, ("x0",)), **visualize_kwargs)


@dataclass(frozen=True)
class VisualizeCallback(CallbackCallable):
    """
    .. attribute:: ambient_dim
    .. attribute:: from_discr
    .. automethod:: write_file

    .. automethod:: get_file_writers
    .. automethod:: __call__
    """

    visualize_file_series: Iterator[FileParts]
    overwrite: bool = False
    frequency: int = 1

    vis: VisualizerBase | None = field(default=None, init=False, repr=False)

    # NOTE: these are stored mostly so that the callback can be restarted
    _nwrites: int = field(default=0, init=False, repr=False)
    _previous_filename: FileParts | None = field(default=None, init=False, repr=False)

    @property
    def from_discr(self):
        return self.vis.from_discr

    @property
    def ambient_dim(self):
        return self.from_discr.ambient_dim

    def get_geometry_kwargs(self, state: Any) -> dict[str, Any]:
        return {"aspect": "equal", "show_vertices": True}

    def write_file(self, *args, **kwargs) -> None:
        self.vis.write_file(*args, overwrite=self.overwrite, **kwargs)

    def _update_vis(
        self,
        actx: ArrayContext,
        places: GeometryCollection,
        dofdesc: sym.DOFDescriptor,
    ) -> None:
        discr = places.get_discretization(dofdesc.geometry, dofdesc.discr_stage)

        if self.vis is None:
            from pystopt.visualization import make_visualizer

            vis = make_visualizer(actx, discr)
        else:
            from pystopt.visualization import make_same_connectivity_visualizer

            vis = make_same_connectivity_visualizer(actx, self.vis, discr)

        object.__setattr__(self, "vis", vis)

    def restart_from_iteration(self, n: int) -> None:
        nwrites = max(0, n // self.frequency)
        filename = next(self.visualize_file_series)

        from pystopt.paths import generate_filename_series

        visualize_file_series = generate_filename_series(
            stem=filename.stem,
            suffix=filename.suffix,
            ext=filename.ext,
            cwd=filename.cwd,
            start=nwrites,
        )

        object.__setattr__(self, "visualize_file_series", visualize_file_series)
        object.__setattr__(self, "_nwrites", nwrites)

    def get_file_writers(self) -> tuple[OutputFieldVisualizeCallable, ...]:
        """
        :returns: a :class:`tuple` of callables used to do the actual
            writing. Subclasses should customize this for their needs.
        """
        if self.from_discr.ambient_dim == 2:
            return (matplotlib_visualize_geometry_writer,)
        else:
            return (default_visualize_writer,)

    def __call__(self, *args: Any, **kwargs: Any) -> int:
        ncalls = kwargs.get("ncalls")
        if ncalls is not None and ncalls % self.frequency != 0:
            return 1

        if "fields" not in kwargs:
            return 1

        (state,) = args
        try:
            places = state.get_geometry_collection()
        except AttributeError as exc:
            raise TypeError(
                f"cannot visualize states of type '{type(state).__name__}'"
            ) from exc

        fields = kwargs.pop("fields")
        visualized_fields = {
            name: f for name, f in fields.items() if f.flag & OutputType.VISUALIZED
        }

        actx = state.array_context
        assert actx is not None, "'state' is frozen"

        self._update_vis(actx, places, state.wrangler.dofdesc)
        basename = next(self.visualize_file_series)

        for func in self.get_file_writers():
            func(self, basename, state, fields=visualized_fields, **kwargs)

        object.__setattr__(self, "_previous_filename", basename)
        object.__setattr__(self, "_nwrites", self._nwrites + 1)

        return 1


# }}}


# {{{ default callback


def _make_default_callback(
    checkpoint_file_name: PathLike | None,
    *,
    manager_factory: Any | None = None,
    checkpoint_callback_factory: bool | CheckpointCallback | None = None,
    history_callback_factory: bool | Any | None = None,
    visualize_callback_factory: bool | Any | None = None,
    log_callback_factory: bool | Any | None = None,
    extra_callbacks: dict[str, Any] | None = None,
    norm: Callable[[Any, int], Any] | None = None,
    log: Callable[..., None] | None = None,
    restart_file_name: PathLike | None = None,
    from_restart: bool | int = False,
    prefix: str | None = None,
    overwrite: bool = False,
) -> CallbackManager:
    # {{{ setup
    if manager_factory in {True, None}:
        manager_factory = CallbackManager

    if checkpoint_callback_factory in {True, None}:
        checkpoint_callback_factory = CheckpointCallback

    if history_callback_factory is None:
        history_callback_factory = False

    if visualize_callback_factory in {True, None}:
        visualize_callback_factory = VisualizeCallback

    if log_callback_factory is None:
        log_callback_factory = False

    if not isinstance(from_restart, bool) and from_restart == 0:
        raise ValueError(f"'from_restart' must be > 0: got '{from_restart}'")

    checkpoint = None
    if checkpoint_file_name is not None:
        checkpoint_file_name = pathlib.Path(checkpoint_file_name)
        dirname = checkpoint_file_name.parent

        if checkpoint_callback_factory is not False:
            from pystopt.checkpoint import make_hdf_checkpoint_manager

            checkpoint = make_hdf_checkpoint_manager(checkpoint_file_name)
    else:
        if from_restart:
            raise RuntimeError("cannot restart when no checkpoint file is given")

        checkpoint_callback_factory = False

    if log is None:
        from pystopt.optimize.steepest import logger as cg_logger

        log = cg_logger.info

    if from_restart and "result" not in checkpoint:
        from_restart = False

    if from_restart and restart_file_name is None:
        restart_file_name = checkpoint_file_name

    # }}}

    def _issubclass(cls: Any, base: type) -> bool:
        return isinstance(cls, type) and issubclass(cls, base)

    # {{{ manager

    if from_restart:
        from pystopt.checkpoint import make_hdf_checkpoint_manager

        with make_hdf_checkpoint_manager(restart_file_name) as chk:
            cm = chk.read_from("callback")
            assert isinstance(cm, CallbackManager)

            if not isinstance(from_restart, bool):
                cm.restart_from_iteration(from_restart)
    else:
        callbacks = {}

        if extra_callbacks is not None:
            if callable(extra_callbacks):
                callbacks["user"] = extra_callbacks
            elif isinstance(extra_callbacks, list | tuple):
                callbacks.update({
                    f"user_{i}": cb for i, cb in enumerate(extra_callbacks)
                })
            elif isinstance(extra_callbacks, dict):
                callbacks.update(extra_callbacks)
            elif isinstance(extra_callbacks, bool):
                pass
            else:
                raise TypeError(
                    "'extra_callbacks' has unknown type: "
                    f"'{type(extra_callbacks).__name__}'"
                )

        if checkpoint_file_name is not None:
            from pystopt.paths import get_filename

            stop_file_name = get_filename("stop", ext="me", cwd=dirname).aspath()
        else:
            stop_file_name = None

        if isinstance(manager_factory, type) and issubclass(
            manager_factory, CallbackManager
        ):
            cm = manager_factory(
                stop_file_name=stop_file_name,
                callbacks=callbacks,
            )
        else:
            raise TypeError(f"unknown 'manager_factory': {manager_factory}")

    # }}}

    # {{{ log

    if "log" in cm:
        assert from_restart
        assert isinstance(cm["log"], LogCallback)
        cm["log"] = replace(cm["log"], norm=norm, log=log)
    elif isinstance(log_callback_factory, type) and issubclass(
        log_callback_factory, LogCallback
    ):
        cm["log"] = log_callback_factory(norm=norm, log=log)
    elif isinstance(log_callback_factory, LogCallback):
        cm["log"] = log_callback_factory
    elif log_callback_factory is False:
        pass
    else:
        raise TypeError(f"unknown 'log_callback_factory': {log_callback_factory}")

    # }}}

    # {{{ checkpoint

    if "checkpoint" in cm:
        assert from_restart
        assert isinstance(cm["checkpoint"], CheckpointCallback)
    elif isinstance(checkpoint_callback_factory, type) and issubclass(
        checkpoint_callback_factory, CheckpointCallback
    ):
        cm["checkpoint"] = checkpoint_callback_factory(
            norm=norm, checkpoint=checkpoint, overwrite=overwrite
        )
    elif isinstance(checkpoint_callback_factory, CheckpointCallback):
        cm["checkpoint"] = checkpoint_callback_factory
    elif checkpoint_callback_factory is False:
        pass
    else:
        raise TypeError(
            f"unknown 'checkpoint_callback_factory': {checkpoint_callback_factory}"
        )

    # }}}

    # {{{ history

    if "history" in cm:
        assert from_restart
        assert isinstance(cm["history"], HistoryCallback)

        cm["history"] = replace(cm["history"], norm=norm)
    elif isinstance(history_callback_factory, type) and issubclass(
        history_callback_factory, HistoryCallback
    ):
        cm["history"] = history_callback_factory(norm=norm)
    elif isinstance(history_callback_factory, HistoryCallback):
        cm["history"] = history_callback_factory
    elif history_callback_factory is False:
        pass
    else:
        raise TypeError(
            f"unknown 'history_callback_factory': {history_callback_factory}"
        )

    # }}}

    # {{{ visualization

    if "visualize" in cm:
        assert from_restart
        assert isinstance(cm["visualize"], VisualizeCallback)
    elif isinstance(visualize_callback_factory, type) and issubclass(
        visualize_callback_factory, VisualizeCallback
    ):
        from pystopt.paths import generate_filename_series

        if prefix is None:
            visualize_file_series = generate_filename_series("visualize", cwd=dirname)
        else:
            visualize_file_series = generate_filename_series(
                f"{prefix}_visualize", cwd=dirname
            )

        cm["visualize"] = visualize_callback_factory(
            norm=norm,
            visualize_file_series=visualize_file_series,
            overwrite=overwrite,
        )
    elif isinstance(visualize_callback_factory, VisualizeCallback):
        cm["visualize"] = visualize_callback_factory
    elif visualize_callback_factory is False:
        pass
    else:
        raise TypeError(
            f"unknown 'visualize_callback_factory': {visualize_callback_factory}"
        )

    # }}}

    return cm


# }}}


# {{{ make_default_optimize_callback


def make_default_optimize_callback(
    *,
    callback: bool | Any = True,
    log_callback_factory: Any | None = None,
    manager_factory: Any | None = None,
    norm: Callable[[Any, int], Any] | None = None,
    log: Callable[..., None] | None = None,
) -> CallbackManager:
    if isinstance(callback, CallbackManager):
        return callback

    if manager_factory is None:
        manager_factory = CallbackManager

    if log_callback_factory is None:
        from pystopt.optimize.cg_utils import CGLogCallback

        log_callback_factory = CGLogCallback

    if callback is False:
        log_callback_factory = False

    return _make_default_callback(
        None,
        manager_factory=manager_factory,
        checkpoint_callback_factory=False,
        history_callback_factory=False,
        visualize_callback_factory=False,
        log_callback_factory=log_callback_factory,
        extra_callbacks=callback,
        norm=norm,
        log=log,
        from_restart=False,
        overwrite=False,
    )


# }}}


# {{{ make_default_evolution_callback


def make_default_evolution_callback(
    checkpoint_file_name: PathLike,
    *,
    manager_factory: Any | None = None,
    checkpoint_callback_factory: bool | Any | None = None,
    history_callback_factory: bool | Any | None = None,
    visualize_callback_factory: bool | Any | None = None,
    norm: Callable[[Any, int], Any] | None = None,
    log: Callable[..., None] | None = None,
    from_restart: bool = False,
    restart_file_name: PathLike | None = None,
    prefix: str | None = None,
    overwrite: bool = False,
) -> CallbackManager:
    """Create a set of callbacks for unsteady evolution simulations.

    The factories take the arguments of the base classes define in this module.
    For example, the *manager_factory* takes the same arguments as
    :class:`CallbackManager`. The user is responsible for wrapping these
    functions for custom use.

    If any of the factories are set to *False*, then the particular callback
    is not created.

    :arg checkpoint_file_name: a file path that is used for checkpointing
        and other output. The parent directory of this file will contain
        all the output from the simulation.
    :arg from_restart: if *True* and *checkpoint_file_name* exists and contains
        previous checkpoint data, the various callbacks attempt to roll their
        state to match the state in the file.
    :arg overwrite: passed on the *checkpoint_callback_factory* and
        *visualize_callback_factory* to force overwriting existing files.
    """
    if history_callback_factory is None:
        from pystopt.simulation.unsteady import StokesEvolutionHistoryCallback

        history_callback_factory = StokesEvolutionHistoryCallback

    return _make_default_callback(
        checkpoint_file_name,
        manager_factory=manager_factory,
        checkpoint_callback_factory=checkpoint_callback_factory,
        history_callback_factory=history_callback_factory,
        visualize_callback_factory=visualize_callback_factory,
        log_callback_factory=False,
        norm=norm,
        log=log,
        from_restart=from_restart,
        restart_file_name=restart_file_name,
        prefix=prefix,
        overwrite=overwrite,
    )


# }}}


# {{{ make_default_shape_callback


def make_default_shape_callback(
    checkpoint_file_name: PathLike,
    *,
    manager_factory: Any | None = None,
    checkpoint_callback_factory: bool | Any | None = None,
    history_callback_factory: bool | Any | None = None,
    visualize_callback_factory: bool | Any | None = None,
    log_callback_factory: bool | Any | None = None,
    norm: Callable[[Any, int], Any] | None = None,
    log: Callable[..., None] | None = None,
    from_restart: bool | int = False,
    restart_file_name: PathLike | None = None,
    overwrite: bool = False,
) -> CallbackManager:
    if history_callback_factory is None:
        history_callback_factory = OptimizationHistoryCallback

    if log_callback_factory is None:
        log_callback_factory = OptimizationLogCallback

    return _make_default_callback(
        checkpoint_file_name,
        manager_factory=manager_factory,
        checkpoint_callback_factory=checkpoint_callback_factory,
        history_callback_factory=history_callback_factory,
        visualize_callback_factory=visualize_callback_factory,
        log_callback_factory=log_callback_factory,
        norm=norm,
        log=log,
        from_restart=from_restart,
        restart_file_name=restart_file_name,
        overwrite=overwrite,
    )


# }}}
