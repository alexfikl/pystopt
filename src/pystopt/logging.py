# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

import logging
from collections.abc import Sequence
from typing import Any

import numpy as np


def on_ci() -> bool:
    """
    :returns: *True* if the current running system is recognized as a CI.
    """
    import os

    is_gitlab = os.environ.get("CI_PROJECT_DIR") is not None

    return is_gitlab


def afmt(fmt: str, ary: Sequence[Any], prec="%.12e") -> str:
    return fmt.replace("%ary", (f" {prec}" * len(ary)).strip())


def get_environ_log_levels():
    import os

    for level in ("DEBUG", "INFO", "WARNING", "ERROR", "CRITICAL"):
        logs = os.getenv(f"PYSTOPT_LOG_{level}")
        if logs is not None:
            yield level, np.unique(logs.split(":"))


def get_level_from_environment(module):
    for level, level_modules in get_environ_log_levels():
        if module in level_modules:
            return getattr(logging, level)

    return None


def get_rich_handler(level: int):
    try:
        from rich.highlighter import ReprHighlighter
        from rich.logging import RichHandler
    except ImportError:
        # NOTE: rich is vendored by pip since November 2021
        try:
            from pip._vendor.rich.highlighter import ReprHighlighter
            from pip._vendor.rich.logging import RichHandler
        except ImportError:
            return None

    class Highlighter(ReprHighlighter):
        def highlight(self, text):
            # NOTE: do not highlight blocks that already have colors in them
            # since that seems to mostly just really confuse the highlighter
            if text.spans:
                return text
            else:
                return super().highlight(text)

    return RichHandler(
        level=level,
        show_time=True,
        show_level=True,
        show_path=True,
        omit_repeated_times=False,
        markup=True,
        highlighter=Highlighter(),
    )


def get_default_logger(module=None, level=None):
    import os

    if module is None:
        import inspect

        module = os.path.basename(inspect.stack()[1].filename)

    if os.path.exists(module):
        module = os.path.basename(module)
        module = os.path.splitext(module)[0].replace(os.sep, ".")

    env_level = get_level_from_environment(module)
    if env_level is not None:
        level = env_level

    if level is None:
        if on_ci():
            level = logging.ERROR
        else:
            level = logging.INFO

    if isinstance(level, str):
        level = getattr(logging, level.upper())

    logger = logging.getLogger(module)
    logger.setLevel(level)

    handler = get_rich_handler(level)
    if handler:
        logger.addHandler(handler)

    try:
        from warnings import filterwarnings

        from pyopencl import CompilerWarning

        filterwarnings(
            "ignore", module="pyopencl", category=CompilerWarning, lineno=248
        )
    except AssertionError:
        # NOTE: pyopencl is mocked on readthedocs and it doesn't like that
        # it doesn't quite exist, so it raises some errors
        pass

    return logger


# }}}
