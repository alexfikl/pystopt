# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

"""
.. autoclass:: RepresentationInterface
.. autofunction:: make_sym_density
.. autofunction:: make_sym_operator
.. autofunction:: prepare_sym_rhs

.. autoclass:: BeltramiRepresentation
.. autoclass:: LaplaceBeltramiRepresentation
.. autoclass:: YukawaBeltramiRepresentation

.. autoclass:: ScalarNeumannRepresentation
.. autoclass:: LaplaceNeumannRepresentation
.. autoclass:: YukawaNeumannRepresentation
"""

from functools import singledispatch
from typing import Any

import numpy as np

from arraycontext import ArrayContext
from meshmode.dof_array import DOFArray
from pytential import GeometryCollection
from pytential.symbolic.pde.beltrami import BeltramiOperator
from sumpy.kernel import Kernel, LaplaceKernel, YukawaKernel

from pystopt import sym

SymbolicVariableType = sym.ArithmeticExpression | np.ndarray
SymbolicOperatorType = sym.ArithmeticExpression | np.ndarray


# {{{ representation interface


class RepresentationInterface:
    r"""A generic interface for a boundary integral-based representation of
    a PDE. It is usually given as

    .. math::

        (\mathcal{I} + \mathcal{K})[q] = b,

    where :math:`\mathcal{I}` is usually an algebraic linear operator,
    :math:`\mathcal{K}` is the layer potential (a compact linear operator)
    and :math:`b` is the right-hand side. Solving this equation gives the
    density and solves the PDE.

    .. attribute:: ambient_dim
    .. attribute:: kernel_arguments

    .. automethod:: __init__
    """

    def __init__(
        self, ambient_dim: int, kernel_arguments: dict[str, Any] | None = None
    ) -> None:
        if kernel_arguments is None:
            kernel_arguments = {}

        self.ambient_dim = ambient_dim
        self.kernel_arguments = kernel_arguments

    @property
    def dtype(self):
        return np.dtype(np.float64)


@singledispatch
def make_sym_density(
    self: RepresentationInterface, name: str = "sigma"
) -> SymbolicVariableType:
    """
    :returns: a symbolic expression for the density with the given name, depending
        on the dimensions of the representation.
    """
    raise NotImplementedError(type(self).__name__)


@singledispatch
def make_sym_operator(
    self: RepresentationInterface,
    density: SymbolicVariableType,
    *args: Any,
    **kwargs: Any,
) -> SymbolicOperatorType:
    """
    :returns: a symbolic expression for a boundary integral operator applied
        to the given density.
    """
    raise NotImplementedError(type(self).__name__)


@singledispatch
def prepare_sym_rhs(
    self: RepresentationInterface, b: SymbolicVariableType, *args: Any, **kwargs: Any
) -> SymbolicVariableType:
    """
    :returns: a modified right-hand side *b* that conforms to the integral
        operator representation.
    """
    return b


@singledispatch
def prepare_sym_solution(
    self: RepresentationInterface,
    density: SymbolicVariableType,
    *args: Any,
    **kwargs: Any,
) -> SymbolicVariableType:
    """
    :returns: a modified solution *density* that conforms to the integral
        operator representation.
    """
    return density


# }}}


# {{{ Beltrami representations


class BeltramiRepresentation(RepresentationInterface):
    r"""Boundary integral representation of a Beltrami-type surface operator.
    The theorerical background for this type of representation can be found
    in [ONeil2017]_.

    .. [ONeil2017] M. O'Neil, *Second-Kind Integral Equations for the
        Laplace-Beltrami Problem on Surfaces in Three Dimensions*,
        2017,
        `arXiv <https://arxiv.org/abs/1705.00069v2>`__.

    .. automethod:: __init__
    """

    def __init__(self, op: BeltramiOperator) -> None:
        """
        :arg precond: a string identifier for the type of preconditioning
            applied to the operator (as described in [ONeil2017]_). Allowed
            values are ``"left"`` and ``"right"``.
        """
        super().__init__(op.ambient_dim, kernel_arguments=op.kernel_arguments)
        self.op = op

    @property
    def dim(self):
        return self.op.dim

    @property
    def kernel(self):
        return self.op.kernel

    @property
    def dtype(self):
        return np.dtype(np.complex128 if self.kernel.is_complex_valued else np.float64)


class LaplaceBeltramiRepresentation(BeltramiRepresentation):
    def __init__(
        self,
        ambient_dim: int,
        *,
        dim: int | None = None,
        precond: str | None = None,
    ) -> None:
        from pytential.symbolic.pde.beltrami import LaplaceBeltramiOperator

        op = LaplaceBeltramiOperator(ambient_dim, dim=dim, precond=precond)

        super().__init__(op)


class YukawaBeltramiRepresentation(BeltramiRepresentation):
    def __init__(
        self,
        ambient_dim: int,
        *,
        dim: int | None = None,
        precond: str | None = None,
        yukawa_lambda_name: str = "k",
    ) -> None:
        from pytential.symbolic.pde.beltrami import YukawaBeltramiOperator

        op = YukawaBeltramiOperator(
            ambient_dim, dim=dim, precond=precond, yukawa_k_name=yukawa_lambda_name
        )

        super().__init__(op)


@make_sym_density.register(BeltramiRepresentation)
def _make_sym_density_beltrami(
    self: BeltramiRepresentation, name: str = "sigma"
) -> SymbolicVariableType:
    return sym.var(name)


@prepare_sym_rhs.register(BeltramiRepresentation)
def _prepare_sym_rhs_beltrami(
    self: BeltramiRepresentation, b: SymbolicVariableType
) -> SymbolicVariableType:
    return self.op.prepare_rhs(b)


@prepare_sym_solution.register(BeltramiRepresentation)
def _prepare_sym_solution_beltrami(
    self: BeltramiRepresentation, sigma: SymbolicVariableType
) -> SymbolicVariableType:
    return self.op.prepare_solution(sigma)


@make_sym_operator.register(BeltramiRepresentation)
def _make_sym_operator_beltrami(
    self: BeltramiRepresentation, sigma: SymbolicVariableType
) -> SymbolicOperatorType:
    return self.op.operator(sigma)


# }}}


# {{{ scalar pdes


class ScalarNeumannRepresentation(RepresentationInterface):
    def __init__(
        self,
        kernel: Kernel,
        *,
        sign: int = +1,
        kernel_arguments: dict[str, Any] | None = None,
        use_l2_weighting: bool = True,
        use_improved_operator: bool = False,
    ) -> None:
        super().__init__(kernel.dim, kernel_arguments)

        from pytential.symbolic.pde.scalar import NeumannOperator

        self.op = NeumannOperator(
            kernel,
            loc_sign=sign,
            kernel_arguments=kernel_arguments,
            use_l2_weighting=use_l2_weighting,
            use_improved_operator=use_improved_operator,
        )

    @property
    def kernel(self):
        return self.op.kernel

    @property
    def dtype(self):
        return np.dtype(np.complex128 if self.kernel.is_complex_valued else np.float64)


class LaplaceNeumannRepresentation(ScalarNeumannRepresentation):
    def __init__(
        self,
        ambient_dim: int,
        *,
        sign: int = +1,
        use_l2_weighting: bool = True,
    ) -> None:
        kernel = LaplaceKernel(ambient_dim)
        super().__init__(
            kernel,
            sign=sign,
            kernel_arguments={},
            use_l2_weighting=use_l2_weighting,
            use_improved_operator=False,
        )


class YukawaNeumannRepresentation(ScalarNeumannRepresentation):
    def __init__(
        self,
        ambient_dim: int,
        *,
        sign: int = +1,
        use_l2_weighting: bool = True,
        yukawa_lambda_name: str = "k",
    ) -> None:
        kernel = YukawaKernel(ambient_dim, yukawa_lambda_name=yukawa_lambda_name)

        super().__init__(
            kernel,
            sign=sign,
            kernel_arguments={yukawa_lambda_name: sym.var(yukawa_lambda_name)},
            use_l2_weighting=use_l2_weighting,
            use_improved_operator=False,
        )


@make_sym_density.register(ScalarNeumannRepresentation)
def _make_sym_density_scalar(
    self: ScalarNeumannRepresentation, name: str = "sigma"
) -> SymbolicVariableType:
    return self.op.get_density_var(name)


@prepare_sym_rhs.register(ScalarNeumannRepresentation)
def _prepare_sym_rhs_scalar(
    self: ScalarNeumannRepresentation, b: SymbolicVariableType
) -> SymbolicVariableType:
    return self.op.prepare_rhs(b)


@prepare_sym_solution.register(ScalarNeumannRepresentation)
def _prepare_sym_solution_scalar(
    self: ScalarNeumannRepresentation, sigma: SymbolicVariableType
) -> SymbolicVariableType:
    return sigma / self.op.get_sqrt_weight()


@make_sym_operator.register(ScalarNeumannRepresentation)
def _make_sym_operator_scalar(
    self: ScalarNeumannRepresentation, sigma: SymbolicVariableType
) -> SymbolicOperatorType:
    return self.op.operator(sigma)


# }}}


# {{{ solve beltrami


def solve_beltrami(
    actx: ArrayContext,
    places: GeometryCollection,
    op: BeltramiRepresentation,
    b: DOFArray,
    *,
    dofdesc: sym.DOFDescriptorLike | None = None,
    gmres_arguments: dict[str, Any] | None = None,
    context: dict[str, Any] | None = None,
) -> DOFArray:
    # {{{

    if gmres_arguments is None:
        gmres_arguments = {}

    if context is None:
        context = {}

    all_gmres_arguments = {
        "rtol": 1.0e-7,
        "callback": True,
        "stall_iterations": 0,
        "hard_failure": True,
    }
    all_gmres_arguments.update(gmres_arguments)

    # }}}

    # {{{ symbolic

    sym_density = make_sym_density(op, "sigma")
    sym_op = make_sym_operator(op, sym_density)
    sym_b = make_sym_density(op, "b")

    sym_result = prepare_sym_solution(op, sym_density)
    sym_b = prepare_sym_rhs(op, sym_b)

    # }}}

    # {{{ solve

    # FIXME: do a better job at guessing the dtype from b
    if op.kernel.is_complex_valued:
        dtype = np.complex128
    else:
        dtype = np.float64

    from pystopt import bind

    b = bind(places, sym_b, auto_where=dofdesc)(actx, b=b, **context)
    scipy_op = bind(places, sym_op, auto_where=dofdesc).scipy_op(
        actx, "sigma", dtype, **context
    )

    from pystopt.optimize.gmres import gmres

    result = gmres(scipy_op, b, **all_gmres_arguments)

    solution = bind(places, sym_result, auto_where=dofdesc)(
        actx, sigma=result.solution, **context
    )

    if solution.entry_dtype.kind == "c" and b.entry_dtype.kind != "c":
        solution = actx.np.real(solution)

    # }}}

    return solution


# }}}
