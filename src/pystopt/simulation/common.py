# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

"""
.. currentmodule:: pystopt.simulation

Mixin classes used to provide various functionality for shape optimization.

.. autoclass:: ShapeOptimizationWrangler
.. autoclass:: ShapeOptimizationState

.. autoclass:: StokesGeometryWrangler
.. autoclass:: StokesGeometryState

Additional callbacks specifically for shape optimization.

.. autoclass:: ShapeOptimizationCallbackManager

Additional callbacks specifically for the Stokes equations.

.. autoclass:: StokesHistoryCallback
"""

from dataclasses import dataclass, field
from typing import Any

import numpy as np

from arraycontext import ArrayContext, ArrayOrContainerT
from pytential import GeometryCollection
from pytools import memoize_in, memoize_method

import pystopt.callbacks as scb
from pystopt import bind, sym
from pystopt.cost import ShapeFunctional
from pystopt.filtering import FilterType
from pystopt.paths import FileParts
from pystopt.simulation.state import (
    GeometryState,
    GeometryWrangler,
    MeshQualityHistoryCallback,
    as_state_container,
)
from pystopt.stokes import StokesSolution, TwoPhaseStokesRepresentation
from pystopt.tools import get_default_logger

logger = get_default_logger(__name__)


# {{{ shape optimization state


@dataclass(frozen=True)
class ShapeOptimizationWrangler(GeometryWrangler):
    """
    .. attribute:: cost

        A :class:`~pystopt.cost.ShapeFunctional` used for the optimization.

    .. attribute:: context

        Additional symbolic variables used to define the cost functional that
        will be used in the evaluation.

    .. attribute:: filter_type

        The type of filter to be used of class :class:`~pystopt.filtering.FilterType`.

    .. attribute:: filter_arguments

        Additional filter arguments, that actually define the filter in use.

    .. automethod:: apply_filter

    .. automethod:: get_sym_shape_cost
    .. automethod:: evaluate_shape_cost

    .. automethod:: get_sym_shape_gradient
    .. automethod:: evaluate_shape_gradient
    """

    cost: ShapeFunctional
    context: dict[str, Any]

    filter_type: FilterType
    filter_arguments: dict[str, Any]

    def get_initial_state(self, actx: ArrayContext) -> "ShapeOptimizationState":
        x = super().get_initial_state(actx)
        return ShapeOptimizationState(x=x.x, wrangler=self)

    def apply_filter(
        self,
        actx: ArrayContext,
        places: GeometryCollection,
        ary: ArrayOrContainerT,
        *,
        force_spectral: bool | None = None,
    ) -> ArrayOrContainerT:
        """Apply a filter to the given array *ary*.

        By default, this function calls :func:`~pystopt.filtering.apply_filter_by_type`
        using :attr:`filter_type` and :attr:`filter_arguments`.
        """
        if force_spectral is None:
            force_spectral = self.is_spectral

        from pystopt.filtering import apply_filter_by_type

        return apply_filter_by_type(
            actx,
            places,
            ary,
            filter_type=self.filter_type,
            filter_arguments=self.filter_arguments,
            dofdesc=self.dofdesc,
            force_spectral=force_spectral,
        )

    # {{{ cost

    @memoize_method
    def get_sym_shape_cost(self) -> sym.var:
        """Symbolic expression for the shape cost functional.

        :returns: a fully tagged expression for the cost.
        """
        expr = self.cost(self.ambient_dim, dofdesc=self.dofdesc)

        from pystopt.symbolic.execution import prepare_expr

        return prepare_expr(self.places, expr, self.dofdesc)

    def evaluate_shape_cost(
        self,
        actx: ArrayContext,
        places: GeometryCollection,
        *,
        context: dict[str, Any] | None = None,
    ) -> float:
        """Evaluates the cost functional from :meth:`get_sym_shape_cost`.

        :returns: the value of the cost functional.
        """
        if context is None:
            context = {}
        context = {**self.context, **context}

        r = bind(places, self.get_sym_shape_cost(), _skip_prepare=True)(actx, **context)

        return actx.to_numpy(r)

    # }}}

    # {{{ gradient

    def get_sym_shape_gradient(self) -> np.ndarray:
        """Symbolic expression for the shape gradient.

        This functionality needs to be implemented by the subclasses, as
        the cost functional is not sufficient to define the gradient in
        the presence of constraints.

        :returns: a fully tagged expression for the gradient.
        """
        raise NotImplementedError

    def evaluate_shape_gradient(
        self,
        actx: ArrayContext,
        places: GeometryCollection,
        *,
        context: dict[str, Any] | None = None,
        force_spectral: bool | None = None,
    ) -> np.ndarray:
        """Evaluates the shape gradient from :meth:`get_sym_shape_gradient`.

        By default, the filter from :meth:`apply_filter` is applied to the
        gradient. No additional modifications are performed.

        :returns: the shape gradient for the problem, as set up.
        """
        if context is None:
            context = {}
        context = {**self.context, **context}

        result = bind(
            places,
            self.get_sym_shape_gradient(),
            _skip_prepare=True,
        )(actx, **context)

        return self.apply_filter(actx, places, result, force_spectral=force_spectral)

    # }}}


@as_state_container
class ShapeOptimizationState(GeometryState):
    """State vector for shape optimization.

    Generic functions for obtaining the cost value and the gradient. In the
    general case, these functions just forward to the shape variants below
    and are here just to offer an abstract interface to follow.

    .. automethod:: evaluate_cost
    .. automethod:: evaluate_gradient
    .. automethod:: evaluate_cost_gradient

    When the full (unparametrized) shape gradient is required, use the
    functions below.

    .. automethod:: evaluate_shape_cost
    .. automethod:: evaluate_shape_gradient
    .. automethod:: evaluate_shape_cost_gradient
    """

    def evaluate_cost(self) -> float:
        return self.evaluate_shape_cost()

    def evaluate_gradient(self) -> np.ndarray:
        return self.evaluate_shape_gradient()

    def evaluate_cost_gradient(self) -> np.ndarray:
        return self.evaluate_cost(), self.evaluate_gradient()

    @memoize_method
    def evaluate_shape_cost(self) -> float:
        """
        :returns: the value of the cost functional at the current state.
        """
        return self.wrangler.evaluate_shape_cost(
            self.array_context, self.get_geometry_collection()
        )

    @memoize_method
    def evaluate_shape_gradient(self) -> np.ndarray:
        """
        :returns: the value of the (shape) gradient at the current state.
        """
        return self.wrangler.evaluate_shape_gradient(
            self.array_context, self.get_geometry_collection()
        )

    def evaluate_shape_cost_gradient(self) -> tuple[float, np.ndarray]:
        """
        :returns: a :class:`tuple` containing the cost and the gradient.
        """
        return self.evaluate_shape_cost(), self.evaluate_shape_gradient()


# }}}


# {{{ Stokes state


@dataclass(frozen=True)
class StokesGeometryWrangler(GeometryWrangler):
    """
    .. attribute:: op

        A :class:`~pystopt.stokes.TwoPhaseStokesRepresentation`.

    .. attribute:: lambdas

        A :class:`dict` of viscosity ratios for the geometries of interest.

    .. attribute:: context

        Additional symbolic variable values used in evaluation.

    .. attribute:: gmres_arguments

        A :class:`dict` of additional parameters passed on to
        :func:`~pystopt.stokes.single_layer_solve`.

    .. attribute:: filter_type

        The type of filter to be used of class :class:`~pystopt.filtering.FilterType`.

    .. attribute:: filter_arguments

        Additional filter arguments, that actually define the filter in use.

    .. automethod:: clear_density_cache
    .. automethod:: apply_filter
    .. automethod:: get_stokes_solution
    """

    op: TwoPhaseStokesRepresentation

    lambdas: dict[str, float]
    context: dict[str, Any]
    gmres_arguments: dict[str, Any]

    filter_type: FilterType
    filter_arguments: dict[str, Any]

    # NOTE: enables a density cache between calls to `get_stokes_solution`
    _density_cache: dict[str, np.ndarray] | None = field(
        default_factory=dict, init=False, repr=False
    )

    def clear_density_cache(self) -> None:
        self._density_cache.clear()

    def get_initial_state(self, actx: ArrayContext) -> "StokesGeometryState":
        x = super().get_initial_state(actx)
        return StokesGeometryState(x=x.x, wrangler=self)

    def apply_filter(
        self,
        actx: ArrayContext,
        places: GeometryCollection,
        ary: ArrayOrContainerT,
        *,
        force_spectral: bool | None = None,
    ) -> ArrayOrContainerT:
        """Apply a filter to the given array *ary*.

        By default, this function calls :func:`~pystopt.filtering.apply_filter_by_type`
        using :attr:`filter_type` and :attr:`filter_arguments`.
        """
        if force_spectral is None:
            force_spectral = self.is_spectral

        from pystopt.filtering import apply_filter_by_type

        return apply_filter_by_type(
            actx,
            places,
            ary,
            filter_type=self.filter_type,
            filter_arguments=self.filter_arguments,
            dofdesc=self.dofdesc,
            force_spectral=force_spectral,
        )

    def get_stokes_solution(
        self,
        actx: ArrayContext,
        op: TwoPhaseStokesRepresentation,
        places: GeometryCollection,
        *,
        context: dict[str, Any] | None = None,
        cached: bool = True,
    ) -> StokesSolution:
        """Solves the Stokes problem on the given geometry in *places*.

        :arg op: a :class:`~pystopt.stokes.TwoPhaseStokesRepresentation` of the
            problem that can be solved for the density. This is not necessarily
            the same as :attr:`op`.
        :arg cached: if *True*, the density resulting from the solve is cached
            and used as an initial guess for subsequent calls to this function.
            The cache can be cleared at any time using :meth:`clear_density_cache`.
        """
        if context is None:
            context = {}
        context = {**self.context, **context}

        # FIXME: smarter caching scheme for the density? this doesn't seem to work
        # great for RK schemes or something? No particular decrease in GMRES iterations

        use_cached_density = (
            cached
            # NOTE: no need to cache for `lambda = 1`, since we know the solution
            and abs(context["viscosity_ratio"] - 1) > 1.0e-14
        )

        # {{{ initial guess

        x0 = None
        if use_cached_density:
            key = (type(op), type(op.bc))
            x0 = self._density_cache.get(key, None)

            if __debug__ and x0 is not None:
                from pystopt.dof_array import dof_array_norm

                logger.info(
                    "density_cache: %s %.12e",
                    key,
                    0.0 if x0 is None else actx.to_numpy(dof_array_norm(x0)),
                )

        # }}}

        # {{{ solve

        from pystopt.stokes import single_layer_solve

        gmres_arguments = {**self.gmres_arguments, "x0": x0}
        r = single_layer_solve(
            actx,
            places,
            op,
            gmres_arguments=gmres_arguments,
            lambdas=self.lambdas,
            sources=[self.dofdesc],
            context=context,
        )

        # }}}

        # {{{ cache density

        if use_cached_density:
            if __debug__ and x0 is not None:
                logger.info(
                    "cache density mismatch: %.5e",
                    actx.to_numpy(dof_array_norm(x0 - r.density)),
                )

            self._density_cache[key] = r.density

        # }}}

        return r


@as_state_container
class StokesGeometryState(GeometryState):
    """A :class:`~pystopt.simulation.GeometryState` that contains the Stokes
    flow solution on the given geometry.

    .. automethod:: get_stokes_density
    .. automethod:: get_stokes_velocity
    """

    # NOTE: these fields are cached explicitly so that they can be checkpointed
    density: np.ndarray | None = field(default=None, init=False, repr=False)
    velocity: np.ndarray | None = field(default=None, init=False, repr=False)

    # {{{ internal

    def _get_uncached_stokes_solution(self, density, op, context):
        if density is None:
            result = self.wrangler.get_stokes_solution(
                self.array_context,
                op,
                self.get_geometry_collection(),
                context=context,
            )
        else:
            result = StokesSolution(
                actx=self.array_context,
                source=self.wrangler.dofdesc,
                places=self.get_geometry_collection(),
                density=density,
                op=op,
                context={**self.wrangler.context, **context},
            )

        return result

    @memoize_method
    def _get_stokes_solution(self):
        result = self._get_uncached_stokes_solution(self.density, self.wrangler.op, {})

        if self.density is None:
            object.__setattr__(self, "density", result.density)

        from dataclasses import replace

        return replace(result, density_name="q")

    # }}}

    # {{{ Stokes

    def get_stokes_density(self) -> np.ndarray:
        """
        :returns: the density in the boundary integral representation.
        """
        return self._get_stokes_solution().density

    def get_stokes_velocity(self) -> np.ndarray:
        """
        :returns: the velocity field for the current geometry using the
            density from :meth:`get_stokes_density`.
        """
        if self.velocity is None:
            from pystopt.stokes import sti

            u = sti.velocity(
                self._get_stokes_solution(),
                side=None,
                qbx_forced_limit=+1,
                dofdesc=self.wrangler.dofdesc,
            )

            u = self.wrangler.apply_filter(
                self.array_context,
                self.get_geometry_collection(),
                u,
                force_spectral=False,
            )

            object.__setattr__(self, "velocity", u)

        return self.velocity

    # }}}


# }}}


# {{{ optimization callback manager


def get_output_fields_optimization(
    state: ShapeOptimizationState, **kwargs: Any
) -> dict[str, scb.OutputField]:
    info = kwargs["info"]
    fields = {}

    # {{{ gradient

    fields["f"] = scb.OutputField.checkpoint("f", info.f)
    fields["g"] = scb.OutputField(r"\mathbf{g}", info.g)

    # }}}

    # {{{ cg

    if info.d is not None:
        fields["d"] = scb.OutputField(r"\mathbf{d}", info.d)

    fields["alpha"] = scb.OutputField.checkpoint(r"\alpha", info.alpha)
    fields["it"] = scb.OutputField.checkpoint("n", info.it)

    # }}}

    return fields


def get_output_fields_shape_optimization(
    state: ShapeOptimizationState, **kwargs: Any
) -> dict[str, scb.OutputField]:
    info = kwargs["info"]

    places = state.get_geometry_collection()
    dofdesc = state.wrangler.dofdesc

    # {{{ fields

    fields = get_output_fields_optimization(state, **kwargs)

    g_dot_n = bind(places, sym.n_dot(places.ambient_dim), auto_where=dofdesc)(
        state.array_context, x=info.g
    )
    fields["g_dot_n"] = scb.OutputField.visualize(
        "g_dot_n" if places.ambient_dim == 3 else r"\mathbf{g} \cdot \mathbf{n}",
        g_dot_n,
    )

    # }}}

    return fields


@dataclass
class ShapeOptimizationCallbackManager(scb.CallbackManager):
    r"""A callback manager that handles variables of the type
    :class:`ShapeOptimizationState`.

    In particular, it expects a keyword argument of the type
    :class:`~pystopt.optimize.CGCallbackInfo` containing the optimization
    information.

    .. automethod:: __call__
    """

    def get_output_field_getters(self) -> tuple[scb.OutputFieldCallable, ...]:
        from pystopt.simulation.state import get_output_fields_geometry

        return (
            get_output_fields_geometry,
            get_output_fields_shape_optimization,
        )

    def __call__(self, *args: Any, **kwargs: Any) -> int:
        if "info" not in kwargs:
            raise KeyError(f"'info' not passed to '{type(self).__name__}'")

        def unwrap(ary):
            return getattr(ary, "x", ary)

        from dataclasses import replace

        (state,) = args
        info = kwargs.pop("info")
        info = replace(
            info,
            g=state.wrangler.from_state_type(unwrap(info.g)),
            d=state.wrangler.from_state_type(unwrap(info.d)),
        )

        return super().__call__(state, info=info, **kwargs)


# }}}


# {{{ optimization visualization callbacks


def matplotlib_visualize_shape_optimization_writer(
    vis: scb.VisualizeCallback,
    basename: FileParts,
    state: GeometryState,
    fields: dict[str, scb.OutputField],
    **kwargs: Any,
) -> None:
    if "g" not in fields:
        return

    @memoize_in(
        vis, (matplotlib_visualize_shape_optimization_writer, "gradient_bounds")
    )
    def get_gradient_bounds():
        actx = state.array_context

        g_ = fields["g"].value
        gmin = actx.to_numpy(actx.np.min(g_))
        gmax = actx.to_numpy(actx.np.max(g_))
        gabs = max(abs(gmax), abs(gmin))

        glim = (gmin - 0.1 * gabs, gmax + 0.1 * gabs)
        return {"ylim": glim, "add_legend": True}

    try:
        visualize_kwargs = vis.get_gradient_kwargs(state)
    except AttributeError:
        visualize_kwargs = get_gradient_bounds()

    # {{{ gradient

    markers = ["-", "--"]
    if "d" in fields:
        markers.append(":")

    filename = basename.with_suffix("gradient")
    vis.write_file(
        filename,
        scb.extract_fields(fields, ("g", "g_dot_n", "d")),
        markers=markers,
        **visualize_kwargs,
    )

    # }}}


# }}}


# {{{ stokes callbacks


def get_output_fields_stokes(
    state: GeometryState, **kwargs: Any
) -> dict[str, scb.OutputField]:
    places = state.get_geometry_collection()
    dofdesc = state.wrangler.dofdesc

    u = state.get_stokes_velocity()
    u_dot_n = bind(places, sym.n_dot(places.ambient_dim), auto_where=dofdesc)(
        state.array_context, x=u
    )

    fields = {}
    fields["u"] = scb.OutputField(r"\mathbf{u}", u)
    fields["u_dot_n"] = scb.OutputField.visualize(
        "u_dot_n" if places.ambient_dim == 3 else r"\mathbf{u} \cdot \mathbf{n}",
        u_dot_n,
    )

    q = state.get_stokes_density()
    fields["q"] = scb.OutputField(r"\mathbf{q}", q)

    from pystopt.simulation.unsteady import StokesEvolutionWrangler

    if (
        isinstance(state.wrangler, StokesEvolutionWrangler)
        and state.wrangler.evolution.force_tangential_velocity
    ):
        w = state.get_tangential_forcing()
        w_dot_t = bind(
            places, sym.project_tangent(places.ambient_dim), auto_where=dofdesc
        )(state.array_context, x=w)

        fields["w"] = scb.OutputField(r"\mathbf{w}", w)
        fields["w_dot_t"] = scb.OutputField.visualize(
            "w_dot_t" if places.ambient_dim == 3 else r"\mathbf{w}_\tau",  # noqa: RUF027
            w_dot_t,
        )

    if "ud" in state.wrangler.context:
        fields["ud"] = scb.OutputField.visualize("u_d", state.wrangler.context["ud"])

    return fields


def matplotlib_visualize_stokes_writer(
    vis: scb.VisualizeCallback,
    basename: FileParts,
    state: GeometryState,
    fields: dict[str, scb.OutputField],
    **kwargs: Any,
) -> None:
    if "u" not in fields:
        return

    names_and_fields = scb.extract_fields(fields, ("u", "u_dot_n"))

    @memoize_in(vis, (matplotlib_visualize_stokes_writer, "velocity_bounds"))
    def get_velocity_bounds():
        assert names_and_fields
        actx = state.array_context

        umin = np.inf
        umax = -np.inf
        for _, u in names_and_fields:
            umin = min(umin, actx.to_numpy(actx.np.min(u)))
            umax = max(umax, actx.to_numpy(actx.np.max(u)))

        uabs = max(abs(umax), abs(umin))
        assert np.isfinite(uabs)

        ulim = umin - 0.1 * uabs, umax + 0.1 * uabs
        return {"ylim": ulim}

    try:
        visualize_kwargs = vis.get_velocity_kwargs(state)
    except AttributeError:
        visualize_kwargs = get_velocity_bounds()

    filename = basename.with_suffix("velocity")
    vis.write_file(filename, names_and_fields, **visualize_kwargs)

    if "w" in fields:
        filename = basename.with_suffix("forcing")
        vis.write_file(
            filename,
            scb.extract_fields(fields, ("w", "w_dot_t")),
            **visualize_kwargs,
        )

    if "ud" in fields:
        filename = basename.with_suffix("normal_velocity")
        vis.write_file(
            filename,
            scb.extract_fields(fields, ("u_dot_n", "ud")),
            **visualize_kwargs,
        )


@dataclass(frozen=True)
class StokesHistoryCallback(MeshQualityHistoryCallback):
    r"""
    .. attribute:: deformation

        Deformation of the geometry in the :math:`(x, y)` plane.

    .. attribute:: u_dot_n

        Maximum absolute value of the normal velocity, i.e.
        :math:`\|\mathbf{u} \cdot \mathbf{n}\|_\infty`.
    """

    deformation: list[float] = field(default_factory=list, repr=False)
    u_dot_n: list[float] = field(default_factory=list, repr=False)

    def __call__(self, *args: Any, **kwargs: Any) -> int:
        r = super().__call__(*args, **kwargs)

        (state,) = args
        fields = kwargs.get("fields")

        places = state.get_geometry_collection()
        dofdesc = state.wrangler.dofdesc
        ambient_dim = places.ambient_dim

        actx = state.array_context
        assert actx is not None, "'state' is frozen"

        # {{{ velocity

        if fields is not None:
            # NOTE: should use self.norm? That's mostly some L2 norm
            u_dot_n = bind(
                places,
                sym.norm(places.ambient_dim, sym.var("u_dot_n"), p=np.inf),
                auto_where=dofdesc,
            )(actx, u_dot_n=fields["u_dot_n"].value)

            self.u_dot_n.append(actx.to_numpy(u_dot_n))

        # }}}

        # {{{ geometry

        # NOTE: only valid in 2D with one group/droplet
        if places.ambient_dim == 2 and len(state.x[0]) == 1:
            dx = bind(
                places,
                sym.NodeMax(
                    sym.abs(
                        sym.nodes(ambient_dim).as_vector()
                        - sym.surface_centroid(ambient_dim)
                    )
                ),
                auto_where=dofdesc,
            )(actx)

            self.deformation.append(actx.to_numpy((dx[0] - dx[1]) / (dx[0] + dx[1])))

        # }}}

        return r


# }}}


# {{{ mode visualization


def get_output_fields_modes(
    state: GeometryState, **kwargs: Any
) -> dict[str, scb.OutputField]:
    actx = state.array_context
    places = state.get_geometry_collection()
    dofdesc = state.wrangler.dofdesc

    discr = places.get_discretization(dofdesc.geometry, dofdesc.discr_stage)
    xlm = actx.thaw(discr.xlm)

    fields = {}
    fields["xlm"] = scb.OutputField.visualize(
        "xlm" if places.ambient_dim == 3 else r"\hat{\mathbf{X}}", xlm
    )

    if "xd" in state.wrangler.context:
        dofdesc = state.wrangler.cost.dofdesc
        discr_d = places.get_discretization(dofdesc.geometry, dofdesc.discr_stage)
        xlm_d = actx.thaw(discr_d.xlm)

        fields["xlm_d"] = scb.OutputField.visualize(
            "xlm_d" if places.ambient_dim == 3 else r"\hat{\mathbf{X}}_d", xlm_d
        )
    else:
        from pytools.obj_array import make_obj_array

        fields["xlm_d"] = scb.OutputField.visualize(
            "xlm_d" if places.ambient_dim == 3 else r"\hat{\mathbf{X}}_d",
            make_obj_array([0, 0, 0][: places.ambient_dim]),
        )

    return fields


def visualize_fourier_mode_writer(
    vis: scb.VisualizeCallback,
    basename: FileParts,
    state: GeometryState,
    fields: dict[str, scb.OutputField],
    **kwargs: Any,
) -> None:
    if "xlm" not in fields:
        return

    places = state.get_geometry_collection()
    dofdesc = state.wrangler.dofdesc

    # FIXME: assumes that x and xd have the same underlying discretization
    discr = places.get_discretization(dofdesc.geometry, dofdesc.discr_stage)
    x = fields["xlm"].value
    xd = fields["xlm_d"].value

    from pystopt.mesh.fourier import visualize_fourier_modes

    for axis, xi, xdi in zip(["x", "y"], x, xd, strict=False):
        filename = basename.with_suffix(f"modes_{axis}")
        if xdi == 0:
            names_and_fields = [(r"\hat{x}", xi)]  # noqa: RUF027
        else:
            names_and_fields = [(r"\hat{x}", xi), (r"\hat{y}", xdi)]  # noqa: RUF027

        visualize_fourier_modes(
            filename,
            state.array_context,
            discr,
            names_and_fields,
            markers=["-", "k--"],
            semilogy=True,
            overwrite=vis.overwrite,
        )


def visualize_spharm_mode_writer(
    vis: scb.VisualizeCallback,
    basename: FileParts,
    state: GeometryState,
    fields: dict[str, scb.OutputField],
    **kwargs: Any,
) -> None:
    if "xlm" not in fields:
        return

    try:
        visualize_kwargs = vis.get_modes_kwargs(state)
    except AttributeError:
        visualize_kwargs = {"mode": "imshow", "semilogy": True, "output": "error"}

    places = state.get_geometry_collection()
    dofdesc = state.wrangler.dofdesc

    output = visualize_kwargs.pop("output", "error")

    # FIXME: assumes that x and xd have the same underlying discretization
    discr = places.get_discretization(dofdesc.geometry, dofdesc.discr_stage)
    xlm = fields["xlm"].value
    xlm_d = fields["xlm_d"].value

    from pystopt.mesh.spharm import visualize_spharm_modes

    for axis, xi, xdi in zip(["x", "y", "z"], xlm, xlm_d, strict=False):
        if output == "error":
            filename = basename.with_suffix(f"modes_{axis}_error")
            visualize_spharm_modes(
                filename,
                discr,
                [(axis, xi - xdi)],
                overwrite=vis.overwrite,
                **visualize_kwargs,
            )
        elif output == "abs":
            filename = basename.with_suffix(f"modes_{axis}_abs")
            visualize_spharm_modes(
                filename,
                discr,
                [(axis, xi)],
                overwrite=vis.overwrite,
                **visualize_kwargs,
            )
        else:
            raise ValueError(f"unknown output type: {output}")


# }}}
