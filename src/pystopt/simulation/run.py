# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

"""
.. currentmodule:: pystopt.simulation

.. autofunction:: rescale_state_to_volume
"""

import enum
from dataclasses import dataclass, replace
from typing import Any

import numpy as np

from arraycontext import ArrayContext

from pystopt.callbacks import CallbackManager
from pystopt.optimize import CGResult
from pystopt.simulation.state import GeometryState, GeometryWrangler
from pystopt.tools import get_default_logger

logger = get_default_logger(__name__)


# {{{ rescale state volume


def rescale_state_to_volume(
    state: GeometryState, vd: float | tuple[float, ...]
) -> GeometryState:
    actx = state.array_context
    ngroups = len(state.x[0])

    from numbers import Number

    if isinstance(vd, Number):
        vd = (vd,) * ngroups

    if len(vd) != ngroups:
        raise ValueError(f"Expected {ngroups} volumes, but got {len(vd)}")

    from pystopt.mesh.processing import compute_group_volumes, rescale_nodes

    places = state.get_geometry_collection()
    vs = compute_group_volumes(actx, places, dofdesc=state.wrangler.dofdesc)

    return replace(state, x=rescale_nodes(actx, state.x, vd=vd, vs=vs))


# }}}


# {{{ run driver


@enum.unique
class RunMode(enum.Enum):
    FORWARD_ONLY = enum.auto()
    ADJOINT_ONLY = enum.auto()
    FINITE_DIFFERENCE = enum.auto()
    OPTIMIZATION = enum.auto()


@dataclass(frozen=True)
class RunResult:
    mode: RunMode
    cost: float | None
    grad: np.ndarray | None


@dataclass(frozen=True)
class FiniteRunResult(RunResult):
    grad_fd: np.ndarray
    cost_fd: np.ndarray


@dataclass(frozen=True)
class OptimizationRunResult(RunResult):
    solution: CGResult


def _driver_run_forward_only(actx, wrangler):
    state = wrangler.get_initial_state(actx)

    return RunResult(mode=RunMode.FORWARD_ONLY, cost=state.evaluate_cost(), grad=None)


def _driver_run_adjoint_only(actx, wrangler):
    state = wrangler.get_initial_state(actx)
    c = state.evaluate_cost()
    g = state.evaluate_gradient()

    return RunResult(mode=RunMode.ADJOINT_ONLY, cost=c, grad=g)


def _driver_run_finite_difference_quasistokes(actx, wrangler, *, eps):
    state_m = wrangler.get_initial_state(actx)
    cost_m = state_m.evaluate_cost()

    from itertools import product

    dj_dc = np.empty((eps.size, state_m.x.size))
    cost_p = np.empty((eps.size, state_m.x.size))

    from copy import deepcopy

    for i, j in product(range(eps.size), range(state_m.x.size)):
        x = deepcopy(state_m.x)
        x[j] = x[j] + eps[i]

        state_p = replace(state_m, x=x)
        cost_p[i, j] = state_p.evaluate_cost()

        dj_dc[i, j] = (cost_p[i, j] - cost_m) / eps[i]

    return FiniteRunResult(
        mode=RunMode.FINITE_DIFFERENCE,
        cost=cost_m,
        grad=state_m.evaluate_gradient(),
        cost_fd=cost_p,
        grad_fd=dj_dc,
    )


def _driver_run_optimization(actx, wrangler, callback, *, options):
    state0 = wrangler.get_initial_state(actx)

    from pystopt.optimize.steepest import minimize

    r = minimize(
        fun=lambda x: x.evaluate_cost(),
        x0=state0,
        jac=lambda x: replace(x, x=x.evaluate_gradient()),
        funjac=lambda x: (x.evaluate_cost(), replace(x, x=x.evaluate_gradient())),
        callback=callback,
        options=options,
    )

    return OptimizationRunResult(
        mode=RunMode.OPTIMIZATION,
        cost=r.x.evaluate_cost(),
        grad=r.x.evaluate_gradient(),
        solution=r,
    )


def driver_run(
    actx: ArrayContext,
    wrangler: GeometryWrangler,
    mode: RunMode,
    *,
    eps: float | np.ndarray | None = None,
    callback: CallbackManager | None = None,
    options: dict[str, Any] | None = None,
) -> RunResult:
    if mode == RunMode.FORWARD_ONLY:
        return _driver_run_forward_only(actx, wrangler)
    elif mode == RunMode.ADJOINT_ONLY:
        return _driver_run_adjoint_only(actx, wrangler)
    elif mode == RunMode.FINITE_DIFFERENCE:
        if eps is None:
            raise ValueError(f"'eps' not provided in mode {mode}")

        from numbers import Number

        if isinstance(eps, Number):
            eps = np.array([eps])

        from pystopt.simulation.quasistokes import StokesUnsteadyOptimizationWrangler

        if isinstance(wrangler, StokesUnsteadyOptimizationWrangler):
            return _driver_run_finite_difference_quasistokes(actx, wrangler, eps=eps)
        else:
            raise TypeError(f"unsupported wrangler type: '{type(wrangler).__name__}'")
    elif mode == RunMode.OPTIMIZATION:
        if callback is None:
            callback = CallbackManager()

        if options is None:
            options = {}
        return _driver_run_optimization(actx, wrangler, callback, options=options)
    else:
        raise ValueError(f"unknown run mode: {mode}")


# }}}
