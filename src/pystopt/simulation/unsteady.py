# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

"""
.. autoclass:: GeometryEvolutionState
.. autoclass:: GeometryEvolutionWrangler

.. autoclass:: StokesEvolutionState
.. autoclass:: StokesEvolutionWrangler
.. autofunction:: quasi_evolve_stokes

.. autoclass:: AdjointGeometryEvolutionWrangler
.. autoclass:: AdjointGeometryEvolutionState

.. autoclass:: AdjointStokesEvolutionState
.. autoclass:: AdjointStokesEvolutionWrangler
.. autofunction:: quasi_evolve_adjoint

.. autoclass:: StokesEvolutionCallbackManager
.. autoclass:: StokesEvolutionVisualizeCallback
.. autoclass:: StokesEvolutionHistoryCallback

.. autoclass:: AdjointStokesEvolutionCallbackManager
.. autoclass:: AdjointStokesEvolutionVisualizeCallback
"""

from dataclasses import dataclass, field, replace
from typing import Any

import numpy as np

from arraycontext import ArrayContext, ArrayOrContainerT
from meshmode.dof_array import DOFArray
from pytential import GeometryCollection
from pytools import memoize_method

import pystopt.callbacks as scb
import pystopt.simulation.common as scm
from pystopt import bind, sym
from pystopt.evolution import StokesEvolutionOperator
from pystopt.evolution.stepping import TimeWrangler
from pystopt.filtering import FilterType
from pystopt.paths import FileParts
from pystopt.simulation.state import GeometryState, GeometryWrangler, as_state_container
from pystopt.tools import get_default_logger

logger = get_default_logger(__name__)


# {{{ geometry evolution wrangler


@as_state_container
class GeometryEvolutionState(GeometryState):
    """
    .. attribute:: t

        Time at which the current state is computed. This is expected to be
        advanced by the time integrator together with the geometry. Note that,
        due to floating point operations, the time integrator :math:`t` and this
        one may start diverging.

    .. automethod:: get_velocity_field
    .. automethod:: get_tangential_forcing
    """

    t: np.ndarray

    # NOTE: these fields are cached explicitly so that they can be checkpointed
    velocity: np.ndarray | None = field(default=None, init=False, repr=False)
    forcing: np.ndarray | None = field(default=None, init=False, repr=False)

    def get_velocity_field(self):
        raise NotImplementedError

    def get_tangential_forcing(self):
        if not self.wrangler.has_tangential_forcing:
            from pytools.obj_array import make_obj_array

            return make_obj_array([0, 0, 0][: self.wrangler.ambient_dim])

        if self.forcing is None:
            stab = self.wrangler.get_stabilizer()
            w = stab(
                self.get_geometry_collection(),
                self.get_velocity_field(),
                dofdesc=self.wrangler.dofdesc,
            )

            w = self.wrangler.apply_filter(
                self.array_context,
                self.get_geometry_collection(),
                w,
                force_spectral=False,
            )

            object.__setattr__(self, "forcing", w)

        return self.forcing


@dataclass(frozen=True)
class GeometryEvolutionWrangler(GeometryWrangler):
    """
    .. attribute:: evolution

        A :class:`~pystopt.evolution.StokesEvolutionOperator` that symbolically
        describes the right-hand side terms.

    .. attribute:: stabilizer_name

        The name of the stabilizer, see
        :func:`~pystopt.stabilization.make_stabilizer_from_name`.

    .. attribute:: stabilizer_arguments

        Passed to :func:`~pystopt.stabilization.make_stabilizer_from_name`.

    .. attribute:: filter_type
    .. attribute:: filter_arguments

    .. automethod:: apply_filter
    .. automethod:: get_stabilizer
    .. automethod:: evolution_source_term
    .. automethod:: estimate_time_step
    """

    evolution: StokesEvolutionOperator
    context: dict[str, Any]

    stabilizer_name: str
    stabilizer_arguments: dict[str, Any]

    filter_type: FilterType
    filter_arguments: dict[str, Any]

    @property
    def has_tangential_forcing(self):
        return self.evolution.force_tangential_velocity

    @property
    def keep_vertices(self):
        # NOTE: this is a guess! subclasses should probably overwrite it
        return self.has_tangential_forcing and self.stabilizer_name in {
            "zinchenko2002",
            "zinchenko2013",
            "loewenberg1996",
            "loewenberg2001",
        }

    # {{{ filtering

    def apply_filter(
        self,
        actx: ArrayContext,
        places: GeometryCollection,
        ary: ArrayOrContainerT,
        *,
        force_spectral: bool | None = None,
    ) -> ArrayOrContainerT:
        """Apply a filter to the given array *ary*.

        By default, this function calls :func:`~pystopt.filtering.apply_filter_by_type`
        using :attr:`filter_type` and :attr:`filter_arguments`.
        """
        kwargs = {}
        if self.filter_type == FilterType.Normal:
            kwargs["only_normal"] = (
                not self.evolution.force_tangential_velocity
                and self.evolution.force_normal_velocity
            )

        if force_spectral is None:
            force_spectral = self.is_spectral

        from pystopt.filtering import apply_filter_by_type

        return apply_filter_by_type(
            actx,
            places,
            ary,
            filter_type=self.filter_type,
            filter_arguments={**self.filter_arguments, **kwargs},
            dofdesc=self.dofdesc,
            force_spectral=force_spectral,
        )

    # }}}

    # {{{ stabilizer

    @memoize_method
    def get_stabilizer(self):
        """
        :returns: a :class:`~pystopt.stabilization.PassiveStabilizer` described
            by :attr:`stabilizer_name`. This can be used to compute a
            tangential velocity field. Note that a tangential forcing is only used if
            :attr:`~pystopt.evolution.StokesEvolutionOperator.force_tangential_velocity`
            is set on :attr:`evolution`.
        """
        from pystopt.stabilization import make_stabilizer_from_name

        return make_stabilizer_from_name(
            self.stabilizer_name, **self.stabilizer_arguments
        )

    # }}}

    # {{{ evolution

    def evolution_source_term(
        self,
        t: float,
        state: GeometryEvolutionState,
    ) -> GeometryEvolutionState:
        if abs(state.t[0] - t) > 5.0e-15:
            logger.warning("temporal disturbance detected: %.12e", abs(state.t[0] - t))

        places = state.get_geometry_collection()
        actx = state.array_context

        # {{{ evaluate operator

        context = self.context.copy()
        context["u"] = state.get_velocity_field()
        context["w"] = state.get_tangential_forcing()

        sym_op = self.evolution.get_sym_operator(
            sym.make_sym_vector("u", self.ambient_dim),
            sym.make_sym_vector("w", self.ambient_dim),
            dofdesc=self.dofdesc,
        )

        # FIXME: do we want to filter again? probably not?
        v = bind(places, sym_op, auto_where=self.dofdesc)(actx, **context)

        # }}}

        return replace(state, x=self.to_state_type(v), t=np.array([1.0], dtype=object))

    def estimate_time_step(
        self, t: float, state: GeometryEvolutionState
    ) -> float | None:
        viscosity_ratio = self.context.get("viscosity_ratio", 1.0)
        capillary_number = self.context.get("ca", 1.0)

        # FIXME: should this use the Stokes velocity of the actual velocity
        # field used in the evolution equation? should be easy to cache that
        places = state.get_geometry_collection()
        velocity = state.get_velocity_field()

        # FIXME: add an attribute for the method here
        from pystopt.evolution.estimates import capillary_time_step_estimation

        return capillary_time_step_estimation(
            state.array_context,
            places,
            velocity,
            method="brackbill",
            capillary_number=capillary_number,
            viscosity_ratio=viscosity_ratio,
            sources=self.dofdesc,
        )

    # }}}


# }}}


# {{{ stokes evolution wrangler


@as_state_container
class StokesEvolutionState(scm.StokesGeometryState, GeometryEvolutionState):
    def get_velocity_field(self):
        return self.get_stokes_velocity()

    @memoize_method
    def _get_stokes_solution(self):
        result = self._get_uncached_stokes_solution(
            self.density, self.wrangler.op, context={"t": self.t[0]}
        )

        if self.density is None:
            object.__setattr__(self, "density", result.density)

        return replace(result, density_name="q")


@dataclass(frozen=True)
class StokesEvolutionWrangler(scm.StokesGeometryWrangler, GeometryEvolutionWrangler):
    def get_initial_state(self, actx: ArrayContext) -> StokesEvolutionState:
        x0 = super().get_initial_state(actx)
        return StokesEvolutionState(
            x=x0.x,
            t=np.array([0.0], dtype=object),
            wrangler=self,
        )


def quasi_evolve_stokes(
    actx: ArrayContext,
    wrangler: GeometryEvolutionWrangler,
    step: TimeWrangler,
    *,
    tmax: float | None = None,
    maxit: float | None = None,
    callback: scb.CallbackManager | None = None,
    verbose: bool = True,
) -> GeometryEvolutionState:
    if callback is None:
        callback = scb.CallbackManager()

    result = step.initial_state

    for event in step.run(tmax=tmax, maxit=maxit, verbose=verbose):
        result = event.state_component

        if callback is not None:
            ret = callback(result, event=event)
        else:
            ret = 1

        if ret == 0:
            break

    return result


# }}}


# {{{ adjoint


@dataclass(frozen=True)
class AdjointGeometryEvolutionWrangler:
    """
    .. attribute:: forward

        A :class:`StokesEvolutionWrangler`, whose properties and attributes
        are forwarded through this class.

    .. attribute:: cost

    .. attribute:: cost_final

    .. attribute:: ad

        An adjoint operator constructed from the forward operator and
        :attr:`cost`. This operator is used to compute the adjoint state
        at each time step.

    .. automethod:: get_sym_shape_gradient
    .. automethod:: evaluate_shape_gradient

    .. automethod:: get_sym_shape_gradient_final
    .. automethod:: evaluate_shape_gradient_final

    .. automethod:: get_initial_state
    .. automethod:: evolution_source_term
    """

    forward: GeometryEvolutionWrangler

    # {{{ forwarding

    # NOTE: this is trying to replicate enough of the interface of
    # GeometryEvolutionWrangler to use some of those methods as well

    @property
    def is_spectral(self):
        return self.forward.is_spectral

    @property
    def dofdesc(self):
        return self.forward.dofdesc

    @property
    def ambient_dim(self):
        return self.forward.ambient_dim

    @property
    def places(self):
        return self.forward.places

    @property
    def context(self):
        return self.forward.context

    def to_state_type(self, ary):
        return self.forward.to_state_type(ary)

    def from_state_type(self, ary):
        return self.forward.from_state_type(ary)

    def get_initial_state(
        self,
        actx: ArrayContext,
        state: GeometryEvolutionState,
    ) -> "AdjointGeometryEvolutionState":
        raise NotImplementedError

    # }}}

    # {{{ cost

    @property
    def cost(self):
        raise NotImplementedError

    @memoize_method
    def get_sym_shape_gradient(self) -> sym.var:
        from pystopt.simulation.staticstokes import make_sym_grad_static_stokes

        expr = sym.n_dot(make_sym_grad_static_stokes(self))

        from pystopt.symbolic.execution import prepare_expr

        return prepare_expr(self.places, expr, self.dofdesc)

    def evaluate_shape_gradient(
        self,
        actx: ArrayContext,
        places: GeometryCollection,
        *,
        context: dict[str, Any] | None = None,
    ) -> np.ndarray:
        if context is None:
            context = {}
        context = {**self.context, **context}

        result = bind(places, self.get_sym_shape_gradient(), _skip_prepare=True)(
            actx, **context
        )

        return self.forward.apply_filter(actx, places, result)

    # }}}

    # {{{ cost final

    @property
    def cost_final(self):
        raise NotImplementedError

    @memoize_method
    def get_sym_shape_gradient_final(self) -> sym.var:
        import pystopt.derivatives as grad

        expr = sym.n_dot(grad.shape(self.cost_final, self.ambient_dim))

        from pystopt.symbolic.execution import prepare_expr

        return prepare_expr(self.places, expr, self.dofdesc)

    def evaluate_shape_gradient_final(
        self,
        actx: ArrayContext,
        state: GeometryEvolutionState,
        *,
        context: dict[str, Any] | None = None,
    ) -> np.ndarray:
        if self.cost_final is None:
            return actx.np.zeros_like(state.x[0])

        if context is None:
            context = {}
        context = {**self.context, **context}

        places = state.get_geometry_collection()
        result = bind(places, self.get_sym_shape_gradient_final(), _skip_prepare=True)(
            actx, **context
        )

        return self.forward.apply_filter(actx, places, result)

    # }}}

    # {{{ evolution

    def evolution_source_term(
        self,
        t: float,
        state: GeometryEvolutionState,
        adjoint: "AdjointGeometryEvolutionState",
    ) -> "AdjointGeometryEvolutionState":
        # NOTE: this should have been cached!
        assert state.discr is not None
        grad = adjoint.evaluate_shape_gradient(state)

        # adjoint transverse equation is given by the advection equation
        #
        #   -dxstar/dt = (u - w) @ grad xstar + g
        #
        # and the adjoint time stepper handles the backward stepping

        actx = adjoint.array_context
        places = state.get_geometry_collection()

        if self.forward.evolution.force_normal_velocity:
            sym_u = sym.make_sym_vector("u", places.ambient_dim)
            sym_grad_xstar = sym.surface_gradient(
                self.ambient_dim, sym.var("xstar"), is_spectral=self.is_spectral
            )

            u = state.get_stokes_velocity()
            w = state.get_tangential_forcing()
            vstar = bind(places, sym_u @ sym_grad_xstar + sym.var("g"))(
                actx, g=grad, u=u - w, xstar=adjoint.x
            )
        else:
            vstar = grad

        return replace(
            adjoint,
            x=self.to_state_type(vstar),
            t=np.array([-1.0], dtype=object),
        )

    # }}}


@as_state_container
class AdjointGeometryEvolutionState:
    """
    .. automethod:: evaluate_shape_gradient
    """

    x: DOFArray
    t: np.ndarray
    wrangler: AdjointGeometryEvolutionWrangler

    # NOTE: Avoid broadcasting
    __array_ufunc__ = None

    @property
    def array_context(self):
        return self.x.array_context

    def evaluate_shape_gradient(self, state: GeometryEvolutionState):
        raise NotImplementedError


@dataclass(frozen=True)
class AdjointStokesEvolutionWrangler(AdjointGeometryEvolutionWrangler):
    forward: StokesEvolutionWrangler

    # {{{ forwarding

    @property
    def op(self):
        return self.forward.op

    def get_stokes_solution(self, actx, op, places, context=None):
        return self.forward.get_stokes_solution(actx, op, places, context=context)

    # }}}

    # {{{ adjoint Stokes operator

    @property
    @memoize_method
    def ad(self):
        from pystopt.stokes import (
            TwoPhaseSingleLayerStokesRepresentation,
            make_unsteady_adjoint_boundary_conditions,
        )

        assert isinstance(self.op, TwoPhaseSingleLayerStokesRepresentation)

        bc = make_unsteady_adjoint_boundary_conditions(
            self.ambient_dim, self.cost, xstar_name="xstar"
        )
        return TwoPhaseSingleLayerStokesRepresentation(bc)

    # }}}

    # {{{ initial state

    def get_initial_state(
        self,
        actx: ArrayContext,
        state: StokesEvolutionState,
    ) -> "AdjointStokesEvolutionState":
        # initial value for the adjoint transverse field
        xstar = self.evaluate_shape_gradient_final(
            actx,
            state,
            context={
                "q": state.get_stokes_density(),
                "u": state.get_stokes_velocity(),
            },
        )

        return AdjointStokesEvolutionState(
            x=xstar,
            t=np.array(state.t, dtype=object),
            wrangler=self,
        )

    # }}}

    def evolution_source_term(
        self,
        t: float,
        state: StokesEvolutionState,
        adjoint: "AdjointGeometryEvolutionState",
    ) -> "AdjointGeometryEvolutionState":
        # NOTE: this should have been cached!
        assert state.density is not None
        assert state.velocity is not None

        return super().evolution_source_term(t, state, adjoint)


@as_state_container
class AdjointStokesEvolutionState(AdjointGeometryEvolutionState):
    """
    .. automethod:: get_adjoint_density
    """

    # NOTE: these fields are cached explicitly so that they can be checkpointed
    density_star: np.ndarray | None = field(default=None, init=False, repr=False)
    shape_gradient: np.ndarray | None = field(default=None, init=False, repr=False)

    # NOTE: this is here to ensure that the cached fields above correspond to
    # just the one state and can't be overwritten!
    # TODO: need to figure out a better caching scheme here. The general
    # requirements are that:
    #   * to compute `qstar` (adjoint solution), we need `xstar` and the forward
    #   variables.
    #   * to compute the shape gradient, we need the same variables + `qstar`
    #   * to compute the cost gradient wrt to the control, we mostly need all
    #   the forward and adjoint variables.
    # So the caching is naturally on the `(state, adjoint)` tuple. We mostly
    # need to do this so that we don't recompute it for visualization, so meh :\
    _state: StokesEvolutionState | None = field(default=None, init=False, repr=False)

    # {{{ adjoint solution

    def _get_adjoint_solution(self, state: StokesEvolutionState):
        assert state.wrangler is self.wrangler.forward

        result = state._get_uncached_stokes_solution(
            self.density_star,
            self.wrangler.ad,
            context={
                "q": state.get_stokes_density(),
                "u": state.get_stokes_velocity(),
                "xstar": self.wrangler.from_state_type(self.x),
                "t": self.t[0],
            },
        )

        if self.density_star is None:
            object.__setattr__(self, "density_star", result.density)

        if self._state is None:
            object.__setattr__(self, "_state", state)

        assert self._state is state
        return replace(result, density_name="qstar")

    def get_adjoint_density(self, state: StokesEvolutionState):
        return self._get_adjoint_solution(state).density

    # }}}

    # {{{ optimization

    def evaluate_shape_gradient(self, state: StokesEvolutionState):
        assert state.wrangler is self.wrangler.forward

        if self.shape_gradient is None:
            result = self.wrangler.evaluate_shape_gradient(
                self.array_context,
                state.get_geometry_collection(),
                context={
                    "q": state.get_stokes_density(),
                    "u": state.get_stokes_velocity(),
                    "xstar": self.wrangler.from_state_type(self.x),
                    "qstar": self.get_adjoint_density(state),
                    "t": self.t[0],
                },
            )

            object.__setattr__(
                self, "shape_gradient", self.wrangler.from_state_type(result)
            )

        assert self._state is state
        return self.shape_gradient

    # }}}


def quasi_evolve_adjoint(
    actx: ArrayContext,
    wrangler: AdjointStokesEvolutionWrangler,
    step: TimeWrangler,
    *,
    tmax: float | None = None,
    maxit: float | None = None,
    callback: scb.CallbackManager | None = None,
    force_gc: bool = True,
    verbose: bool = True,
) -> AdjointStokesEvolutionState:
    if callback is None:
        callback = scb.CallbackManager()

    result = step.initial_state
    for event in step.run(tmax=tmax, maxit=maxit, verbose=verbose):
        result = event.adjoint_component

        if callback is not None:
            ret = callback(result, event=event)
        else:
            ret = 1

        if ret == 0:
            break

    return result


# }}}


# {{{ callbacks


def get_output_fields_stokes_evolution(
    state: StokesEvolutionState, **kwargs: Any
) -> dict[str, scb.OutputField]:
    event = kwargs["event"]

    from pystopt.simulation.common import get_output_fields_stokes

    fields = get_output_fields_stokes(state, **kwargs)

    fields["n"] = scb.OutputField.checkpoint("n", event.n)
    fields["t"] = scb.OutputField.checkpoint("t", event.t)
    fields["dt"] = scb.OutputField.checkpoint("dt", event.dt)

    return fields


@dataclass
class StokesEvolutionCallbackManager(scb.CallbackManager):
    def get_output_field_getters(self) -> tuple[scb.OutputFieldCallable, ...]:
        from pystopt.simulation.state import get_output_fields_geometry

        return (
            get_output_fields_geometry,
            get_output_fields_stokes_evolution,
        )

    def __call__(self, *args: Any, **kwargs: Any) -> int:
        if "event" not in kwargs:
            raise KeyError(f"'event' not passed to '{type(self).__name__}'")

        return super().__call__(*args, **kwargs)


@dataclass(frozen=True)
class StokesEvolutionVisualizeCallback(scb.VisualizeCallback):
    def get_file_writers(self) -> tuple[scb.OutputFieldVisualizeCallable, ...]:
        if self.ambient_dim == 2:
            return (
                scb.matplotlib_visualize_geometry_writer,
                scm.matplotlib_visualize_stokes_writer,
            )
        else:
            return super().get_file_writers()


@dataclass(frozen=True)
class StokesEvolutionHistoryCallback(scm.StokesHistoryCallback):
    time: list[np.ndarray] = field(default_factory=list, repr=False)
    centroid: list[np.ndarray] = field(default_factory=list, repr=False)

    def __call__(self, *args, **kwargs):
        r = super().__call__(*args, **kwargs)

        (state,) = args
        actx = state.array_context
        places = state.get_geometry_collection()

        from arraycontext import flatten

        xc = bind(
            places,
            sym.surface_centroid(places.ambient_dim, groupwise=True),
            auto_where=state.wrangler.dofdesc,
        )(actx)
        xc = actx.to_numpy(flatten(xc, actx)).reshape(places.ambient_dim, -1)

        self.centroid.append(xc)
        if isinstance(state, StokesEvolutionState):
            self.time.append(state.t[0])
        elif "event" in kwargs:
            self.time.append(kwargs["event"].t)
        else:
            raise ValueError("could not find time 't' in arguments")

        return r


def get_output_fields_adjoint_stokes_evolution(
    adjoint: AdjointStokesEvolutionState, **kwargs: Any
) -> dict[str, scb.OutputField]:
    wrangler = adjoint.wrangler
    if "event" in kwargs:
        state = kwargs["event"].state_component
    elif "state" in kwargs:
        state = kwargs["state"]
    else:
        raise ValueError("cannot determine state container")

    fields = {}
    fields["xstar"] = scb.OutputField(
        "xstar" if wrangler.ambient_dim == 3 else "X^*",
        wrangler.from_state_type(adjoint.x),
    )
    fields["shape_grad"] = scb.OutputField(
        "g_shape" if wrangler.ambient_dim == 3 else r"\mathbf{g}_\Sigma",
        adjoint.evaluate_shape_gradient(state),
    )

    return fields


@dataclass
class AdjointStokesEvolutionCallbackManager(scb.CallbackManager):
    def get_output_field_getters(self) -> tuple[scb.OutputFieldCallable, ...]:
        return (get_output_fields_adjoint_stokes_evolution,)


def matplotlib_visualize_adjoint_stokes_evolution_writer(
    vis: scb.VisualizeCallback,
    basename: "FileParts",
    state: scm.GeometryState,
    fields: dict[str, scb.OutputField],
    **kwargs: Any,
) -> None:
    if "xstar" not in fields:
        return

    filename = basename.with_suffix("xstar")
    vis.write_file(filename, scb.extract_fields(fields, ("xstar",)))

    filename = basename.with_suffix("shape_gradient")
    vis.write_file(filename, scb.extract_fields(fields, ("shape_grad",)))


@dataclass(frozen=True)
class AdjointStokesEvolutionVisualizeCallback(scb.VisualizeCallback):
    def get_file_writers(self) -> tuple[scb.OutputFieldVisualizeCallable, ...]:
        if self.ambient_dim == 2:
            return (matplotlib_visualize_adjoint_stokes_evolution_writer,)
        else:
            return super().get_file_writers()


# }}}
