# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

from dataclasses import dataclass, field, replace
from typing import Any

import numpy as np

from arraycontext import ArrayContext
from pytools import memoize_method
from pytools.obj_array import make_obj_array

import pystopt.callbacks as scb
from pystopt import sym
from pystopt.simulation.quasistokes import (
    AdjointStokesCostEvolutionState,
    AdjointStokesCostEvolutionWrangler,
    StokesCostEvolutionState,
    StokesCostEvolutionWrangler,
    StokesUnsteadyOptimizationCallbackManager,
    StokesUnsteadyOptimizationState,
    StokesUnsteadyOptimizationWrangler,
)
from pystopt.simulation.state import as_state_container
from pystopt.tools import get_default_logger

logger = get_default_logger(__name__)


# {{{ evolution


@dataclass(frozen=True)
class FarfieldStokesEvolutionWrangler(StokesCostEvolutionWrangler):
    # {{{ fields

    @property
    def fieldnames(self):
        raise NotImplementedError

    @property
    def nfields(self):
        return len(self.fieldnames)

    def state_to_fieldnames(self, state) -> dict[str, Any]:
        assert state.x.size == len(self.fieldnames)
        return {k: state.x[i] for i, k in enumerate(self.fieldnames)}

    @classmethod
    def make_sym_boundary_condition(cls, ambient_dim: int, **kwargs: Any):
        raise NotImplementedError

    # }}}


@dataclass(frozen=True)
class AdjointFarfieldStokesEvolutionWrangler(AdjointStokesCostEvolutionWrangler):
    forward: FarfieldStokesEvolutionWrangler

    def get_initial_state(
        self,
        actx: ArrayContext,
        state: StokesCostEvolutionState,
    ) -> AdjointStokesCostEvolutionState:
        r = super().get_initial_state(actx, state)
        return replace(r, dj=np.zeros(self.forward.nfields, dtype=object))

    # {{{ gradient

    @memoize_method
    def get_sym_gradient(self):
        from pystopt.operators import make_sym_density

        q = make_sym_density(self.op, "q")
        qstar = make_sym_density(self.ad, "qstar")

        import pystopt.derivatives as grad

        expr = grad.farfield(self.op.bc, self.op, q, self.ad, qstar)

        if self.cost is not None:
            expr += grad.farfield(self.cost, self.ambient_dim)

        from pystopt.symbolic.execution import prepare_expr

        expr = prepare_expr(self.places, expr, self.dofdesc)

        if not isinstance(expr, np.ndarray):
            expr = np.array([expr], dtype=object)

        return expr

    def evaluate_gradient(self, actx, places, *, context=None):
        g = super().evaluate_gradient(actx, places, context=context)
        return actx.to_numpy(g)

    # }}}


# }}}


# {{{ optimization


@dataclass(frozen=True)
class FarfieldOptimizationWrangler(StokesUnsteadyOptimizationWrangler):
    forward: FarfieldStokesEvolutionWrangler

    @property
    def state_cls(self):
        raise NotImplementedError

    def get_initial_state(self, actx: ArrayContext) -> "FarfieldOptimizationState":
        x = make_obj_array([self.forward.context[k] for k in self.forward.fieldnames])

        return self.state_cls(x=x, array_context=actx, wrangler=self)


@as_state_container
class FarfieldOptimizationState(StokesUnsteadyOptimizationState):
    wrangler: FarfieldOptimizationWrangler

    @memoize_method
    def _get_forward_wrangler(self):
        wrangler = self.wrangler.forward

        return replace(
            wrangler,
            context={
                **wrangler.context,
                **wrangler.state_to_fieldnames(self),  # pylint: disable=no-member
            },
        )


# }}}


# {{{ capillary


@dataclass(frozen=True)
class CapillaryStokesEvolutionWrangler(FarfieldStokesEvolutionWrangler):
    @property
    def fieldnames(self):
        return ("ca",)


@dataclass(frozen=True)
class AdjointCapillaryStokesEvolutionWrangler(AdjointFarfieldStokesEvolutionWrangler):
    def get_sym_gradient(self):
        assert abs(self.context["ca"]) != np.inf

        from pystopt.operators import make_sym_density

        q = make_sym_density(self.op, "q")
        qstar = make_sym_density(self.ad, "qstar")

        import pystopt.derivatives as grad

        expr = grad.capillary(self.op.bc, self.op, q, self.ad, qstar)

        # NOTE: this is actually always zero, but for completeness
        expr += grad.capillary(self.op, self.op, q, self.ad, qstar)

        if self.cost is not None:
            expr += grad.capillary(self.cost, self.ambient_dim)

        from pystopt.symbolic.execution import prepare_expr

        return np.array([prepare_expr(self.places, expr, self.dofdesc)], dtype=object)


@as_state_container
class CapillaryOptimizationState(FarfieldOptimizationState):
    pass


@dataclass(frozen=True)
class CapillaryOptimizationWrangler(FarfieldOptimizationWrangler):
    @property
    def state_cls(self):
        return CapillaryOptimizationState

    @property
    @memoize_method
    def adjoint(self):
        assert isinstance(self.forward, CapillaryStokesEvolutionWrangler)
        return AdjointCapillaryStokesEvolutionWrangler(forward=self.forward)


# }}}


# {{{ uniform


@dataclass(frozen=True)
class UniformFarfieldStokesEvolutionWrangler(FarfieldStokesEvolutionWrangler):
    def __post_init__(self):
        import pystopt.stokes.boundary_conditions as bc

        assert isinstance(self.op.bc, bc.UniformFlowBoundaryCondition)

    @property
    def fieldnames(self):
        if self.ambient_dim == 2:
            return ("uinf", "vinf")
        elif self.ambient_dim == 3:
            return ("uinf", "vinf", "winf")
        else:
            raise ValueError(f"unsupported ambient dimension: '{self.ambient_dim}'")

    @classmethod
    def make_sym_boundary_condition(cls, ambient_dim: int, **kwargs: Any):
        from pystopt.stokes.boundary_conditions import get_uniform_farfield

        uinf = make_obj_array(
            [
                sym.var("uinf"),
                sym.var("vinf"),
                sym.var("winf"),
            ][:ambient_dim]
        )

        return get_uniform_farfield(ambient_dim, uinf=uinf)


@dataclass(frozen=True)
class AdjointUniformFarfieldStokesEvolutionWrangler(
    AdjointFarfieldStokesEvolutionWrangler
):
    pass


@as_state_container
class UniformFarfieldOptimizationState(FarfieldOptimizationState):
    pass


@dataclass(frozen=True)
class UniformFarfieldOptimizationWrangler(FarfieldOptimizationWrangler):
    @property
    def state_cls(self):
        return UniformFarfieldOptimizationState

    @property
    @memoize_method
    def adjoint(self):
        assert isinstance(self.forward, UniformFarfieldStokesEvolutionWrangler)
        return AdjointUniformFarfieldStokesEvolutionWrangler(forward=self.forward)


# }}}


# {{{ solid body rotation


@dataclass(frozen=True)
class RotationFarfieldStokesEvolutionWrangler(FarfieldStokesEvolutionWrangler):
    def __post_init__(self):
        import pystopt.stokes.boundary_conditions as bc

        assert isinstance(self.op.bc, bc.SolidBodyRotationBoundaryCondition)

    @property
    def fieldnames(self):
        return ("omega",)

    @classmethod
    def make_sym_boundary_condition(cls, ambient_dim: int, **kwargs: Any):
        from pystopt.stokes.boundary_conditions import get_solid_body_rotation_farfield

        return get_solid_body_rotation_farfield(ambient_dim, omega=sym.var("omega"))


@dataclass(frozen=True)
class AdjointRotationFarfieldStokesEvolutionWrangler(
    AdjointFarfieldStokesEvolutionWrangler
):
    pass


@as_state_container
class RotationFarfieldOptimizationState(FarfieldOptimizationState):
    pass


@dataclass(frozen=True)
class RotationFarfieldOptimizationWrangler(FarfieldOptimizationWrangler):
    @property
    def state_cls(self):
        return RotationFarfieldOptimizationState

    @property
    @memoize_method
    def adjoint(self):
        assert isinstance(self.forward, RotationFarfieldStokesEvolutionWrangler)
        return AdjointRotationFarfieldStokesEvolutionWrangler(forward=self.forward)


# }}}


# {{{ time-dependent uniform farfield


@dataclass(frozen=True)
class BezierUniformFarfieldStokesEvolutionWrangler(FarfieldStokesEvolutionWrangler):
    def __post_init__(self):
        import pystopt.stokes.boundary_conditions as bc

        assert isinstance(self.op.bc, bc.BezierUniformFlowBoundaryCondition)

    @property
    def fieldnames(self):
        return tuple([f"bezier_p{i}" for i in range(self.op.bc.order)])

    @classmethod
    def make_sym_boundary_condition(cls, ambient_dim: int, **kwargs: Any):
        from pystopt.stokes.boundary_conditions import get_bezier_uniform_farfield

        order = kwargs.get("order", 3)
        points = tuple([
            sym.make_sym_vector(f"bezier_p{i}", ambient_dim) for i in range(order)
        ])

        return get_bezier_uniform_farfield(
            ambient_dim,
            points_or_order=points,
            tmax=sym.var("tmax"),
            tau=sym.var("t"),
        )


@dataclass(frozen=True)
class AdjointBezierUniformFarfieldStokesEvolutionWrangler(
    AdjointFarfieldStokesEvolutionWrangler
):
    pass


@as_state_container
class BezierUniformFarfieldOptimizationState(FarfieldOptimizationState):
    pass


@dataclass(frozen=True)
class BezierUniformFarfieldOptimizationWrangler(FarfieldOptimizationWrangler):
    @property
    def state_cls(self):
        return BezierUniformFarfieldOptimizationState

    @property
    @memoize_method
    def adjoint(self):
        assert isinstance(self.forward, BezierUniformFarfieldStokesEvolutionWrangler)
        return AdjointBezierUniformFarfieldStokesEvolutionWrangler(forward=self.forward)


# }}}


# {{{ time-dependent helical farfield


@dataclass(frozen=True)
class HelicalFarfieldStokesEvolutionWrangler(FarfieldStokesEvolutionWrangler):
    def __post_init__(self):
        import pystopt.stokes.boundary_conditions as bc

        assert isinstance(self.op.bc, bc.HelicalFlowBoundaryCondition)

    @property
    def fieldnames(self):
        return ("omega", "height")

    @classmethod
    def make_sym_boundary_condition(cls, ambient_dim: int, **kwargs: Any):
        from pystopt.stokes.boundary_conditions import get_helical_farfield

        return get_helical_farfield(
            ambient_dim,
            tmax=sym.var("tmax"),
            height=sym.var("height"),
            omega=sym.var("omega"),
        )


@dataclass(frozen=True)
class AdjointHelicalFarfieldStokesEvolutionWrangler(
    AdjointFarfieldStokesEvolutionWrangler
):
    pass


@as_state_container
class HelicalFarfieldOptimizationState(FarfieldOptimizationState):
    pass


@dataclass(frozen=True)
class HelicalFarfieldOptimizationWrangler(FarfieldOptimizationWrangler):
    @property
    def state_cls(self):
        return HelicalFarfieldOptimizationState

    @property
    @memoize_method
    def adjoint(self):
        assert isinstance(self.forward, HelicalFarfieldStokesEvolutionWrangler)
        return AdjointHelicalFarfieldStokesEvolutionWrangler(forward=self.forward)


# }}}


# {{{ callbacks


def get_output_fields_farfield(
    state: FarfieldOptimizationState, **kwargs: Any
) -> dict[str, scb.OutputField]:
    wrangler = state.wrangler.forward
    return {
        name: scb.OutputField.checkpoint(name, value)
        for name, value in wrangler.state_to_fieldnames(state).items()
    }


@dataclass
class FarfieldOptimizationCallbackManager(StokesUnsteadyOptimizationCallbackManager):
    def get_output_field_getters(self) -> tuple[scb.OutputFieldCallable, ...]:
        return (*super().get_output_field_getters(), get_output_fields_farfield)


@dataclass(frozen=True)
class FarfieldOptimizationHistoryCallback(scb.OptimizationHistoryCallback):
    parameters: list[np.ndarray] = field(default_factory=list, repr=False)

    def __call__(self, *args, **kwargs):
        r = super().__call__(*args, **kwargs)

        (state,) = args

        wrangler = state.wrangler.forward
        self.parameters.append(
            np.array(
                [
                    kwargs["fields"][name].value
                    for name in wrangler.state_to_fieldnames(state)
                ],
                dtype=np.float64,
            )
        )

        return r


# }}}
