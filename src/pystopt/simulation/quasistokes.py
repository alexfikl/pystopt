# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

"""
.. autoclass:: StokesUnsteadyOptimizationWrangler
.. autoclass:: StokesUnsteadyOptimizationState

.. autoclass:: GeometryCostEvolutionWrangler
.. autoclass:: GeometryCostEvolutionState

.. autoclass:: StokesCostEvolutionWrangler
.. autoclass:: StokesCostEvolutionState

.. autoclass:: AdjointStokesCostEvolutionWrangler
.. autoclass:: AdjointStokesCostEvolutionState
"""

from collections.abc import Iterator
from dataclasses import dataclass, replace
from typing import Any

import numpy as np

from arraycontext import ArrayContext
from pytential import GeometryCollection
from pytools import memoize_method

import pystopt.callbacks as scb
import pystopt.simulation.common as scm
import pystopt.simulation.unsteady as sun
from pystopt import bind, sym
from pystopt.cost import ShapeFunctional
from pystopt.evolution import TimeWrangler
from pystopt.paths import FileParts, PathLike
from pystopt.simulation.state import as_state_container
from pystopt.tools import get_default_logger

logger = get_default_logger(__name__)


# {{{ geometry evolution


@as_state_container
class GeometryCostEvolutionState(sun.GeometryEvolutionState):
    r"""A geometry evolved using a velocity field that also handles
    time-dependent cost functionals.

    Given a cost functional of the form

    .. math::

        J = \int_0^T g(t) \,\mathrm{d}t

    we can alternatively express it as an ODE

    .. math::

        \left\{
        \begin{aligned}
        \frac{\mathrm{d} j}{t} =\,\, & g(t), \\
        j(0) =\,\, & 0,
        \end{aligned}
        \right.

    such that :math:`J = j(T)`. This allows using the same time integrator
    as for the evolution equation and maintain the same order of convergence
    when computing the cost.

    .. attribute:: j

        The value of the cost integrated up to the current time, as explained
        above.

    .. automethod:: evaluate_cost
    .. automethod:: evaluate_cost_final
    """

    j: np.ndarray

    def evaluate_cost(self):
        raise NotImplementedError

    @memoize_method
    def evaluate_cost_final(self):
        if self.wrangler.cost_final is None:
            return 0

        return self.wrangler.evaluate_shape_cost_final(
            self.array_context,
            self.get_geometry_collection(),
            context={"t": self.t[0]},
        )


@dataclass(frozen=True)
class GeometryCostEvolutionWrangler(sun.GeometryEvolutionWrangler):
    r"""An evolution wrangler that handles costs of the form

    .. math::

        J = J_t + J_T = \int_0^T g(t, \mathbf{X}) \,\mathrm{d}t + g_T(\mathbf{X})

    with a term that considers contributions from the open time horizon
    :math:`(0, T)` and a term that considers the state at the final time.
    Either one of these can be omitted.

    .. attribute:: cost

        The time-dependent term :math:`J_t` in the cost functional.

    .. attribute:: cost_final

        The final time term :math:`J_T` in the cost functional.

    .. automethod:: evaluate_shape_cost
    .. automethod:: evaluate_shape_cost_final
    """

    cost: ShapeFunctional | None
    cost_final: ShapeFunctional | None

    # {{{ cost

    @memoize_method
    def get_sym_shape_cost(self):
        if self.cost is None:
            return 0

        expr = self.cost(self.ambient_dim, dofdesc=self.dofdesc)

        from pystopt.symbolic.execution import prepare_expr

        return prepare_expr(self.places, expr, self.dofdesc)

    def evaluate_shape_cost(
        self,
        actx: ArrayContext,
        places: GeometryCollection,
        *,
        context: dict[str, Any] | None = None,
    ) -> float:
        if context is None:
            context = {}
        context = {**self.context, **context}

        r = bind(places, self.get_sym_shape_cost(), _skip_prepare=True)(actx, **context)

        return actx.to_numpy(r)

    # }}}

    # {{{ cost_final

    @memoize_method
    def get_sym_shape_cost_final(self):
        if self.cost_final is None:
            return 0

        expr = self.cost_final(self.ambient_dim, dofdesc=self.dofdesc)

        from pystopt.symbolic.execution import prepare_expr

        return prepare_expr(self.places, expr, self.dofdesc)

    def evaluate_shape_cost_final(
        self,
        actx: ArrayContext,
        places: GeometryCollection,
        *,
        context: dict[str, Any] | None = None,
    ) -> float:
        if context is None:
            context = {}
        context = {**self.context, **context}

        r = bind(places, self.get_sym_shape_cost_final(), _skip_prepare=True)(
            actx, **context
        )

        return actx.to_numpy(r)

    # }}}

    def evolution_source_term(
        self,
        t: float,
        state: GeometryCostEvolutionState,
    ) -> GeometryCostEvolutionState:
        v = super().evolution_source_term(t, state)
        return replace(v, j=np.array([state.evaluate_cost()], dtype=object))


# }}}


# {{{ stokes forward evolution


@as_state_container
class StokesCostEvolutionState(sun.StokesEvolutionState, GeometryCostEvolutionState):
    @memoize_method
    def evaluate_shape_cost(self):
        return self.wrangler.evaluate_shape_cost(
            self.array_context,
            self.get_geometry_collection(),
            context={
                "q": self.get_stokes_density(),
                "u": self.get_stokes_velocity(),
                "t": self.t[0],
            },
        )

    def evaluate_cost(self):
        return self.evaluate_shape_cost()


@dataclass(frozen=True)
class StokesCostEvolutionWrangler(
    sun.StokesEvolutionWrangler, GeometryCostEvolutionWrangler
):
    def get_initial_state(self, actx: ArrayContext) -> StokesCostEvolutionState:
        x0 = super().get_initial_state(actx)
        return StokesCostEvolutionState(
            x=x0.x,
            t=np.array([0.0], dtype=object),
            j=np.array([0.0], dtype=object),
            wrangler=self,
        )


# }}}


# {{{ adjoint evolution


@as_state_container
class AdjointStokesCostEvolutionState(sun.AdjointStokesEvolutionState):
    dj: np.ndarray

    # {{{ optimization

    def evaluate_gradient(self, state: StokesCostEvolutionState):
        return self.wrangler.evaluate_gradient(
            self.array_context,
            state.get_geometry_collection(),
            context={
                "q": state.get_stokes_density(),
                "u": state.get_stokes_velocity(),
                "xstar": self.wrangler.from_state_type(self.x),
                "qstar": self.get_adjoint_density(state),
                "t": self.t[0],
            },
        )

    # }}}


@dataclass(frozen=True)
class AdjointStokesCostEvolutionWrangler(sun.AdjointStokesEvolutionWrangler):
    forward: StokesCostEvolutionWrangler

    def get_initial_state(
        self,
        actx: ArrayContext,
        state: StokesCostEvolutionState,
    ) -> AdjointStokesCostEvolutionState:
        x0 = super().get_initial_state(actx, state)
        return AdjointStokesCostEvolutionState(
            x=x0.x,
            wrangler=self,
            dj=np.array([0.0], dtype=object),
            t=np.array(state.t, dtype=object),
        )

    # {{{ cost

    @property
    def cost(self):
        return self.forward.cost

    # }}}

    # {{{ cost final

    @property
    def cost_final(self):
        return self.forward.cost_final

    # }}}

    # {{{ gradient

    def get_sym_gradient(self) -> sym.var:
        raise NotImplementedError

    def evaluate_gradient(
        self,
        actx: ArrayContext,
        places: GeometryCollection,
        *,
        context: dict[str, Any] | None = None,
    ) -> np.ndarray:
        if context is None:
            context = {}
        context = {**self.context, **context}

        return bind(places, self.get_sym_gradient(), _skip_prepare=True)(
            actx, **context
        )

    # }}}

    # {{{ evolution

    def evolution_source_term(
        self,
        t: float,
        state: StokesCostEvolutionState,
        adjoint: AdjointStokesCostEvolutionState,
    ) -> AdjointStokesCostEvolutionState:
        result = super().evolution_source_term(t, state, adjoint)
        return replace(result, dj=adjoint.evaluate_gradient(state))

    # }}}


# }}}


# {{{ wrangler


def vdot(
    x: "StokesUnsteadyOptimizationState", y: "StokesUnsteadyOptimizationState"
) -> float:
    if isinstance(x, StokesUnsteadyOptimizationState):
        assert x.wrangler is y.wrangler
        return np.vdot(x.x, y.x)
    else:
        return np.vdot(x, y)


@dataclass(frozen=True)
class StokesUnsteadyOptimizationWrangler:
    r"""A wrangler for doing optimization on the quasi-static Stokes equations.

    By default, this does not actually compute any cost and gradient for
    optimization, but only sets up the appropriate forward and adjoint
    evolution equations.

    The optimization supports cost functionals of of the form

    .. math::

        J = \int_0^T j_t(\mathbf{u}, \mathbf{X}) \,\mathrm{d}t
            + j_T(\mathbf{X}(T))

    with a time-dependent component :math:`j_t` (given by
    :attr:`~GeometryCostEvolutionWrangler.cost`)
    and a final-time component :math:`j_T` (given by
    :attr:`~GeometryCostEvolutionWrangler.cost_final`).

    .. attribute:: ambient_dim

    .. attribute:: dofdesc

    .. attribute:: time

        A :class:`~pystopt.evolution.TimeWrangler` used for the forward and
        adjoint evolution.

    .. attribute:: forward

        A :class:`StokesCostEvolutionWrangler`
        describing the forward evolution.

    .. attribute:: adjoint

        A :class:`AdjointStokesCostEvolutionWrangler` describing the adjoint
        evolution. This wrangler is completely defined from the forward problem
        and uses :class:`~pystopt.stokes.AdjointBoundaryCondition` for the
        boundary condition.

    .. attribute:: checkpoint_directory_series

        A generator for directory names that will contain any output from the
        forward and adjoint evolution equations. This can also always return
        the same directory name, in which case it will overwrite the
        previous data.

    .. automethod:: get_initial_state
    .. automethod:: evolve_forward
    .. automethod:: evolve_adjoint
    """

    time: TimeWrangler
    forward: StokesCostEvolutionWrangler
    checkpoint_directory_series: Iterator[FileParts]

    overwrite: bool

    # {{{ properties

    @property
    def ambient_dim(self):
        return self.forward.ambient_dim

    @property
    def context(self):
        return self.forward.context

    @property
    def dofdesc(self):
        return self.forward.dofdesc

    @property
    @memoize_method
    def adjoint(self):
        return AdjointStokesCostEvolutionWrangler(forward=self.forward)

    @property
    def vdot(self):
        return vdot

    # }}}

    # {{{ initial state

    def get_initial_state(
        self, actx: ArrayContext
    ) -> "StokesUnsteadyOptimizationState":
        raise NotImplementedError

    # }}}

    # {{{ output

    def get_forward_callback(
        self,
        actx: ArrayContext,
        checkpoint_directory_name: PathLike,
    ) -> scb.CallbackManager:
        """Construct a :class:`~pystopt.callbacks.CallbackManager` to be used
        in :meth:`evolve_forward`.

        By default, no callback is given, so no visualization or checkpointing
        of the individual iterations is performed.
        """
        return scb.CallbackManager()

    def get_adjoint_callback(
        self,
        actx: ArrayContext,
        checkpoint_directory_name: PathLike,
    ) -> scb.CallbackManager:
        """Construct a :class:`~pystopt.callbacks.CallbackManager` to be used
        in :meth:`evolve_adjoint`.

        By default, no callback is given, so no visualization or checkpointing
        of the individual iterations is performed.
        """
        return scb.CallbackManager()

    # }}}

    # {{{ evolution

    def evolve_forward(
        self,
        state: "StokesUnsteadyOptimizationState",
        wrangler: StokesCostEvolutionWrangler,
    ) -> StokesCostEvolutionState:
        """Evolve the quasi-static Stokes equations forward in time from
        *state* using *wrangler*.

        :arg checkpoint_directory_name: a given directory to store checkpointed
            data and other visualization files.
        :returns: a :class:`StokesCostEvolutionState` containing the geometry
            at the final time and the cost functional value, integrated
            over all the time interval.
        """

        def asarray(x):
            return np.array(x).astype(np.float64)

        from pystopt.tools import dc_stringify

        logger.info("\n%s", dc_stringify(wrangler.context))
        assert all(
            np.allclose(asarray(wrangler.context[name]), asarray(value))
            for name, value in wrangler.state_to_fieldnames(state).items()
        )

        checkpoint_directory_name = state.checkpoint_directory_name
        if checkpoint_directory_name.exists():
            if self.overwrite:
                import shutil

                shutil.rmtree(checkpoint_directory_name)
            else:
                raise FileExistsError(
                    f"checkpoint already exists: '{checkpoint_directory_name}'"
                )

        actx = state.array_context
        step = state.get_time_stepper()
        callback = self.get_forward_callback(actx, checkpoint_directory_name)

        # {{{ evolve

        result = sun.quasi_evolve_stokes(
            actx,
            wrangler,
            step,
            tmax=self.time.tmax,
            maxit=self.time.maxit,
            callback=callback,
        )

        if wrangler.cost_final is not None:
            result = replace(result, j=result.j + result.evaluate_cost_final())

        # }}}

        # {{{ checkpoint

        # TODO: checkpoint this properly

        checkpoint = step.leap.checkpoint
        discr = wrangler.places.get_discretization(wrangler.dofdesc.geometry)
        checkpoint.write_to("orig_discr", discr, overwrite=True)

        if "history" in callback:
            checkpoint.write_to(
                "history", callback["history"], overwrite=self.overwrite
            )

        # }}}

        return result

    def evolve_adjoint(
        self,
        state: "StokesUnsteadyOptimizationState",
        final_evolution_state: StokesCostEvolutionState,
        wrangler: AdjointStokesCostEvolutionWrangler,
    ) -> AdjointStokesCostEvolutionState:
        """Evolve the quasi-static adjoint Stokes equations backward in time from
        *state* using *wrangler*, starting from the final solution of the
        forward problem *final_evolution_state* (as given by
        :meth:`evolve_forward`).

        :arg checkpoint_directory_name: a given directory to read checkpointed
            data from and store other visualization files.
        :returns: an :class:`AdjointStokesCostEvolutionState` containing the
            adjoint transverse field at :math:`t = 0` and the gradient,
            integrated over all the time interval.
        """
        if not state.checkpoint_directory_name.exists():
            raise RuntimeError("'checkpoint_directory_name' does not exist")

        # TODO: does anyone actually close all these checkpoints we keep
        # opening? Ideally they get closed when the `StokesUnsteadyOptimizationState`
        # gets deleted, since they're cached there?

        actx = state.array_context

        # {{{ get adjoint stepper

        state0 = wrangler.get_initial_state(actx, final_evolution_state)
        step = state.get_time_stepper().make_adjoint(
            "adjoint", state0, wrangler.evolution_source_term
        )

        # }}}

        from pystopt.checkpoint.hdf import wrangler_for_pickling

        with wrangler_for_pickling(wrangler.forward):
            callback = self.get_adjoint_callback(actx, state.checkpoint_directory_name)
            return sun.quasi_evolve_adjoint(actx, wrangler, step, callback=callback)

    # }}}


@dataclass(frozen=True)
class StokesUnsteadyOptimizationState:
    """
    .. attribute:: wrangler
    .. attribute:: checkpoint_directory_name

        Directory containing the checkpointed data for the forward problem.

    .. attribute:: cost
    .. attribute:: gradient
    """

    x: np.ndarray
    wrangler: StokesUnsteadyOptimizationWrangler
    array_context: ArrayContext

    def get_geometry_collection(self):
        return self._get_forward_evolution_solution().get_geometry_collection()

    # {{{ time stepping

    @property
    @memoize_method
    def checkpoint_directory_name(self):
        return next(self.wrangler.checkpoint_directory_series).aspath()

    @memoize_method
    def get_time_stepper(self):
        wrangler = self._get_forward_wrangler()
        state0 = wrangler.get_initial_state(self.array_context)

        return self.wrangler.time.make_stepper(
            state0,
            wrangler.evolution_source_term,
            wrangler.estimate_time_step,
            checkpoint_directory_name=self.checkpoint_directory_name,
        )

    # }}}

    # {{{ evolution

    @memoize_method
    def _get_forward_wrangler(self):
        raise NotImplementedError

    @memoize_method
    def _get_forward_evolution_solution(self):
        return self.wrangler.evolve_forward(self, self._get_forward_wrangler())

    @memoize_method
    def _get_adjoint_wrangler(self):
        return replace(self.wrangler.adjoint, forward=self._get_forward_wrangler())

    @memoize_method
    def _get_adjoint_evolution_solution(self):
        return self.wrangler.evolve_adjoint(
            self,
            self._get_forward_evolution_solution(),
            self._get_adjoint_wrangler(),
        )

    # }}}

    # {{{{ optimization

    def evaluate_cost(self):
        # NOTE: for now, j always only contains the scalar cost functional,
        # so return the value itself (not wrapped in an object array)
        return self._get_forward_evolution_solution().j[0]

    def evaluate_gradient(self):
        return self._get_adjoint_evolution_solution().dj

    # }}}


# }}}


# {{{ callbacks


def get_output_fields_geometry_wrapper(
    state: StokesUnsteadyOptimizationState, **kwargs: Any
) -> dict[str, scb.OutputField]:
    from pystopt.simulation.state import get_output_fields_geometry

    return get_output_fields_geometry(state._get_forward_evolution_solution(), **kwargs)


def get_output_fields_stokes_wrapper(
    state: StokesUnsteadyOptimizationState, **kwargs: Any
) -> dict[str, scb.OutputField]:
    from pystopt.simulation.common import get_output_fields_stokes

    return get_output_fields_stokes(state._get_forward_evolution_solution(), **kwargs)


def get_output_fields_adjoint_stokes_wrapper(
    state: StokesUnsteadyOptimizationState, **kwargs: Any
) -> dict[str, scb.OutputField]:
    from pystopt.simulation.unsteady import get_output_fields_adjoint_stokes_evolution

    return get_output_fields_adjoint_stokes_evolution(
        state._get_adjoint_evolution_solution(),
        state=state._get_forward_evolution_solution(),
        **kwargs,
    )


@dataclass
class StokesUnsteadyOptimizationCallbackManager(scb.CallbackManager):
    def get_output_field_getters(self) -> tuple[scb.OutputFieldCallable, ...]:
        return (
            scm.get_output_fields_optimization,
            get_output_fields_geometry_wrapper,
            get_output_fields_stokes_wrapper,
            get_output_fields_adjoint_stokes_wrapper,
        )

    def __call__(self, *args: Any, **kwargs: Any) -> int:
        if "info" not in kwargs:
            raise KeyError(f"'info' not passed to '{type(self).__name__}'")

        def unwrap(x):
            return getattr(x, "x", x)

        (state,) = args
        info = kwargs.pop("info")
        info = replace(info, g=unwrap(info.g), d=unwrap(info.d))

        return super().__call__(state, info=info, **kwargs)


@dataclass(frozen=True)
class StokesUnsteadyOptimizationVisualizeCallback(scb.VisualizeCallback):
    def get_file_writers(self) -> tuple[scb.OutputFieldVisualizeCallable, ...]:
        if self.ambient_dim == 2:
            from pystopt.simulation.unsteady import (
                matplotlib_visualize_adjoint_stokes_evolution_writer,
            )

            return (
                scb.matplotlib_visualize_geometry_writer,
                scm.matplotlib_visualize_stokes_writer,
                matplotlib_visualize_adjoint_stokes_evolution_writer,
            )
        else:
            return super().get_file_writers()


# }}}
