# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

"""
These functions can be used to easily encapsulate and perform shape optimization
without constraints. The general workflow using these classes is

.. code:: python

    wrangler = UnconstrainedShapeOptimizationWrangler(
        cost=cost,
        context=context,
        filter_type=OutputType.Unfiltered,
        filter_arguments={},
        )
    state0 = wrangler.get_initial_state(actx)

    from pystopt.optimize import minimize_steepest
    result = minimize_steepest(
        fun=lambda x: x.cost,
        x0=state0,
        jac=lambda x: x.wrap(x.gradient),
        callback=UnconstrainedShapeOptimizationCallbackManager(...),
        options={"rtol": 1.0e-3}
        )

All the complexity of reconstructing the geometry, applying filters and
computing the cost and gradient are handled by the wrangler and state
classes. Furthermore, the reconstructed values are cached on the state,
so they are never out of sync or unnecessarily recomputed.

.. autofunction:: make_unconstrained_shape_optimization_wrangler

.. autoclass:: UnconstrainedShapeOptimizationWrangler
.. autoclass:: UnconstrainedShapeOptimizationState

.. autoclass:: UnconstrainedShapeOptimizationCallbackManager
.. autoclass:: UnconstrainedShapeOptimizationVisualizeCallback
"""

from dataclasses import dataclass
from typing import Any

from arraycontext import ArrayContext
from pytential import GeometryCollection
from pytools import memoize_method

import pystopt.callbacks as scb
import pystopt.simulation.common as scm
from pystopt import bind, sym
from pystopt.cost import ShapeFunctional
from pystopt.filtering import FilterType
from pystopt.simulation.state import as_state_container
from pystopt.tools import get_default_logger

logger = get_default_logger(__name__)


# {{{ wrangler


@dataclass(frozen=True)
class UnconstrainedShapeOptimizationWrangler(scm.ShapeOptimizationWrangler):
    def get_initial_state(
        self,
        actx: ArrayContext,
    ) -> "UnconstrainedShapeOptimizationState":
        from pystopt.simulation.state import get_geometry_state_as_cls

        return get_geometry_state_as_cls(
            actx, self, cls=UnconstrainedShapeOptimizationState
        )

    @memoize_method
    def get_sym_shape_gradient(self):
        import pystopt.derivatives as grad

        expr = grad.shape(self.cost, self.ambient_dim, dofdesc=self.dofdesc)

        from pystopt.symbolic.execution import prepare_expr

        return prepare_expr(self.places, expr, self.dofdesc)


@as_state_container
class UnconstrainedShapeOptimizationState(scm.ShapeOptimizationState):
    pass


def make_unconstrained_shape_optimization_wrangler(
    places: GeometryCollection,
    cost: ShapeFunctional,
    *,
    is_spectral: bool = True,
    dofdesc: sym.DOFDescriptorLike | None = None,
    context: dict[str, Any] | None = None,
    filter_type: FilterType = FilterType.Unfiltered,
    filter_arguments: dict[str, Any] | None = None,
) -> UnconstrainedShapeOptimizationWrangler:
    """Construct a :class:`UnconstrainedShapeOptimizationWrangler`."""
    dofdesc = places.auto_source if dofdesc is None else sym.as_dofdesc(dofdesc)

    if context is None:
        context = {}

    if filter_arguments is None:
        filter_arguments = {}

    return UnconstrainedShapeOptimizationWrangler(
        places=places,
        dofdesc=dofdesc,
        is_spectral=is_spectral,
        cost=cost,
        context=context,
        filter_type=filter_type,
        filter_arguments=filter_arguments,
    )


# }}}


# {{{ callbacks


def get_output_fields_unconstrained_shape_optimization(
    state: UnconstrainedShapeOptimizationState, **kwargs: Any
) -> dict[str, scb.OutputField]:
    if "xd" not in state.wrangler.context:
        return {}

    places = state.get_geometry_collection()
    dofdesc = state.wrangler.dofdesc
    actx = state.array_context

    discr = places.get_discretization(dofdesc.geometry, dofdesc.discr_stage)
    x = actx.thaw(discr.nodes())

    dofdesc_d = state.wrangler.cost.dofdesc
    discr_d = places.get_discretization(dofdesc_d.geometry, dofdesc_d.discr_stage)
    xd = actx.thaw(discr_d.nodes())

    e = x - xd
    e_dot_n = bind(places, sym.n_dot(places.ambient_dim), auto_where=dofdesc)(actx, x=e)

    fields = {}
    fields["e"] = scb.OutputField.visualize(
        "e" if places.ambient_dim == 3 else r"\mathbf{e}", e
    )
    fields["e_dot_n"] = scb.OutputField.visualize(
        "e_dot_n" if places.ambient_dim == 3 else r"\mathbf{e} \cdot \mathbf{n}",
        e_dot_n,
    )

    return fields


@dataclass
class UnconstrainedShapeOptimizationCallbackManager(
    scm.ShapeOptimizationCallbackManager
):
    def get_output_field_getters(self) -> tuple[scb.OutputFieldCallable, ...]:
        return (
            *super().get_output_field_getters(),
            get_output_fields_unconstrained_shape_optimization,
        )


def matplotlib_visualize_unconstrained_writer(
    vis: scb.VisualizeCallback,
    basename,
    state: scm.GeometryState,
    fields: dict[str, scb.OutputField],
    **kwargs: Any,
) -> None:
    if "e" not in fields:
        return

    vis.write_file(
        basename.with_suffix("error"),
        scb.extract_fields(fields, ("e", "e_dot_n")),
    )


@dataclass(frozen=True)
class UnconstrainedShapeOptimizationVisualizeCallback(scb.VisualizeCallback):
    def get_file_writers(self) -> tuple[scb.OutputFieldVisualizeCallable, ...]:
        if self.ambient_dim == 2:
            return (
                scb.matplotlib_visualize_geometry_writer,
                scm.matplotlib_visualize_shape_optimization_writer,
                matplotlib_visualize_unconstrained_writer,
            )
        else:
            return super().get_file_writers()


# }}}
