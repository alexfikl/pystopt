# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

"""
.. currentmodule:: pystopt.simulation

.. autofunction:: as_state_container
.. autofunction:: reconstruct_discretization

.. autoclass:: GeometryWrangler
.. autoclass:: GeometryState
.. autoclass:: make_geometry_wrangler

.. autoclass:: MeshQualityHistoryCallback
"""

import sys
from dataclasses import dataclass, field
from typing import Any

import numpy as np

from arraycontext import ArrayContainerT, ArrayContext
from meshmode.discretization import Discretization
from pytential import GeometryCollection
from pytools import memoize_method

from pystopt import sym
from pystopt.callbacks import OutputField, PerformanceHistoryCallback
from pystopt.tools import get_default_logger

logger = get_default_logger(__name__)


# {{{ dataclass container


def get_dataclass_array_fields(cls: type) -> type:
    """A decorator based on :func:`arraycontext.dataclass_array_container`
    that allows :class:`typing.Optional` containers and `init=False` fields.
    """

    from dataclasses import Field, fields, is_dataclass
    from typing import Union, get_args, get_origin

    assert is_dataclass(cls)

    def is_array_field(f: Field) -> bool:
        from arraycontext.container.dataclass import is_array_type

        if isinstance(f.type, str):
            raise TypeError(f"String annotation on field '{f.name}' not supported")

        origin = get_origin(f.type)
        if origin is Union:
            # NOTE: `Optional` is caught in here as an alias for `Union[Anon, type]`
            return all(
                is_array_type(arg) or isinstance(arg, type(None))
                for arg in get_args(f.type)
            )

        from typing import _GenericAlias, _SpecialForm  # type: ignore[attr-defined]

        if isinstance(f.type, _GenericAlias | _SpecialForm):
            return False

        return is_array_type(f.type)

    from pytools import partition

    array_fields, non_array_fields = partition(
        is_array_field, [f for f in fields(cls) if f.init]
    )

    if not array_fields and non_array_fields:
        raise ValueError(
            f"'{cls.__module__}.{cls.__name__}' must have fields with array "
            "container type in order to use the 'dataclass_array_container' "
            "decorator. Found only non-array fields: '{}'".format(
                "', '".join([f.name for f in non_array_fields])
            )
        )

    return array_fields, non_array_fields


def as_state_container(cls: type) -> type:
    """A decorator for subclasses of :class:`GeometryState`.

    It transforms the subclass into a dataclass with the minimum required
    arithmetic operations from :mod:`arraycontext`.
    """
    if getattr(sys, "_BUILDING_SPHINX_DOCS", False):
        return cls

    from arraycontext import with_container_arithmetic
    from arraycontext.container.dataclass import _inject_dataclass_serialization

    # make into a dataclass
    cls = dataclass(cls, frozen=True, eq=False, order=False)

    # add container serialization
    array_fields, non_array_fields = get_dataclass_array_fields(cls)
    cls = _inject_dataclass_serialization(cls, array_fields, non_array_fields)

    # add arithmetic
    cls = with_container_arithmetic(
        arithmetic=True,
        bcast_obj_array=False,
        matmul=False,
        bitwise=False,
        shift=False,
        rel_comparison=False,
        eq_comparison=False,
        _cls_has_array_context_attr=True,
    )(cls)

    return cls


# }}}


# {{{ reconstruct_discretization


def reconstruct_discretization(
    actx: ArrayContext,
    orig_discr: Discretization,
    xlm: np.ndarray,
    *,
    keep_vertices: bool = False,
) -> Discretization:
    """Reconstruct the geometry pointed at by *dofdesc* using the
    nodes from *xlm*.

    The *xlm* variable must match the number of nodes or the spectral
    representation on the geometry described by *dofdesc*.

    :arg keep_vertices: if *True*, the vertices are reconstructed by interpolating
        from the nodes *xlm*. Otherwise, they are set to *None* and assumed to
        be unused in the new discretization.
    :returns: a new :class:`~meshmode.discretization.Discretization`.
    """
    assert xlm.size == orig_discr.ambient_dim

    from pystopt.dof_array import SpectralDOFArray
    from pystopt.mesh.spharm import SpectralDiscretization

    if isinstance(orig_discr, SpectralDiscretization):
        if not isinstance(xlm[0], SpectralDOFArray):
            from warnings import warn

            warn(
                f"passed a '{type(xlm[0]).__name__}' to 'reconstruct_discretization'",
                UserWarning,
                stacklevel=2,
            )
            xlm = orig_discr.to_spectral_conn(xlm)

        from pystopt.mesh import update_discr_from_spectral

        discr = update_discr_from_spectral(
            actx, orig_discr, xlm, keep_vertices=keep_vertices
        )
    elif isinstance(orig_discr, Discretization):
        if any(isinstance(x, SpectralDOFArray) for x in xlm):
            raise NotImplementedError(
                f"{type(orig_discr).__name__} does not support SpectralDOFArray"
            )

        from pystopt.mesh.processing import update_discr_mesh_from_nodes

        mesh = update_discr_mesh_from_nodes(
            actx, orig_discr, xlm, keep_vertices=keep_vertices
        )

        discr = orig_discr.copy(mesh=mesh)
    else:
        raise NotImplementedError(type(orig_discr).__name__)

    return discr


# }}}


# {{{ wrangler


def vdot(x: "GeometryState", y: "GeometryState") -> float:
    assert x.array_context is y.array_context

    # NOTE: this vdot take advantage of the fact that (x, y) are
    # always real function in physical space. Then the Plancherel
    # theorem (up to some scaling)
    #   int(x^* y) = sum(FFT(x)^* FFT(y))
    # implies that the vdot of the FFTs should always be real as well.

    actx = x.array_context
    r = actx.to_numpy(actx.np.vdot(x, y))
    if abs(np.imag(r)) > 1.0e-10:
        from warnings import warn

        warn(f"Plancherel not respected <x, y> = {r}", UserWarning, stacklevel=2)

    return np.real(r)


def get_geometry_state_as_cls(
    actx: ArrayContext, wrangler: "GeometryWrangler", cls: type | None = None
) -> "GeometryState":
    if cls is None:
        cls = GeometryState

    if wrangler.is_spectral:
        x = actx.thaw(wrangler.discr.xlm)
    else:
        x = actx.thaw(wrangler.discr.nodes())

    return cls(x=x, wrangler=wrangler)


@dataclass(frozen=True)
class GeometryWrangler:
    """
    .. attribute:: ambient_dim

    .. attribute:: places

        A :class:`~pytential.collection.GeometryCollection` that contains all the
        geometries in the simulation. These are just used to describe the
        connectivity and initial state of the geometry.

    .. attribute:: dofdesc

        A :class:`~pytential.symbolic.dof_desc.DOFDescriptor` for the
        discretization being evolved, optimized or otherwise modified.

    .. attribute:: is_spectral

        If *True*, the state is represented by the spectral coefficients of the
        geometry coordinates. Otherwise, the physical coordinates are used.

    .. automethod:: get_initial_state
    .. automethod:: to_state_type
    .. automethod:: from_state_type
    """

    places: GeometryCollection
    dofdesc: sym.DOFDescriptor
    is_spectral: bool

    @property
    def ambient_dim(self):
        return self.places.ambient_dim

    @property
    def discr(self):
        return self.places.get_discretization(
            self.dofdesc.geometry, self.dofdesc.discr_stage
        )

    @property
    def keep_vertices(self):
        return False

    @property
    def vdot(self):
        return vdot

    def get_initial_state(self, actx: ArrayContext) -> "GeometryState":
        """
        :returns: a :class:`GeometryState` using the nodes of :attr:`dofdesc`
            as found in :attr:`places`.
        """
        return get_geometry_state_as_cls(actx, self)

    def to_state_type(self, ary: ArrayContainerT) -> ArrayContainerT:
        """Projects *ary* to match the type of data in the :class:`GeometryState`
        handled by this wrangler.

        If :attr:`is_spectral` is *True*, *ary* is expected to be a
        :class:`~meshmode.dof_array.DOFArray` which will be projected to its
        spectral coefficients. Otherwise, it is returned as is.
        """
        from pystopt.mesh.spectral import to_spectral

        if self.is_spectral:
            return to_spectral(self.discr, ary, strict=False)
        else:
            return ary

    def from_state_type(self, ary: ArrayContainerT) -> ArrayContainerT:
        """Projects *ary* from the type of data in the :class:`GeometryState`
        handled by this wrangler back to physical space.

        If :attr:`is_spectral` is *True*, *ary* is expected to be a
        :class:`~pystopt.dof_array.SpectralDOFArray` of spectral coefficients,
        which will be projected to physical space. Otherwise, it is returned as is.
        """
        from pystopt.mesh.spectral import from_spectral

        if self.is_spectral:
            return from_spectral(self.discr, ary, strict=False)
        else:
            return ary

    def merge_collection(self, discr: Discretization) -> GeometryCollection:
        from pytential.qbx import QBXLayerPotentialSource

        lpot = self.places.get_geometry(self.dofdesc.geometry)
        if isinstance(lpot, QBXLayerPotentialSource):
            discr = lpot.copy(density_discr=discr)

        return self.places.merge({self.dofdesc.geometry: discr})


def make_geometry_wrangler(
    places: GeometryCollection,
    *,
    dofdesc: sym.DOFDescriptorLike | None = None,
    is_spectral: bool = True,
) -> GeometryWrangler:
    dofdesc = places.auto_source if dofdesc is None else sym.as_dofdesc(dofdesc)

    return GeometryWrangler(places=places, dofdesc=dofdesc, is_spectral=is_spectral)


# }}}


# {{{ state


@as_state_container
class GeometryState:
    """
    .. attribute:: x

        A set of nodes (or spectral coefficients for the same nodes) that
        describe the geometry from *wrangler*.

    .. attribute:: wrangler

    .. automethod:: get_geometry_collection
    """

    x: np.ndarray
    wrangler: GeometryWrangler

    # NOTE: these fields are cached explicitly so that they can be checkpointed
    discr: Discretization | None = field(default=None, init=False, repr=False)

    # NOTE: Avoid broadcasting
    __array_ufunc__ = None

    @property
    def array_context(self):
        return self.x[0].array_context

    @array_context.setter
    def array_context(self, actx):
        pass

    # {{{ collection

    @memoize_method
    def get_geometry_collection(self) -> GeometryCollection:
        """
        Updates the discretization from :attr:`GeometryWrangler.places` using
        the nodes from :attr:`x` using :func:`reconstruct_discretization`.

        :returns: a new :class:`~pytential.collection.GeometryCollection`
            containing the updated discretization, while preserving all
            other geometries in the collection.
        """
        if self.discr is None:
            discr = reconstruct_discretization(
                self.array_context,
                self.wrangler.discr,
                self.x,
                keep_vertices=self.wrangler.keep_vertices,
            )

            object.__setattr__(self, "discr", discr)

        return self.wrangler.merge_collection(self.discr)

    # }}}


# }}}


# {{{ callbacks


def get_output_fields_geometry(state, **kwargs: Any) -> dict[str, OutputField]:
    places = state.get_geometry_collection()
    dofdesc = state.wrangler.dofdesc
    actx = state.array_context

    fields = {}
    fields["discr"] = OutputField.checkpoint("discr", state.discr)

    # {{{ desired

    # FIXME: have the wrangler figure this out on a case-by-case way

    if hasattr(state.wrangler, "context") and "xd" in state.wrangler.context:
        # NOTE: the `xd` could be time-dependent (and wrangler.context["xd"]
        # is just the initial condition), so we try to fish it out of the cost
        # functional, if there is one (unsteady optimization can set it to
        # None).

        from pystopt.cost import GeometryTrackingFunctional

        cost = getattr(state.wrangler, "cost", None)
        if isinstance(cost, GeometryTrackingFunctional):
            if "event" in kwargs:
                t = kwargs["event"].t
            elif hasattr(state, "t"):
                t = state.t
            else:
                t = None

            from pystopt import bind

            xd = bind(
                places,
                cost.nodes(),
                auto_where=cost.dofdesc,
            )(actx, t=t, **state.wrangler.context)

            fields["x0"] = OutputField.visualize(
                "x_d" if places.ambient_dim == 3 else r"\mathbf{X}_d", xd
            )
    else:
        discr_0 = state.wrangler.places.get_discretization(
            dofdesc.geometry, dofdesc.discr_stage
        )
        x0 = actx.thaw(discr_0.nodes())

        fields["x0"] = OutputField.visualize(
            "x_0" if places.ambient_dim == 3 else r"\mathbf{X}_0", x0
        )

    # }}}

    return fields


@dataclass(frozen=True)
class MeshQualityHistoryCallback(PerformanceHistoryCallback):
    """
    .. attribute:: volume

        Total volume of the closed surfaces in the geometry.

    .. attribute:: area

        Total surface area of the closed surfaces in the geometry.

    .. attribute:: h_max

        Maximum element area.

    .. attribute:: h_avg

        Mean element area.

    .. attribute:: h_std

        Element area standard deviation.
    """

    volume: list[float] = field(default_factory=list, repr=False)
    area: list[float] = field(default_factory=list, repr=False)

    h_max: list[float] = field(default_factory=list, repr=False)
    h_avg: list[float] = field(default_factory=list, repr=False)
    h_std: list[float] = field(default_factory=list, repr=False)

    def __call__(self, *args: Any, **kwargs: Any) -> int:
        r = super().__call__(*args, **kwargs)

        (state,) = args

        places = state.get_geometry_collection()
        dofdesc = state.wrangler.dofdesc
        ambient_dim = places.ambient_dim

        actx = state.array_context
        assert actx is not None, "'state' is frozen"

        from pystopt import bind

        volume = bind(places, sym.surface_volume(ambient_dim), auto_where=dofdesc)(actx)

        from arraycontext import flatten

        area = actx.to_numpy(
            flatten(bind(places, sym.element_area(ambient_dim))(actx), actx)
        )

        self.volume.append(actx.to_numpy(volume))
        self.area.append(np.sum(area))

        self.h_max.append(np.max(area))
        self.h_avg.append(np.mean(area))
        self.h_std.append(np.std(area, ddof=1))

        return r


# }}}
