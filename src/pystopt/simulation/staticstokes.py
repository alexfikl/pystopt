# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

"""
.. autofunction:: make_stokes_shape_optimization_wrangler

.. autoclass:: StokesShapeOptimizationWrangler
.. autoclass:: StokesShapeOptimizationState

.. autoclass:: StokesShapeOptimizationCallbackManager
.. autoclass:: StokesShapeOptimizationVisualizeCallback
.. autoclass:: StokesShapeOptimizationHistoryCallback
"""

from dataclasses import dataclass, field
from typing import Any

import numpy as np

from arraycontext import ArrayContext
from pytential import GeometryCollection
from pytools import memoize_method

import pystopt.callbacks as scb
import pystopt.simulation.common as scm
from pystopt import sym
from pystopt.cost import ShapeFunctional
from pystopt.filtering import FilterType
from pystopt.simulation.state import as_state_container
from pystopt.stokes import TwoPhaseStokesRepresentation
from pystopt.tools import get_default_logger

logger = get_default_logger(__name__)


# {{{ helpers


def make_stokes_shape_optimization_wrangler(
    places: GeometryCollection,
    cost: ShapeFunctional,
    op: TwoPhaseStokesRepresentation,
    *,
    viscosity_ratio: float = 1.0,
    capillary_number: float = np.inf,
    is_spectral: bool = True,
    dofdesc: sym.DOFDescriptorLike | None = None,
    context: dict[str, Any] | None = None,
    filter_type: FilterType = FilterType.Unfiltered,
    filter_arguments: dict[str, Any] | None = None,
    gmres_arguments: dict[str, Any] | None = None,
):
    """Construct a :class:`StokesShapeOptimizationWrangler`.

    The arguments are described in :class:`StokesShapeOptimizationWrangler`.
    """
    dofdesc = places.auto_source if dofdesc is None else sym.as_dofdesc(dofdesc)

    if context is None:
        context = {}
    context = {**context, "ca": capillary_number, "viscosity_ratio": viscosity_ratio}

    if filter_arguments is None:
        filter_arguments = {}

    if gmres_arguments is None:
        gmres_arguments = {}
    gmres_arguments = {"rtol": 1.0e-9, **gmres_arguments}

    return StokesShapeOptimizationWrangler(
        places=places,
        dofdesc=dofdesc,
        cost=cost,
        context=context,
        op=op,
        lambdas={"viscosity_ratio": viscosity_ratio},
        gmres_arguments=gmres_arguments,
        is_spectral=is_spectral,
        filter_type=filter_type,
        filter_arguments=filter_arguments,
    )


def make_sym_grad_static_stokes(
    wrangler: "StokesShapeOptimizationWrangler",
    context: dict[str, Any] | None = None,
) -> sym.var:
    if context is None:
        context = {}
    context = {**wrangler.context, **context}

    ambient_dim = wrangler.ambient_dim
    dofdesc = wrangler.dofdesc

    capillary_number = context["ca"]
    viscosity_ratio = context["viscosity_ratio"]

    from pystopt.operators import make_sym_density

    q = make_sym_density(wrangler.op, "q")
    qstar = make_sym_density(wrangler.ad, "qstar")

    import pystopt.derivatives as grad

    sym_grad = 0
    if wrangler.cost is not None:
        sym_grad += grad.shape(wrangler.cost, ambient_dim, dofdesc=dofdesc)

    if abs(capillary_number) != np.inf:
        sym_grad += grad.shape(wrangler.op.bc, wrangler.op, q, wrangler.ad, qstar)

    if abs(viscosity_ratio - 1) > 1.0e-14:
        sym_grad += grad.shape(
            wrangler.op,
            wrangler.op,
            q,
            wrangler.ad,
            qstar,
            viscosity_ratio_name="viscosity_ratio",
        )

    return sym_grad


# }}}


# {{{ state


@dataclass(frozen=True)
class StokesShapeOptimizationWrangler(
    scm.ShapeOptimizationWrangler, scm.StokesGeometryWrangler
):
    """
    .. attribute:: ad

        A :class:`~pystopt.stokes.TwoPhaseStokesRepresentation` for the
        adjoint equations.
    """

    def get_initial_state(self, actx: ArrayContext) -> "StokesShapeOptimizationState":
        from pystopt.simulation.state import get_geometry_state_as_cls

        return get_geometry_state_as_cls(actx, self, cls=StokesShapeOptimizationState)

    @property
    @memoize_method
    def ad(self):
        from pystopt.stokes import (
            TwoPhaseSingleLayerStokesRepresentation,
            make_static_adjoint_boundary_conditions,
        )

        assert isinstance(self.op, TwoPhaseSingleLayerStokesRepresentation)

        bc = make_static_adjoint_boundary_conditions(self.ambient_dim, self.cost)
        return TwoPhaseSingleLayerStokesRepresentation(bc)

    # {{{ shape optimization mixins

    @memoize_method
    def get_sym_shape_gradient(self):
        expr = make_sym_grad_static_stokes(self)

        from pystopt.symbolic.execution import prepare_expr

        return prepare_expr(self.places, expr, self.dofdesc)

    # }}}


@as_state_container
class StokesShapeOptimizationState(scm.ShapeOptimizationState, scm.StokesGeometryState):
    """
    .. automethod:: get_adjoint_density
    .. automethod:: get_adjoint_velocity
    """

    # NOTE: these fields are cached explicitly so that they can be checkpointed
    density_star: np.ndarray | None = field(default=None, init=False, repr=False)
    velocity_star: np.ndarray | None = field(default=None, init=False, repr=False)

    # {{{ adjoint

    @memoize_method
    def _get_adjoint_solution(self):
        result = self._get_uncached_stokes_solution(
            self.density_star,
            self.wrangler.ad,
            context={
                "q": self.get_stokes_density(),
                "u": self.get_stokes_velocity(),
            },
        )

        if self.density_star is None:
            object.__setattr__(self, "density_star", result.density)

        from dataclasses import replace

        return replace(result, density_name="qstar")

    def get_adjoint_density(self) -> np.ndarray:
        """The density in the boundary integral representation of the adjoint
        Stokes variables.

        The adjoint operator is obtained from
        :attr:`StokesShapeOptimizationWrangler.ad` and solved.

        :returns: the adjoint density in the boundary integral representation.
        """
        return self._get_adjoint_solution().density

    def get_adjoint_velocity(self) -> np.ndarray:
        """
        :returns: the adjoint velocity field for the current geometry.
        """
        if self.velocity_star is None:
            from pystopt.stokes import sti

            ustar = sti.velocity(
                self._get_adjoint_solution(),
                side=None,
                qbx_forced_limit=+1,
                dofdesc=self.wrangler.dofdesc,
            )

            object.__setattr__(self, "velocity_star", ustar)

        return self.velocity_star

    # }}}

    # {{{ optim

    @memoize_method
    def evaluate_shape_cost(self):
        return self.wrangler.evaluate_shape_cost(
            self.array_context,
            self.get_geometry_collection(),
            context={
                "q": self.get_stokes_density(),
                "u": self.get_stokes_velocity(),
            },
        )

    @memoize_method
    def evaluate_shape_gradient(self):
        return self.wrangler.evaluate_shape_gradient(
            self.array_context,
            self.get_geometry_collection(),
            context={
                "q": self.get_stokes_density(),
                "u": self.get_stokes_velocity(),
                "qstar": self.get_adjoint_density(),
                "ustar": self.get_adjoint_velocity(),
            },
        )

    # }}}


# }}}


# {{{ callbacks


def get_output_fields_stokes_optimization(state, **kwargs):
    fields = scm.get_output_fields_stokes(state, **kwargs)
    ambient_dim = state.wrangler.ambient_dim

    fields["qstar"] = scb.OutputField.checkpoint(
        r"\mathbf{q}^*" if ambient_dim == 2 else "qstar", state.get_adjoint_density()
    )
    fields["ustar"] = scb.OutputField(
        r"\mathbf{u}^*" if ambient_dim == 2 else "ustar", state.get_adjoint_density()
    )

    return fields


def matplotlib_visualize_stokes_optimization_writer(
    vis, basename, state, fields, **kwargs
):
    filename = basename.with_suffix("ustar")
    vis.write_file(filename, scb.extract_fields(fields, ("ustar",)))


@dataclass
class StokesShapeOptimizationCallbackManager(scm.ShapeOptimizationCallbackManager):
    def get_output_field_getters(self) -> tuple[scb.OutputFieldCallable, ...]:
        return (
            *super().get_output_field_getters(),
            get_output_fields_stokes_optimization,
        )


@dataclass(frozen=True)
class StokesShapeOptimizationVisualizeCallback(scb.VisualizeCallback):
    def get_file_writers(self) -> tuple[scb.OutputFieldVisualizeCallable, ...]:
        if self.ambient_dim == 2:
            return (
                scb.matplotlib_visualize_geometry_writer,
                scm.matplotlib_visualize_shape_optimization_writer,
                scm.matplotlib_visualize_stokes_writer,
                matplotlib_visualize_stokes_optimization_writer,
            )
        else:
            return super().get_file_writers()


@dataclass(frozen=True)
class StokesShapeOptimizationHistoryCallback(
    scm.StokesHistoryCallback,
    scb.OptimizationHistoryCallback,
    scb.PerformanceHistoryCallback,
):
    def __call__(self, *args, **kwargs):
        r0 = scm.StokesHistoryCallback.__call__(self, *args, **kwargs)
        r1 = scb.OptimizationHistoryCallback.__call__(self, *args, **kwargs)
        r2 = scb.PerformanceHistoryCallback.__call__(self, *args, **kwargs)

        return r0 & r1 & r2


# }}}
