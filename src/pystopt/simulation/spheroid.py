# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

"""
.. autoclass:: SpheroidShapeOptimizationWrangler
.. autoclass:: SpheroidShapeOptimizationState

.. autoclass:: SpheroidStokesOptimizationWrangler
.. autoclass:: SpheroidStokesOptimizationState

.. autoclass:: SpheroidOptimizationCallbackManager
.. autoclass:: SpheroidOptimizationHistoryCallback
"""

from dataclasses import dataclass, field
from typing import Any

import numpy as np

from arraycontext import ArrayContext
from meshmode.discretization import Discretization
from pytential import GeometryCollection
from pytools import memoize_method
from pytools.obj_array import make_obj_array

import pystopt.callbacks as scb
from pystopt import bind, sym
from pystopt.simulation.state import as_state_container
from pystopt.simulation.staticstokes import (
    StokesShapeOptimizationState,
    StokesShapeOptimizationWrangler,
)
from pystopt.simulation.unconstrained import (
    UnconstrainedShapeOptimizationState,
    UnconstrainedShapeOptimizationWrangler,
)
from pystopt.tools import get_default_logger

logger = get_default_logger(__name__)


# {{{ helpers


def make_spheroid_geometry(
    actx: ArrayContext,
    ambient_dim: int,
    *,
    resolution: int,
    mesh_kwargs: dict[str, Any],
) -> Discretization:
    from pystopt.mesh import generate_discretization, get_mesh_generator_from_name

    if ambient_dim == 2:
        gen = get_mesh_generator_from_name("fourier_ellipse", **mesh_kwargs)
    elif ambient_dim == 3:
        gen = get_mesh_generator_from_name("spharm_spheroid", **mesh_kwargs)
    else:
        raise ValueError(f"unsupported dimension: {ambient_dim}")

    return generate_discretization(gen, actx, resolution)


def make_ellipse_gradient(
    rho: sym.var, aspect_ratio: sym.var, alpha: sym.var
) -> np.ndarray:
    r"""Construct the gradient of an ellipse parametrization.

    The ellipse is parametrized by

    .. math::

        \mathbf{x}(\rho, a, \alpha) = R(\alpha)
        \begin{bmatrix}
        r \cos \theta \\
        r / a \sin \theta
        \end{bmatrix},

    where :math:`R(\alpha)` is the rotation matrix by the angle :math:`\alpha`

    .. math::

        R(\alpha) =
        \begin{bmatrix}
        \cos \alpha & -\sin \alpha \\
        \sin \alpha & \cos \alpha
        \end{bmatrix}.

    This function computes the derivatives with respect to :\math:`\rho, a`
    and :math:`\alpha` of the parametrization :math:`\mathbf{x}`.

    :returns: a symbolic expression for the gradient.
    """
    x = sym.nodes(2).as_vector()
    rot = np.array([
        [sym.cos(alpha), -sym.sin(alpha)],
        [sym.sin(alpha), sym.cos(alpha)],
    ])

    # NOTE: meshmode represents the ellipse as (cos(t), sin(t) / a)
    mat = np.array([[0, 0], [0, -1 / aspect_ratio]])

    dx_drho = x / rho
    dx_da = rot @ (mat @ (rot.T @ x))

    # NOTE: this one's a bit trickier, but we have that
    #
    #   dx/dalpha = dR/dalpha @ x0 = dR/dalpha @ R.T @ x
    #
    # since R.T @ R = I. Multiplying the matrices gives just [[0, -1], [1, 0]]
    dx_dalpha = make_obj_array([-x[1], x[0]])

    return make_obj_array([dx_drho, dx_da, dx_dalpha])


def make_spheroid_gradient(
    rho: sym.var, aspect_ratio: sym.var, euler_angles: np.ndarray
) -> np.ndarray:
    r"""Construct the gradient of a spheroid parametrization.

    The ellipse is parametrized by

    .. math::

        \mathbf{x}(\rho, a, \alpha, \beta, \gamma) = R(\alpha, \beta, \gamma)
        \begin{bmatrix}
        r \sin \theta \cos \phi \\
        r \sin \theta \sin \phi \\
        r a \cos \theta
        \end{bmatrix},

    where :math:`\theta \in [0, \pi]` and :math:`\phi \in [0, 2 \pi]` are
    the latitude and longitude, respectively. The rotation matrix :math:`R`
    is constructed in terms of the Euler angles as

    .. math::

        R = R_z(\alpha) R_y(\beta) R_x(\gamma)

    This function computes the derivatives with respect to :\math:`\rho, a`
    and :math:`\alpha` of the parametrization :math:`\mathbf{x}`.

    """
    # {{{ sympy

    # NOTE: follows the notation and order from
    # https://en.wikipedia.org/wiki/Rotation_formalisms_in_three_dimensions

    import sympy as sp

    alpha, beta, gamma = sp.symbols([a.name for a in euler_angles], real=True)
    rot = sp.rot_axis3(alpha) @ sp.rot_axis2(beta) @ sp.rot_axis1(gamma)

    drot_dalpha = sp.simplify(sp.diff(rot, alpha) @ rot.T)
    drot_dbeta = sp.simplify(sp.diff(rot, beta) @ rot.T)
    drot_dgamma = sp.simplify(sp.diff(rot, gamma) @ rot.T)

    # NOTE: represent the spheroid as (r * ..., r * ..., r * a)
    a = sp.Symbol(aspect_ratio.name, positive=True, real=True)
    mat = sp.Matrix([[0, 0, 0], [0, 0, 0], [0, 0, 1 / a]])
    da = sp.simplify(rot @ mat @ rot.T)

    # }}}

    # {{{ pymbolic

    from pystopt.symbolic.mappers import SympyToPytentialMapper

    pytentialize = SympyToPytentialMapper(3, dim=2)

    drot_dalpha = pytentialize(drot_dalpha)
    drot_dbeta = pytentialize(drot_dbeta)
    drot_dgamma = pytentialize(drot_dgamma)
    da = pytentialize(da)

    x = sym.nodes(3).as_vector()

    dx_drho = x / rho
    dx_da = da @ x

    # NOTE: to get the derivatives of the rotation matrix we do the following.
    # first, our coordinates are given by
    #       x = R @ x0
    #
    # for some unrotated x0, then, we have that
    #
    #       dx/dalpha = dR/dalpha @ x0 = dR/dalpha @ R^T @ x,
    #
    # since R^T @ R = I. There's probably some simplifycations here, but who
    # has the time :(

    dx_dalpha = drot_dalpha @ x
    dx_dbeta = drot_dbeta @ x
    dx_dgamma = drot_dgamma @ x

    # }}}

    return make_obj_array([dx_drho, dx_da, dx_dalpha, dx_dbeta, dx_dgamma])


# }}}


# {{{ wrangler


class SpheroidOptimizationWranglerMixin:
    # {{{ fields

    @property
    def fieldnames(self):
        if self.ambient_dim == 2:
            return ("sph_rho", "sph_a", "sph_alpha")
        else:
            return ("sph_rho", "sph_a", "sph_alpha", "sph_beta", "sph_gamma")

    def state_to_mesh_kwargs(self, state) -> dict[str, Any]:
        assert state.x.size == len(self.fieldnames)

        from pystopt.mesh.generation import make_transform_matrix_from_angles

        return {
            **self.mesh_kwargs,
            "radius": state.x[0],
            "aspect_ratio": state.x[1],
            "transform_matrix": make_transform_matrix_from_angles(*state.x[2:]),
        }

    def state_to_fieldnames(self, state) -> dict[str, Any]:
        assert state.x.size == len(self.fieldnames)
        return {k: state.x[i] for i, k in enumerate(self.fieldnames)}

    # }}}

    # {{{ gradient

    @memoize_method
    def get_sym_spheroid_gradient(self) -> np.ndarray:
        ambient_dim = self.discr.ambient_dim
        dim = self.discr.dim

        if ambient_dim == 2:
            rho, a, alpha = self.fieldnames
            dx = make_ellipse_gradient(
                sym.var(rho),
                sym.var(a),
                sym.var(alpha),
            )
        elif ambient_dim == 3:
            rho, a, alpha, beta, gamma = self.fieldnames
            dx = make_spheroid_gradient(
                sym.var(rho),
                sym.var(a),
                make_obj_array([sym.var(alpha), sym.var(beta), sym.var(gamma)]),
            )
        else:
            raise ValueError(f"unsupported dimension: {ambient_dim}")

        shape_gradient = sym.make_sym_vector("grad", ambient_dim)
        expr = make_obj_array([
            sym.sintegral(shape_gradient @ dxi, ambient_dim, dim, dofdesc=self.dofdesc)
            for dxi in dx
        ])

        from pystopt.symbolic.execution import prepare_expr

        return prepare_expr(self.places, expr, self.dofdesc)

    def evaluate_spheroid_gradient(
        self,
        actx: ArrayContext,
        places: GeometryCollection,
        *,
        context: dict[str, Any] | None,
    ) -> np.ndarray:
        if context is None:
            context = {}
        context = {**self.context, **context}

        grad = bind(
            places,
            self.get_sym_spheroid_gradient(),
            _skip_prepare=True,
        )(actx, **context)

        return make_obj_array([actx.to_numpy(gi) for gi in grad])

    def evaluate_shape_gradient(  # pylint: disable=useless-parent-delegation
        self,
        actx: ArrayContext,
        places: GeometryCollection,
        *,
        context: dict[str, Any] | None = None,
        force_spectral: bool | None = None,
    ) -> np.ndarray:
        return super().evaluate_shape_gradient(
            actx, places, context=context, force_spectral=False
        )

    # }}}


@dataclass(frozen=True)
class SpheroidShapeOptimizationWrangler(
    UnconstrainedShapeOptimizationWrangler, SpheroidOptimizationWranglerMixin
):
    resolution: int
    mesh_kwargs: dict[str, Any]

    def get_initial_state(  # pylint: disable=
        self,
        actx: ArrayContext,
        x: np.ndarray,
    ) -> "SpheroidShapeOptimizationState":
        return SpheroidShapeOptimizationState(x=x, wrangler=self, array_context_=actx)


@dataclass(frozen=True)
class SpheroidStokesOptimizationWrangler(
    StokesShapeOptimizationWrangler, SpheroidOptimizationWranglerMixin
):
    resolution: int
    mesh_kwargs: dict[str, Any]

    def get_initial_state(
        self,
        actx: ArrayContext,
        x: np.ndarray,
    ) -> "SpheroidStokesOptimizationState":
        return SpheroidStokesOptimizationState(x=x, wrangler=self, array_context_=actx)


# }}}


# {{{ state


class SpheroidOptimizationStateMixin:
    @memoize_method
    def get_geometry_collection(self):
        if self.discr is None:
            discr = make_spheroid_geometry(
                self.array_context,
                self.wrangler.ambient_dim,
                resolution=self.wrangler.resolution,
                mesh_kwargs=self.wrangler.state_to_mesh_kwargs(self),
            )

            object.__setattr__(self, "discr", discr)

        return self.wrangler.merge_collection(self.discr)

    @memoize_method
    def evaluate_gradient(self):
        return self.wrangler.evaluate_spheroid_gradient(
            self.array_context,
            self.get_geometry_collection(),
            context={
                **self.wrangler.state_to_fieldnames(self),
                "grad": self.wrangler.from_state_type(self.evaluate_shape_gradient()),
            },
        )


@as_state_container
class SpheroidShapeOptimizationState(
    SpheroidOptimizationStateMixin, UnconstrainedShapeOptimizationState
):
    array_context_: ArrayContext

    @property
    def array_context(self):
        return self.array_context_


@as_state_container
class SpheroidStokesOptimizationState(
    SpheroidOptimizationStateMixin, StokesShapeOptimizationState
):
    array_context_: ArrayContext

    @property
    def array_context(self):
        return self.array_context_


# }}}


# {{{ callbacks


def get_output_fields_spheroid(state, **kwargs):
    return {
        name: scb.OutputField.checkpoint(name, value)
        for name, value in state.wrangler.state_to_fieldnames(state).items()
    }


@dataclass
class SpheroidOptimizationCallbackManager(scb.CallbackManager):
    def get_output_field_getters(self) -> tuple[scb.OutputFieldCallable, ...]:
        from pystopt.simulation.common import get_output_fields_optimization
        from pystopt.simulation.state import get_output_fields_geometry

        return (
            get_output_fields_geometry,
            get_output_fields_optimization,
            get_output_fields_spheroid,
        )

    def __call__(self, *args: Any, **kwargs: Any) -> int:
        if "info" not in kwargs:
            raise KeyError(f"'info' not passed to '{type(self).__name__}'")

        def unwrap(ary):
            return getattr(ary, "x", ary)

        from dataclasses import replace

        (state,) = args
        info = kwargs.pop("info")
        info = replace(info, g=unwrap(info.g), d=unwrap(info.d))

        return super().__call__(state, info=info, **kwargs)


@dataclass(frozen=True)
class SpheroidOptimizationHistoryCallback(scb.OptimizationHistoryCallback):
    parameters: list[np.ndarray] = field(default_factory=list, repr=False)

    def __call__(self, *args, **kwargs):
        r = super().__call__(*args, **kwargs)

        (state,) = args
        fields = kwargs["fields"]

        self.parameters.append(
            np.array(
                [fields[name].value for name in state.wrangler.fieldnames],
                dtype=np.float64,
            )
        )

        return r


# }}}
