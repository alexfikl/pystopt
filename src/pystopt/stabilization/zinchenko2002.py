# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

"""
.. currentmodule:: pystopt.stabilization
.. autoclass:: Zinchenko2002Stabilizer
    :no-show-inheritance:
"""

from typing import Any

import numpy as np

from arraycontext import ArrayContext
from meshmode.discretization import Discretization
from meshmode.dof_array import DOFArray
from meshmode.mesh import MeshElementGroup
from pytential import GeometryCollection

import pystopt.stabilization.zinchenko_util as zu
from pystopt import sym
from pystopt.mesh import ToUniqueDOFConnection
from pystopt.tools import get_default_logger

logger = get_default_logger(__name__)


class Zinchenko2002Stabilizer(zu.ZinchenkoStabilizer):
    def optimize(
        self,
        actx: ArrayContext,
        places: GeometryCollection,
        conn: ToUniqueDOFConnection,
        w0: np.ndarray,
        dofdesc: sym.DOFDescriptor,
    ) -> Any:
        # NOTE: we always only have one group in the vertex connections because
        # they all map to one unique vertex array (with multiple batches)
        assert len(conn.groups) == 1

        discr = places.get_discretization(dofdesc.geometry, dofdesc.discr_stage)

        c_sqr = None
        if abs(self.beta) > 1.0e-14:
            c_sqr = zu.get_element_compactness_measure(discr)

        import scipy.optimize

        shape = w0.shape
        r = scipy.optimize.minimize(
            lambda w: _z2002_cost(self, discr, c_sqr, w.reshape(shape)),
            w0.reshape(-1),
            jac=lambda w: _z2002_grad(self, discr, c_sqr, w.reshape(shape)),
            method=self.optim_method,
            options=self.optim_options,
        )

        return r


# {{{ cost


def _z2002_cost_length(
    mgrp: MeshElementGroup, r: np.ndarray, d: np.ndarray
) -> np.ndarray:
    face_vertex_indices = zu.get_face_vertex_indices(mgrp, triangulate=False)

    c_length = 0
    for f, _ in enumerate(face_vertex_indices):
        c_length += d[f] ** 2 / r[f] ** 2

    return c_length


def _z2002_cost(
    stab: Zinchenko2002Stabilizer,
    discr: Discretization,
    c_sqr: DOFArray | None,
    w: np.ndarray,
) -> float:
    w = w.reshape(*discr.mesh.vertices.shape)

    c_sum = 0.0
    for igrp, mgrp in enumerate(discr.mesh.groups):
        r, d, _ = zu.get_edge_length_with_velocity(discr, mgrp, w)

        c_length = 0
        if abs(stab.alpha) > 1.0e-14:
            c_length = np.sum(_z2002_cost_length(mgrp, r, d))

        c_compact = 0
        if c_sqr is not None:
            from pystopt.stabilization.zinchenko2013 import _z2013_cost_compactness

            c_compact = np.sum(_z2013_cost_compactness(mgrp, r, d, c_sqr[igrp]))

        c_sum += stab.alpha / 2.0 * c_length + stab.beta / 2.0 * c_compact

    return c_sum


# }}}


# {{{ gradient


def _z2002_grad_length(
    mgrp: MeshElementGroup,
    alpha: float,
    grad: np.ndarray,
    r: np.ndarray,
    d: np.ndarray,
    v: np.ndarray,
) -> None:
    face_vertex_indices = zu.get_face_vertex_indices(mgrp, triangulate=False)
    vi = mgrp.vertex_indices

    for f, (i, j) in enumerate(face_vertex_indices):
        dc_length = d[f] / r[f] ** 2

        for k in range(grad.shape[0]):
            np.add.at(grad[k], vi[:, i], +alpha * dc_length * v[f][k])
            np.add.at(grad[k], vi[:, j], -alpha * dc_length * v[f][k])


def _z2002_grad(
    stab: Zinchenko2002Stabilizer,
    discr: Discretization,
    c_sqr: DOFArray | None,
    w: np.ndarray,
) -> np.ndarray:
    w = w.reshape(*discr.mesh.vertices.shape)

    grad = np.zeros_like(w)
    for igrp, mgrp in enumerate(discr.mesh.groups):
        r, d, v = zu.get_edge_length_with_velocity(discr, mgrp, w)

        if abs(stab.alpha) > 1.0e-14:
            _z2002_grad_length(mgrp, stab.alpha, grad, r, d, v)

        if c_sqr is not None:
            from pystopt.stabilization.zinchenko2013 import _z2013_grad_compactness

            _z2013_grad_compactness(mgrp, stab.beta, grad, r, d, v, c_sqr[igrp])

    return grad.reshape(-1)


# }}}
