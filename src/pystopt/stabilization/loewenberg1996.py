# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

"""
.. currentmodule:: pystopt.stabilization
.. autoclass:: Loewenberg1996Stabilizer
"""

import numpy as np

from arraycontext import ArrayContext
from meshmode.dof_array import DOFArray
from pytential import GeometryCollection

import pystopt.stabilization.zinchenko_util as zu
from pystopt import bind, sym
from pystopt.mesh import ToUniqueDOFConnection
from pystopt.stabilization.passive import PassiveStabilizer

# FIXME: This is completely broken! The original paper is already complete
# nonsense: arbitrary constants, scaling doesn't match up even slightly,
# no explanation of the intention behind some of the terms.


class Loewenberg1996Stabilizer(PassiveStabilizer):
    r"""Implements the tangential forcing from [Loewenberg1996]_.

    The tangential velocity field is given by

    .. math::

        \mathbf{w}_i \triangleq N^{\gamma} \frac{1}{1 + \lambda}
            \sum_{j} \left\{
                1
                + \alpha \frac{\|\mathbf{x}_i - \mathbf{x}_j\|}{h(\mathbf{x}_j)}
                + \beta \kappa_j^{\gamma}
            \right\} \Delta S_j (\mathbf{x}_i - \mathbf{x}_j),

    where :math:`N` is the number of vertices in the geometry, :math:`\lambda`
    is the viscosity ratio and :math:`\Delta S_j` is the dual area around the
    vertex :math:`\mathbf{x}_j`. In [Loewenberg1996]_, the authors take
    :math:`\gamma = 3/2, \alpha = 4` and :math:`\beta = 1`.

    .. [Loewenberg1996] M. Loewenberg, E. J. Hinch, *Numerical Simulation of a
        Concentrated Emulsion in Shear Flow*,
        Journal of Fluid Mechanics, Vol. 321, pp. 395, 1996,
        `DOI <https://doi.org/10.1017/s002211209600777x>`__.

    .. attribute:: alpha
    .. attribute:: beta
    .. attribute:: gamma

    .. automethod:: __init__
    .. automethod:: __call__
    """

    def __init__(
        self,
        *,
        vlambda: float = 1.0,
        alpha: float | None = None,
        beta: float | None = None,
        gamma: float | None = None,
    ) -> None:
        super().__init__()

        if alpha is None:
            alpha = 4.0

        if beta is None:
            beta = 1.0

        if gamma is None:
            gamma = 1.5

        self.vlambda = vlambda
        self.alpha = alpha
        self.beta = beta
        self.gamma = gamma

    def make_velocity_field(
        self,
        actx: ArrayContext,
        places: GeometryCollection,
        velocity: np.ndarray,
        *,
        dofdesc: sym.DOFDescriptor,
    ) -> np.ndarray:
        from pystopt.mesh.connection import get_unique_mesh_vertex_connections

        to_conn, from_conn = get_unique_mesh_vertex_connections(places, dofdesc)

        from pytools.obj_array import make_obj_array

        w = _l1996_velocity_field(self, actx, places, to_conn, dofdesc)

        return make_obj_array([
            from_conn(DOFArray(actx, (actx.from_numpy(subary.reshape(-1, 1)),)))
            for subary in w
        ])


# {{{ velocity field


def _l1996_velocity_field(
    stab: Loewenberg1996Stabilizer,
    actx: ArrayContext,
    places: GeometryCollection,
    conn: ToUniqueDOFConnection,
    dofdesc: sym.DOFDescriptor,
) -> np.ndarray:
    discr = places.get_discretization(dofdesc.geometry, dofdesc.discr_stage)

    # {{{ setup

    from arraycontext import flatten

    kappa = bind(places, sym.mean_curvature(places.ambient_dim), auto_where=dofdesc)(
        actx
    )
    kappa = np.abs(actx.to_numpy(flatten(conn(kappa), actx)))

    area = zu.get_vertex_dual_area(discr)

    # }}}

    # {{{ compute velocity field

    w = np.zeros((discr.ambient_dim, discr.mesh.nvertices), dtype=discr.real_dtype)

    for _, mgrp in enumerate(discr.mesh.groups):
        face_vertex_indices, _ = zu.get_face_vertex_indices(mgrp)
        vi = mgrp.vertex_indices

        # NOTE: in Loewenberg1996, h is set to be the smallest distance between
        # x_j and points on another surface; this is annoying to compute so
        # we leave it for later
        h = 1

        r, v = zu.get_edge_length_for_group(discr, mgrp)

        for f, (i, j) in enumerate(face_vertex_indices):
            factor_i = (
                0.5
                * area[vi[:, i]]
                * (
                    1.0
                    + stab.alpha * r[f] / h
                    + stab.beta * (kappa[vi[:, i]]) ** stab.gamma
                )
            )

            factor_j = (
                0.5
                * area[vi[:, j]]
                * (
                    1.0
                    + stab.alpha * r[f] / h
                    + stab.beta * (kappa[vi[:, j]]) ** stab.gamma
                )
            )

            for k in range(discr.ambient_dim):
                np.add.at(w[k], vi[:, i], -factor_i * v[f][k])
                np.add.at(w[k], vi[:, j], +factor_j * v[f][k])

    nelements = discr.mesh.nelements
    w = nelements**stab.gamma / (1 + stab.vlambda) * w

    # }}}

    return w


# }}}
