# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

"""
.. currentmodule:: pystopt.stabilization
.. autoclass:: Zinchenko2013Stabilizer
    :no-show-inheritance:
"""

from collections.abc import Iterable
from typing import Any

import numpy as np

from arraycontext import ArrayContext
from meshmode.discretization import Discretization
from meshmode.dof_array import DOFArray
from meshmode.mesh import (
    MeshElementGroup,
    SimplexElementGroup,
    TensorProductElementGroup,
)
from pytential import GeometryCollection

import pystopt.stabilization.zinchenko_util as zu
from pystopt import sym
from pystopt.mesh import ToUniqueDOFConnection
from pystopt.tools import get_default_logger

logger = get_default_logger(__name__)


class Zinchenko2013Stabilizer(zu.ZinchenkoStabilizer):
    def __init__(
        self,
        *,
        alpha: float | None = None,
        beta: float | None = None,
        curvature_power: float | None = None,
        curvature_eps: float | None = None,
        optim_options: dict[str, Any] | None = None,
        verbose: bool = False,
    ) -> None:
        """
        :arg alpha: weight for first term.
        :arg beta: weight for second term.
        :arg curvature_power: power coefficient used in curvature length scale.
        :arg curvature_eps: base value to use when the surface is flat, i.e. the
            curvature is zero.
        :arg optim_options: options passed to :func:`scipy.optimize.minimize`.
        """
        if curvature_power is None:
            curvature_power = 0.25

        if curvature_eps is None:
            curvature_eps = 1.0e-3

        super().__init__(
            alpha=alpha, beta=beta, optim_options=optim_options, verbose=verbose
        )

        self.curvature_power = curvature_power
        self.curvature_eps = curvature_eps

    def optimize(
        self,
        actx: ArrayContext,
        places: GeometryCollection,
        conn: ToUniqueDOFConnection,
        w0: np.ndarray,
        dofdesc: sym.DOFDescriptor,
    ) -> Any:
        # NOTE: we always only have one group in the vertex connections because
        # they all map to one unique vertex array (with multiple batches)
        assert len(conn.groups) == 1

        discr = places.get_discretization(dofdesc.geometry, dofdesc.discr_stage)

        h_sqr = None
        if abs(self.alpha) > 1.0e-14:
            h_sqr = zu.get_vertex_curvature_length(
                actx,
                places,
                conn,
                dofdesc=dofdesc,
                eps=self.curvature_eps,
                gamma=self.curvature_power,
            )

        c_sqr = None
        if abs(self.beta) > 1.0e-14:
            c_sqr = zu.get_element_compactness_measure(discr)

        import scipy.optimize

        shape = w0.shape
        r = scipy.optimize.minimize(
            lambda w: _z2013_cost(self, discr, h_sqr, c_sqr, w.reshape(shape)),
            w0.reshape(-1),
            jac=lambda w: _z2013_grad(self, discr, h_sqr, c_sqr, w.reshape(shape)),
            method=self.optim_method,
            options=self.optim_options,
        )

        return r


# {{{ cost


def _z2013_cost_simplex_compact_derivative(
    mgrp: MeshElementGroup, r: np.ndarray, c_delta: np.ndarray
) -> tuple[np.ndarray, Iterable[tuple[int, np.ndarray]]]:
    (nfaces,) = r.shape

    r2 = sum(r[f] for f in range(nfaces))
    r4 = sum(r[f] ** 2 for f in range(nfaces))

    factor = 2.0 / c_delta / r2**3
    return factor, ((f, r4 - r[f] * r2) for f in range(nfaces))


def _z2013_cost_quad_compact_derivative(
    grp: MeshElementGroup, r: np.ndarray, c_delta: np.ndarray
) -> tuple[np.ndarray, Iterable[tuple[int, np.ndarray]]]:
    _, (t0, t1) = zu.get_face_vertex_indices(grp)
    (i,) = set(t0) & set(t1)
    assert i == t0[-1]
    assert i == t1[-1]

    # factor and area of triangle 0
    r2 = sum(r[f] for f in t0)
    r4 = sum(r[f] ** 2 for f in t0)

    factor0 = r2
    area0 = np.sqrt(r2**2 - 2.0 * r4)

    # factor and area of triangle 1
    r2 = sum(r[f] for f in t1)
    r4 = sum(r[f] ** 2 for f in t1)

    factor1 = r2
    area1 = np.sqrt(r2**2 - 2.0 * r4)

    def dr():
        for f in t0[:-1]:
            yield f, (factor0 - 2 * r[f]) / area0 - 4.0 * c_delta

        for f in t1[:-1]:
            yield f, (factor1 - 2 * r[f]) / area1 - 4.0 * c_delta

        yield i, (factor0 - 2 * r[i]) / area0 + (factor1 - 2 * r[i]) / area1

    rr = 4.0 * sum(r[f] for f in range(r.size - 1))
    return rr, dr()


def _z2013_cost_curvature(
    grp: MeshElementGroup, r: np.ndarray, d: np.ndarray, h_sqr: np.ndarray
) -> np.ndarray:
    face_vertex_indices = zu.get_face_vertex_indices(grp, triangulate=False)
    vi = grp.vertex_indices

    # NOTE: interior edges are added twice. we would need to construct a
    # facial adjacency to deduplicate, which is just not worth it

    c_length = 0
    for f, (i, j) in enumerate(face_vertex_indices):
        h = 0.5 * (h_sqr[vi[:, i]] + h_sqr[vi[:, j]])
        c_length += ((1.0 / h - h / r[f] ** 2) * d[f]) ** 2

    return c_length


def _z2013_cost_compactness(
    grp: MeshElementGroup, r: np.ndarray, d: np.ndarray, c_delta: np.ndarray
) -> np.ndarray:
    if grp.dim == 1:
        dc_dt = d[0] / (2.0 * c_delta)
    elif isinstance(grp, SimplexElementGroup):
        factor, drs = _z2013_cost_simplex_compact_derivative(grp, r, c_delta)
        dc_dt = factor * sum(dr * d[f] for f, dr in drs)
    elif isinstance(grp, TensorProductElementGroup):
        factor, drs = _z2013_cost_quad_compact_derivative(grp, r, c_delta)
        dc_dt = sum(dr * d[f] for f, dr in drs) / factor
    else:
        raise TypeError("unsupported mesh element group type")

    return dc_dt**2 / c_delta**2


def _z2013_cost(
    stab: Zinchenko2013Stabilizer,
    discr: Discretization,
    h_sqr: np.ndarray | None,
    c_sqr: DOFArray | None,
    w: np.ndarray,
) -> float:
    w = w.reshape(*discr.mesh.vertices.shape)

    c_sum = 0.0
    for igrp, mgrp in enumerate(discr.mesh.groups):
        r, d, _ = zu.get_edge_length_with_velocity(discr, mgrp, w)

        c_length = 0
        if h_sqr is not None:
            c_length = np.sum(_z2013_cost_curvature(mgrp, r, d, h_sqr))

        c_compact = 0
        if c_sqr is not None:
            c_compact = np.sum(_z2013_cost_compactness(mgrp, r, d, c_sqr[igrp]))

        c_sum += stab.alpha / 2.0 * c_length + stab.beta / 2.0 * c_compact

    return c_sum


# }}}


# {{{ gradient


def _z2013_grad_curvature(
    mgrp: MeshElementGroup,
    alpha: float,
    grad: np.ndarray,
    r: np.ndarray,
    d: np.ndarray,
    v: np.ndarray,
    h_sqr: np.ndarray,
) -> None:
    face_vertex_indices = zu.get_face_vertex_indices(mgrp, triangulate=False)
    vi = mgrp.vertex_indices

    for f, (i, j) in enumerate(face_vertex_indices):
        h = 0.5 * (h_sqr[vi[:, i]] + h_sqr[vi[:, j]])
        c_length = (1.0 / h - h / r[f] ** 2) ** 2 * d[f]

        for k in range(grad.shape[0]):
            np.add.at(grad[k], vi[:, i], +alpha * c_length * v[f][k])
            np.add.at(grad[k], vi[:, j], -alpha * c_length * v[f][k])


def _z2013_grad_compactness(
    grp: MeshElementGroup,
    beta: float,
    grad: np.ndarray,
    r: np.ndarray,
    d: np.ndarray,
    v: np.ndarray,
    c_delta: np.ndarray,
) -> None:
    face_vertex_indices, _ = zu.get_face_vertex_indices(grp)
    vi = grp.vertex_indices

    if grp.dim == 1:
        dc = beta * d[0] / (4.0 * c_delta**4)
        fvi, drs = (0,), (1,)
    elif isinstance(grp, SimplexElementGroup):
        factor, drs = _z2013_cost_simplex_compact_derivative(grp, r, c_delta)
        fvi, drs = zip(*drs, strict=False)

        dc_dt = factor * sum(dr * d[f] for f, dr in zip(fvi, drs, strict=False))
        dc = beta * dc_dt / c_delta**2 * factor
    elif isinstance(grp, TensorProductElementGroup):
        factor, drs = _z2013_cost_quad_compact_derivative(grp, r, c_delta)
        fvi, drs = zip(*drs, strict=False)

        dc_dt = sum(dr * d[f] for f, dr in zip(fvi, drs, strict=False)) / factor
        dc = beta * dc_dt / c_delta**2 / factor
    else:
        raise TypeError("unsupported mesh element group type")

    for k in range(grad.shape[0]):
        for i_f, f in enumerate(fvi):
            i, j = face_vertex_indices[f]
            np.add.at(grad[k], vi[:, i], +dc * drs[i_f] * v[f][k])
            np.add.at(grad[k], vi[:, j], -dc * drs[i_f] * v[f][k])


def _z2013_grad(
    stab: Zinchenko2013Stabilizer,
    discr: Discretization,
    h_sqr: np.ndarray | None,
    c_sqr: DOFArray | None,
    w: np.ndarray,
) -> np.ndarray:
    w = w.reshape(*discr.mesh.vertices.shape)

    grad = np.zeros_like(w)
    for igrp, mgrp in enumerate(discr.mesh.groups):
        r, d, v = zu.get_edge_length_with_velocity(discr, mgrp, w)

        if h_sqr is not None:
            _z2013_grad_curvature(mgrp, stab.alpha, grad, r, d, v, h_sqr)

        if c_sqr is not None:
            _z2013_grad_compactness(mgrp, stab.beta, grad, r, d, v, c_sqr[igrp])

    return grad.reshape(-1)


# }}}
