# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

"""
.. currentmodule:: pystopt.stabilization
.. autoclass:: Kropinski2001Stabilizer
"""

import numpy as np

from arraycontext import ArrayContext
from pytential import GeometryCollection
from pytools import memoize_in

from pystopt import bind, sym
from pystopt.mesh import FourierDiscretization
from pystopt.stabilization.passive import PassiveStabilizer


class Kropinski2001Stabilizer(PassiveStabilizer):
    r"""Implements the tangential forcing from [Kropinski2001]_.

    The class implements a combination of the forcing :math:`\mathbf{T}_1` from
    Equation 20, which seeks to  maintain the arclength of each element, and
    the forcing :math:`\mathbf{T}_2` from Equation 21, which seeks to
    cluster points in regions of higher curvature. The final forcing is

    .. math::

        \mathbf{w} = \alpha \mathbf{T}_1 + \beta \mathbf{T}_2,

    where the coefficients are given by :attr:`alpha` and :attr:`beta`.

    .. [Kropinski2001] M. C. A. Kropinski, *An Efficient Numerical Method for
        Studying Interfacial Motion in Two-Dimensional Creeping Flows*,
        Journal of Computational Physics, Vol. 171, pp. 479--508, 2001,
        `DOI <https://doi.org/10.1006/jcph.2001.6787>`__.

    .. attribute:: alpha
    .. attribute:: beta

    .. automethod:: __init__
    .. automethod:: __call__
    """

    def __init__(
        self, *, alpha: float | None = None, beta: float | None = None
    ) -> None:
        super().__init__()

        if alpha is None:
            alpha = 1

        if alpha < 0:
            raise ValueError(f"'alpha' must be non-negative: {alpha}")

        if beta is None:
            beta = 0

        if alpha < 0:
            raise ValueError(f"'beta' must be non-negative: {beta}")

        self.alpha = alpha
        self.beta = beta

    def make_velocity_field(
        self,
        actx: ArrayContext,
        places: GeometryCollection,
        velocity: np.ndarray,
        *,
        dofdesc: sym.DOFDescriptor,
    ) -> np.ndarray:
        w = 0
        if self.alpha > 1.0e-14:
            w += self.alpha * _k2001_velocity_arclength(
                self, actx, places, velocity, dofdesc
            )

        if self.beta > 1.0e-14:
            w += self.beta * _k2001_velocity_curvature(
                self, actx, places, velocity, dofdesc
            )

        @memoize_in(self, (Kropinski2001Stabilizer.make_velocity_field, "tangent"))
        def sym_tangent():
            (expr,) = sym.tangential_onb(places.ambient_dim).T

            from pystopt.symbolic.execution import prepare_expr

            return prepare_expr(places, sym.var("x") * expr, dofdesc)

        return bind(places, sym_tangent(), auto_where=dofdesc, _skip_prepare=True)(
            actx, x=w
        )


# {{{ arclength


def _k2001_velocity_arclength(
    stab: Kropinski2001Stabilizer,
    actx: ArrayContext,
    places: GeometryCollection,
    velocity: np.ndarray,
    dofdesc: sym.DOFDescriptor,
) -> np.ndarray:
    discr = places.get_discretization(dofdesc.geometry, dofdesc.discr_stage)
    if not isinstance(discr, FourierDiscretization):
        raise TypeError(
            f"only available for 'FourierDiscretization', got '{type(discr).__name__}'"
        )

    # {{{ setup

    @memoize_in(stab, (_k2001_velocity_arclength, "curvature"))
    def sym_mean_curvature():
        expr = sym.mean_curvature(ambient_dim) * sym.area_element(ambient_dim)

        from pystopt.symbolic.execution import prepare_expr

        return prepare_expr(places, expr, dofdesc)

    @memoize_in(stab, (_k2001_velocity_arclength, "n_dot"))
    def sym_n_dot():
        expr = sym.n_dot(places.ambient_dim)

        from pystopt.symbolic.execution import prepare_expr

        return prepare_expr(places, expr, dofdesc)

    ambient_dim = places.ambient_dim
    kappa_g = bind(
        places, sym_mean_curvature(), auto_where=dofdesc, _skip_prepare=True
    )(actx)
    u_dot_n = bind(places, sym_n_dot(), auto_where=dofdesc, _skip_prepare=True)(
        actx, x=velocity
    )

    # }}}

    ary = discr.to_spectral_conn(kappa_g * u_dot_n)
    result = []
    for igrp, subary in enumerate(ary):
        # compute integral by fft
        k = discr._fftfreq(igrp)
        r = -1j * subary / k

        assert actx.to_numpy(k[0]) == 0
        r[0] = 0

        dt = discr.specgroups[igrp].jacobian
        result.append(r / dt)

    w = type(ary)(actx, tuple(result))
    return discr.from_spectral_conn(w)


# }}}


# {{{ curvature


def _k2001_velocity_curvature(
    stab: Kropinski2001Stabilizer,
    actx: ArrayContext,
    places: GeometryCollection,
    velocity: np.ndarray,
    dofdesc: sym.DOFDescriptor,
) -> np.ndarray:
    discr = places.get_discretization(dofdesc.geometry, dofdesc.discr_stage)
    if not isinstance(discr, FourierDiscretization):
        raise TypeError(
            f"only available for 'FourierDiscretization', got '{type(discr).__name__}'"
        )

    # {{{ setup

    ambient_dim = places.ambient_dim
    kappa = bind(places, sym.mean_curvature(ambient_dim), auto_where=dofdesc)(actx)
    g = bind(places, sym.area_element(ambient_dim), auto_where=dofdesc)(actx)

    un = bind(places, sym.n_dot(ambient_dim), auto_where=dofdesc)(actx, x=velocity)
    dun_dds = bind(
        places,
        sym.surface_laplace_beltrami(ambient_dim, sym.var("un")),
        auto_where=dofdesc,
    )(actx, un=un)

    # }}}

    ary = discr.to_spectral_conn(
        (kappa * dun_dds + kappa * (kappa**2 / 2 - 1) * un) * g
    )

    result = []
    for igrp, subary in enumerate(ary):
        # compute integral by fft
        k = discr._fftfreq(igrp)
        r = -1j * subary / k

        assert actx.to_numpy(k[0]) == 0
        r[0] = 0

        dt = discr.specgroups[igrp].jacobian
        result.append(r / dt)

    w = type(ary)(actx, tuple(result))
    return discr.from_spectral_conn(w) / (1 + kappa**2 / 2)


# }}}
