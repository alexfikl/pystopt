# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

"""
.. currentmodule:: pystopt.stabilization
.. autoclass:: Veerapaneni2011Stabilizer
"""

from collections.abc import Callable
from typing import Any

import numpy as np

from arraycontext import ArrayContext
from meshmode.discretization import Discretization
from pytential import GeometryCollection

from pystopt import sym
from pystopt.mesh import SpectralConnectionElementGroup
from pystopt.stabilization.passive import PassiveStabilizer

FilterCallable = Callable[[ArrayContext, SpectralConnectionElementGroup], Any]


class Veerapaneni2011Stabilizer(PassiveStabilizer):
    r"""Implements the tangential forcing from [Veerapaneni2011]_.

    The forcing is given as the gradient of the functional

    .. math::

        E[\mathbf{x}] = \frac{1}{2} \sum_{n, m}
            (1 - \alpha^m_n)^2 \langle \mathbf{x}, Y^m_n \rangle^2,

    where :math:`Y^m_n` are the spherical harmonics and
    :math:`\langle \cdot, \cdot \rangle` is the inner product on the unit
    sphere. Unlike [Veerapaneni2011]_, we use :math:`1 - \alpha^m_n`, where
    :math:`\alpha^m_n` are the coefficients of a corresponding filter operator.

    .. [Veerapaneni2011] S. K. Veerapaneni, A. Rahimian, G. Biros, D. Zorin,
        *A Fast Algorithm for Simulating Vesicle Flows in Three Dimensions*,
        Journal of Computational Physics, Vol. 230, pp. 5610--5634, 2011,
        `DOI <https://doi.org/10.1016/j.jcp.2011.03.045>`__.

    .. attribute:: alpha

    .. automethod:: __init__
    """

    def __init__(
        self,
        *,
        optimized: bool = False,
        nmax: int | None = None,
        alpha: FilterCallable | None = None,
    ) -> None:
        """
        :arg nmax: maximum spherical harmonic degree. If no attenuation *alpha*
            is given, then this is used in the default ideal filter, otherwise
            it is ignored.
        :arg alpha: a callable used to obtain the attenuation factor.
            By default, an ideal filter in the degree :math:`n` of the
            Spherical Harmonics is used.
        """
        if alpha is None:
            from functools import partial

            from pystopt.filtering.spharm import filter_spharm_ideal_n_factor

            alpha = partial(filter_spharm_ideal_n_factor, kmax=nmax)

        self.optimized = optimized
        self.nmax = nmax
        self.alpha = alpha

    def make_velocity_field(
        self,
        actx: ArrayContext,
        places: GeometryCollection,
        velocity: np.ndarray,
        *,
        dofdesc: sym.DOFDescriptor,
    ) -> np.ndarray:
        discr = places.get_discretization(dofdesc.geometry, dofdesc.discr_stage)

        from pystopt.mesh import SphericalHarmonicDiscretization

        if not isinstance(discr, SphericalHarmonicDiscretization):
            raise ValueError(
                f"'{type(self).__name__}' only supports 3d discretizations: "
                f"got a {discr.ambient_dim}d '{type(discr).__name__}'"
            )

        if self.optimized:
            w = _v2011_optimized_velocity_field(
                actx, places, self.alpha, dofdesc=dofdesc
            )
        else:
            w = _v2011_velocity_field(actx, places, self.alpha, dofdesc=dofdesc)

        return discr.from_spectral_conn(w)


# {{{ cost and gradient


def _v2011_cost_functional(
    actx: ArrayContext,
    places: GeometryCollection,
    alpha: FilterCallable,
    dofdesc: sym.DOFDescriptor,
) -> np.ndarray:
    discr = places.get_discretization(dofdesc.geometry, dofdesc.discr_stage)
    xlm = actx.thaw(discr.xlm)

    e = 0
    for igrp, sgrp in enumerate(discr.specgroups):
        # NOTE: (1 - alpha) because we want to penalize the high frequencies
        factor = (1.0 - alpha(actx, sgrp).reshape(-1, 1)) ** 2

        e = (
            e
            + actx.np.sum(factor * actx.np.abs(xlm[0][igrp]) ** 2)
            + actx.np.sum(factor * actx.np.abs(xlm[1][igrp]) ** 2)
            + actx.np.sum(factor * actx.np.abs(xlm[2][igrp]) ** 2)
        )

    return 0.5 * actx.to_numpy(e)


def _v2011_gradient(
    actx: ArrayContext,
    places: GeometryCollection,
    alpha: FilterCallable,
    dofdesc: sym.DOFDescriptor,
) -> np.ndarray:
    discr = places.get_discretization(dofdesc.geometry, dofdesc.discr_stage)
    xlm = actx.thaw(discr.xlm)

    result_x, result_y, result_z = [], [], []
    for igrp, sgrp in enumerate(discr.specgroups):
        # NOTE: (1 - alpha) because we want to penalize the high frequencies
        factor = (1.0 - alpha(actx, sgrp).reshape(-1, 1)) ** 2

        result_x.append(factor * xlm[0][igrp])
        result_y.append(factor * xlm[1][igrp])
        result_z.append(factor * xlm[2][igrp])

    from pytools.obj_array import make_obj_array

    g = make_obj_array([
        type(xlm[0])(actx, tuple(result_x)),
        type(xlm[0])(actx, tuple(result_y)),
        type(xlm[0])(actx, tuple(result_z)),
    ])

    from pystopt import bind

    g_dot_t = bind(
        discr, sym.project_tangent(sym.make_sym_vector("g", places.ambient_dim))
    )(actx, g=discr.from_spectral_conn(g))

    return discr.to_spectral_conn(g_dot_t)


# }}}


# {{{ velocity fields


def _v2011_velocity_field(
    actx: ArrayContext,
    discr: Discretization,
    alpha: FilterCallable,
    dofdesc: sym.DOFDescriptor,
) -> np.ndarray:
    return _v2011_gradient(actx, discr, alpha=alpha, dofdesc=dofdesc)


def _v2011_optimized_velocity_field(
    actx: ArrayContext,
    places: GeometryCollection,
    alpha: FilterCallable,
    dofdesc: sym.DOFDescriptor,
) -> np.ndarray:
    orig_discr = places.get_discretization(dofdesc.geometry, dofdesc.discr_stage)

    # {{{ setup optimization

    def _update_collection(xlm):
        from pystopt.mesh import update_discr_from_spectral

        return places.merge({
            dofdesc.geometry: update_discr_from_spectral(actx, orig_discr, xlm)
        })

    def _v2011_cost(xlm):
        return _v2011_cost_functional(
            actx, _update_collection(xlm), alpha=alpha, dofdesc=dofdesc
        )

    def _v2011_grad(xlm):
        return _v2011_gradient(
            actx, _update_collection(xlm), alpha=alpha, dofdesc=dofdesc
        )

    def _v2011_cost_grad(xlm):
        new_places = _update_collection(xlm)
        return (
            _v2011_cost_functional(actx, new_places, alpha=alpha, dofdesc=dofdesc),
            _v2011_gradient(actx, new_places, alpha=alpha, dofdesc=dofdesc),
        )

    # }}}

    # {{{ optimize

    from pystopt.optimize.linesearch import StabilizedBarzilaiBorweinLineSearch

    linesearch = StabilizedBarzilaiBorweinLineSearch(fun=_v2011_cost)

    from pystopt.optimize.descentdirection import SteepestDescentDirection

    descent = SteepestDescentDirection()

    from pystopt.optimize.riemannian import CartesianEucledeanSpace

    manifold = CartesianEucledeanSpace(
        vdot=lambda x, y: actx.to_numpy(actx.np.vdot(x, y))
    )

    from pystopt.optimize.steepest import minimize

    xlm0 = actx.thaw(orig_discr.xlm)
    result = minimize(
        fun=_v2011_cost,
        jac=_v2011_grad,
        x0=xlm0,
        funjac=_v2011_cost_grad,
        rtol=1.0e-4,
        options={
            "linesearch": linesearch,
            "descent": descent,
            "manifold": manifold,
        },
    )

    # }}}

    # xlm = xlm + dt * ((u dot n) n + w)
    # xlm = xlm + dt * (u dot n) n + dt * (xlmo - xlm)
    # xlm = (1 - dt) * xlm + dt * ((u dot n) * n + xlmo)

    return result.x - xlm0


# }}}
