# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

"""
.. currentmodule:: pystopt.stabilization

.. autoclass:: PassiveStabilizerCallable

.. autoclass:: PassiveStabilizer
.. autoclass:: CustomPassiveStabilizer
.. autofunction:: make_stabilizer_from_name
"""

from typing import Any, Protocol

import numpy as np

from arraycontext import ArrayContext
from pytential import GeometryCollection
from pytools import memoize_in

from pystopt import sym
from pystopt.tools import get_default_logger

logger = get_default_logger(__name__)


# {{{ passive stabilizer


class PassiveStabilizer:
    """Passive stabilization of a discretization / mesh is done by constructing
    a tangential velocity field that improves the mesh quality.

    .. attribute:: discr

    .. automethod:: make_velocity_field
    .. automethod:: __call__
    """

    def make_velocity_field(
        self,
        actx: ArrayContext,
        places: GeometryCollection,
        velocity: np.ndarray,
        *,
        dofdesc: sym.DOFDescriptor,
    ) -> np.ndarray:
        """This method is called by :meth:`__call__` to construct the velocity
        field. It is not meant to be called directly, but as a way for subclasses
        to provide the basic functionality.

        :returns: a velocity field on *dofdesc*.
        """
        return velocity

    def __call__(
        self,
        places: GeometryCollection,
        velocity: np.ndarray,
        *,
        force_normalize: bool = False,
        scale_to_velocity: bool = False,
        dofdesc: sym.DOFDescriptorLike | None = None,
    ) -> np.ndarray:
        r"""
        :arg velocity: a given initial velocity field. Its tangential components
            can be used as an initial guess for passive stabilization.

        :arg force_tangential: if *True*, projects the velocity field obtained
            from :meth:`~PassiveStabilizer.make_velocity_field` to the
            tangent space.
        :arg force_normalize: if *True*, normalizes the velocity field
            obtained from :meth:`~PassiveStabilizer.make_velocity_field`
            in the standard :math:`\ell^\infty` norm.
        :arg sale_to_velocity: if *True* scales the velocity field
            obtained from :meth:`~PassiveStabilizer.make_velocity_field` to
            have the same :math:`\ell^\infty` norm as *velocity*.

        :returns: a tangential velocity field on *dofdesc*.
        """
        if force_normalize and scale_to_velocity:
            raise ValueError(
                "'force_normalize' and 'scale_to_velocity' "
                "cannot both be True at the same time"
            )

        if dofdesc is None:
            dofdesc = places.auto_source
        else:
            dofdesc = sym.as_dofdesc(dofdesc)

        actx = velocity[0].array_context
        assert actx is not None

        w = self.make_velocity_field(actx, places, velocity, dofdesc=dofdesc)

        from pystopt.dof_array import dof_array_norm

        if force_normalize or scale_to_velocity:

            @memoize_in(self, "w_norm")
            def w_norm():
                return actx.to_numpy(
                    dof_array_norm(actx.np.sqrt(sum(w**2)), ord=np.inf)
                )

            w = w / w_norm()

        if scale_to_velocity:
            w = w * actx.to_numpy(dof_array_norm(velocity, ord=np.inf))

        return w


# }}}


# {{{ custom stabilizer


class PassiveStabilizerCallable(Protocol):
    """A callable used by :class:`CustomPassiveStabilizer` that follows the
    signature of  :meth:`~PassiveStabilizer.make_velocity_field`.

    .. automethod:: __call__
    """

    def __call__(
        self,
        actx: ArrayContext,
        places: GeometryCollection,
        velocity: np.ndarray,
        *,
        dofdesc: sym.DOFDescriptor,
    ) -> np.ndarray:
        pass


class CustomPassiveStabilizer(PassiveStabilizer):
    """
    .. automethod:: __init__
    """

    def __init__(self, func: PassiveStabilizerCallable):
        self.func = func

    def make_velocity_field(
        self,
        actx: ArrayContext,
        places: GeometryCollection,
        velocity: np.ndarray,
        *,
        dofdesc: sym.DOFDescriptor,
    ) -> np.ndarray:
        return self.func(actx, places, velocity, dofdesc=dofdesc)


# }}}


def make_stabilizer_from_name(name: str, **kwargs: Any) -> PassiveStabilizer:
    """Build passive or active stabilizers.

    :arg name: a string name for the stabilizer.
    :arg kwargs: a :class:`dict` of arguments to pass to the constructor.
    """

    name = name.lower()
    if name == "none":
        stab = None
    elif name == "default":
        stab = PassiveStabilizer(**kwargs)
    elif name == "custom":
        stab = CustomPassiveStabilizer(**kwargs)
    elif name == "loewenberg1996":
        from pystopt.stabilization.loewenberg1996 import Loewenberg1996Stabilizer

        stab = Loewenberg1996Stabilizer(**kwargs)
    elif name == "loewenberg2001":
        from pystopt.stabilization.loewenberg2001 import Loewenberg2001Stabilizer

        stab = Loewenberg2001Stabilizer(**kwargs)
    elif name == "kropinski2001":
        from pystopt.stabilization.kropinski2001 import Kropinski2001Stabilizer

        stab = Kropinski2001Stabilizer(**kwargs)
    elif name == "veerapaneni2011":
        from pystopt.stabilization.veerapaneni2011 import Veerapaneni2011Stabilizer

        stab = Veerapaneni2011Stabilizer(**kwargs)
    elif name == "zinchenko2002":
        from pystopt.stabilization.zinchenko2002 import Zinchenko2002Stabilizer

        stab = Zinchenko2002Stabilizer(**kwargs)
    elif name == "zinchenko2013":
        from pystopt.stabilization.zinchenko2013 import Zinchenko2013Stabilizer

        stab = Zinchenko2013Stabilizer(**kwargs)
    else:
        raise ValueError(f"unknown stabilization method: '{name}'")

    return stab
