# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

from typing import Any

import numpy as np
import numpy.linalg as la

from arraycontext import ArrayContext
from meshmode.discretization import Discretization
from meshmode.dof_array import DOFArray
from meshmode.mesh import (
    MeshElementGroup,
    SimplexElementGroup,
    TensorProductElementGroup,
)
from pytential import GeometryCollection
from pytools import memoize_in

from pystopt import bind, sym
from pystopt.mesh import ToUniqueDOFConnection
from pystopt.stabilization.passive import PassiveStabilizer
from pystopt.tools import get_default_logger

logger = get_default_logger(__name__)


# {{{ base stabilizer class


class ZinchenkoStabilizer(PassiveStabilizer):
    def __init__(
        self,
        *,
        alpha: float | None = None,
        beta: float | None = None,
        optim_options: dict[str, Any] | None = None,
        verbose: bool = False,
    ) -> None:
        if alpha is None:
            alpha = 1.0

        if beta is None:
            beta = 1.0

        if optim_options is None:
            optim_options = {}

        all_optim_options = {
            "method": "L-BFGS-B",
            "maxiter": 128,
        }
        all_optim_options.update(optim_options)

        if abs(alpha) < 1.0e-14 and abs(beta) < 1.0e-14:
            raise ValueError("either 'alpha' or 'beta' need to be non-zero")

        super().__init__()

        self.alpha = alpha
        self.beta = beta
        self.optim_method = all_optim_options.pop("method")
        self.optim_options = all_optim_options
        self.verbose = verbose

        self._last_w0 = None

    def optimize(
        self,
        actx: ArrayContext,
        places: GeometryCollection,
        conn: ToUniqueDOFConnection,
        w0: np.ndarray,
        dofdesc: sym.DOFDescriptor,
    ) -> Any:
        raise NotImplementedError

    def make_velocity_field(
        self,
        actx: ArrayContext,
        places: GeometryCollection,
        velocity: np.ndarray,
        *,
        dofdesc: sym.DOFDescriptor,
    ) -> np.ndarray:
        from pystopt.mesh.connection import get_unique_mesh_vertex_connections

        to_conn, from_conn = get_unique_mesh_vertex_connections(places, dofdesc)

        from arraycontext import flatten

        w0 = actx.to_numpy(flatten(to_conn(velocity), actx)).reshape(velocity.size, -1)
        if self._last_w0 is not None:
            w0 = 0.25 * w0 + 0.75 * self._last_w0

        r = self.optimize(actx, places, to_conn, w0, dofdesc)
        self._last_w0 = r.x.reshape(*w0.shape)

        if self.verbose:
            logger.info("%s:", type(self).__name__)
            logger.info("   Status: %s (%d)", r.message, r.status)
            logger.info("   Current Function Value: %.12e", r.fun)
            logger.info("   Iterations: %d", r.nit)
            logger.info("   Evaluations: fun %d jac %d", r.nfev, r.njev)

        from pytools.obj_array import make_obj_array

        return make_obj_array([
            from_conn(DOFArray(actx, (actx.from_numpy(ary.reshape(-1, 1)),)))
            for ary in self._last_w0
        ])


# }}}


# {{{ utils


def get_face_vertex_indices(
    mgrp: MeshElementGroup, *, triangulate: bool = True
) -> tuple[tuple[tuple[int, int], ...], tuple[tuple[int, ...], ...]]:
    if mgrp.dim == 1:
        fvi, simplices = ((0, 1),), ((0,),)
    elif mgrp.dim == 2:
        if isinstance(mgrp, SimplexElementGroup):
            fvi, simplices = mgrp.face_vertex_indices(), ((0, 1, 2),)
        elif isinstance(mgrp, TensorProductElementGroup):
            #   2     1     3
            #   o-----------o
            #   |         / |
            # 2 |       /   | 3
            #   |    4/     |
            #   |   /       |
            #   | /         |
            #   o-----------o
            #   0    0      1
            #
            # we add the 4th edge there so we can triangulate the quad

            fvi = mgrp.face_vertex_indices()
            simplices = ((0, 3, 4), (1, 2, 4))
            if triangulate:
                fvi = (*fvi, (0, 3))
        else:
            raise TypeError(f"unsupported element group type: '{type(mgrp).__name__}'")
    else:
        raise ValueError("unsupported dimension: {mgrp.dim}")

    if triangulate:
        return fvi, simplices

    return fvi


def get_group_ref_area(grp: MeshElementGroup) -> float:
    # NOTE: taking an element of edge length 1, unlike the convention from
    # modepy/meshmode which uses an element on [-1, 1]

    if grp.dim == 1:
        return 1.0
    elif isinstance(grp, SimplexElementGroup):
        # areas of equilateral simplices
        if grp.dim == 2:
            return np.sqrt(3.0) / 4.0
        elif grp.dim == 3:
            return np.sqrt(2.0) / 12.0
        else:
            raise ValueError(f"unsupported simplex dimension: {grp.dim}")
    elif isinstance(grp, TensorProductElementGroup):
        return 1.0
    else:
        raise TypeError(f"unrecognized mesh element group type: '{type(grp).__name__}'")


# }}}


# {{{ area


def get_edge_length_for_group(
    discr: Discretization, grp: MeshElementGroup
) -> np.ndarray:
    @memoize_in(discr, (get_edge_length_for_group, id(grp)))
    def _edge_length():
        vertices = discr.mesh.vertices
        vi = grp.vertex_indices

        face_vertex_indices, _ = get_face_vertex_indices(grp)
        nfaces = len(face_vertex_indices)
        r = np.empty(nfaces, dtype=object)
        v = np.empty(nfaces, dtype=object)

        for f, (i, j) in enumerate(face_vertex_indices):
            v[f] = vertices[:, vi[:, i]] - vertices[:, vi[:, j]]
            r[f] = la.norm(v[f], axis=0) ** 2

        return r, v

    return _edge_length()


def get_edge_length_with_velocity(
    discr: Discretization, grp: MeshElementGroup, w: np.ndarray
) -> tuple[np.ndarray, np.ndarray, np.ndarray]:
    face_vertex_indices, _ = get_face_vertex_indices(grp)
    vi = grp.vertex_indices

    r, v = get_edge_length_for_group(discr, grp)
    d = np.empty(r.size, dtype=object)

    for f, (i, j) in enumerate(face_vertex_indices):
        d[f] = np.sum(v[f] * (w[:, vi[:, i]] - w[:, vi[:, j]]), axis=0)

    return r, d, v


def get_simplex_group_element_area(grp: MeshElementGroup, r: np.ndarray) -> np.ndarray:
    (nfaces,) = r.shape

    # https://en.wikipedia.org/wiki/Heron%27s_formula
    r2 = sum(r[f] for f in range(nfaces))
    r4 = sum(r[f] ** 2 for f in range(nfaces))
    return 1.0 / 4.0 * np.sqrt(r2**2 - 2.0 * r4)


def get_quad_group_element_area(grp: MeshElementGroup, r: np.ndarray) -> np.ndarray:
    assert grp.dim == 2
    _, triangles = get_face_vertex_indices(grp)

    # NOTE: there's no general formula to get the area of a quadrilateral only
    # from its edge lengths, so we just add up the two triangles
    area = 0
    for faces in triangles:
        r2 = sum(r[f] for f in faces)
        r4 = sum(r[f] ** 2 for f in faces)
        area += 1.0 / 4.0 * np.sqrt(r2**2 - 2.0 * r4)

    return area


def get_element_area(discr: Discretization) -> DOFArray:
    @memoize_in(discr, (get_element_area, "element_area"))
    def _area():
        areas = []
        for grp in discr.mesh.groups:
            r, _ = get_edge_length_for_group(discr, grp)

            if grp.dim == 1:
                area = np.sqrt(r[0])
            elif isinstance(grp, SimplexElementGroup):
                area = get_simplex_group_element_area(grp, r)
            elif isinstance(grp, TensorProductElementGroup):
                area = get_quad_group_element_area(grp, r)
            else:
                raise TypeError("unsupported mesh element group type")

            areas.append(area)

        return DOFArray(None, tuple(areas))

    return _area()


def get_vertex_dual_area(
    discr: Discretization, area: DOFArray | None = None
) -> np.ndarray:
    if area is None:
        area = get_element_area(discr)

    return get_vertex_average(discr, area)


def get_vertex_average(discr: Discretization, ary: DOFArray) -> np.ndarray:
    result = np.zeros(discr.mesh.nvertices, dtype=discr.real_dtype)
    for mgrp, g_ary in zip(discr.mesh.groups, ary, strict=False):
        nvertices = mgrp.vertex_indices.shape[-1]
        for i in range(nvertices):
            np.add.at(result, mgrp.vertex_indices[:, i], g_ary / nvertices)

    return result


# }}}


# {{{ compactness


def get_element_compactness_measure(discr: Discretization) -> DOFArray:
    @memoize_in(discr, (get_element_compactness_measure, "compactness"))
    def _compactness():
        area = get_element_area(discr)
        if discr.dim == 1:
            return DOFArray(None, tuple(0.5 * subary for subary in area))

        assert discr.dim == 2

        c_deltas = []
        for mgrp, g_area in zip(discr.mesh.groups, area, strict=False):
            r, _ = get_edge_length_for_group(discr, mgrp)

            if isinstance(mgrp, SimplexElementGroup):
                pass
            elif isinstance(mgrp, TensorProductElementGroup):
                # removing the added edge for the triangulation in
                # `get_face_vertex_indices`
                r = r[:-1]
            else:
                raise TypeError

            c_delta = g_area / sum(r[f] for f in range(r.size))
            c_deltas.append(c_delta)

        return DOFArray(None, tuple(c_deltas))

    return _compactness()


# }}}


# {{{ curvature length scale


def get_vertex_curvature_length(
    actx: ArrayContext,
    places: GeometryCollection,
    conn: ToUniqueDOFConnection,
    dofdesc: sym.DOFDescriptor,
    eps: float | None = 1.0e-3,
    gamma: float | None = 0.25,
) -> np.ndarray:
    """
    :arg conn: a connection from nodes to unique vertices.
    :arg eps: minimum threshold for the length scale, to avoid divisions by 0.
    :arg gamma: parameter used in length scale computation (see paper).
    """
    discr = places.get_discretization(dofdesc.geometry, dofdesc.discr_stage)
    nelements = discr.mesh.nelements

    # {{{ curvature: compute :math:`\kappa_1^2 + \kappa_2^2 + eps`

    from arraycontext import flatten

    kappa = bind(
        places, sym.squared_curvature(places.ambient_dim) + eps, auto_where=dofdesc
    )(actx)
    kappa = actx.to_numpy(flatten(conn(kappa), actx))

    # }}}

    vertex_dual_area = get_vertex_dual_area(discr)

    ref_area = sum(get_group_ref_area(mgrp) for mgrp in discr.mesh.groups)
    ref_area = ref_area / (np.sum(vertex_dual_area) / nelements)

    k = np.sum(kappa**gamma * vertex_dual_area) / nelements / ref_area
    return k * kappa ** (-gamma)


# }}}
