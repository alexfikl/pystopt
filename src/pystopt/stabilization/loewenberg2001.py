# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

"""
.. currentmodule:: pystopt.stabilization
.. autoclass:: Loewenberg2001Stabilizer
"""

import numpy as np

from arraycontext import ArrayContext
from meshmode.dof_array import DOFArray
from pytential import GeometryCollection

import pystopt.stabilization.zinchenko_util as zu
from pystopt import bind, sym
from pystopt.mesh import ToUniqueDOFConnection
from pystopt.stabilization.passive import PassiveStabilizer


class Loewenberg2001Stabilizer(PassiveStabilizer):
    """Implements the tangential forcing from [loewenberg2001]_.

    .. [loewenberg2001]  V. Cristini, J. Bławzdziewicz, M. Loewenberg,
        *An Adaptive Mesh Algorithm for Evolving Surfaces: Simulations of Drop
        Breakup and Coalescence*,
        Journal of Computational Physics, Vol. 168, pp. 445--463, 2001,
        `DOI <https://doi.org/10.1006/jcph.2001.6713>`__.

    .. attribute:: alpha

    .. automethod:: __init__
    """

    def __init__(self, *, alpha: float | None = None) -> None:
        super().__init__()

        if alpha is None:
            alpha = 0.1

        self.alpha = alpha

    def make_velocity_field(
        self,
        actx: ArrayContext,
        places: GeometryCollection,
        velocity: np.ndarray,
        *,
        dofdesc: sym.DOFDescriptor,
    ) -> np.ndarray:
        from pystopt.mesh.connection import get_unique_mesh_vertex_connections

        to_conn, from_conn = get_unique_mesh_vertex_connections(places, dofdesc)

        from pytools.obj_array import make_obj_array

        w = _l2001_velocity_field(self, actx, places, to_conn, dofdesc)

        return make_obj_array([
            from_conn(DOFArray(actx, (actx.from_numpy(subary.reshape(-1, 1)),)))
            for subary in w
        ])


# {{{ velocity field


def _l2001_velocity_field(
    stab: Loewenberg2001Stabilizer,
    actx: ArrayContext,
    places: GeometryCollection,
    conn: ToUniqueDOFConnection,
    dofdesc: sym.DOFDescriptor,
) -> np.ndarray:
    discr = places.get_discretization(dofdesc.geometry, dofdesc.discr_stage)

    # {{{ compute vertex length scale

    from arraycontext import flatten

    area_length_scale = actx.to_numpy(
        bind(
            places,
            sym.surface_area(places.ambient_dim) / discr.mesh.nelements,
            auto_where=dofdesc,
        )(actx)
        ** (1.0 / discr.dim)
    )

    # L0 = element-based length scale       [Length] units
    L0 = bind(
        places,
        sym.element_area(
            places.ambient_dim, dofdesc=dofdesc, granularity=sym.GRANULARITY_NODE
        ),
        auto_where=dofdesc,
    )(actx)
    L0 = L0 ** (1.0 / discr.dim)

    # L1 = curvature based length scale     1 / [Length]^2 units
    L1 = bind(
        places,
        0.5 * sym.squared_curvature(places.ambient_dim, dofdesc=dofdesc),
        auto_where=dofdesc,
    )(actx)
    L1 = 1.0 / actx.np.sqrt(L1)

    # L2 = length to next surface, if any
    # L2 = np.inf

    # overall length scale
    L = stab.alpha * actx.np.minimum(L0, L1)

    # FIXME: in [Loewenberg2001] this seems to do some fancy averaging?
    # L = zu.get_vertex_average(discr, L)
    L = actx.to_numpy(flatten(conn(L), actx))

    # }}}

    # {{{ compute velocity field

    # As described in [Loewenberg2001] Section 2.2.1 we want to compute the
    # the velocity field
    #
    #   w_i = \sum_j e_{ij} \gamma_{ij}
    #
    # where `e_{ij}` is the unit vector parallel to edge `j` that touches the
    # vertex `i`. The weight `gamma_{ij}` is given by
    #
    #   \gamma = l_{opt} - \alpha <L>
    #
    # where `l` is a desired optimal edge length and `<L>` denotes a local average
    # (which we will ignore for now). Finally, we take `l_opt` to be the average
    # edge length in the group for now.

    w = np.zeros((discr.ambient_dim, discr.mesh.nvertices), dtype=discr.real_dtype)

    for _, mgrp in enumerate(discr.mesh.groups):
        face_vertex_indices, _ = zu.get_face_vertex_indices(mgrp)
        vi = mgrp.vertex_indices
        r, v = zu.get_edge_length_for_group(discr, mgrp)

        # FIXME: in [Loewenberg2001] it seems like they're doing something else
        # with this l_opt, but needs more reading / investigating to figure out
        # l_opt = np.mean([np.median(np.sqrt(rf)) for rf in r])
        l_opt = area_length_scale

        for f, (i, j) in enumerate(face_vertex_indices):
            gamma_ij = l_opt - L[vi[:, i]]
            gamma_ji = l_opt - L[vi[:, j]]
            gamma_ij = gamma_ji = l_opt - r[f]

            norm_v = np.sqrt(sum(v[f][k] ** 2 for k in range(discr.ambient_dim)))
            for k in range(discr.ambient_dim):
                unit_v = v[f][k] / norm_v
                np.add.at(w[k], vi[:, i], +gamma_ij * unit_v)
                np.add.at(w[k], vi[:, j], -gamma_ji * unit_v)

    # }}}

    return w


# }}}
