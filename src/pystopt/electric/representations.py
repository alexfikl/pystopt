# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

"""
The :class:`TwoPhaseSingleLayerElectricRepresentation` class implements
:class:`pystopt.stokes.StokesInterface` functions with the arguments

.. code:: python

    @electric_field.register(TwoPhaseSingleLayerElectricRepresentation)
    def _electric_field(self, density, *, side=None, qbx_forced_limit=None):
        pass

and similar for the other variables.

.. autoclass:: TwoPhaseSingleLayerElectricRepresentation
"""

from pytools.obj_array import make_obj_array

import pystopt.electric.interface as eli
from pystopt import sym
from pystopt.operators import (
    RepresentationInterface,
    make_sym_density,
    make_sym_operator,
    prepare_sym_rhs,
)

# {{{ operator


class TwoPhaseSingleLayerElectricRepresentation(
    RepresentationInterface, eli.ElectricInterface
):
    """Representation of an electrostatic problem based on a single-layer
    potential.
    """

    def __init__(self, ambient_dim, kernel_arguments=None):
        super().__init__(ambient_dim, kernel_arguments)

        from sumpy.kernel import LaplaceKernel

        self.kernel = LaplaceKernel(self.ambient_dim)


@make_sym_density.register(TwoPhaseSingleLayerElectricRepresentation)
def _make_sym_density_electric(self, name="q"):
    return sym.var(name)


@make_sym_operator.register(TwoPhaseSingleLayerElectricRepresentation)
def _make_sym_operator_electric(self, density, *, ratio_name="R"):
    ratio = sym.var(ratio_name)
    lpot = eli.normal_electric_field(self, density, qbx_forced_limit="avg")

    return -(1 + ratio) / 2 * density + (1 - ratio) * lpot


@prepare_sym_rhs.register(TwoPhaseSingleLayerElectricRepresentation)
def _prepare_sym_rhs_electric(self, b, *, ratio_name="R"):
    # FIXME: right-hand side also needs some BCs in it and stuff
    return b


# }}}


# {{{ variables


@eli.electric_potential.register(TwoPhaseSingleLayerElectricRepresentation)
def _electric_potential_single_layer(
    self: TwoPhaseSingleLayerElectricRepresentation,
    density,
    *,
    side=None,
    qbx_forced_limit=None,
):
    import pytential.symbolic.primitives as prim

    return prim.S(self.kernel, density, qbx_forced_limit=qbx_forced_limit)


@eli.electric_field.register(TwoPhaseSingleLayerElectricRepresentation)
def _electric_field_single_layer(
    self: TwoPhaseSingleLayerElectricRepresentation,
    density,
    *,
    side=None,
    qbx_forced_limit=None,
):
    from pystopt.symbolic.mappers import DerivativeTaker

    # FIXME: missing jump
    phi = self.electric_potential(density, qbx_forced_limit=qbx_forced_limit)
    return make_obj_array([
        -DerivativeTaker(i).map_int_g(phi) for i in range(self.ambient_dim)
    ])


@eli.normal_electric_field.register(TwoPhaseSingleLayerElectricRepresentation)
def _normal_electric_field_single_layer(
    self: TwoPhaseSingleLayerElectricRepresentation,
    density,
    *,
    side=None,
    qbx_forced_limit=None,
):
    if qbx_forced_limit not in {None, +1, -1, "avg"}:
        raise ValueError("cannot evaluate normal electric field off-surface")

    # FIXME: missing jump
    import pytential.symbolic.primitives as prim

    return -prim.Sp(self.kernel, density, qbx_forced_limit=qbx_forced_limit)


# }}}
