# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

r"""
.. autoclass:: ElectricInterface
    :no-show-inheritance:

.. function:: electric_potential(self: ElectricInterface, ...)
.. function:: electric_field(self: ElectricInterface, ...)

    The electric field is simply :math:`\mathbf{E} = -\nabla \phi`, where
    :math:`\phi` is the electric potential.

.. function:: normal_electric_field(self: ElectricInterface, ...)
"""

from functools import singledispatch


class ElectricInterface:
    def __init__(self, ambient_dim):
        self.ambient_dim = ambient_dim


@singledispatch
def electric_potential(self: ElectricInterface, *args, **kwargs):
    raise NotImplementedError(type(self).__name__)


@singledispatch
def electric_field(self: ElectricInterface, *args, **kwargs):
    raise NotImplementedError(type(self).__name__)


@singledispatch
def normal_electric_field(self: ElectricInterface, *args, **kwargs):
    raise NotImplementedError(type(self).__name__)
