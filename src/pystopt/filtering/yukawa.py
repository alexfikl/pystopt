# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

"""
.. currentmodule:: pystopt.filtering

.. autofunction:: apply_yukawa_smoothing
.. autofunction:: apply_yukawa_filter
"""

from typing import Any

import numpy as np

from arraycontext import ArrayContext, ArrayOrContainerT
from pytential import GeometryCollection

from pystopt import bind, sym
from pystopt.tools import get_default_logger

logger = get_default_logger(__name__)


# {{{ yukawa smoothing


def apply_yukawa_smoothing(
    actx: ArrayContext,
    places: GeometryCollection,
    x: ArrayOrContainerT,
    *,
    method: str = "beltrami",
    strength: float | None = None,
    gmres_arguments: dict[str, Any] | None = None,
    dofdesc: sym.DOFDescriptorLike | None = None,
) -> ArrayOrContainerT:
    r"""Filter a given quantity on a surface.

    The filtering is done by solving the Yukawa-Beltrami equation

    .. math::

        -\Delta_\Sigma y + \frac{1}{k^2} y = x.

    where the input *strength* is :math:`k^2` on a surface :math:`\Sigma`. A
    smaller *strength* means less smoothing.

    :arg x: array container with :class:`~meshmode.dof_array.DOFArray` leaves.
    :arg strength: strength of the smoothing in :math:`(0, \infty]`. If it
        is :math:`\infty`, then the equation reduces to a Laplace-Beltrami
        equation.
    """
    # {{{ validate

    if dofdesc is None:
        dofdesc = places.auto_source

    if strength is None:
        strength = 5.0e-2

    if strength < 0:
        raise ValueError(f"'strength' must be non-negative, got {strength}")

    if gmres_arguments is None:
        gmres_arguments = {}

    all_gmres_arguments = {
        "rtol": 1.0e-10,
        "callback": True,
        "stall_iterations": 0,
        "hard_failure": True,
    }
    all_gmres_arguments.update(gmres_arguments)

    # }}}

    # {{{ symbolic

    if strength == np.inf:
        context = {}
    else:
        context = {"k": 1.0 / strength}

    import pystopt.operators as operator

    if method == "beltrami":
        if strength == np.inf:
            op = operator.LaplaceBeltramiRepresentation(
                places.ambient_dim, precond="left"
            )
        else:
            op = operator.YukawaBeltramiRepresentation(
                places.ambient_dim, precond="left", yukawa_lambda_name="k"
            )
    elif method == "neumann":
        if strength == np.inf:
            op = operator.LaplaceNeumannRepresentation(
                places.ambient_dim, use_l2_weighting=True
            )
        else:
            op = operator.YukawaNeumannRepresentation(
                places.ambient_dim, yukawa_lambda_name="k", use_l2_weighting=False
            )
    else:
        raise ValueError(f"unknown method: '{method}'")

    sym_density = operator.make_sym_density(op, "sigma")
    sym_b = operator.make_sym_density(op, "b")
    sym_op = operator.make_sym_operator(op, sym_density) / sym.cse(
        operator.make_sym_operator(op, sym.Ones())
    )

    sym_result = operator.prepare_sym_solution(op, sym_density) / sym.cse(
        operator.prepare_sym_solution(op, sym.Ones())
    )
    sym_b = operator.prepare_sym_rhs(op, sym_b) / sym.cse(
        operator.prepare_sym_rhs(op, sym.Ones())
    )

    # }}}

    # {{{ solve

    # bound all operators so they can be applied repeatedly
    bound_b = bind(places, sym_b, auto_where=dofdesc)
    bound_result = bind(places, sym_result, auto_where=dofdesc)
    scipy_op = bind(places, sym_op, auto_where=dofdesc).scipy_op(
        actx, "sigma", op.dtype, **context
    )

    def _rec_apply_beltrami_filter(ary):
        from meshmode.dof_array import DOFArray

        if not isinstance(ary, DOFArray):
            raise TypeError(f"cannot filter arrays of type '{type(ary).__name__}'")

        from pystopt.optimize.gmres import gmres

        b = bound_b(actx, b=ary, **context)
        result = gmres(scipy_op, b, **all_gmres_arguments)
        result = bound_result(actx, sigma=result.solution, **context)

        if ary.entry_dtype.kind != "c" and result.entry_dtype.kind == "c":
            result = actx.np.real(result)

        return result

    from meshmode.dof_array import rec_map_dof_array_container

    x = rec_map_dof_array_container(_rec_apply_beltrami_filter, x)

    # }}}

    return x


# }}}


# {{{ yukawa filtering


def apply_yukawa_filter(
    actx: ArrayContext,
    places: GeometryCollection,
    x: ArrayOrContainerT,
    *,
    strength: float | None = None,
    gmres_arguments: dict[str, Any] | None = None,
    dofdesc: sym.DOFDescriptorLike | None = None,
) -> ArrayOrContainerT:
    r"""Filter a given quantity on a surface.

    :arg x: array container with :class:`~meshmode.dof_array.DOFArray` leaves.
    :arg strength: strength of the smoothing in :math:`(0, \infty]`. If it
        is :math:`\infty`, then the equation reduces to a Laplace-Beltrami
        equation.
    """
    # {{{ validate

    if dofdesc is None:
        dofdesc = places.auto_source

    if strength is None:
        strength = 1.0e-2

    if strength < 0:
        raise ValueError(f"'strength' must be non-negative, got {strength}")

    if gmres_arguments is None:
        gmres_arguments = {}

    all_gmres_arguments = {
        "rtol": 1.0e-6,
        "callback": True,
        "stall_iterations": 0,
        "hard_failure": True,
    }
    all_gmres_arguments.update(gmres_arguments)

    # }}}

    # {{{ symbolic

    from sumpy.kernel import YukawaKernel

    kernel = YukawaKernel(places.ambient_dim, yukawa_lambda_name="k")
    context = {"k": 1.0 / strength}

    from functools import partial

    K = partial(
        sym.D,
        kernel,
        qbx_forced_limit="avg",
        kernel_arguments={"k": sym.var("k")},
    )

    sym_density = sym.var("sigma")
    sym_op = K(sym_density) / sym.cse(K(sym.Ones()))

    # }}}

    # {{{ filter

    # bound all operators so they can be applied repeatedly
    scipy_op = bind(places, sym_op, auto_where=dofdesc).scipy_op(
        actx, "sigma", np.complex128, **context
    )

    def _rec_apply_beltrami_filter(ary):
        from meshmode.dof_array import DOFArray

        if isinstance(ary, DOFArray):
            from pystopt.optimize.gmres import gmres

            result = gmres(scipy_op, ary, **all_gmres_arguments).solution
            if isinstance(result, DOFArray):
                dtype = result.entry_dtype
            else:
                raise TypeError(f"Unsupported result type: '{type(result).__name__}'")

            if ary.entry_dtype.kind != "c" and dtype.kind == "c":
                result = actx.np.real(result)
        else:
            raise TypeError(f"cannot filter arrays of type '{type(ary).__name__}'")

        return result

    from meshmode.dof_array import rec_map_dof_array_container

    x = rec_map_dof_array_container(_rec_apply_beltrami_filter, x)

    # }}}

    return x


# }}}
