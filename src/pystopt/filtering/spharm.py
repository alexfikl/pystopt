# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

"""
.. currentmodule:: pystopt.filtering

.. autofunction:: apply_filter_spharm_ideal
.. autofunction:: apply_filter_spharm_tikhonov
.. autofunction:: apply_filter_spharm_tikhonov_opt
.. autofunction:: apply_filter_spharm_tikhonov_ideal
"""

from collections.abc import Sequence
from typing import Any

import numpy as np

from arraycontext import ArrayContext
from pytools import memoize_in

from pystopt.dof_array import SpectralDOFArray
from pystopt.mesh import SpectralConnectionElementGroup, SpectralDiscretization
from pystopt.tools import get_default_logger

logger = get_default_logger(__name__)


# {{{ caches


def _ideal_nm_indices(
    actx: ArrayContext, sgrp: SpectralConnectionElementGroup
) -> tuple[Any, Any]:
    @memoize_in(sgrp.sh, (_ideal_nm_indices, "nm"))
    def _nm():
        sh_l = actx.from_numpy(sgrp.sh.l.astype(np.float64))
        sh_m = actx.from_numpy(sgrp.sh.m.astype(np.float64))
        return actx.freeze(sh_l), actx.freeze(sh_m)

    n, m = _nm()
    return actx.thaw(n), actx.thaw(m)


def _tikhonov_sqr_laplace_eigenvalues(
    actx: ArrayContext, sgrp: SpectralConnectionElementGroup
) -> Any:
    @memoize_in(sgrp.sh, (_tikhonov_sqr_laplace_eigenvalues, "eigs"))
    def _eigs():
        sh_l = actx.from_numpy(sgrp.sh.l.astype(np.float64)).reshape(-1, 1)
        sh_l = (sh_l * (sh_l + 1)) ** 2

        return actx.freeze(sh_l)

    return actx.thaw(_eigs())


# }}}


# {{{ ideal


def filter_spharm_ideal_n_factor(
    actx: ArrayContext,
    sgrp: SpectralConnectionElementGroup,
    kmax: int | None = None,
) -> np.ndarray:
    if kmax is None:
        kmax = sgrp.sh.lmax // 2

    sh_l, _ = _ideal_nm_indices(actx, sgrp)
    return sh_l < kmax


def filter_spharm_ideal_m_factor(
    actx: ArrayContext,
    sgrp: SpectralConnectionElementGroup,
    kmax: int | None = None,
) -> np.ndarray:
    if kmax is None:
        kmax = sgrp.sh.mmax // 2

    _, sh_m = _ideal_nm_indices(actx, sgrp)
    return sh_m < kmax


def filter_spharm_ideal_factor(
    actx: ArrayContext,
    sgrp: SpectralConnectionElementGroup,
    kmax: int | tuple[int, int] | None = None,
    p: float | str | None = None,
) -> np.ndarray:
    if p is None:
        p = 2

    if kmax is None:
        kmax = sgrp.nelphi // 2

    sh_l, sh_m = _ideal_nm_indices(actx, sgrp)

    from numbers import Number

    if isinstance(kmax, int):
        if p in {np.inf, "inf"}:
            mask = actx.np.maximum(sh_l, sh_m) < kmax
        elif isinstance(p, Number) and p > 0:
            mask = (sh_l**p + sh_m**p) < kmax**p
        else:
            raise ValueError(f"unsupported norm type: '{p}'")
    elif isinstance(kmax, tuple):
        mask = (sh_m < kmax[0]) & (sh_l < kmax[1])
    else:
        raise TypeError(f"'kmax' has unexpected type: {type(kmax).__name__}")

    return mask


def apply_filter_spharm_ideal(
    actx: ArrayContext,
    discr: SpectralDiscretization,
    xk: SpectralDOFArray,
    *,
    kmax: int | Sequence[int] | None = None,
    p: float | str | None = None,
) -> SpectralDOFArray:
    r"""Ideal low-pass filtering for Spherical Harmonic coefficients.

    All coefficients for which :math:`(n^p + m^p)^{1/p} > k_{max}` are set
    to zero, where :math:`n` is the order and :math:`m` is the degree of the
    corresponding spherical harmonic.

    :arg kmax: cut-off for spherical harmonic orders.
    :arg p: order of the norm used for the spherical harmonic indices.
    """
    # {{{ validate input

    if p is None:
        p = 2

    if kmax is None:
        kmax = [g.nelphi // 2 - 1 for g in discr.specgroups]

    if isinstance(kmax, int | tuple):
        kmax = [kmax] * len(discr.specgroups)

    if len(kmax) != len(discr.specgroups):
        raise ValueError("'kmax' does not match group count")

    for igrp, (k, grp) in enumerate(zip(kmax, discr.specgroups, strict=False)):
        if isinstance(k, int):
            if k <= 0 or k > grp.nspec:
                raise ValueError(
                    f"'kmax' out of bounds for group {igrp}: "
                    f"got {k}, expected maximum {grp.nspec}"
                )
        elif isinstance(k, tuple):
            if (k[0] <= 0 or k[0] > grp.sh.mmax) or (k[1] <= 0 or k[1] > grp.sh.lmax):
                raise ValueError(
                    f"'kmax' out of bounds for group {igrp}: "
                    f"got {k}, expected maximum ({grp.sh.mmax}, {grp.sh.lmax})"
                )
        else:
            raise TypeError(f"'kmax' has unexpected type: {type(k).__name__}")

    # }}}

    result = []
    for igrp, ixk in enumerate(xk):
        sgrp = discr.specgroups[igrp]

        # FIXME: should check if this works for complex transforms too
        assert sgrp.is_real

        mask = filter_spharm_ideal_factor(actx, sgrp, kmax[igrp], p)
        result.append(actx.np.where(mask.reshape(-1, 1), ixk, actx.np.zeros_like(ixk)))

    return SpectralDOFArray(actx, tuple(result))


# }}}


# {{{ tikhonov


def filter_spharm_tikhonov_filter(
    actx: ArrayContext,
    sgrp: SpectralConnectionElementGroup,
    alpha: float | None = None,
    p: float | None = None,
) -> Any:
    if p is None:
        p = 2

    if alpha is None:
        alpha = 1.0e-5

    sh_l = _tikhonov_sqr_laplace_eigenvalues(actx, sgrp)
    return 1.0 / (1.0 + alpha * sh_l**p)


def apply_filter_spharm_tikhonov(
    actx: ArrayContext,
    discr: SpectralDiscretization,
    xk: SpectralDOFArray,
    *,
    alpha: float | None = None,
    p: int | None = None,
) -> SpectralDOFArray:
    r"""Tikhonov regularization based filtering, obtained by optimizing

    .. math::

        \frac{1}{2} \|f - I[f]\|_2
        + \frac{\alpha}{2} \|\Delta^p_S I[f]\|_2,

    where :math:`\Delta_S` is the Laplacian on the unit sphere and :math:`I[f]`
    is the projection of :math:`f` into the spherical harmonic basis. The
    solution filters as

    .. math::

        \hat{f}^m_n = \frac{f^m_n}{1 + \alpha n^{2p} (n + 1)^{2 p}}.

    :arg alpha: weight used in the regularization; a larger *alpha* implies
        a smaller cutoff region in the filter.
    :arg p: order of the Laplacian used in the regularization; a larger *p*
        implies a steeper cutoff in the filter.
    """

    # {{{ validate input

    if p is None:
        p = 1

    if not isinstance(p, int | np.int32 | np.int64):
        raise TypeError(f"order 'p' should be an <class 'int'>, got {type(p)}")

    if alpha is None:
        alpha = 1.0e-2

    if alpha < 1.0e-13:
        raise ValueError(f"'alpha' should be positive: alpha {alpha}")

    # }}}

    result = []
    for igrp, ixk in enumerate(xk):
        factor = filter_spharm_tikhonov_filter(
            actx, discr.specgroups[igrp], alpha=alpha, p=p
        )

        result.append(factor * ixk)

    return SpectralDOFArray(actx, tuple(result))


# }}}


# {{{ optimized tikhonov


def _tikhonov_gcv(
    actx: ArrayContext,
    discr: SpectralDiscretization,
    xk: SpectralDOFArray,
    *,
    p: int,
    alpha: float,
) -> float:
    if isinstance(alpha, np.ndarray) and alpha.shape == ():
        alpha = alpha[()]

    # {{{ trace

    tr_f = 0
    for sgrp in discr.specgroups:
        sh_l = actx.thaw(_tikhonov_sqr_laplace_eigenvalues(actx, sgrp))
        tr_f += actx.np.sum(1 / (1.0 + alpha * sh_l**p))

    # }}}

    ndofs = sum(ixk.size for ixk in xk)
    xkhat = apply_filter_spharm_tikhonov(actx, discr, xk, alpha=alpha, p=p)

    f = (
        ndofs
        / 2.0
        * actx.np.sum(actx.np.abs(xk - xkhat) ** 2)
        / actx.np.abs(ndofs - tr_f) ** 2
    )

    assert f.dtype.kind == "f"
    return actx.to_numpy(f)


def _tikhonov_gcv_gradient(
    actx: ArrayContext,
    discr: SpectralDiscretization,
    xk: SpectralDOFArray,
    *,
    p: int,
    alpha: float,
) -> float:
    if isinstance(alpha, np.ndarray) and alpha.shape == ():
        alpha = alpha[()]

    # {{{ trace

    tr_f = 0
    for sgrp in discr.specgroups:
        sh_l = _tikhonov_sqr_laplace_eigenvalues(actx, sgrp) ** p
        tr_f += actx.np.sum(1 / (1.0 + alpha * sh_l))

    # }}}

    # {{{ compute gradient

    xkhat = apply_filter_spharm_tikhonov(actx, discr, xk, alpha=alpha, p=p)

    g = 0
    for ixk, ixkhat, sgrp in zip(xk, xkhat, discr.specgroups, strict=False):
        sh_l = _tikhonov_sqr_laplace_eigenvalues(actx, sgrp) ** p
        idk = ixk - ixkhat

        error_f = actx.np.sum(actx.np.abs(idk) ** 2)
        df_dalpha = ixkhat * sh_l / (1 + alpha * sh_l)
        dtrf_dalpha = -actx.np.sum(sh_l / (1 + alpha * sh_l) ** 2)

        ndofs = ixk.size
        g += ndofs * (
            (
                actx.np.sum(actx.np.real(idk * df_dalpha))
                / actx.np.abs(ndofs - tr_f) ** 2
            )
            + error_f / actx.np.abs(ndofs - tr_f) ** 3 * dtrf_dalpha
        )

    # }}}

    assert not isinstance(g, int)
    assert g.dtype.kind == "f"
    return actx.to_numpy(g)


def apply_filter_spharm_tikhonov_opt(
    actx: ArrayContext,
    discr: SpectralDiscretization,
    xk: SpectralDOFArray,
    *,
    p: int | None = None,
    alpha: float | None = None,
    maxiter: int = 32,
    optimize: str | None = None,
    method: str | None = None,
) -> SpectralDOFArray:
    r"""Tikhonov regularization based filtering, where the weight is obtained
    by Generalized Cross Validation.

    :arg maxiter: maximum number of iterations to perform in the optimization
        of the GCV functional.
    :arg optimize: optimization method to use, can be one of
        ``"minimize_scalar"`` or ``"minimize"`` from :mod:`scipy`.
    :arg method: optimization method to use when *optimize* is
        ``"minimize"``.
    """

    if optimize is None:
        # NOTE: this seemed to be the faster method in a simple test
        optimize = "minimize_scalar"

    if p is None:
        # FIXME: p = 2 seems to work nicely for some of the applications, but
        # it is just a guess; can it be made part of the optimization?
        p = 2

    if alpha is None:
        alpha = 1.0

    # FIXME: need to think a bit about how to extend this
    assert len(discr.specgroups) == 1

    # {{{ optimize

    import scipy.optimize as so

    from pytools import ProcessTimer

    with ProcessTimer() as tic:
        if optimize == "minimize_scalar":
            if method is None:
                method = "bounded"

            if method.lower() not in {"bounded"}:
                raise ValueError(f"unsupported 'minimize_scalar' method: '{method}'")

            result = so.minimize_scalar(
                fun=lambda alpha: _tikhonov_gcv(actx, discr, xk, p=p, alpha=alpha),
                method="bounded",
                bounds=(1.0e-12, alpha),
                options={"disp": 0, "xatol": 1.0e-12, "maxiter": maxiter},
            )
        elif optimize == "minimize":
            if method is None:
                # NOTE: this seemed to be the fastest method in a simple test
                method = "slsqp"

            if method.lower() not in {"l-bfgs-b", "tnc", "slsqp"}:
                raise ValueError(f"unsupported 'minimize' method: '{method}'")

            result = so.minimize(
                fun=lambda alpha: _tikhonov_gcv(actx, discr, xk, p=p, alpha=alpha),
                x0=0.5 * alpha,
                jac=lambda alpha: _tikhonov_gcv_gradient(
                    actx, discr, xk, p=p, alpha=alpha
                ),
                bounds=[(1.0e-12, alpha)],
                method=method,
                tol=1.0e-12,
                options={"disp": False, "maxiter": maxiter},
            )
        else:
            raise ValueError(f"unknown optimization method: '{optimize}'")

    logger.info("alpha_opt/%s: %.5e (%s) (%s)", method, result.x, result.message, tic)

    # }}}

    return apply_filter_spharm_tikhonov(actx, discr, xk, alpha=result.x, p=p)


# }}}


# {{{ ideal + tikhonov


def apply_filter_spharm_tikhonov_ideal(
    actx: ArrayContext,
    discr: SpectralDiscretization,
    xk: SpectralDOFArray,
    *,
    kmax: int | Sequence[int] | None = None,
    alpha: float | None = None,
    p: int | None = None,
) -> SpectralDOFArray:
    """Applies a combination of an ideal filter and a Tikhonov regularization.

    The ideal filter is only applied in :math:`m` and the Tikhonov regularization,
    by definition, is applied in :math:`n`. This allows handling the two
    sources of error coming from the DG interpolation.

    See :func:`apply_filter_spharm_ideal` and :func:`apply_filter_spharm_tikhonov`.
    """
    if kmax is None:
        kmax = tuple([g.nelphi // 2 - 1 for g in discr.specgroups])

    if isinstance(kmax, int):
        kmax = [kmax] * len(discr.specgroups)

    xk = apply_filter_spharm_tikhonov(actx, discr, xk, alpha=alpha, p=p)

    result = []
    for igrp, ixk in enumerate(xk):
        sgrp = discr.specgroups[igrp]
        assert sgrp.is_real

        _, sh_m = _ideal_nm_indices(actx, sgrp)
        mask = (abs(sh_m) < kmax[igrp]).reshape(-1, 1)
        result.append(actx.np.where(mask, ixk, actx.np.zeros_like(ixk)))

    return SpectralDOFArray(actx, tuple(result))


# }}}
