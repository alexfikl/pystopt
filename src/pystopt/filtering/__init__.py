# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

"""
.. currentmodule:: pystopt.filtering

.. autoclass:: FilterType
   :undoc-members:
   :inherited-members:

.. autofunction:: apply_filter_spectral
.. autofunction:: apply_normal_gradient_filter_spectral
.. autofunction:: apply_filter_by_type

.. autofunction:: modal_decay_estimate
.. autofunction:: interp_error_estimate
"""

import enum
from typing import Any

import numpy as np

from arraycontext import ArrayContext, ArrayOrContainerT, make_loopy_program
from meshmode.transform_metadata import ConcurrentDOFInameTag, ConcurrentElementInameTag
from pytential import GeometryCollection
from pytools import memoize_in

from pystopt import sym
from pystopt.dof_array import SpectralDOFArray
from pystopt.filtering.fourier import (
    apply_filter_fourier_butterworth,
    apply_filter_fourier_ideal,
    apply_filter_fourier_trapezoidal,
)
from pystopt.filtering.spharm import (
    apply_filter_spharm_ideal,
    apply_filter_spharm_tikhonov,
    apply_filter_spharm_tikhonov_ideal,
    apply_filter_spharm_tikhonov_opt,
)
from pystopt.filtering.yukawa import apply_yukawa_filter, apply_yukawa_smoothing
from pystopt.mesh import SpectralDiscretization

__all__ = (
    "apply_filter_fourier_butterworth",
    "apply_filter_fourier_ideal",
    "apply_filter_fourier_trapezoidal",
    "apply_filter_spectral",
    "apply_filter_spharm_ideal",
    "apply_filter_spharm_tikhonov",
    "apply_filter_spharm_tikhonov_ideal",
    "apply_filter_spharm_tikhonov_opt",
    "apply_normal_gradient_filter_spectral",
    "apply_yukawa_filter",
    "apply_yukawa_smoothing",
    "interp_error_estimate",
    "modal_decay_estimate",
)


# {{{ spectral filtering


@enum.unique
class FilterType(enum.Enum):
    Full = enum.auto()
    Normal = enum.auto()
    Unfiltered = enum.auto()


def apply_filter_spectral(
    actx: ArrayContext,
    discr: SpectralDiscretization,
    x: ArrayOrContainerT,
    *,
    method: str = "ideal",
    **kwargs: Any,
) -> ArrayOrContainerT:
    """Apply a spectral filter to an array *x*.

    For curves in two-dimensional space, the following Fourier-based filters
    are available

    - ``"ideal"`` uses :func:`apply_filter_fourier_ideal`.
    - ``"trapezoidal"`` uses :func:`apply_filter_fourier_trapezoidal`.
    - ``"butterworth"`` uses :func:`apply_filter_fourier_butterworth`.

    For surfaces in three-dimensional space, the following Spherical Harmonic-based
    filters are available

    - ``"ideal"`` uses :func:`apply_filter_spharm_ideal`.
    - ``"tikhonov"`` uses :func:`apply_filter_spharm_tikhonov`.
    - ``"tikhonov_opt"`` uses :func:`apply_filter_spharm_tikhonov_opt`.
    - ``"tikhonov_ideal"`` uses :func:`apply_filter_spharm_tikhonov_ideal`.

    :arg x: array container to filter.
    :arg method: filtering method to use.
    """

    dofdesc = kwargs.pop("dofdesc", None)
    if isinstance(discr, GeometryCollection):
        places = discr
        if dofdesc is None:
            dofdesc = places.auto_source
        dofdesc = sym.as_dofdesc(dofdesc)

        discr = places.get_discretization(dofdesc.geometry, dofdesc.discr_stage)

    # {{{ get filtering function

    if discr.ambient_dim == 2:
        from pystopt.mesh import FourierDiscretization

        assert isinstance(discr, FourierDiscretization)

        if method == "ideal":
            func = apply_filter_fourier_ideal
        elif method == "trapezoidal":
            func = apply_filter_fourier_trapezoidal
        elif method == "butterworth":
            func = apply_filter_fourier_butterworth
        else:
            raise ValueError(f"unknown method: '{method}'")
    elif discr.ambient_dim == 3:
        from pystopt.mesh import SphericalHarmonicDiscretization

        assert isinstance(discr, SphericalHarmonicDiscretization)

        if method == "ideal":
            func = apply_filter_spharm_ideal
        elif method == "tikhonov":
            func = apply_filter_spharm_tikhonov
        elif method == "tikhonov_opt":
            func = apply_filter_spharm_tikhonov_opt
        elif method == "tikhonov_ideal":
            func = apply_filter_spharm_tikhonov_ideal
        else:
            raise ValueError(f"unknown method: '{method}'")
    else:
        raise ValueError(f"filter not supported in {discr.ambient_dim}d")

    # }}}

    def _apply_filter(ary):
        from numbers import Number

        if isinstance(ary, Number):
            return ary

        from meshmode.dof_array import DOFArray

        if not isinstance(ary, DOFArray):
            raise TypeError(f"cannot filter array of type '{type(ary).__name__}'")

        xk = ary
        if not isinstance(ary, SpectralDOFArray):
            xk = discr.to_spectral_conn(xk)

        xk = func(actx, discr, xk, **kwargs)

        if not isinstance(ary, SpectralDOFArray):
            xk = discr.from_spectral_conn(xk)

        return xk

    from meshmode.dof_array import rec_map_dof_array_container

    return rec_map_dof_array_container(_apply_filter, x)


def apply_normal_gradient_filter_spectral(
    actx: ArrayContext,
    places: GeometryCollection,
    g: np.ndarray,
    *,
    dofdesc: sym.DOFDescriptorLike | None = None,
    only_normal: bool = False,
    method: str = "ideal",
    **kwargs: Any,
) -> np.ndarray:
    r"""Wrapper around :func:`apply_filter_spectral` that can be used to only
    filter the normal component of the vector *g*.

    This is mainly meant for shape optimization, where the gradient is of the
    norm :math:`g \mathbf{n}`. Filtering the whole vector *g* would result in
    a gradient that does not point purely in the normal direction and can
    unnaturally distort the mesh.

    :arg only_normal: if *True*, the tangential component is discarded
        completely after filtering.
    """

    if dofdesc is None:
        dofdesc = places.auto_source
    dofdesc = sym.as_dofdesc(dofdesc)

    discr = places.get_discretization(dofdesc.geometry, dofdesc.discr_stage)
    assert g.size == discr.ambient_dim

    from pystopt import bind

    # filter normal component
    g_dot_n = bind(places, sym.n_dot(places.ambient_dim), auto_where=dofdesc)(actx, x=g)
    g_dot_n = apply_filter_spectral(actx, discr, g_dot_n, method=method, **kwargs)

    # reconstruct gradient
    if only_normal:
        g = bind(places, sym.n_times(places.ambient_dim), auto_where=dofdesc)(
            actx, x=g_dot_n
        )
    else:
        g_dot_t = bind(
            places, sym.project_tangent(places.ambient_dim), auto_where=dofdesc
        )(actx, x=g)

        g = bind(
            places,
            sym.make_sym_vector("g_dot_t", places.ambient_dim)
            + sym.n_times(places.ambient_dim),
            auto_where=dofdesc,
        )(actx, x=g_dot_n, g_dot_t=g_dot_t)

    return g


def apply_filter_by_type(
    actx: ArrayContext,
    places: GeometryCollection,
    ary: ArrayOrContainerT,
    *,
    filter_type: FilterType = FilterType.Unfiltered,
    filter_arguments: dict[str, Any] | None = None,
    dofdesc: sym.DOFDescriptorLike | None = None,
    force_spectral: bool = False,
) -> ArrayOrContainerT:
    if filter_arguments is None:
        filter_arguments = {}

    dofdesc = places.auto_source if dofdesc is None else sym.as_dofdesc(dofdesc)
    discr = places.get_discretization(dofdesc.geometry, dofdesc.discr_stage)

    from pystopt.mesh.spectral import to_spectral

    if filter_type == FilterType.Full:
        if force_spectral:
            ary = to_spectral(discr, ary, strict=False)

        result = apply_filter_spectral(actx, discr, ary, **filter_arguments)
    elif filter_type == FilterType.Normal:
        assert isinstance(ary, np.ndarray)
        result = apply_normal_gradient_filter_spectral(
            actx, places, ary, dofdesc=dofdesc, **filter_arguments
        )

        if force_spectral:
            result = to_spectral(discr, ary, strict=False)
    elif filter_type == FilterType.Unfiltered:
        if force_spectral:
            ary = to_spectral(discr, ary, strict=False)

        result = ary
    else:
        raise ValueError(f"unknown filter type: '{filter_type}'")

    return result


# }}}


# {{{ smoothness measure


def modal_decay_estimate(
    actx: ArrayContext, discr: SpectralDiscretization, x: ArrayOrContainerT
) -> float:
    @memoize_in(actx, (modal_decay_estimate, "modal_coeffs_knl"))
    def prg():
        t_unit = make_loopy_program(
            "{[iel, idof, j]: 0 <= iel < nelements and 0 <= idof,j < ndofs}",
            """
            result[iel, idof] = sum(j, vinv[idof, j] * vec[iel, j])
            """,
            name="modal_coeffs",
        )

        import loopy as lp

        return lp.tag_inames(
            t_unit,
            {
                "iel": ConcurrentElementInameTag(),
                "idof": ConcurrentDOFInameTag(),
            },
        )

    import modepy as mp
    from modepy.modal_decay import fit_modal_decay

    def vinv(grp):
        # FIXME: should really be memoized
        return actx.from_numpy(
            np.linalg.inv(mp.vandermonde(grp.basis(), grp.unit_nodes))
        )

    from meshmode.dof_array import DOFArray

    coeffs = DOFArray(
        actx,
        tuple([
            actx.call_loopy(prg(), vinv=vinv(grp), vec=gx)["result"]
            for gx, grp in zip(x, discr.groups, strict=True)
        ]),
    )

    estimate = -float("inf")
    for gc, grp in zip(coeffs, discr.groups, strict=True):
        exp, _ = fit_modal_decay(actx.to_numpy(gc), grp.dim, grp.order)
        estimate = max(estimate, exp[0])

    return estimate


def interp_error_estimate(
    actx: ArrayContext, discr: SpectralDiscretization, x: ArrayOrContainerT
) -> ArrayOrContainerT:
    @memoize_in(actx, (interp_error_estimate, "interp_rel_error_knl"))
    def prg():
        t_unit = make_loopy_program(
            """{[iel, idof, itail, i, j]:
                0 <= iel < nelements
                and 0 <= idof, i, j < ndofs
                and 0 <= itail < nnodes}
            """,
            """
            <> norm_coeffs = sum(idof, \
                    sum(i, vinv[idof, i] * vec[iel, i])**2)
            <> norm_interp = sum(itail, \
                    sum(j, interp[itail, j] * vec[iel, j])**2)

            result[iel] = sqrt(norm_interp / norm_coeffs)
            """,
            name="interp_rel_error",
        )

        import loopy as lp

        return lp.tag_inames(
            t_unit,
            {
                "iel": ConcurrentElementInameTag(),
            },
        )

    import modepy as mp
    from modepy.modal_decay import simplex_interp_error_coefficient_estimator_matrix

    from meshmode.mesh import SimplexElementGroup

    assert all(isinstance(grp, SimplexElementGroup) for grp in discr.mesh.groups)

    def group_vinv(grp):
        return actx.from_numpy(
            np.linalg.inv(mp.vandermonde(grp.basis(), grp.unit_nodes))
        )

    def group_interp(grp):
        return actx.from_numpy(
            simplex_interp_error_coefficient_estimator_matrix(
                grp.unit_nodes, grp.order, n_tail_orders=1 if discr.dim > 1 else 2
            )
        )

    grp_start = 0
    estimator = actx.np.zeros(discr.mesh.nelements, x.entry_dtype)
    for x_grp, grp in zip(x, discr.groups, strict=True):
        prg()(
            actx.queue,
            vinv=group_vinv(grp),
            interp=group_interp(grp),
            vec=x_grp,
            result=estimator[grp_start : grp_start + grp.nelements],
        )

        grp_start += grp.nelements

    return actx.to_numpy(np.max(actx.np.fabs(estimator)))


# }}}
