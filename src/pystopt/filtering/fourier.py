# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

"""
.. currentmodule:: pystopt.filtering

.. autofunction:: apply_filter_fourier_ideal
.. autofunction:: apply_filter_fourier_trapezoidal
.. autofunction:: apply_filter_fourier_butterworth
"""

from collections.abc import Callable, Sequence
from typing import TypeVar

from arraycontext import ArrayContext

from pystopt.dof_array import SpectralDOFArray
from pystopt.mesh import SpectralDiscretization
from pystopt.tools import get_default_logger

logger = get_default_logger(__name__)
T = TypeVar("T")


def _apply_fourier_filter(
    actx: ArrayContext,
    discr: SpectralDiscretization,
    xk: SpectralDOFArray,
    ffilter: Callable[[int, T], T],
) -> SpectralDOFArray:
    return SpectralDOFArray(
        actx,
        tuple(
            ixk * ffilter(igrp, actx.thaw(discr._fftfreq(igrp)))
            for igrp, ixk in enumerate(xk)
        ),
    )


def _normalize_wavenumber(
    discr: SpectralDiscretization, kmax: int | Sequence[int] | None
) -> tuple[int, ...]:
    if kmax is None:
        kmax = tuple(max(grp.nelements // 2 - 4, 1) for grp in discr.groups)

    from numbers import Integral

    if isinstance(kmax, Integral):
        kmax = (kmax,) * len(discr.specgroups)
    else:
        assert len(kmax) == len(discr.specgroups)

    return tuple(
        (int(k) if k >= 0 else int(grp.nspec + k))
        for k, grp in zip(kmax, discr.specgroups, strict=False)
    )


# {{{ ideal


def apply_filter_fourier_ideal(
    actx: ArrayContext,
    discr: SpectralDiscretization,
    xk: SpectralDOFArray,
    *,
    kmax: int | Sequence[int] | None = None,
) -> SpectralDOFArray:
    r"""Ideal filter in Fourier space based on

    .. math::

        \hat{f}_k = \begin{cases}
        f_k, & \quad |k| < k_{max}, \\
        0, & \quad \text{otherwise}.
        \end{cases}

    :arg x: array to filter.
    :arg kmax: cutoff wavenumber above which all coefficients are set to zero.
    """

    # {{{ validate input

    kmax = _normalize_wavenumber(discr, kmax)
    if any(
        (k <= 0 or k > grp.nspec)
        for k, grp in zip(kmax, discr.specgroups, strict=False)
    ):
        raise ValueError(
            f"'kmax' out of bounds: {kmax} out of "
            f"{tuple(grp.nspec for grp in discr.specgroups)}"
        )

    # }}}

    def _filter_fourier_ideal(igrp: int, k: T) -> T:
        grp_k_max = kmax[igrp]

        return actx.np.where(
            actx.np.abs(k) < grp_k_max, actx.np.ones_like(k), actx.np.zeros_like(k)
        )

    return _apply_fourier_filter(actx, discr, xk, _filter_fourier_ideal)


# }}}


# {{{ trapezoidal


def apply_filter_fourier_trapezoidal(
    actx: ArrayContext,
    discr: SpectralDiscretization,
    xk: SpectralDOFArray,
    *,
    ka: int | Sequence[int] | None = None,
    kb: int | Sequence[int] | None = None,
) -> SpectralDOFArray:
    r"""Trapezoidal filter in Fourier space based on

    .. math::

        \hat{f}_k = \begin{cases}
        f_k, & \quad |k| <= k_a, \\
        \frac{k_b - k}{k_b - k_a} f_k, & \quad k_a < |k| < k_b, \\
        0, & \quad |k| >= k_b.
        \end{cases}

    :arg x: array to filter.
    :arg ka: wavenumber at which the ramp should start.
    :arg kb: wavenumber at which the ramp should end, above which all
        coefficients are set to zero.
    """

    # {{{ validate input

    if ka is None:
        ka = (0,) * len(discr.specgroups)

    ka = _normalize_wavenumber(discr, ka)
    if any(k >= grp.nspec for k, grp in zip(ka, discr.specgroups, strict=False)):
        raise ValueError(
            f"'ka' out of bounds: {ka} out of "
            f"{tuple(grp.nspec for grp in discr.specgroups)}"
        )

    if kb is None:
        kb = tuple(
            min(k + grp.nelements // 2, sgrp.nspec)
            for k, grp, sgrp in zip(ka, discr.groups, discr.specgroups, strict=False)
        )

    kb = _normalize_wavenumber(discr, kb)
    if any(
        (k <= 0 or k > grp.nspec) for k, grp in zip(kb, discr.specgroups, strict=False)
    ):
        raise ValueError(
            f"'kb' out of bounds: {kb} out of "
            f"{tuple(grp.nspec for grp in discr.specgroups)}"
        )

    if any(ika >= ikb for ika, ikb in zip(ka, kb, strict=False)):
        raise ValueError(f"wavenumber 'ka' >= 'kb': ka {ka} kb {kb}")

    # }}}

    def _filter_fourier_trapezoidal(igrp: int, k: T) -> T:
        grp_ka = ka[igrp]
        grp_kb = kb[igrp]

        k = actx.np.abs(k)
        return actx.np.where(
            k > grp_kb,
            actx.np.zeros_like(k),
            actx.np.where(
                k <= grp_ka, actx.np.ones_like(k), (grp_kb - k) / (grp_kb - grp_ka)
            ),
        )

    return _apply_fourier_filter(actx, discr, xk, _filter_fourier_trapezoidal)


# }}}


# {{{ butterworth


def apply_filter_fourier_butterworth(
    actx: ArrayContext,
    discr: SpectralDiscretization,
    xk: SpectralDOFArray,
    *,
    kmax: int | Sequence[int] | None = None,
    p: float | Sequence[float] | None = None,
) -> SpectralDOFArray:
    r"""Butterworth filter in Fourier space based on

    .. math::

        \hat{f}_k = \left[1 + \left(\frac{k}{k_{max}}\right)^p\right] f_k.

    where a smaller :math:`k_{max}` has the effect of localizing the wavenumbers
    around :math:`0` and a larger :math:`p` has the effect of making the
    transition sharper.

    :arg x: array to filter.
    :arg kmax: cutoff wavenumber above which all coefficients are set to zero.
    """

    # {{{ validate input

    if kmax is None:
        kmax = tuple(max(1, grp.nelements // 8) for grp in discr.groups)

    kmax = _normalize_wavenumber(discr, kmax)
    if any(
        (k <= 0 or k > grp.nspec)
        for k, grp in zip(kmax, discr.specgroups, strict=False)
    ):
        raise ValueError(
            f"'kmax' out of bounds: {kmax} out of "
            f"{tuple(grp.nspec for grp in discr.specgroups)}"
        )

    if p is None:
        p = (6.0,) * len(discr.specgroups)

    from numbers import Number

    if isinstance(p, Number):
        p = (float(p),) * len(discr.specgroups)
    else:
        assert len(p) == len(discr.specgroups)

    if any(ip <= 0 for ip in p):
        raise ValueError(f"power 'p' cannot be <= 0: {p}")

    # }}}

    def _filter_fourier_butterworth(igrp: int, k: T) -> T:
        grp_k_max = kmax[igrp]
        grp_p = p[igrp]

        return 1.0 / actx.np.sqrt(1.0 + (k / grp_k_max) ** float(grp_p))

    return _apply_fourier_filter(actx, discr, xk, _filter_fourier_butterworth)


# }}}
