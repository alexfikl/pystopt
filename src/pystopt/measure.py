# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

"""
.. currentmodule:: pystopt.measure

Measuring
---------

.. autoexception:: IncorrectConvergenceOrderError

.. autoclass:: EOCRecorder
    :no-show-inheritance:

.. autofunction:: verify_eoc
.. autofunction:: stringify_eoc
.. autofunction:: visualize_eoc

.. autoclass:: TimeResult
    :no-show-inheritance:

.. autofunction:: timeit

.. autoclass:: TicTocTimer
    :no-show-inheritance:

.. autofunction:: format_seconds
.. autofunction:: estimate_wall_time_from_timestep
.. autofunction:: estimate_wall_time_from_iteration
"""

import os
import time
from collections.abc import Callable
from dataclasses import dataclass, field
from typing import Any

import numpy as np

from pytools.convergence import EOCRecorder as EOCRecorderBase

from pystopt.tools import get_default_logger

logger = get_default_logger(__name__)


# {{{ convergence


class _NoExpectedOrder:
    pass


class IncorrectConvergenceOrderError(ValueError):
    """
    .. autoattribute:: p_est
    .. autoattribute:: max_error
    """

    def __init__(self, msg: str, p_est: float, max_error: float) -> None:
        super().__init__(msg)

        self.p_est: float = p_est
        """Estimated order of convergence that was not satisfied."""
        self.max_error: float = max_error
        """Maximum error throughout the history."""


class EOCRecorder(EOCRecorderBase):
    """Bases ``pytools.convergence.EOCRecorder``.

    .. autoattribute:: name
    .. autoattribute:: expected_order

    .. automethod:: add_data_point
    .. automethod:: order_estimate
    .. automethod:: max_error
    """

    def __init__(
        self, *, name: str = "Error", expected_order: float | None = None
    ) -> None:
        """
        :arg name: a name that will be used for the column name when, e.g.,
            stringifying with :func:`stringify_eoc`.
        :arg expected_order: the expected order for the data added to the
            recorder. This is used if checking :meth:`satisfied`, stringifying
            or visualizing the data.
        """

        super().__init__()

        self.name: str = name
        """An identifier for the error being computed."""
        self.expected_order: float | None = expected_order
        """Expected order of convergence of the errors."""

    def __str__(self) -> str:
        return stringify_eoc(self)

    def satisfied(
        self,
        *,
        order: float | None = None,
        window: float | None = 0.25,
        atol: float = 1.0e-14,
        strict: bool = False,
    ) -> bool:
        try:
            verify_eoc(self, order, window=window, atol=atol, strict=strict)
        except IncorrectConvergenceOrderError:
            return False
        else:
            return True


def verify_eoc(
    eoc: EOCRecorder,
    expected_order: float | None = None,
    *,
    window: float = 0.25,
    atol: float = 1.0e-14,
    strict: bool = False,
) -> None:
    r"""Checks that the order of convergence matches an expected value.

    The order estimate :math:`p_{est}` is satisfied if

    .. math::

        p - w < p_{est} < p + w

    where :math:`p` is the expected order and :math:`w` is a given *window*.
    The error estimate is satisfied if

    .. math::

        \max e_i < atol

    :arg strict: If *True*, :math:`p_{est}` is bounded on both sides (as above).
        Otherwise, just the lower bound is checked, which allows for an order
        of convergence much better than the expected one.

    :raises IncorrectConvergenceOrderError: if the estimated order of
        convergence is not within the margins of *expected_order* or the
        maximum error is larger than *atol*.
    """

    if expected_order is None:
        p = eoc.expected_order
    else:
        p = expected_order

    max_error = eoc.max_error()
    is_satisfied = max_error < atol

    p_est = eoc.order_estimate()
    if p is not None:
        if strict:
            is_satisfied = is_satisfied or (p - window < p_est < p + window)
        else:
            is_satisfied = is_satisfied or (p - window < p_est)

    if not is_satisfied:
        if p is None:
            raise IncorrectConvergenceOrderError(
                f"'{eoc.name}': expected max error {max_error:.5e} < {atol:.5e}",
                p_est,
                max_error,
            )
        else:
            raise IncorrectConvergenceOrderError(
                f"'{eoc.name}': expected {p - window} < eoc '{p_est}' < {p + window} "
                f"or max error '{max_error:.5e}' < {atol:.5e}",
                p_est,
                max_error,
            )


def stringify_eoc(
    *eocs: EOCRecorder,
    table_format: str = "unicode",
    abscissa_name: str = "h",
    abscissa_format: str | None = None,
    error_format: str | None = None,
    eoc_format: str | None = None,
):
    r"""
    :arg table_format: a string defining the table format. Can be one of
        ``["markdown", "latex", "unicode"]``.
    :arg abscissa_format: :meth:`str.format` compatible formatting string.
    :arg error_format: :meth:`str.format` compatible formatting string.
    :arg eoc_format: :meth:`str.format` compatible formatting string.
    """

    # {{{ validate

    def _compare_abscissa(e0, e1):
        return np.allclose(
            np.array([h for h, _ in e0.history]),
            np.array([h for h, _ in e1.history]),
        )

    from pytools import is_single_valued

    if not is_single_valued(len(eoc.history) for eoc in eocs):
        raise RuntimeError("all EOCRecorders must have the same history size")

    if not is_single_valued(eocs, equality_pred=_compare_abscissa):
        raise RuntimeError("all EOCRecorders must have the same abscissa")

    ncolumns = 1 + 2 * len(eocs)
    nrows = len(eocs[0].history)
    abscissas = np.array([h for h, _ in eocs[0].history])

    # }}}

    # {{{ formatting options

    if abscissa_format is None:
        abscissa_format = "{:.6e}"
    if error_format is None:
        error_format = "{:.6e}"
    if eoc_format is None:
        eoc_format = "{:+2.2f}"

    table_format = table_format.lower()

    from pystopt import table

    if table_format == "latex":
        tbl = table.LatexTable(ncolumns=ncolumns)
    elif table_format == "markdown":
        tbl = table.MarkdownTable(ncolumns=ncolumns)
    elif table_format == "unicode":
        tbl = table.UnicodeTable(ncolumns=ncolumns, box_type="heavy")
    else:
        raise ValueError(f"unknown table format: '{table_format}'")

    # }}}

    def _eoc_str(eoc, gm, i):
        if i < gliding_mean - 1:
            eoc_str = "---"
        else:
            eoc_str = eoc_format.format(gm[i - gliding_mean + 1, 1])

        error_str = error_format.format(eoc.history[i][1])
        return (error_str, eoc_str)

    # add header
    tbl.add_header((
        abscissa_name,
        *sum(((getattr(eoc, "name", "Error"), "EOC") for eoc in eocs), ()),
    ))

    # add rows
    gliding_mean = 2
    gms = [eoc.estimate_order_of_convergence(gliding_mean) for eoc in eocs]

    for i in range(nrows):
        h_str = abscissa_format.format(abscissas[i])
        tbl.add_row((
            h_str,
            *sum(
                (_eoc_str(eoc, gm, i) for eoc, gm in zip(eocs, gms, strict=False)), ()
            ),
        ))

    # add overall order
    if len(abscissas) > 1:
        tbl.add_footer((
            "Overall",
            *sum((("", eoc_format.format(eoc.order_estimate())) for eoc in eocs), ()),
        ))

        if any(eoc.expected_order is not None for eoc in eocs):
            tbl.add_footer((
                "Expected",
                *sum(
                    (
                        (
                            "",
                            (
                                eoc_format.format(eoc.expected_order)
                                if eoc.expected_order
                                else "--"
                            ),
                        )
                        for eoc in eocs
                    ),
                    (),
                ),
            ))

    return str(tbl)


def visualize_eoc(
    fig_or_filename: os.PathLike,
    *eocs: EOCRecorder,
    order: float | None = None,
    abscissa: str = "h",
    ylabel: str = "Error",
    enable_legend: bool = True,
    overwrite: bool = False,
) -> None:
    """
    :arg fig_or_filename: output file name or an existing figure.
    :arg order: expected order for all the errors recorded in *eocs*.
    :arg abscissa: name for the abscissa.
    """
    if not eocs:
        raise ValueError("no EOCRecorders are provided")

    from pystopt.visualization.matplotlib import subplots

    with subplots(fig_or_filename, overwrite=overwrite) as fig:
        ax = fig.gca()

        # {{{ plot eocs

        max_h, max_e, min_e = -np.inf, np.inf, np.inf
        has_local_plot = False
        for eoc in eocs:
            h, error = np.array(eoc.history).T
            ax.loglog(h, error, "o-", label=f"${eoc.name}$")

            imax = np.argmax(h)
            local_max_h = h[imax]
            local_max_e = np.max(error)
            local_min_e = np.min(error)

            max_h = max(max_h, local_max_h)
            max_e = min(max_e, error[imax])
            min_e = min(min_e, local_min_e)

            if order is None and eoc.expected_order is not None:
                has_local_plot = True
                slope = eoc.expected_order
                local_min_h = np.exp(
                    np.log(local_max_h) + np.log(local_min_e / local_max_e) / slope
                )

                ax.loglog([local_max_h, local_min_h], [local_max_e, local_min_e], "k-")

        # }}}

        # {{{ plot order

        if order is not None and not has_local_plot:
            min_h = np.exp(np.log(max_h) + np.log(min_e / max_e) / order)
            ax.loglog([max_h, min_h], [max_e, min_e], "k-")

        # }}}

        ax.grid(visible=True, which="major", linestyle="-", alpha=0.75)
        ax.grid(visible=True, which="minor", linestyle="--", alpha=0.5)

        from pystopt.visualization.matplotlib import preprocess_latex

        ax.set_xlabel(preprocess_latex(abscissa))
        ax.set_ylabel(preprocess_latex(ylabel))

        if enable_legend:
            ax.legend()


# }}}


# {{{ timeit wrapper


@dataclass(frozen=True)
class TimeResult:
    """
    .. autoattribute:: walltime
    .. autoattribute:: mean
    .. autoattribute:: std
    """

    __slots__ = ("mean", "std", "walltime")

    walltime: float
    """Smallest value of the walltime for all the runs."""
    mean: float
    """Mean value for the walltime."""
    std: float
    """Standard deviation for the walltime."""

    def __str__(self) -> str:
        return f"wall {self.walltime:.3e}s mean {self.mean:.3e}s ± {self.std:.3e}"


def timeit(
    stmt: str | Callable[[], Any],
    *,
    setup: str | Callable[[], Any] = "pass",
    repeat: int = 16,
    number: int = 1,
) -> TimeResult:
    """Run *stmt* using :func:`timeit.repeat`.

    :returns: a :class:`TimeResult` with statistics about the runs.
    """

    import timeit as _timeit

    r = _timeit.repeat(stmt=stmt, setup=setup, repeat=repeat + 1, number=number)
    r = np.array(r)

    return TimeResult(walltime=np.min(r), mean=np.mean(r), std=np.std(r, ddof=1))


# }}}


# {{{ cProfile decorator


def profileit(func):
    import cProfile
    import datetime

    from pystopt.tools import slugify

    def wrapper(*args, **kwargs):
        today = datetime.datetime.utcnow()
        filename = slugify(f"{func.__name__}_{today}.cProfile")

        prof = cProfile.Profile()
        retval = prof.runcall(func, *args, **kwargs)

        prof.dump_stats(filename)
        return retval

    return wrapper


# }}}


# {{{ tick-toc timer with history


@dataclass
class TicTocTimer:
    """A simple tic-toc-like timer with support for various statistics.

    The intended use of this timer is in some looping code where we want to
    keep track of the iteration time. For example

    .. code:: python

        timer = TicTocTimer()
        for i in range(maxit):
            timer.tic()
            # ... loads of heavy work ...
            timer.toc()
            print(f"elapsed: {timer}")

        print(timer.stats())

    .. automethod:: tic
    .. automethod:: toc
    .. automethod:: stats
    .. automethod:: __str__
    """

    perf_counter_elapsed: float = field(init=False)
    process_time_elapsed: float = field(init=False)

    perf_counter_start: float = field(init=False, repr=False)
    process_time_start: float = field(init=False, repr=False)

    perf_counter_history: list[float] = field(
        default_factory=list, init=False, repr=False
    )
    process_time_history: list[float] = field(
        default_factory=list, init=False, repr=False
    )
    perf_counter_total: float = field(default=0.0, init=False, repr=False)

    def tic(self) -> None:
        """Clear previous values and start the timer."""

        self.perf_counter_start = time.perf_counter()
        self.process_time_start = time.process_time()

        self.perf_counter_elapsed = None
        self.process_time_elapsed = None

    def toc(self) -> None:
        """Stop the timer and store elapsed values since last call to :meth:`tic`.

        At this point, the history of the timer is also updated.
        """

        self.perf_counter_elapsed = time.perf_counter() - self.perf_counter_start
        self.process_time_elapsed = time.process_time() - self.process_time_start

        self.perf_counter_history.append(self.perf_counter_elapsed)
        self.process_time_history.append(self.process_time_elapsed)

        self.perf_counter_total += self.perf_counter_elapsed

    def stats(self) -> TimeResult:
        """
        :returns: an instance of :class:`TimeResult` with statistics from all
            the calls to :meth:`tic` and :meth:`toc`.
        """
        history = np.array(self.perf_counter_history)
        return TimeResult(
            walltime=np.min(history), mean=np.mean(history), std=np.std(history)
        )

    def __str__(self) -> str:
        """Stringify the latest elapsed time from :meth:`toc`.

        If called outside a :meth:`tic`-:meth:`toc` pair, an empty string is
        returned instead.
        """
        if self.perf_counter_elapsed is None:
            return "---"

        wall = self.perf_counter_elapsed
        cpux = self.process_time_elapsed / self.perf_counter_elapsed

        if wall < 60:
            return f"wall {wall:.3e}s cpu {cpux:.1f}x"
        else:
            wall_str = format_seconds(wall)
            return f"wall {wall_str} cpu {cpux:.1f}x"


# }}}


# {{{ wall time estimate


def format_seconds(s: float, *, days: bool = False) -> str:
    """Format (fractional) seconds to a nicer format ``%Hh%Mm%Ss``.

    :arg s: an elapsed time in (fractional) seconds, as obtained from, e.g.,
        :func:`time.perf_counter`.
    :arg days: if *True*, days are listed separately, instead of being added
        to hours.
    """
    import datetime

    delta = datetime.timedelta(seconds=int(s))

    hours, remainder = divmod(delta.seconds, 60 * 60)
    minutes, seconds = divmod(remainder, 60)

    if days:
        return f"{delta.days}d{hours}h{minutes}m{seconds}s"
    else:
        hours += delta.days * 24
        return f"{hours}h{minutes}m{seconds}s"


def estimate_wall_time_from_timestep(
    timer: TicTocTimer,
    dt: float | None,
    n: int | None,
    t: float | None,
    maxit: int | None,
    tmax: float | None,
    nhistory: int | None = 5,
) -> tuple[float, float | None]:
    """Estimate total time of an iteration based on a *timer*.

    This function is meant to be used for time advancing simulations.
    Then, if we know the current iteration *n* and the maximum number of
    iterations *maxit*, we can use the history of the timer to
    estimate the remaining wall time.

    If *tmax* is given instead of *maxit*, we can simply compute::

        remaining_steps = int((tmax - t) / dt)

    :arg dt: time step used in the iteration.
    :arg n: current iteration count.
    :arg t: current time in the iteration.
    :arg maxit: maximum number of iterations.
    :arg tmax: maximum time of the iteration.
    :arg nhistory: number of items in the history of the *timer* to use
        in estimating the real time per iteration.
    """
    t_elapsed = timer.perf_counter_total
    if nhistory is None or nhistory > len(timer.perf_counter_history):
        time_per_timestep = t_elapsed / len(timer.perf_counter_history)
    else:
        time_per_timestep = np.mean(timer.perf_counter_history[-nhistory:])

    if tmax is not None or maxit is not None:
        if maxit is not None:
            assert n is not None
            remaining_time_steps = maxit - n
        elif tmax is not None:
            assert t is not None
            assert dt is not None
            remaining_time_steps = int((tmax - t) / dt)
        else:
            raise AssertionError

        t_remaining = time_per_timestep * (remaining_time_steps + 1)
    else:
        t_remaining = None

    return t_elapsed, t_remaining


def estimate_wall_time_from_iteration(timer: TicTocTimer, n: int, maxit: int):
    """Simplified version of :func:`estimate_wall_time_from_timestep`
    for the case where we know the iteration counts.
    """
    t_elapsed = timer.perf_counter_total
    time_per_timestep = t_elapsed / len(timer.perf_counter_history)

    remaining_time_steps = maxit - n
    t_remaining = time_per_timestep * (remaining_time_steps + 1)

    return t_elapsed, t_remaining


# }}}
