# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

"""
This module defined all the derivatives required and supported by the
optimization problems. The specific implementations are registered through
the :func:`functools.singledispatch` mechanism.

The intended use of this functionality, e.g., for a
:class:`~pystopt.cost.ShapeFunctional` is

.. code:: python

    import pystopt.derivatives as grad
    g = grad.shape(cost, ambient_dim, dofdesc=dofdesc)

.. autofunction:: shape
.. autofunction:: velocity
.. autofunction:: traction
.. autofunction:: capillary
"""

from functools import singledispatch

from pystopt.tools import get_default_logger

logger = get_default_logger(__name__)


@singledispatch
def shape(self, *args, **kwargs):
    """Computes the shape derivative of *self*."""
    raise NotImplementedError(type(self).__name__)


@singledispatch
def velocity(self, *args, **kwargs):
    """Computes the derivative with respect to the velocity field of *self*."""
    raise NotImplementedError(type(self).__name__)


@singledispatch
def traction(self, *args, **kwargs):
    """Computes the derivative with respect to the traction of *self*."""
    raise NotImplementedError(type(self).__name__)


@singledispatch
def capillary(self, *args, **kwargs):
    """Computes the derivative with respect to the Capillary number of *self*."""
    raise NotImplementedError(type(self).__name__)


@singledispatch
def farfield(self, *args, **kwargs):
    """Computes derivatives with respect to the farfield velocity of *self*."""
    raise NotImplementedError(type(self).__name__)
