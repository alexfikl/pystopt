# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

"""
Dataclasses
-----------

.. autofunction:: dc_asdict
.. autofunction:: dc_items
.. autofunction:: dc_stringify

Logging and Testing
-------------------

.. data:: COLORS

.. autofunction:: gc_collect
.. autofunction:: gitrev
.. autofunction:: slugify
.. autofunction:: is_mpl_latex_available
.. autofunction:: pytest_generate_tests_for_array_contexts
"""

import pathlib
from collections.abc import Iterator
from dataclasses import dataclass, is_dataclass
from typing import Any

import numpy as np

from arraycontext.pytest import (
    _PytestPyOpenCLArrayContextFactoryWithClass,
    register_pytest_array_context_factory,
)

from pystopt.logging import afmt, get_default_logger, on_ci  # noqa: F401

logger = get_default_logger(__name__)


# {{{ gc_collect


def gc_collect() -> None:
    """Collect any memory and force the allocator to release it.

    This is a rather forceful approach and should not be used.
    """
    import gc

    gc.collect()

    import ctypes

    libc = ctypes.CDLL("libc.so.6")
    libc.malloc_trim(0)


# }}}


# {{{ slugify


def slugify(stem: str | None, separator: str = "_") -> str | None:
    """
    :returns: an ASCII slug representing *stem*, with all the unicode cleaned up
        and all non-standard separators replaced.
    """
    if not stem:
        return stem

    import re

    try:
        from slugify import slugify as _slugify

        stem = _slugify(stem, separator=separator).lower()
    except ImportError:
        import unicodedata

        stem = unicodedata.normalize("NFKD", stem)
        stem = stem.encode("ascii", "ignore").decode().lower()
        stem = re.sub(r"[^a-z0-9]+", separator, stem)

    stem = re.sub(rf"[{separator}]+", separator, stem.strip(separator))
    return stem


# }}}


# {{{ gitrev


def gitrev() -> str | None:
    """Get the hex string for the current HEAD revision."""

    try:
        import pygit2
    except ImportError:
        return None

    repo = pygit2.Repository(pathlib.Path.cwd())
    return str(repo.head.target)


# }}}


# {{{ checkdep_usetex

# NOTE: this seems to be the path on arch
_LATEX_DIST_PATH = pathlib.Path("/usr/share/texmf-dist/tex/latex")
# NOTE: this seems to be the path on debian
_ALT_LATEX_DIST_PATH = pathlib.Path("/usr/share/texmf/tex/latex")


def to_tuple(version: str) -> tuple[int, ...]:
    return tuple(int(p) for p in version.split("."))


def check_usetex(*, s: bool) -> bool:
    try:
        import matplotlib
    except ImportError:
        return False

    if to_tuple(matplotlib.__version__) < (3, 6):
        return matplotlib.checkdep_usetex(s)

    # NOTE: simplified version from matplotlib
    # https://github.com/matplotlib/matplotlib/blob/ec85e725b4b117d2729c9c4f720f31cf8739211f/lib/matplotlib/__init__.py#L439=L456

    import shutil

    if not shutil.which("tex"):
        logger.warning("usetex mode requires 'tex'")
        return False

    if not shutil.which("dvipng"):
        logger.warning("usetex mode requires 'dvipng'")
        return False

    if not shutil.which("gs"):
        logger.warning("usetex mode requires 'gs'")
        return False

    return True


def is_mpl_latex_available() -> bool:
    try:
        import matplotlib  # noqa: F401
    except ImportError:
        return False

    # FIXME: this does not seem to be a documented way to check for tex support,
    # but it works for our current usecase. it also does not catch the actual
    # issue on porter, which is the missing `cm-super` package
    usetex = check_usetex(s=True)

    # FIXME: better way to get the latex package path?
    if _LATEX_DIST_PATH.exists():
        cm_super_exists = (_LATEX_DIST_PATH / "cm-super").exists()
    elif _ALT_LATEX_DIST_PATH.exists():
        cm_super_exists = (_ALT_LATEX_DIST_PATH / "cm-super").exists()
    else:
        # NOTE: hope that check_usetex did a good enough job
        cm_super_exists = True

    return usetex and cm_super_exists


# }}}


# {{{ random


def randn(
    shape: tuple[int, ...], dtype, rng: np.random.Generator | None = None
) -> np.ndarray:
    if rng is None:
        rng = np.random.default_rng()

    dtype = np.dtype(dtype)
    ashape: tuple[int, ...]

    if shape == 0:
        ashape = (1,)
    else:
        ashape = shape

    if dtype.kind == "c":
        dtype = np.dtype(f"<f{dtype.itemsize // 2}")
        r = rng.standard_normal(ashape, dtype) + 1j * rng.standard_normal(ashape, dtype)
    elif dtype.kind == "f":
        r = rng.standard_normal(ashape, dtype)
    elif dtype.kind == "i":
        r = rng.integers(0, 512, ashape, dtype)
    else:
        raise TypeError(dtype.kind)

    if shape == 0:
        return np.array(r[0])

    return r


# }}}


# {{{ dataclasses


def dc_asdict(dc, *, init_only: bool = True) -> dict[str, Any]:
    """
    :returns: a shallow copy of the fields in the dataclass *dc*.
    """
    if not is_dataclass(dc):
        raise TypeError("input 'dc' is not a dataclass")

    return dict(dc_items(dc, init_only=init_only))


def dc_items(dc, *, init_only: bool = True) -> Iterator[tuple[str, Any]]:
    """
    :param init_only: if *False*, all the fields of the dataclass are returned,
        even those with ``init=False``.
    :returns: tuples of the form ``(field, value)`` with the fields from the
        dataclass.
    """
    if not is_dataclass(dc):
        raise TypeError("input 'dc' is not a dataclass")

    from dataclasses import fields

    for field in fields(dc):
        if not init_only or field.init:
            yield field.name, getattr(dc, field.name)


def dc_stringify(dc) -> str:
    if is_dataclass(dc):
        fields = dc_asdict(dc)
    elif isinstance(dc, dict):
        fields = dc
    else:
        raise TypeError(f"unrecognized type: '{type(dc).__name__}'")

    width = len(max(fields, key=len))
    fmt = f"{{:{width}}} : {{}}"

    def stringify(v):
        sv = repr(v)
        if len(sv) > 128:
            sv = f"{type(v).__name__}<...>"

        return sv

    instance_attrs = sorted(
        {k: stringify(v) for k, v in fields.items() if k != "name"}.items()
    )

    header_attrs = [("class", type(dc).__name__), ("-" * width, "-" * width)]
    if "name" in fields:
        header_attrs.insert(1, ("name", fields["name"]))

    return "\n".join([
        "\t{}".format("\n\t".join(fmt.format(k, v) for k, v in header_attrs)),
        "\t{}".format("\n\t".join(fmt.format(k, v) for k, v in instance_attrs)),
    ])


# }}}


# {{{ colors


class ColorCodes:
    Black: str = ""
    Red: str = ""
    Green: str = ""
    Brown: str = ""
    Blue: str = ""
    Purple: str = ""
    Cyan: str = ""
    LightGray: str = ""
    DarkGray: str = ""
    LightRed: str = ""
    LightGreen: str = ""
    Yellow: str = ""
    LightBlue: str = ""
    LightPurple: str = ""
    LightCyan: str = ""
    White: str = ""
    Normal: str = ""

    @classmethod
    def warn(cls, s: Any) -> str:
        return f"{cls.LightRed}{s}{cls.Normal}"

    @classmethod
    def info(cls, s: Any) -> str:
        return f"{cls.DarkGray}{s}{cls.Normal}"

    @classmethod
    def message(cls, s: Any, *, success: bool = True) -> str:
        return cls.info(s) if success else cls.warn(s)

    @classmethod
    def wrap(cls, s: Any, color: str) -> str:
        return f"{color}{s}{cls.Normal}"


@dataclass(frozen=True)
class ANSIColorCodes(ColorCodes):
    Black: str = "\033[0;30m"
    Red: str = "\033[0;31m"
    Green: str = "\033[0;32m"
    Brown: str = "\033[0;33m"
    Blue: str = "\033[0;34m"
    Purple: str = "\033[0;35m"
    Cyan: str = "\033[0;36m"
    LightGray: str = "\033[0;37m"
    DarkGray: str = "\033[1;30m"
    LightRed: str = "\033[1;31m"
    LightGreen: str = "\033[1;32m"
    Yellow: str = "\033[1;33m"
    LightBlue: str = "\033[1;34m"
    LightPurple: str = "\033[1;35m"
    LightCyan: str = "\033[1;36m"
    White: str = "\033[1;37m"
    Normal: str = "\033[0m"


@dataclass(frozen=True)
class RichColorCodes(ColorCodes):
    Black: str = "[black]"
    Red: str = "[red]"
    Green: str = "[green]"
    Brown: str = "[brown]"
    Blue: str = "[blue]"
    Purple: str = "[purple]"
    Cyan: str = "[cyan]"
    LightGray: str = "[gray]"
    DarkGray: str = "[gray]"
    LightRed: str = "[bold red]"
    LightGreen: str = "[bold green]"
    Yellow: str = "[bold yellow]"
    LightBlue: str = "[bold blue]"
    LightPurple: str = "[bold purple]"
    LightCyan: str = "[bold cyan]"
    White: str = "[white]"
    Normal: str = "[/]"


def _get_color_codes():
    try:
        import rich  # noqa: F401

        has_rich = True
    except ImportError:
        # NOTE: rich is vendored by pip since November 2021
        try:
            import pip._vendor.rich  # noqa: F401

            has_rich = True
        except ImportError:
            has_rich = False

    if has_rich:
        return RichColorCodes()
    else:
        return ANSIColorCodes()


c = _get_color_codes()

# }}}


# {{{ pytest


def get_cl_array_context(context_or_factory=None, *, allocator=True):
    """
    :arg context_or_factory: can be any of: a :class:`pyopencl.Context`,
        :class:`pyopencl.CommandQueue` or even an
        :class:`~arraycontext.ArrayContext` already (in
        which case it is just returned). Can also be a callable that
        takes no arguments and returns one of the above. If a context or a
        queue is passed in, an appropriate :class:`~arraycontext.PyOpenCLArrayContext`
        is constructed.

    :arg allocator: A class satisfying the interface of
        :class:`~pyopencl.tools.AllocatorInterface`. It can also be a boolean
        flag, which when *True* constructs a default allocator.

    :returns: a :class:`~arraycontext.ArrayContext`.
    """

    import pyopencl as cl

    if context_or_factory is None:
        context_or_factory = cl.create_some_context

    from arraycontext import ArrayContext

    if isinstance(context_or_factory, ArrayContext):
        return context_or_factory

    if isinstance(context_or_factory, cl.Context):
        ctx = context_or_factory
        queue = cl.CommandQueue(ctx)
    elif isinstance(context_or_factory, cl.CommandQueue):
        queue = context_or_factory
        ctx = queue.context
    elif callable(context_or_factory):
        return get_cl_array_context(context_or_factory(), allocator=allocator)
    else:
        raise TypeError(f"unsupported type: {type(context_or_factory).__name__}")

    if isinstance(allocator, bool):
        if allocator:
            import pyopencl.tools

            allocator = cl.tools.MemoryPool(cl.tools.ImmediateAllocator(queue))
        else:
            allocator = None

    from pystopt.dof_array import PyOpenCLArrayContext

    return PyOpenCLArrayContext(queue, allocator=allocator, force_device_scalars=True)


class _PytestPyOpenCLArrayContextFactory(_PytestPyOpenCLArrayContextFactoryWithClass):
    force_device_scalars = True

    @property
    def actx_class(self):
        from pystopt.dof_array import PyOpenCLArrayContext

        return PyOpenCLArrayContext


register_pytest_array_context_factory(
    "pystopt.pyopencl", _PytestPyOpenCLArrayContextFactory
)


def pytest_generate_tests_for_array_contexts(
    factories, factory_arg_name="actx_factory"
):
    from arraycontext import pytest as _pytest

    func = _pytest.pytest_generate_tests_for_array_contexts(
        factories=factories, factory_arg_name=factory_arg_name
    )

    def wrapper(metafunc):
        func(metafunc)

        if "visualize" in metafunc.fixturenames:
            is_verbose = metafunc.config.getoption("verbose") >= 1

            visualize = is_verbose and not on_ci()
            metafunc.parametrize("visualize", (visualize,))

    return wrapper


# }}}
