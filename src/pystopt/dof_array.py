# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

r"""
Functions taking :class:`~meshmode.dof_array.DOFArray` that extend the
functionality present in :mod:`meshmode`.

.. class:: DOFArrayT
.. autoclass:: SpectralDOFArray

.. autofunction:: uniform_like
.. autofunction:: dof_array_norm
.. autofunction:: dof_array_rnorm

.. class:: Array

    See :class:`arraycontext.Array`.

.. class:: ArrayContainer

    See :class:`arraycontext.ArrayContainer`
"""

from typing import Any, TypeVar

import numpy as np

from arraycontext import (  # noqa: F401
    ArrayContainer,
    ArrayContext,
    ArrayOrContainer,
    ArrayOrContainerT,
    with_container_arithmetic,
)
from arraycontext.impl.pyopencl.fake_numpy import (
    PyOpenCLFakeNumpyNamespace as _PyOpenCLFakeNumpyNamespaceBase,
)
from arraycontext.impl.pyopencl.fake_numpy import (
    _PyOpenCLFakeNumpyLinalgNamespace as _PyOpenCLFakeNumpyLinalgNamespaceBase,
)
from meshmode.array_context import PyOpenCLArrayContext as PyOpenCLArrayContextBase
from meshmode.dof_array import DOFArray

from pystopt.tools import get_default_logger

logger = get_default_logger(__name__)


# {{{ PyOpenCLArrayContext extensions


class _PyOpenCLFakeNumpyLinalgNamespace(_PyOpenCLFakeNumpyLinalgNamespaceBase):
    pass


class _PyOpenCLFakeNumpyNamespace(_PyOpenCLFakeNumpyNamespaceBase):
    def _get_fake_numpy_linalg_namespace(self):
        return _PyOpenCLFakeNumpyLinalgNamespace(self._array_context)

    def vdot(self, x, y, dtype=None):
        import pyopencl.array as cla

        def vdot(subx, suby):
            if np.isscalar(subx) and np.isscalar(suby):
                return np.vdot(subx, suby)

            return cla.vdot(subx, suby, dtype=dtype, queue=self._array_context.queue)

        from arraycontext import rec_multimap_reduce_array_container

        return rec_multimap_reduce_array_container(sum, vdot, x, y)


class PyOpenCLArrayContext(PyOpenCLArrayContextBase):
    def _get_fake_numpy_namespace(self):
        return _PyOpenCLFakeNumpyNamespace(self)

    def to_numpy(self, array):
        if np.isscalar(array):
            return array

        array = super().to_numpy(array)
        if array.shape == ():
            array = array[()]

        return array

    def transfer_tags(self, from_ary, to_ary):
        if np.isscalar(from_ary):
            assert np.isscalar(to_ary)
            return to_ary

        assert isinstance(from_ary, self.array_types)
        assert isinstance(to_ary, self.array_types)

        import arraycontext.impl.pyopencl.taggable_cl_array as tga

        if not isinstance(to_ary, tga.TaggableCLArray):
            to_ary = tga.to_tagged_cl_array(to_ary)

        if not isinstance(from_ary, tga.TaggableCLArray):
            return to_ary

        to_ary = to_ary.tagged(from_ary.tags)
        for iaxis, ax in enumerate(from_ary.axes):
            to_ary = to_ary.with_tagged_axis(iaxis, ax.tags)

        return to_ary


# }}}


# {{{ spectral DOFArray


@with_container_arithmetic(
    bcast_obj_array=True,
    bcast_numpy_array=True,
    rel_comparison=True,
    _cls_has_array_context_attr=True,
)
class SpectralDOFArray(DOFArray):
    """A :class:`~meshmode.dof_array.DOFArray` that contains spectral coefficients."""

    def __str__(self):
        return f"SpectralDOFArray({self._data})"

    def __repr__(self):
        return f"SpectralDOFArray({self._data!r})"


DOFArrayT = TypeVar("DOFArrayT", DOFArray, SpectralDOFArray)

# }}}


# {{{ DOFArray extensions


def get_container_context_recursively(
    ary: ArrayOrContainer,
) -> ArrayContext | None:
    if isinstance(ary, np.ndarray) and ary.dtype.char != "O":
        return None

    import arraycontext

    return arraycontext.get_container_context_recursively(ary)


def uniform_like(
    actx: ArrayContext,
    ary: ArrayOrContainerT,
    *,
    a: float = 0.0,
    b: float = 1.0,
    seed: int | None = None,
    rng: np.random.Generator | None = None,
) -> ArrayOrContainerT:
    if rng is None:
        rng = np.random.default_rng(seed=seed)

    def _uniform_like(subary):
        if subary.dtype.kind == "c":
            dtype = subary.real.dtype
            result = a + (b - a) * rng.random(size=(2, *subary.shape), dtype=dtype)
            result = result[0] + 1j * result[1]
        elif subary.dtype.kind == "f":
            result = a + (b - a) * rng.random(size=subary.shape, dtype=subary.dtype)
        else:
            raise ValueError(f"unsupported dtype: {subary.dtype}")

        return actx.transfer_tags(subary, actx.from_numpy(result))

    from arraycontext import rec_map_array_container

    return rec_map_array_container(_uniform_like, ary)


def dof_array_norm(
    ary: ArrayOrContainer,
    ord: float | None = None,  # noqa: A002
) -> Any:
    """
    :arg ary: a :class:`~meshmode.dof_array.DOFArray` or an object array of such.
    """
    from numbers import Number

    if isinstance(ary, Number):
        return abs(ary)

    if isinstance(ary, np.ndarray) and ary.dtype.char != "O":
        return np.linalg.norm(ary)

    from meshmode.dof_array import flat_norm

    return flat_norm(ary, ord=ord)


def dof_array_rnorm(
    x: ArrayOrContainerT,
    y: ArrayOrContainerT,
    ord: float | None = None,  # noqa: A002
) -> Any:
    """Relative norm of the given order."""

    y_norm = dof_array_norm(y, ord=ord)
    if y_norm < 1.0e-14:
        y_norm = 1.0

    return dof_array_norm(x - y, ord=ord) / y_norm


# }}}
