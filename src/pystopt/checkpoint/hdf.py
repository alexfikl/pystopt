# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

import pathlib
import threading
from collections.abc import Iterator
from contextlib import contextmanager
from typing import Any

import numpy as np
from h5pyckle import (  # noqa: F401
    dump,
    dump_to_group,
    dumper,
    load,
    load_from_group,
    loader,
)
from h5pyckle.base import PickleGroup, load_from_type
from h5pyckle.interop_meshmode import (  # noqa: F401
    array_context_for_pickling,
    get_array_context,
)

from pystopt.callbacks import (
    CallbackManager,
    CheckpointCallback,
    HistoryCallback,
    LogCallback,
    VisualizeCallback,
)
from pystopt.checkpoint import H5Checkpoint, IteratorCheckpointManager
from pystopt.mesh import (
    FourierConnectionElementGroup,
    SpectralDiscretization,
    SpectralGridConnection,
    SphericalHarmonicConnectionElementGroup,
    UniqueConnectionBatch,
    UniqueConnectionElementGroup,
    UniqueConnectionInterpolationBatch,
    UniqueDOFConnectionBase,
)
from pystopt.optimize import CGResult
from pystopt.simulation.common import StokesGeometryState
from pystopt.simulation.quasistokes import (
    AdjointStokesCostEvolutionState,
    StokesCostEvolutionState,
    StokesUnsteadyOptimizationState,
)
from pystopt.simulation.run import RunResult
from pystopt.simulation.state import GeometryState
from pystopt.simulation.staticstokes import StokesShapeOptimizationState
from pystopt.simulation.unsteady import (
    AdjointStokesEvolutionState,
    StokesEvolutionState,
)


def from_dataset(
    group: PickleGroup, name: str, alt: str | None = None
) -> np.ndarray | None:
    from h5pyckle.interop_meshmode import from_numpy

    if name in group:
        ds = from_numpy(group[name][:])
    elif alt in group:
        ds = from_numpy(group[alt][:])
    else:
        ds = None

    return ds


# {{{ wrangler for pickling

_WRANGLER_FOR_PICKLING_TLS = threading.local()


@contextmanager
def wrangler_for_pickling(wrangler: Any) -> Iterator[None]:
    try:
        existing_pickle_wrangler = _WRANGLER_FOR_PICKLING_TLS.wrangler
    except AttributeError:
        existing_pickle_wrangler = None

    if existing_pickle_wrangler is not None:
        raise RuntimeError("'wrangler_for_pickling' can not be nested.")

    _WRANGLER_FOR_PICKLING_TLS.wrangler = wrangler
    try:
        yield
    finally:
        _WRANGLER_FOR_PICKLING_TLS.wrangler = None


def get_wrangler(cls: type) -> Any:
    try:
        wrangler = _WRANGLER_FOR_PICKLING_TLS.wrangler
    except AttributeError:
        wrangler = None

    if wrangler is None:
        raise ValueError("cannot unpickle without a wrangler")

    if not isinstance(wrangler, cls):
        raise TypeError(
            f"expected a wrangler of type '{cls.__name__}' but got "
            f"a '{type(wrangler).__name__}'"
        )

    return wrangler


# }}}


# {{{ unique dof connection


@dumper.register(UniqueConnectionBatch)
def _dump_unique_connection_batch(
    obj: UniqueConnectionBatch, parent: PickleGroup, *, name: str | None = None
):
    group = parent.create_type(name, obj)

    from h5pyckle.interop_meshmode import to_numpy

    group.attrs["from_group_index"] = obj.from_group_index
    group.create_dataset("node_indices", data=to_numpy(obj.node_indices))


@loader.register(UniqueConnectionBatch)
def _load_unique_connection_batch(parent: PickleGroup) -> UniqueConnectionBatch:
    from_group_index = parent.attrs["from_group_index"]

    from h5pyckle.interop_meshmode import from_numpy

    return parent.pycls(
        from_group_index=int(from_group_index),
        node_indices=from_numpy(parent["node_indices"][:]),
    )


@dumper.register(UniqueConnectionInterpolationBatch)
def _dump_unique_connection_interpolation_batch(
    obj: UniqueConnectionInterpolationBatch,
    parent: PickleGroup,
    *,
    name: str | None = None,
):
    group = parent.create_type(name, obj)

    from h5pyckle.interop_meshmode import to_numpy

    group.attrs["from_group_index"] = obj.from_group_index
    group.create_dataset("node_indices", data=to_numpy(obj.node_indices))

    group.create_dataset("to_resampling_mat", data=to_numpy(obj.to_resampling_mat))
    group.create_dataset("from_resampling_mat", data=to_numpy(obj.from_resampling_mat))


@loader.register(UniqueConnectionInterpolationBatch)
def _load_unique_connection_interpolation_batch(
    parent: PickleGroup,
) -> UniqueConnectionInterpolationBatch:
    from_group_index = parent.attrs["from_group_index"]

    from h5pyckle.interop_meshmode import from_numpy

    return parent.pycls(
        from_group_index=int(from_group_index),
        node_indices=from_numpy(parent["node_indices"][:]),
        to_resampling_mat=from_dataset(
            parent, "to_resampling_mat", alt="push_resampling_mat"
        ),
        from_resampling_mat=from_dataset(
            parent, "from_resampling_mat", alt="pull_resampling_mat"
        ),
    )


@dumper.register(UniqueConnectionElementGroup)
def _dump_unique_connection_element_group(
    obj: UniqueConnectionElementGroup,
    parent: PickleGroup,
    *,
    name: str | None = None,
):
    group = parent.create_type(name, obj)

    group.attrs["noutputs"] = obj.noutputs
    dumper(obj.batches, group, name="batches")


@loader.register(UniqueConnectionElementGroup)
def _load_unique_connection_element_group(
    parent: PickleGroup,
) -> UniqueConnectionElementGroup:
    return parent.pycls(
        noutputs=int(parent.attrs["noutputs"]),
        batches=load_from_type(parent["batches"]),
    )


@dumper.register(UniqueDOFConnectionBase)
def _dump_unique_dof_connection(
    obj: UniqueDOFConnectionBase, parent: PickleGroup, *, name: str | None = None
):
    group = parent.create_type(name, obj)

    dumper(obj.from_discr, group, name="from_discr")
    dumper(obj.groups, group, name="groups")
    dumper(obj.weights, group, name="weights")


@loader.register(UniqueDOFConnectionBase)
def _load_unique_dof_connection(parent: PickleGroup) -> UniqueDOFConnectionBase:
    from_discr = load_from_type(parent["from_discr"])
    groups = load_from_type(parent["groups"])
    weights = load_from_type(parent["weights"])

    return parent.pycls(from_discr=from_discr, groups=groups, weights=weights)


# }}}


# {{{ fourier


@dumper.register(FourierConnectionElementGroup)
def _dump_fourier_connection_element_group(
    obj: FourierConnectionElementGroup,
    parent: PickleGroup,
    *,
    name: str | None = None,
):
    group = parent.create_type(name, obj)

    group.attrs["noutputs"] = obj.noutputs
    group.attrs["is_real"] = obj.is_real
    dumper(obj.batches, group, name="batches")


@loader.register(FourierConnectionElementGroup)
def _load_fourier_connection_element_group(
    parent: PickleGroup,
) -> FourierConnectionElementGroup:
    return parent.pycls(
        noutputs=int(parent.attrs["noutputs"]),
        batches=load_from_type(parent["batches"]),
        is_real=bool(parent.attrs["is_real"]),
    )


# }}}


# {{{ spectral


@dumper.register(SpectralGridConnection)
def _dump_spectral_grid_connection(
    obj: SpectralGridConnection, parent: PickleGroup, *, name: str | None = None
):
    group = parent.create_type(name, obj)
    dumper(obj.from_discr, group, name="from_discr")
    dumper(obj.groups, group, name="groups")


@loader.register(SpectralGridConnection)
def _load_spectral_grid_connection(parent: PickleGroup) -> SpectralGridConnection:
    from_discr = load_from_type(parent["from_discr"])
    groups = load_from_type(parent["groups"])

    return parent.pycls(from_discr, groups)


@dumper.register(SpectralDiscretization)
def _dump_spectral_discretization(
    obj: SpectralDiscretization, parent: PickleGroup, *, name: str | None = None
):
    group = parent.create_type(name, obj)

    group.attrs["use_spectral_derivative"] = obj.use_spectral_derivative
    group.attrs["real_dtype"] = np.void(obj.real_dtype.str.encode())

    dumper(obj.mesh, group, name="mesh")
    dumper(obj.specgroups, group, name="specgroups")
    dumper(obj.groups, group, name="groups")

    if obj.xlm is not None:
        dumper(obj.xlm, group, name="xlm")


@loader.register(SpectralDiscretization)
def _load_spectral_discretization(parent: PickleGroup) -> SpectralDiscretization:
    mesh = load_from_type(parent["mesh"])
    specgroups = load_from_type(parent["specgroups"])
    groups = load_from_type(parent["groups"])
    if "xlm" in parent:
        xlm = load_from_type(parent["xlm"])
    else:
        xlm = None

    from pystopt.mesh.poly_element import GivenGroupsGroupFactory

    actx = get_array_context()
    return parent.pycls(
        actx,
        mesh,
        specgroups,
        group_factory=GivenGroupsGroupFactory(groups),
        real_dtype=np.dtype(parent.attrs["real_dtype"].tobytes()),
        use_spectral_derivative=bool(parent.attrs["use_spectral_derivative"]),
        _xlm=xlm,
    )


# }}}


# {{{ spharm

try:
    import shtns

    _H5PYCKLE_REGISTER_SHTNS = True
except ImportError:
    _H5PYCKLE_REGISTER_SHTNS = False


if _H5PYCKLE_REGISTER_SHTNS:

    @dumper.register(shtns.sht)
    def _dump_shtns(obj: shtns.sht, parent: PickleGroup, *, name: str | None = None):
        group = parent.create_type(name, obj)

        group.attrs["lmax"] = obj.lmax
        group.attrs["mmax"] = obj.mmax
        group.attrs["ntheta"] = obj.nlat
        group.attrs["nphi"] = obj.nphi

    @loader.register(shtns.sht)
    def _load_shtns(parent: PickleGroup) -> shtns.sht:
        from pystopt.mesh.spharm import _make_shtns

        return _make_shtns(
            lmax=int(parent.attrs["lmax"]),
            mmax=int(parent.attrs["mmax"]),
            ntheta=int(parent.attrs["ntheta"]),
            nphi=int(parent.attrs["nphi"]),
        )

    @dumper.register(SphericalHarmonicConnectionElementGroup)
    def _dump_spherical_harmonic_connection_element_group(
        obj: SphericalHarmonicConnectionElementGroup,
        parent: PickleGroup,
        *,
        name: str | None = None,
    ):
        group = parent.create_type(name, obj)

        group.attrs["neltheta"] = obj.neltheta
        group.attrs["nelphi"] = obj.nelphi
        dumper(obj.batches, group, name="batches")
        dumper(obj.sh, group, name="sh")

    @loader.register(SphericalHarmonicConnectionElementGroup)
    def _load_spherical_harmonic_connection_element_group(
        parent: PickleGroup,
    ) -> SphericalHarmonicConnectionElementGroup:
        sh = load_from_type(parent["sh"])
        batches = load_from_type(parent["batches"])

        return parent.pycls(
            sh=sh,
            batches=batches,
            neltheta=int(parent.attrs["neltheta"]),
            nelphi=int(parent.attrs["nelphi"]),
        )


# }}}


# {{{ cg result


@dumper.register(CGResult)
def _dump_cg_result(obj: CGResult, parent: PickleGroup, name: str | None = None):
    group = parent.create_type(name, obj)

    from pystopt.tools import dc_asdict

    dumper(dc_asdict(obj), group, name="fields")


@loader.register(CGResult)
def _load_cg_result(parent: PickleGroup) -> CGResult:
    fields = load_from_type(parent["fields"])
    return parent.pycls(**fields)


# }}}


# {{{ checkpoints


@dumper.register(H5Checkpoint)
def _dump_h5_checkpoint(
    obj: H5Checkpoint, parent: PickleGroup, name: str | None = None
):
    group = parent.create_type(name, obj)

    group.attrs["filename"] = str(obj.filename)
    group.attrs["name"] = obj.h5root.name
    dumper(obj.h5root.h5_dset_options, group, name="h5_dset_options")


@loader.register(H5Checkpoint)
def _load_h5_checkpoint(parent: PickleGroup) -> H5Checkpoint:
    import h5py

    h5 = h5py.File(parent.attrs["filename"], "a")
    h5 = h5.require_group(str(parent.attrs["name"]))

    root = PickleGroup(
        h5.id,
        h5_dset_options=load_from_type(parent["h5_dset_options"]),
    )

    return parent.pycls(h5root=root, owned=True)


@dumper.register(IteratorCheckpointManager)
def _dump_iterator_checkpoint_manager(
    obj: IteratorCheckpointManager, parent: PickleGroup, name: str | None = None
):
    group = parent.create_type(name, obj)
    group.attrs["pattern"] = obj.pattern
    group.attrs["ncalls"] = obj._ncalls
    dumper(obj.rw, group, name="rw")


@loader.register(IteratorCheckpointManager)
def _load_iterator_checkpoint_manager(
    parent: PickleGroup,
) -> IteratorCheckpointManager:
    cm = parent.pycls(
        rw=load_from_type(parent["rw"]), pattern=str(parent.attrs["pattern"])
    )
    cm.advance(int(parent.attrs["ncalls"]))

    return cm


# }}}


# {{{ callbacks


@dumper.register(CallbackManager)
def _dump_callback_manager(
    obj: CallbackManager, parent: PickleGroup, name: str | None = None
):
    group = parent.create_type(name, obj)

    group.attrs["stop_file_name"] = str(obj.stop_file_name)
    group.attrs["short_circuit"] = obj.short_circuit
    dumper(obj.callbacks, group, name="callbacks")

    # private-ish attributes
    group.attrs["ncalls"] = obj._ncalls
    group.attrs["force_gc"] = obj._force_gc


@loader.register(CallbackManager)
def _load_callback_manager(parent: PickleGroup) -> CallbackManager:
    from h5pyckle import load_from_attribute

    force_gc = load_from_attribute("force_gc", parent)

    callbacks = load_from_type(parent["callbacks"])
    cm = parent.pycls(
        stop_file_name=pathlib.Path(parent.attrs["stop_file_name"]),
        short_circuit=bool(parent.attrs["short_circuit"]),
        callbacks=callbacks,
        _force_gc=True if force_gc is None else bool(force_gc),
    )
    cm.restart_from_iteration(parent.attrs["ncalls"] - 1)

    return cm


@dumper.register(LogCallback)
def _dump_log_callback(obj: LogCallback, parent: PickleGroup, name: str | None = None):
    # NOTE: this has a `log` and `norm` callable, which we do not pickle
    parent.create_type(name, obj)


@loader.register(LogCallback)
def _load_log_callback(parent: PickleGroup) -> LogCallback:
    return parent.pycls(norm=None, log=None)


@dumper.register(HistoryCallback)
def _dump_history_callback(
    obj: HistoryCallback, parent: PickleGroup, name: str | None = None
):
    group = parent.create_type(name, obj)
    dumper(obj.to_numpy(), group, name="history")


@loader.register(HistoryCallback)
def _load_history_callback(parent: PickleGroup) -> HistoryCallback:
    history = load_from_type(parent["history"])
    cb = parent.pycls(norm=None)
    cb.from_numpy(history)

    return cb


@dumper.register(CheckpointCallback)
def _dump_checkpoint_callback(
    obj: CheckpointCallback, parent: PickleGroup, name: str | None = None
):
    group = parent.create_type(name, obj)
    group.attrs["overwrite"] = obj.overwrite
    dumper(obj.checkpoint, group, name="checkpoint")


@loader.register(CheckpointCallback)
def _load_checkpoint_callback(parent: PickleGroup) -> CheckpointCallback:
    return parent.pycls(
        norm=None,
        checkpoint=load_from_type(parent["checkpoint"]),
        overwrite=bool(parent.attrs["overwrite"]),
    )


@dumper.register(VisualizeCallback)
def _dump_visualize_callback(
    obj: VisualizeCallback, parent: PickleGroup, name: str | None = None
):
    group = parent.create_type(name, obj)
    group.attrs["overwrite"] = obj.overwrite
    group.attrs["frequency"] = obj.frequency
    group.attrs["nwrites"] = obj._nwrites

    fp = obj._previous_filename
    if fp is None:
        # FIXME: this advances the iterator and can mess up things, but for now
        # we just hope this was called at the end of the run, so that it doesn't
        # matter :\
        fp = next(obj.visualize_file_series)

    from h5pyckle import dump_to_attribute

    dump_to_attribute(fp.stem, group, name="stem")
    dump_to_attribute(fp.suffix, group, name="suffix")
    dump_to_attribute(fp.ext, group, name="ext")
    dump_to_attribute(str(fp.cwd), group, name="cwd")


@loader.register(VisualizeCallback)
def _load_visualize_callback(parent: PickleGroup) -> VisualizeCallback:
    from h5pyckle import load_from_attribute

    from pystopt.paths import generate_filename_series

    cwd = load_from_attribute("cwd", parent)
    if cwd is not None:
        cwd = pathlib.Path(cwd)

    nwrites = parent.attrs["nwrites"]
    visualize_file_series = generate_filename_series(
        stem=load_from_attribute("stem", parent),
        suffix=load_from_attribute("suffix", parent),
        ext=load_from_attribute("ext", parent),
        cwd=cwd,
        start=nwrites,
    )

    cb = parent.pycls(
        norm=None,
        visualize_file_series=visualize_file_series,
        overwrite=bool(parent.attrs["overwrite"]),
        frequency=int(parent.attrs["frequency"]),
    )
    object.__setattr__(cb, "_nwrites", nwrites)

    return cb


# }}}


# {{{ states

# FIXME: should this force evaluation of the fields that are not cached?


def _dump_state_fields(obj, group, fields):
    for f in fields:
        dumper(getattr(obj, f), group, name=f)


def _load_state_fields(group, wrangler_cls, *, fields, **kwargs):
    obj = group.pycls(
        x=load_from_type(group["x"]), wrangler=get_wrangler(wrangler_cls), **kwargs
    )

    for f in fields:
        object.__setattr__(obj, f, load_from_type(group[f]))

    return obj


# {{{ GeometryState


@dumper.register(GeometryState)
def _dump_geometry_state(
    obj: GeometryState, parent: PickleGroup, name: str | None = None
):
    group = parent.create_type(name, obj)

    dumper(obj.x, group, name="x")
    _dump_state_fields(obj, group, ("discr",))


@loader.register(GeometryState)
def _load_geometry_state(parent: PickleGroup) -> GeometryState:
    from pystopt.simulation.state import GeometryWrangler

    return _load_state_fields(parent, GeometryWrangler, fields=("discr",))


# }}}


# {{{ StokesGeometryState


@dumper.register(StokesGeometryState)
def _dump_stokes_geometry_state(
    obj: StokesGeometryState, parent: PickleGroup, name: str | None = None
):
    group = parent.create_type(name, obj)

    dumper(obj.x, group, name="x")
    _dump_state_fields(obj, group, ("discr", "density", "velocity"))


@loader.register(StokesGeometryState)
def _load_stokes_geometry_state(parent: PickleGroup) -> StokesGeometryState:
    from pystopt.simulation.common import StokesGeometryWrangler

    return _load_state_fields(
        parent, StokesGeometryWrangler, fields=("discr", "density", "velocity")
    )


# }}}


# {{{ StokesShapeOptimizationState


@dumper.register(StokesShapeOptimizationState)
def _dump_stokes_shape_optimization_state(
    obj: StokesShapeOptimizationState,
    parent: PickleGroup,
    name: str | None = None,
):
    group = parent.create_type(name, obj)

    dumper(obj.x, group, name="x")
    _dump_state_fields(
        obj, group, ("discr", "density", "velocity", "density_star", "velocity_star")
    )


@loader.register(StokesShapeOptimizationState)
def _load_stokes_shape_optimization_state(
    parent: PickleGroup,
) -> StokesShapeOptimizationState:
    from pystopt.simulation.staticstokes import StokesShapeOptimizationWrangler

    return _load_state_fields(
        parent,
        StokesShapeOptimizationWrangler,
        fields=("discr", "density", "velocity", "density_star", "velocity_star"),
    )


# }}}


# {{{ StokesEvolutionState


@dumper.register(StokesEvolutionState)
def _dump_stokes_evolution_state(
    obj: StokesEvolutionState, parent: PickleGroup, name: str | None = None
):
    group = parent.create_type(name, obj)

    group.attrs["t"] = obj.t[0]
    dumper(obj.x, group, name="x")
    _dump_state_fields(obj, group, ("discr", "density", "velocity", "forcing"))


@loader.register(StokesEvolutionState)
def _load_stokes_evolution_state(parent: PickleGroup) -> StokesEvolutionState:
    from pystopt.simulation.unsteady import StokesEvolutionWrangler

    return _load_state_fields(
        parent,
        StokesEvolutionWrangler,
        fields=("discr", "density", "velocity", "forcing"),
        t=np.array([parent.attrs["t"]], dtype=object),
    )


@dumper.register(AdjointStokesEvolutionState)
def _dump_adjoint_stokes_evolution_state(
    obj: AdjointStokesEvolutionState, parent: PickleGroup, name: str | None = None
):
    group = parent.create_type(name, obj)

    group.attrs["t"] = obj.t[0]
    dumper(obj.x, group, name="x")
    _dump_state_fields(obj, group, ("density_star", "shape_gradient"))


@loader.register(StokesEvolutionState)
def _load_adjoint_stokes_evolution_state(
    parent: PickleGroup,
) -> AdjointStokesEvolutionState:
    from pystopt.simulation.unsteady import AdjointStokesEvolutionWrangler

    return _load_state_fields(
        parent,
        AdjointStokesEvolutionWrangler,
        fields=("density_star", "shape_gradient"),
        t=np.array([parent.attrs["t"]], dtype=object),
    )


# }}}


# {{{ StokesCostEvolutionState


@dumper.register(StokesCostEvolutionState)
def _dump_stokes_cost_evolution_state(
    obj: StokesCostEvolutionState, parent: PickleGroup, name: str | None = None
):
    group = parent.create_type(name, obj)

    group.attrs["t"] = obj.t[0]
    group.attrs["j"] = obj.j[0]
    dumper(obj.x, group, name="x")
    _dump_state_fields(obj, group, ("discr", "density", "velocity", "forcing"))


@loader.register(StokesCostEvolutionState)
def _load_stokes_cost_evolution_state(
    parent: PickleGroup,
) -> StokesCostEvolutionState:
    from pystopt.simulation.quasistokes import StokesCostEvolutionWrangler

    return _load_state_fields(
        parent,
        StokesCostEvolutionWrangler,
        fields=("discr", "density", "velocity", "forcing"),
        t=np.array([parent.attrs["t"]], dtype=object),
        j=np.array([parent.attrs["j"]], dtype=object),
    )


@dumper.register(AdjointStokesCostEvolutionState)
def _dump_adjoint_stokes_cost_evolution_state(
    obj: AdjointStokesCostEvolutionState,
    parent: PickleGroup,
    name: str | None = None,
):
    group = parent.create_type(name, obj)

    group.attrs["t"] = obj.t[0]
    dumper(obj.x, group, name="x")
    dumper(obj.dj, group, name="dj")
    _dump_state_fields(obj, group, ("density_star", "shape_gradient"))


@loader.register(AdjointStokesEvolutionState)
def _load_adjoint_stokes_cost_evolution_state(
    parent: PickleGroup,
) -> AdjointStokesCostEvolutionState:
    from pystopt.simulation.quasistokes import AdjointStokesCostEvolutionWrangler

    return _load_state_fields(
        parent,
        AdjointStokesCostEvolutionWrangler,
        fields=("density_star", "shape_gradient"),
        t=np.array([parent.attrs["t"]], dtype=object),
        dj=load_from_type(parent["dj"]),
    )


# }}}


# {{{ StokesUnsteadyOptimizationState


@dumper.register(StokesUnsteadyOptimizationState)
def _dump_stokes_unsteady_optimization_state(
    obj: StokesUnsteadyOptimizationState,
    parent: PickleGroup,
    name: str | None = None,
):
    group = parent.create_type(name, obj)
    dumper(obj.x, group, name="x")


@loader.register(StokesUnsteadyOptimizationState)
def _load_stokes_unsteady_optimization_state(
    parent: PickleGroup,
) -> StokesUnsteadyOptimizationState:
    from pystopt.simulation.quasistokes import StokesUnsteadyOptimizationWrangler

    return parent.pycls(
        x=load_from_type(parent["x"]),
        wrangler=get_wrangler(StokesUnsteadyOptimizationWrangler),
        array_context=get_array_context(),
    )


# }}}

# }}}


# {{{ RunResult

# FIXME: This is just a standard dataclass, so it should just work


@dumper.register(RunResult)
def _dump_run_result(obj: RunResult, parent: PickleGroup, name: str | None = None):
    group = parent.create_type(name, obj)

    from pystopt.tools import dc_asdict

    dumper(dc_asdict(obj), group, name="fields")


@loader.register(RunResult)
def _load_run_result(parent: PickleGroup) -> RunResult:
    return parent.pycls(**load_from_type(parent["fields"]))


# }}}
