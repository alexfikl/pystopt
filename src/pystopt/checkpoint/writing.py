# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

"""
.. currentmodule:: pystopt.checkpoint

.. autoclass:: Checkpoint
    :no-show-inheritance:
    :members:
.. autoclass:: OnDiskCheckpoint
    :members:

.. autoclass:: CheckpointManager
    :no-show-inheritance:
    :members:
.. autoclass:: CheckpointManagerWithKeys
    :members:

.. autoclass:: IteratorCheckpointManager
    :members:
.. autoclass:: OnDiskIteratorCheckpointManager

.. autoclass:: InMemoryCheckpoint
.. autofunction:: make_memory_checkpoint_manager

.. autoclass:: H5Checkpoint
.. autofunction:: make_hdf_checkpoint
.. autofunction:: make_hdf_checkpoint_manager
"""

import pathlib
from collections.abc import Hashable
from dataclasses import dataclass, field
from typing import Any

from pystopt.tools import get_default_logger

logger = get_default_logger(__name__)


# {{{ checkpoint


class CheckpointExistsError(RuntimeError):
    pass


class Checkpoint:
    def __contains__(self, key: Hashable) -> bool:
        """Check if a key has been added to the checkpoint.

        Not all the implementations are capable of this.
        """
        raise NotImplementedError

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, exc_traceback):
        self.close()

    def close(self):
        """Close or otherwise finalize the checkpoint class.

        After this method is called, the checkpoint should not be able to read
        or write. By default, it does nothing.
        """

    def write(self, key: Hashable, obj: Any, *, overwrite: bool = False) -> None:
        """
        :arg key: a key that is interpretable by the writer. For
            example, this can be a suffix for a filename, if each object is
            written in a different file, or a key in a dictionary if the
            checkpoint is kept in memory.
        :arg overwrite: if *True*, existing data with the same *key* should
            be overwritten.
        """
        raise NotImplementedError

    def read(self, key: Hashable) -> Any:
        """
        :arg key: a string key interpretable by the reader, which should match
            the choice in :meth:`write`.
        """
        raise NotImplementedError


class OnDiskCheckpoint(Checkpoint):
    @property
    def filename(self) -> pathlib.Path:
        """File name in which the current checkpoint is stored."""
        raise NotImplementedError

    @property
    def root(self) -> pathlib.Path:
        """Root directory for all the checkpoints."""
        return self.filename.parent


# }}}


# {{{ manager


@dataclass(frozen=True)
class CheckpointManager:
    """A generic manager for writing and reading checkpoint-like data.

    Specific implementations should be used as a context manager, i.e.,

    .. code-block:: python

        with MyCheckpointManager(rw) as chk:
            chk.write_to("checkpoint0", {"data"})

            # do some things

            chk.write("checkpoint0", {"more_data"})

    where read and writes should not be used inside the same context.
    """

    rw: Checkpoint
    """A :class:`Checkpoint` instance used to :meth:`read_from` and :meth:`write_to`."""

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, exc_traceback):
        self.done()

    def __contains__(self, key):
        return key in self.rw

    def done(self):
        """Method to call to finalize the checkpoint writing."""
        self.rw.close()

    def write_to(self, key: Hashable, obj: Any, *, overwrite: bool = False) -> None:
        """Write an object to the checkpoint (see :meth:`Checkpoint.write`)."""
        self.rw.write(key, obj, overwrite=overwrite)

    def read_from(self, key: Hashable) -> Any | None:
        """Read an object from the checkpoint (see :meth:`Checkpoint.read`)."""
        result = None
        if key in self.rw:
            result = self.rw.read(key)

        return result


@dataclass(frozen=True)
class CheckpointManagerWithKeys(CheckpointManager):
    """A version of :class:`CheckpointManager` that generates its own unique keys."""

    @property
    def prev_key(self) -> Hashable:
        """The previously used key during a read or write."""
        raise NotImplementedError

    @property
    def next_key(self) -> Hashable:
        """The key that would be used in the next call to :meth:`read`
        or :meth:`write`."""
        raise NotImplementedError

    def skip(self) -> bool:
        """Skips the current key.

        :returns: *True* if the skipped key is actually in the checkpoint,
            *False* otherwise.
        """
        raise NotImplementedError

    def write(self, obj: Any, *, overwrite: bool = False) -> None:
        """Write the given data to the next unique key.

        :arg obj: an object that can be written using :attr:`CheckpointManager.rw`.
        :arg overwrite: passed on to :meth:`Checkpoint.write`.
        """
        raise NotImplementedError

    def read(self) -> Any | None:
        """Read the data from the next unique key.

        :returns: the stored checkpoint, if any.
        """
        raise NotImplementedError


# }}}


# {{{ iterator checkpoints

_ITERATOR_CHK_METADATA_NAME = "_chk_metadata"


@dataclass(frozen=True)
class IteratorCheckpointManager(CheckpointManagerWithKeys):
    """A checkpoint helper that just increments a given iterator on each write.

    .. automethod:: __init__
    """

    pattern: str = "Checkpoint{i:09d}"
    """A :meth:`str.format` pattern for checkpoint names that takes the
    variable ``i`` as a counter for the number of calls to
    :meth:`~CheckpointManagerWithKeys.write` or
    :meth:`~CheckpointManagerWithKeys.read`.
    """

    _offset: int = field(default=1, init=False, repr=False)
    _ncalls: int = field(default=0, init=False, repr=False)
    _prev_key: str | None = field(default=None, init=False, repr=False)

    # {{{ keys

    def _get_and_advance_key(self):
        key = self.next_key

        object.__setattr__(self, "_ncalls", self._ncalls + self._offset)
        object.__setattr__(self, "_prev_key", key)

        return key

    @property
    def prev_key(self):
        return self._prev_key

    @property
    def next_key(self):
        return self.pattern.format(i=self._ncalls)

    # }}}

    # {{{ metadata

    @property
    def _metadata(self):
        # if we didn't iterate at all yet, just try to get the data from cache
        if self._prev_key is None and _ITERATOR_CHK_METADATA_NAME in self.rw:
            metadata = self.rw.read(_ITERATOR_CHK_METADATA_NAME)
        else:
            from pystopt.tools import gitrev

            metadata = {
                "rw": type(self.rw).__name__,
                "pattern": self.pattern,
                "ncalls": self._ncalls,
                "offset": self._offset,
                "gitrev": gitrev(),
            }

        return metadata

    def done(self):
        """
        Should be called to finalize the checkpointing and write the final state.
        This information is used to reset the iterator in :meth:`advance`.
        """

        self.rw.write(_ITERATOR_CHK_METADATA_NAME, self._metadata, overwrite=True)
        super().done()

    # }}}

    # {{{ advance

    @property
    def direction(self):
        return -1 if self._offset < 0 else +1

    def reverse(self) -> None:
        object.__setattr__(self, "_offset", -self._offset)

        # NOTE: this is required to start reading from the last written
        # checkpoint, otherwise it would not exist at `ncalls + 1`
        self._get_and_advance_key()

    def reset(self, pattern: str | None = None) -> None:
        """Resets the iterator to its original state."""
        object.__setattr__(self, "_ncalls", 0)
        object.__setattr__(self, "_prev_key", None)

        if pattern is not None:
            object.__setattr__(self, "pattern", pattern)

    def advance(self, ncalls: int | None = None) -> None:
        """Advance the iterator to the given number of calls. If the number is
        not given, it can be determined:

        * from metadata saved by the checkpoint on a previous run, see
          :meth:`~CheckpointManager.done`.
        * by iterating over the keys with the current pattern until a key is
          found that is not yet in the checkpoint.
        """
        assert self._offset > 0

        if ncalls is None and _ITERATOR_CHK_METADATA_NAME in self.rw:
            metadata = self.rw.read(_ITERATOR_CHK_METADATA_NAME)
            if metadata["offset"] > 0:
                ncalls = metadata["ncalls"]

        if ncalls is None:
            while self.skip():
                pass

            ncalls = self._ncalls

        if ncalls is None:
            raise RuntimeError("cannot determine checkpoint state")

        if ncalls > 0:
            object.__setattr__(self, "_ncalls", ncalls - 1)
            self._get_and_advance_key()

    def skip(self) -> bool:
        """Advances the :attr:`pattern` without reading or writing.

        :returns: *True* if the next key is already in the checkpoint. If it is
            not, the iterator is advanced and *False* is returned.
        """
        # do not advance if the key is in the checkpoint
        if self.next_key not in self.rw:
            return False

        return self._get_and_advance_key() in self.rw

    # }}}

    # {{{ rw

    def write(self, obj: Any, *, overwrite: bool = False) -> None:
        self.write_to(self._get_and_advance_key(), obj, overwrite=overwrite)

    def read(self) -> Any | None:
        return self.read_from(self._get_and_advance_key())

    # }}}


@dataclass(frozen=True)
class OnDiskIteratorCheckpointManager(IteratorCheckpointManager):
    @property
    def filename(self) -> pathlib.Path:
        """File name in which the current checkpoint is stored."""
        if isinstance(self.rw, OnDiskCheckpoint):
            return self.rw.filename

        raise NotImplementedError

    @property
    def root(self) -> pathlib.Path:
        """Root directory for all the checkpoints."""
        return self.filename.parent


# }}}


# {{{ in-memory dict


@dataclass(frozen=True)
class InMemoryCheckpoint(Checkpoint):
    storage: dict[Hashable, Any] = field(default_factory=dict, init=False, repr=False)
    is_open: bool = True

    def __contains__(self, key: Hashable) -> bool:
        return key in self.storage

    def close(self):
        object.__setattr__(self, "is_open", False)

    def write(self, key: Hashable, obj: Any, *, overwrite: bool = False) -> None:
        if not self.is_open:
            raise RuntimeError("checkpoint is closed")

        if not overwrite and key in self.storage:
            raise CheckpointExistsError(f"group {key} already exists")

        self.storage[key] = obj

    def read(self, key: Hashable) -> Any:
        if not self.is_open:
            raise RuntimeError("checkpoint is closed")

        if key not in self.storage:
            raise KeyError(f"key '{key}' cannot be found")

        return self.storage[key]


def make_memory_checkpoint_manager() -> IteratorCheckpointManager:
    rw = InMemoryCheckpoint()
    return IteratorCheckpointManager(rw=rw)


# }}}


# {{{ hdf reader/writer


@dataclass(frozen=True)
class H5Checkpoint(OnDiskCheckpoint):
    """A reader and writer for HDF5 checkpoints."""

    h5root: Any
    owned: bool

    @property
    def filename(self) -> pathlib.Path:
        return pathlib.Path(self.h5root.file.filename)

    def __del__(self):
        if self.owned:
            self.h5root.file.close()

    def __contains__(self, key: Hashable) -> bool:
        return key in self.h5root

    def close(self) -> None:
        if self.owned:
            self.h5root.file.close()
            object.__setattr__(self, "owned", False)

    def write(self, key: Hashable, obj: Any, *, overwrite: bool = False) -> None:
        if key in self.h5root:
            if overwrite:
                del self.h5root[key]
            else:
                raise ValueError(f"group {key} already exists")

        from pystopt.checkpoint.hdf import dump_to_group

        grp = self.h5root.require_group(key)
        dump_to_group(obj, grp)
        self.h5root.file.flush()

    def read(self, key: Hashable) -> Any:
        if key not in self.h5root:
            raise KeyError(f"key '{key}' cannot be found")

        from pystopt.checkpoint.hdf import load_from_group

        return load_from_group(self.h5root[key])

    def find(self, key: Hashable, group: str | None = None) -> Any | None:
        if group is not None:
            group = self.h5root[group]
        else:
            group = self.h5root

        from h5pyckle import load_by_pattern

        try:
            return load_by_pattern(group, pattern=key)
        except ValueError:
            return None


def make_hdf_checkpoint(
    filename_or_h5,
    *,
    mode: str = "a",
    h5_file_options: dict[str, Any] | None = None,
    h5_dset_options: dict[str, Any] | None = None,
) -> H5Checkpoint:
    """
    :arg filename_or_h5: can be a filename or an ``h5py`` object,
        like a file or a group.
    :arg h5_file_options: used to open an :class:`h5py.File` if
        *filename_or_h5* is a file name.
    :arg h5_dset_options: used when creating new datasets, see
        :attr:`h5pyckle.PickleGroup.h5_dset_options`.
    """

    if h5_file_options is None:
        h5_file_options = {}

    if h5_dset_options is None:
        h5_dset_options = {}

    import os

    import h5py

    if isinstance(filename_or_h5, str | bytes | os.PathLike):
        filename = pathlib.Path(filename_or_h5)
        if filename.suffix != ".h5":
            filename = filename.with_suffix(".h5")

        h5 = h5py.File(str(filename), mode, **h5_file_options)
        root = h5["/"]
        owned = True
    elif isinstance(filename_or_h5, h5py.File):
        root = filename_or_h5["/"]
        owned = False
    elif isinstance(filename_or_h5, h5py.Group):
        root = filename_or_h5
        owned = False
    else:
        raise TypeError(
            f"unsupported type for 'filename_or_h5': {type(filename_or_h5).__name__}"
        )

    from pystopt.checkpoint.hdf import PickleGroup

    root = PickleGroup(root.id, h5_dset_options=h5_dset_options)

    return H5Checkpoint(h5root=root, owned=owned)


def make_hdf_checkpoint_manager(
    filename_or_h5,
    *,
    mode: str = "a",
    h5_file_options: dict[str, Any] | None = None,
    h5_dset_options: dict[str, Any] | None = None,
) -> OnDiskIteratorCheckpointManager:
    rw = make_hdf_checkpoint(
        filename_or_h5,
        mode=mode,
        h5_file_options=h5_file_options,
        h5_dset_options=h5_dset_options,
    )

    return OnDiskIteratorCheckpointManager(rw=rw)


# }}}
