# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

from collections.abc import Sequence
from typing import Any

from pystopt.checkpoint.writing import (
    Checkpoint,
    CheckpointManager,
    CheckpointManagerWithKeys,
    H5Checkpoint,
    InMemoryCheckpoint,
    IteratorCheckpointManager,
    OnDiskCheckpoint,
    OnDiskIteratorCheckpointManager,
    make_hdf_checkpoint,
    make_hdf_checkpoint_manager,
    make_memory_checkpoint_manager,
)
from pystopt.paths import PathLike
from pystopt.tools import get_default_logger

logger = get_default_logger(__name__)


__all__ = (
    "Checkpoint",
    "CheckpointManager",
    "CheckpointManagerWithKeys",
    "H5Checkpoint",
    "InMemoryCheckpoint",
    "IteratorCheckpointManager",
    "OnDiskCheckpoint",
    "OnDiskIteratorCheckpointManager",
    "make_default_checkpoint_path",
    "make_hdf_checkpoint",
    "make_hdf_checkpoint_manager",
    "make_memory_checkpoint_manager",
)


# {{{ make_default_checkpoint_path


def make_default_checkpoint_path(
    name: str,
    key: Sequence[Any],
    *,
    today: str | bool = True,
    overwrite: bool = False,
    cwd: PathLike | None = None,
    ext: str = "h5",
) -> PathLike:
    """Creates a path for checkpoints of the form::

        [cwd]/output_[date]/[name]_[key]/checkpoint.[ext]

    where the key is stringified and concatenated from *key*.

    :arg overwrite: if *True* and the file already exists, a
        :exc:`FileExistsError` is raised.
    :returns: a path for the checkpoint file in a standard directory.
    """

    suffix = "_".join(str(k).lower() for k in key if k)

    # make directories
    from pystopt.paths import make_dirname

    todaydir = make_dirname("output", today=today, cwd=cwd)
    dirname = make_dirname(f"{name}_{suffix}", today=False, cwd=todaydir)

    # create checkpointing file
    from pystopt.paths import get_filename

    checkpoint_file_name = get_filename("checkpoint", ext=ext, cwd=dirname).aspath()

    if not overwrite and checkpoint_file_name.exists():
        raise FileExistsError(
            f"checkpoint file already exists: '{checkpoint_file_name}'"
        )

    return checkpoint_file_name


# }}}


# {{{ restore checkpointed result


def get_result_from_checkpoint(
    checkpoint: CheckpointManager,
    *,
    from_restart: bool = True,
    result_name: str = "result",
    variable_name: str = "x",
) -> Any:
    if isinstance(from_restart, bool) and from_restart:
        if result_name in checkpoint:
            result = checkpoint.read_from(result_name)
            x = result.x
        else:
            raise ValueError(f"could not find result at '{result_name}'")
    else:
        assert isinstance(checkpoint, IteratorCheckpointManager)
        name = checkpoint.pattern.format(i=from_restart + 1)
        name = f"{name}/{variable_name}"
        logger.info("reading variable '%s'", name)

        if name in checkpoint:
            x = checkpoint.read_from(name)
        else:
            raise ValueError(f"could not find result at '{name}'")

    return x


# }}}
