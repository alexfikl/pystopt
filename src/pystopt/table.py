# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

"""
Tables
------

.. autoclass:: UnicodeTable
.. autoclass:: LatexTable
.. autoclass:: MarkdownTable
"""

from dataclasses import dataclass, replace
from typing import Any

import numpy as np

from pystopt.tools import get_default_logger

logger = get_default_logger(__name__)


# <<< box


@dataclass(frozen=True)
class BoxSegment:
    """
    .. attribute:: left
    .. attribute:: mid
    .. attribute:: sep
    .. attribute:: right

    .. automethod:: render
    """

    left: str
    mid: str
    sep: str
    right: str

    def render(
        self, widths: tuple[int, ...], cols: tuple[str, ...] | None = None
    ) -> str:
        if cols is None:
            cols = tuple([f"{self.mid * w}" for w in widths])

        cols = f"{self.mid}{self.sep}{self.mid}".join(
            f"{{:{w}}}".format(c) for c, w in zip(cols, widths, strict=False)
        )

        return f"{self.left}{self.mid}{cols}{self.mid}{self.right}"

    @classmethod
    def from_format(cls, left: str, mid: str, sep: str, right: str) -> "BoxSegment":
        return cls(left=left, mid=mid, sep=sep, right=right)


@dataclass(frozen=True)
class Box:
    """
    .. attribute:: top
    .. attribute:: bottom
    .. attritute:: row
    .. attribute:: head
    .. attribute:: sep_head
    .. attribute:: sep_rows
    """

    top: BoxSegment
    head: BoxSegment
    sep_head: BoxSegment
    row: BoxSegment
    sep_rows: BoxSegment
    bottom: BoxSegment

    @classmethod
    def from_format(cls, fmt: str | tuple[tuple[str, ...], ...]) -> "Box":
        if isinstance(fmt, str):
            lines = tuple([tuple(line.strip()) for line in fmt.strip().split("\n")])
        elif isinstance(fmt, tuple):
            lines = fmt
        else:
            raise TypeError(f"unknown format type: '{type(fmt).__name__}'")

        return cls(
            top=BoxSegment.from_format(*lines[0]),
            head=BoxSegment.from_format(*lines[1]),
            sep_head=BoxSegment.from_format(*lines[2]),
            row=BoxSegment.from_format(*lines[3]),
            sep_rows=BoxSegment.from_format(*lines[4]),
            bottom=BoxSegment.from_format(*lines[5]),
        )


def make_box(name: str) -> str | Box:
    name = name.lower()
    if name == "ascii":
        fmt = """
        +--+
        | ||
        |-+|
        | ||
        |-+|
        +--+
        """
    elif name == "markdown":
        fmt = (
            ("", "", "", ""),
            ("|", " ", "|", "|"),
            ("|", "-", "|", "|"),
            ("|", " ", "|", "|"),
            ("", "", "", ""),
            ("", "", "", ""),
        )
    elif name == "latex":
        fmt = (
            (r"\begin{tabular}", "", "", ""),  # top
            ("", " ", "&", r"\\"),  # header
            ("", "", "", ""),  # header separator
            ("", " ", "&", r"\\"),  # row
            ("", "", "", ""),  # row separator
            (r"\end{tabular}", "", "", ""),  # bottom
        )
    elif name == "square":
        fmt = """
        ┌─┬┐
        │ ││
        ├─┼┤
        │ ││
        ├─┼┤
        └─┴┘
        """
    elif name == "heavy":
        fmt = """
        ┏━┳┓
        ┃ ┃┃
        ┡━╇┩
        │ ││
        ├─┼┤
        └─┴┘
        """
    else:
        raise ValueError(f"unknown name: '{name}'")

    return Box.from_format(fmt)


# >>>


# <<< tables


class UnicodeTable:
    """
    .. attribute:: ncolumns
    .. attribute:: nrows

    .. automethod:: __init__
    .. automethod:: add_header
    .. automethod:: add_footer
    .. automethod:: add_row
    .. automethod:: render
    """

    def __init__(
        self,
        ncolumns: int,
        *,
        box_type: str | Box | None = None,
        alignment: str = "c",
    ) -> None:
        """
        :arg ncolumns: number of columns in the table.
        :arg box_type: styling for the table borders with possible values:

            * ``'ascii'`` for a simple ASCII table,
            * ``'square'`` for a Unicode table,
            * ``'heavy'`` for a Unicode table similar to ``'simple'`` but with
              bold borders on the header.
            * ``'markdown'`` for a bare bones Markdown table, that does not have
              support for alignment, see also :class:`MarkdownTable`.
            * ``'latex'`` for a bare bones LaTex table that does not have
              support for alignment, see also :class:`LatexTable`.

        :arg alignment: alignment for each column as a single string,
            with supported values being ``'l'``, ``'r'`` and ``'c'``, for
            left, right and centered alignment.
        """
        if box_type is None:
            box_type = "heavy"

        if len(alignment) != ncolumns:
            if len(alignment) == 1:
                alignment = alignment * ncolumns
            else:
                raise ValueError(
                    f"'alignment' has unexpected size {len(alignment)} for "
                    f"{ncolumns} columns"
                )

        if isinstance(box_type, str):
            box = make_box(box_type)
        elif isinstance(box_type, Box):
            box = box_type
        else:
            raise TypeError(f"unknown box type: {box_type}")

        assert isinstance(box, Box)

        self.rows = []
        self.header = None
        self.footer = []

        self.ncolumns = ncolumns
        self.alignment = alignment
        self.box = box

    @property
    def nrows(self) -> int:
        return len(self.rows)

    def add_header(self, header: tuple[str, ...]) -> None:
        if len(header) != self.ncolumns:
            raise ValueError(f"expected {self.ncolumns} columns")

        self.header = header

    def add_footer(self, footer: tuple[str, ...]) -> None:
        if len(footer) != self.ncolumns:
            raise ValueError(f"expected {self.ncolumns} columns")

        self.footer.append(footer)

    def add_row(self, row: tuple[Any, ...]) -> None:
        if len(row) != self.ncolumns:
            raise ValueError(f"expected {self.ncolumns} columns")

        def formatter(s):
            if isinstance(s, float | np.float32 | np.float64):
                return f"{s:.6e}"

            return str(s)

        self.rows.append([formatter(r).strip() for r in row])

    def _column_widths(self) -> tuple[int, ...]:
        widths = tuple([
            max([len(r[n]) for r in self.rows], default=0) for n in range(self.ncolumns)
        ])

        if self.header:
            widths = tuple([max(w, len(self.header[n])) for n, w in enumerate(widths)])
        if self.footer:
            widths = tuple([
                max(*[len(f[n]) for f in self.footer], w) for n, w in enumerate(widths)
            ])

        return widths

    def _align_format(self, widths: tuple[int, ...]) -> tuple[str, ...]:
        fmt = {"l": "{{:<{}}}", "r": "{{:>{}}}", "c": "{{:^{}}}"}
        return tuple([
            fmt[key].format(width)
            for key, width in zip(self.alignment, widths, strict=False)
        ])

    def _render_rows(self) -> list[str]:
        widths = self._column_widths()
        row_fmt = self._align_format(widths)
        lines = []

        def _align_row(
            row_fmt: tuple[str, ...], row: tuple[str, ...]
        ) -> tuple[str, ...]:
            return tuple([fmt.format(r) for fmt, r in zip(row_fmt, row, strict=False)])

        # render header
        lines.append(self.box.top.render(widths))
        if self.header is not None:
            lines.append(
                self.box.head.render(widths, cols=_align_row(row_fmt, self.header))
            )
            lines.append(self.box.sep_head.render(widths))

        # render rows
        lines.extend([
            self.box.row.render(widths, cols=_align_row(row_fmt, row))
            for row in self.rows
        ])

        # render footer
        if self.footer:
            lines.append(self.box.sep_rows.render(widths))
            lines.extend([
                self.box.row.render(widths, cols=_align_row(row_fmt, footer))
                for footer in self.footer
            ])
        lines.append(self.box.bottom.render(widths))

        return [line for line in lines if line.strip()]

    def render(self) -> str:
        return "\n".join(self._render_rows())

    def __str__(self) -> str:
        return self.render()


class LatexTable(UnicodeTable):
    """LaTeX table based on ``tabular``.

    .. attribute:: use_booktabs

        If *True*, use ``booktabs`` compatible formatting for the table.
    """

    def __init__(
        self, ncolumns: int, *, alignment: str = "c", use_booktabs: bool = True
    ) -> None:
        if len(alignment) == 1:
            self.tabular_alignment = f"{alignment * ncolumns}"
        else:
            self.tabular_alignment = alignment

        super().__init__(ncolumns=ncolumns, box_type="latex", alignment="l")

        self.box = replace(
            self.box,
            top=replace(
                self.box.top, left=rf"\begin{{tabular}}{{{self.tabular_alignment}}}"
            ),
        )

        if use_booktabs:
            self.box = replace(
                self.box,
                top=replace(self.box.top, right="\n\\toprule"),
                head=replace(self.box.head, right="\\\\ \n\\midrule"),
                sep_rows=replace(self.box.sep_rows, left="\\midrule"),
                bottom=replace(self.box.bottom, left="\\bottomrule\n\\end{tabular}"),
            )


class MarkdownTable(UnicodeTable):
    """A Markdown compatible table."""

    def __init__(self, ncolumns: int, *, alignment: str = "c") -> None:
        super().__init__(ncolumns=ncolumns, box_type="markdown", alignment=alignment)

    def _render_rows(self) -> list[str]:
        def align_markdown(align: str, col: str) -> str:
            if align == "l":
                return f":{col[2:-1]}"
            elif align == "r":
                return f"{col[1:-2]}:"
            elif align == "c":
                return f":{col[2:-2]}:"
            else:
                raise ValueError(f"unknown alignment: '{align}'")

        lines = super()._render_rows()
        if self.header is not None:
            separators = tuple([
                align_markdown(a, s)
                for a, s in zip(self.alignment, lines[1].split("|")[1:-1], strict=False)
            ])

            widths = self._column_widths()
            lines[1] = self.box.head.render(widths, cols=separators)

        return lines


# >>>

# NOTE: all the latex stuff in here confuses vim's default markers
# vim:foldmarker=<<<,>>>:foldmethod=marker
