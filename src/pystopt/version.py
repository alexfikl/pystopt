# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

__title__ = "pystopt"
__description__ = "Two-Phase Stokes Adjoint-based Optimization"
__full_version__ = __version__ = "2020.5"
__author__ = __maintainer__ = "Alexandru Fikl"
__email__ = "alexfikl@gmail.com"
__license__ = "MIT"
__copyright__ = "Copyright 2020 Alexandru Fikl"
