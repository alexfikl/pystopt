# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

"""
.. currentmodule:: pystopt.mesh

.. autoclass:: MeshGenerator
.. autofunction:: get_mesh_generator_from_name
.. autofunction:: generate_discretization

.. autoclass:: FourierGenerator
.. autoclass:: SPHARMGenerator

.. currentmodule:: pystopt.mesh.generation

.. autofunction:: ellipse_from_axes
.. autofunction:: fourier_ufo

.. autofunction:: spharm_sphere
.. autofunction:: spharm_spheroid
.. autofunction:: spharm_ufo
.. autofunction:: spharm_urchin
"""

from abc import ABC, abstractmethod
from collections.abc import Callable
from dataclasses import dataclass
from functools import partial, singledispatch
from typing import Any

import numpy as np

from arraycontext import ArrayContext
from meshmode.discretization import Discretization
from meshmode.discretization.poly_element import (
    ElementGroupFactory,
    InterpolatoryQuadratureGroupFactory,
)

from pystopt.tools import get_default_logger

logger = get_default_logger(__name__)


# {{{ helpers


def make_transform_matrix_from_angles(
    alpha: float,
    beta: float | None = None,
    gamma: float | None = None,
) -> np.ndarray:
    if beta is None and gamma is None:
        # assume we're in 2d
        return np.array([
            [np.cos(alpha), -np.sin(alpha)],
            [np.sin(alpha), np.cos(alpha)],
        ])
    elif beta is not None and gamma is not None:
        # assume we're in 3d
        from scipy.spatial.transform import Rotation

        # this matches
        #   sp.rot_axis3(alpha) @ sp.rot_axis2(beta) @ sp.rot_axis1(gamma)
        rot = Rotation.from_euler("ZYX", (-alpha, -beta, -gamma))

        return rot.as_matrix()
    else:
        raise ValueError(
            "both 'beta' and 'gamma' should be None or floats: "
            f"given {beta} and {gamma}"
        )


# }}}


# {{{ mesh generator interface


def get_mesh_generator_class_from_name(name):
    name_to_cls = {
        # fourier
        "fourier": FourierGenerator,
        "fourier_circle": FourierCircleGenerator,
        "fourier_ellipse": FourierEllipseGenerator,
        "fourier_ellipse_v2": FourierEllipseV2Generator,
        "fourier_starfish": FourierStarfishGenerator,
        "fourier_ufo": FourierUFOGenerator,
        # spharm
        "spharm": SPHARMGenerator,
        "spharm_sphere": SPHSphereGenerator,
        "spharm_spheroid": SPHSpheroidGenerator,
        "spharm_ufo": SPHUFOGenerator,
        "spharm_urchin": SPHUrchinGenerator,
    }

    return name_to_cls.get(name)


def get_mesh_generator_from_name(name, **kwargs):
    """Construct a specific mesh generator from its name.

    .. code::

        circle = build_mesh_from_name("circle", radius=np.pi)
        mesh = circle.get_mesh(resolution=32, mesh_order=4)

    :arg name: name of one of the existing mesh generators.
    :arg kwargs: any additional arguments that can be passed to the generator.

    :returns: a :class:`MeshGenerator`.
    """

    cls = get_mesh_generator_class_from_name(name)
    if cls is None:
        raise ValueError(f"unknown mesh name '{name}'")

    return cls(**kwargs)


@dataclass(frozen=True)
class MeshGenerator(ABC):
    """
    .. attribute:: dim
    .. attribute:: ambient_dim

    .. attribute:: name
    .. attribute:: mesh_order
    .. attribute:: target_order

    .. attribute:: group_factory_cls
    .. attribute:: mesh_unit_nodes

    .. attribute:: use_default_mesh_unit_nodes
    .. attribute:: use_spectral_derivative

    .. attribute:: offset
    .. attribute:: transform_matrix

    .. automethod:: default_group_factory
    .. automethod:: default_mesh_unit_nodes_for_factory
    """

    name: str | None = None
    mesh_order: int | None = None
    target_order: int | None = None

    group_factory_cls: type[ElementGroupFactory] = InterpolatoryQuadratureGroupFactory
    mesh_unit_nodes: np.ndarray | None = None

    use_default_mesh_unit_nodes: bool = True
    use_spectral_derivative: bool = True

    offset: np.ndarray | None = None
    transform_matrix: float | np.ndarray | None = None

    def __post_init__(self):
        assert self.name is not None

        if self.target_order is None:
            raise ValueError("'target_order' not provided")

        if self.mesh_order is None:
            object.__setattr__(self, "mesh_order", self.target_order)

    @property
    def dim(self):
        return self.ambient_dim - 1

    @property
    @abstractmethod
    def ambient_dim(self):
        pass

    @property
    def _transform_matrix(self):
        if self.transform_matrix is None:
            return self.transform_matrix

        from numbers import Number

        if isinstance(self.transform_matrix, Number):
            return self.transform_matrix * np.eye(self.ambient_dim)

        assert isinstance(self.transform_matrix, np.ndarray)
        if self.transform_matrix.ndim == 1:
            return np.diag(self.transform_matrix)
        elif self.transform_matrix.ndim == 2:
            return self.transform_matrix
        else:
            raise ValueError(f"unsupported dimension: {self.transform_matrix.ndim}")

    def default_group_factory(self, order: int | None = None) -> ElementGroupFactory:
        if order is None:
            order = self.target_order

        if order is None:
            raise ValueError("must provide 'order' or 'target_order'")

        return self.group_factory_cls(order=order)

    def default_mesh_unit_nodes_for_factory(
        self, group_factory: ElementGroupFactory
    ) -> np.ndarray | None:
        if self.mesh_unit_nodes is not None:
            return self.mesh_unit_nodes

        if not self.use_default_mesh_unit_nodes:
            return None

        from pystopt.mesh.poly_element import get_unit_nodes_for_group_factory

        return get_unit_nodes_for_group_factory(self.dim, group_factory)

    def __str__(self):
        from pystopt.tools import dc_stringify

        return dc_stringify(self)


@singledispatch
def generate_discretization(
    gen: Any, actx: ArrayContext, resolution: int | tuple[int, ...]
) -> Discretization:
    raise NotImplementedError(type(gen).__name__)


# }}}


# {{{ fourier generators


@dataclass(frozen=True)
class FourierGenerator(MeshGenerator):
    """
    .. attribute:: curve
    """

    name: str = "_fourier_unknown"
    curve: Callable[[np.ndarray], np.ndarray] | None = None
    rfft: bool = False

    @property
    def ambient_dim(self):
        return 2

    @property
    def curve_fn(self):
        if self.curve is None:
            raise AttributeError

        return self.curve


@generate_discretization.register(FourierGenerator)
def _generate_fourier_discretization(
    gen: FourierGenerator,
    actx: ArrayContext,
    resolution: int | tuple[int, ...],
) -> Discretization:
    group_factory = gen.default_group_factory(gen.target_order)
    mesh_unit_nodes = gen.default_mesh_unit_nodes_for_factory(group_factory)

    if isinstance(resolution, tuple):
        (resolution,) = resolution

    def curve_fn(t):
        f = gen.curve_fn(t)
        if gen._transform_matrix is not None:
            f = gen._transform_matrix @ f

        if gen.offset is not None:
            f = f + gen.offset.reshape(-1, 1)

        return f

    from pystopt.mesh.fourier import make_fourier_discretization

    discr, _ = make_fourier_discretization(
        actx,
        resolution,
        curve_fn=curve_fn,
        order=gen.mesh_order,
        group_factory=group_factory,
        mesh_unit_nodes=mesh_unit_nodes,
        is_real=gen.rfft,
        use_spectral_derivative=gen.use_spectral_derivative,
    )

    return discr


@dataclass(frozen=True)
class FourierCircleGenerator(FourierGenerator):
    """
    .. attribute:: radius
    """

    name: str = "fourier_circle"
    radius: float = 1.0

    @property
    def curve_fn(self):
        from meshmode.mesh.generation import ellipse

        return lambda t: self.radius * ellipse(1.0, t)


@dataclass(frozen=True)
class FourierEllipseGenerator(FourierGenerator):
    """
    .. attribute:: radius
    .. attribute:: aspect_ratio
    """

    name: str = "fourier_ellipse"
    radius: float = 1.0
    aspect_ratio: float = 2.0

    @property
    def curve_fn(self):
        from meshmode.mesh.generation import ellipse

        return lambda t: self.radius * ellipse(self.aspect_ratio, t)


@dataclass(frozen=True)
class FourierEllipseV2Generator(FourierEllipseGenerator):
    name: str = "fourier_ellipse_v2"

    @property
    def a(self):
        return self.radius

    @property
    def b(self):
        return self.radius * self.aspect_ratio

    @property
    def curve_fn(self):
        return partial(ellipse_from_axes, a=self.a, b=self.b)


@dataclass(frozen=True)
class FourierStarfishGenerator(FourierGenerator):
    """
    .. attribute:: narms
    .. attribute:: amplitude
    """

    name: str = "fourier_starfish"
    narms: int = 5
    amplitude: float = 0.25

    @property
    def curve_fn(self):
        from meshmode.mesh.generation import NArmedStarfish

        return NArmedStarfish(self.narms, self.amplitude)


@dataclass(frozen=True)
class FourierUFOGenerator(FourierGenerator):
    """
    .. attribute:: k
    .. attribute:: a
    .. attribute:: b
    """

    name: str = "fourier_ufo"

    k: int = 4
    a: float = 3
    b: float = 1
    radius: float = 1.0

    @property
    def curve_fn(self):
        return lambda t: self.radius * fourier_ufo(t, a=self.a, b=self.b, k=self.k)


# }}}


# {{{ spharm generators


@dataclass(frozen=True)
class SPHARMGenerator(MeshGenerator):
    """
    .. attribute:: surface

    .. attribute:: lmax
    .. attribute:: mmax
    .. attribute:: polar_opt
    """

    name: str = "_spharm_unknown"
    surface: Callable[[np.ndarray, np.ndarray], np.ndarray] | None = None

    lmax: int | None = None
    mmax: int | None = None
    polar_opt: float | None = 1.0e-10

    @property
    def ambient_dim(self):
        return 3

    @property
    def surface_fn(self):
        if self.surface is None:
            raise AttributeError

        return self.surface


@generate_discretization.register(SPHARMGenerator)
def _generate_spharm_discretization(
    gen: SPHARMGenerator, actx: ArrayContext, resolution: int | tuple[int, ...]
) -> Discretization:
    group_factory = gen.default_group_factory(gen.mesh_order)
    mesh_unit_nodes = gen.default_mesh_unit_nodes_for_factory(group_factory)

    if isinstance(resolution, int):
        nellon, nellat = resolution, resolution
    else:
        nellon, nellat = resolution

    def surface_fn(theta, phi):
        f = gen.surface_fn(theta, phi)
        if gen._transform_matrix is not None:
            f = np.einsum("ij,j...->i...", gen._transform_matrix, f)

        if gen.offset is not None:
            f = f + gen.offset.reshape((-1,) + (1,) * theta.ndim)

        return f

    from pystopt.mesh.spharm import make_spharm_discretization

    discr, _ = make_spharm_discretization(
        actx,
        nellon,
        nellat,
        surface_fn=surface_fn,
        order=gen.mesh_order,
        lmax=gen.lmax,
        mmax=gen.mmax,
        group_factory=group_factory,
        mesh_unit_nodes=mesh_unit_nodes,
        use_spectral_derivative=gen.use_spectral_derivative,
        polar_opt=gen.polar_opt,
    )

    return discr


@dataclass(frozen=True)
class SPHSphereGenerator(SPHARMGenerator):
    name: str = "spharm_sphere"
    radius: float = 1.0

    @property
    def surface_fn(self):
        return partial(spharm_sphere, radius=self.radius)


@dataclass(frozen=True)
class SPHSpheroidGenerator(SPHARMGenerator):
    name: str = "spharm_spheroid"
    radius: float = 1.0
    aspect_ratio: float = 2.0

    @property
    def surface_fn(self):
        return partial(
            spharm_spheroid, radius=self.radius, aspect_ratio=self.aspect_ratio
        )


@dataclass(frozen=True)
class SPHUFOGenerator(SPHARMGenerator):
    name: str = "spharm_ufo"

    k: float = 2.0
    a: float = 1.5
    b: float = 1.0
    aspect_ratio: float = 1.0

    @property
    def surface_fn(self):
        return partial(
            spharm_ufo, k=self.k, a=self.a, b=self.b, aspect_ratio=self.aspect_ratio
        )


@dataclass(frozen=True)
class SPHUrchinGenerator(SPHARMGenerator):
    name: str = "spharm_urchin"

    m: int = 2
    n: int = 3
    k: float = 3.0

    @property
    def surface_fn(self):
        return partial(spharm_urchin, m=self.m, n=self.n, k=self.k)


# }}}


# {{{ curves


def ellipse_from_axes(t: np.ndarray, *, a: float = 2.0, b: float = 1.0) -> np.ndarray:
    r"""Generate an ellipse parametrized by *t* given its axes.

    .. math::

        \begin{bmatrix}
        r(t) \cos 2 \pi t \\
        r(t) \sin 2 \pi t
        \end{bmatrix}, \quad \text{where }
        r(t) = \frac{a b}{\sqrt{(b \cos 2 \pi t)^2 + (a \sin 2 \pi t)^2}}

    :arg a: major axis.
    :arg b: minor axis.
    """
    t = 2.0 * np.pi * t
    r = a * b / np.sqrt((b * np.cos(t)) ** 2 + (a * np.sin(t)) ** 2)

    return np.vstack([
        r * np.cos(t),
        r * np.sin(t),
    ])


def fourier_ufo(
    t: np.ndarray, *, a: float = 3.0, b: float = 1.0, k: int = 4
) -> np.ndarray:
    r"""Generate a potato parametrized by *t*.

    .. math::

        \begin{bmatrix}
        r(t) \cos 2 \pi t \\
        r(t) \sin 2 \pi t
        \end{bmatrix}, \quad \text{where }
        r(t) = \sqrt{(b \cos 2 \pi t)^2 + (a \sin 2 \pi t)^2} + \cos^2 (2 \pi k t)
    """
    t = 2 * np.pi * t
    r = np.sqrt((a * np.cos(t)) ** 2 + (b * np.sin(t)) ** 2) + np.cos(k * t) ** 2

    return np.stack([r * np.cos(t), r * np.sin(t)])


# }}}


# {{{ surfaces


def spharm_sphere(
    theta: np.ndarray, phi: np.ndarray, *, radius: float = 1.0
) -> np.ndarray:
    r"""Generate a sphere parametrized by :math:`(\theta, \phi)`.

    .. math::

        \begin{bmatrix}
        r \sin \theta \cos \phi \\
        r \sin \theta \sin \phi \\
        r \cos \theta
        \end{bmatrix}

    :arg radius: sphere radius.
    """

    return np.stack([
        radius * np.sin(theta) * np.cos(phi),
        radius * np.sin(theta) * np.sin(phi),
        radius * np.cos(theta),
    ])


def spharm_spheroid(
    theta: np.ndarray,
    phi: np.ndarray,
    *,
    radius: float = 1.0,
    aspect_ratio: float = 2.0,
) -> np.ndarray:
    r"""Generate a spheroid parametrized by :math:`(\theta, \phi)`.

    .. math::

        \begin{bmatrix}
        r \sin \theta \cos \phi \\
        r \sin \theta \sin \phi \\
        \alpha r \cos \theta
        \end{bmatrix}

    where :math:`\alpha` is the aspect ratio.

    :arg radius: spheroid radius.
    :arg aspect_ratio: aspect ratio of the spheroid.
    """
    return np.stack([
        radius * np.sin(theta) * np.cos(phi),
        radius * np.sin(theta) * np.sin(phi),
        radius * aspect_ratio * np.cos(theta),
    ])


def spharm_ufo(
    theta: np.ndarray,
    phi: np.ndarray,
    *,
    k: float = 4.0,
    a: float = 2.0,
    b: float = 1.0,
    aspect_ratio: float = 0.5,
) -> np.ndarray:
    r"""Generate a potato parametrized by :math:`(\theta, \phi)`.

    .. math::

        \begin{bmatrix}
        r(\theta) \sin \theta \cos \phi \\
        r(\theta) \sin \theta \sin \phi \\
        \alpha r(\theta) \cos \theta
        \end{bmatrix}, \quad \text{where }
        r(\theta) = \sqrt{(b \cos \theta)^2 + (a \sin \theta)^2} + \cos^2 (k \theta)

    where :math:`\alpha` is the aspect ratio.
    """
    r = (
        np.sqrt((a * np.cos(theta)) ** 2 + (b * np.sin(theta)) ** 2)
        + np.cos(k * theta) ** 2
    )
    r_sin_theta = r * np.sin(theta)

    return np.stack([
        r_sin_theta * np.cos(phi),
        r_sin_theta * np.sin(phi),
        aspect_ratio * r * np.cos(theta),
    ])


def spharm_urchin(
    theta: np.ndarray, phi: np.ndarray, *, m: int = 2, n: int = 3, k: float = 3.0
) -> np.ndarray:
    r"""Generate a potato parametrized by :math:`(\theta, \phi)`.

    .. math::

        \begin{bmatrix}
        r(\theta, \phi) \sin \theta \cos \phi \\
        r(\theta, \phi) \sin \theta \sin \phi \\
        r(\theta, \phi) \cos \theta
        \end{bmatrix}, \quad \text{where }
        r(\theta, \phi) = 1 + \exp \big(-k Y^m_n(\theta, \phi)\big)

    where :math:`Y^m_n` is the spherical harmonic of order :math:`m` and
    degree :math:`n`.
    """
    # NOTE: inspired by https://doi.org/10.1016/j.jcp.2011.03.045

    from scipy.special import sph_harm  # pylint: disable=no-name-in-module

    ymn = sph_harm(m, n, phi, theta)

    r = 1 + np.exp(-k * np.real(ymn))
    r_sin_theta = r * np.sin(theta)

    return np.stack([
        r_sin_theta * np.cos(phi),
        r_sin_theta * np.sin(phi),
        r * np.cos(theta),
    ])


# }}}
