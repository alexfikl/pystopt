# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

"""
.. currentmodule:: pystopt.mesh

.. autoclass:: SpectralConnectionBatch
.. autoclass:: SpectralConnectionElementGroup
.. autoclass:: SpectralGridConnection

.. autofunction:: make_spectral_connections
.. autofunction:: update_spectral_connections_for_factory

.. autoclass:: SpectralDiscretization
"""

from abc import ABC, abstractmethod
from dataclasses import dataclass
from typing import Any

import numpy as np

import meshmode.discretization.poly_element as mpoly
from arraycontext import ArrayContext, ArrayOrContainerT
from meshmode.discretization import Discretization, InterpolatoryElementGroupBase
from meshmode.discretization.connection import (
    ChainedDiscretizationConnection,
    DiscretizationConnection,
)
from meshmode.mesh import Mesh
from pytools import memoize_method

from pystopt.dof_array import DOFArray, SpectralDOFArray
from pystopt.mesh.connection import (
    FromUniqueDOFConnection,
    ToUniqueDOFConnection,
    UniqueConnectionBatch,
    UniqueConnectionElementGroup,
)
from pystopt.tools import get_default_logger

logger = get_default_logger(__name__)


# {{{ spectral element groups


@dataclass(frozen=True)
class SpectralConnectionBatch(UniqueConnectionBatch):
    """Describes transporting elements on a spectral mesh."""

    from_group_index: int
    node_indices: np.ndarray


@dataclass(frozen=True)
class SpectralConnectionElementGroup(ABC, UniqueConnectionElementGroup):
    r"""Holds batches and spectral element information.

    .. attribute:: dim
    .. attribute:: nspec

        Number of spectral coefficients to represent the
        :attr:`~pystopt.mesh.UniqueConnectionElementGroup.noutputs` DOFs.

    .. attribute:: spat_shape

        Shape of the spatial grid.

    .. automethod:: to_spectral
    .. automethod:: from_spectral
    """

    @property
    @abstractmethod
    def nspec(self):
        pass

    @property
    @abstractmethod
    def spat_shape(self):
        pass

    @abstractmethod
    def to_spectral(self, actx: ArrayContext, ary: Any) -> Any:
        """Transforms an array of shape :attr:`spat_shape` into its spectral
        coefficients.

        :arg ary: an array of type :attr:`arraycontext.ArrayContext.array_types`.
        :returns: an array of type :attr:`arraycontext.ArrayContext.array_types`.
        """

    @abstractmethod
    def from_spectral(self, actx: ArrayContext, ary: Any) -> Any:
        """Transforms an array of spectral coefficients of shape (:attr:`nspec`, 1)
        back to its spatial representation.

        :arg ary: an array of type :attr:`arraycontext.ArrayContext.array_types`.
        :returns: an array of type :attr:`arraycontext.ArrayContext.array_types`.
        """


# }}}


# {{{ spectral grid connection


class SpectralGridConnection(DiscretizationConnection):
    r"""Transports from the spectral grid to the spectral coefficients.

    .. attribute:: groups

        A :class:`tuple` of :class:`SpectralConnectionElementGroup`\ s
        used for the connection to the spectral grid.
    """

    def __init__(
        self,
        discr: "SpectralDiscretization",
        groups: tuple[SpectralConnectionElementGroup, ...],
    ) -> None:
        super().__init__(discr, discr, is_surjective=True)
        self.groups = groups


class ToSpectralGridConnection(SpectralGridConnection):
    @property
    def noutputs(self) -> int:
        return sum(grp.nspec for grp in self.groups)

    def _to_spectral(self, ary: DOFArray) -> SpectralDOFArray:
        if not isinstance(ary, DOFArray):
            raise TypeError("non-array passed to mesh connection")

        if ary.shape != (len(self.groups),):
            raise ValueError("invalid shape of incoming resampling data")

        actx = ary.array_context
        return SpectralDOFArray(
            actx,
            tuple(
                grp.to_spectral(actx, g_ary)
                for grp, g_ary in zip(self.groups, ary, strict=False)
            ),
        )

    def __call__(self, ary: ArrayOrContainerT) -> ArrayOrContainerT:
        from meshmode.dof_array import rec_map_dof_array_container

        return rec_map_dof_array_container(self._to_spectral, ary)


class FromSpectralGridConnection(SpectralGridConnection):
    @property
    def noutputs(self) -> int:
        return sum(np.prod(grp.spat_shape) for grp in self.groups)

    def _from_spectral(self, ary: SpectralDOFArray) -> DOFArray:
        if not isinstance(ary, SpectralDOFArray):
            raise TypeError("non-spectral array passed to mesh connection")

        if ary.shape != (len(self.groups),):
            raise ValueError("invalid shape of incoming resampling data")

        actx = ary.array_context
        return DOFArray(
            ary.array_context,
            tuple(
                grp.from_spectral(actx, g_ary)
                for grp, g_ary in zip(self.groups, ary, strict=False)
            ),
        )

    def __call__(self, ary: ArrayOrContainerT) -> ArrayOrContainerT:
        from meshmode.dof_array import rec_map_dof_array_container

        return rec_map_dof_array_container(self._from_spectral, ary)


def from_spectral(
    discr: "SpectralDiscretization", ary: ArrayOrContainerT, *, strict: bool = True
) -> ArrayOrContainerT:
    """A wrapper around :attr:`pystopt.mesh.SpectralConnection.from_spectral_conn`
    that can silently ignore any :class:`meshmode.dof_array.DOFArray` entries
    it finds in the container *ary*.
    """

    def _from_spectral(subary):
        from numbers import Number

        if isinstance(subary, Number):
            return subary

        if isinstance(subary, SpectralDOFArray):
            return discr.from_spectral_conn(subary)

        if isinstance(subary, DOFArray):
            if strict:
                raise TypeError("non-spectral array passed to mesh connection")

            return subary

        raise TypeError("non-dof array passed to connection")

    from meshmode.dof_array import rec_map_dof_array_container

    return rec_map_dof_array_container(_from_spectral, ary)


def to_spectral(
    discr: "SpectralDiscretization", ary: ArrayOrContainerT, *, strict: bool = True
) -> ArrayOrContainerT:
    """A wrapper around :attr:`pystopt.mesh.SpectralConnection.to_spectral_conn`
    that can silently ignore any :class:`meshmode.dof_array.SpectralDOFArray`
    entries it finds in the container *ary*.
    """

    def _to_spectral(subary):
        if isinstance(subary, SpectralDOFArray):
            if strict:
                raise TypeError("non-dof array passed to mesh connection")

            return subary

        from numbers import Number

        if isinstance(subary, Number):
            return subary

        return discr.to_spectral_conn(subary)

    from meshmode.dof_array import rec_map_dof_array_container

    return rec_map_dof_array_container(_to_spectral, ary)


def is_spectral(ary: ArrayOrContainerT) -> bool:
    def _is_spectral(subary):
        if isinstance(subary, SpectralDOFArray):
            return True

        if isinstance(subary, DOFArray):
            return False

        from arraycontext import NotAnArrayContainerError, serialize_container

        try:
            iterable = serialize_container(subary)
        except NotAnArrayContainerError:
            return 1
        else:
            return sum(_is_spectral(value) for _, value in iterable)

    return _is_spectral(ary) == 0


# }}}


# {{{ spectral connection


def _is_equidistant(grp):
    is_equidistant = isinstance(  # noqa: UP038
        grp,
        (
            mpoly.PolynomialEquidistantSimplexElementGroup,
            mpoly.EquidistantTensorProductElementGroup,
        ),
    )
    if not is_equidistant:
        return False

    grp_unit_nodes = grp.unit_nodes
    mgrp_unit_nodes = grp.mesh_el_group.unit_nodes

    eps = 10 * np.finfo(grp_unit_nodes.dtype).eps
    return (
        grp_unit_nodes.shape == mgrp_unit_nodes.shape
        and np.linalg.norm(grp_unit_nodes - mgrp_unit_nodes) < eps
    )


def _make_unique_connection_groups(
    actx: ArrayContext,
    specgroups: tuple[SpectralConnectionElementGroup, ...],
    discr_groups: tuple[InterpolatoryElementGroupBase, ...],
) -> tuple[UniqueConnectionElementGroup, ...]:
    from pystopt.mesh.connection import UniqueConnectionInterpolationBatch
    from pystopt.mesh.poly_element import (
        from_equidistant_interp_matrix,
        to_equidistant_interp_matrix,
    )

    groups = []
    for sgrp in specgroups:
        batches = []
        for sbatch in sgrp.batches:
            dgrp = discr_groups[sbatch.from_group_index]

            if _is_equidistant(dgrp):
                batch = UniqueConnectionBatch(
                    from_group_index=sbatch.from_group_index,
                    node_indices=sbatch.node_indices,
                )
            else:
                batch = UniqueConnectionInterpolationBatch(
                    from_group_index=sbatch.from_group_index,
                    node_indices=sbatch.node_indices,
                    to_resampling_mat=actx.freeze(
                        actx.from_numpy(to_equidistant_interp_matrix(dgrp))
                    ),
                    from_resampling_mat=actx.freeze(
                        actx.from_numpy(from_equidistant_interp_matrix(dgrp))
                    ),
                )

            batches.append(batch)

        grp = UniqueConnectionElementGroup(
            noutputs=sgrp.noutputs, batches=tuple(batches)
        )

        groups.append(grp)

    return tuple(groups)


def make_spectral_connections(
    discr: "SpectralDiscretization",
    specgroups: tuple[SpectralConnectionElementGroup, ...],
) -> tuple[DiscretizationConnection, DiscretizationConnection]:
    actx = discr._setup_actx
    groups = _make_unique_connection_groups(actx, specgroups, discr.groups)

    from pystopt.mesh.connection import compute_node_count_from_groups

    weights = actx.freeze(compute_node_count_from_groups(actx, groups))

    to_spectral_conn = ChainedDiscretizationConnection(
        [
            ToUniqueDOFConnection(discr, groups, weights),
            ToSpectralGridConnection(discr, specgroups),
        ],
        from_discr=discr,
    )

    from_spectral_conn = ChainedDiscretizationConnection(
        [
            FromSpectralGridConnection(discr, specgroups),
            FromUniqueDOFConnection(discr, groups, weights),
        ],
        from_discr=discr,
    )

    return to_spectral_conn, from_spectral_conn


def update_spectral_connections_for_factory(
    to_discr: "SpectralDiscretization",
    from_discr: "SpectralDiscretization",
    group_factory: mpoly.ElementGroupFactory | None = None,
) -> tuple[DiscretizationConnection, DiscretizationConnection]:
    if from_discr.to_spectral_conn is None:
        return None, None

    if group_factory is None:
        groups = from_discr.to_spectral_conn.connections[0].groups
    else:
        actx = to_discr._setup_actx
        to_groups = tuple([
            group_factory(grp.mesh_el_group) for igrp, grp in enumerate(to_discr.groups)
        ])
        groups = _make_unique_connection_groups(actx, from_discr.specgroups, to_groups)

    weights = from_discr.to_spectral_conn.connections[0].weights

    to_spectral_conn = ChainedDiscretizationConnection(
        [
            ToUniqueDOFConnection(to_discr, groups, weights),
            ToSpectralGridConnection(to_discr, from_discr.specgroups),
        ],
        from_discr=to_discr,
    )

    from_spectral_conn = ChainedDiscretizationConnection(
        [
            FromSpectralGridConnection(to_discr, from_discr.specgroups),
            FromUniqueDOFConnection(to_discr, groups, weights),
        ],
        from_discr=to_discr,
    )

    return to_spectral_conn, from_spectral_conn


# }}}


# {{{ discretization base


class NotProvidedSentinel:
    pass


class SpectralDiscretization(Discretization):
    r"""
    .. attribute:: specgroups

        A :class:`list` of :class:`SpectralConnectionElementGroup` that are
        used in :attr:`to_spectral_conn` and :attr:`from_spectral_conn`.

    .. attribute:: to_spectral_conn

        A :class:`~meshmode.discretization.connection.DiscretizationConnection`
        used to transport from the discretization DOFs to the spectral coefficients.

    .. attribute:: from_spectral_conn

        A :class:`~meshmode.discretization.connection.DiscretizationConnection`
        used to transport from the spectral coefficients to the discretization DOFs.

    .. automethod:: spectral_num_reference_derivative
    """

    def __init__(
        self,
        actx: ArrayContext,
        mesh: Mesh,
        specgroups: tuple[SpectralConnectionElementGroup, ...],
        *,
        group_factory: InterpolatoryElementGroupBase | None = None,
        real_dtype: np.dtype = np.float64,
        use_spectral_derivative: bool = False,
        _xlm: np.ndarray | None = None,
        _to_spectral_conn: DiscretizationConnection | None = NotProvidedSentinel,
        _from_spectral_conn: DiscretizationConnection | None = NotProvidedSentinel,
        _force_actx_clone: bool = True,
    ) -> None:
        r"""
        :arg specgroups: a :class:`list` of :class:`SpectralConnectionElementGroup`\ s
            used for the connection to the spectral :math:`(\theta, \phi)` grid.
        :arg use_spectral_derivative: if *True*, the discretization uses
            :meth:`spectral_num_reference_derivative` as the
            :meth:`Discretization.num_reference_derivative` implementation.
        """
        super().__init__(
            actx,
            mesh,
            group_factory,
            real_dtype=real_dtype,
            _force_actx_clone=_force_actx_clone,
        )

        if not self._is_interpolatory:
            _to_spectral_conn = _from_spectral_conn = None
            use_spectral_derivative = False
        elif _to_spectral_conn is NotProvidedSentinel:
            assert _from_spectral_conn is NotProvidedSentinel
            _to_spectral_conn, _from_spectral_conn = make_spectral_connections(
                self, specgroups
            )

        if _to_spectral_conn is None:
            _xlm = None

        assert _to_spectral_conn is not NotProvidedSentinel
        assert _from_spectral_conn is not NotProvidedSentinel

        if _xlm is not None:
            assert all(xi.array_context is None for xi in _xlm)
            assert all(isinstance(xi, SpectralDOFArray) for xi in _xlm)

        self.xlm = _xlm
        self.specgroups = specgroups
        self.to_spectral_conn = _to_spectral_conn
        self.from_spectral_conn = _from_spectral_conn
        self.use_spectral_derivative = use_spectral_derivative

    @property
    @memoize_method
    def _is_interpolatory(self):
        return all(
            isinstance(grp, InterpolatoryElementGroupBase) for grp in self.groups
        )

    def copy(
        self,
        actx=None,
        mesh=None,
        group_factory=None,
        real_dtype=None,
        xlm=NotProvidedSentinel,
    ) -> "SpectralDiscretization":
        if mesh is not None and mesh.nelements != self.mesh.nelements:
            return Discretization(
                self._setup_actx if actx is None else actx.clone(),
                mesh,
                group_factory=(
                    self._group_factory if group_factory is None else group_factory
                ),
                real_dtype=real_dtype,
                _force_actx_clone=False,
            )

        from dataclasses import replace

        discr = type(self)(
            self._setup_actx if actx is None else actx.clone(),
            self.mesh if mesh is None else mesh,
            tuple([replace(grp) for grp in self.specgroups]),
            group_factory=(
                self._group_factory if group_factory is None else group_factory
            ),
            real_dtype=self.real_dtype if real_dtype is None else real_dtype,
            use_spectral_derivative=self.use_spectral_derivative,
            _xlm=None,
            _to_spectral_conn=None,
            _from_spectral_conn=None,
            _force_actx_clone=False,
        )

        from pystopt.mesh import check_discr_same_connectivity

        if (
            discr._is_interpolatory
            and discr.use_spectral_derivative
            and check_discr_same_connectivity(discr, self)
        ):
            (
                to_spectral_conn,
                from_spectral_conn,
            ) = update_spectral_connections_for_factory(discr, self, group_factory)

            discr.to_spectral_conn = to_spectral_conn
            discr.from_spectral_conn = from_spectral_conn
            discr.xlm = self.xlm if xlm is NotProvidedSentinel else xlm

        return discr

    @property
    def nspec(self):
        return sum(sgrp.nspec for sgrp in self.specgroups)

    def num_reference_derivative(
        self, ref_axes: tuple[int, ...], vec: ArrayOrContainerT
    ) -> ArrayOrContainerT:
        if self.use_spectral_derivative:
            vec = self.to_spectral_conn(vec)
            return self.spectral_num_reference_derivative(ref_axes, vec)

        from meshmode.discretization import num_reference_derivative

        return num_reference_derivative(self, ref_axes, vec)

    def spectral_num_reference_derivative(
        self, ref_axes: tuple[int, ...], ary: ArrayOrContainerT
    ) -> ArrayOrContainerT:
        """Takes derivatives by doing a roundtrip through spectral space.

        :arg ref_axes: a :class:`list` of axis indices, e.g. in 1D a list of
            ``[0, 0, 0]`` would take the third derivative of *ary*.
        """
        raise NotImplementedError

    def nodes(self, *, cached: bool = True):  # pylint: disable=arguments-differ
        if self._cached_nodes is not None:
            if not cached:
                from warnings import warn

                warn(
                    "It was requested that the computed nodes not be cached, "
                    "but a cached copy of the nodes was already present.",
                    stacklevel=2,
                )

            return self._cached_nodes

        if self.xlm is None or self.from_spectral_conn is None:
            return super().nodes(cached=cached)

        x = self._setup_actx.freeze(
            self.from_spectral_conn(self._setup_actx.thaw(self.xlm))
        )

        if cached:
            self._cached_nodes = x

        return x


def spectral_num_reference_derivative(
    discr: SpectralDiscretization,
    ref_axes: tuple[int, ...],
    ary: ArrayOrContainerT,
    *,
    force_projection: bool = True,
) -> ArrayOrContainerT:
    if not discr.use_spectral_derivative:
        raise ValueError("discretization does not support spectral derivatives")

    if force_projection:
        assert discr.to_spectral_conn is not None
        ary = discr.to_spectral_conn(ary)

    return discr.spectral_num_reference_derivative(ref_axes, ary)


# }}}
