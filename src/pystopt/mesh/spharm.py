# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

"""
.. currentmodule:: pystopt.mesh

.. autoclass:: SphericalHarmonicConnectionBatch
.. autoclass:: SphericalHarmonicConnectionElementGroup
.. autoclass:: SphericalHarmonicDiscretization

.. autofunction:: make_spharm_discretization

.. currentmodule:: pystopt.mesh.spharm

.. autofunction:: visualize_spharm_modes
"""

from collections.abc import Callable, Sequence
from dataclasses import dataclass, field
from typing import Any

import numpy as np
import numpy.linalg as la

import meshmode.discretization.poly_element as mpoly
from arraycontext import ArrayContext
from meshmode.discretization import Discretization
from meshmode.dof_array import DOFArray
from meshmode.mesh import MeshElementGroup
from pytools import memoize_in, memoize_method
from pytools.obj_array import make_obj_array

from pystopt.dof_array import DOFArrayT, SpectralDOFArray
from pystopt.mesh.spectral import (
    SpectralConnectionBatch,
    SpectralConnectionElementGroup,
    SpectralDiscretization,
)
from pystopt.paths import PathLike
from pystopt.tools import get_default_logger

logger = get_default_logger(__name__)


# {{{ connection


@dataclass(frozen=True)
class SphericalHarmonicConnectionBatch(SpectralConnectionBatch):
    itheta: slice | None = None
    theta: np.ndarray | None = None
    phi: np.ndarray | None = None


@dataclass(frozen=True)
class SphericalHarmonicConnectionElementGroup(SpectralConnectionElementGroup):
    r"""
    .. attribute:: sh

        An instance of the ``shtns`` main data structure.

    .. attribute:: theta

        The longitutinal :math:`\theta \in [-\pi/2, \pi/2]` coordinates on
        the uniform spatial grid of :attr:`sh`.

    .. attribute:: phi

        The latitudinal :math:`\phi \in [0, 2 \pi]` coordinates on
        the uniform spatial grid of :attr:`sh`.
    """

    sh: Any
    neltheta: int
    nelphi: int

    batches: Sequence[SphericalHarmonicConnectionBatch]
    noutputs: int = field(init=False)
    is_real: bool = True

    def __post_init__(self):
        object.__setattr__(self, "noutputs", np.prod(self.sh.spat_shape))

    # {{{ interface

    @property
    def dim(self):
        return 2

    @property
    def spat_shape(self):
        return (self.noutputs, 1)

    @property
    def nspec(self):
        return self.sh.nlm if self.is_real else self.sh.nlm_cplx

    def to_spectral(self, actx: ArrayContext, ary: Any) -> Any:
        assert ary.shape == self.spat_shape

        # FIXME: There should be some way to layput the data so that we do not
        # need to do this copy here? SHTns expects C contiguous data from v3.7
        ary = actx.to_numpy(ary).reshape(self.sh.spat_shape, order="F").copy()

        if self.is_real:
            ary = self.sh.analys(ary)
        else:
            ary = self.sh.analys_cplx(ary.astype(np.complex128, copy=False))

        return actx.from_numpy(ary.reshape(-1, 1))

    def from_spectral(self, actx: ArrayContext, ary: Any) -> Any:
        assert ary.shape == (self.nspec, 1)
        ary = actx.to_numpy(ary).squeeze()

        if self.is_real:
            ary = self.sh.synth(ary)
        else:
            ary = np.real(self.sh.synth_cplx(ary))

        return actx.from_numpy(ary.reshape(self.spat_shape, order="F"))

    # }}}

    # {{{ spherical harmonic grid

    @property
    @memoize_method
    def theta(self):
        return np.arccos(self.sh.cos_theta)

    @property
    @memoize_method
    def phi(self):
        return (2.0 * np.pi / self.sh.nphi) * np.arange(self.sh.nphi)

    @property
    @memoize_method
    def sin_theta(self):
        # NOTE: used to compute derivatives
        return np.sin(self.theta).reshape(-1, 1)

    # NOTE: the "jacobians" here are used to compute derivatives that match
    # the ones returned by meshmode on the reference element [-1, 1]. by the
    # chain rule, we just have that
    #
    #   df/dxi = df/dtheta dtheta/dxi
    #
    # and these compute `dtheta/dxi`, since they're both on uniform grids.

    @property
    def jac_theta(self):
        # NOTE: theta is in [d/2, pi - d/2], so we need to subtract that
        d = abs(self.theta[1] - self.theta[0])
        return (np.pi - d) / 2.0 / self.neltheta

    @property
    def jac_phi(self):
        return np.pi / self.nelphi

    @property
    def _theta_buf(self):
        @memoize_in(self.sh, (SphericalHarmonicConnectionElementGroup, "theta_buf"))
        def _buf():
            return np.empty(self.sh.spat_shape, dtype=np.float64)

        return _buf()

    @property
    def _phi_buf(self):
        @memoize_in(self.sh, (SphericalHarmonicConnectionElementGroup, "phi_buf"))
        def _buf():
            return np.empty(self.sh.spat_shape, dtype=np.float64)

        return _buf()

    # }}}


# }}}


# {{{ discretization


def _make_theta_jacobian(
    actx: ArrayContext, discr: "SphericalHarmonicDiscretization"
) -> DOFArray:
    @memoize_in(discr, (_make_theta_jacobian, "wrapper"))
    def wrapper():
        result = [None] * len(discr.groups)
        for sgrp in discr.specgroups:
            for batch in sgrp.batches:
                grp = discr.groups[batch.from_group_index]

                _, nunit_theta = grp.order
                d = abs(sgrp.theta[1] - sgrp.theta[0])
                d = (np.pi - d) / 2.0 / (sgrp.sh.nlat - 1) * nunit_theta

                jac = d * (
                    1 + actx.np.zeros((grp.nelements, grp.nunit_dofs), discr.real_dtype)
                )
                result[batch.from_group_index] = actx.freeze(jac)

        return DOFArray(None, tuple(result))

    return actx.thaw(wrapper())


class SphericalHarmonicDiscretization(SpectralDiscretization):
    @memoize_method
    def _conj_scaled_area_element(self):
        from pystopt import bind, sym

        actx = self._setup_actx

        area = bind(self, sym.area_element(self.ambient_dim, dim=self.dim))(actx)

        sin_theta = DOFArray(
            actx,
            tuple([
                actx.from_numpy(
                    np.tile(sgrp.sin_theta, sgrp.sh.nphi).reshape(
                        sgrp.spat_shape, order="F"
                    )
                )
                for sgrp in self.specgroups
            ]),
        )
        sin_theta = self.from_spectral_conn.connections[-1](sin_theta)

        area = type(area)(
            actx,
            tuple([
                ary / (sgrp.jac_theta * sgrp.jac_phi) / sin_ary
                for ary, sin_ary, sgrp in zip(
                    area, sin_theta, self.specgroups, strict=False
                )
            ]),
        )

        return actx.freeze(actx.np.conj(self.to_spectral_conn(area)))

    def spectral_num_reference_derivative(self, ref_axes, ary):
        if isinstance(ref_axes, int):
            ref_axes = [ref_axes]
        ref_axes = list(ref_axes)

        if not all(ref_axis < self.dim for ref_axis in ref_axes):
            raise ValueError("axis index too large")

        actx = ary.array_context

        def _take_derivative(igrp, ary, d):
            sgrp = self.specgroups[igrp]

            dtheta = sgrp._theta_buf
            dphi = sgrp._phi_buf
            sgrp.sh.SHsph_to_spat(actx.to_numpy(ary).squeeze(), dtheta, dphi)

            if d == 0:
                # NOTE: the theta jacobian is added later since it can be
                # discontinuous across elements
                if is_jac_constant:
                    df = sgrp.jac_theta * dtheta
                else:
                    df = dtheta
            elif d == 1:
                # NOTE: shtns computes 1/(sin(theta)) * df/dphi, so we multiply by
                # sin(theta) to get back just the partial derivative
                df = sgrp.jac_phi * dphi * sgrp.sin_theta
            else:
                raise ValueError(f"unknown axis '{d}' not in {{0, 1}}")

            return actx.from_numpy(df.reshape(sgrp.spat_shape, order="F"))

        assert len(ref_axes) == 1, ref_axes

        is_jac_constant = all(len(sgrp.batches) == 1 for sgrp in self.specgroups)
        result = DOFArray(
            actx,
            tuple(
                _take_derivative(igrp, g_ary, ref_axes[0])
                for igrp, g_ary in enumerate(ary)
            ),
        )
        result = self.from_spectral_conn.connections[-1](result)

        if ref_axes[0] == 1:  # if is_phi_derivative
            return result

        if is_jac_constant:
            return result

        return _make_theta_jacobian(actx, self) * result


# }}}


# {{{ spharm connection groups


@dataclass(frozen=True)
class _SphericalHarmonicGridInfo:
    r"""Information about the :math:`(\theta, \phi)` grid."""

    neltheta: int
    nelphi: int
    order: int
    theta_phi: np.ndarray

    surface_fn: Callable[[np.ndarray, np.ndarray], np.ndarray]
    mesh_unit_nodes: np.ndarray

    vertices: np.ndarray = field(init=False)
    vertex_indices: np.ndarray = field(init=False)

    nodes: np.ndarray = field(init=False)
    node_indices: np.ndarray = field(init=False)

    def __post_init__(self):
        vertices, vertex_indices = _make_spharm_node_indices(
            self.theta_phi[:, :: self.order, :: self.order]
        )
        nodes, node_indices = _make_spharm_node_indices(self.theta_phi)

        assert vertices.shape == (self.dim, (self.neltheta + 1) * self.nelphi)

        object.__setattr__(self, "vertices", vertices)
        object.__setattr__(self, "vertex_indices", vertex_indices)
        object.__setattr__(self, "nodes", nodes)
        object.__setattr__(self, "node_indices", node_indices)

    @property
    def dim(self):
        return 2

    @property
    def ntheta(self):
        return self.theta_phi.shape[-2]

    @property
    def nphi(self):
        return self.theta_phi.shape[-1]

    def index_range_for_group(self, i: int):
        assert i == 0
        return (0, self.nelphi), (0, self.neltheta)


def _make_spharm_node_indices(theta_phi):
    dim, ntheta, nphi = theta_phi.shape

    # construct indices
    nodes = theta_phi.reshape((dim, -1), order="F")
    node_indices = np.arange(nodes.shape[-1]).reshape((ntheta, nphi), order="F")

    # wrap around indices at phi == 2 pi
    node_indices = np.hstack([node_indices, node_indices[:, 0].reshape(-1, 1)])

    return nodes, node_indices


def _visualize_spharm_elements(fig, nodes, nelements, node_indices, *, coltype):
    from mpl_toolkits.mplot3d import Axes3D  # noqa: F401

    if coltype == "vertices":
        from mpl_toolkits.mplot3d.art3d import Poly3DCollection as ElementCollection

        kwargs = {"facecolors": (0, 0, 0, 0.25), "edgecolors": "k"}
    elif coltype == "nodes":
        from mpl_toolkits.mplot3d.art3d import Line3DCollection as ElementCollection

        kwargs = {}
    else:
        raise ValueError(f"unknown collection type: '{coltype}'")

    ax = fig.gca()
    ax.set_axis_off()
    if coltype == "nodes":
        ax.plot(nodes[0], nodes[1], nodes[2], "ko", ms=2)

    for i in range(nelements):
        ax.add_collection(
            ElementCollection([nodes[:, node_indices[i, :]].T], linewidths=1, **kwargs)
        )

    ax.set_xlim([-1.1, 1.1])
    ax.set_ylim([-1.1, 1.1])
    ax.set_zlim([-1.1, 1.1])


def _resample_group_nodes_from_equidistant(
    order: int, nodes: np.ndarray, to_unit_nodes: np.ndarray
) -> np.ndarray:
    """
    :arg meg: an element group with equidistant unit nodes.
    :returns: a new element group with unit nodes *to_unit_nodes* and a resampled
        set of :attr:`~meshmode.mesh.Mesh.nodes`.
    """
    import modepy as mp

    shape = mp.Hypercube(to_unit_nodes.shape[0])
    space = mp.space_for_shape(shape, order)

    from_unit_nodes = mp.equispaced_nodes_for_space(space, shape)
    basis = mp.basis_for_space(space, shape).functions

    resample_mat = mp.resampling_matrix(basis, to_unit_nodes, from_unit_nodes)
    return np.einsum("ij,dkj->dki", resample_mat, nodes)


def _make_spharm_groups(
    actx: ArrayContext,
    info: _SphericalHarmonicGridInfo,
    *,
    from_group_index: int,
    is_equidistant: bool,
    visualize: bool = False,
) -> tuple[MeshElementGroup, SphericalHarmonicConnectionBatch]:
    (s_phi, e_phi), (s_theta, e_theta) = info.index_range_for_group(from_group_index)

    nelements = (e_theta - s_theta) * (e_phi - s_phi)
    nunit_phi, nunit_theta = info.order, info.order
    nunit_nodes = (nunit_theta + 1) * (nunit_phi + 1)

    mesh_unit_nodes = info.mesh_unit_nodes
    assert mesh_unit_nodes.shape == (info.dim, nunit_nodes)

    grp_node_indices = np.empty((nelements, nunit_nodes), dtype=np.int64)
    grp_vertex_indices = np.empty((nelements, 2**info.dim), dtype=np.int32)

    ielement = 0
    o_theta = (nunit_phi - nunit_theta) * s_theta

    for j in range(s_phi, e_phi):
        sj = np.s_[(j * nunit_phi) : ((j + 1) * nunit_phi + 1)]

        for i in range(s_theta, e_theta):
            a = info.vertex_indices[i + 0, j + 0]
            b = info.vertex_indices[i + 0, j + 1]
            c = info.vertex_indices[i + 1, j + 0]
            d = info.vertex_indices[i + 1, j + 1]
            grp_vertex_indices[ielement, :] = (a, c, b, d)

            si = np.s_[
                (i * nunit_theta + o_theta) : ((i + 1) * nunit_theta + o_theta + 1)
            ]
            grp_node_indices[ielement, :] = info.node_indices[si, sj].ravel(order="F")

            ielement += 1

    # get element nodes and reset for periodicity
    neltheta = e_theta - s_theta
    theta_phi_nodes = info.nodes[:, grp_node_indices].copy()
    theta_phi_nodes[0, -neltheta:, -nunit_theta - 1 :] = 2.0 * np.pi

    assert nunit_phi == nunit_theta
    if not is_equidistant:
        theta_phi_nodes = _resample_group_nodes_from_equidistant(
            nunit_phi, theta_phi_nodes, mesh_unit_nodes
        )

    from meshmode.mesh import TensorProductElementGroup

    nodes = info.surface_fn(theta_phi_nodes[1], theta_phi_nodes[0])
    group = TensorProductElementGroup.make_group(
        nunit_phi,
        vertex_indices=grp_vertex_indices,
        nodes=nodes,
        dim=info.dim,
        unit_nodes=mesh_unit_nodes,
    )

    itheta = np.s_[
        (s_theta * nunit_theta + o_theta) : (e_theta * nunit_theta + o_theta + 1)
    ]
    specbatch = SphericalHarmonicConnectionBatch(
        itheta=itheta,
        from_group_index=from_group_index,
        node_indices=actx.freeze(actx.from_numpy(grp_node_indices)),
        theta=actx.freeze(actx.from_numpy(theta_phi_nodes[1])),
        phi=actx.freeze(actx.from_numpy(theta_phi_nodes[0])),
    )

    if visualize:
        from pystopt.visualization.matplotlib import subplots

        with subplots(
            f"_make_spharm_mesh_theta_phi_{from_group_index}", overwrite=True
        ) as fig:
            ax = fig.gca()  # pylint: disable=no-member

            ax.plot(theta_phi_nodes[0].ravel(), theta_phi_nodes[1].ravel(), "ko-")
            ax.plot(info.nodes[0].ravel(), info.nodes[1].ravel(), "v")
            ax.plot(info.vertices[0].ravel(), info.vertices[1].ravel(), "o")
            ax.set_ylabel(r"$\theta$")
            ax.set_xlabel(r"$\phi$")
            ax.margins(0.05, 0.05)

    if visualize:
        with subplots(
            f"_make_spharm_mesh_surface_{from_group_index}",
            projection="3d",
            overwrite=True,
        ) as fig:
            vertices = info.surface_fn(info.vertices[1], info.vertices[0])
            _visualize_spharm_elements(
                fig,
                vertices,
                ielement,
                grp_vertex_indices[:, [0, 1, 3, 2]],
                coltype="vertices",
            )

            equi_nodes = info.surface_fn(info.nodes[1], info.nodes[0])
            _visualize_spharm_elements(
                fig, equi_nodes, e_phi - s_phi, grp_node_indices, coltype="nodes"
            )

    return group, specbatch


# }}}


# {{{ make spharm discretization


def _make_shtns(
    lmax: int, mmax: int, ntheta: int, nphi: int, *, polar_opt: float = 1.0e-10
) -> Any:
    import shtns

    # NOTE: `2*pi/mres` is the periodity in phi, which should always be `2*pi`
    sh = shtns.sht(lmax=int(lmax), mmax=int(mmax), mres=1, norm=shtns.sht_orthonormal)

    # NOTE: this uses the Condon-Shortley phase to match scipy conventions,
    #   disabling requires explicitly adding shtns.SHT_NO_CS_PHASE
    # NOTE: cannot set SHT_SCALAR_ONLY, even if we just use scalars because
    #   taking derivatives requires a vector function `SHsph_to_spat`
    sh.set_grid(
        int(ntheta),
        int(nphi),
        flags=(
            shtns.sht_reg_fast | shtns.SHT_PHI_CONTIGUOUS
            # | shtns.SHT_NO_CS_PHASE
            # | shtns.SHT_SCALAR_ONLY
        ),
        polar_opt=polar_opt,
    )

    return sh


def make_spharm_discretization(
    actx: ArrayContext,
    nellon: int,
    nellat: int,
    *,
    surface_fn: Callable[[np.ndarray, np.ndarray], np.ndarray],
    order: int,
    lmax: int | None = None,
    mmax: int | None = None,
    polar_opt: float = 1.0e-10,
    stol: float = 1.0e-10,
    group_factory: mpoly.ElementGroupFactory | None = None,
    mesh_unit_nodes: bool | np.ndarray = False,
    use_spectral_derivative: bool = True,
    visualize: bool = False,
) -> Discretization:
    r"""
    :arg nellon: (approximate) number of elements in the
        longitudinal direction with :math:`\phi \in [0, 2 \pi)`.
    :arg nellat: (approximate) number of elements in the
        latitudinal direction with :math:`\theta \in [0, \pi)`.

    :arg surface_fn: a :class:`~collections.abc.Callable` that takes
        :math:`(\theta, \phi)` and returns and array of shape ``(3, ...)``.
    :arg order: desired order of the discretization.
    :arg mesh_unit_nodes: if this is *True* then the unit nodes
        of the provided *group_factory* are used. If *False*, equidistant
        nodes are unused. Otherwise, if a specific set of unit nodes is provided,
        it is used as is.

    :arg lmax: maximum spherical harmonic degree.
    :arg mmax: maximum spherical harmonic order.
    :arg polar_opt: tolerance for singularities near poles.
    :arg stol: if in debug mode, this tolerance is used to check how well
        the spherical harmonics approximate the surface by doing a roundtrip.
    """

    if group_factory is None:
        group_factory = mpoly.GaussLegendreTensorProductGroupFactory(order=order)

    # {{{ determine number of nodes in each direction

    # on an equidistant grid with M elenents and P + 1 nodes on each element,
    # we have a total of
    #   M * P + 1
    # nodes. for the spharm code, we need to take periodicity and the poles into
    # account.

    # 1. phi in [0, 2 pi] direction, we have a total of `nellon * order`
    #    points, since the last one at `2 pi` is periodic and equal to `phi = 0`
    nphi = nellon * order

    # 2. theta in [0, pi] direction, we have the full `nellon * order + 1`
    #    points, since the grid is actually on `[dt / 2, pi - dt / 2]`
    if nellat % 2 == 0:
        nellat += 1
    ntheta = nellat * order + 1

    # }}}

    # {{{ construct shtns object

    if lmax is None:
        lmax = ntheta // 2 - 3

    if mmax is None:
        mmax = min(nphi // 2 - 2, lmax)

    # create shtns objects
    try:
        sh = _make_shtns(lmax, mmax, ntheta, nphi, polar_opt=polar_opt)
    except ImportError as exc:
        raise ImportError(
            "discretizations based on spherical harmonics require the 'shtns' library."
        ) from exc

    # }}}

    # {{{ construct mesh nodes

    mesh_group_factory = group_factory
    is_equidistant = isinstance(
        group_factory, mpoly.InterpolatoryEquidistantGroupFactory
    )

    # NOTE: this is for consistency with `make_fourier_discretization`
    if mesh_unit_nodes is None:
        mesh_unit_nodes = False

    if isinstance(mesh_unit_nodes, bool):
        from pystopt.mesh.poly_element import get_unit_nodes_for_group_factory

        if not mesh_unit_nodes:
            mesh_group_factory = mpoly.InterpolatoryEquidistantGroupFactory(order=order)
            is_equidistant = True

        mesh_unit_nodes = get_unit_nodes_for_group_factory(
            2, mesh_group_factory, order=order
        )

    # }}}

    # {{{ construct theta-phi grid

    # get vertex coordinates
    theta = np.arccos(sh.cos_theta)
    phi = (2.0 * np.pi / nphi) * np.arange(nphi)
    assert theta.size == sh.nlat
    assert sh.nlat == ntheta
    assert phi.size == sh.nphi
    assert sh.nphi == nphi

    theta_phi = np.stack(np.meshgrid(phi, theta, copy=False))
    assert theta_phi.shape == (2, *sh.spat_shape)

    # }}}

    # {{{ construct groups

    info = _SphericalHarmonicGridInfo(
        neltheta=nellat,
        nelphi=nellon,
        order=order,
        theta_phi=theta_phi,
        surface_fn=surface_fn,
        mesh_unit_nodes=mesh_unit_nodes,
    )

    grp, specbatch = _make_spharm_groups(
        actx,
        info,
        from_group_index=0,
        is_equidistant=is_equidistant,
        visualize=visualize,
    )
    assert grp.nelements == info.neltheta * info.nelphi

    vertices = info.surface_fn(info.vertices[1], info.vertices[0])
    specgroup = SphericalHarmonicConnectionElementGroup(
        sh=sh, batches=[specbatch], neltheta=info.neltheta, nelphi=info.nelphi
    )

    # }}}

    # {{{ construct discretization

    from meshmode.mesh import make_mesh

    mesh = make_mesh(
        vertices=vertices,
        groups=[grp],
        is_conforming=True,
        # NOTE: skip tests if the mesh_unit_nodes are not equidistant
        # (interpolating using the quadrature nodes will give large errors)
        skip_tests=not is_equidistant,
    )

    nodes = surface_fn(theta_phi[1], theta_phi[0])
    assert nodes.shape == (3, sh.nlat, sh.nphi)

    xlm = make_obj_array([
        SpectralDOFArray(
            None,
            (actx.freeze(actx.from_numpy(sh.analys(x).reshape(-1, 1))),),  # pylint: disable=no-member
        )
        for x in nodes
    ])

    discr = SphericalHarmonicDiscretization(
        actx,
        mesh,
        [specgroup],
        group_factory=group_factory,
        use_spectral_derivative=use_spectral_derivative,
        _xlm=xlm,
    )

    # }}}

    if __debug__:
        error = max(la.norm(x - sh.synth(sh.analys(x))) / la.norm(x) for x in nodes)
        if error > stol:
            from warnings import warn

            # fmt: off
            warn(
                "spherical harmonic expansion not sufficient to approximate "
                f"surface, try using a larger 'lmax' (roundtrip error {error:.5e})",
                UserWarning,
                stacklevel=2,
            )
            # fmt: on

    return discr, xlm


# }}}


# {{{ evaluate spherical harmonics


def evaluate_spharm_from_index(
    actx: ArrayContext, discr: SphericalHarmonicDiscretization, m: int, n: int
) -> DOFArray:
    """Evaluate the spherical harmonic :math:`Y^m_n` on the given discretization.

    If the discretization has multiple connected components, then the same
    spherical harmonic will be evaluated on each one, based on the
    :attr:`~pystopt.mesh.SpectralDiscretization.specgroups`.

    :arg m: order of the spherical harmonic.
    :arg n: degree of the spherical harmonic.
    """
    result = []
    for grp in discr.specgroups:
        if not grp.is_real:
            raise RuntimeError("only real transforms are supported")

        cmn = actx.np.zeros(grp.sh.nlm, dtype=np.complex128)
        cmn[grp.sh.idx(n, m)] = 1.0 if m == 0 else 0.5

        result.append(cmn.reshape(-1, 1))

    return discr.from_spectral_conn(SpectralDOFArray(actx, tuple(result)))


def _evaluate_spharm_func_from_coordinates(
    actx: ArrayContext,
    discr: SphericalHarmonicDiscretization,
    func: Callable[[Any, Any], Any],
) -> DOFArray:
    from pystopt.mesh.connection import (
        from_mesh_nodes_interp_matrix,
        resample_by_mat_kernel,
    )

    prg = resample_by_mat_kernel(actx)

    result = [None] * len(discr.groups)
    for sgrp in discr.specgroups:
        for batch in sgrp.batches:
            mat = from_mesh_nodes_interp_matrix(discr, batch.from_group_index)

            theta = actx.call_loopy(prg, ary=batch.theta, resampling_mat=mat)["result"]
            phi = actx.call_loopy(prg, ary=batch.phi, resampling_mat=mat)["result"]

            result[batch.from_group_index] = func(phi, theta)

    return DOFArray(actx, tuple(result))


def evaluate_spharm_from_coordinates(
    actx: ArrayContext, discr: SphericalHarmonicDiscretization, m: int, n: int
) -> DOFArray:
    """Evaluate the spherical harmonic :math:`Y^m_n` at the given points.

    This method uses :mod:`scipy` to evaluate the spherical harmonics, so it
    does an expensive roundtrip through the host memory. The performant
    version is :func:`evaluate_spharm_from_index`.

    :arg m: order of the spherical harmonic.
    :arg n: degree of the spherical harmonic.
    """

    def func(phi, theta):
        from scipy.special import sph_harm  # pylint: disable=no-name-in-module

        ymn = sph_harm(m, n, actx.to_numpy(phi), actx.to_numpy(theta)).real.copy()
        return actx.from_numpy(ymn)

    return _evaluate_spharm_func_from_coordinates(actx, discr, func)


# }}}


# {{{ visualize modes


def _make_spharm_group_array(sgrp):
    @memoize_in(sgrp.sh, (_make_spharm_group_array, sgrp.is_real, "array"))
    def prg():
        sh = sgrp.sh
        if sgrp.is_real:
            shape = (sh.lmax + 1, sh.mmax + 1)
        else:
            shape = (sh.lmax + 1, 2 * sh.mmax + 1)

        # NOTE: array is always real because we just plot real / imag / abs
        nlm = np.full(shape, np.nan, dtype=np.float64)

        return nlm

    return prg()


def _make_spharm_group_indices(sgrp):
    @memoize_in(sgrp.sh, (_make_spharm_group_indices, sgrp.is_real, "indices"))
    def prg():
        sh = sgrp.sh
        if sgrp.is_real:
            n, m = sh.l, sh.m
        else:
            n, m = sh.zlm(np.arange(sh.nlm_cplx))
            m += sh.mmax

        return n, m

    return prg()


def _add_labels_to_axis(ax, title, xlabel, ylabel, *, is_first, is_last, layout):
    ax.set_title(title)

    if layout == "horizontal":
        ax.set_xlabel(f"${xlabel}$")
        if is_first:
            ax.set_ylabel(f"${ylabel}$")
    else:
        if is_last:
            ax.set_xlabel(f"${xlabel}$")
        ax.set_ylabel(f"${ylabel}$")


def _visualize_spharm_modes_imshow(
    fig, discr, names_and_fields, *, semilogy, layout, colorbar
):
    # FIXME: does this even work for vertical ones?
    assert layout == "horizontal"

    from pystopt.visualization.matplotlib import preprocess_latex

    axes = fig.axes
    iaxis = 0
    for name, ary in names_and_fields:
        for jaxis, (sgrp, subary) in enumerate(
            zip(discr.specgroups, ary, strict=False)
        ):
            nlm = _make_spharm_group_array(sgrp)
            n, m = _make_spharm_group_indices(sgrp)
            nlm[n, m] = subary
            if semilogy:
                nlm = np.log10(np.abs(nlm) + 1.0e-16)

            # {{{ plot

            ax = axes[iaxis + jaxis]
            im = ax.imshow(nlm, origin="lower")
            _add_labels_to_axis(
                ax,
                preprocess_latex(f"({name})_{{{jaxis}}}"),
                "m",
                "n",
                is_first=(jaxis == 0),
                is_last=(jaxis == len(ary) - 1),
                layout=layout,
            )

            if colorbar:
                fig.colorbar(im, ax=ax, shrink=0.75)

            # }}}

        iaxis += len(ary)


def _visualize_spharm_modes_by_index(
    fig, discr, names_and_fields, *, mode, semilogy, layout, max_degree_or_order
):
    assert len(discr.specgroups) == 1

    for ifield, (ax, (name, xlm)) in enumerate(
        zip(fig.axes, names_and_fields, strict=False)
    ):
        (sgrp,) = discr.specgroups

        nlm = _make_spharm_group_array(sgrp)
        n, m = _make_spharm_group_indices(sgrp)
        nlm[n, m] = xlm
        if semilogy:
            nlm = np.log10(np.abs(nlm) + 1.0e-16)

        lmax = sgrp.sh.lmax
        if sgrp.is_real:
            mmin, mmax = 0, sgrp.sh.mmax
        else:
            mmin, mmax = -sgrp.sh.mmax, sgrp.sh.mmax

        # {{{ plot

        g_max_degree_or_order = max_degree_or_order
        if mode == "byorder":
            if g_max_degree_or_order is None:
                g_max_degree_or_order = sgrp.sh.mmax

            xlabel = "n"
            n = np.arange(0, lmax + 1)
            mmin = -min(-mmin, g_max_degree_or_order)
            mmax = min(mmax, g_max_degree_or_order)

            for m in range(mmin, mmax + 1):
                ax.plot(n, nlm[:, m])
        else:
            if g_max_degree_or_order is None:
                g_max_degree_or_order = sgrp.sh.lmax

            xlabel = "m"
            m = np.arange(mmin, mmax + 1)

            for n in range(min(lmax, g_max_degree_or_order) + 1):
                ax.plot(m, nlm[n, :])

        _add_labels_to_axis(
            ax,
            name,
            xlabel,
            name,
            is_first=(ifield == 0),
            is_last=(ifield == len(names_and_fields) - 1),
            layout=layout,
        )

        if semilogy:
            ax.set_ylim([-16, 0])

        # }}}


def visualize_spharm_modes(
    fig_or_filename: PathLike,
    discr: SphericalHarmonicDiscretization,
    names_and_fields: Sequence[tuple[str, DOFArrayT]],
    *,
    semilogy: bool = True,
    mode: str = "imshow",
    component: str = "abs",
    layout: str = "horizontal",
    max_degree_or_order: int | None = None,
    overwrite: bool = False,
) -> None:
    r"""Visualize spherical harmonic modes of a given set of arrays.

    The *mode* can be one of:

    * ``"imshow"``, which plots all the modes using ``imshow``.
    * ``"byorder"``, which uses line plots to plot :math:`f^m_{\cdot}` for each
      order :math:`m`, i.e. a vertical slice through ``imshow``.
    * ``"bydegree"``, which uses line plots to plot :math:`f^{\cdot}_n` for
      each degree :math:`n`, i.e. a horizontal slice through ``imshow``.

    :arg mode: type of the plot to use in the visualization.
    :arg component: can be one of ``"real"``, ``"imag"`` or ``"abs"``, to
        visualize the real, imaginary or absolute value of each component.
    :arg layout: can be one of ``"horizontal"`` or ``"vertical"`` and is
        used to determine how to stack the multiple arrays in
        *names_and_fields*.
    :arg max_degree_or_order: a cutoff used when *mode* is ``"bydegree"``
        or ``"byorder"`` to determine the maximum value to plot.
    """
    # {{{ validate input

    if discr.ambient_dim != 3:
        raise ValueError(
            f"only 3d geometries are supported; got a {discr.ambient_dim}d geometry"
        )

    if mode not in {"imshow", "bydegree", "byorder"}:
        raise ValueError(f"unknown mode: '{mode}'")

    if component not in {"real", "imag", "abs"}:
        raise ValueError(f"unknown component: '{component}'")

    if layout not in {"horizontal", "vertical"}:
        raise ValueError(f"unknown layout: '{layout}'")

    # }}}

    # {{{ preprocess

    def preprocess_field(name, ary):
        if isinstance(ary, SpectralDOFArray):
            pass
        elif isinstance(ary, DOFArray):
            ary = discr.to_spectral_conn(ary)
        else:
            raise TypeError(
                f"unsupported field type for '{name}': '{type(ary).__name__}'"
            )

        actx = ary.array_context
        if component == "real":
            ary = actx.np.real(ary)
        elif component == "imag":
            ary = actx.np.imag(ary)
        else:
            ary = actx.np.abs(ary)

        assert len(ary) == len(discr.specgroups)
        return SpectralDOFArray(
            None, tuple([actx.to_numpy(subary).ravel() for subary in ary])
        )

    names_and_fields = [
        (name, preprocess_field(name, field)) for name, field in names_and_fields
    ]

    if layout == "horizontal":
        nrows, ncols = len(names_and_fields), len(discr.specgroups)
    else:
        nrows, ncols = len(discr.specgroups), len(names_and_fields)

    kwargs = {"overwrite": overwrite, "nrows": nrows, "ncols": ncols}

    import matplotlib.pyplot as mp

    if isinstance(fig_or_filename, mp.Figure) and len(fig_or_filename.axes) != (
        nrows * ncols
    ):
        raise RuntimeError(
            f"expected figure with {nrows * ncols} axes, "
            f"got {len(fig_or_filename.axes)}"
        )

    # }}}

    # {{{ plot

    from pystopt.visualization.matplotlib import subplots

    with subplots(fig_or_filename, **kwargs) as fig:
        if mode == "imshow":
            _visualize_spharm_modes_imshow(
                fig,
                discr,
                names_and_fields,
                semilogy=semilogy,
                layout=layout,
                colorbar=True,
            )
        elif mode in {"bydegree", "byorder"}:
            _visualize_spharm_modes_by_index(
                fig,
                discr,
                names_and_fields,
                mode=mode,
                semilogy=semilogy,
                layout=layout,
                max_degree_or_order=max_degree_or_order,
            )
        else:
            # NOTE: it shouldn't get here because we already checked it
            raise AssertionError(f"unknown mode: '{mode}'")

    # NOTE: for some reason matplotlib seems to be allocating a lot of memory
    # (even though the figures are closed?) and this seems to help with that
    import gc

    gc.collect()

    # }}}


# }}}
