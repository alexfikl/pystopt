# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

"""
.. autofunction:: to_mesh_nodes_interp_matrix
.. autofunction:: from_mesh_nodes_interp_matrix
.. autofunction:: to_mesh_vertices_interp_matrix
.. autofunction:: from_mesh_vertices_interp_matrix
.. autofunction:: to_equidistant_interp_matrix
.. autofunction:: from_equidistant_interp_matrix

.. autofunction:: resample_group_nodes_from_equidistant
.. autofunction:: equidistant_tensor_product_nodes

.. autofunction:: get_unit_nodes_for_group_factory
.. autoclass:: GivenGroupsGroupFactory
.. autoclass:: LegendreTensorProductMeshGroupFactory
"""

from collections.abc import Sequence
from typing import TypeVar

import modepy as mp
import numpy as np

import meshmode.discretization.poly_element as mpoly
from meshmode.discretization import ElementGroupFactory, NodalElementGroupBase
from meshmode.mesh import (
    MeshElementGroup,
    SimplexElementGroup,
    TensorProductElementGroup,
)

from pystopt.tools import get_default_logger

logger = get_default_logger(__name__)


MeshElementGroupT = TypeVar("MeshElementGroupT", bound=MeshElementGroup)


# {{{ matrices


def to_mesh_nodes_interp_matrix(grp: NodalElementGroupBase) -> np.ndarray:
    """
    :returns: interpolation matrix from the group unit nodes to the
        mesh element unit nodes.
    """
    from meshmode.discretization.poly_element import to_mesh_interp_matrix

    return to_mesh_interp_matrix(grp)


def from_mesh_nodes_interp_matrix(grp: NodalElementGroupBase) -> np.ndarray:
    """
    :returns: interpolation matrix from the mesh element unit nodes to the
        group unit nodes.
    """
    from meshmode.discretization.poly_element import from_mesh_interp_matrix

    return from_mesh_interp_matrix(grp)


def to_mesh_vertices_interp_matrix(grp: NodalElementGroupBase) -> np.ndarray:
    """
    :returns: interpolation matrix from the group unit nodes to the
        mesh vertices.
    """
    meg = grp.mesh_el_group
    return mp.resampling_matrix(
        grp.basis_obj().functions, meg.vertex_unit_coordinates().T, grp.unit_nodes
    )


def from_mesh_vertices_interp_matrix(grp: NodalElementGroupBase) -> np.ndarray:
    """
    :returns: interpolation matrix from the group vertices to the
        reference nodes.
    """
    from meshmode.mesh import ModepyElementGroup

    assert isinstance(grp.mesh_el_group, ModepyElementGroup)

    meg = grp.mesh_el_group
    space = mp.space_for_shape(meg.shape, 1)
    assert type(space) is type(meg.space)

    basis = mp.basis_for_space(space, meg.shape).functions

    return mp.resampling_matrix(
        # NOTE: this is just linear interpolation
        basis,
        grp.unit_nodes,
        meg.vertex_unit_coordinates().T,
    )


def to_equidistant_interp_matrix(grp: NodalElementGroupBase) -> np.ndarray:
    """
    :returns: an interpolation matrix from the group nodes to equidistant
        unit nodes.
    """
    from meshmode.mesh import ModepyElementGroup

    assert isinstance(grp.mesh_el_group, ModepyElementGroup)

    meg = grp.mesh_el_group
    unit_nodes = mp.equispaced_nodes_for_space(meg.space, meg.shape)

    return mp.resampling_matrix(grp.basis_obj().functions, unit_nodes, grp.unit_nodes)


def from_equidistant_interp_matrix(grp: NodalElementGroupBase) -> np.ndarray:
    """
    :returns: an interpolation matrix from the group nodes to equidistant
        unit nodes.
    """
    from meshmode.mesh import ModepyElementGroup

    assert isinstance(grp.mesh_el_group, ModepyElementGroup)

    meg = grp.mesh_el_group
    unit_nodes = mp.equispaced_nodes_for_space(meg.space, meg.shape)
    basis = mp.basis_for_space(meg.space, meg.shape).functions

    return mp.resampling_matrix(basis, grp.unit_nodes, unit_nodes)


# }}}


# {{{ nodes


def equidistant_tensor_product_nodes(
    dim: int, order: int | tuple[int, ...]
) -> np.ndarray:
    from numbers import Number

    if isinstance(order, Number):
        order = (order,) * dim
    else:
        assert len(order) == dim

    return mp.tensor_product_nodes([mp.equidistant_nodes(1, n)[0] for n in order])


# }}}


# {{{ element groups


def get_unit_nodes_for_group_factory(
    dim: int,
    group_factory: mpoly.ElementGroupFactory,
    mesh_el_group_cls: type[MeshElementGroup] | None = None,
    order: int | None = None,
) -> np.ndarray:
    if mesh_el_group_cls is None and isinstance(
        group_factory, mpoly.HomogeneousOrderBasedGroupFactory
    ):
        mesh_el_group_cls = group_factory.mesh_group_class

    if mesh_el_group_cls is None:
        if dim == 1:
            mesh_el_group_cls = SimplexElementGroup
        elif dim == 2:
            mesh_el_group_cls = TensorProductElementGroup
        else:
            raise ValueError(f"unsupported dimension: '{dim}'")

    if order is None:
        order = group_factory.order

    if order is None:
        raise ValueError("must provide 'order'")

    meg = mesh_el_group_cls.make_group(order, vertex_indices=None, nodes=None, dim=dim)
    grp = group_factory(meg)

    return grp.unit_nodes


# }}}


# {{{ element factories


class GivenGroupsGroupFactory(ElementGroupFactory):
    """Recreates the given groups for a new discretization.

    .. automethod:: __init__
    """

    def __init__(self, groups: Sequence[NodalElementGroupBase]) -> None:
        """
        :arg groups: a :class:`list` of
            :class:`~meshmode.discretization.ElementGroupBase`.
        """
        super().__init__()
        self.groups = list(groups)

    def __call__(self, mesh_el_group: MeshElementGroup) -> NodalElementGroupBase:
        grp = self.groups.pop(0)

        if isinstance(grp, mpoly.PolynomialRecursiveNodesElementGroup):
            return type(grp)(mesh_el_group, grp.order, grp.family)

        return type(grp)(mesh_el_group, grp.order)


class FromMeshNodesGroupFactory(ElementGroupFactory):
    """A group factory for discretization element groups that matches the
    unit nodes and order of the underlying mesh.
    """

    def __init__(self, order: int | None = None) -> None:
        super().__init__()
        self.order = order

    def __call__(self, mesh_el_group: MeshElementGroup) -> NodalElementGroupBase:
        order = mesh_el_group.order if self.order is None else self.order

        if isinstance(mesh_el_group, SimplexElementGroup):
            return mpoly.PolynomialGivenNodesElementGroup(
                mesh_el_group,
                order,
                unit_nodes=mesh_el_group.unit_nodes,
            )
        elif isinstance(mesh_el_group, TensorProductElementGroup):
            return mpoly.LegendreTensorProductElementGroup(
                mesh_el_group, order, unit_nodes=mesh_el_group.unit_nodes
            )
        else:
            raise TypeError(f"unsupported element group: '{type(mesh_el_group)}'")


# }}}
