# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

from pystopt.mesh.connection import (
    FromUniqueDOFConnection,
    ToUniqueDOFConnection,
    UniqueConnectionBatch,
    UniqueConnectionElementGroup,
    UniqueConnectionInterpolationBatch,
    UniqueDOFConnectionBase,
    make_from_mesh_connection,
    make_from_unique_mesh_vertex_connection,
    make_same_layout_connection,
    make_to_mesh_connection,
    make_to_unique_mesh_vertex_connection,
    make_unique_mesh_vertex_connections,
)
from pystopt.mesh.fourier import (
    FourierConnectionBatch,
    FourierConnectionElementGroup,
    FourierDiscretization,
    find_modes_for_tolerance,
    make_fourier_discretization,
    visualize_fourier_modes,
)
from pystopt.mesh.generation import (
    FourierGenerator,
    MeshGenerator,
    SPHARMGenerator,
    generate_discretization,
    get_mesh_generator_from_name,
)
from pystopt.mesh.processing import (
    generate_random_discr_array,
    generate_uniform_discr_array,
    merge_disjoint_spectral_discretizations,
    update_discr_from_nodes,
    update_discr_from_spectral,
    update_discr_mesh_from_nodes,
)
from pystopt.mesh.spectral import (
    FromSpectralGridConnection,
    SpectralConnectionBatch,
    SpectralConnectionElementGroup,
    SpectralDiscretization,
    SpectralGridConnection,
    ToSpectralGridConnection,
    make_spectral_connections,
    update_spectral_connections_for_factory,
)
from pystopt.mesh.spharm import (
    SphericalHarmonicConnectionBatch,
    SphericalHarmonicConnectionElementGroup,
    SphericalHarmonicDiscretization,
    make_spharm_discretization,
)


def check_discr_same_connectivity(discr, other):
    if len(discr.groups) != len(other.groups):
        return False

    return all(
        sg.discretization_key() == og.discretization_key()
        and sg.nelements == og.nelements
        for sg, og in zip(discr.groups, other.groups, strict=False)
    )


__all__ = (
    "FourierConnectionBatch",
    "FourierConnectionElementGroup",
    "FourierDiscretization",
    "FourierGenerator",
    "FromSpectralGridConnection",
    "FromUniqueDOFConnection",
    "MeshGenerator",
    "SPHARMGenerator",
    "SpectralConnectionBatch",
    "SpectralConnectionElementGroup",
    "SpectralDiscretization",
    "SpectralGridConnection",
    "SphericalHarmonicConnectionBatch",
    "SphericalHarmonicConnectionElementGroup",
    "SphericalHarmonicDiscretization",
    "ToSpectralGridConnection",
    "ToUniqueDOFConnection",
    "UniqueConnectionBatch",
    "UniqueConnectionElementGroup",
    "UniqueConnectionInterpolationBatch",
    "UniqueDOFConnectionBase",
    "check_discr_same_connectivity",
    "find_modes_for_tolerance",
    "generate_discretization",
    "generate_random_discr_array",
    "generate_uniform_discr_array",
    "get_mesh_generator_from_name",
    "make_fourier_discretization",
    "make_from_mesh_connection",
    "make_from_unique_mesh_vertex_connection",
    "make_same_layout_connection",
    "make_spectral_connections",
    "make_spharm_discretization",
    "make_to_mesh_connection",
    "make_to_unique_mesh_vertex_connection",
    "make_unique_mesh_vertex_connections",
    "merge_disjoint_spectral_discretizations",
    "update_discr_from_nodes",
    "update_discr_from_spectral",
    "update_discr_mesh_from_nodes",
    "update_spectral_connections_for_factory",
    "visualize_fourier_modes",
)
