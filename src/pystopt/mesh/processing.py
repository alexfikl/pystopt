# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

"""
.. currentmodule:: pystopt.mesh

.. autofunction:: update_discr_mesh_from_nodes
.. autofunction:: update_discr_from_nodes
.. autofunction:: update_discr_from_spectral

.. autofunction:: merge_disjoint_spectral_discretizations
.. autofunction:: generate_random_discr_array
.. autofunction:: generate_uniform_discr_array
"""

from typing import Any

import numpy as np

from arraycontext import ArrayContext
from meshmode.discretization.poly_element import ElementGroupFactory
from meshmode.mesh import Mesh
from pytential import GeometryCollection
from pytools import keyed_memoize_in, log_process

from pystopt import bind, sym
from pystopt.mesh.generation import MeshGenerator, generate_discretization
from pystopt.mesh.spectral import SpectralDiscretization
from pystopt.tools import get_default_logger

logger = get_default_logger(__name__)


# {{{ duplicate mesh vertices


def duplicate_mesh_vertices(mesh: Mesh) -> np.ndarray:
    """
    :returns: a vertex array where each point is unique.
    """

    nvertices = sum(grp.vertex_indices.size for grp in mesh.groups)
    vertices = np.empty((mesh.ambient_dim, nvertices), dtype=mesh.vertices.dtype)

    vertex_nr_base = 0
    for grp in mesh.groups:
        nvertices = grp.vertex_indices.size
        vertices[:, vertex_nr_base : vertex_nr_base + nvertices] = mesh.vertices[
            :, grp.vertex_indices
        ].reshape(mesh.ambient_dim, -1)

        vertex_nr_base += nvertices

    return vertices


# }}}


# {{{ reconstruct mesh from discretization nodes


@log_process(logger)
def update_discr_mesh_from_nodes(
    actx: ArrayContext,
    discr: SpectralDiscretization,
    nodes: np.ndarray,
    *,
    keep_vertices: bool = True,
    atol: float | None = None,
) -> Mesh:
    """
    :arg discr: previous :class:`~pystopt.mesh.SpectralDiscretization`.
    :arg nodes: a node array with the same number of DOFs as the one
        in *discr*.
    :arg keep_vertices: if *True*, the nodes are also used to
        reconstruct the vertices. As the discretization is discontinuous,
        vertices are averaged over all elements to obtain a unique value.
    :arg atol: a floating point tolerance used to check if the unit nodes
        of the discretization groups are the same as the unit nodes of the
        mesh element groups.

    :returns: a :class:`~meshmode.mesh.Mesh` with the same structure
        as *discr.mesh*, but with nodes interpolated from *nodes*.
    """

    if nodes.shape != (discr.ambient_dim,):
        raise ValueError("invalid shape of incoming data")

    if nodes[0].shape != (len(discr.groups),):
        raise ValueError("invalid shape of incoming data")

    if atol is None:
        atol = 10 * np.finfo(nodes[0].entry_dtype).eps

    # {{{ project vertices

    if keep_vertices:
        from pystopt.mesh import make_to_unique_mesh_vertex_connection

        to_vertex = make_to_unique_mesh_vertex_connection(discr)

        from arraycontext import flatten

        vertices = actx.to_numpy(flatten(to_vertex(nodes), actx)).reshape(
            nodes.size, -1
        )
    else:
        vertices = None

    # }}}

    # {{{ project nodes

    # FIXME: this memoize is ambiguous because the underlying mesh group
    # unit nodes can change while the key doesn't catch that at all

    @keyed_memoize_in(
        actx,
        (update_discr_mesh_from_nodes, "to_mesh_interp_matrix"),
        lambda ggrp: ggrp.discretization_key(),
    )
    def to_mesh_interp_matrix(ggrp) -> np.ndarray:
        import modepy as mp

        mat = mp.resampling_matrix(
            ggrp.basis_obj().functions,
            ggrp.mesh_el_group.unit_nodes,
            ggrp.unit_nodes,
        )

        return actx.freeze(actx.from_numpy(mat))

    def resample_nodes_to_mesh(grp, discr_nodes):
        grp_unit_nodes = grp.unit_nodes.reshape(-1)
        meg_unit_nodes = grp.mesh_el_group.unit_nodes.reshape(-1)

        if (
            grp_unit_nodes.shape == meg_unit_nodes.shape
            and np.linalg.norm(grp_unit_nodes - meg_unit_nodes) < atol
        ):
            return discr_nodes

        from pystopt.mesh.connection import resample_by_mat_kernel

        return actx.call_loopy(
            resample_by_mat_kernel(actx),
            ary=discr_nodes,
            resampling_mat=to_mesh_interp_matrix(grp),
        )["result"]

    megs = []
    for igrp, grp in enumerate(discr.groups):
        mesh_nodes = np.stack([
            actx.to_numpy(resample_nodes_to_mesh(grp, nodes[iaxis][igrp]))
            for iaxis in range(discr.ambient_dim)
        ])

        if keep_vertices:
            vertex_indices = grp.mesh_el_group.vertex_indices
        else:
            vertex_indices = None

        from dataclasses import replace

        meg = replace(
            grp.mesh_el_group,
            vertex_indices=vertex_indices,
            nodes=mesh_nodes,
        )
        megs.append(meg)

    # }}}

    return discr.mesh.copy(vertices=vertices, groups=megs, skip_tests=True)


# }}}


# {{{ reconstruct discretization from nodes


def update_discr_from_nodes(
    actx: ArrayContext,
    discr: SpectralDiscretization,
    nodes: np.ndarray,
    *,
    group_factory: ElementGroupFactory | None = None,
    keep_vertices: bool = False,
) -> SpectralDiscretization:
    """
    :arg discr: previous :class:`~meshmode.discretization.Discretization`.
    :arg nodes: a node array with the same number of DOFs as the one
        in *discr*.

    :arg group_factory: a group factory used to construct the new
        discretization (should match the one used to construct *discr*
        in most cases). A default is provided if *discr* has a single
        group from :mod:`meshmode.discretization.poly_element`.

    :returns: a :class:`~meshmode.discretization.Discretization` with the
        same structure (i.e. number of groups, DOFs, etc) as *discr*, but
        with nodes interpolated from *nodes*.
    """
    mesh = update_discr_mesh_from_nodes(actx, discr, nodes, keep_vertices=keep_vertices)

    if discr.xlm is not None:
        xlm = actx.freeze(discr.to_spectral_conn(nodes))
    else:
        xlm = None

    return discr.copy(mesh=mesh, xlm=xlm, group_factory=group_factory)


def update_discr_from_spectral(
    actx: ArrayContext,
    discr: SpectralDiscretization,
    xlm: np.ndarray,
    *,
    group_factory: ElementGroupFactory | None = None,
    keep_vertices: bool = False,
) -> SpectralDiscretization:
    """
    :arg discr: a :class:`~pystopt.mesh.SpectralDiscretization`.
    :arg xlm: a :class:`~meshmode.dof_array.DOFArray` of spectral coefficients
        for the new nodes.
    :arg group_factory: element group factory for the polynomial discretization.
    """
    nodes = discr.from_spectral_conn(xlm)
    xlm = actx.freeze(xlm)

    mesh = update_discr_mesh_from_nodes(actx, discr, nodes, keep_vertices=keep_vertices)
    return discr.copy(mesh=mesh, xlm=xlm, group_factory=group_factory)


# }}}


# {{{ merge disjoint discretizations


def merge_disjoint_spectral_discretizations(actx, discretizations):
    r"""
    :arg discretizations: a :class:`list` of
        :class:`~pystopt.mesh.SpectralDiscretization`\ s.
    :returns: A single :class:`~pystopt.mesh.SpectralDiscretization`, where
        all the meshes have been merged using
        :func:`~meshmode.mesh.processing.merge_disjoint_meshes`.
    """

    if len(discretizations) == 1:
        return discretizations[0]

    from pytools import is_single_valued

    if not is_single_valued(type(d) for d in discretizations):
        raise TypeError("discretizations must be of the same type")

    # merge meshes
    from meshmode.mesh.processing import merge_disjoint_meshes

    mesh = merge_disjoint_meshes(
        [d.mesh for d in discretizations], skip_tests=True, single_group=False
    )

    # merge spectral connections
    from dataclasses import replace

    specgroups = []
    grp_nr_base = 0
    for d in discretizations:
        specgroups.extend([
            replace(
                grp,
                batches=[
                    replace(
                        batch,
                        from_group_index=batch.from_group_index + grp_nr_base,
                    )
                    for batch in grp.batches
                ],
            )
            for grp in d.specgroups
        ])

        grp_nr_base += len(d.groups)

    # construct new discretization
    from pytools import flatten

    groups = list(flatten(d.groups for d in discretizations))

    from pystopt.mesh.poly_element import GivenGroupsGroupFactory

    dtype = np.result_type(*[d.real_dtype for d in discretizations])
    use_spectral_derivative = all(d.use_spectral_derivative for d in discretizations)

    xlm = None
    if all(d.xlm is not None for d in discretizations):
        from pytools.obj_array import make_obj_array

        from pystopt.dof_array import SpectralDOFArray

        xlm = make_obj_array([
            SpectralDOFArray(
                None,
                sum(tuple([tuple(d.xlm[axis]) for d in discretizations]), ()),
            )
            for axis in range(mesh.ambient_dim)
        ])

    return type(discretizations[0])(
        actx,
        mesh=mesh,
        specgroups=specgroups,
        group_factory=GivenGroupsGroupFactory(groups),
        real_dtype=dtype,
        use_spectral_derivative=use_spectral_derivative,
        _xlm=xlm,
    )


# }}}


# {{{ generate disjoint array


def _generate_array_discr_from_centers(
    actx,
    generator,
    centers,
    *,
    resolution: int,
) -> SpectralDiscretization:
    from dataclasses import replace

    discretizations = []
    for center in centers:
        g = replace(generator, offset=center)
        discretizations.append(generate_discretization(g, actx, resolution))

    return merge_disjoint_spectral_discretizations(actx, discretizations)


def generate_random_discr_array(
    actx: ArrayContext,
    generator: MeshGenerator,
    ndiscrs: int,
    *,
    resolution: int,
    radius: float,
    bbox: np.ndarray | None = None,
    origin: np.ndarray | None = None,
    gap: float | None = None,
    allowed_failures: int = 100,
    rng: np.random.Generator | None = None,
) -> SpectralDiscretization:
    r"""Constructs a random and disjoint set of discretizations in *bbox*.

    If a bounding box *bbox* is not provided, one is approximated from
    *ndiscrs*, *radius* and *gap*, such that approximately
    ``ndiscrs**(1/dim)`` discretizations can fit in each dimension.

    :arg ndiscrs: desired number of discretizations to generate.
    :arg bbox: bounding box in which to generate the discretizations. The
        bounding box an array of shape ``(2, dim)`` and is described in terms of its
        closest (to :math:`-\infty`) and furthest (to :math:`+\infty`) corners
        (i.e. lower left and upper right in 2d).
    :arg allowed_failures: discretizations are generated randomly inside the
        bounding box and can overlap (based on *radius*). If an overlap occurs,
        another attempt is made. Then, *allowed_failures* controls how many
        attempts can be made before failing.

    :arg origin: origin of the bounding box, which is is closest point to
        :math:`-\infty`. If *bbox* is provided, this value is ignored.
    :arg radius: radius of the smallest ball that contains the unshifted
        geometry from *generator*.
    :arg gap: approximate gap (based on *radius*) between the discretization
        in the lattice.

    :arg resolution: passed on to :func:`~pystopt.mesh.generate_discretization`.
    """
    # {{{ validate inputs

    if rng is None:
        rng = np.random.default_rng()

    if ndiscrs <= 0:
        raise ValueError("'nballs' must be > 0")

    ambient_dim = generator.ambient_dim
    if bbox is not None and any(len(b) != ambient_dim for b in bbox):
        raise ValueError(
            "'bbox' shape does not match 'ambient_dim'; "
            f"expected {ambient_dim}, but got {bbox.shape}"
        )

    if gap is None:
        gap = radius / 2

    if bbox is None:
        nballs_per_dim = ndiscrs ** (1 / ambient_dim)
        width_per_dim = 2.5 * nballs_per_dim * (radius + gap)
        width = np.array([width_per_dim] * ambient_dim, dtype=np.float64)

        if origin is None:
            origin = np.zeros(ambient_dim, dtype=np.float64)
    else:
        origin = np.array(bbox[0])
        width = np.array(bbox[1]) - np.array(bbox[0])

    # }}

    radius = radius + gap
    centers = np.empty((ambient_dim, ndiscrs), dtype=np.float64)
    centers[:, 0] = origin + width * rng.random(ambient_dim)

    i = 1
    failed = 0
    while i < ndiscrs and failed < allowed_failures:
        logger.debug("%d / %d with %d failed", i, ndiscrs, failed)
        center = origin + width * rng.random(ambient_dim)

        import numpy.linalg as la

        dist = np.min(la.norm(center.reshape(-1, 1) - centers[:, :i], axis=0))
        if dist < 2 * radius:
            failed += 1
            continue

        centers[:, i] = center
        i += 1
        failed = 0

    centers = centers[:, :i]
    if i != ndiscrs:
        from warnings import warn

        warn(
            f"only created {i} balls out of the requested {ndiscrs}; "
            "try enlarging 'bbox' or decreasing 'ndiscrs'",
            UserWarning,
            stacklevel=2,
        )

    return _generate_array_discr_from_centers(
        actx, generator, centers.T, resolution=resolution
    )


def generate_uniform_discr_array(
    actx: ArrayContext,
    generator: MeshGenerator,
    shape: tuple[int, ...],
    *,
    resolution: int,
    radius: float,
    origin: np.ndarray | None = None,
    mask: np.ndarray | None = None,
    gap: float | None = None,
) -> SpectralDiscretization:
    """Constructs a merged uniform lattice of repeated discretizations from *generator*.

    :arg shape: the number of repetitions of the discretization in each dimension.
    :arg origin: origin (or center) of the lattice.
    :arg mask: an array of size ``np.prod(shape)``, which can be used to
        remove certain entries in the lattice.
    :arg radius: radius of the smallest ball that contains the unshifted
        geometry from *generator*.
    :arg gap: approximate gap (based on *radius*) between the discretization
        in the lattice.
    """

    # {{{ validate inputs

    if len(shape) != generator.ambient_dim:
        raise ValueError(
            "'shape' size does not match 'ambient_dim'; "
            f"expected {generator.ambient_dim}, but got {len(shape)}"
        )

    if origin is None:
        origin = np.array([0.0, 0.0, 0.0][: generator.ambient_dim])

    if mask is None:
        mask = np.ones(np.prod(shape), dtype=bool)

    if mask.shape != (np.prod(shape),):
        raise ValueError(
            "'mask' size does not match 'shape': expected "
            f"{mask.shape}, but got ({np.prod(shape)},)"
        )

    if gap is None:
        gap = radius / 2

    # }}}

    # {{{ generate centers

    centers = [
        origin + (2 * radius + gap) * np.array(i)
        for iball, i in enumerate(np.ndindex(shape))
        if mask[iball]
    ]

    # }}}

    return _generate_array_discr_from_centers(
        actx, generator, centers, resolution=resolution
    )


# }}}


# {{{ rescale volume


def compute_group_volumes(
    actx: ArrayContext,
    places: GeometryCollection,
    *,
    dofdesc: sym.DOFDescriptorLike | None = None,
) -> tuple[Any, ...]:
    """Computes the volume of each group in *discr*.

    Note that this assumes that each group of the *discr* is a closed surface.
    """
    if dofdesc is None:
        dofdesc = places.auto_source
    else:
        dofdesc = sym.as_dofdesc(dofdesc)

    discr = places.get_discretization(dofdesc.geometry)
    x = actx.thaw(discr.nodes())
    n = bind(places, sym.normal(places.ambient_dim).as_vector(), auto_where=dofdesc)(
        actx
    )

    if isinstance(discr, SpectralDiscretization):
        integrand = 1 / places.ambient_dim * discr.to_spectral_conn(x @ n)
        return tuple([
            # NOTE: py Parseval's theorem, this should always be real, but just
            # to make sure, we take the real part anyway to remove any errors
            actx.np.real(actx.np.sum(subary * waa))
            for subary, waa in zip(
                integrand, discr._conj_scaled_area_element(), strict=False
            )
        ])
    else:
        raise TypeError(f"unsupported discretization type: '{type(discr).__name__}'")


def rescale_nodes(
    actx: ArrayContext, x: np.ndarray, *, vd: tuple[Any, ...], vs: tuple[Any, ...]
) -> np.ndarray:
    assert len(x[0]) == len(vd)
    assert len(x[0]) == len(vs)

    # NOTE: this should work for both SpectralDOFArray and DOFArray since we're
    # just scaling the result by a number (and spectral transforms are linear)
    from pytools.obj_array import make_obj_array

    return make_obj_array([
        type(xi)(
            actx,
            tuple([
                # NOTE: volume is int(1/d * x @ n * waa) and if we scale x by
                # alpha, it also pops up in the area elements -> alpha^2 total
                # change in the volume, so we need a sqrt here
                xi[i] * (vd[i] / vs[i]) ** (1.0 / x.size)
                for i in range(xi.size)
            ]),
        )
        for xi in x
    ])


def rescale_discretization(
    actx: ArrayContext,
    places: GeometryCollection,
    *,
    vd: float | tuple[float, ...],
    dofdesc: sym.DOFDescriptorLike | None = None,
) -> SpectralDiscretization:
    if dofdesc is None:
        dofdesc = places.auto_source
    else:
        dofdesc = sym.as_dofdesc(dofdesc)
    discr = places.get_discretization(dofdesc.geometry)

    from numbers import Number

    if isinstance(vd, Number):
        vd = (vd,) * len(discr.groups)

    if len(vd) != len(discr.groups):
        raise ValueError(f"Expected {len(discr.groups)} volumes, but got {len(vd)}")

    vs = compute_group_volumes(actx, places, dofdesc=dofdesc)
    print(vs)

    if isinstance(discr, SpectralDiscretization):
        x = actx.thaw(discr.xlm)
    else:
        x = actx.thaw(discr.nodes())

    x = rescale_nodes(actx, x, vd=vd, vs=vs)
    if isinstance(discr, SpectralDiscretization):
        return update_discr_from_spectral(actx, discr, x)
    else:
        return update_discr_from_nodes(actx, discr, x)


# }}}
