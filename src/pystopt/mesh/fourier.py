# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

"""
.. currentmodule:: pystopt.mesh

.. autoclass:: FourierConnectionBatch
.. autoclass:: FourierConnectionElementGroup
.. autoclass:: FourierDiscretization

.. autofunction:: make_fourier_discretization

.. currentmodule:: pystopt.mesh.fourier

.. autofunction:: polar_angle
.. autofunction:: find_modes_for_tolerance
.. autofunction:: visualize_fourier_modes
"""

from collections.abc import Callable, Sequence
from dataclasses import dataclass
from typing import Any

import numpy as np
import numpy.linalg as la

from arraycontext import ArrayContext
from meshmode.discretization.poly_element import (
    ElementGroupFactory,
    InterpolatoryQuadratureSimplexGroupFactory,
)
from meshmode.dof_array import DOFArray
from pytools import memoize_in, memoize_method
from pytools.obj_array import make_obj_array

from pystopt import bind, sym
from pystopt.dof_array import SpectralDOFArray
from pystopt.mesh.spectral import (
    SpectralConnectionBatch,
    SpectralConnectionElementGroup,
    SpectralDiscretization,
)
from pystopt.paths import PathLike
from pystopt.tools import get_default_logger

logger = get_default_logger(__name__)


# {{{ connection

try:
    from scipy import fft as _fft
except ImportError:
    from numpy import fft as _fft


@dataclass(frozen=True)
class FourierConnectionBatch(SpectralConnectionBatch):
    from_group_index: int
    node_indices: np.ndarray


@dataclass(frozen=True)
class FourierConnectionElementGroup(SpectralConnectionElementGroup):
    noutputs: int
    batches: tuple[FourierConnectionBatch, ...]
    is_real: bool = False

    # {{{ interface

    @property
    def dim(self):
        return 1

    @property
    def spat_shape(self):
        return (self.noutputs, 1)

    @property
    def nspec(self):
        if self.is_real:
            # NOTE: because we use the real FFT the coefficients get halved
            return self.noutputs // 2 + 1
        else:
            return self.noutputs

    def to_spectral(self, actx: ArrayContext, ary: Any) -> Any:
        assert ary.shape == self.spat_shape
        if not isinstance(ary, np.ndarray):
            ary = actx.to_numpy(ary)

        if self.is_real:
            result = _fft.rfft(ary.squeeze())
        else:
            result = _fft.fft(ary.squeeze())

        return actx.from_numpy(result.reshape(-1, 1))

    def from_spectral(self, actx: ArrayContext, ary: Any) -> Any:
        assert ary.shape == (self.nspec, 1)
        if not isinstance(ary, np.ndarray):
            ary = actx.to_numpy(ary)

        if self.is_real:
            result = _fft.irfft(ary.squeeze())
        else:
            result = _fft.ifft(ary.squeeze()).real.copy()

        return actx.from_numpy(result).reshape(self.spat_shape, order="F")

    # }}}

    # {{{

    @property
    @memoize_method
    def fftfreq(self):
        if self.is_real:
            return _fft.rfftfreq(self.noutputs, d=1.0 / self.noutputs)
        else:
            return _fft.fftfreq(self.noutputs, d=1.0 / self.noutputs)

    @property
    @memoize_method
    def theta(self):
        return np.linspace(0.0, 2.0 * np.pi, self.noutputs, endpoint=False)

    @property
    def jacobian(self):
        return np.pi / self.nelements

    # }}}


# }}}


# {{{ clfft


@dataclass(frozen=True)
class CLFFTConnectionElementGroup(FourierConnectionElementGroup):
    is_real: bool = True

    def transform(
        self, actx: ArrayContext, ary: Any, result: Any, *, forward: bool = True
    ) -> Any:
        try:
            import gpyfft  # pytype: disable=import-error
        except ImportError as exc:
            raise ImportError("CLFFTConnectionElementGroup requires gpyfft") from exc

        # FIXME: this is super slow for some reason. because we keep
        # re-creating the FFT plan with that call to gpyfft.fft.FFT?

        # NOTE: using a complex FFT for real inputs would require a copy
        # of *ary*, which is not exactly free (np.fft.fft probably does this
        # too, but better optimize device memory than host memory)
        assert self.is_real
        if isinstance(ary, np.ndarray):
            ary = actx.from_numpy(ary)

        fft = gpyfft.fft.FFT(
            actx.context,
            actx.queue,
            in_array=ary.ravel(),
            out_array=result.ravel(),
            real=not forward,
        )
        fft.enqueue(forward=forward)

    def to_spectral(self, actx: ArrayContext, ary: Any) -> Any:
        assert ary.shape == self.spat_shape
        dtype = {
            np.float32: np.complex64,
            np.float64: np.complex128,
        }[ary.dtype.type]
        result = actx.np.zeros(self.nspec, dtype=dtype)

        self.transform(actx, ary, result, forward=True)
        return result.reshape((self.nspec, 1))

    def from_spectral(self, actx: ArrayContext, ary: Any) -> Any:
        assert ary.shape == (self.nspec, 1)
        dtype = {
            np.complex64: np.float32,
            np.complex128: np.float64,
        }[ary.dtype.type]
        result = actx.np.zeros(self.noutputs, dtype=dtype)

        self.transform(actx, ary, result, forward=False)
        return result.reshape(self.spat_shape)


# }}}


# {{{ discretization


class FourierDiscretization(SpectralDiscretization):
    @memoize_method
    def _theta(self, igrp):
        actx = self._setup_actx
        theta = self.specgroups[igrp].theta

        return actx.freeze(actx.from_numpy(theta))

    @memoize_method
    def _conj_scaled_area_element(self):
        actx = self._setup_actx

        area = bind(self, sym.area_element(self.ambient_dim, dim=self.dim))(actx)

        area = type(area)(
            actx,
            tuple([
                ary / sgrp.noutputs * (2 * np.pi / sgrp.jacobian / sgrp.noutputs)
                for ary, sgrp in zip(area, self.specgroups, strict=False)
            ]),
        )

        return actx.freeze(actx.np.conj(self.to_spectral_conn(area)))

    @memoize_method
    def _fftfreq(self, igrp):
        actx = self._setup_actx

        fftfreq = self.specgroups[igrp].fftfreq.reshape(-1, 1)
        return actx.freeze(actx.from_numpy(fftfreq))

    def spectral_num_reference_derivative(self, ref_axes, ary):
        if isinstance(ref_axes, int):
            ref_axes = [ref_axes]
        ref_axes = list(ref_axes)

        if not all(ref_axis < self.dim for ref_axis in ref_axes):
            raise ValueError("axis index too large")

        actx = ary.array_context

        d = len(ref_axes)
        result = SpectralDOFArray(
            actx,
            tuple(
                (1.0j * actx.thaw(self._fftfreq(igrp))) ** d * ary[igrp] * grp.jacobian
                for igrp, grp in enumerate(self.specgroups)
            ),
        )

        return self.from_spectral_conn(result)


# }}}


# {{{ make fourier discretization


def _make_fourier_curve_discretization(
    actx: ArrayContext,
    nelements: int,
    *,
    curve_fn: Callable[[np.ndarray], np.ndarray],
    order: int,
    group_factory: ElementGroupFactory | None = None,
    mesh_unit_nodes: np.ndarray | None = None,
    use_spectral_derivative: bool = True,
    is_real: bool = False,
) -> tuple[FourierDiscretization, np.ndarray]:
    if group_factory is None:
        group_factory = InterpolatoryQuadratureSimplexGroupFactory(order=order)

    node_vertex_consistency_tolerance = None
    if mesh_unit_nodes is not None:
        # NOTE: if unit nodes are given, they may not include the boundary,
        # so the checks become a very restrictive
        node_vertex_consistency_tolerance = False

    if mesh_unit_nodes is None:
        import modepy as mp

        mesh_unit_nodes = mp.equidistant_nodes(1, order)

    from meshmode.mesh.generation import make_curve_mesh

    mesh, _ = make_curve_mesh(
        curve_fn,
        np.linspace(0.0, 1.0, nelements + 1),
        order=order,
        unit_nodes=mesh_unit_nodes,
        closed=True,
        node_vertex_consistency_tolerance=node_vertex_consistency_tolerance,
        return_parametrization_points=True,
    )

    # {{{ create spectral connection groups

    assert len(mesh.groups) == 1
    grp = mesh.groups[0]

    # set up per-group numbering
    node_indices = np.arange(grp.nelements * grp.nunit_nodes).reshape(
        grp.nelements, grp.nunit_nodes
    )

    # remove repeated element boundary nodes
    node_indices -= np.arange(0, grp.nelements).reshape(-1, 1)
    # wrap around for periodicity
    node_indices[-1, -1] = node_indices[0, 0]
    assert np.max(node_indices) == (grp.nelements * (grp.nunit_nodes - 1) - 1)

    batch = FourierConnectionBatch(
        from_group_index=0,
        node_indices=actx.freeze(actx.from_numpy(node_indices)),
    )
    noutputs = grp.nelements * (grp.nunit_nodes - 1)

    specgroup = FourierConnectionElementGroup(
        noutputs=noutputs, batches=(batch,), is_real=is_real
    )

    # }}}

    # NOTE: these spectral coefficients for the geometry are exact, since
    # we don't do any additional interpolation from whatever unit nodes the
    # mesh or the discretization have to the equidistant FFT grid
    xlm = make_obj_array([
        SpectralDOFArray(
            None, (actx.freeze(specgroup.to_spectral(actx, x.reshape(-1, 1))),)
        )
        for x in curve_fn(specgroup.theta / (2.0 * np.pi))
    ])

    discr = FourierDiscretization(
        actx,
        mesh,
        (specgroup,),
        group_factory=group_factory,
        use_spectral_derivative=use_spectral_derivative,
        _xlm=xlm,
    )

    return discr, xlm


def make_fourier_discretization(
    actx: ArrayContext,
    nelements: int | tuple[int, ...],
    *,
    curve_fn: Callable[[np.ndarray], np.ndarray],
    order: int,
    group_factory: ElementGroupFactory | None = None,
    mesh_unit_nodes: np.ndarray | None = None,
    use_spectral_derivative: bool = True,
    is_real: bool = False,
) -> tuple[FourierDiscretization, np.ndarray]:
    if not isinstance(nelements, tuple):
        nelements = (int(nelements),)

    if len(nelements) == 1:
        return _make_fourier_curve_discretization(
            actx,
            *nelements,
            curve_fn=curve_fn,
            order=order,
            group_factory=group_factory,
            mesh_unit_nodes=mesh_unit_nodes,
            use_spectral_derivative=use_spectral_derivative,
            is_real=is_real,
        )
    elif len(nelements) == 2:
        raise NotImplementedError
    else:
        raise ValueError(f"'nelements' has size {len(nelements)} > 2")


# }}}


# {{{ theta interpolation connection


class FourierThetaInterpolateConnection:
    def __init__(self, from_discr, to_discr):
        if not isinstance(from_discr, FourierDiscretization):
            raise TypeError(
                f"'from_discr' not a FourierDiscretization: {type(from_discr).__name__}"
            )

        if not isinstance(to_discr, FourierDiscretization):
            raise TypeError(
                f"'to_discr' not a FourierDiscretization: {type(to_discr).__name__}"
            )

        self.from_discr = from_discr
        self.to_discr = to_discr
        self.is_surjective = True

    @property
    @memoize_method
    def theta(self):
        actx = self.to_discr._setup_actx
        ambient_dim = self.to_discr.ambient_dim

        x, y = bind(
            self.to_discr,
            sym.nodes(ambient_dim).as_vector() - sym.surface_centroid(ambient_dim),
        )(actx)

        return actx.freeze(2.0 * np.pi * (y < 0) + actx.np.arctan2(y, x))

    def _interpolate(self, ary: DOFArray) -> DOFArray:
        if not isinstance(ary, DOFArray):
            raise TypeError("non-array passed to discretization connection")

        if ary.shape != (len(self.from_discr.groups),):
            raise ValueError("invalid shape of incoming resampling data")

        actx = ary.array_context

        @memoize_in(
            actx, (FourierThetaInterpolateConnection, "fourier_interpolate_knl")
        )
        def prg():
            from arraycontext import make_loopy_program

            t_unit = make_loopy_program(
                [
                    "{[iel, idof]: 0 <= iel < nelements and 0 <= idof < nnodes}",
                    "{[j]: 0 <= j < nfrequencies}",
                ],
                """
                result[iel, idof] = reduce(sum, j,
                        ary[j, 0] * exp(1j * k[j, 0] * theta[iel, idof])
                        ) / nfrequencies
                """,
                name="fourier_interpolate_knl",
            )

            import loopy as lp
            from meshmode.transform_metadata import (
                ConcurrentDOFInameTag,
                ConcurrentElementInameTag,
            )

            return lp.tag_inames(
                t_unit,
                {
                    "iel": ConcurrentElementInameTag(),
                    "idof": ConcurrentDOFInameTag(),
                },
            )

        from_discr = self.from_discr
        ahat = from_discr.to_spectral_conn(ary)

        result = []
        for igrp, grp in enumerate(from_discr.specgroups):
            if grp.is_real:
                ahat[igrp][0, 0] /= 2.0

            result.append(
                actx.call_loopy(
                    prg(),
                    ary=ahat[igrp],
                    k=from_discr._fftfreq(igrp),
                    theta=self.theta[igrp],
                )["result"].real
            )

        return DOFArray(actx, tuple(result))

    def __call__(self, ary):
        from meshmode.dof_array import rec_map_dof_array_container

        return rec_map_dof_array_container(self._interpolate, ary)


# }}}


# {{{ angle from coordinates


def polar_angle(x: np.ndarray) -> DOFArray:
    if x.shape != (2,):
        raise ValueError("only two-dimensional arrays are supported")

    x, y = x

    assert x.array_context is not None
    actx = x.array_context

    # remove centroids
    x = x - actx.np.sum(x) / sum(xi.size for xi in x)
    y = y - actx.np.sum(y) / sum(xi.size for xi in y)

    return actx.np.where(
        y < 0, 2.0 * np.pi * actx.np.ones_like(y), actx.np.zeros_like(y)
    ) + actx.np.arctan2(y, x)


# }}}


# {{{ find mode count for tolerance


def evaluate_fourier_expansion(actx, modes, k, t):
    @memoize_in(actx, (evaluate_fourier_expansion, f"fourier_eval_knl_{modes.ndim}d"))
    def prg():
        import loopy as lp
        from arraycontext import make_loopy_program

        if modes.ndim == 1:
            t_unit = make_loopy_program(
                [
                    "{[i, j]: 0 <= i < nresults and 0 <= j < nmodes}",
                ],
                f"""
                    result[i] = reduce(sum, j,
                        modes[j] * exp(2j * {np.pi!r} * k[j] * t[i])
                        ) / nmodes
                    """,
                name="fourier_eval_knl",
            )
        else:
            t_unit = make_loopy_program(
                [
                    "{[i, j]: 0 <= i < nresults and 0 <= j < nmodes}",
                    "{[idim]: 0 <= idim < ndim}",
                ],
                f"""
                    result[idim, i] = reduce(sum, j,
                        modes[idim, j] * exp(2j * {np.pi!r} * k[j] * t[i])
                        ) / nmodes
                    """,
                name="fourier_eval_knl",
            )
            t_unit = lp.fix_parameters(t_unit, ndim=modes.shape[0])
            t_unit = lp.tag_inames(t_unit, {"idim": "unr"})

        t_unit = lp.tag_inames(t_unit, {"i": "g.0"})
        return t_unit.executor(actx.context)

    _, result = prg()(actx.queue, modes=modes, k=k, t=t, allocator=actx.allocator)

    return result["result"]


def find_modes_for_tolerance(
    actx: ArrayContext,
    curve_fn: Callable[[np.ndarray], np.ndarray],
    *,
    rtol: float = 1.0e-8,
    kmin: int = 1,
    step: int = 2,
) -> int:
    r"""Finds a minimum number of nodes required to represent a given curve.

    The implementation uses a simple iterative approach where for each :math:`k`,
    the geometry is interpolated to a fine grid and compared to the exact
    geometry provided by *curve_fn*. If the relative :math:`\ell^2` error
    is sufficiently small, the algorithms is stopped.

    :arg rtol: relative tolerance in error between the geometry approximated
        with a given :math:`k` modes and the exact geometry.
    :arg kmin: starting number of modes.
    :arg step: increase in number of Fourier modes each time the error is
        too large.

    :returns: an approximate number of Fourier modes required to represent the
        geometry described by *curve_fn* to the given relative tolerance *rtol*.
    """
    nmodes = kmin

    while True:
        theta_coarse = np.linspace(0.0, 1.0, nmodes, endpoint=False)
        fhat = np.fft.fft(curve_fn(theta_coarse))
        k = np.fft.fftfreq(nmodes, d=1.0 / nmodes)

        theta_fine = np.linspace(0.0, 1.0, 4 * nmodes, endpoint=False)
        f_from_fine = curve_fn(theta_fine)

        # FIXME: use a NUFFT for this?
        f_from_coarse = evaluate_fourier_expansion(
            actx,
            actx.from_numpy(fhat),
            actx.from_numpy(k),
            actx.from_numpy(theta_fine),
        )
        f_from_coarse = actx.to_numpy(f_from_coarse)

        error = la.norm(f_from_coarse - f_from_fine) / la.norm(f_from_fine)
        logger.info("[%4d] error %.5e (target %.5e)", nmodes, error, rtol)

        if error < rtol:
            break

        nmodes += step

    return nmodes


# }}}


# {{{ visualize fourier modes


def visualize_fourier_modes(
    fig_or_filename: PathLike,
    actx: ArrayContext,
    discr: FourierDiscretization,
    names_and_fields: Sequence[tuple[str, Any]],
    *,
    markers: Sequence[str] | None = None,
    n_max_modes: float | None = None,
    semilogy: bool = True,
    overwrite: bool = False,
) -> None:
    """Write out the Fourier modes of the given fields.

    :arg: fig_or_filename: if a ``matplotlib.figure.Figure``, then the
        fields are added to the current axis.
    :arg n_max_modes: number of modes to plot, regardless of the number of
        modes in the *discr*. If the discretization has a complex FFT, then
        the slice ``-n_max_modes:n_max_modes`` is plotted.
    :arg semilogy: if *True*, the modes are plotted in a log scale.
    """
    if discr.ambient_dim != 2:
        raise ValueError(
            f"only 2d geometries are supported; got a {discr.ambient_dim}d geometry"
        )

    if len(discr.specgroups) != 1:
        raise ValueError(
            "only one spectral group is supported; "
            f"got discretization with {len(discr.specgroups)} groups"
        )

    if markers is None:
        markers = ["-"] * len(names_and_fields)

    from pystopt.visualization.matplotlib import subplots

    with subplots(fig_or_filename, overwrite=overwrite) as fig:
        assert len(fig.axes) == 1
        ax = fig.axes[0]

        fourier_names_and_fields = []
        fourier_markers = []
        for marker, (name, field) in zip(markers, names_and_fields, strict=False):
            spec_field = field

            if isinstance(field, SpectralDOFArray):
                actx_np = actx.np
                is_complex = field.entry_dtype.kind == "c"
            elif isinstance(field, DOFArray):
                spec_field = discr.to_spectral_conn(spec_field)
                actx_np = actx.np
                is_complex = field.entry_dtype.kind == "c"
            elif isinstance(field, np.ndarray) and field.dtype.char != "O":
                actx_np = np
                is_complex = field.dtype.kind == "c"
            else:
                raise TypeError(f"unsupported field type: '{type(field).__name__}'")

            if is_complex:
                fourier_names_and_fields.extend([
                    (f"{{{name}}}_r", actx_np.real(spec_field)),
                    (f"{{{name}}}_i", actx_np.imag(spec_field)),
                ])
                fourier_markers.extend([marker, marker])
            else:
                fourier_names_and_fields.append((name, spec_field))
                fourier_markers.append(marker)

        for igrp, grp in enumerate(discr.specgroups):
            k = grp.fftfreq.ravel(order="A")
            if n_max_modes is not None:
                if grp.is_real:
                    k = k[:n_max_modes]
                else:
                    k = np.concatenate([k[:n_max_modes], k[-n_max_modes:]])

            indices = np.argsort(k)
            k = k[indices]

            for marker, (name, field) in zip(
                fourier_markers, fourier_names_and_fields, strict=False
            ):
                np_field = field
                if isinstance(field, DOFArray):
                    np_field = actx.to_numpy(field[igrp]).ravel(order="A")[indices]
                else:
                    np_field = field.ravel(order="A")[indices]

                if semilogy:
                    ax.semilogy(
                        k, np.abs(np_field) + 1.0e-16, marker, label=f"${name}$"
                    )
                else:
                    ax.plot(k, np_field, marker, label=f"${name}$")

        if semilogy:
            ax.grid(visible=True, which="major", linestyle="-", alpha=0.75)
            ax.grid(visible=True, which="minor", linestyle="--", alpha=0.5)
        ax.set_xlabel("$k$")

        ax.legend(
            bbox_to_anchor=(0, 1.02, 1.0, 0.2),
            loc="lower left",
            mode="expand",
            borderaxespad=0,
            ncol=2,
        )

    # NOTE: for some reason matplotlib seems to be allocating a lot of memory
    # (even though the figures are closed?) and this seems to help with that
    import gc

    gc.collect()


# }}}
