# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

"""
.. currentmodule:: pystopt.mesh

.. autoclass:: UniqueConnectionBatch
.. autoclass:: UniqueConnectionInterpolationBatch
.. autoclass:: UniqueConnectionElementGroup

.. autoclass:: UniqueDOFConnectionBase
.. autoclass:: ToUniqueDOFConnection
.. autoclass:: FromUniqueDOFConnection

.. autofunction:: make_to_mesh_connection
.. autofunction:: make_from_mesh_connection

.. autofunction:: make_to_unique_mesh_vertex_connection
.. autofunction:: make_from_unique_mesh_vertex_connection
.. autofunction:: make_unique_mesh_vertex_connections

.. autofunction:: make_same_layout_connection
"""

from dataclasses import dataclass
from typing import Any, TypeVar

import numpy as np

import loopy as lp
from arraycontext import ArrayContext, ArrayOrContainerT, make_loopy_program
from meshmode.discretization import Discretization
from meshmode.discretization.connection import DiscretizationConnection
from meshmode.dof_array import DOFArray
from meshmode.transform_metadata import ConcurrentDOFInameTag, ConcurrentElementInameTag
from pytential import GeometryCollection
from pytools import memoize_in, memoize_on_first_arg

import pystopt.mesh.poly_element as mpoly
from pystopt import sym
from pystopt.tools import get_default_logger

logger = get_default_logger(__name__)


# {{{ kernels


@memoize_on_first_arg
def resample_by_mat_kernel(actx: ArrayContext) -> lp.TranslationUnit:
    t_unit = make_loopy_program(
        """{[iel, idof, j]:
            0 <= iel < nelements and
            0 <= idof < nto and
            0 <= j < nfrom}""",
        """
        result[iel, idof] = \
            sum(j, resampling_mat[idof, j] * ary[iel, j])
        """,
        name="resample_by_mat",
    )

    return lp.tag_inames(
        t_unit,
        {
            "iel": ConcurrentElementInameTag(),
            "idof": ConcurrentDOFInameTag(),
        },
    )


@memoize_on_first_arg
def to_unique_by_resampling_kernel(actx: ArrayContext) -> lp.TranslationUnit:
    t_unit = make_loopy_program(
        """{[iel, idof, j]:
        0 <= iel < nelements and
        0 <= idof < nto and
        0 <= j < nfrom}""",
        """
        result[to_indices[iel, idof]] = result[to_indices[iel, idof]] \
                + sum(j, resampling_mat[idof, j] * ary[iel, j]) \
                / weight[to_indices[iel, idof]] {atomic}
        """,
        kernel_data=[lp.GlobalArg("result", for_atomic=True), ...],
        name="to_unique_by_resampling",
    )

    t_unit = lp.tag_array_axes(t_unit, "to_indices", "stride:auto,stride:auto")
    return lp.tag_inames(
        t_unit,
        {
            "iel": ConcurrentElementInameTag(),
            "idof": ConcurrentDOFInameTag(),
        },
    )


@memoize_on_first_arg
def from_unique_by_resampling_kernel(actx: ArrayContext) -> lp.TranslationUnit:
    t_unit = make_loopy_program(
        """{[iel, idof, j]:
        0 <= iel < nelements and
        0 <= idof < nto and
        0 <= j < nfrom}""",
        """
        result[iel, idof] = \
                sum(j, resampling_mat[idof, j] * ary[from_indices[iel, j]])
        """,
        name="from_unique_by_resampling",
    )

    t_unit = lp.tag_array_axes(t_unit, "from_indices", "stride:auto,stride:auto")
    return lp.tag_inames(
        t_unit,
        {
            "iel": ConcurrentElementInameTag(),
            "idof": ConcurrentDOFInameTag(),
        },
    )


@memoize_on_first_arg
def to_unique_by_picking_kernel(actx: ArrayContext) -> lp.TranslationUnit:
    t_unit = make_loopy_program(
        """{[iel, idof]:
        0 <= iel < nelements and
        0 <= idof < nnodes}""",
        """
        result[to_indices[iel, idof]] = \
                result[to_indices[iel, idof]] \
                + ary[iel, idof] / weight[to_indices[iel, idof]] {atomic}
        """,
        kernel_data=[lp.GlobalArg("result", for_atomic=True), ...],
        name="to_unique_by_picking",
    )

    t_unit = lp.tag_array_axes(t_unit, "to_indices", "stride:auto,stride:auto")
    return lp.tag_inames(
        t_unit,
        {
            "iel": ConcurrentElementInameTag(),
            "idof": ConcurrentDOFInameTag(),
        },
    )


@memoize_on_first_arg
def from_unique_by_picking_kernel(actx: ArrayContext) -> lp.TranslationUnit:
    t_unit = make_loopy_program(
        """{[iel, idof]:
        0 <= iel < nelements and
        0 <= idof < nnodes}""",
        """
        result[iel, idof] = ary[from_indices[iel, idof]]
        """,
        name="from_unique_by_picking",
    )

    t_unit = lp.tag_array_axes(t_unit, "from_indices", "stride:auto,stride:auto")
    return lp.tag_inames(
        t_unit,
        {
            "iel": ConcurrentElementInameTag(),
            "idof": ConcurrentDOFInameTag(),
        },
    )


# }}}


# {{{ device matrices


@memoize_on_first_arg
def to_mesh_vertices_interp_matrix(discr: Discretization, igrp: int) -> Any:
    grp = discr.groups[igrp]
    actx = discr._setup_actx

    return actx.freeze(actx.from_numpy(mpoly.to_mesh_vertices_interp_matrix(grp)))


@memoize_on_first_arg
def to_mesh_nodes_interp_matrix(discr: Discretization, igrp: int) -> Any:
    grp = discr.groups[igrp]
    actx = discr._setup_actx

    return actx.freeze(actx.from_numpy(mpoly.to_mesh_nodes_interp_matrix(grp)))


@memoize_on_first_arg
def from_mesh_vertices_interp_matrix(discr: Discretization, igrp: int) -> Any:
    grp = discr.groups[igrp]
    actx = discr._setup_actx

    return actx.freeze(actx.from_numpy(mpoly.from_mesh_vertices_interp_matrix(grp)))


@memoize_on_first_arg
def from_mesh_nodes_interp_matrix(discr: Discretization, igrp: int) -> Any:
    grp = discr.groups[igrp]
    actx = discr._setup_actx

    return actx.freeze(actx.from_numpy(mpoly.from_mesh_nodes_interp_matrix(grp)))


# }}}


# {{{ unique dof connection


@dataclass(frozen=True)
class UniqueConnectionBatch:
    """
    .. attribute:: nelements
    .. attribute:: nunit_nodes
    .. attribute:: node_indices

        An array of size ``(nelements, nunit_nodes)`` mapping the nodes in
        each element to a unique numbering.

    .. attribute:: from_group_index

        An integer indicating to which element group in *from_discr*
        the data should be interpolated.

    """

    from_group_index: int
    node_indices: np.ndarray

    @property
    def nelements(self):
        return self.node_indices.shape[0]

    @property
    def nunit_nodes(self):
        return self.node_indices.shape[1]


@dataclass(frozen=True)
class UniqueConnectionInterpolationBatch(UniqueConnectionBatch):
    """
    .. attribute:: to_resampling_mat
    .. attribute:: from_resampling_mat

        Both :attr:`to_resampling_mat` and :attr:`from_resampling_mat` are
        frozen matrices used to interpolate the unit nodes in each element.
    """

    to_resampling_mat: Any
    from_resampling_mat: Any


@dataclass(frozen=True)
class UniqueConnectionElementGroup:
    r"""
    .. attribute:: dim
    .. attribute:: nelements
    .. attribute:: noutputs

        Number of unique degrees of freedom in the group.

    .. attribute:: batches

        A list of :class:`UniqueConnectionBatch`\ s describing the
        transport to the unique degrees of freedom and back. It is assumed
        that the :attr:`UniqueConnectionBatch.node_indices`
        from the batches fill all :attr:`noutputs`.
    """

    noutputs: int
    batches: tuple[UniqueConnectionBatch, ...]

    @property
    def dim(self):
        raise NotImplementedError

    @property
    def nelements(self):
        return sum(batch.nelements for batch in self.batches)


def compute_node_count_from_groups(
    actx: ArrayContext, groups: tuple[UniqueConnectionElementGroup, ...]
) -> DOFArray:
    @memoize_in(actx, (compute_node_count_from_groups, "node_element_count_knl"))
    def prg():
        t_unit = make_loopy_program(
            "{[iel, idof]: 0 <= iel < nelements and 0 <= idof < nnodes}",
            """
                result[node_indices[iel, idof]] = \
                        result[node_indices[iel, idof]] + 1   {atomic}
                """,
            kernel_data=[lp.GlobalArg("result", for_atomic=True), ...],
            name="node_element_count",
        )

        t_unit = lp.tag_array_axes(t_unit, "node_indices", "stride:auto,stride:auto")
        return lp.tag_inames(
            t_unit,
            {
                "iel": ConcurrentElementInameTag(),
                "idof": ConcurrentDOFInameTag(),
            },
        )

    results = []
    for grp in groups:
        result = actx.np.zeros(grp.noutputs, dtype=np.intp)

        for batch in grp.batches:
            actx.call_loopy(prg(), node_indices=batch.node_indices, result=result)

        results.append(result)

    return DOFArray(actx, tuple(results))


class UniqueDOFConnectionBase(DiscretizationConnection):
    def __init__(
        self,
        discr: Discretization,
        groups: tuple[UniqueConnectionElementGroup, ...],
        weights: DOFArray | None = None,
    ):
        if weights is None:
            actx = discr._setup_actx
            weights = actx.freeze(compute_node_count_from_groups(actx, groups))

        assert len(groups) == len(weights)
        assert weights.array_context is None

        super().__init__(discr, discr, is_surjective=True)
        self.groups = groups
        self.weights = weights

    @property
    def noutputs(self):
        return sum(grp.noutputs for grp in self.groups)


class ToUniqueDOFConnection(UniqueDOFConnectionBase):
    def _to_unique(self, ary: DOFArray) -> DOFArray:
        if not isinstance(ary, DOFArray):
            raise TypeError("non-array passed to discretization connection")

        if ary.shape != (len(self.from_discr.groups),):
            raise ValueError("invalid shape of incoming resampling data")

        actx = ary.array_context
        result = []

        for grp, weight in zip(self.groups, self.weights, strict=False):
            r = actx.np.zeros(grp.noutputs, dtype=ary.entry_dtype)

            for batch in grp.batches:
                if isinstance(batch, UniqueConnectionInterpolationBatch):
                    kwargs = {"resampling_mat": batch.to_resampling_mat}
                    knl = to_unique_by_resampling_kernel(actx)
                else:
                    kwargs = {}
                    knl = to_unique_by_picking_kernel(actx)

                actx.call_loopy(
                    knl,
                    ary=ary[batch.from_group_index],
                    to_indices=batch.node_indices,
                    weight=weight,
                    result=r,
                    **kwargs,
                )

            result.append(r.reshape(-1, 1))

        return DOFArray(actx, tuple(result))

    def __call__(self, ary: ArrayOrContainerT) -> ArrayOrContainerT:
        from meshmode.dof_array import rec_map_dof_array_container

        return rec_map_dof_array_container(self._to_unique, ary)


class FromUniqueDOFConnection(UniqueDOFConnectionBase):
    def _from_unique(self, ary: DOFArray) -> DOFArray:
        if ary.shape != (len(self.groups),):
            raise ValueError("invalid shape of incoming resampling data")

        actx = ary.array_context
        result = self.from_discr.zeros(actx, dtype=ary.entry_dtype)

        for subary, grp in zip(ary, self.groups, strict=False):
            for batch in grp.batches:
                if isinstance(batch, UniqueConnectionInterpolationBatch):
                    kwargs = {"resampling_mat": batch.from_resampling_mat}
                    knl = from_unique_by_resampling_kernel(actx)
                else:
                    kwargs = {}
                    knl = from_unique_by_picking_kernel(actx)

                assert subary.shape == (grp.noutputs, 1)

                actx.call_loopy(
                    knl,
                    ary=subary.reshape(-1),
                    from_indices=batch.node_indices,
                    result=result[batch.from_group_index],
                    **kwargs,
                )

        return result

    def __call__(self, ary: ArrayOrContainerT) -> ArrayOrContainerT:
        from meshmode.dof_array import rec_map_dof_array_container

        return rec_map_dof_array_container(self._from_unique, ary)


# }}}


# {{{ discretization <-> mesh connection


def make_to_mesh_connection(
    actx: ArrayContext, discr: Discretization
) -> DiscretizationConnection:
    # NOTE: we don't need the whole spectral machinery for this, so just create
    # a standard discretization for the connection
    from_discr = discr
    to_discr = Discretization(
        actx,
        discr.mesh,
        group_factory=mpoly.FromMeshNodesGroupFactory(),
        real_dtype=discr.real_dtype,
    )

    from meshmode.discretization.connection import make_same_mesh_connection

    return make_same_mesh_connection(actx, to_discr, from_discr)


def make_from_mesh_connection(
    actx: ArrayContext, discr: Discretization
) -> DiscretizationConnection:
    # NOTE: we don't need the whole spectral machinery for this, so just create
    # a standard discretization for the connection
    from_discr = Discretization(
        actx,
        discr.mesh,
        group_factory=mpoly.FromMeshNodesGroupFactory(),
        real_dtype=discr.real_dtype,
    )
    to_discr = discr

    from meshmode.discretization.connection import make_same_mesh_connection

    return make_same_mesh_connection(actx, to_discr, from_discr)


# }}}


# {{{ discretization <-> mesh vertex connections

UniqueDOFConnectionT = TypeVar("UniqueDOFConnectionT", bound=UniqueDOFConnectionBase)


def _make_mesh_vertex_connection(
    discr: Discretization, cls: type[UniqueDOFConnectionT]
) -> UniqueDOFConnectionT:
    actx = discr._setup_actx

    batches = tuple([
        UniqueConnectionInterpolationBatch(
            igrp,
            actx.freeze(actx.from_numpy(grp.vertex_indices)),
            to_resampling_mat=to_mesh_vertices_interp_matrix(discr, igrp),
            from_resampling_mat=from_mesh_vertices_interp_matrix(discr, igrp),
        )
        for igrp, grp in enumerate(discr.mesh.groups)
    ])
    group = UniqueConnectionElementGroup(discr.mesh.nvertices, batches)

    return cls(discr, (group,))


def make_to_unique_mesh_vertex_connection(
    discr: Discretization,
) -> ToUniqueDOFConnection:
    return _make_mesh_vertex_connection(discr, ToUniqueDOFConnection)


def make_from_unique_mesh_vertex_connection(
    discr: Discretization,
) -> FromUniqueDOFConnection:
    return _make_mesh_vertex_connection(discr, FromUniqueDOFConnection)


def make_unique_mesh_vertex_connections(
    discr: Discretization,
) -> tuple[ToUniqueDOFConnection, FromUniqueDOFConnection]:
    to_vertex = make_to_unique_mesh_vertex_connection(discr)
    from_vertex = FromUniqueDOFConnection(discr, to_vertex.groups)

    return to_vertex, from_vertex


def get_unique_mesh_vertex_connections(
    places: GeometryCollection, dofdesc: sym.DOFDescriptorLike | None = None
) -> tuple[ToUniqueDOFConnection, FromUniqueDOFConnection]:
    if dofdesc is None:
        dofdesc = places.auto_source
    dofdesc = sym.as_dofdesc(dofdesc)

    to_key = (dofdesc.geometry, dofdesc.discr_stage, "vertex")
    from_key = (dofdesc.geometry, "vertex", dofdesc.discr_stage)

    try:
        to_conn = places._get_conn_from_cache(*to_key)
        from_conn = places._get_conn_from_cache(*from_key)
    except KeyError:
        discr = places.get_discretization(dofdesc.geometry, dofdesc.discr_stage)
        to_conn, from_conn = make_unique_mesh_vertex_connections(discr)

        places._add_conn_to_cache(to_conn, *to_key)
        places._add_conn_to_cache(from_conn, *from_key)

    return to_conn, from_conn


# }}}


# {{{ make_same_layout_connection


def make_same_layout_connection(actx, to_discr, from_discr):
    """A version of :func:`~meshmode.discretization.connection.make_same_mesh_connection`
    that only checks that the two discretizations have the same layout,
    not exactly the same underlying meshes.

    In this case, the layout is implied by the types of the mesh element groups
    and the number of elements in each group.
    """  # noqa: E501

    from meshmode.discretization.connection.direct import (
        DirectDiscretizationConnection,
        DiscretizationConnectionElementGroup,
        IdentityDiscretizationConnection,
        InterpolationBatch,
    )

    if from_discr is to_discr:
        return IdentityDiscretizationConnection(from_discr)

    if from_discr.ambient_dim != to_discr.ambient_dim or from_discr.dim != to_discr.dim:
        raise ValueError("discreitzations have different dimensions")

    if len(from_discr.groups) != len(to_discr.groups):
        raise ValueError(
            "discretizations do not have same number of groups; got "
            f"{len(from_discr.groups)} 'from' "
            f"and {len(to_discr.groups)} 'to' groups"
        )

    groups = []
    for igrp, (fgrp, tgrp) in enumerate(
        zip(from_discr.groups, to_discr.groups, strict=False)
    ):
        if type(fgrp.mesh_el_group) is not type(tgrp.mesh_el_group):
            raise TypeError(
                "groups must have the same underlying MeshElementGroup; "
                f"got '{type(fgrp.mesh_el_group).__name__}' in 'from'"
                f"and '{type(tgrp.mesh_el_group).__name__}' in 'to'"
            )

        if fgrp.nelements != tgrp.nelements:
            raise ValueError(
                f"group '{igrp}' has different number of elements; "
                f"got {fgrp.nelements} 'from'"
                f" and {tgrp.nelements} 'to' elements"
            )

        all_elements = actx.freeze(
            actx.from_numpy(np.arange(fgrp.nelements, dtype=np.intp))
        )
        ibatch = InterpolationBatch(
            from_group_index=igrp,
            from_element_indices=all_elements,
            to_element_indices=all_elements,
            result_unit_nodes=tgrp.unit_nodes,
            to_element_face=None,
        )

        groups.append(DiscretizationConnectionElementGroup([ibatch]))

    return DirectDiscretizationConnection(
        from_discr, to_discr, groups, is_surjective=True
    )


# }}}
