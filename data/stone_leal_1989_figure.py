# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

import pathlib

import numpy as np


def main(filename: pathlib.Path, capillary_number: float) -> None:
    deformation_vs_ca = np.genfromtxt(filename,
            dtype=np.float64,
            delimiter=",", skip_header=True,
            usemask=True).T

    n = deformation_vs_ca.shape[0] // 2
    for i in range(n):
        ca = deformation_vs_ca[2 * i].compressed()
        dd = deformation_vs_ca[2 * i + 1].compressed()

        try:
            from scipy.interpolate import interp1d
            d = interp1d(ca, dd, kind="cubic", copy=False)(capillary_number)
        except ImportError:
            d = np.interp(capillary_number, ca, dd)

        print(f"Deformation: {d:0.12e}")


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("filename", type=pathlib.Path)
    parser.add_argument("capillary_number", type=float)
    args = parser.parse_args()

    main(args.filename, args.capillary_number)
