pystopt: Two-phase Stokes Optimization
======================================

This project aims to do some adjoint-based optimization on simple two-phase
Stokes problems. The preferred numerical method in use is based
on boundary integral equations.

For more details and examples on how everything works, there's an out of
date ``docs`` folder, where one can generate documentation using Sphinx.
The documentation can also be found online on
`readthedocs <https://pystopt.readthedocs.io/en/latest/index.html>`__.

This is an evolution of a previous project that can be found
`here <https://gitlab.com/alexfikl/stokes-phase>`__. That relies on
MATLAB and only supports axisymmetric single-droplet setups.

Requirements
============

The library relies on

* `pytential <https://github.com/inducer/pytential>`__ for accurate layer
  potential evaluation.
* `PyOpenCL <https://pypi.python.org/pypi/pyopencl>`__ as computational
  infrastructure.
* `SHTns <https://bitbucket.org/nschaeff/shtns>`__ for spherical harmonic
  transform support.

and any packages they require. For most libraries, the development versions
from their respective *main* branches are required. These are made
explicit in ``pyproject.toml``.

Installation
============

A simple installation can be done using ``pip``::

    python -m pip install --upgrade -e .[dev,git]

to get all the dependencies.

License
=======

Everything is licensed under MIT (see provided ``LICENSES/MIT.txt`` file).

Spherical harmonic functionality is provided by the
`SHTns <https://bitbucket.org/nschaeff/shtns>`__ library, which is licensed
under the GPL-compatible `CeCILL-2.1 <https://spdx.org/licenses/CECILL-2.1.html>`__
license.

Publications
============

The work in here is based on several publications. To cite the two-dimensional
version, we recommend::

    @article{PYSTOPT2021,
        author = { A. Fikl and D. J. Bodony },
        doi = { https://doi.org/10.1017/jfm.2020.1013 },
        journal = { Journal of Fluid Mechanics },
        publisher = { Cambridge University Press (CUP) },
        title = { Adjoint-Based Interfacial Control of Viscous Drops },
        volume = { 911 },
        year = { 2021 }
    }

and for the three-dimensional version, we recommend::

    @article{PYSTOPT2022,
        author = { A. Fikl and D. J. Bodony },
        eprint = { 2210.11916v1 },
        eprinttype = { arXiv },
        title = { Adjoint-Based Control of Three Dimensional {Stokes} Droplets },
        year = { 2022 }
    }

The work and algorithms are also described in detail in the PhD thesis::

    @phdthesis{PYSTOPT2022,
        author = { A. Fikl },
        institution = { University of Illinois Urbana-Champaign },
        title = { Adjoint-Based Optimization of Multiphase Flows With Sharp Interfaces },
        year = { 2022 },
        url = { https://www.ideals.illinois.edu/items/124661 },
    }
