# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

import numpy as np

from pystopt.tools import get_default_logger

logger = get_default_logger(module=__file__)


def benchmark_shtns(resolution: int) -> tuple[float, float]:
    nlon = resolution
    nlat = resolution

    lmax = nlon // 2 - 1
    mmax = nlat // 2 - 1

    from pystopt.mesh.spharm import _make_shtns

    sh = _make_shtns(lmax, mmax, nlat, nlon, polar_opt=1.0e-10)

    theta = np.arccos(sh.cos_theta)
    phi = (2.0 * np.pi / nlon) * np.arange(nlon)

    mphi, mtheta = np.meshgrid(phi, theta)
    x = np.sin(mtheta) * np.cos(mphi)
    xlm = sh.analys(x)

    from pystopt.measure import timeit

    t_synth = timeit(lambda: sh.synth(xlm), repeat=16)
    t_analys = timeit(lambda: sh.analys(x), repeat=16)
    logger.info("resolution %5d synth [%s] analys [%s]", resolution, t_synth, t_analys)

    return t_synth.mean, t_analys.mean


def run_benchmark(offset: int, suffix: str) -> None:
    from pystopt.measure import EOCRecorder

    eoc_s = EOCRecorder(name=f"synth ({suffix})")
    eoc_a = EOCRecorder(name=f"analys ({suffix})")
    resolutions = offset + np.arange(32, 1024, 32)

    for resolution in resolutions:
        t_synth, t_analys = benchmark_shtns(resolution)
        eoc_s.add_data_point(resolution, t_synth)
        eoc_a.add_data_point(resolution, t_analys)

    from pystopt.measure import stringify_eoc

    logger.info("\n%s", stringify_eoc(eoc_s, eoc_a))

    return eoc_s, eoc_a


def run() -> None:
    eoc_even = run_benchmark(offset=0, suffix="even")
    eoc_odd = run_benchmark(offset=1, suffix="odd")

    from pystopt.measure import visualize_eoc

    visualize_eoc(
        "benchmark-shtns",
        *eoc_even,
        *eoc_odd,
        abscissa="N",
        ylabel="Time~ (s)",
        overwrite=True,
    )


if __name__ == "__main__":
    run()
