# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

from dataclasses import dataclass, replace

import numpy as np

from arraycontext import ArrayContext

import pystopt.callbacks as cb
from pystopt.simulation import as_state_container
from pystopt.tools import get_cl_array_context, get_default_logger

logger = get_default_logger(module=__file__)


# {{{ callbacks


@dataclass
class CallbackManager(cb.CallbackManager):
    def get_output_field_getters(self):
        def get_output_field_state(state, **kwargs):
            fields = {}
            fields["x"] = cb.OutputField("x", state.x)

            return fields

        return (get_output_field_state,)


@dataclass(frozen=True)
class VisualizeCallback(cb.VisualizeCallback):
    def _update_vis(self, actx, places, dofdesc):
        pass


@as_state_container
class State:
    x: np.ndarray
    array_context: ArrayContext

    @property
    def places(self):
        return None

    @property
    def dofdesc(self):
        return None

    @classmethod
    def wrap(cls, actx: ArrayContext, x: float) -> np.ndarray:
        from pytools.obj_array import make_obj_array

        return make_obj_array([actx.from_numpy(np.array([x]))])

    def unwrap(self) -> float:
        return self.array_context.to_numpy(self.x[0])[0]


# }}}


# {{{ run


def make_callback(
    actx,
    checkpoint_file_name,
    *,
    overwrite: bool = False,
    from_restart: bool | int = False,
) -> CallbackManager:
    from pystopt.checkpoint import make_hdf_checkpoint_manager

    checkpoint = make_hdf_checkpoint_manager(checkpoint_file_name)

    if from_restart:
        cm = checkpoint.read_from("callback")
    else:
        cm = CallbackManager(callbacks={})

    # {{{ checkpoint

    if from_restart:
        assert "checkpoint" in cm.callbacks
    else:
        cm["checkpoint"] = cb.CheckpointCallback(
            norm=None, checkpoint=checkpoint, overwrite=overwrite
        )

    # }}}

    # {{{ history

    if from_restart:
        assert "history" in cm.callbacks
        object.__setattr__(cm["history"], "norm", actx.np.linalg.norm)
    else:
        cm["history"] = cb.OptimizationHistoryCallback(
            norm=actx.np.linalg.norm,
        )

    # }}}

    # {{{ log

    if from_restart:
        assert "log" in cm.callbacks
        cm["log"] = replace(
            cm["log"],
            norm=actx.np.linalg.norm,
            log=logger.info,
        )
    else:
        cm["log"] = cb.OptimizationLogCallback(
            norm=actx.np.linalg.norm,
            log=logger.info,
        )

    # }}}

    # {{{ visualize

    if from_restart:
        assert "visualize" in cm.callbacks
    else:
        from pystopt.paths import generate_filename_series

        visualize_file_series = generate_filename_series(
            "visualize",
            cwd=checkpoint.filename.parent,
        )
        cm["visualize"] = VisualizeCallback(
            norm=None,
            visualize_file_series=visualize_file_series,
            frequency=1,
            overwrite=overwrite,
        )

    if not isinstance(from_restart, bool):
        cm.restart_from_iteration(from_restart)

    # }}}

    return cm


def run_optimize(
    ctx_factory,
    *,
    maxit: int = 32,
    from_restart: bool = False,
    overwrite: bool = True,
) -> None:
    actx = get_cl_array_context(ctx_factory)

    # {{{ callbacks

    from pystopt.checkpoint import make_default_checkpoint_path

    checkpoint_file_name = make_default_checkpoint_path(
        "optimization-checkpointing-restart", ("v0",), overwrite=overwrite
    )
    if not from_restart and checkpoint_file_name.exists():
        checkpoint_file_name.unlink()

    callback = make_callback(
        actx, checkpoint_file_name, from_restart=from_restart, overwrite=overwrite
    )
    checkpoint = callback["checkpoint"].checkpoint

    # }}}

    # {{{ "optimize"

    def fun(x):
        return actx.to_numpy(42 * x.x[0] ** 2 + 3)[0]

    def jac(x):
        return 84 * x

    from pystopt.checkpoint.hdf import array_context_for_pickling

    if from_restart:
        from pystopt.checkpoint import get_result_from_checkpoint

        with array_context_for_pickling(actx):
            state0 = get_result_from_checkpoint(
                checkpoint,
                from_restart=from_restart,
            )
            if isinstance(state0, np.ndarray):
                state0 = actx.to_numpy(state0[0])[0]

        logger.info("x[restart]: %.15e", state0)
    else:
        state0 = 100.0
    state0 = State(x=State.wrap(actx, state0), array_context=actx)

    from pystopt.optimize.linesearch import StabilizedBarzilaiBorweinLineSearch

    ls = StabilizedBarzilaiBorweinLineSearch(
        fun=fun,
        max_alpha=1.0e-3,
    )

    from pystopt.optimize.steepest import minimize

    r = minimize(
        fun=fun,
        x0=state0,
        jac=jac,
        callback=callback,
        options={"maxit": maxit, "linesearch": ls, "rtol": 1.0e-12},
    )

    r = replace(r, x=r.x.unwrap())
    if from_restart:
        from pystopt.optimize.cg_utils import combine_cg_results

        rprev = checkpoint.read_from("result")
        r = combine_cg_results(r, rprev, n=max(0, from_restart - 1))

    logger.info("result:\n%s", r.pretty())

    # }}}

    with array_context_for_pickling(actx):
        checkpoint.write_to("callback", callback, overwrite=True)
        checkpoint.write_to("result", r, overwrite=True)

        checkpoint.done()


def run(ctx_factory) -> None:
    actx = get_cl_array_context(ctx_factory)

    # NOTE: this just runs the optimization a few times and checks that it
    # gets correctly restarted on the next call using the info from the previous
    # call. this means the checkpoint, history and visualization callbacks
    run_optimize(actx, maxit=16)
    run_optimize(actx, maxit=16, from_restart=True)
    run_optimize(actx, maxit=16, from_restart=True)
    run_optimize(actx, maxit=16, from_restart=36)


# }}}


if __name__ == "__main__":
    import sys

    import pyopencl as cl

    if len(sys.argv) > 1:
        exec(sys.argv[1])
    else:
        run(cl._csc)
