# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

"""
Approximate a helix by quintic Bezier curves using::

    https://doi.org/10.1016/S0167-8396(03)00074-8
"""

import pathlib

import numpy as np
import numpy.linalg as la
import sympy as sp


def evaluate_bernstein_basis(n: int, t: np.ndarray) -> tuple[np.ndarray, ...]:
    s = sp.Symbol("s", real=True, positive=True)

    bases = []
    for i in range(n + 1):
        bi = (
            sp.factorial(n)
            / (sp.factorial(i) * sp.factorial(n - i))
            * s**i
            * (1 - s) ** (n - i)
        )
        bi = sp.lambdify((s,), bi, "numpy")
        bases.append(bi(t))

    return tuple(bases)


def get_helix_control_points(r: float, b: float, theta: float) -> np.ndarray:
    ct = np.cos(theta)
    st = np.sin(theta)

    k0 = 1 / r
    l1 = 2 * np.sin(theta)

    # {{{ alpha

    # NOTE: from equation (16)
    a0 = 25 / 16 * k0 / st * (1 - ct)
    b0 = -5 / 4 * (1 - ct)
    c0 = -2 * r / st * (1 - ct) ** 2 + 15 / 4 * r * (theta - st)
    # NOTE: choosing first root as recommended
    alpha = (-b0 + np.sqrt(b0**2 - 4 * a0 * c0)) / (2 * a0)

    # }}}

    # {{{ Q

    # NOTE: from equation (10)
    s1 = 8 / 5 * r * (1 - ct) - l1 * alpha / 4
    s0 = (5 / 4 * k0 * alpha**2 + ct * s1) / st

    V0 = np.array([0, 1])
    V1 = np.array([-np.sin(2 * theta), np.cos(2 * theta)])

    Q = np.empty((6, 2))
    Q[0] = np.array([r, 0])
    Q[5] = np.array([r * np.cos(2 * theta), r * np.sin(2 * theta)])

    U0 = -(Q[0] - Q[5]) / la.norm(Q[0] - Q[5])
    U1 = np.array([U0[1], -U0[0]])

    Q[1] = Q[0] + alpha * V0
    Q[2] = Q[0] + s0 * U0 + s1 * U1
    Q[3] = Q[5] - s0 * U0 + s1 * U1
    Q[4] = Q[5] - alpha * V1

    # }}}

    # {{{ z

    hp0 = 5 * alpha * b / r
    hpp0 = 20 * (ct * s0 + st * s1 - 2 * alpha) * b / r

    hp1 = hp0
    hpp1 = -hpp0

    z = np.empty(Q.shape[0])
    z[0] = 0
    z[5] = 2 * theta * b

    z[1] = z[0] + hp0 / 5
    z[4] = z[5] - hp1 / 5

    z[2] = hpp0 / 20 + 2 * z[1] - z[0]
    z[3] = hpp1 / 20 + 2 * z[4] - z[5]

    # }}}

    return Q, z, (V0, V1, U0, U1)


# {{{ arc


def approximate_arc(r: float = 100.0, theta: float = np.pi / 2, *, npoints: int = 256):
    assert theta <= np.pi / 2
    t = np.linspace(0.0, 1.0, npoints)

    basis = evaluate_bernstein_basis(n=5, t=t)
    Q, _, (V0, V1, U0, U1) = get_helix_control_points(r, 0.0, theta)
    arc = sum(Q[i].reshape(-1, 1) * basis[i] for i in range(Q.shape[0]))
    ref_arc = np.stack([r * np.cos(2 * theta * t), r * np.sin(2 * theta * t)])
    # {{{ plot

    from pystopt.visualization.matplotlib import subplots

    filename = pathlib.Path.cwd() / "bezier-arc"
    with subplots(filename, overwrite=True) as fig:
        ax = fig.gca()

        ax.plot(ref_arc[0], ref_arc[1], "k--")
        ax.plot(arc[0], arc[1])

        ax.plot(Q[:, 0], Q[:, 1], "o")
        for i in range(Q.shape[0]):
            ax.text(Q[i, 0], Q[i, 1], f"$Q_{i}$")

        ax.quiver(Q[0, 0], Q[0, 1], V0[0], V0[1])
        ax.quiver(Q[5, 0], Q[5, 1], -V1[0], -V1[1])
        ax.quiver(Q[0, 0], Q[0, 1], U0[0], U0[1], color="blue")
        ax.quiver(Q[0, 0], Q[0, 1], U1[0], U1[1], color="red")

        ax.margins(0.1, 0.1)
        ax.set_aspect("equal")

    filename = pathlib.Path.cwd() / "bezier-arc-error"
    with subplots(filename, overwrite=True) as fig:
        ax = fig.gca()

        ax.plot(t, (ref_arc[0] - arc[0]) / la.norm(ref_arc[0]))
        ax.plot(t, (ref_arc[1] - arc[1]) / la.norm(ref_arc[1]))

    # }}}


# }}}


# {{{ helix


def approximate_helix(
    r: float = 1.0,
    b: float = 1.0,
    theta: float = np.pi / 2,
    *,
    npoints: int = 256,
    narcs: int = 6,
) -> None:
    t = np.linspace(0.0, 1.0, npoints)

    basis = evaluate_bernstein_basis(n=5, t=t)
    Q, z, _ = get_helix_control_points(r, b, theta)
    Q = np.vstack([Q.T, z]).T

    helix_arc = sum(Q[i].reshape(-1, 1) * basis[i] for i in range(Q.shape[0]))

    pitch = 2 * b * theta
    ref_helix_arc = np.stack([
        r * np.cos(2 * theta * t),
        r * np.sin(2 * theta * t),
        pitch * t,
    ])

    print("error:", la.norm(helix_arc - ref_helix_arc) / la.norm(ref_helix_arc))

    # {{{ concatenate

    rot = np.array([
        [np.cos(2 * theta), -np.sin(2 * theta), 0],
        [np.sin(2 * theta), np.cos(2 * theta), 0],
        [0, 0, 1],
    ])
    offset = np.array([0, 0, pitch]).reshape(-1, 1)

    helix = [helix_arc]
    ref_helix = [helix_arc]
    for i in range(1, narcs):
        helix.append(rot @ (helix[i - 1] + offset))
        ref_helix.append(rot @ (ref_helix[i - 1] + offset))

    helix = np.hstack(helix)
    ref_helix = np.hstack(ref_helix)

    # }}}

    # {{{ plot

    from pystopt.visualization.matplotlib import subplots

    filename = pathlib.Path.cwd() / "bezier-helix"
    with subplots(filename, projection="3d", overwrite=True) as fig:
        ax = fig.gca()

        ax.plot(ref_helix[0], ref_helix[1], ref_helix[2], "k--")
        ax.plot(helix[0], helix[1], helix[2])

        ax.plot(Q[:, 0], Q[:, 1], Q[:, 2], "o")
        for i in range(Q.shape[0]):
            ax.text(Q[i, 0], Q[i, 1], Q[i, 2], f"$Q_{i}$")

    filename = pathlib.Path.cwd() / "bezier-helix-error"
    with subplots(filename, overwrite=True) as fig:
        ax = fig.gca()

        ax.plot(t, (ref_helix_arc[0] - helix_arc[0]) / la.norm(ref_helix_arc[0]))
        ax.plot(t, (ref_helix_arc[1] - helix_arc[1]) / la.norm(ref_helix_arc[1]))
        ax.plot(t, (ref_helix_arc[2] - helix_arc[2]) / la.norm(ref_helix_arc[2]))

    # }}}


# }}}


if __name__ == "__main__":
    approximate_arc()
    approximate_helix()
