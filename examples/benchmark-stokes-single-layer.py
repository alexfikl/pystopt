# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

"""
Constructs a whole lot of Stokes kernels with the goal of profiling
direct evaluation and improving the performance.

elapsed: 1917.34s wall 1.01x CPU
elapsed: 2094.80s wall 0.91x CPU
elapsed: 1917.34s wall 1.01x CPU
"""

import logging
import os

from pytential import bind, sym
from pytential.log import set_up_logging

set_up_logging(os.path.basename(__file__), logging.INFO)
logger = logging.getLogger(os.path.basename(__file__))


def main(
    actx,
    ambient_dim: int = 3,
    resolution: int = 2,
    target_order: int = 4,
    source_ovsmp: int = 4,
    qbx_order: int = 4,
):
    # {{{ geometry

    import meshmode.mesh.generation as mgen

    radius = 1

    if ambient_dim == 3:
        mesh = mgen.generate_sphere(
            radius,
            target_order,
            uniform_refinement_rounds=resolution,
            node_vertex_consistency_tolerance=False,
        )
    else:
        raise ValueError(f"unsupported dimension: {ambient_dim}")

    from meshmode.discretization import Discretization
    from meshmode.discretization.poly_element import (
        InterpolatoryQuadratureSimplexGroupFactory,
    )

    pre_density_discr = Discretization(
        actx, mesh, InterpolatoryQuadratureSimplexGroupFactory(target_order)
    )

    from pytential.qbx import QBXLayerPotentialSource

    qbx = QBXLayerPotentialSource(
        pre_density_discr,
        fine_order=source_ovsmp * target_order,
        qbx_order=qbx_order,
        fmm_order=10,
        fmm_backend="sumpy",
        _disable_refinement=True,
    )

    from pytential import GeometryCollection

    dofdesc = sym.DOFDescriptor("qbx")
    places = GeometryCollection(qbx, auto_where=dofdesc)

    from pytential.qbx.refinement import refine_geometry_collection

    places = refine_geometry_collection(
        places, refine_discr_stage=sym.QBX_SOURCE_QUAD_STAGE2
    )

    density_discr = places.get_discretization(dofdesc.geometry)
    logger.info("nelements: %d", density_discr.mesh.nelements)
    logger.info("ndofs: %d", density_discr.ndofs)

    # }}}

    # {{{ symbolic

    from pytential.symbolic.stokes import StokesletWrapper

    method = "biharmonic" if ambient_dim == 2 else "laplace"
    stokeslet = StokesletWrapper(dim=ambient_dim, mu_sym=1, nu_sym=0.5, method=method)

    sym_viscosity_ratio = sym.var("lambda")
    sym_normal = sym.make_sym_vector("normal", ambient_dim)

    # surface tension coefficient
    sym_gamma = sym.var("gamma")
    # mean curvature
    sym_kappa = (ambient_dim - 1) / radius

    sym_sigma = sym.make_sym_vector("sigma", ambient_dim)
    sym_deltaf = sym_gamma * sym_kappa * sym_normal
    sym_velocity = stokeslet.apply(sym_sigma, qbx_forced_limit=+1)

    # }}}

    # {{{ evaluate

    normal = bind(places, sym.normal(ambient_dim).as_vector(), auto_where=dofdesc)(actx)
    context = {"lambda": 10, "gamma": 0.5, "normal": normal}

    sigma = bind(
        places, 2 * sym_deltaf / (1 + sym_viscosity_ratio), auto_where=dofdesc
    )(actx, **context)

    from pytential.symbolic.execution import BoundExpression, _prepare_expr

    _ = BoundExpression(
        places,
        _prepare_expr(places, sym_velocity, auto_where=dofdesc),
    )(actx, sigma=sigma, **context)

    # }}}


def set_caching_enabled(*, flag: bool = True) -> None:
    import loopy
    import sumpy

    sumpy.set_caching_enabled(flag)
    loopy.set_caching_enabled(flag)


def run_benchmark():
    from meshmode import _acf

    actx = _acf()
    set_caching_enabled(flag=False)

    from pytools import ProcessTimer

    min_wall = 10**10
    timings = []
    for _ in range(2):
        with ProcessTimer() as p:
            main(actx, ambient_dim=3)
            actx.queue.finish()

        timings.append(f"elapsed: {p}")
        min_wall = min(min_wall, p.wall_elapsed)
        print(timings[-1])

    for t in timings:
        print(t)


if __name__ == "__main__":
    # logging.basicConfig(level=logging.INFO)
    run_benchmark()
