# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

"""
This example is a simple benchmark for the various Stokes solvers, with and
without FMM.

Results

- 2021-10-14: using the FMM wins easily every time and the scalar kernels,
  i.e. biharmonic or Laplace, are also better. Code generation time was not
  taken into account.
"""

from dataclasses import dataclass, field
from typing import Any

import numpy as np  # noqa: F401

from pystopt import bind
from pystopt.tools import get_cl_array_context, get_default_logger

logger = get_default_logger(module=__file__)


@dataclass(frozen=True)
class BenchmarkParameters:
    target_order: int = 3
    fmm_order: bool | int = False

    @property
    def fmm_backend(self):
        if self.fmm_order:
            return "sumpy"

        return None

    @property
    def mesh_arguments(self):
        kwargs = getattr(self, "mesh_kwargs", {}).copy()
        kwargs["target_order"] = self.target_order
        kwargs["mesh_order"] = self.target_order

        return kwargs


def benchmark_velocity(ctx_factory, p: BenchmarkParameters) -> None:
    actx = get_cl_array_context(ctx_factory)

    # {{{ setup Stokes representation

    from pystopt.stokes.kernels import ModernStokeslet, Stokeslet

    if ModernStokeslet.is_available():
        knl = ModernStokeslet(ambient_dim=p.ambient_dim, method=p.method)
    elif p.method == "naive":
        knl = Stokeslet(p.ambient_dim)
    else:
        raise ValueError(f"unsupported Stokeslet method: '{p.method}'")

    from pystopt.stokes import get_uniform_farfield

    jmp = knl.get_jump_relations()
    bc = get_uniform_farfield(p.ambient_dim, uinf=1.0, pinf=0)

    from pystopt.stokes import TwoPhaseStokesRepresentation

    op = TwoPhaseStokesRepresentation(knl, jmp, bc)

    from pystopt.operators import make_sym_density
    from pystopt.stokes import sti

    sym_density = make_sym_density(op)
    sym_velocity = sti.velocity(op, sym_density, side=+1, qbx_forced_limit=+1)

    # }}}

    # {{{ run benchmark

    from pystopt.measure import EOCRecorder, stringify_eoc, visualize_eoc

    eoc_wall = EOCRecorder(name=f"{p.method}-wall")
    eoc_mean = EOCRecorder(name=f"{p.method}-mean")

    from pystopt.mesh import generate_discretization, get_mesh_generator_from_name

    for r in p.resolutions:
        from pystopt.qbx import QBXLayerPotentialSource

        g = get_mesh_generator_from_name(p.mesh_name, **p.mesh_arguments)
        pre_density_discr = generate_discretization(g, actx, resolution=r)
        qbx = QBXLayerPotentialSource(
            pre_density_discr,
            fine_order=p.source_ovsmp * p.target_order,
            qbx_order=p.qbx_order,
            fmm_order=p.fmm_order,
            fmm_backend=p.fmm_backend,
            _disable_refinement=True,
        )

        from pystopt.measure import timeit

        q = actx.thaw(pre_density_discr.nodes())
        result = timeit(
            lambda qbx=qbx, q=q: bind(qbx, sym_velocity)(actx, q=q),
            repeat=5,
        )

        logger.info("ndofs %6d: %s", pre_density_discr.ndofs, result)
        eoc_wall.add_data_point(pre_density_discr.ndofs, result.walltime)
        eoc_mean.add_data_point(pre_density_discr.ndofs, result.mean)

    logger.info("\n%s", stringify_eoc(eoc_wall, eoc_mean))
    visualize_eoc(
        f"stokes-kernel-benchmark-{p.method}-{repr(p.fmm_order).lower()}",
        eoc_wall,
        eoc_mean,
        order=1 if p.fmm_order else 2,
        abscissa="M",
        overwrite=True,
    )

    # }}}


# {{{ 2d benchmarks


@dataclass(frozen=True)
class Benchmark2DParameters(BenchmarkParameters):
    ambient_dim: int = 2
    method: str = "naive"

    target_order: int = 4
    source_ovsmp: int = 4
    qbx_order: int = 4

    mesh_name: str = "fourier_circle"
    mesh_kwargs: dict[str, Any] = field(default_factory=dict)
    resolutions: tuple[int, ...] = (
        16,
        32,
        48,
        64,
        96,
        128,
        160,
        192,
        224,
        256,
        320,
        384,
        448,
        512,
        640,
        768,
        896,
        1024,
    )


def benchmark_velocity_2d_naive(ctx_factory):
    p = Benchmark2DParameters(method="naive", fmm_order=False)
    benchmark_velocity(ctx_factory, p)

    p = Benchmark2DParameters(method="naive", fmm_order=10)
    benchmark_velocity(ctx_factory, p)


def benchmark_velocity_2d_biharmonic(ctx_factory):
    p = Benchmark2DParameters(method="biharmonic", fmm_order=False)
    benchmark_velocity(ctx_factory, p)

    p = Benchmark2DParameters(method="biharmonic", fmm_order=10)
    benchmark_velocity(ctx_factory, p)


# }}}


# {{{ 3d benchmarks


@dataclass(frozen=True)
class Benchmark3DParameters(BenchmarkParameters):
    ambient_dim: int = 3
    method: str = "naive"

    target_order: int = 3
    source_ovsmp: int = 4
    qbx_order: int = 4

    mesh_name: str = "spharm_sphere"
    mesh_kwargs: dict[str, Any] = field(default_factory=dict)
    resolutions: tuple[int, ...] = (8, 12, 16, 24, 32, 40)


def benchmark_velocity_3d_laplace(ctx_factory):
    p = Benchmark3DParameters(method="laplace", fmm_order=False)
    benchmark_velocity(ctx_factory, p)

    p = Benchmark3DParameters(method="laplace", fmm_order=10)
    benchmark_velocity(ctx_factory, p)


# }}}


def run(ctx_factory):
    actx = get_cl_array_context(ctx_factory)
    benchmark_velocity_2d_naive(actx)
    benchmark_velocity_2d_biharmonic(actx)

    # benchmark_velocity_3d_naive(actx)
    # benchmark_velocity_3d_biharmonic(actx)
    # benchmark_velocity_3d_laplace(actx)


if __name__ == "__main__":
    import sys

    import pyopencl as cl

    if len(sys.argv) > 1:
        exec(sys.argv[1])
    else:
        run(cl._csc)
