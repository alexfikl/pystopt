# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

from collections.abc import Callable
from typing import Any

import numpy as np

from arraycontext import ArrayContext
from pytools.obj_array import make_obj_array

from pystopt.tools import get_cl_array_context, get_default_logger

logger = get_default_logger(module=__file__)


# {{{ evolve


def evolve(
    x: np.ndarray,
    m: np.ndarray,
    n: np.ndarray,
    *,
    timestep: float,
    tmax: float,
    omega: float,
    alpha: float | None = None,
    p: int | None = None,
    method: str = "exact",
) -> np.ndarray:
    assert x.shape == (2, *m.shape)
    assert m.shape == n.shape

    if alpha and p:
        omega = omega / (1.0 + alpha * (n * (n + 1)) ** (2 * p))

    if method == "exact":
        mat = np.empty((2, 2, m.size))
        mat[0, 0] = +np.cos(omega * tmax)
        mat[0, 1] = -np.sin(omega * tmax)
        mat[1, 0] = +np.sin(omega * tmax)
        mat[1, 1] = +np.cos(omega * tmax)

        x = np.einsum("ijk,jk->ik", mat, x)
    else:
        raise ValueError(f"unknown method: '{method}'")

    return x


# }}}


# {{{ run


def run(
    ctx_factory_or_actx: Callable[[Any], ArrayContext],
    *,
    viscosity_ratio: float = 1,
    capillary_number: float = 1,
    spectral_discr: bool = False,
    overwrite: bool = True,
    visualize: bool = True,
) -> None:
    actx = get_cl_array_context(ctx_factory_or_actx)
    ambient_dim = 3

    # {{{ parameters

    offset = np.array([-2, 0, 0])
    omega = 4.0

    resolution = 24
    target_order = 5

    alphas = np.array([1, 2, 3, 4, 5, 6, 7, 8])
    ps = np.array([1, 2, 3, 4])

    timestep = 1.0e-2
    tmax = np.pi

    from meshmode.discretization.poly_element import InterpolatoryQuadratureGroupFactory

    group_factory = InterpolatoryQuadratureGroupFactory(target_order)

    from pystopt.mesh.poly_element import get_unit_nodes_for_group_factory

    unit_nodes = get_unit_nodes_for_group_factory(ambient_dim - 1, group_factory)

    # }}}

    # {{{ geometry

    def offset_spharm_ufo(theta, phi):
        from pystopt.mesh.generation import spharm_ufo

        r = spharm_ufo(theta, phi, k=1, a=1, b=1, aspect_ratio=1)

        return r + offset.reshape((-1, 1, 1))

    from pystopt.mesh import make_spharm_discretization

    discr, _ = make_spharm_discretization(
        actx,
        nellon=resolution,
        nellat=resolution,
        surface_fn=offset_spharm_ufo,
        order=target_order,
        mesh_unit_nodes=unit_nodes,
        group_factory=group_factory,
    )
    (sgrp,) = discr.specgroups

    logger.info("nelements: %d", discr.mesh.nelements)
    logger.info("ndofs:     %d", discr.ndofs)

    if visualize:
        from pystopt.visualization import make_visualizer

        vis = make_visualizer(actx, discr)
        vis.write_file("solid-body-rotation-before", [], overwrite=overwrite)

    # }}}

    # {{{ find modes

    from arraycontext import flatten

    xlm = [actx.to_numpy(flatten(discr.xlm[i], actx)) for i in range(discr.ambient_dim)]

    m = sgrp.sh.m
    n = sgrp.sh.l

    from functools import reduce

    indices = reduce(
        lambda x, y: x | y, [set(np.where(abs(xi) > 1.0e-8)[0]) for xi in xlm[:-1]]
    )
    indices = np.array(list(indices))
    logger.info("indices: %s", indices)

    m = sgrp.sh.m[indices]
    n = sgrp.sh.l[indices]

    # }}}

    from itertools import product

    for alpha_p, p in product(alphas, ps):
        alpha = 10.0 ** (-alpha_p)

        # {{{ evolve

        x = evolve(
            np.stack([xlm[0][indices], xlm[1][indices]]),
            m,
            n,
            timestep=timestep,
            tmax=tmax,
            omega=omega,
            alpha=alpha,
            p=p,
            method="exact",
        )

        new_xlm = [np.copy(xlm[i]) for i in range(discr.ambient_dim)]
        new_xlm[0][indices] = x[0]
        new_xlm[1][indices] = x[1]

        from arraycontext import unflatten

        new_xlm = make_obj_array([
            unflatten(actx.thaw(discr.xlm[i]), actx.from_numpy(new_xlm[i]), actx)
            for i in range(discr.ambient_dim)
        ])

        # }}}

        # {{{ reconstruct mesh

        from pystopt.mesh import update_discr_from_spectral

        new_discr = update_discr_from_spectral(actx, discr, new_xlm)

        if visualize:
            from pystopt.visualization import make_visualizer

            vis = make_visualizer(actx, new_discr)
            vis.write_file(
                f"solid-body-rotation-after-alpha-{alpha_p}{p}",
                [],
                overwrite=overwrite,
            )

        # }}}


# }}}


if __name__ == "__main__":
    import sys

    import pyopencl as cl

    if len(sys.argv) > 1:
        exec(sys.argv[1])
    else:
        run(cl._csc)
