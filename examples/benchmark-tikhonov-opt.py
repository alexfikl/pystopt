# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

"""
This example is a simple benchmark of the various optimization methods
used by :func:`pystopt.filtering.spharm.apply_filter_spharm_tikhonov_opt`.

Results:

- 2021-10-14: the `minimize.SLSQP` and `minimize_scalar.bounded` ones seem to be
  faster for the sizes we are looking at. None of them seem to become slower
  with more points.
"""

from pystopt import bind, sym
from pystopt.tools import get_cl_array_context, get_default_logger

logger = get_default_logger(module=__file__)


def benchmark_tikhonov_opt(actx, eoc, *, optimize: str, method: str):
    from pystopt.mesh import generate_discretization, get_mesh_generator_from_name

    mesh_arguments = {
        "target_order": 3,
        "mesh_order": 3,
    }
    resolutions = [8, 16, 24, 32, 40, 48]

    from pystopt.filtering import apply_filter_spharm_tikhonov_opt

    for r in resolutions:
        g = get_mesh_generator_from_name("spharm_spheroid", **mesh_arguments)
        discr = generate_discretization(g, actx, resolution=r)

        from pystopt.dof_array import uniform_like

        sym_normal = sym.normal(discr.ambient_dim).as_vector()
        f = bind(discr, sym_normal[0])(actx)
        f = f + 0.1 * uniform_like(actx, f)
        fk = discr.to_spectral_conn(f)

        from pystopt.measure import timeit

        result = timeit(
            lambda discr=discr, fk=fk: apply_filter_spharm_tikhonov_opt(
                actx,
                discr,
                fk,
                p=2,
                optimize=optimize,
                method=method,
                maxiter=512,
            ),
            repeat=5,
        )

        logger.info("ndofs %6d: %s", discr.ndofs, result)
        eoc.add_data_point(discr.ndofs, result.mean)

    return eoc


def run(actx_factory, *, visualize: bool = True):
    actx = get_cl_array_context(actx_factory)

    methods = [
        ("minimize_scalar", "bounded"),
        ("minimize", "L-BFGS-B"),
        ("minimize", "TNC"),
        ("minimize", "SLSQP"),
    ]

    from pystopt.measure import EOCRecorder

    eocs = [EOCRecorder(name=name) for _, name in methods]

    for i, (optimize, method) in enumerate(methods):
        benchmark_tikhonov_opt(actx, eocs[i], optimize=optimize, method=method)

    from pystopt.measure import stringify_eoc, visualize_eoc

    logger.info("\n%s", stringify_eoc(*eocs))

    if not visualize:
        return

    visualize_eoc(
        "benchmark-tikhonov-opt",
        *eocs,
        abscissa="M",
        ylabel="Mean~Time~(s)",
        overwrite=True,
        order=False,
    )


if __name__ == "__main__":
    import sys

    import pyopencl as cl

    if len(sys.argv) > 1:
        exec(sys.argv[1])
    else:
        run(cl._csc)
