# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

r"""
Analytical solution for two-phase spheroidal droplets from

    .. [1] M. Zabarankin, I. Smagin, O. M. Lavrenteva, A. Nir,
    *Viscous Drop in Compressional Stokes Flow*,
    Journal of Fluid Mechanics, Vol. 720, pp. 169--191, 2013,
    `DOI https://doi.org/10.1017/jfm.2013.6`__.

TODO: `sympy` doesn't seem able to handle this, so we need to move to just
explicitly computing everything
"""

import sympy as sp

from pystopt.tools import get_default_logger

logger = get_default_logger(__file__)


def legendre_q(n: int, x: sp.Expr) -> sp.Expr:
    """
    :returns: :math:`Q_n(x)`, the Legendre function of the second kind of order
        :math:`n`.
    """
    q0 = sp.atanh(x)
    if n == 0:
        return q0

    q1 = x * q0 - 1
    if n == 1:
        return q1

    for i in range(2, n + 1):
        q0, q1 = q1, sp.Rational(2 * i - 1, i) * x * q1 - sp.Rational(i - 1, i) * q0

    return q1


def assoc_legendre_q(n: int, m: int, x: sp.Expr) -> sp.Expr:
    """
    :returns: :math:`Q^m_n(x)`, the associated Legendre function of the second
        kind of order :math:`n` and degree :math:`m`.
    """
    assert m == 1
    if n == 0:
        return -1 / sp.sqrt(1 - x**2)

    # NOTE: in the case m == 1, we have that
    #   Q^1_n = sqrt{1 - x^2} d/dx Q_n(x)
    #         = n / sqrt{1 - x^2} * (Q_{n - 1}(x) - x Q_n(x))

    q0 = sp.atanh(x)
    q1 = x * q0 - 1
    for i in range(2, n + 1):
        q0, q1 = q1, sp.Rational(2 * i - 1, i) * x * q1 - sp.Rational(i - 1, i) * q0

    # FIXME: this does not match Mathematica
    return n / sp.sqrt(1 - x**2) * (q0 - x * q1)


def main(nterms: int = 4):
    # {{{ setup

    # spheroidal coordinates
    # xi \in [0, infty], eta \in [0, pi], \phi \in [0, 2 pi]
    eta = sp.Symbol("eta", real=True, positive=True)
    # xi = sp.Symbol("xi", real=True, positive=True)
    # phi = sp.Symbol("phi", real=True, positive=True)

    # parameters
    # viscosity ratio
    vlambda = sp.Symbol("lambda", real=True, positive=True)
    # capillary number
    ca = sp.Symbol("Ca", real=True, positive=True)

    # spheroid surface at fixed xi == s
    s = sp.Symbol("s", real=True, positive=True)
    # fix focal axis by setting volume same as unit sphere
    c = 1 / (sp.cosh(s) ** 2 * sp.sinh(s)) ** 3

    # curvature
    kappa = (
        sp.tanh(s) / sp.sqrt(sp.sinh(s) ** 2 + sp.cos(eta) ** 2)
        + sp.sinh(s) * sp.cosh(s) / sp.sqrt(sp.sinh(s) ** 2 + sp.cos(eta) ** 2) ** 3
    ) / c

    # }}}

    # {{{ determine terms

    indices = tuple([2 * k + 1 for k in range(nterms)])

    # {{{ legendre coefficients

    xi_p, xi_m = [None] * nterms, [None] * nterms
    theta_p, theta_m = [None] * nterms, [None] * nterms

    for i, n in enumerate(indices):
        logger.info("legendre %4d %4d", i, n)

        # equation (4.10a)
        xi_p[i] = sp.simplify(sp.re(sp.I**n * sp.legendre(n, sp.I * sp.sinh(s))))
        theta_p[i] = sp.simplify(
            sp.re(sp.I ** (n + 1) * sp.assoc_legendre(n, 1, sp.I * sp.sinh(s)))
        )

        # equation (4.10b)
        xi_m[i] = sp.simplify(sp.re(sp.I ** (n + 1) * legendre_q(n, sp.I * sp.sinh(s))))
        theta_m[i] = sp.simplify(
            sp.re(sp.I**n * assoc_legendre_q(n, 1, sp.I * sp.sinh(s)))
        )

    logger.info("computed Legendre function coefficients...")

    # }}}

    # {{{ delta coefficients

    delta_p, delta_m = [None] * nterms, [None] * nterms
    ddelta_p, ddelta_m = [None] * nterms, [None] * nterms
    d = [None] * nterms

    for i, n in enumerate(indices):
        logger.info("delta %4d %4d", i, n)

        # equation (4.6)
        delta_p[i] = sp.simplify(
            (sp.sinh(s) ** 2 - 1) * xi_p[i] * theta_p[i]
            + sp.sinh(2 * s) / 2 * (theta_p[i] ** 2 - n * (n + 1) * xi_p[i] ** 2)
        )
        delta_m[i] = sp.simplify(
            (sp.sinh(s) ** 2 - 1) * xi_m[i] * theta_m[i]
            + sp.sinh(2 * s) / 2 * (theta_m[i] ** 2 - n * (n + 1) * xi_m[i] ** 2)
        )

        # equation (4.7)
        ddelta_p[i] = sp.simplify(
            n
            * (n + 1)
            * (
                delta_p[i]
                + theta_p[i] * (2 * xi_p[i] - sp.tanh(s) * theta_p[i] / n / (n + 1))
            )
        )
        ddelta_m[i] = sp.simplify(
            n
            * (n + 1)
            * (
                delta_m[i]
                + theta_m[i] * (2 * xi_m[i] - sp.tanh(s) * theta_m[i] / n / (n + 1))
            )
        )

        # equation (4.5)
        d[i] = (
            (1 - 1 / vlambda) * delta_p[i] * ddelta_m[i]
            + (1 - vlambda) * delta_m[i] * ddelta_p[i] / n / (n + 1)
            - 1 / sp.cosh(s) ** 2
        )

    logger.info("computed delta coefficients...")

    # }}}

    # {{{ integral coefficients

    f, g = [None] * nterms, [None] * nterms
    psi_p, psi_m = [None] * nterms, [None] * nterms

    for i, n in enumerate(indices):
        logger.info("integral %4d %4d", i, n)

        # equation (4.8)
        kr = sp.simplify(
            kappa
            * c
            * sp.sinh(s)
            * sp.sin(eta)
            * sp.sin(eta)
            * sp.assoc_legendre(1, n, sp.cos(eta))
        )
        f[i] = (
            (2 * n + 1) / (2 * c * ca * n * (n + 1)) * sp.integrate(kr, (eta, 0, sp.pi))
        )

        # equation (4.9)
        kz = sp.simplify(
            kappa
            * c
            * sp.cosh(s)
            * sp.cos(eta)
            * sp.sin(eta)
            * sp.legendre(n, sp.cos(eta))
        )
        g[i] = (2 * n + 1) / (2 * c * ca) * sp.integrate(kz, (eta, 0, sp.pi))

        # equation (4.4)
        psi_p[i] = (
            +c
            / (2 * d[i])
            * (
                (-1) ** n / vlambda * (theta_m[i] * f[i] + xi_m[i] * g[i])
                + (1 - 1 / vlambda)
                * delta_m[i]
                * sp.cosh(s)
                * (theta_p[i].diff(s) * g[i] / n / (n + 1) - theta_p[i] * f[i])
            )
        )
        psi_m[i] = (
            -c
            / (2 * d[i])
            * (
                (-1) ** n * (theta_p[i] * f[i] + xi_p[i] * g[i])
                + (1 - 1 / vlambda)
                * delta_p[i]
                * sp.cosh(s)
                * (theta_m[i].diff(s) * g[i] / n / (n + 1) - theta_m[i] * f[i])
            )
        )

    logger.info("computed integral coefficients...")

    # }}}

    # {{{ expansion coefficients

    a, b = [None] * nterms, [None] * nterms

    # equation (4.3a)
    xi_m2 = sp.I**3 * legendre_q(2, sp.I * sp.sinh(s))
    a[0] = sp.simplify(
        c
        * xi_m2
        * sp.cosh(s)
        / (2 * d[0])
        * (6 * (1 - vlambda) * sp.sinh(s) + f[0] + g[0] * sp.tanh(s))
    )
    b[0] = 2 * a[0] * sp.tanh(s)

    for i, n in enumerate(indices[1:]):
        # equation (4.3b)
        a[i + 1] = (
            (-1) ** (n + 1)
            / (n + 1)
            / (n + 2)
            * (
                theta_p[i + 1] * delta_m[i + 1] * psi_m[i + 1]
                + theta_m[i + 1] * delta_p[i + 1] * psi_p[i + 1]
            )
        )

        # equation (4.3c)
        b[i + 1] = (-1) ** (n + 1) * (
            xi_p[i + 1] * delta_m[i + 1] * psi_m[i + 1]
            + xi_m[i + 1] * delta_p[i + 1] * psi_p[i + 1]
        )

    logger.info("computed series coefficients...")

    # }}}

    # }}}


if __name__ == "__main__":
    main()
