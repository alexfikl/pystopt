# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

"""
This examples showcases the errors due to the spectral transform from and to
the quadrature grid.
"""

import pathlib

import numpy as np

from pystopt import bind, sym
from pystopt.tools import get_cl_array_context, get_default_logger

logger = get_default_logger(module=__file__)


def run(
    ctx_factory,
    *,
    ambient_dim: int = 2,
    overwrite: bool = True,
    visualize: bool = True,
) -> None:
    actx = get_cl_array_context(ctx_factory)

    # {{{ geometry

    if ambient_dim == 2:
        mesh_name = "fourier_ellipse"
        mesh_arguments = {
            "target_order": 4,
            "mesh_order": 4,
            "radius": 1,
            "aspect_ratio": 1,
        }
        r = 64
    elif ambient_dim == 3:
        mesh_name = "spharm_spheroid"
        mesh_arguments = {
            "target_order": 3,
            "mesh_order": 3,
            "radius": 1,
            "aspect_ratio": 1,
        }
        # mesh_name = "spharm_ufo"
        # mesh_arguments = {
        #     "target_order": 3, "mesh_order": 3,
        #     "k": 1, "a": 1, "b": 1,
        # }
        r = 32
    else:
        raise ValueError

    from pystopt.mesh import generate_discretization, get_mesh_generator_from_name

    g = get_mesh_generator_from_name(mesh_name, **mesh_arguments)
    discr = generate_discretization(g, actx, resolution=r)

    logger.info("nelements: %d", discr.mesh.nelements)
    logger.info("ndofs:     %d", discr.ndofs)
    logger.info("nspec:     %d", discr.nspec)

    # }}}

    # {{{ filter normal

    # compute normal
    normal = bind(discr, sym.normal(ambient_dim).as_vector())(actx)
    nx = normal[0]

    from pystopt.filtering import apply_filter_spectral

    if ambient_dim == 2:
        kwargs = {"method": "ideal", "kmax": r // 2 - 1}
    else:
        kwargs = {
            "method": "tikhonov_ideal",
            "alpha": 1.0e-5,
            "p": 2,
            "kmax": r // 2 - 1,
        }

    nx_filtered = apply_filter_spectral(actx, discr, nx, **kwargs)

    # }}}

    from pystopt.paths import get_filename

    basename = get_filename(f"{mesh_name}_normal", cwd=pathlib.Path(__file__).parent)
    data = {}

    from arraycontext import flatten

    nx_hat = actx.to_numpy(flatten(discr.to_spectral_conn(nx), actx))
    nx_filtered_hat = actx.to_numpy(flatten(discr.to_spectral_conn(nx_filtered), actx))

    (specgrp,) = discr.specgroups
    if ambient_dim == 2:
        data = {
            "nx_hat": nx_hat,
            "nx_filtered_hat": nx_filtered_hat,
            "k": specgrp.fftfreq.ravel(order="A"),
        }
    else:
        from pystopt.mesh.spharm import _make_spharm_group_indices

        n, m = _make_spharm_group_indices(specgrp)

        from pystopt.mesh.spharm import _make_spharm_group_array

        nlm = _make_spharm_group_array(specgrp)
        nlm = nlm + 1j * nlm

        nlm[n, m] = nx_hat
        nx_hat = nlm.copy()
        nlm[n, m] = nx_filtered_hat
        nx_filtered_hat = nlm.copy()

        data = {
            "nx_hat": nx_hat,
            "nx_filtered_hat": nx_filtered_hat,
            "n": n,
            "m": m,
        }

    np.savez(basename.aspath().with_suffix(".npz"), **data)

    if not visualize:
        return

    from pystopt.visualization import make_visualizer

    vis = make_visualizer(actx, discr, vis_order=mesh_arguments["target_order"])

    vis.write_file(
        basename,
        [
            ("n_x", nx),
            (r"\hat{n}_x", nx_filtered),  # noqa: RUF027
        ],
        overwrite=overwrite,
    )

    if ambient_dim == 2:
        from pystopt.mesh.fourier import visualize_fourier_modes

        visualize_fourier_modes(
            basename.with_suffix("modes"),
            actx,
            discr,
            [("n_x", nx)],
            overwrite=overwrite,
        )
        visualize_fourier_modes(
            basename.with_suffix("filtered_modes"),
            actx,
            discr,
            [("n_x", nx_filtered)],
            overwrite=overwrite,
        )
    else:
        from pystopt.mesh.spharm import visualize_spharm_modes

        visualize_spharm_modes(
            basename.with_suffix("modes"),
            discr,
            [("{n_x}", nx)],
            overwrite=overwrite,
        )
        visualize_spharm_modes(
            basename.with_suffix("filtered_modes"),
            discr,
            [("{n_x}", nx_filtered)],
            overwrite=overwrite,
        )


if __name__ == "__main__":
    import sys

    import pyopencl as cl

    if len(sys.argv) > 1:
        exec(sys.argv[1])
    else:
        run(cl._csc)
