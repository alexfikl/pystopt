# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

from dataclasses import dataclass, replace
from typing import Any

import numpy as np

from pytools import memoize_method

from pystopt import bind, sym
from pystopt.simulation import as_state_container
from pystopt.tools import get_cl_array_context, get_default_logger

logger = get_default_logger(module=__file__)


# {{{ cost and gradient


@dataclass(frozen=True)
class StokesShapeGradientWrangler:
    places: Any
    dofdesc: Any

    cost: Any
    op: Any
    ad: Any
    context: dict[str, Any]
    gmres_arguments: dict[str, Any]

    @property
    def ambient_dim(self):
        return self.places.ambient_dim

    @property
    def discr(self):
        return self.places.get_discretization(
            self.dofdesc.geometry, self.dofdesc.discr_stage
        )

    def solve_stokes(self, actx, op, places, context=None):
        if context is None:
            context = {}
        context = {**self.context, **context}

        from pystopt.stokes import single_layer_solve

        return single_layer_solve(
            actx,
            places,
            op,
            gmres_arguments=self.gmres_arguments,
            lambdas={"viscosity_ratio": context["viscosity_ratio"]},
            sources=[self.dofdesc],
            context=context,
        )

    def get_velocity(self, solution):
        from pystopt.stokes import sti

        return sti.velocity(
            solution, side=None, qbx_forced_limit=+1, dofdesc=self.dofdesc
        )

    def compute_cost(self, actx, places, context=None):
        if context is None:
            context = {}
        context = {**self.context, **context}

        r = bind(
            places,
            self.cost(places.ambient_dim, dofdesc=self.dofdesc),
            auto_where=self.dofdesc,
        )(actx, **context)

        return actx.to_numpy(r)

    def compute_grad(self, actx, places, context=None):
        if context is None:
            context = {}
        context = {**self.context, **context}

        from pystopt.simulation.staticstokes import make_sym_grad_static_stokes

        return bind(
            places,
            make_sym_grad_static_stokes(self, context=context),
            auto_where=self.dofdesc,
        )(actx, **context)


@as_state_container
class StokesShapeGradientState:
    x: np.ndarray
    wrangler: StokesShapeGradientWrangler

    # {{ forwarding

    @property
    def array_context(self):
        return self.x[0].array_context

    @property
    def dofdesc(self):
        return self.wrangler.dofdesc

    # }}}

    # {{{ solve

    @property
    @memoize_method
    def places(self):
        from pystopt.simulation.state import reconstruct_discretization

        discr = reconstruct_discretization(
            self.array_context, self.wrangler.discr, self.x, keep_vertices=True
        )

        return self.wrangler.places.merge({self.wrangler.dofdesc.geometry: discr})

    @property
    def _forward_solution(self):
        return self.wrangler.solve_stokes(
            self.array_context, self.wrangler.op, self.places
        )

    @property
    def _adjoint_solution(self):
        context = {
            "q": self._forward_solution.density,
            "u": self._forward_velocity,
        }

        r = self.wrangler.solve_stokes(
            self.array_context, self.wrangler.ad, self.places, context=context
        )
        return replace(r, density_name="qstar")

    @property
    @memoize_method
    def _forward_velocity(self):
        return self.wrangler.get_velocity(self._forward_solution)

    @property
    @memoize_method
    def _adjoint_velocity(self):
        return self.wrangler.get_velocity(self._adjoint_solution)

    # }}}

    # {{{ optim

    @property
    @memoize_method
    def cost(self):
        context = {
            "q": self._forward_solution.density,
            "u": self._forward_velocity,
        }
        return self.wrangler.compute_cost(
            self.array_context, self.places, context=context
        )

    @property
    @memoize_method
    def grad(self):
        context = {
            "q": self._forward_solution.density,
            "u": self._forward_velocity,
            "qstar": self._adjoint_solution.density,
            "ustar": self._adjoint_velocity,
        }

        return self.wrangler.compute_grad(
            self.array_context, self.places, context=context
        )

    # }}}


# }}}


# {{{ run


def run(
    ctx_factory_or_actx,
    *,
    viscosity_ratio: float = 1,
    capillary_number: float = 1,
    spectral_discr: bool = False,
    overwrite: bool = True,
    visualize: bool = True,
) -> None:
    actx = get_cl_array_context(ctx_factory_or_actx)
    ambient_dim = 3

    # {{{ parameters

    radius = 1

    resolution = 24
    uniform_refinement_rounds = 4
    target_order = 5
    source_ovsmp = 4

    qbx_order = 4
    fmm_order = 10
    fmm_backend = "sumpy"

    from meshmode.discretization.poly_element import InterpolatoryQuadratureGroupFactory

    group_factory = InterpolatoryQuadratureGroupFactory(target_order)

    from pystopt.mesh.poly_element import get_unit_nodes_for_group_factory

    unit_nodes = get_unit_nodes_for_group_factory(ambient_dim - 1, group_factory)

    epsilons = 10.0 ** (-np.arange(1, 7))
    # epsilons = 10.0 ** (-np.arange(1, 4))
    sigma = 1.0e-1

    # }}}

    # {{{ geometry

    if spectral_discr:
        from pystopt.mesh import make_spharm_discretization
        from pystopt.mesh.generation import spharm_sphere

        pre_density_discr, _ = make_spharm_discretization(
            actx,
            nellon=resolution,
            nellat=resolution,
            surface_fn=spharm_sphere,
            order=target_order,
            mesh_unit_nodes=unit_nodes,
            group_factory=group_factory,
        )
    else:
        from meshmode.mesh import TensorProductElementGroup
        from meshmode.mesh.generation import generate_sphere

        mesh = generate_sphere(
            radius,
            target_order,
            uniform_refinement_rounds=uniform_refinement_rounds,
            node_vertex_consistency_tolerance=False,
            unit_nodes=unit_nodes,
            group_cls=TensorProductElementGroup,
        )

        from meshmode.discretization import Discretization

        pre_density_discr = Discretization(actx, mesh, group_factory)

    from pystopt.qbx import QBXLayerPotentialSource

    qbx = QBXLayerPotentialSource(
        pre_density_discr,
        fine_order=source_ovsmp * target_order,
        qbx_order=qbx_order,
        fmm_order=fmm_order,
        fmm_backend=fmm_backend,
        _disable_refinement=True,
    )

    from pytential import GeometryCollection

    places = GeometryCollection(qbx, auto_where="shape")
    dofdesc = places.auto_source

    density_discr = places.get_discretization(dofdesc.geometry, dofdesc.discr_stage)

    logger.info("nelements: %d", density_discr.mesh.nelements)
    logger.info("ndofs:     %d", density_discr.ndofs)

    # }}}

    # {{{ cost

    from pystopt.cost import NormalVelocityFunctional

    cost = NormalVelocityFunctional(
        u=sym.make_sym_vector("u", ambient_dim), ud=sym.var("ud"), dofdesc=None
    )

    # }}}

    # {{{ stokes

    from pystopt.stokes import get_farfield_boundary_from_name

    bc = get_farfield_boundary_from_name(
        ambient_dim, "extensional", alpha=1.0, axis=2, capillary_number_name="ca"
    )

    from pystopt.stokes import TwoPhaseSingleLayerStokesRepresentation

    op = TwoPhaseSingleLayerStokesRepresentation(bc)

    from pystopt.stokes import make_static_adjoint_boundary_conditions

    bc = make_static_adjoint_boundary_conditions(ambient_dim, cost)
    ad = TwoPhaseSingleLayerStokesRepresentation(bc)

    context = {
        "viscosity_ratio": viscosity_ratio,
        "ca": capillary_number,
        "ud": 0,
    }

    # }}}

    # {{{ compare to finite difference

    wrangler = StokesShapeGradientWrangler(
        places=places,
        dofdesc=places.auto_source,
        cost=cost,
        op=op,
        ad=ad,
        context=context,
        gmres_arguments={},
    )
    rng = np.random.default_rng(42)

    x = actx.thaw(density_discr.nodes())
    state = StokesShapeGradientState(x, wrangler)

    grad = state.grad
    g_dot_n = bind(places, sym.n_dot(places.ambient_dim), auto_where=dofdesc)(
        actx, x=grad
    )

    if visualize:
        name = "spharm" if spectral_discr else "quads"

        from pystopt.visualization import make_visualizer

        vis = make_visualizer(actx, density_discr)
        vis.write_file(
            f"stokes-shape-gradient-{name}",
            [
                ("grad", grad),
                ("g_dot_n", g_dot_n),
            ],
            overwrite=overwrite,
        )

    from pystopt.measure import EOCRecorder, stringify_eoc

    eoc = EOCRecorder()

    from pystopt.finite import generate_random_dofs

    indices = generate_random_dofs(density_discr, 5, rng=rng)

    from pystopt.finite import compare_finite_difference_shape_gradient

    for eps in epsilons:
        r = compare_finite_difference_shape_gradient(
            state, grad=g_dot_n, eps=eps, sigma=sigma, indices=indices
        )

        grad_ad = actx.to_numpy(r.grad_ad)
        grad_fd = actx.to_numpy(r.grad_fd)
        logger.info("errors: %s", abs(grad_ad - grad_fd) / abs(grad_fd))

        eoc.add_data_point(eps, r.error)

    logger.info("\n%s", stringify_eoc(eoc))

    # }}}


# }}}


if __name__ == "__main__":
    import sys

    import pyopencl as cl

    if len(sys.argv) > 1:
        exec(sys.argv[1])
    else:
        run(cl._csc)
