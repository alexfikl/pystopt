# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

from functools import partial

import numpy as np

import meshmode.discretization.poly_element as mpoly
from arraycontext import ArrayContext
from meshmode.discretization import Discretization
from pytential import GeometryCollection

from pystopt import sym
from pystopt.tools import get_cl_array_context, get_default_logger

logger = get_default_logger(module=__file__)


# {{{ setup


def make_discretization(
    actx: ArrayContext,
    ambient_dim: int,
    *,
    target_order: int = 3,
    group_factory_cls: type | None = None,
) -> Discretization:
    if group_factory_cls is None:
        group_factory_cls = mpoly.InterpolatoryQuadratureGroupFactory

    if ambient_dim == 2:
        mesh_name = "fourier_ellipse"
        resolution = 128
    elif ambient_dim == 3:
        mesh_name = "spharm_spheroid"
        resolution = 56
    else:
        raise ValueError(f"unsupported dimension: {ambient_dim}")

    from pystopt.mesh import generate_discretization, get_mesh_generator_from_name

    mesh_arguments = {
        "target_order": target_order,
        "mesh_order": target_order,
        "mesh_unit_nodes": True,
        "use_default_mesh_unit_nodes": True,
        "group_factory_cls": group_factory_cls,
    }

    g = get_mesh_generator_from_name(mesh_name, **mesh_arguments)
    return generate_discretization(g, actx, resolution)


def make_geometry_collection(
    actx: ArrayContext,
    ambient_dim: int,
    *,
    target_order: int = 3,
    qbx_order: int = 4,
    source_ovsmp: int = 4,
    group_factory_cls: type | None = None,
) -> GeometryCollection:
    from pytential.qbx import QBXLayerPotentialSource

    pre_density_discr = make_discretization(
        actx,
        ambient_dim,
        target_order=target_order,
        group_factory_cls=group_factory_cls,
    )

    qbx = QBXLayerPotentialSource(
        pre_density_discr,
        fine_order=source_ovsmp * target_order,
        qbx_order=qbx_order,
        fmm_order=10,
        fmm_backend="sumpy",
        _disable_refinement=True,
    )

    return GeometryCollection(qbx)


def make_stokes_gradient(
    ambient_dim: int,
    capillary_number: float = 0.05,
    viscosity_ratio: float = 1.0,
) -> None:
    pass


def to_fourier_spectral(actx, discr, x):
    def to_spectral(sgrp, ix):
        ix = actx.to_numpy(ix).reshape(sgrp.sh.spat_shape, order="F")

        return actx.from_numpy(np.fft.fft2(ix))

    from pystopt.dof_array import SpectralDOFArray

    conn = discr.to_spectral_conn.connections[0]
    return SpectralDOFArray(
        actx,
        tuple([
            to_spectral(sgrp, ix)
            for ix, sgrp in zip(conn(x), discr.specgroups, strict=False)
        ]),
    )


def from_fourier_spectral(actx, discr, xk):
    def from_spectral(sgrp, ixk):
        ixk = np.fft.ifft2(actx.to_numpy(ixk)).real

        return actx.from_numpy(ixk.reshape(sgrp.spat_shape, order="F").copy())

    from pystopt.dof_array import DOFArray

    x = DOFArray(
        actx,
        tuple([
            from_spectral(sgrp, ixk)
            for ixk, sgrp in zip(xk, discr.specgroups, strict=False)
        ]),
    )

    conn = discr.from_spectral_conn.connections[1]
    return conn(x)


# }}}


def _run_spharm(
    actx_factory,
    func_factory,
    *,
    suffix: str = "spharm",
    spharm_m: int = -1,
    spharm_n: int = -1,
    visualize: bool = True,
) -> None:
    actx = get_cl_array_context(actx_factory)

    # {{{ geometry

    places = make_geometry_collection(
        actx, ambient_dim=3, target_order=3, qbx_order=4, source_ovsmp=4
    )
    equi_discr = make_discretization(
        actx,
        ambient_dim=3,
        target_order=3,
        group_factory_cls=mpoly.InterpolatoryEquidistantGroupFactory,
    )
    places = places.merge({"equi": equi_discr})
    assert places.ambient_dim == 3, places.ambient_dim

    dofdesc = places.auto_source
    x = func_factory(actx, places, dofdesc)

    dofequi = sym.as_dofdesc("equi")
    equi_x = func_factory(actx, places, dofequi)

    # }}}

    # {{{ perform transforms

    discr = places.get_discretization(dofdesc.geometry, dofdesc.discr_stage)
    equi_discr = places.get_discretization(dofequi.geometry, dofequi.discr_stage)

    xlm = discr.to_spectral_conn(x)
    equi_xlm = equi_discr.to_spectral_conn(equi_x)

    xkm = to_fourier_spectral(actx, discr, x)
    equi_xkm = to_fourier_spectral(actx, equi_discr, equi_x)
    fourier_xkm = to_fourier_spectral(actx, discr, discr.from_spectral_conn(xlm))

    from pystopt.dof_array import dof_array_rnorm

    spharm_error = actx.to_numpy(dof_array_rnorm(xlm, equi_xlm, ord=np.inf))
    fourier_error = actx.to_numpy(dof_array_rnorm(xkm, equi_xkm, ord=np.inf))
    fourier_rt_error = actx.to_numpy(dof_array_rnorm(fourier_xkm, equi_xkm, ord=np.inf))

    logger.info(
        "error: spharm %.5e fourier %.5e %.5e",
        spharm_error,
        fourier_error,
        fourier_rt_error,
    )

    xs = discr.from_spectral_conn(xlm)
    xf = from_fourier_spectral(actx, discr, xkm)

    spharm_error = actx.to_numpy(dof_array_rnorm(x, xs, ord=np.inf))
    fourier_error = actx.to_numpy(dof_array_rnorm(x, xf, ord=np.inf))
    logger.info("roundtrip: spharm %.5e fourier %.5e", spharm_error, fourier_error)

    # }}}

    # {{{ filter fourier modes

    def _filter_fourier_modes(sgrp, ixk):
        # ktheta = np.fft.fftfreq(ixk.shape[0], d=1.0 / ixk.shape[0])
        kphi = np.fft.fftfreq(ixk.shape[1], d=1.0 / ixk.shape[1])

        mphi = sgrp.nelphi // 2
        mask = (kphi < -mphi) | (kphi > mphi)

        ixk = actx.to_numpy(ixk)
        ixk[:, mask] = 0.0

        return actx.from_numpy(ixk)

    from pystopt.dof_array import SpectralDOFArray

    xkm_filtered = SpectralDOFArray(
        actx,
        tuple([
            _filter_fourier_modes(sgrp, ixk)
            for ixk, sgrp in zip(xkm, discr.specgroups, strict=False)
        ]),
    )

    fourier_x = from_fourier_spectral(actx, discr, xkm_filtered)

    # }}}

    if not visualize:
        return

    from pystopt.paths import make_dirname

    todaydir = make_dirname("output", today=True)
    dirname = make_dirname("stokes-interpolation-filtering", today=False, cwd=todaydir)

    from pystopt.paths import get_filename

    visualize_file_name = get_filename(
        f"visualize-{discr.ndofs:06d}-{suffix}", cwd=dirname
    )

    # {{{ plot values

    xroundtrip = discr.from_spectral_conn(discr.to_spectral_conn(x))
    error = actx.np.log10(actx.np.abs(x - xroundtrip) + 1.0e-16)
    fourier_error = actx.np.log10(actx.np.abs(x - fourier_x) + 1.0e-16)

    from pystopt.visualization import make_visualizer

    vis = make_visualizer(actx, discr)

    filename = visualize_file_name.aspath()
    vis.write_file(
        filename,
        [
            ("x", x),
            ("xhat", xroundtrip),
            ("error", error),
            ("fourier", fourier_error),
        ],
        overwrite=True,
    )

    # }}}

    # {{{ plot spharm modes

    from pystopt.mesh.spharm import visualize_spharm_modes

    filename = visualize_file_name.with_suffix("spharm_quadrature")
    visualize_spharm_modes(filename, discr, [("f", xlm)], mode="imshow", overwrite=True)

    filename = visualize_file_name.with_suffix("spharm_equidistant")
    visualize_spharm_modes(
        filename, equi_discr, [("f", equi_xlm)], mode="imshow", overwrite=True
    )

    filename = visualize_file_name.with_suffix("spharm_error")
    visualize_spharm_modes(
        filename, discr, [("e", xlm - equi_xlm)], mode="byorder", overwrite=True
    )

    # }}}

    # {{{ plot fourier modes

    assert len(xkm) == 1
    xkm = actx.to_numpy(xkm[0])
    equi_xkm = actx.to_numpy(equi_xkm[0])
    fourier_xkm = actx.to_numpy(fourier_xkm[0])

    ktheta = np.fft.fftfreq(xkm.shape[0], d=1.0 / xkm.shape[0])
    kphi = np.fft.fftfreq(xkm.shape[1], d=1.0 / xkm.shape[1])

    # reorder modes for visualization
    itheta = np.argsort(ktheta)
    iphi = np.argsort(kphi)

    ktheta = ktheta[itheta]
    kphi = kphi[iphi]
    extent = [ktheta[0], ktheta[-1], kphi[0], kphi[-1]]

    xkm = xkm[np.ix_(itheta, iphi)]
    equi_xkm = equi_xkm[np.ix_(itheta, iphi)]
    fourier_xkm = fourier_xkm[np.ix_(itheta, iphi)]

    from pystopt.visualization.matplotlib import subplots

    def _visualize_fourier_modes(name, fk):
        with subplots(name, nrows=1, ncols=2, overwrite=True) as fig:
            ax = fig.axes

            im = ax[0].imshow(np.log10(np.abs(fk.real) + 1.0e-16), extent=extent)
            # im = ax[0].imshow(xkm.real, extent=extent)
            ax[0].set_xlabel(r"$k_\phi$")
            ax[0].set_ylabel(r"$k_\theta$")
            ax[0].set_title("$Real$")
            ax[0].margins(0.01, 0.01)
            fig.colorbar(im, ax=ax[0], shrink=0.75)

            im = ax[1].imshow(np.log10(np.abs(fk.imag) + 1.0e-16), extent=extent)
            # im = ax[1].imshow(xk.imag, extent=extent)
            ax[1].set_xlabel(r"$k_\phi$")
            ax[1].set_title("$Imaginary$")
            fig.colorbar(im, ax=ax[1], shrink=0.75)

    filename = visualize_file_name.with_suffix("fourier_quadrature")
    _visualize_fourier_modes(filename, xkm)

    filename = visualize_file_name.with_suffix("fourier_equidistant")
    _visualize_fourier_modes(filename, equi_xkm)

    filename = visualize_file_name.with_suffix("fourier_quadrature_roundtrip")
    _visualize_fourier_modes(filename, fourier_xkm)

    filename = visualize_file_name.with_suffix("fourier_error")
    with subplots(filename, nrows=1, ncols=2, overwrite=True) as fig:
        ax = fig.axes

        m0 = equi_xkm[:, iphi[spharm_m]].real
        m1 = fourier_xkm[:, iphi[spharm_m]].real
        ktheta_max = discr.specgroups[0].sh.lmax // 2
        logger.info("ktheta max: %d", ktheta_max)

        # ax[0].plot(ktheta, np.log10(np.abs(m1 - m0) + 1.0e-16), "-")
        ax[0].plot(ktheta, np.log10(np.abs(m1) + 1.0e-16), "-")
        ax[0].plot(ktheta, np.log10(np.abs(m0) + 1.0e-16), "k--")
        ax[0].axvline(ktheta_max, color="k", ls="-")
        ax[0].set_xlabel(r"$k_\phi$")
        ax[0].set_ylabel(r"$\hat{x}^k_m$")  # noqa: RUF027
        ax[0].margins(0.0, 0.05)

        m1 = fourier_xkm[itheta[spharm_n], :].real

        ax[1].plot(kphi, np.log10(np.abs(m1) + 1.0e-16), "o-")
        ax[1].set_xlabel(r"$k_\phi$")
        ax[1].set_ylabel(r"$\hat{x}_k^m$")  # noqa: RUF027

    # }}}


def run_spharm_mode(actx_factory, spharm_m: int = 3, spharm_n: int = 5) -> None:
    def _make_func(actx, places, dofdesc):
        discr = places.get_discretization(dofdesc.geometry, dofdesc.discr_stage)

        from pystopt.mesh.spharm import evaluate_spharm_from_coordinates

        x = evaluate_spharm_from_coordinates(actx, discr, m=spharm_m, n=spharm_n)

        return x

    return _run_spharm(
        actx_factory,
        _make_func,
        spharm_m=spharm_m,
        spharm_n=spharm_m,
        suffix=f"y_{spharm_m}_{spharm_n}",
    )


def run_spharm_fourier_mode(
    actx_factory, *, fourier_k: int = 2, fourier_m: int = 0
) -> None:
    def evaluate_fourier(actx, phi, theta):
        return (
            actx.np.exp(2j * fourier_k * theta) * actx.np.exp(1j * fourier_m * phi)
        ).real

    def _make_func(actx, places, dofdesc):
        discr = places.get_discretization(dofdesc.geometry, dofdesc.discr_stage)

        from pystopt.mesh.spharm import _evaluate_spharm_func_from_coordinates

        x = _evaluate_spharm_func_from_coordinates(
            actx, discr, partial(evaluate_fourier, actx)
        )

        return x

    return _run_spharm(
        actx_factory,
        _make_func,
        spharm_m=fourier_m,
        spharm_n=fourier_k,
        suffix=f"exp_{fourier_k}_{fourier_m}",
    )


if __name__ == "__main__":
    import sys

    import pyopencl as cl

    if len(sys.argv) > 1:
        exec(sys.argv[1])
    else:
        run_spharm_mode(cl._csc)
