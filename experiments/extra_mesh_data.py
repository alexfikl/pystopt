# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

import pathlib
from dataclasses import dataclass, field
from typing import Any

import numpy as np

from meshmode.discretization.poly_element import InterpolatoryQuadratureGroupFactory
from pytools.obj_array import make_obj_array

from pystopt.filtering import FilterType
from pystopt.mesh.generation import make_transform_matrix_from_angles


def make_param_from_class(
    cls: type["ExampleParameters"],
    *,
    suffix: str = "v0",
    from_restart_filename: str | None = None,
    overwrite: bool = False,
    **kwargs: Any,
) -> "ExampleParameters":
    p = cls(**kwargs)

    assert p.name is not None, f"{cls} has no 'name'"
    assert p.ambient_dim is not None, f"{cls} has no 'ambient_dim'"
    assert p.resolution is not None, f"{cls} has no 'resolution'"
    assert p.target_order is not None, f"{cls} has no 'target_order'"

    fields = {}

    from pystopt.checkpoint import make_default_checkpoint_path

    if from_restart_filename is not None:
        fields["checkpoint_file_name"] = pathlib.Path(from_restart_filename)
    else:
        if p.cost_name is None:
            suffix = (p.mesh_name, p.farfield_name, suffix)
        else:
            suffix = (p.mesh_name, p.cost_name, suffix)

        fields["checkpoint_file_name"] = make_default_checkpoint_path(
            p.name,
            suffix,
            overwrite=overwrite,
        )
    fields["dirname"] = fields["checkpoint_file_name"].parent

    # {{{ fix orders

    if p.mesh_order is None:
        fields["mesh_order"] = p.target_order
    else:
        fields["mesh_order"] = p.mesh_order

    if p.qbx_order is None:
        fields["qbx_order"] = p.target_order
    else:
        fields["qbx_order"] = p.qbx_order

    if p.source_ovsmp is None:
        fields["source_ovsmp"] = 4 if p.ambient_dim == 2 else 3
    else:
        fields["source_ovsmp"] = p.source_ovsmp

    # }}}

    # {{{ fix arguments

    discretization_arguments = {
        "mesh_order": fields["mesh_order"],
        "target_order": p.target_order,
        "group_factory_cls": p.group_factory_cls,
        "use_spectral_derivative": p.use_spectral_derivative,
        "use_default_mesh_unit_nodes": p.use_default_mesh_unit_nodes,
    }

    fields["mesh_arguments"] = {
        **p.mesh_default_arguments,
        **p.mesh_arguments,
        **discretization_arguments,
    }

    fields["qbx_arguments"] = {
        **p.qbx_default_arguments,
        **p.qbx_arguments,
        "fine_order": fields["source_ovsmp"] * p.target_order,
        "qbx_order": fields["qbx_order"],
    }

    fields["farfield_arguments"] = {
        **p.farfield_default_arguments,
        **p.farfield_arguments,
    }

    fields["stokes_lambdas"] = {"viscosity_ratio": p.viscosity_ratio}
    fields["stokes_arguments"] = {
        **fields["stokes_lambdas"],
        "ca": p.capillary_number,
    }

    fields["filter_arguments"] = {
        **p.filter_default_arguments,
        **p.filter_arguments,
    }
    if p.filter_type != FilterType.Unfiltered:
        method = fields["filter_arguments"].get("method")
        kmax = fields["filter_arguments"].get("kmax")
        if (method is None or method == "ideal") and kmax is None:
            fields["filter_arguments"]["kmax"] = p.resolution // 2

    assert p.cgmethod == "steepest", f"'{p.cgmethod}' is not supported"
    fields["cg_arguments"] = {
        **p.cg_default_arguments,
        **p.cg_arguments,
    }

    fields["desired_mesh_arguments"] = {
        **p.mesh_default_arguments,
        **p.desired_mesh_arguments,
        **discretization_arguments,
    }

    # }}}

    from dataclasses import replace

    return replace(p, **fields)


# {{{ base


@dataclass(frozen=True)
class ExampleParameters:
    # NOTE: all the parameters should have some default parameters, but the
    # undefined ones should be set in `make_param_from_class` above

    name: str | None = None
    ambient_dim: int | None = None

    # {{{ mesh

    mesh_name: str | None = None
    resolution: int | None = None
    mesh_order: int | None = None

    mesh_arguments: dict[str, Any] = field(default_factory=dict)
    mesh_default_arguments: dict[str, Any] = field(default_factory=dict)

    # }}}

    # {{{ discretization

    target_order: int | None = None
    use_spectral_derivative: bool = True
    use_default_mesh_unit_nodes: bool = True
    group_factory_cls: type = InterpolatoryQuadratureGroupFactory

    # }}}

    # {{{ qbx

    qbx_order: int | None = None
    source_ovsmp: int | None = None
    qbx_arguments: dict[str, Any] = field(default_factory=dict)
    qbx_default_arguments: dict[str, Any] = field(
        default_factory=lambda: {
            "_disable_refinement": True,
            "fmm_order": 10,
            "fmm_backend": "sumpy",
        }
    )

    # }}}

    # {{{ stokes

    viscosity_ratio: float = 1.0
    capillary_number: float = 0.1
    stokes_lambdas: dict[str, float] = field(default_factory=dict)
    stokes_arguments: dict[str, Any] = field(default_factory=dict)

    farfield_name: str = "extensional"
    jump_name: str = "young_laplace"
    farfield_arguments: dict[str, Any] = field(default_factory=dict)
    farfield_default_arguments: dict[str, Any] = field(default_factory=dict)

    gmres_arguments: dict[str, Any] = field(default_factory=dict)

    # }}}

    # {{{ processing

    filter_type: FilterType = FilterType.Unfiltered
    filter_arguments: dict[str, Any] = field(default_factory=dict)
    filter_default_arguments: dict[str, Any] = field(default_factory=dict)

    # }}}

    # {{{ cost

    cost_name: str | None = None
    cost_arguments: dict[str, Any] = field(default_factory=dict)

    desired_mesh_name: str | None = None
    desired_mesh_arguments: dict[str, Any] = field(default_factory=dict)

    # }}}

    # {{{ optim

    cgmethod: str = "steepest"
    cg_arguments: dict[str, Any] = field(default_factory=dict)
    cg_default_arguments: dict[str, Any] = field(
        default_factory=lambda: {
            "rtol": 1.0e-5,
            "ftol": 1.0e-7,
        }
    )

    cg_descent_name: str = "steepest"
    cg_descent_arguments: dict[str, Any] = field(default_factory=dict)

    cg_linesearch_name: str = "backtracking"
    cg_linesearch_arguments: dict[str, Any] = field(
        default_factory=lambda: {
            "max_alpha": 0.01,
        }
    )

    # }}}

    # {{{ timestepping

    time_step_method: str = "aeuler"
    tmax: float | None = None
    timestep: float | None = None
    maxit: int | None = None
    cfl_theta: float | None = 1.0

    # unsteady tangential forcing
    force_normal_velocity: bool = True
    force_tangential_velocity: bool = False

    # stabilizer
    stabilizer_name: str = "default"
    stabilizer_arguments: dict[str, Any] = field(default_factory=dict)

    # }}}

    # {{{ checkpointing

    checkpoint_file_name: pathlib.Path | None = None
    dirname: pathlib.Path | None = None

    # }}}

    @property
    def long_name(self):
        from pystopt.tools import slugify

        return slugify(f"{self.name}-{self.mesh_name}-{self.cost_name}")

    def __str__(self):
        from pystopt.tools import dc_stringify

        return dc_stringify(self)


@dataclass(frozen=True)
class ExampleCurveParameters(ExampleParameters):
    ambient_dim: int = 2
    resolution: int = 128
    target_order: int = 4
    source_ovsmp: int = 4


@dataclass(frozen=True)
class ExampleSurfaceParameters(ExampleParameters):
    ambient_dim: int = 3
    resolution: int = 16
    target_order: int = 3
    source_ovsmp: int = 2


# }}}


# {{{ timestampped stokes parameters

# {{{ finite difference


@dataclass(frozen=True)
class StokesEllipseFD2021_05_13(ExampleCurveParameters):  # noqa: N801
    # mesh
    mesh_name: str = "fourier_ellipse"
    resolution: int = 128
    target_order: int = 4

    # cost
    cost_name: str = "steady"
    cost_arguments: dict[str, Any] = field(
        default_factory=lambda: {
            "gamma": 0,
        }
    )
    desired_mesh_name: str = "fourier_ufo"
    desired_mesh_arguments: dict[str, Any] = field(
        default_factory=lambda: {
            "k": 1,
            "a": 1,
            "b": 1,
        }
    )

    # stokes
    viscosity_ratio: float = 1.0
    capillary_number: float = 0.1


@dataclass(frozen=True)
class StokesSphereFD2021_05_13(ExampleSurfaceParameters):  # noqa: N801
    # mesh
    mesh_name: str = "spharm_sphere"
    resolution: int = 64

    # qbx
    source_ovsmp: int = 5
    qbx_order: int = 4

    farfield_name: str = "extensional"
    farfield_arguments: dict[str, Any] = field(
        default_factory=lambda: {
            "alpha": 1.0,
            "axis": 2,
        }
    )

    # cost
    cost_name: str = "steady"
    cost_arguments: dict[str, Any] = field(
        default_factory=lambda: {
            "gamma": 0,
        }
    )
    desired_mesh_name: str = "spharm_ufo"
    desired_mesh_arguments: dict[str, Any] = field(
        default_factory=lambda: {
            "k": 1,
            "a": 1,
            "b": 1,
        }
    )

    # stokes
    viscosity_ratio: float = 1.0
    capillary_number: float = 0.1


# }}}


# {{{ optim


@dataclass(frozen=True)
class StokesUfoSteady2D_2021_05_27(ExampleCurveParameters):  # noqa: N801
    # mesh
    resolution: int = 128
    mesh_name: str = "fourier_ufo"
    mesh_arguments: dict[str, Any] = field(
        default_factory=lambda: {
            "k": 1,
            "a": 1,
            "b": 1,
            "radius": 0.5,
            "angle": 0,
            # "angle": np.pi/2,
        }
    )

    # discretization
    target_order: int = 3
    qbx_order: int = 3
    source_ovsmp: int = 2

    # processing
    filter_default_arguments: dict[str, Any] = field(
        default_factory=lambda: {
            "method": "butterworth",
            "kmax": 32,
            "p": 6,
        }
    )

    # optim
    cost_name: str = "steady"
    cost_arguments: dict[str, Any] = field(
        default_factory=lambda: {
            # NOTE: volume of a disk of radius 1
            "gamma": 0,
            "vd": np.pi,
        }
    )

    # cg
    cg_arguments: dict[str, Any] = field(
        default_factory=lambda: {
            "maxit": 41,
        }
    )
    cg_linesearch_arguments: dict[str, Any] = field(
        default_factory=lambda: {
            "max_alpha": 0.001,
        }
    )


@dataclass(frozen=True)
class StokesUfoSteady2D_2021_07_12(StokesUfoSteady2D_2021_05_27):  # noqa: N801
    filter_type: FilterType = FilterType.Full
    filter_default_arguments: dict[str, Any] = field(
        default_factory=lambda: {
            "method": "ideal",
            "kmax": None,
        }
    )


@dataclass(frozen=True)
class StokesUfoSteady3D_2021_05_27(ExampleSurfaceParameters):  # noqa: N801
    # mesh
    mesh_name: str = "spharm_ufo"
    mesh_arguments: dict[str, Any] = field(
        default_factory=lambda: {
            "k": 1,
            "a": 1,
            "b": 1,
            "stretch": np.array([1.5, 1, 1]),
            "angle": np.pi / 2.0,
            "axis": np.array([0, 1, 0]),
        }
    )

    # processing
    filter_type: FilterType = FilterType.Full
    filter_default_arguments: dict[str, Any] = field(
        default_factory=lambda: {
            "mesh": "ideal",
            "kmax": -48,
        }
    )

    # optim
    cost_name: str = "steady"
    cost_arguments: dict[str, Any] = field(
        default_factory=lambda: {
            # NOTE: volume of a sphere of radius 1
            "gamma": 0.5,
            "vd": 4.0 / 3.0 * np.pi,
        }
    )

    # cg
    cg_linesearch_arguments: dict[str, Any] = field(
        default_factory=lambda: {
            "max_alpha": 0.005,
        }
    )


@dataclass(frozen=True)
class StokesCircleSteady_2021_09_06(StokesUfoSteady2D_2021_07_12):  # noqa: N801
    resolution: int = 64

    # discretization
    source_ovsmp: int = 4
    mesh_name: str = "fourier_circle"
    mesh_arguments: dict[str, Any] = field(
        default_factory=lambda: {
            "radius": 1.0,
        }
    )

    # filtering
    filter_type: FilterType = FilterType.Full
    filter_default_arguments: dict[str, Any] = field(
        default_factory=lambda: {
            "method": "ideal",
            "kmax": None,
        }
    )

    # cg
    cg_arguments: dict[str, Any] = field(
        default_factory=lambda: {
            "rtol": 1.0e-5,
            "ftol": 1.0e-5,
        }
    )
    cg_linesearch_name: str = "stabilized-barzilai-borwein"
    cg_linesearch_arguments: dict[str, Any] = field(
        default_factory=lambda: {
            "max_alpha": 0.001,
        }
    )


@dataclass(frozen=True)
class StokesSphereSteady_2021_09_06(StokesUfoSteady3D_2021_05_27):  # noqa: N801
    resolution: int = 16

    # discretization
    source_ovsmp: int = 2
    mesh_name: str = "spharm_sphere"
    mesh_arguments: dict[str, Any] = field(
        default_factory=lambda: {
            "radius": 1.0,
        }
    )

    # filtering
    filter_type: FilterType = FilterType.Full
    filter_default_arguments: dict[str, Any] = field(
        default_factory=lambda: {
            # NOTE: kmax <= min(nlat, nlon) // 2
            "method": "ideal",
            "kmax": 6,
        }
    )

    # cg
    cg_arguments: dict[str, Any] = field(
        default_factory=lambda: {
            "rtol": 1.0e-6,
            "ftol": 1.0e-6,
        }
    )
    cg_linesearch_name: str = "stabilized-barzilai-borwein"
    cg_linesearch_arguments: dict[str, Any] = field(
        default_factory=lambda: {
            "max_alpha": 0.01,
        }
    )


@dataclass(frozen=True)
class StokesSphereSteady_2021_09_13(StokesSphereSteady_2021_09_06):  # noqa: N801
    resolution: int = 64
    source_ovsmp: int = 4
    qbx_order: int = 4

    # optim
    cost_arguments: dict[str, Any] = field(
        default_factory=lambda: {
            # NOTE: volume of a sphere of radius 1
            "gamma": 0.5,
            "vd": 4.0 / 3.0 * np.pi,
        }
    )

    # filtering
    filter_type: FilterType = FilterType.Full
    filter_default_arguments: dict[str, Any] = field(
        default_factory=lambda: {
            "method": "tikhonov_ideal",
            "alpha": 1.0e-6,
            "p": 2,
            "kmax": None,
        }
    )

    # cg
    cg_linesearch_arguments: dict[str, Any] = field(
        default_factory=lambda: {
            "max_alpha": 0.002,
        }
    )


@dataclass(frozen=True)
class StokesCircleSteady_2021_11_09(StokesCircleSteady_2021_09_06):  # noqa: N801
    pass


@dataclass(frozen=True)
class StokesSpheroidSteady_2021_11_12(StokesSphereSteady_2021_09_13):  # noqa: N801
    # discretization
    mesh_name: str = "spharm_spheroid"
    mesh_arguments: dict[str, Any] = field(
        default_factory=lambda: {
            "radius": 1.0,
            "aspect_ratio": 0.75,
            "transform_matrix": make_transform_matrix_from_angles(
                # z, y, x
                0.0,
                np.pi / 4,
                np.pi / 3,
            ),
        }
    )


@dataclass(frozen=True)
class StokesCircleSteady_2022_01_10(ExampleCurveParameters):  # noqa: N801
    # mesh
    resolution: int = 32
    mesh_name: str = "fourier_ellipse"
    mesh_arguments: dict[str, Any] = field(
        default_factory=lambda: {
            "radius": 1.0,
            "aspect_ratio": 1.0,
            # "transform_matrix": make_transform_matrix_from_angles(np.pi / 3),
        }
    )

    # filter
    filter_type: FilterType = FilterType.Full
    filter_default_arguments: dict[str, Any] = field(
        default_factory=lambda: {
            "method": "ideal",
            "kmax": 12,
        }
    )

    # optim
    cost_name: str = "steady"
    cost_arguments: dict[str, Any] = field(
        default_factory=lambda: {
            # NOTE: volume of a circle of radius 1
            "gamma": 0.5,
            "vd": np.pi,
        }
    )

    # cg
    cg_arguments: dict[str, Any] = field(
        default_factory=lambda: {
            "rtol": 1.0e-6,
            "ftol": 1.0e-6,
        }
    )
    cg_linesearch_name: str = "stabilized-barzilai-borwein"
    # cg_linesearch_name: str = "backtracking"
    cg_linesearch_arguments: dict[str, Any] = field(
        default_factory=lambda: {
            "max_alpha": 0.001,
        }
    )


# }}}


# {{{ optim - ellipse


@dataclass(frozen=True)
class StokesEllipseSteady_2021_11_02(ExampleCurveParameters):  # noqa: N801
    # mesh
    resolution: int = 64
    mesh_name: str = "fourier_ellipse"
    mesh_arguments: dict[str, Any] = field(
        default_factory=lambda: {
            "radius": 1.0,
            "aspect_ratio": 0.5,
            "alpha": np.pi / 3,
        }
    )

    # filter
    filter_default_arguments: dict[str, Any] = field(
        default_factory=lambda: {
            "method": "ideal",
            "kmax": 32,
        }
    )

    # optim
    cost_name: str = "steady"
    cost_arguments: dict[str, Any] = field(
        default_factory=lambda: {
            # NOTE: volume of a circle of radius 1
            "gamma": 0.5,
            "vd": np.pi,
        }
    )

    # cg
    cg_arguments: dict[str, Any] = field(
        default_factory=lambda: {
            "rtol": 1.0e-6,
            "ftol": 1.0e-6,
        }
    )
    cg_linesearch_name: str = "stabilized-barzilai-borwein"
    cg_linesearch_arguments: dict[str, Any] = field(
        default_factory=lambda: {
            "max_alpha": 0.01,
        }
    )


@dataclass(frozen=True)
class StokesSpheroidSteady_2021_11_02(ExampleSurfaceParameters):  # noqa: N801
    # mesh
    resolution: int = 32
    source_ovsmp: int = 3

    mesh_name: str = "spharm_spheroid"
    mesh_arguments: dict[str, Any] = field(
        default_factory=lambda: {
            "radius": 1.0,
            "aspect_ratio": 1.0,
            "alpha": np.array([0, 0, 0]),
        }
    )

    # filter
    filter_default_arguments: dict[str, Any] = field(
        default_factory=lambda: {
            "method": "tikhonov",
            "alpha": 1.0e-6,
            "p": 2,
        }
    )

    # optim
    cost_name: str = "steady"
    cost_arguments: dict[str, Any] = field(
        default_factory=lambda: {
            # NOTE: volume of a sphere of radius 1
            "gamma": 0.5,
            "vd": 4.0 / 3.0 * np.pi,
        }
    )

    # cg
    cg_arguments: dict[str, Any] = field(
        default_factory=lambda: {
            "rtol": 1.0e-6,
            "ftol": 1.0e-6,
        }
    )
    cg_linesearch_name: str = "stabilized-barzilai-borwein"
    cg_linesearch_arguments: dict[str, Any] = field(
        default_factory=lambda: {
            "max_alpha": 0.01,
        }
    )


# }}}


# {{{ evolution - volume conservation


@dataclass(frozen=True)
class StokesVolumeConservation2d_2021_08_09(ExampleCurveParameters):  # noqa: N801
    # mesh
    mesh_name: str = "fourier_circle"
    resolution: int = 64

    # discretization
    target_order: int = 4
    qbx_order: int = 4
    source_ovsmp: int = 4

    # stokes
    viscosity_ratio: float = 1.0
    capillary_number: float = 0.05
    farfield_name: str = "extensional"
    farfield_arguments: dict[str, Any] = field(
        default_factory=lambda: {
            "alpha": 1.0,
            "axis": -1,
        }
    )

    # time stepping
    tmax: float = 1.0
    timestep: float = 1.0e-2
    time_step_method: str = "aeuler"

    # filtering
    filter_type: FilterType = FilterType.Full
    filter_arguments: dict[str, Any] = field(
        default_factory=lambda: {
            "method": "ideal",
            "kmax": None,
        }
    )


@dataclass(frozen=True)
class StokesVolumeConservation3d_2021_08_13(ExampleSurfaceParameters):  # noqa: N801
    # mesh
    mesh_name: str = "spharm_sphere"
    resolution: int = 24

    # discretization
    target_order: int = 3
    qbx_order: int = 3
    source_ovsmp: int = 3

    # stokes
    viscosity_ratio: float = 1.0
    capillary_number: float = 0.05
    farfield_name: str = "extensional"
    farfield_arguments: dict[str, Any] = field(
        default_factory=lambda: {
            "alpha": 1.0,
            "axis": -1,
        }
    )

    # time stepping
    tmax: float = 1.0
    timestep: float = 1.0e-2
    time_step_method: str = "aeuler"

    # filtering
    filter_type: FilterType = FilterType.Full
    filter_arguments: dict[str, Any] = field(
        default_factory=lambda: {
            "method": "tikhonov_ideal",
            "alpha": 1.0e-5,
            "p": 2,
            "kmax": None,
        }
    )


@dataclass(frozen=True)
class StokesVolumeConservation3d_2021_09_14(  # noqa: N801
    StokesVolumeConservation3d_2021_08_13
):
    resolution: int = 32
    qbx_order: int = 4


@dataclass(frozen=True)
class StokesVolumeConservation2d_2022_02_20(ExampleCurveParameters):  # noqa: N801
    # mesh
    mesh_name: str = "fourier_double_ufo"
    resolution: int = 64
    mesh_arguments: dict[str, Any] = field(
        default_factory=lambda: {
            "k": 1,
            "a": 1,
            "b": 1,
            "offset": make_obj_array([1.5, 0.0]),
            "transform_matrix": 0.75 * make_transform_matrix_from_angles(np.pi / 2),
        }
    )

    # discretization
    target_order: int = 4
    qbx_order: int = 4
    source_ovsmp: int = 4

    # stokes
    viscosity_ratio: float = 1.0
    capillary_number: float = np.inf
    farfield_name: str = "solid_body_rotation"
    farfield_arguments: dict[str, Any] = field(
        default_factory=lambda: {
            # "uinf": 1.0,
            "omega": 4.0,
        }
    )

    # time stepping
    tmax: float = 1.0 * np.pi
    timestep: float = 1.0e-2
    time_step_method: str = "aeuler"

    # filtering
    filter_type: FilterType = FilterType.Full
    filter_arguments: dict[str, Any] = field(
        default_factory=lambda: {
            "method": "ideal",
            "kmax": None,
        }
    )


@dataclass(frozen=True)
class StokesVolumeConservation3d_2022_02_20(ExampleSurfaceParameters):  # noqa: N801
    # mesh
    mesh_name: str = "spharm_double_sphere"
    resolution: int = 24

    # discretization
    target_order: int = 3
    qbx_order: int = 3
    source_ovsmp: int = 3

    # stokes
    viscosity_ratio: float = 5.0
    capillary_number: float = 0.1
    farfield_name: str = "extensional"
    farfield_arguments: dict[str, Any] = field(
        default_factory=lambda: {
            "uinf": 1.0,
        }
    )

    # time stepping
    tmax: float = 1.0
    timestep: float = 1.0e-2
    time_step_method: str = "aeuler"

    # filtering
    filter_type: FilterType = FilterType.Full
    filter_arguments: dict[str, Any] = field(
        default_factory=lambda: {
            "method": "tikhonov_ideal",
            "alpha": 1.0e-5,
            "p": 2,
            "kmax": None,
        }
    )


@dataclass(frozen=True)
class StokesVolumeConservation3d_2022_02_22(  # noqa: N801
    StokesVolumeConservation3d_2022_02_20
):
    # mesh
    mesh_name: str = "spharm_double_ufo"
    resolution: int = 32
    mesh_arguments: dict[str, Any] = field(
        default_factory=lambda: {
            "k": 1,
            "a": 1,
            "b": 1,
            "transform_matrix": make_transform_matrix_from_angles(0, 0, 0),
            "offset": np.array([1.5, 0.0, 0.0]),
        }
    )

    # discretization
    target_order: int = 5
    qbx_order: int = 3
    source_ovsmp: int = 3

    # stokes
    viscosity_ratio: float = 1.0
    capillary_number: float = np.inf
    farfield_name: str = "solid_body_rotation"
    farfield_arguments: dict[str, Any] = field(
        default_factory=lambda: {
            # "uinf": 1.0,
            "omega": 4.0
        }
    )

    # time stepping
    tmax: float = 1 * np.pi
    timestep: float = 1.0e-2
    time_step_method: str = "assprk33"

    # filtering
    filter_type: FilterType = FilterType.Unfiltered
    filter_arguments: dict[str, Any] = field(
        default_factory=lambda: {
            "method": "tikhonov_ideal",
            "alpha": 1.0e-5,
            "p": 4,
            "kmax": None,
            # "method": "tikhonov", "alpha": 1.0e-5, "p": 2,
            # "method": "ideal", "kmax": None,
        }
    )


@dataclass(frozen=True)
class StokesVolumeConservation3d_2022_03_13(  # noqa: N801
    StokesVolumeConservation3d_2022_02_22
):
    # mesh
    mesh_name: str = "spharm_double_ufo"
    resolution: int = 24
    mesh_arguments: dict[str, Any] = field(
        default_factory=lambda: {
            "k": 1,
            "a": 1,
            "b": 1,
            "transform_matrix": make_transform_matrix_from_angles(0, 0, 0),
            "offset": np.array([2.0, 0.0, 0.0]),
        }
    )

    # discretization
    target_order: int = 3
    qbx_order: int = 3
    source_ovsmp: int = 3

    # stokes
    viscosity_ratio: float = 1.0
    capillary_number: float = 0.1
    farfield_name: str = "extensional"
    farfield_arguments: dict[str, Any] = field(
        default_factory=lambda: {
            "alpha": 1.0,
            "axis": 0,
        }
    )

    # time stepping
    tmax: float = np.pi
    timestep: float = 5.0e-3
    time_step_method: str = "assprk22"


# }}}


# {{{ evolution - steady state


@dataclass(frozen=True)
class StokesSteadyEvolution2d_2021_10_04(  # noqa: N801
    StokesVolumeConservation2d_2021_08_09
):
    # stokes
    viscosity_ratio: float = 1.0
    capillary_number: float = 0.05
    farfield_name: str = "extensional"
    farfield_arguments: dict[str, Any] = field(
        default_factory=lambda: {
            "alpha": 1.0,
            "axis": 1,
        }
    )

    # time stepping
    tmax: float | None = None
    timestep: float = 5.0e-3
    time_step_method: str = "assprk22"


@dataclass(frozen=True)
class StokesSteadyEvolution3d_2021_10_04(  # noqa: N801
    StokesVolumeConservation3d_2021_09_14
):
    # stokes
    viscosity_ratio: float = 1.0
    capillary_number: float = 0.05
    farfield_name: str = "extensional"
    farfield_arguments: dict[str, Any] = field(
        default_factory=lambda: {
            # NOTE: sets up the axisymmetric extensional flow
            "alpha": 1.0,
            "axis": 1,
        }
    )

    # time stepping
    tmax: float | None = None
    timestep: float = 1.0e-3
    time_step_method: str = "assprk22"


# }}}


# {{{ evolution - stabilization


@dataclass(frozen=True)
class StokesStabilizedEvolution2d_2021_12_31(  # noqa: N801
    StokesVolumeConservation2d_2021_08_09
):
    resolution: int = 40
    mesh_name: str = "fourier_ufo"
    mesh_arguments: dict[str, Any] = field(
        default_factory=lambda: {
            "k": 1,
            "a": 1,
            "b": 1,
            "radius": 0.5,
        }
    )

    # stokes
    viscosity_ratio: float = 1.0
    capillary_number: float = 0.05
    farfield_name: str = "uniform"
    farfield_arguments: dict[str, Any] = field(
        default_factory=lambda: {
            "uinf": 1.0,
        }
    )

    # forcing
    force_normal_velocity: bool = True
    force_tangential_velocity: bool = True

    # stabilizer
    stabilizer_name: str = "kropinski2001"
    stabilizer_arguments: dict[str, Any] = field(default_factory=dict)

    # time stepping
    tmax: float = 1.0
    timestep: float = 5.0e-3
    time_step_method: str = "assprk22"


@dataclass(frozen=True)
class StokesStabilizedEvolution3d_2021_12_31(  # noqa: N801
    StokesVolumeConservation3d_2021_09_14
):
    resolution: int = 32
    source_ovsmp: int = 4

    mesh_name: str = "spharm_ufo"
    mesh_arguments: dict[str, Any] = field(
        default_factory=lambda: {
            "k": 1,
            "a": 1,
            "b": 1,
            "transform_matrix": make_transform_matrix_from_angles(0, np.pi / 2.0, 0),
        }
    )

    # stokes
    viscosity_ratio: float = 1.0
    capillary_number: float = 0.05
    farfield_name: str = "uniform"
    farfield_arguments: dict[str, Any] = field(
        default_factory=lambda: {
            "uinf": 1.0,
        }
    )

    # forcing
    force_normal_velocity: bool = True
    force_tangential_velocity: bool = True

    # stabilizer
    stabilizer_name: str = "zinchenko2002"
    stabilizer_arguments: dict[str, Any] = field(default_factory=dict)

    # time stepping
    tmax: float = 1.0
    timestep: float = 5.0e-3
    time_step_method: str = "aeuler"


# }}}

# }}}


# {{{ timestamped shape parameters


@dataclass(frozen=True)
class ShapeCircleParameters_2021_05_29(ExampleCurveParameters):  # noqa: N801
    # mesh
    mesh_name: str = "fourier_circle"
    resolution: int = 256
    target_order: int = 4

    # cost
    cost_name: str = "geometry_tracking"
    desired_mesh_name: str = "fourier_starfish"
    desired_mesh_arguments: dict[str, Any] = field(
        default_factory=lambda: {
            "narms": 3,
        }
    )


@dataclass(frozen=True)
class ShapeSphereParameters_2021_05_29(ExampleSurfaceParameters):  # noqa: N801
    # mesh
    mesh_name: str = "spharm_sphere"
    resolution: int = 32
    target_order: int = 3

    # cost
    cost_name: str = "geometry_tracking"
    desired_mesh_name: str = "spharm_ufo"
    desired_mesh_arguments: dict[str, Any] = field(
        default_factory=lambda: {
            "k": 2,
        }
    )


@dataclass(frozen=True)
class ShapeCircleParameters_2021_05_30(ShapeCircleParameters_2021_05_29):  # noqa
    resolution: int = 64
    target_order: int = 3

    # processing
    filter_default_arguments: dict[str, Any] = field(
        default_factory=lambda: {
            "method": "butterworth",
            "kmax": 32,
            "p": 6,
        }
    )


@dataclass(frozen=True)
class ShapeSphereParameters_2021_05_30(ShapeSphereParameters_2021_05_29):  # noqa
    desired_mesh_name: str = "spharm_ufo"
    desired_mesh_arguments: dict[str, Any] = field(
        default_factory=lambda: {
            "k": 2,
            "a": 1.25,
        }
    )

    # processing
    filter_default_arguments: dict[str, Any] = field(
        default_factory=lambda: {
            "mesh": "ideal",
            "kmax": -48,
        }
    )


@dataclass(frozen=True)
class ShapeCircleParameters_2021_09_05(ShapeCircleParameters_2021_05_30):  # noqa
    # filtering
    filter_type: FilterType = FilterType.Full
    filter_default_arguments: dict[str, Any] = field(
        default_factory=lambda: {
            "method": "ideal",
            "kmax": None,
        }
    )

    # cg
    cg_descent_name: str = "steepest"
    cg_linesearch_name: str = "stabilized-barzilai-borwein"
    cg_linesearch_arguments: dict[str, Any] = field(
        default_factory=lambda: {
            "max_alpha": 1.0,
        }
    )


@dataclass(frozen=True)
class ShapeSphereParameters_2021_09_05(ShapeSphereParameters_2021_05_30):  # noqa
    # filtering
    filter_type: FilterType = FilterType.Full
    filter_default_arguments: dict[str, Any] = field(
        default_factory=lambda: {
            "method": "tikhonov_ideal",
            "alpha": 1.0e-5,
            "p": 2,
            "kmax": None,
        }
    )

    # cg
    cg_descent_name: str = "steepest"
    cg_linesearch_name: str = "stabilized-barzilai-borwein"
    cg_linesearch_arguments: dict[str, Any] = field(
        default_factory=lambda: {
            "max_alpha": 1.0,
        }
    )


@dataclass(frozen=True)
class ShapeEllipseParameters_2021_12_19(ExampleCurveParameters):  # noqa: N801
    # mesh
    resolution: int = 128
    mesh_name: str = "fourier_ellipse"
    mesh_arguments: dict[str, Any] = field(
        default_factory=lambda: {
            "radius": 1.0,
            "aspect_ratio": 2.0,
            "alpha": 0,
        }
    )

    # optim
    cost_name: str = "geometry_tracking_volume"
    cost_arguments: dict[str, Any] = field(
        default_factory=lambda: {
            "gamma": 0.5,
        }
    )

    desired_mesh_name: str = "fourier_ellipse"
    desired_mesh_arguments: dict[str, Any] = field(
        default_factory=lambda: {
            "radius": 1.0,
            "aspect_ratio": 1.25,
            "transform_matrix": make_transform_matrix_from_angles(np.pi / 4),
        }
    )

    # cg
    cg_arguments: dict[str, Any] = field(
        default_factory=lambda: {
            "rtol": 1.0e-6,
            "ftol": 1.0e-6,
        }
    )
    cg_descent_name: str = "fletcher-reeves"
    cg_linesearch_name: str = "stabilized-barzilai-borwein"
    cg_linesearch_arguments: dict[str, Any] = field(
        default_factory=lambda: {
            "max_alpha": 0.1,
        }
    )


@dataclass(frozen=True)
class ShapeSpheroidParameters_2021_12_19(ExampleSurfaceParameters):  # noqa: N801
    # mesh
    resolution: int = 64
    mesh_name: str = "spharm_spheroid"
    mesh_arguments: dict[str, Any] = field(
        default_factory=lambda: {
            "radius": 1.0,
            "aspect_ratio": 2.0,
            "alpha": np.array([0, 0, 0]),
        }
    )

    # optim
    cost_name: str = "geometry_tracking_volume"
    cost_arguments: dict[str, Any] = field(
        default_factory=lambda: {
            "gamma": 0.5,
        }
    )

    desired_mesh_name: str = "spharm_spheroid"
    desired_mesh_arguments: dict[str, Any] = field(
        default_factory=lambda: {
            "radius": 1.0,
            "aspect_ratio": 1.25,
            "transform_matrix": make_transform_matrix_from_angles(
                np.pi / 4, np.pi / 6, np.pi / 3
            ),
        }
    )

    # cg
    cg_arguments: dict[str, Any] = field(
        default_factory=lambda: {
            "rtol": 1.0e-6,
            "ftol": 1.0e-6,
        }
    )
    cg_descent_name: str = "steepest"
    cg_linesearch_name: str = "stabilized-barzilai-borwein"
    cg_linesearch_arguments: dict[str, Any] = field(
        default_factory=lambda: {
            "max_alpha": 0.1,
        }
    )


# }}}


# {{{ capillary


@dataclass(frozen=True)
class StokesCapillary2d_2021_10_12(ShapeCircleParameters_2021_09_05):  # noqa: N801
    # time stepping
    tmax: float = 0.1
    timestep: float = 1.0e-3
    time_step_method: str = "aeuler"

    farfield_name: str = "extensional"

    # cost
    cost_name: str = "geometry_tracking"
    desired_mesh_name: str = "fourier_ellipse"
    desired_mesh_arguments: dict[str, Any] = field(
        default_factory=lambda: {
            "aspect_ratio": 1.0,
            "radius": 1.0,
            # "aspect_ratio": 2.0, "radius": 1.0,
        }
    )


@dataclass(frozen=True)
class StokesCapillary3d_2021_10_12(ShapeSphereParameters_2021_09_05):  # noqa: N801
    resolution: int = 32
    source_ovsmp: int = 3
    qbx_order: int = 4

    farfield_name: str = "extensional"
    farfield_arguments: dict[str, Any] = field(
        default_factory=lambda: {
            "alpha": 1.0,
            "axis": 2,
        }
    )

    # time stepping
    tmax: float = 0.1
    timestep: float = 1e-3
    time_step_method: str = "assprk22"

    # filtering
    filter_default_arguments: dict[str, Any] = field(
        default_factory=lambda: {
            "method": "tikhonov",
            "alpha": 1.0e-7,
            "p": 2,
            # "method": "tikhonov_opt", "p": 2,
            # "method": "ideal", "kmax": (48, 64),
        }
    )

    # cost
    cost_name: str = "geometry_tracking"
    desired_mesh_name: str = "spharm_spheroid"
    desired_mesh_arguments: dict[str, Any] = field(
        default_factory=lambda: {
            "aspect_ratio": 2.0,
            "radius": 1.0,
            # "aspect_ratio": 1.0, "radius": 1.0,
        }
    )


# }}}


# {{{ boundary


@dataclass(frozen=True)
class StokesBoundary2d_2021_11_15(StokesCapillary2d_2021_10_12):  # noqa: N801
    resolution: int = 32
    filter_default_arguments: dict[str, Any] = field(
        default_factory=lambda: {
            "method": "ideal",
            "kmax": 15,
        }
    )

    # time stepping
    tmax: float = 0.5
    timestep: float = 1.0e-3
    time_step_method: str = "aeuler"

    # forcing / stabilization
    force_tangential_velocity: bool = True

    # stabilizer
    stabilizer_name: str = "kropinski2001"
    stabilizer_arguments: dict[str, Any] = field(default_factory=dict)

    # farfield
    farfield_name: str = "uniform"
    farfield_arguments: dict[str, Any] = field(
        default_factory=lambda: {"uinf": np.array([1.0, 0.0])}
    )

    # cost
    cost_name: str = "uniform_geometry_tracking"
    cost_arguments: dict[str, Any] = field(
        default_factory=lambda: {"uinf_d": np.array([0.0, 1.0])}
    )

    desired_mesh_name: str = "fourier_circle"
    desired_mesh_arguments: dict[str, Any] = field(
        default_factory=lambda: {
            "radius": 1.0,
        }
    )

    # cg
    cg_arguments: dict[str, Any] = field(
        default_factory=lambda: {
            "ftol": 1.0e-6,
            "rtol": 1.0e-6,
        }
    )
    cg_linesearch_name: str = "stabilized-barzilai-borwein"
    cg_linesearch_arguments: dict[str, Any] = field(
        default_factory=lambda: {
            "max_alpha": 1.0,
        }
    )


@dataclass(frozen=True)
class StokesSolidBodyBoundary_2021_11_16(StokesBoundary2d_2021_11_15):  # noqa: N801
    # time stepping
    tmax: float = np.pi / 2
    timestep: float = 2.5e-3
    time_step_method: str = "aeuler"

    # farfield
    farfield_name: str = "solid_body_rotation"
    farfield_arguments: dict[str, Any] = field(
        default_factory=lambda: {
            "omega": -1.0,
        }
    )

    # cost
    cost_name: str = "sbr_geometry_tracking"
    cost_arguments: dict[str, Any] = field(
        default_factory=lambda: {
            "omega_d": 1.0,
        }
    )

    cg_linesearch_arguments: dict[str, Any] = field(
        default_factory=lambda: {
            "max_alpha": 0.01,
        }
    )


@dataclass(frozen=True)
class StokesSolidBodyBoundary_2022_05_30(StokesSolidBodyBoundary_2021_11_16):  # noqa
    # cost
    cost_name: str = "sbr_centroid_tracking"
    cost_arguments: dict[str, Any] = field(
        default_factory=lambda: {
            "omega_d": 1.0,
        }
    )


@dataclass(frozen=True)
class StokesBezierBoundary2d_2021_11_28(StokesBoundary2d_2021_11_15):  # noqa: N801
    # time stepping
    # NOTE: a smaller final time may mean that the farfield is very fast!
    tmax: float = np.pi
    timestep: float = 2.5e-3
    time_step_method: str = "aeuler"

    # mesh
    mesh_name: str = "fourier_ellipse"
    mesh_arguments: dict[str, Any] = field(
        default_factory=lambda: {
            "aspect_ratio": 2.0,
            "radius": 1.25,
        }
    )

    # forcing / stabilization
    force_tangential_velocity: bool = True

    # stabilizer
    stabilizer_name: str = "kropinski2001"
    stabilizer_arguments: dict[str, Any] = field(default_factory=dict)

    # farfield
    farfield_name: str = "bezier_uniform"
    farfield_arguments: dict[str, Any] = field(
        default_factory=lambda: {
            "tau": "t",
            # "points_or_order": (
            #     np.array([0, 0], dtype=object),
            #     np.array([6, 0], dtype=object),
            #     np.array([0, 2], dtype=object),
            #     np.array([6, 2], dtype=object),
            # ),
            "points_or_order": (
                np.array([0, 0], dtype=object),
                np.array([2, 0], dtype=object),
                np.array([4, 0], dtype=object),
                np.array([6, 0], dtype=object),
            ),
        }
    )

    # cost
    cost_name: str = "bezier_uniform_geometry_tracking"
    cost_arguments: dict[str, Any] = field(
        default_factory=lambda: {
            "t": "t",
            "points_d": (
                np.array([0, 0], dtype=object),
                np.array([6, 0], dtype=object),
                np.array([0, 2], dtype=object),
                np.array([6, 2], dtype=object),
            ),
        }
    )

    cg_linesearch_arguments: dict[str, Any] = field(
        default_factory=lambda: {
            "max_alpha": 0.75,
        }
    )


@dataclass(frozen=True)
class StokesHelicalBoundary_2021_12_06(StokesCapillary3d_2021_10_12):  # noqa: N801
    # resolution
    resolution: int = 24
    source_ovsmp: int = 4
    qbx_order: int = 4

    # time stepping
    tmax: float = np.pi
    # tmax: float = None
    # maxit: int = 3
    timestep: float = 7.5e-3
    time_step_method: str = "aeuler"

    # forcing / stabilization
    force_normal_velocity: bool = False
    force_tangential_velocity: bool = False

    # stabilizer
    stabilizer_name: str = "zinchenko2002"
    stabilizer_arguments: dict[str, Any] = field(
        default_factory=lambda: {
            # "optim_options": {"maxiter": 32},
        }
    )

    # farfield
    viscosity_ratio: float = 1.0
    capillary_number: float = 0.05
    farfield_name: str = "helix"
    farfield_arguments: dict[str, Any] = field(
        default_factory=lambda: {
            "tmax": np.pi,
            # "height": 3.5, "omega": 2 * np.pi,
            # NOTE: these are the "optimal" values found in optim
            # "height": 1.890092078753982, "omega": 12.561383273648197,
            # NOTE: these are the initial guesses for the optim
            "height": 1.5,
            "omega": 3.0 * np.pi,
        }
    )

    # filtering
    filter_default_arguments: dict[str, Any] = field(
        default_factory=lambda: {
            "method": "tikhonov_ideal",
            "kmax": None,
            "alpha": 1.0e-5,
            "p": 2,
        }
    )

    # cost
    cost_name: str = "helix_geometry_tracking"
    cost_arguments: dict[str, Any] = field(
        default_factory=lambda: {
            "height_d": 2.0,
            "omega_d": 4.0 * np.pi,
            "tau": "t",
        }
    )
    desired_mesh_name: str = "spharm_sphere"
    desired_mesh_arguments: dict[str, Any] = field(
        default_factory=lambda: {
            "radius": 1.0,
        }
    )

    # cg
    cg_arguments: dict[str, Any] = field(
        default_factory=lambda: {
            "ftol": 1.0e-6,
            "rtol": 1.0e-6,
        }
    )
    cg_linesearch_name: str = "stabilized-barzilai-borwein"
    cg_linesearch_arguments: dict[str, Any] = field(
        default_factory=lambda: {"max_alpha": 0.01, "require_monotonicity": True}
    )


@dataclass(frozen=True)
class StokesHelicalBoundary_2022_02_09(StokesHelicalBoundary_2021_12_06):  # noqa: N801
    mesh_name: str = "spharm_ufo"
    mesh_arguments: dict[str, Any] = field(
        default_factory=lambda: {
            "k": 1,
            "a": 1,
            "b": 1,
            "transform_matrix": make_transform_matrix_from_angles(0.0, 0.0, 0.0),
        }
    )

    timestep: float = 1.0e-3


@dataclass(frozen=True)
class StokesHelicalBoundary_2022_04_28(StokesHelicalBoundary_2021_12_06):  # noqa: N801
    farfield_arguments: dict[str, Any] = field(
        default_factory=lambda: {
            "tmax": np.pi,
            "height": 0.0,
            "omega": 2.0 * np.pi,
        }
    )

    enable_gravity: bool = True
    enable_variable_surface_tension: bool = False
    double_helix: bool = True


@dataclass(frozen=True)
class StokesHelicalBoundary_2022_06_02(StokesHelicalBoundary_2021_12_06):  # noqa: N801
    tmax: float = np.pi
    timestep: float = 5.0e-3
    time_step_method: str = "aeuler"

    cost_name: str = "helix_centroid_tracking"
    cost_arguments: dict[str, Any] = field(
        default_factory=lambda: {
            "height_d": 2.0,
            "omega_d": 4.0 * np.pi,
            "tau": "t",
        }
    )


@dataclass(frozen=True)
class StokesHelicalBoundary_2022_08_29(StokesHelicalBoundary_2022_06_02):  # noqa: N801
    tmax: float = np.pi
    timestep: float = 1.0e-2
    time_step_method: str = "assprk22"

    farfield_arguments: dict[str, Any] = field(
        default_factory=lambda: {
            "tmax": np.pi,
            "height": 1.5,
            "omega": 3.0 * np.pi,
        }
    )

    cost_name: str = "helix_centroid_tracking"
    cost_arguments: dict[str, Any] = field(
        default_factory=lambda: {
            "height_d": 2.0,
            "omega_d": 4.0 * np.pi,
            "tau": "t",
        }
    )

    cg_linesearch_name: str = "stabilized-barzilai-borwein"
    cg_linesearch_arguments: dict[str, Any] = field(
        default_factory=lambda: {"max_alpha": 0.1, "require_monotonicity": True}
    )


# }}}
