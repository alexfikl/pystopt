# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

import enum
from dataclasses import dataclass, replace
from typing import Any

import numpy as np

from arraycontext import ArrayContext
from pytools.obj_array import make_obj_array

import pystopt.callbacks as cb
import pystopt.simulation as sim
from pystopt.tools import get_cl_array_context, get_default_logger

logger = get_default_logger(module=__file__)


# {{{ callbacks


@dataclass
class CallbackManager(sim.FarfieldOptimizationCallbackManager):
    pass


@dataclass(frozen=True)
class HistoryCallback(cb.OptimizationHistoryCallback):
    def __call__(self, *args, **kwargs):
        r = super().__call__(*args, **kwargs)
        (state,) = args

        with np.printoptions(
            formatter={
                "float": lambda x: format(x, ".5e"),
            }
        ):
            logger.info("=" * 128)
            logger.info(
                "omega %s / %s grad %s",
                state.x,
                state.wrangler.context["omega_d"],
                state.gradient,
            )
            logger.info("=" * 128)

        return r


class VisualizeCallback(sim.StokesUnsteadyOptimizationVisualizeCallback):
    def get_geometry_kwargs(self, state):
        return {"xlim": [-3.25, 3.25], "ylim": [-3.25, 5.25]}


class StokesEvolutionVisualizeCallback(sim.StokesEvolutionVisualizeCallback):
    def get_geometry_kwargs(self, state):
        return {
            "xlim": [-3.25, 3.25],
            "ylim": [-3.25, 3.25],
            "markers": ["--"],
        }


@dataclass(frozen=True)
class StokesEvolutionHistoryCallback(sim.StokesEvolutionHistoryCallback):
    def __call__(self, state, *args, fields, **kwargs):
        ret = super().__call__(state, *args, fields=fields, **kwargs)

        from pystopt import bind

        xd = bind(
            state.places,
            state.wrangler.cost.nodes(),
            auto_where=state.wrangler.cost.dofdesc,
        )(state.array_context, t=kwargs["event"].t, **state.wrangler.context)

        from arraycontext import flatten

        xd = state.array_context.to_numpy(flatten(xd, state.array_context)).reshape(
            state.places.ambient_dim, -1
        )

        logger.info(
            "volume %+.15e area %+.15e",
            (self.volume[-1] - self.volume[0]) / self.volume[0],
            (self.area[-1] - self.area[0]) / self.area[0],
        )

        logger.info(
            "centroid (%s, %s) / (%s, %s)",
            self.centroid[-1][0],
            self.centroid[-1][1],
            xd[0],
            xd[1],
        )

        return ret


# }}}


# {{{ wranglers


@dataclass(frozen=True)
class StokesEvolutionWrangler(sim.RotationFarfieldStokesEvolutionWrangler):
    pass


@dataclass(frozen=True)
class AdjointStokesEvolutionWrangler(
    sim.AdjointRotationFarfieldStokesEvolutionWrangler
):
    pass


@dataclass(frozen=True)
class BoundaryOptimizationWrangler(sim.RotationFarfieldOptimizationWrangler):
    visualize: bool

    def get_callback(self, actx, checkpoint_directory_name, *, prefix):
        if not self.visualize:
            return None

        if prefix == "forward":
            manager_factory = sim.StokesEvolutionCallbackManager
            visualize_callback_factory = StokesEvolutionVisualizeCallback
            history_callback_factory = StokesEvolutionHistoryCallback
        else:
            manager_factory = sim.AdjointStokesEvolutionCallbackManager
            visualize_callback_factory = sim.AdjointStokesEvolutionVisualizeCallback
            history_callback_factory = False

        from pystopt.dof_array import dof_array_norm

        return cb.make_default_evolution_callback(
            checkpoint_directory_name / "dummy",
            manager_factory=manager_factory,
            history_callback_factory=history_callback_factory,
            checkpoint_callback_factory=False,
            visualize_callback_factory=visualize_callback_factory,
            norm=dof_array_norm,
            prefix=prefix,
            overwrite=True,
        )


# }}}


# {{{ geometry


def get_discretization_from_param(actx, p, mesh_name, mesh_arguments):
    import extra_optim_data as eod

    discrs = []

    for offset in [-2, 2]:
        new_mesh_arguments = mesh_arguments.copy()
        new_mesh_arguments["offset"] = np.array([offset, 0, 0][: p.ambient_dim])

        px = replace(p, mesh_name=mesh_name)
        discr, _ = eod.get_discretization_from_param(
            actx, px, mesh_name, new_mesh_arguments
        )

        discrs.append(discr)

    from pystopt.mesh import merge_disjoint_spectral_discretizations

    return merge_disjoint_spectral_discretizations(actx, discrs)


def get_geometry_collection_from_param(actx, p):
    discr_d = get_discretization_from_param(
        actx, p, p.desired_mesh_name, p.desired_mesh_arguments
    )
    pre_density_discr = get_discretization_from_param(
        actx, p, p.mesh_name, p.mesh_arguments
    )

    from pystopt.qbx import QBXLayerPotentialSource

    qbx = QBXLayerPotentialSource(pre_density_discr, **p.qbx_arguments)

    from pytential import GeometryCollection

    return GeometryCollection(
        {p.long_name: qbx, "desired": discr_d}, auto_where=p.long_name
    )


# }}}


# {{{ run


class Mode(enum.Enum):
    FINITE_DIFFERENCE = enum.auto()
    ADJOINT_ONLY = enum.auto()
    OPTIMIZATION = enum.auto()
    FORWARD_ONLY = enum.auto()


def run_finite_difference(state_m, eps):
    cost_m = state_m.cost
    logger.info("cost_m: %.12e", cost_m)

    dc_domega_fd = np.empty((eps.size, state_m.x.size))

    for i in range(eps.size):
        for j in range(state_m.x.size):
            x = make_obj_array(state_m.x)
            x[j] = x[j] + eps[i]

            state_p = replace(state_m, x=x)
            cost_p = state_p.cost
            dc_domega_fd[i, j] = (cost_p - cost_m) / eps[i]

        with np.printoptions(precision=15):
            logger.info("gradient[fd]: eps %.15e grad %s", eps[i], dc_domega_fd[i])

    return dc_domega_fd


def run_optimization(
    state0,
    p,
    *,
    from_restart: bool = False,
    overwrite: bool = True,
    visualize: bool = True,
):
    logger.info("\n%s", p)

    # {{{ visualization

    if visualize:
        visualize_callback_factory = VisualizeCallback
    else:
        visualize_callback_factory = False

    callback = cb.make_default_shape_callback(
        p.checkpoint_file_name,
        manager_factory=CallbackManager,
        visualize_callback_factory=visualize_callback_factory,
        history_callback_factory=HistoryCallback,
        norm=lambda x, p: np.linalg.norm(state0.unwrap(x), ord=p),
        from_restart=from_restart,
        overwrite=overwrite,
    )
    checkpoint = callback["checkpoint"].checkpoint

    # }}}

    # {{{ optimize

    from pystopt.optimize import CartesianEucledeanSpace

    manifold = CartesianEucledeanSpace(
        vdot=lambda x, y: np.vdot(state0.unwrap(x), state0.unwrap(y))
    )

    from pystopt.optimize import get_line_search_from_name

    linesearch = get_line_search_from_name(
        p.cg_linesearch_name, fun=lambda x: x.cost, **p.cg_linesearch_arguments
    )

    from pystopt.optimize import get_descent_direction_from_name

    descent = get_descent_direction_from_name(
        p.cg_descent_name, **p.cg_descent_arguments
    )

    options = {
        **p.cg_arguments,
        "linesearch": linesearch,
        "descent": descent,
        "manifold": manifold,
    }

    from pystopt.optimize.steepest import minimize

    r = minimize(
        fun=lambda x: x.cost,
        x0=state0,
        jac=lambda x: x.wrap(x.gradient),
        funjac=lambda x: (x.cost, x.wrap(x.gradient)),
        callback=callback,
        options=options,
    )
    logger.info("result:\n%s", r.pretty())

    # }}}

    from pystopt.tools import dc_asdict

    checkpoint.write_to("parameters", dc_asdict(p), overwrite=True)
    checkpoint.write_to("callback", callback, overwrite=True)

    rx = replace(r, x=state0.unwrap(r.x))
    checkpoint.write_to("result", rx, overwrite=True)
    checkpoint.done()

    return r


def run(
    actx: ArrayContext,
    p,
    *,
    mode: Mode = Mode.FINITE_DIFFERENCE,
    eps: float | np.ndarray | None = None,
    overwrite: bool = True,
    visualize: bool = True,
) -> tuple[float, float]:
    import extra_optim_data as eod

    # {{{ geometry

    places = get_geometry_collection_from_param(actx, p)
    dofdesc = places.auto_source
    desire_dd = dofdesc.copy(geometry="desired")

    discr = places.get_discretization(dofdesc.geometry, dofdesc.discr_stage)
    logger.info("nspec:     %d", discr.nspec)
    logger.info("ndofs:     %d", discr.ndofs)
    logger.info("nelements: %d", discr.mesh.nelements)

    from pystopt import bind, sym

    h_max = bind(places, sym.h_max_from_volume(places.ambient_dim), auto_where=dofdesc)(
        actx
    )
    h_max = actx.to_numpy(h_max)

    # }}}

    # {{{ stokes

    if p.farfield_name != "solid_body_rotation":
        raise ValueError(p.farfield_name)

    from pystopt import stokes

    bc = stokes.get_farfield_boundary_from_name(
        p.ambient_dim, p.farfield_name, omega=sym.var("omega")
    )

    st_op = stokes.TwoPhaseSingleLayerStokesRepresentation(bc)

    from pystopt.evolution.stokes import StokesEvolutionOperator

    evo_op = StokesEvolutionOperator(
        force_normal_velocity=p.force_normal_velocity,
        force_tangential_velocity=p.force_tangential_velocity,
    )

    # }}}

    # {{{ cost

    cost, context = eod.make_unsteady_cost_functional(
        actx, places, st_op, p, source_dd=dofdesc, desire_dd=desire_dd
    )

    context.update(p.stokes_arguments)
    context.update(p.farfield_arguments)

    # }}}

    # {{{ evolution

    from pystopt.evolution import TimeWrangler

    time = TimeWrangler(
        time_step_name=p.time_step_method,
        maxit=p.maxit,
        tmax=p.tmax,
        dt=p.timestep,
        theta=1.0,
        estimate_time_step=False,
    )

    forward = StokesEvolutionWrangler(
        # geometry
        places=places,
        dofdesc=dofdesc,
        is_spectral=True,
        keep_reconstruction_vertices=p.force_tangential_velocity,
        # stokes
        op=st_op,
        lambdas=p.stokes_lambdas,
        gmres_arguments={},
        # cost
        cost=cost,
        cost_final=None,
        # evolution
        evolution=evo_op,
        filter_type=p.grad_filtered,
        filter_arguments=p.filter_arguments,
        stabilizer_arguments=p.tangential_forcing_arguments,
        context=context,
    )

    # }}}

    # {{{ optimization

    from pystopt.paths import generate_filename_series

    checkpoint_directory_series = generate_filename_series("evolution", cwd=p.dirname)

    wrangler = BoundaryOptimizationWrangler(
        time=time,
        forward=forward,
        checkpoint_directory_series=checkpoint_directory_series,
        visualize=False if mode == Mode.OPTIMIZATION else visualize,
    )

    # }}}

    state = wrangler.get_initial_state(actx)
    if mode == Mode.FINITE_DIFFERENCE:
        assert eps is not None

        if isinstance(eps, float):
            eps = np.array([eps])

        dc_domega_fd = run_finite_difference(state, eps=eps)
        dc_domega_ad = state.gradient
        return dc_domega_fd, dc_domega_ad, h_max
    elif mode == Mode.ADJOINT_ONLY:
        return state.gradient, h_max
    elif mode == Mode.FORWARD_ONLY:
        return state.cost, h_max
    elif mode == Mode.OPTIMIZATION:
        return run_optimization(state, p, overwrite=overwrite, visualize=visualize)
    else:
        raise ValueError(f"unknown run mode: {mode}")


def make_parameters(
    *,
    ambient_dim: int,
    extra_kwargs: dict[str, Any],
    suffix: str = "v0",
    overwrite: bool = True,
):
    if ambient_dim == 2:
        from extra_mesh_data import StokesSolidBodyBoundary_2022_05_30 as Parameters
    else:
        raise ValueError(f"unsupported dimension: {ambient_dim}")

    import extra_mesh_data as emd

    p = emd.make_param_from_class(
        Parameters,
        name="boundary-optim",
        suffix=suffix,
        overwrite=overwrite,
        **extra_kwargs,
    )
    assert p.ambient_dim == ambient_dim

    return p


def prepare_and_run(
    ctx_factory_or_actx,
    *,
    ambient_dim: int = 2,
    suffix: str = "v0",
    mode: Mode = Mode.FINITE_DIFFERENCE,
    eps: float | np.ndarray = 1.0e-2,
    extra_kwargs: dict[str, Any] | None = None,
    cached: bool = False,
    overwrite: bool = True,
    visualize: bool = True,
):
    actx = get_cl_array_context(ctx_factory_or_actx)
    if extra_kwargs is None:
        extra_kwargs = {}

    # {{{ parameters

    p = make_parameters(
        ambient_dim=ambient_dim,
        extra_kwargs=extra_kwargs,
        suffix=f"{mode.name}_{suffix}",
        overwrite=overwrite,
    )

    # }}}

    from pystopt.checkpoint.hdf import array_context_for_pickling

    if mode == Mode.FINITE_DIFFERENCE:
        from pystopt.checkpoint import make_hdf_checkpoint_manager

        checkpoint = make_hdf_checkpoint_manager(p.checkpoint_file_name)

        with array_context_for_pickling(actx):
            if cached and "result" in checkpoint:
                r = checkpoint.read_from("result")
                grad_ad = r["grad_ad"]
                grad_fd = r["grad_fd"]
                h_max = r["h_max"]
            else:
                logger.info("\n%s", p)
                logger.info("dirname:   %s", p.dirname)

                grad_fd, grad_ad, h_max = run(
                    actx,
                    p,
                    eps=eps,
                    mode=mode,
                    overwrite=overwrite,
                    visualize=visualize,
                )

                from pystopt.tools import dc_asdict

                checkpoint.write_to("parameters", dc_asdict(p), overwrite=True)
                checkpoint.write_to(
                    "result",
                    {
                        "grad_fd": grad_fd,
                        "grad_ad": grad_ad,
                        "h_max": h_max,
                    },
                    overwrite=True,
                )
                checkpoint.done()

        logger.info(
            "h_max %.5e grad_ad %s grad_fd %s", h_max, grad_ad, grad_fd.squeeze()
        )
        return grad_fd, grad_ad, h_max
    else:
        with array_context_for_pickling(actx):
            return run(actx, p, mode=mode, overwrite=overwrite, visualize=visualize)


# }}}


# {{{


def main(
    ctx_factory_or_actx,
    *,
    ambient_dim: int = 2,
    viscosity_ratio: float = 1.0,
    capillary_number: float = 0.05,
    suffix: str = "v0",
    overwrite: bool = True,
    visualize: bool = True,
):
    prepare_and_run(
        ctx_factory_or_actx,
        ambient_dim=ambient_dim,
        extra_kwargs={
            "viscosity_ratio": viscosity_ratio,
            "capillary_number": capillary_number,
        },
        eps=1.0e-7,
        mode=Mode.FINITE_DIFFERENCE,
        suffix=suffix,
        overwrite=overwrite,
        visualize=visualize,
    )


def optim(
    ctx_factory_or_actx,
    *,
    ambient_dim: int = 2,
    suffix: str = "v0",
    overwrite: bool = True,
    visualize: bool = True,
):
    prepare_and_run(
        ctx_factory_or_actx,
        ambient_dim=ambient_dim,
        extra_kwargs={
            "viscosity_ratio": 1.0,
            "capillary_number": 0.05,
        },
        eps=1.0e-7,
        mode=Mode.OPTIMIZATION,
        suffix=suffix,
        overwrite=overwrite,
        visualize=visualize,
    )


# }}}


# {{{ contour


def run_contour(
    ctx_factory_or_actx,
    *,
    ambient_dim: int = 2,
    suffix: str = "v0",
    overwrite: bool = True,
    visualize: bool = True,
):
    actx = get_cl_array_context(ctx_factory_or_actx)

    extra_kwargs = {
        "tmax": np.pi / 2,
        "timestep": 2.5e-3,
        "farfield_arguments": {
            "omega": -1.0,
        },
        "cost_arguments": {
            "omega_d": 1.0,
        },
    }

    omegas = np.linspace(-1.5, -0.5, 16).tolist()
    cost = np.empty(len(omegas))
    mode = Mode.FORWARD_ONLY

    from pystopt.checkpoint import make_hdf_checkpoint_manager
    from pystopt.checkpoint.hdf import array_context_for_pickling

    for i in range(cost.size):
        extra_kwargs["farfield_arguments"]["omega"] = omegas[i]

        p = make_parameters(
            ambient_dim=ambient_dim,
            extra_kwargs=extra_kwargs,
            suffix=f"{mode.name}_{suffix}_{i}",
            overwrite=overwrite,
        )
        with array_context_for_pickling(actx):
            if p.checkpoint_file_name.exists():
                checkpoint = make_hdf_checkpoint_manager(p.checkpoint_file_name)
                cost[i] = checkpoint.read_from("result")["cost"]
            else:
                cost[i], _ = run(
                    actx, p, mode=mode, overwrite=overwrite, visualize=False
                )

                checkpoint = make_hdf_checkpoint_manager(p.checkpoint_file_name)
                checkpoint.write_to("result", {"cost": cost[i]})

    extra_kwargs["farfield_arguments"]["omega"] = -1.0
    mode = Mode.FINITE_DIFFERENCE

    p = make_parameters(
        ambient_dim=ambient_dim,
        extra_kwargs=extra_kwargs,
        suffix=f"{mode.name}_{suffix}",
        overwrite=overwrite,
    )

    with array_context_for_pickling(actx):
        if p.checkpoint_file_name.exists():
            checkpoint = make_hdf_checkpoint_manager(p.checkpoint_file_name)

            r = checkpoint.read_from("result")
            dc_duinf_fd = r["dc_duinf_fd"]
            dc_duinf_ad = r["dc_duinf_ad"]
        else:
            dc_duinf_fd, dc_duinf_ad, _ = run(
                actx, p, eps=1.0e-5, mode=mode, overwrite=overwrite, visualize=False
            )

            checkpoint = make_hdf_checkpoint_manager(p.checkpoint_file_name)
            checkpoint.write_to(
                "result",
                {
                    "dc_duinf_fd": dc_duinf_fd,
                    "dc_duinf_ad": dc_duinf_ad,
                },
            )

    filename = p.checkpoint_file_name.parent / "contour_sbr_evolution.npz"
    np.savez(
        filename,
        omegas=omegas,
        dc_duinf_fd=dc_duinf_fd,
        dc_duinf_ad=dc_duinf_ad,
        cost=cost,
    )
    logger.info("dc_duinf_fd: %s", dc_duinf_fd[0, :])
    logger.info("dc_duinf_ad: %s", dc_duinf_ad)

    if not visualize:
        return

    _plot_contour(filename, overwrite=overwrite)


def _plot_contour(filename: str, *, overwrite: bool = True) -> None:
    import pathlib

    filename = pathlib.Path(filename).resolve()
    d = np.load(filename, allow_pickle=True)

    omegas = d["omegas"]
    cost = d["cost"]

    import numpy.linalg as la

    i = np.argmin(abs(omegas + 1))
    x0 = np.array([omegas[i], cost[i]])
    x1 = np.array([omegas[i + 1], cost[i + 1]])
    g = (x0 - x1) / la.norm(x0 - x1)

    # print(i, omegas[i], g)
    dc_duinf_fd = d["dc_duinf_fd"]
    dc_duinf_ad = d["dc_duinf_ad"]
    logger.info("dc_duinf_fd: %s", dc_duinf_fd)
    logger.info("dc_duinf_ad: %s", dc_duinf_ad)

    from pystopt.visualization.matplotlib import subplots

    filename = filename.parent / "contour_sbr_evolution"
    with subplots(filename, overwrite=overwrite) as fig:
        ax = fig.gca()

        ax.plot(omegas, cost, "o-", zorder=1)
        d = 0.1 * dc_duinf_fd[0, 0] * g
        ax.arrow(omegas[i], cost[i], d[0], d[1], color="k", width=0.009, zorder=2)
        d = 0.1 * dc_duinf_ad[0] * g
        ax.arrow(omegas[i], cost[i], d[0], d[1], color="r", width=0.009, zorder=2)

        ax.set_xlabel(r"$\omega$")


# }}}


# {{{ plot_optim_solution


def plot_optim_solution(
    ctx_factory_or_actx,
    filename: str,
    *,
    single_plot_geometry: bool = True,
    overwrite: bool = True,
) -> None:
    actx = get_cl_array_context(ctx_factory_or_actx)

    import pathlib

    filename = pathlib.Path(filename)

    from pystopt.checkpoint import make_hdf_checkpoint_manager

    checkpoint = make_hdf_checkpoint_manager(filename, mode="r")

    # {{{ history

    from pystopt.paths import generate_filename_series, get_filename

    visualize_file_name = get_filename("history", cwd=filename.parent)

    from pystopt.checkpoint.hdf import array_context_for_pickling

    with array_context_for_pickling(actx):
        history = checkpoint.read_from("callback/callbacks/history/history")

    from pystopt.visualization.optimization import visualize_optimization_history

    visualize_optimization_history(
        visualize_file_name,
        history,
        overwrite=overwrite,
    )

    # }}}

    # {{{ plot

    if not single_plot_geometry:
        return

    visualize_file_series = generate_filename_series("visualize", cwd=filename.parent)
    p = make_parameters(ambient_dim=2, extra_kwargs={})

    import extra_optim_data as eod

    places = get_geometry_collection_from_param(actx, p)
    dofdesc = places.auto_source
    tgtdesc = dofdesc.copy(geometry="desired")

    from pystopt.visualization import make_visualizer

    discr = places.get_discretization(places.auto_source.geometry)
    vis = make_visualizer(actx, discr)

    from pystopt import bind

    cost, context = eod.make_unsteady_cost_functional(
        actx, places, None, p, source_dd=dofdesc, desire_dd=tgtdesc
    )

    xd = bind(
        places,
        cost.nodes(),
        auto_where="desired",
    )(actx, t=p.tmax, **context)
    x0 = actx.thaw(discr.nodes())

    with array_context_for_pickling(actx):
        names_and_fields = [("$x_d$", xd), ("$x_0$", x0)]
        markers = ["r-", "k--"]
        kwargs = {
            "force_clf": False,
            "write_mode": "geometry",
            "add_legend": False,
            "xlim": [-3.5, 3.25],
            "ylim": [-3.5, 3.25],
        }

        while True:
            result = checkpoint.read()

            if result is None:
                break

            from pystopt.mesh.spectral import from_spectral

            x = from_spectral(discr, result["x"], strict=False)

            filename = next(visualize_file_series)
            vis.write_file(
                filename.with_suffix("geometry"),
                [*names_and_fields, ("__self_x", x)],
                markers=[*markers, ":"],
                overwrite=True,
                **kwargs,
            )

            names_and_fields = []
            markers = []

    # }}}


# }}}


if __name__ == "__main__":
    import sys

    import pyopencl as cl

    if len(sys.argv) > 1:
        exec(sys.argv[1])
    else:
        main(cl._csc)
