# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

from dataclasses import dataclass, replace
from typing import Any

import numpy as np

import pystopt.callbacks as cb
from pystopt import bind, sym
from pystopt.simulation.unsteady import (
    StokesEvolutionCallbackManager,
    StokesEvolutionHistoryCallback,
    StokesEvolutionVisualizeCallback,
)
from pystopt.tools import get_cl_array_context, get_default_logger

logger = get_default_logger(module=__file__)


# {{{ callbacks


@dataclass(frozen=True)
class HistoryCallback(StokesEvolutionHistoryCallback):
    def __call__(self, state, *args, fields, **kwargs):
        ret = super().__call__(state, *args, fields=fields, **kwargs)

        logger.info(
            "volume %+.15e area %+.15e",
            (self.volume[-1] - self.volume[0]) / self.volume[0],
            (self.area[-1] - self.area[0]) / self.area[0],
        )

        return ret


@dataclass
class CallbackManager(StokesEvolutionCallbackManager):
    plot_modes: bool = False

    def get_output_field_getters(self):
        getters = super().get_output_field_getters()

        import pystopt.simulation.common as scm

        if self.plot_modes:
            getters += (scm.get_output_fields_modes,)

        return getters


@dataclass(frozen=True)
class VisualizeCallback(StokesEvolutionVisualizeCallback):
    frequency: int = 1
    plot_modes: bool = False

    def get_file_writers(self):
        writers = super().get_file_writers()

        import pystopt.simulation.common as scm

        if self.plot_modes:
            if self.ambient_dim == 2:
                writers += (scm.visualize_fourier_mode_writer,)
            else:
                writers += (scm.visualize_spharm_mode_writer,)

        return writers

    def get_modes_kwargs(self, state):
        return {
            "mode": "byorder",
            "output": "abs",
            "semilogy": True,
            "max_degree_or_order": 4,
        }

    def get_geometry_kwargs(self, state):
        return {"xlim": [-3.25, 3.25], "ylim": [-3.25, 3.25], "show_vertices": True}


# }}}


# {{{ geometry


def get_discretization_from_param(actx, p, mesh_name, mesh_arguments):
    import extra_optim_data as eod

    discrs = []

    o = mesh_arguments.get("offset", [0.0])[0]
    for x, y in zip([-o, o], [0.0, 0.0], strict=False):
        new_mesh_arguments = mesh_arguments.copy()
        new_mesh_arguments["offset"] = np.array([x, y, 0][: p.ambient_dim])

        px = replace(p, mesh_name=mesh_name)
        discr = eod.get_discretization_from_param(
            actx, px, mesh_name, new_mesh_arguments
        )

        discrs.append(discr)

    from pystopt.mesh import merge_disjoint_spectral_discretizations

    return merge_disjoint_spectral_discretizations(actx, discrs)


def get_geometry_collection_from_param(actx, p):
    mesh_name = {
        "fourier_double_circle": "fourier_circle",
        "fourier_double_ufo": "fourier_ufo",
        "spharm_double_sphere": "spharm_sphere",
        "spharm_double_ufo": "spharm_ufo",
    }.get(p.mesh_name, p.mesh_name)
    pre_density_discr = get_discretization_from_param(
        actx, p, mesh_name, p.mesh_arguments
    )

    from pystopt.qbx import QBXLayerPotentialSource

    qbx = QBXLayerPotentialSource(pre_density_discr, **p.qbx_arguments)

    from pytential import GeometryCollection

    return GeometryCollection(qbx, auto_where=p.long_name)


# }}}


# {{{ run


def make_parameters(
    ambient_dim: int,
    *,
    extra_kwargs: dict[str, Any] | None,
    suffix: str = "v0",
    overwrite: bool = True,
):
    if extra_kwargs is None:
        extra_kwargs = {}

    if ambient_dim == 2:
        from extra_mesh_data import StokesVolumeConservation2d_2022_02_20 as Parameters
    elif ambient_dim == 3:
        from extra_mesh_data import StokesVolumeConservation3d_2022_03_13 as Parameters
    else:
        raise ValueError(f"unsupported dimension: {ambient_dim}")

    import extra_mesh_data as emd

    p = emd.make_param_from_class(
        Parameters,
        name="evo-conservation",
        suffix=suffix,
        overwrite=overwrite,
        **extra_kwargs,
    )
    assert p.ambient_dim == ambient_dim

    return p


def run(
    ctx_factory,
    *,
    ambient_dim: int = 2,
    extra_kwargs: dict[str, Any] | None = None,
    suffix: str = "v0",
    p=None,
    visualize: bool = True,
    checkpoint: bool = True,
    overwrite: bool = True,
) -> None:
    actx = get_cl_array_context(ctx_factory)

    if p is None:
        p = make_parameters(
            ambient_dim,
            extra_kwargs=extra_kwargs,
            suffix=suffix,
            overwrite=overwrite,
        )

    logger.info("\n%s", p)

    # {{{ geometry

    import extra_optim_data as eod

    if "double" in p.mesh_name:
        places = get_geometry_collection_from_param(actx, p)
    else:
        places = eod.get_geometry_collection_from_param(actx, p, qbx=True)
    dofdesc = places.auto_source

    discr = places.get_discretization(dofdesc.geometry, dofdesc.discr_stage)
    logger.info("nspec:     %d", discr.nspec)
    logger.info("ndofs:     %d", discr.ndofs)
    logger.info("nelements: %d", discr.mesh.nelements)

    # }}}

    # {{{ stokes

    from pystopt import stokes

    bc = stokes.get_farfield_boundary_from_name(
        p.ambient_dim, p.farfield_name, **p.farfield_arguments
    )
    op = stokes.TwoPhaseSingleLayerStokesRepresentation(bc)

    from pystopt.evolution.stokes import StokesEvolutionOperator

    evolution = StokesEvolutionOperator(
        force_normal_velocity=False,
        force_tangential_velocity=False,
        force_volume_conservation=False,
    )

    # }}}

    # {{{ output

    visualize_callback_factory = visualize
    if visualize:
        visualize_callback_factory = VisualizeCallback

    output = cb.make_default_evolution_callback(
        p.checkpoint_file_name,
        manager_factory=CallbackManager,
        history_callback_factory=HistoryCallback,
        visualize_callback_factory=visualize_callback_factory,
        checkpoint_callback_factory=checkpoint,
        overwrite=overwrite,
    )

    from pystopt.simulation.common import StokesGeometryWrangler

    wrangler = StokesGeometryWrangler(
        # geometry
        places=places,
        dofdesc=dofdesc,
        is_spectral=True,
        # stokes
        op=op,
        lambdas=p.stokes_lambdas,
        context=p.stokes_arguments,
        gmres_arguments={"rtol": 1.0e-8},
        # filter
        filter_type=p.filter_type,
        filter_arguments=p.filter_arguments,
    )
    state0 = wrangler.get_initial_state(actx)

    # }}}

    # {{{ evolution source term

    from pystopt.mesh.processing import compute_group_volumes

    vd = compute_group_volumes(actx, places, dofdesc=dofdesc)

    def state_filter(state, *, disable=False):
        if disable:
            return state

        from pystopt.simulation import rescale_state_to_volume

        return rescale_state_to_volume(state, vd=vd)

    def evolution_source_term(t, state):
        # {{{ get full velocity field

        sym_u = sym.make_sym_vector("u", places.ambient_dim)
        v = bind(places, evolution.get_sym_operator(sym_u), auto_where=dofdesc)(
            actx, u=state.get_stokes_velocity()
        )

        # }}}

        return replace(state, x=state.wrangler.to_state_type(v))

    def estimate_time_step(t, state, *, disable=False):
        if disable:
            return None

        from pystopt.evolution.estimates import capillary_time_step_estimation

        places = state.get_geometry_collection()
        dt_estimate = capillary_time_step_estimation(
            actx,
            places,
            state.get_stokes_velocity(),
            method="brackbill",
            capillary_number=p.capillary_number,
            viscosity_ratio=p.viscosity_ratio,
            sources=state.wrangler.dofdesc,
        )
        logger.info("dt_estimate: %.12e", dt_estimate)

        return None
        # return p.cfl_theta * dt_estimate

    # }}}

    # {{{ time stepping

    from pystopt.evolution.estimates import fixed_time_step_from

    maxit, tmax, timestep = fixed_time_step_from(
        # maxit=5, tmax=None, timestep=p.timestep,
        maxit=p.maxit,
        tmax=p.tmax,
        timestep=p.timestep,
    )

    logger.info("tmax:  %g", tmax)
    logger.info("maxit: %d", maxit)
    logger.info("dt:    %.5e", timestep)

    from pystopt.evolution import make_adjoint_time_stepper

    stepper = make_adjoint_time_stepper(
        p.time_step_method,
        "state",
        state0,
        evolution_source_term,
        dt_start=timestep,
        t_start=0.0,
        state_filter=state_filter,
        dt_max=timestep,
        estimate_time_step=estimate_time_step,
    )

    from pystopt.checkpoint.hdf import array_context_for_pickling

    with array_context_for_pickling(actx):
        for event in stepper.run(tmax=tmax, maxit=maxit):
            if (event.n - 1) % 1 == 0:
                ret = output(event.state_component, event=event)
            else:
                ret = 1

            if ret == 0:
                from pystopt.tools import c

                logger.info(c.warn("Stopped by callback..."))
                break

        if checkpoint:
            chk = output["checkpoint"].checkpoint
            history = output["history"].to_numpy()

            from pystopt.tools import dc_asdict

            chk.write_to("parameters", dc_asdict(p), overwrite=True)
            chk.write_to("orig_discr", discr, overwrite=True)
            chk.write_to("history", history, overwrite=True)

            chk.done()

    if visualize and checkpoint:
        plot_evolution(actx, p.checkpoint_file_name)

    # }}}


def run_multiple(
    ctx_factory,
    *,
    ambient_dim: int = 2,
    visualize: bool = True,
    overwrite: bool = True,
) -> None:
    cases = [
        (1.0e-2, "aeuler"),
        (1.0e-3, "aeuler"),
        (1.0e-2, "assprk33"),
        (1.0e-3, "assprk33"),
    ]

    for i, (timestep, time_step_method) in enumerate(cases):
        suffix = f"{time_step_method}_v{i}"
        extra_kwargs = {"timestep": timestep, "time_step_method": time_step_method}
        p = make_parameters(
            ambient_dim,
            extra_kwargs=extra_kwargs,
            suffix=suffix,
            overwrite=overwrite,
        )

        if p.checkpoint_file_name.exists():
            continue

        run(
            ctx_factory,
            ambient_dim=ambient_dim,
            p=p,
            visualize=visualize,
            overwrite=overwrite,
        )


# }}}


# {{{ plot


def plot_evolution(ctx_factory, filename):
    actx = get_cl_array_context(ctx_factory)

    import pathlib

    filename = pathlib.Path(filename)

    from pystopt.paths import get_filename

    dirname = filename.parent
    visualize_file_name = get_filename("history", cwd=dirname)

    # {{{ load

    from pystopt.checkpoint import make_hdf_checkpoint

    checkpoint = make_hdf_checkpoint(filename, mode="r")

    from pystopt.checkpoint.hdf import array_context_for_pickling

    with array_context_for_pickling(actx):
        history = checkpoint.read("history")

    # }}}

    # {{{ plot

    from pystopt.visualization.optimization import visualize_stokes_evolution_history

    visualize_stokes_evolution_history(visualize_file_name, history)

    # }}}


# }}}


if __name__ == "__main__":
    import sys

    import pyopencl as cl

    if len(sys.argv) > 1:
        exec(sys.argv[1])
    else:
        run(cl._csc)
