# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

from dataclasses import replace

import numpy as np

from pystopt import bind, grad, sym
from pystopt.tools import get_cl_array_context, get_default_logger

logger = get_default_logger(module=__file__)


# {{{ run


def run(ctx_factory_or_actx, *, ambient_dim: int = 2, visualize: bool = True) -> None:
    actx = get_cl_array_context(ctx_factory_or_actx)

    # {{{ geometry

    target_order = 3
    source_ovsmp = 5
    qbx_order = 4

    if ambient_dim == 2:
        mesh_name = "fourier_ellipse"
        mesh_arguments = {}
        resolution = 128
    elif ambient_dim == 3:
        mesh_name = "spharm_spheroid"
        mesh_arguments = {}
        resolution = 32
    else:
        raise ValueError(f"unsupported dimension: {ambient_dim}")

    from meshmode.discretization.poly_element import InterpolatoryQuadratureGroupFactory

    mesh_arguments["mesh_order"] = target_order
    mesh_arguments["target_order"] = target_order
    mesh_arguments["group_factory_cls"] = InterpolatoryQuadratureGroupFactory

    import pystopt.mesh.generation as mgen

    gen = mgen.get_mesh_generator_from_name(mesh_name, **mesh_arguments)
    discr = mgen.generate_discretization(gen, actx, resolution)

    from pystopt.qbx import QBXLayerPotentialSource

    qbx = QBXLayerPotentialSource(
        discr,
        fine_order=source_ovsmp * target_order,
        qbx_order=qbx_order,
        fmm_order=10,
        fmm_backend="sumpy",
        _disable_refinement=True,
    )

    from pytential import GeometryCollection

    places = GeometryCollection(qbx, auto_where="qbx")
    dofdesc = places.auto_source

    density_discr = places.get_discretization(dofdesc.geometry, dofdesc.discr_stage)
    logger.info("nelements:     %d", density_discr.mesh.nelements)
    logger.info("ndofs:         %d", density_discr.ndofs)
    logger.info("nspec:         %d", density_discr.nspec)

    # }}}

    # {{{ setup stokes

    import pystopt.cost as pc

    cost = pc.NormalVelocityFunctional(
        u=sym.make_sym_vector("u", places.ambient_dim), ud=0
    )

    import pystopt.stokes as stk

    bc = stk.get_farfield_boundary_from_name(
        ambient_dim, "uniform", uinf=1.0, capillary_number_name="ca"
    )

    op = stk.TwoPhaseSingleLayerStokesRepresentation(bc)
    ad = stk.TwoPhaseSingleLayerStokesRepresentation(
        stk.make_static_adjoint_boundary_conditions(ambient_dim, cost)
    )

    # }}}

    # {{{ compute gradients

    if visualize:
        from pystopt.checkpoint import make_default_checkpoint_path

        visualize_file_name = make_default_checkpoint_path(
            "stokes_adjoint_gradient", "v0", overwrite=True
        ).with_suffix("")

        from pystopt.visualization import make_visualizer

        vis = make_visualizer(actx, density_discr)

    lambdas = np.array([0.1, 0.5, 1.0, 2.0, 10.0, 50.0])
    capillary_numbers = np.array([0.01, 0.05, 0.1, 0.5, 1.0, 2.0])
    # lambdas = np.array([1.0])
    # capillary_numbers = np.array([0.01])

    from itertools import product

    for i, j in product(range(lambdas.size), range(capillary_numbers.size)):
        # {{{ solve Stokes

        context = {
            "viscosity_ratio": lambdas[i],
            "ca": capillary_numbers[j],
        }

        q = stk.single_layer_solve(
            actx,
            places,
            op,
            gmres_arguments={},
            lambdas={"viscosity_ratio": context["viscosity_ratio"]},
            sources=[dofdesc],
            context=context,
        )
        u = stk.velocity(q, side=None, qbx_forced_limit=+1, dofdesc=dofdesc)

        context = context.copy()
        context["q"] = q.density
        context["u"] = u

        qstar = stk.single_layer_solve(
            actx,
            places,
            ad,
            gmres_arguments={},
            lambdas={"viscosity_ratio": context["viscosity_ratio"]},
            sources=[dofdesc],
            context=context,
        )
        qstar = replace(qstar, density_name="qstar")
        ustar = stk.velocity(qstar, side=None, qbx_forced_limit=+1, dofdesc=dofdesc)

        context = context.copy()
        context["qstar"] = qstar.density
        context["ustar"] = ustar

        # }}}

        # {{{ compute gradient terms

        from pystopt.operators import make_sym_density

        sym_q = make_sym_density(op, q.density_name)
        sym_qstar = make_sym_density(ad, qstar.density_name)
        sym_lambda = sym.var("viscosity_ratio")

        # surface tension
        t_jmp = bind(
            places,
            sym.n_dot(grad.shape(op.bc, op, sym_q, ad, sym_qstar)),
            auto_where=dofdesc,
        )(actx, **context)

        # viscosity ratio
        from pystopt.stokes.derivatives import _stokes_bulk_shape_derivative

        terms = _stokes_bulk_shape_derivative(op, sym_q, ad, sym_qstar, sym_lambda)

        t_vir_0 = bind(places, terms["t0"], auto_where=dofdesc)(actx, **context)
        t_vir_1 = bind(places, terms["t1"], auto_where=dofdesc)(actx, **context)
        t_vir_2 = bind(places, terms["t2"], auto_where=dofdesc)(actx, **context)

        # }}}

        # {{{ visualize

        if not visualize:
            continue

        from pystopt.tools import slugify

        suffix = slugify(f"vr_{lambdas[i]:02.2f}_ca_{capillary_numbers[j]:02.2f}")
        filename = visualize_file_name.with_stem(f"{visualize_file_name.stem}_{suffix}")

        if ambient_dim == 2:
            vis.write_file(
                filename.with_stem(f"{filename.stem}_jmp"),
                [("T", t_jmp)],
                add_legend=False,
                ylim=[-1200, 3000],
                overwrite=True,
            )
            vis.write_file(
                filename.with_stem(f"{filename.stem}_vir_0"),
                [("T", t_vir_0)],
                add_legend=False,
                ylim=[-46, 0.1],
                overwrite=True,
            )
            vis.write_file(
                filename.with_stem(f"{filename.stem}_vir_1"),
                [("T", t_vir_1)],
                add_legend=False,
                ylim=[-1400, 220],
                overwrite=True,
            )
            vis.write_file(
                filename.with_stem(f"{filename.stem}_vir_2"),
                [("T", t_vir_2)],
                add_legend=False,
                ylim=[-320, 500],
                overwrite=True,
            )
            vis.write_file(
                filename.with_stem(f"{filename.stem}_vir_3"),
                [("T", t_vir_1 + t_vir_2)],
                add_legend=False,
                ylim=[-900, 20],
                overwrite=True,
            )
        else:
            vis.write_file(
                filename,
                [
                    ("t_jmp", t_jmp),
                    ("t_vir_0", t_vir_0),
                    ("t_vir_1", t_vir_1),
                    ("t_vir_2", t_vir_2),
                ],
                overwrite=True,
            )

        # }}}

    # }}}


# }}}


if __name__ == "__main__":
    import sys

    import pyopencl as cl

    if len(sys.argv) > 1:
        exec(sys.argv[1])
    else:
        run(cl._csc)
