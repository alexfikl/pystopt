# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

from dataclasses import dataclass, field
from typing import Any, ClassVar

import numpy as np

from arraycontext import ArrayContext
from pytential import GeometryCollection
from pytools import memoize_method

import pystopt.callbacks as cb
from pystopt import bind, grad, sym
from pystopt.cost import GeometryShapeFunctional
from pystopt.evolution import TimeWrangler
from pystopt.simulation import as_state_container
from pystopt.simulation.farfield import FarfieldOptimizationHistoryCallback
from pystopt.tools import get_cl_array_context, get_default_logger

logger = get_default_logger(module=__file__)


# {{{ cost


@dataclass(frozen=True)
class HelixFunctional(GeometryShapeFunctional):
    r"""Implements the evaluation and gradient of the cost functional

    .. math::

        J = \frac{1}{2} \int_0^T \int_\Sigma
            \|\mathbf{X}(t) - \mathbf{X}_d(t)\|^2 \dx[S]

    where :math:`\mathbf{X}` and :math:`\mathbf{X}_d` are spheres offset in
    the :math:`x` direction by :math:`\pm c`. They are translated in time
    using a solid body rotation velocity field

    .. math::

        \mathbf{u} = (-\omega y, \omega x, U)

    with :math:`(\omega, U)` and :math:`(\omega_d, U_d)` respectively. As the
    geometry is spherical, we can actually integrate the spatial component
    analytically, which gives

    .. math::

        \frac{1}{2} \int_\Sigma \|\mathbf{X} - \mathbf{X}_d\|^2 =
        \frac{2 \pi}{3} \left(
        4 + 6 c^2
        + 3 (U - U_d)^2 t^2
        - 2 (2 + 3 c^2) \cos (\omega - \omega_d) t
        \right)

    for a single droplet. For the helix case, we assume that we have two drops
    at :math:`(\pm c, 0, 0)` at :math:`t = 0`, so the above is doubled. In
    fact, this can also be integrated in time to obtain

    .. math::

        J =
    """

    offset: float = 2.0
    w: sym.var = field(default_factory=lambda: sym.var("w"))
    wd: sym.var = field(default_factory=sym.var("w_d"))
    omega: sym.var = field(default_factory=sym.var("omega"))
    omega_d: sym.var = field(default_factory=sym.var("omega_d"))

    t: sym.var = field(default_factory=sym.var("t"))
    tmax: sym.var | None = field(default_factory=sym.var("T"))

    def __call__(
        self,
        ambient_dim: int,
        *,
        dim: int | None = None,
        dofdesc: sym.DOFDescriptorLike | None = None,
    ) -> sym.Expression:
        c = self.offset
        t = self.t
        w, wd = self.w, self.wd
        omega, omega_d = self.omega, self.omega_d

        if self.tmax is None:
            int_cost = (
                2
                * np.pi
                / 3
                * (
                    4
                    + 6 * c**2
                    + 3 * (w - wd) ** 2 * t**2
                    - 2 * (2 + 3 * c**2) * sym.cos((omega - omega_d) * t)
                )
            )
        else:
            tmax = self.tmax
            int_cost_f = (
                2
                * np.pi
                / 3
                * (
                    tmax * (4 + 6 * c**2 + (w - wd) ** 2 * tmax**2)
                    - (
                        2
                        * (2 + 3 * c**2)
                        * sym.sin((omega - omega_d) * tmax)
                        / (omega - omega_d)
                    )
                )
            )
            int_cost_o = (
                2 * np.pi / 3 * (w - wd) ** 2 * tmax**3
                + 2 * np.pi / 9 * (2 + 3 * c**2) * tmax**3 * (omega - omega_d) ** 2
            )

            int_cost = sym.If(
                sym.Comparison(sym.abs(omega - omega_d), "<", 1.0e-8),
                int_cost_o,
                int_cost_f,
            )

        return 2 * int_cost


@grad.farfield.register(HelixFunctional)
def _grad_shape_helix(
    self: HelixFunctional,
    ambient_dim: int,
    *,
    dim: int | None = None,
    dofdesc: sym.DOFDescriptorLike | None = None,
) -> sym.Expression:
    c = self.offset
    t = self.t
    w, wd = self.w, self.wd
    omega, omega_d = self.omega, self.omega_d

    if self.tmax is None:
        domega = (8.0 / 3.0 + 4.0 * c**2) * np.pi * sym.sin((omega - omega_d) * t) * t
        dw = 4.0 * np.pi * (w - wd) * t**2
    else:
        tmax = self.tmax
        domega_f = (
            4
            / 3
            * np.pi
            * (2 + 3 * c**2)
            / ((omega - omega_d) ** 2 + 1.0e-8)
            * (
                sym.sin((omega - omega_d) * tmax)
                - tmax * (omega - omega_d) * sym.cos((omega - omega_d) * tmax)
            )
        )
        # NOTE: this is the limit as omega -> omega_d up to 4th order
        domega_o = (4 / 9 * (2 + 3 * c**2) * np.pi * tmax**3) * (omega - omega_d) - (
            2 / 45 * (2 + 3 * c**2) * np.pi * tmax**5
        ) * (omega - omega_d) ** 3

        domega = sym.If(
            sym.Comparison(sym.abs(omega - omega_d), "<", 1.0e-8), domega_o, domega_f
        )

        dw = 4.0 / 3.0 * np.pi * (w - wd) * tmax**3

    from pytools.obj_array import make_obj_array

    return make_obj_array([2.0 * domega, 2.0 * dw])


# }}}


# {{{ wrangler


@dataclass(frozen=True)
class HelixWrangler:
    places: GeometryCollection
    cost: HelixFunctional
    time: TimeWrangler
    context: dict[str, Any]

    @property
    def ambient_dim(self):
        return 3

    def evaluate_cost(self, actx, omega: float, w: float):
        return bind(self.places, self.cost(self.ambient_dim))(
            actx, omega=omega, w=w, **self.context
        )

    def evaluate_gradient(self, actx, omega: float, w: float):
        return bind(self.places, grad.farfield(self.cost, self.ambient_dim))(
            actx, omega=omega, w=w, **self.context
        )


@as_state_container
class HelixState:
    _FIELDNAMES: ClassVar[tuple[str, str]] = ("omega", "w")

    x: np.ndarray
    wrangler: HelixWrangler
    array_context: ArrayContext

    @property
    @memoize_method
    def cost(self) -> float:
        return self.wrangler.evaluate_cost(
            self.array_context, omega=self.x[0], w=self.x[1]
        )

    @property
    @memoize_method
    def gradient(self) -> np.ndarray:
        return self.wrangler.evaluate_gradient(
            self.array_context, omega=self.x[0], w=self.x[1]
        )

    @property
    def cost_gradient(self) -> tuple[float, np.ndarray]:
        return self.cost, self.gradient

    # {{{ wrap / unwrap

    def wrap(self, x: np.ndarray) -> "HelixState":
        from dataclasses import replace

        return replace(self, x=x)

    @classmethod
    def unwrap(cls, state: Any) -> Any:
        if isinstance(state, HelixState):
            return state.x

        return state

    # }}}


# }}}


# {{{ callback


@dataclass
class CallbackManager(cb.CallbackManager):
    def get_output_field_getters(self):
        def get_output_fields_farfield(state, **kwargs):
            fields = {}
            fields["omega"] = cb.OutputField.checkpoint("omega", state.x[0])
            fields["w"] = cb.OutputField.checkpoint("w", state.x[1])

            return fields

        from pystopt.simulation.common import get_output_fields_optimization

        return (
            get_output_fields_optimization,
            get_output_fields_farfield,
        )

    def __call__(self, *args: Any, **kwargs: Any):
        from dataclasses import replace

        info = kwargs.pop("info")
        info = replace(info, g=HelixState.unwrap(info.g), d=HelixState.unwrap(info.d))

        return super().__call__(*args, info=info, **kwargs)


@dataclass(frozen=True)
class HistoryCallback(FarfieldOptimizationHistoryCallback):
    def __call__(self, state, *args, **kwargs):
        logger.info(
            "omega %.5e / %.5e w %.5e / %.5e",
            state.x[0],
            state.wrangler.context["omega_d"],
            state.x[1],
            state.wrangler.context["w_d"],
        )
        logger.info("gradient omega %.5e w %.5e", state.gradient[0], state.gradient[1])

        return super().__call__(state, *args, **kwargs)


# }}}


# {{{


def make_parameters(
    *,
    ambient_dim: int,
    extra_kwargs: dict[str, Any],
    suffix: str = "v0",
    overwrite: bool = True,
):
    if ambient_dim == 3:
        from extra_mesh_data import StokesHelicalBoundary_2021_12_06 as Parameters
    else:
        raise ValueError(f"unsupported dimension: {ambient_dim}")

    import extra_mesh_data as emd

    p = emd.make_param_from_class(
        Parameters,
        name="boundary-optim",
        suffix=suffix,
        overwrite=overwrite,
        **extra_kwargs,
    )
    assert p.ambient_dim == ambient_dim

    return p


def main(
    ctx_factory_or_actx,
    *,
    suffix: str = "v0",
    overwrite: bool = True,
    visualize: bool = True,
):
    actx = get_cl_array_context(ctx_factory_or_actx)
    p = make_parameters(
        ambient_dim=3, extra_kwargs={}, suffix=suffix, overwrite=overwrite
    )

    # {{{ cost

    cost = HelixFunctional(offset=sym.var("c"))

    context = {"c": 2.0}
    context["T"] = p.tmax
    context["omega_d"] = p.cost_arguments["omega_d"]
    context["w_d"] = p.cost_arguments["height_d"]

    # }}}

    # {{{ wrangler

    def norm(x, p) -> float:
        return np.linalg.norm(state0.unwrap(x), ord=p)

    from pytential.source import PointPotentialSource

    places = GeometryCollection(
        {p.long_name: PointPotentialSource(actx.np.zeros((3, 0), np.float64))},
        auto_where=p.long_name,
    )

    time = TimeWrangler(
        time_step_name=p.time_step_method,
        maxit=p.maxit,
        tmax=p.tmax,
        dt=p.timestep,
        theta=1.0,
        # maxit=10, tmax=None, dt=p.timestep, theta=1.0,
        estimate_time_step=False,
        # state_filter=state_filter,
    )

    wrangler = HelixWrangler(places=places, cost=cost, time=time, context=context)

    from pystopt.callbacks import make_default_shape_callback

    callback = make_default_shape_callback(
        p.checkpoint_file_name,
        manager_factory=CallbackManager,
        history_callback_factory=HistoryCallback,
        visualize_callback_factory=False,
        norm=norm,
        overwrite=overwrite,
    )
    checkpoint = callback["checkpoint"].checkpoint

    # }}}

    # {{{ state

    omega = p.farfield_arguments["omega"]
    w = p.farfield_arguments["height"]
    omega, w = 3 * np.pi, np.pi
    # omega, w = 4 * np.pi, 2

    from pytools.obj_array import make_obj_array

    state0 = HelixState(
        x=make_obj_array([omega, w]), wrangler=wrangler, array_context=actx
    )

    # }}}

    # {{{ optimization

    def vdot(x, y) -> float:
        return np.vdot(HelixState.unwrap(x), HelixState.unwrap(y))

    from pystopt.optimize import CartesianEucledeanSpace

    manifold = CartesianEucledeanSpace(vdot=vdot)

    from pystopt.optimize import get_line_search_from_name

    linesearch = get_line_search_from_name(
        p.cg_linesearch_name, fun=lambda x: x.cost, **p.cg_linesearch_arguments
    )

    from pystopt.optimize import get_descent_direction_from_name

    descent = get_descent_direction_from_name(
        p.cg_descent_name, **p.cg_descent_arguments
    )

    options = {
        **p.cg_arguments,
        "linesearch": linesearch,
        "descent": descent,
        "manifold": manifold,
    }

    from pystopt.optimize.steepest import minimize

    r = minimize(
        fun=lambda x: x.cost,
        x0=state0,
        jac=lambda x: x.wrap(x.gradient),
        funjac=lambda x: (x.cost, x.wrap(x.gradient)),
        callback=callback,
        options=options,
    )
    logger.info("result:\n%s", r.pretty())

    # }}}

    from pystopt.tools import dc_asdict

    checkpoint.write_to("parameters", dc_asdict(p), overwrite=True)
    checkpoint.write_to("callback", callback, overwrite=True)

    from dataclasses import replace

    rx = replace(r, x=state0.unwrap(r.x))
    checkpoint.write_to("result", rx, overwrite=True)
    checkpoint.done()


# }}}


# {{{ plot_contour


def plot_contour(ctx_factory_or_actx, *, suffix: str = "v0", overwrite: bool = True):
    actx = get_cl_array_context(ctx_factory_or_actx)
    p = make_parameters(
        ambient_dim=3, extra_kwargs={}, suffix=suffix, overwrite=overwrite
    )

    from pytential.source import PointPotentialSource

    places = GeometryCollection(
        {p.long_name: PointPotentialSource(actx.np.zeros((3, 0), np.float64))},
        auto_where=p.long_name,
    )

    from pystopt.paths import get_filename

    visualize_file_name = get_filename("history", cwd=p.checkpoint_file_name.parent)

    # {{{ cost

    cost = HelixFunctional(offset=sym.var("c"))

    context = {"c": 2.0}
    context["T"] = p.tmax
    context["omega_d"] = p.cost_arguments["omega_d"]
    context["w_d"] = p.cost_arguments["height_d"]

    bound_cost = bind(places, cost(places.ambient_dim))

    omega = np.linspace(np.pi, 5.0 * np.pi, 256)
    w = np.linspace(1, 3, 256)
    cost_omega_w = np.empty((omega.size, w.size))

    from itertools import product

    for i, j in product(range(omega.size), range(w.size)):
        cost_omega_w[i, j] = bound_cost(actx, omega=omega[i], w=w[i], **context)

    # }}}

    omega, w = np.meshgrid(omega, w)
    cost_omega_w = cost_omega_w.T

    from pystopt.visualization.matplotlib import subplots

    with subplots(
        visualize_file_name.with_suffix("contourf"), overwrite=overwrite
    ) as fig:
        ax = fig.gca()
        ax.grid(visible=False, which="both")

        im = ax.contourf(omega, w, cost_omega_w, levels=32, zorder=0)
        ax.contour(
            omega, w, cost_omega_w, colors="k", linewidths=(1,), levels=32, zorder=1
        )
        ax.set_xlabel(r"$\omega$")
        ax.set_ylabel("$w$")

        fig.colorbar(im, ax=ax)

    with subplots(
        visualize_file_name.with_suffix("cost_omega_w"),
        projection="3d",
        overwrite=overwrite,
    ) as fig:
        ax = fig.gca()
        ax.view_init(15, -90)

        im = ax.plot_surface(omega, w, cost_omega_w, cmap="viridis", antialiased=True)
        # ax.set_xlabel(r"$\omega$")
        # ax.set_ylabel("$w$")


# }}}


if __name__ == "__main__":
    import sys

    import pyopencl as cl

    if len(sys.argv) > 1:
        exec(sys.argv[1])
    else:
        main(cl._csc)
