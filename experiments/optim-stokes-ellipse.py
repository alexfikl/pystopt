# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

from dataclasses import dataclass, field, replace

import numpy as np
import pyopencl as cl

import pystopt.callbacks as cb
import pystopt.simulation as sim
import pystopt.simulation.spheroid as sph
from pystopt.tools import get_cl_array_context, get_default_logger

logger = get_default_logger(module=__file__)


# {{{ callbacks


@dataclass
class CallbackManager(sph.SpheroidOptimizationCallbackManager):
    def get_output_field_getters(self):
        from pystopt.simulation.common import get_output_fields_stokes

        return (
            *super().get_output_field_getters(),
            get_output_fields_stokes,
        )


@dataclass(frozen=True)
class VisualizeCallback(sim.StokesShapeOptimizationVisualizeCallback):
    def get_geometry_kwargs(self, state):
        return {"xlim": [-2.25, 2.25], "ylim": [-2.25, 2.25]}


@dataclass(frozen=True)
class HistoryCallback(sim.StokesShapeOptimizationHistoryCallback):
    x: list[np.ndarray] = field(default_factory=list, repr=False)

    def __call__(self, *args, **kwargs):
        r = super().__call__(*args, **kwargs)

        (state,) = args
        self.x.append(state.x)

        logger.info(
            "radius %.5e aspect_ratio %.5e angles" + (" %.5e" * (state.x.size - 2)),
            state.x[0],
            state.x[1],
            *state.x[2:],
        )

        grad = state.gradient
        logger.info(
            "radius %.5e aspect_ratio %.5e angles" + (" %.5e" * (state.x.size - 2)),
            grad[0],
            grad[1],
            *grad[2:],
        )

        from pystopt import bind, sym

        volume = bind(
            state.places,
            sym.surface_volume(state.ambient_dim),
            auto_where=state.dofdesc,
        )(state.array_context)
        logger.info(
            "volume %.12e / %.12e",
            state.array_context.to_numpy(volume),
            state.wrangler.context["vd"],
        )

        return r


# }}}


# {{{ run


def check_gradient_vs_finite(
    state0: sph.SpheroidStokesOptimizationState, eps: float = 1.0e-5
) -> None:
    logger.info("eps: %.5e", eps)
    cost0 = state0.cost

    grad_ad = state0.gradient
    grad_fd = np.empty_like(grad_ad)
    for i in range(state0.x.size):
        x = state0.x.copy()
        x[i] = x[i] + eps
        state = replace(state0, x=x)

        grad_fd[i] = (state.cost - cost0) / eps
        logger.info(
            "gradient: %6s ad %+.5e fd %+.5e error %.5e",
            state0._FIELDNAMES[i],
            grad_ad[i],
            grad_fd[i],
            abs(grad_ad[i] - grad_fd[i]) / abs(grad_fd[i]),
        )

    return grad_fd


def make_wrangler(actx, p) -> sph.SpheroidStokesOptimizationWrangler:
    # {{{ geometry

    from pytools.obj_array import make_obj_array

    if p.ambient_dim == 2:
        x = make_obj_array([
            p.mesh_arguments["radius"],
            p.mesh_arguments["aspect_ratio"],
            p.mesh_arguments.pop("alpha"),
        ])
    elif p.ambient_dim == 3:
        x = make_obj_array([
            p.mesh_arguments["radius"],
            p.mesh_arguments["aspect_ratio"],
            *p.mesh_arguments.pop("alpha"),
        ])

    from pystopt.mesh.generation import make_transform_matrix_from_angles

    p.mesh_arguments["transform_matrix"] = make_transform_matrix_from_angles(*x[2:])

    import extra_optim_data as eod

    places, _ = eod.get_geometry_collection_from_param(actx, p, qbx=True)
    srcdesc = places.auto_source
    tgtdesc = srcdesc.copy(geometry="desired")

    # }}}

    # {{{ optimization problem

    from pystopt import stokes

    bc = stokes.get_extensional_farfield(
        p.ambient_dim, alpha=1.0, axis=2, capillary_number_name="ca"
    )

    from pystopt.stokes import TwoPhaseSingleLayerStokesRepresentation

    op = TwoPhaseSingleLayerStokesRepresentation(bc)

    cost, context = eod.make_stokes_cost_functional(
        actx, places, op, p, source_dd=srcdesc, desire_dd=tgtdesc
    )
    context.update(p.stokes_arguments)

    wrangler = sph.SpheroidStokesOptimizationWrangler(
        # geometry
        places=places,
        dofdesc=srcdesc,
        is_spectral=False,
        resolution=p.resolution,
        mesh_kwargs=p.mesh_arguments,
        # cost
        cost=cost,
        context=context,
        # stokes
        op=op,
        lambdas=p.stokes_lambdas,
        gmres_arguments={},
        # filtering
        filter_type="none",
        filter_arguments=p.filter_arguments,
    )
    state0 = wrangler.get_initial_state(actx, x)

    # }}}

    return wrangler, state0


def make_parameters(
    ambient_dim: int,
    *,
    viscosity_ratio: float = 1,
    capillary_number: float = 0.05,
    from_restart_filename: str | None = None,
    suffix: str = "v0",
    overwrite: bool = True,
):
    if ambient_dim == 2:
        from extra_mesh_data import StokesEllipseSteady_2021_11_02 as Parameters
    elif ambient_dim == 3:
        from extra_mesh_data import StokesSpheroidSteady_2021_11_02 as Parameters
    else:
        raise ValueError(f"unsupported dimension: '{ambient_dim}'")

    import extra_mesh_data as emd

    suffix = f"ca-{capillary_number:.3f}-vr-{viscosity_ratio:.2f}-{suffix}"
    p = emd.make_param_from_class(
        Parameters,
        name="optim-stokes-rom",
        from_restart_filename=from_restart_filename,
        suffix=suffix,
        overwrite=overwrite,
        # kwargs
        viscosity_ratio=viscosity_ratio,
        capillary_number=capillary_number,
    )
    assert p.ambient_dim == ambient_dim

    return p


def run(
    ctx_factory_or_actx,
    *,
    ambient_dim: int = 2,
    viscosity_ratio: float = 1,
    capillary_number: float = 0.05,
    from_restart: bool | int = False,
    suffix: str = "v0",
    overwrite: bool = True,
    visualize: bool = True,
):
    assert not from_restart, "not quite implemented at the moment"
    actx = get_cl_array_context(ctx_factory_or_actx)

    p = make_parameters(
        ambient_dim,
        viscosity_ratio=viscosity_ratio,
        capillary_number=capillary_number,
        suffix=suffix,
        overwrite=overwrite,
    )

    wrangler, state0 = make_wrangler(actx, p)

    # {{{ output

    if visualize:
        visualize_callback_factory = VisualizeCallback
    else:
        visualize_callback_factory = False

    callback = cb.make_default_shape_callback(
        p.checkpoint_file_name,
        manager_factory=CallbackManager,
        visualize_callback_factory=visualize_callback_factory,
        history_callback_factory=HistoryCallback,
        norm=lambda x, p: np.linalg.norm(state0.unwrap(x), ord=p),
        from_restart=from_restart,
        overwrite=overwrite,
    )
    checkpoint = callback["checkpoint"].checkpoint

    from pystopt.tools import dc_stringify

    logger.info("\n%s", str(p))
    logger.info("Context\n%s", dc_stringify(wrangler.context))

    import os

    logger.info("=" * 48)
    logger.info("Send SIGUSR1 to PID %s to stop optimization", os.getpid())

    from pystopt.paths import relative_to

    logger.info("Stop file '%s'", relative_to(callback.stop_file_name))
    if from_restart:
        logger.info("Restarted from '%s'", relative_to(p.checkpoint_file_name))
    logger.info("=" * 48)

    srcdesc = wrangler.places.auto_source
    discr = wrangler.places.get_discretization(srcdesc.geometry, srcdesc.discr_stage)

    logger.info("nelements: %d", discr.mesh.nelements)
    logger.info("ndofs:     %d", discr.ndofs)
    logger.info("nspec:     %d", discr.nspec)

    # }}}

    # {{{ minimize

    from pystopt.optimize import CartesianEucledeanSpace

    manifold = CartesianEucledeanSpace(vdot=lambda x, y: np.vdot(x.x, y.x))

    from pystopt.optimize import get_line_search_from_name

    linesearch = get_line_search_from_name(
        p.cg_linesearch_name, fun=lambda x: x.cost, **p.cg_linesearch_arguments
    )

    from pystopt.optimize import get_descent_direction_from_name

    descent = get_descent_direction_from_name(
        p.cg_descent_name, **p.cg_descent_arguments
    )

    options = {
        **p.cg_arguments,
        "linesearch": linesearch,
        "descent": descent,
        "manifold": manifold,
    }

    from pystopt.checkpoint.hdf import array_context_for_pickling

    with array_context_for_pickling(actx):
        from pystopt.optimize.steepest import minimize

        r = minimize(
            fun=lambda x: x.cost,
            x0=state0,
            jac=lambda x: x.wrap(x.gradient),
            funjac=lambda x: (x.cost, x.wrap(x.gradient)),
            callback=callback,
            options=options,
        )

        logger.info("result:\n%s", r.pretty())

    # }}}

    # {{{ plot

    with array_context_for_pickling(actx):
        from pystopt.tools import dc_asdict

        checkpoint.write_to("parameters", dc_asdict(p), overwrite=True)
        checkpoint.write_to("callback", callback, overwrite=True)

        r = replace(r, x=state0.unwrap(r.x))
        checkpoint.write_to("result", r, overwrite=True)

        checkpoint.done()

    if not visualize:
        return

    plot_history(actx, p.checkpoint_file_name, overwrite=overwrite)

    # }}}


# }}}


# {{{ run convergence


def run_convergence_eps(
    ctx_factory_or_actx,
    *,
    ambient_dim: int = 2,
    viscosity_ratio: float = 1,
    capillary_number: float = 0.05,
    suffix: str = "v0",
    overwrite: bool = True,
):
    actx = get_cl_array_context(ctx_factory_or_actx)

    nvariables = 3 if ambient_dim == 2 else 5
    eps = 10.0 ** (-np.arange(1, 8))

    p = make_parameters(
        ambient_dim,
        viscosity_ratio=viscosity_ratio,
        capillary_number=capillary_number,
        suffix=suffix,
        overwrite=overwrite,
    )
    _, state0 = make_wrangler(actx, p)
    logger.info("\n%s", p)

    from pystopt.measure import EOCRecorder, stringify_eoc

    eoc = [EOCRecorder(name=state0._FIELDNAMES[i]) for i in range(nvariables)]

    grad_ad = state0.gradient
    grad_fd = np.empty((nvariables, eps.size))
    for i in range(eps.size):
        grad_fd[:, i] = check_gradient_vs_finite(state0, eps=eps[i])

        for j in range(nvariables):
            error = abs(grad_fd[j, i] - grad_ad[j]) / abs(grad_fd[j, i])
            eoc[j].add_data_point(eps[i], error)

    # NOTE: this is not going to converge because we're not comparing the same
    # values. basically when deriving the shape gradient, we assume that the
    # perturbations only have normal components. however, here the perturbation
    # is of the form
    #       xhat = A x
    # where A depends on which variable we're taking the derivative wrt, and
    # is generally not only in the normal direction.

    logger.info("\n%s", stringify_eoc(*eoc))

    filename = p.checkpoint_file_name.with_suffix(".npz")
    logger.info("output: %s", filename)

    from pystopt.tools import dc_asdict

    np.savez(
        filename, eps=eps, grad_ad=grad_ad, grad_fd=grad_fd, parameters=dc_asdict(p)
    )


# }}}


# {{{ plot optimization history


def plot_history(ctx_factory, filename: str, *, overwrite: bool = True) -> None:
    actx = get_cl_array_context(ctx_factory)

    import pathlib

    filename = pathlib.Path(filename)

    from pystopt.paths import get_filename

    visualize_file_name = get_filename("history", cwd=filename.parent)

    from pystopt.checkpoint import make_hdf_checkpoint

    checkpoint = make_hdf_checkpoint(filename, mode="r")

    from pystopt.checkpoint.hdf import array_context_for_pickling

    with array_context_for_pickling(actx):
        history = checkpoint.read("callback/callbacks/history/history")
    del checkpoint

    from pystopt.visualization.optimization import visualize_optimization_history

    visualize_optimization_history(
        visualize_file_name,
        history,
        overwrite=overwrite,
    )

    from pystopt.visualization.optimization import visualize_stokes_evolution_history

    visualize_stokes_evolution_history(
        visualize_file_name, history, expected_deformation=None, overwrite=overwrite
    )


# }}}


# {{{ plot solution over contour


def plot_solution_contour(
    ctx_factory, filename: str, *, overwrite: bool = True
) -> None:
    actx = get_cl_array_context(ctx_factory)

    import pathlib

    filename = pathlib.Path(filename)
    contourfile = filename.parent / "contours.npz"

    from pystopt.paths import get_filename

    visualize_file_name = get_filename("history_contourf", cwd=filename.parent)

    # {{{ get history

    from pystopt.checkpoint import make_hdf_checkpoint

    checkpoint = make_hdf_checkpoint(filename, mode="r")

    from pystopt.checkpoint.hdf import array_context_for_pickling

    with array_context_for_pickling(actx):
        history = checkpoint.read("callback/callbacks/history/history")
        p = checkpoint.read("parameters")
    del checkpoint

    from extra_mesh_data import ExampleParameters

    p = ExampleParameters(**p)
    x = np.asarray(history["x"].tolist()).T
    logger.info("history size: %d", x.shape[1])

    # }}}

    # {{{ compute cost contour

    if contourfile.exists():
        data = np.load(contourfile)

        radii = data["radii"]
        aspect_ratios = data["aspect_ratios"]
        cost = data["cost"]
    else:
        _, state = make_wrangler(actx, p)

        radii = np.linspace(np.min(x[0]) - 0.1, np.max(x[0]) + 0.25, 64)
        aspect_ratios = np.linspace(np.min(x[1]) - 0.25, np.max(x[1]) + 0.25, 64)

        cost = np.empty((radii.size, aspect_ratios.size))

        from pystopt.measure import (
            TicTocTimer,
            estimate_wall_time_from_timestep,
            format_seconds,
        )
        from pystopt.tools import COLORS

        time = TicTocTimer()

        from itertools import product

        for i, j in product(range(radii.size), range(aspect_ratios.size)):
            time.tic()
            state = replace(
                state,
                x=np.array([
                    radii[i],
                    aspect_ratios[j],
                    x[2, -1],
                ]),
            )

            cost[i, j] = state.cost
            time.toc()

            t_elapsed, t_remaining = estimate_wall_time_from_timestep(
                time,
                n=i * radii.size + j,
                maxit=radii.size * aspect_ratios.size,
                dt=None,
                t=None,
                tmax=None,
            )

            logger.info(
                "cost[%4d, %4d] = %.5e %s(elapsed %s remaining %s)%s",
                i,
                j,
                cost[i, j],
                COLORS.Cyan,
                format_seconds(t_elapsed),
                format_seconds(t_remaining),
                COLORS.Normal,
            )

        np.savez(contourfile, radii=radii, aspect_ratios=aspect_ratios, cost=cost)

    # }}}

    # {{{ plot

    from pystopt.visualization.matplotlib import subplots

    with subplots(visualize_file_name.aspath(), overwrite=overwrite) as fig:
        ax = fig.gca()

        r, a = np.meshgrid(radii, aspect_ratios)
        ax.grid(zorder=2)
        im = ax.contourf(r, a, cost, levels=64, zorder=0)
        ax.contour(r, a, cost, colors="k", linewidths=(1,), levels=64, zorder=1)
        fig.colorbar(im, ax=ax)

        ax.plot(x[0], x[1], "wo-")
        ax.plot(x[0, 0], x[1, 0], "ro-")
        ax.plot(x[0, -1], x[1, -1], "go-")
        ax.set_xlabel(r"$\rho$")
        ax.set_ylabel("$a$")

    # }}}


# }}}


if __name__ == "__main__":
    import sys

    if len(sys.argv) > 1:
        exec(sys.argv[1])
    else:
        run(cl._csc)
