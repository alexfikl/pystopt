# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

from dataclasses import dataclass

import numpy as np

from pystopt import sym


def make_obj_array(x):
    if not isinstance(x, list | tuple | np.ndarray):
        x = [x]

    from pytools import obj_array

    return obj_array.make_obj_array(x)


@dataclass(frozen=True)
class SolutionInfo:
    r"""We assume a cost functional of the form

    .. math::

        J = \int_0^T f(t, \mathbf{c}) \,\mathrm{d}t

    which we compute by integrating an ODE, with :math:`j(0) = 0`,

    .. math::

        \frac{\mathrm{d} j}{\mathrm{d} t} = f(t, \mathbf{c}).

    Then, we can compute the gradient by integrating an ODE solved backwards
    in time, with :math:`\mathbf{g}(T) = 0`,

    .. math::

        \frac{\mathrm{d} \mathbf{g}}{\mathrm{d} t} =
            \frac{\partial f}{\partial \mathbf{c}}.

    The cost can be obtained by :math:`J = j(T)` and the gradient can be obtained
    from :math:`\nabla_{\mathbf{c}} J = \mathbf{g}(0)`.

    .. automethod:: get_sym_control
    """

    ambient_dim: int

    name: str = "ginf"
    name_d: str = "ginf_d"

    tmax_name: str = "tmax"
    offset_name: str = "offset"

    u_name: str = "u"
    w_name: str = "w"

    @property
    def dim(self):
        return self.ambient_dim - 1

    def get_sym_control(self, name: str):
        raise NotImplementedError

    # {{{ analytical

    def get_sym_cost(self) -> sym.Expression:
        """
        :returns: the solution :math:`j` of the cost ODE at eact time :math:`t`.
            This is an analytical solution to the ODE.
        """
        raise NotImplementedError

    def get_sym_gradient(self) -> np.ndarray:
        r"""
        :returns: the solution :math:`\mathbf{g}` of the gradient ODE at each
            time :math:`t`. This is an analytical solution to the ODE.
        """
        raise NotImplementedError

    def get_sym_cost_and_gradient(self) -> tuple[sym.Expression, np.ndarray]:
        """
        :returns: a :class:`tuple` containing the cost and the gradient values.
            This is analytically integrated.
        """
        raise NotImplementedError

    def get_sym_xstar(self) -> sym.Expression:
        """
        :returns: the solution for the normal adjoint transverse field equation.
        """
        raise NotImplementedError

    # }}}

    # {{{ numerical

    def get_sym_adjoint_gradient(
        self, xstar: np.ndarray, dofdesc: sym.DOFDescriptor | None = None
    ) -> np.ndarray:
        """
        :returns: an alternative expression for :meth:`get_sym_gradient`
            based on the adjoint transverse field. This expression is used to
            numerically approximate the gradient.
        """
        raise NotImplementedError

    def get_sym_shape_gradient(
        self, xstar: sym.Expression, dofdesc: sym.DOFDescriptor | None = None
    ) -> sym.Expression:
        """
        :returns: the source terms for the adjoint evolution equation.
        """
        u = sym.make_sym_vector(self.u_name, self.ambient_dim)
        w = sym.make_sym_vector(self.w_name, self.ambient_dim)

        return (u - w) @ sym.surface_gradient(self.ambient_dim, xstar, dofdesc=dofdesc)

    # }}}


# {{{ uniform


@dataclass(frozen=True)
class UniformSolutionInfo(SolutionInfo):
    def get_sym_control(self, name):
        return sym.make_sym_vector(name, self.ambient_dim)

    def get_sym_cost(self):
        u = self.get_sym_control(self.name)
        ud = self.get_sym_control(self.name_d)

        return sym.var("t") ** 2 / 2 * (u - ud) @ (u - ud)

    def get_sym_gradient(self):
        u = self.get_sym_control(self.name)
        ud = self.get_sym_control(self.name_d)
        T = sym.var(self.tmax_name)
        t = sym.var("t")

        return ((T - t) ** 2 * (2 * T + t) / 6) * (u - ud)

    def get_sym_cost_and_gradient(self):
        u = self.get_sym_control(self.name)
        ud = self.get_sym_control(self.name_d)
        T = sym.var(self.tmax_name)

        cost = T**3 / 6 * (u - ud) @ (u - ud)

        from pystopt.symbolic.mappers import differentiate

        return cost, make_obj_array([differentiate(cost, u[i]) for i in range(u.size)])

    def get_sym_xstar(self, dofdesc=None):
        u = self.get_sym_control(self.name)
        ud = self.get_sym_control(self.name_d)

        volume = np.pi
        T = sym.var(self.tmax_name)

        x = sym.nodes(self.ambient_dim, dofdesc=dofdesc).as_vector()
        gradj = ((u - ud) @ x) / volume

        return gradj * (T**2 - sym.var("t") ** 2) / 2

    def get_sym_adjoint_gradient(self, xstar, dofdesc=None):
        return make_obj_array([
            sym.sintegral(xstar[i], self.ambient_dim, self.dim, dofdesc=dofdesc)
            for i in range(xstar.size)
        ])


# }}}


# {{{ solid body rotation


@dataclass(frozen=True)
class SolidBodyRotationSolutionInfo(SolutionInfo):
    def get_sym_control(self, name):
        return sym.var(name)[0]

    def get_sym_cost(self):
        omega = self.get_sym_control(self.name)
        omega_d = self.get_sym_control(self.name_d)
        c = sym.var(self.offset_name)
        t = sym.var("t")

        return 2 * c**2 * sym.sin((omega - omega_d) * t / 2) ** 2

    def get_sym_gradient(self):
        omega = self.get_sym_control(self.name)
        omega_d = self.get_sym_control(self.name_d)
        c = sym.var(self.offset_name)
        T = sym.var(self.tmax_name)
        t = sym.var("t")

        result = (
            -(c**2)
            / (omega - omega_d) ** 2
            * (
                (T - t) * (omega - omega_d) * sym.cos((omega - omega_d) * T)
                + sym.sin((omega - omega_d) * t)
                - sym.sin((omega - omega_d) * T)
            )
        )

        return make_obj_array([result])

    def get_sym_cost_and_gradient(self):
        omega = self.get_sym_control(self.name)
        omega_d = self.get_sym_control(self.name_d)
        c = sym.var(self.offset_name)
        T = sym.var(self.tmax_name)

        cost = c**2 * (T - sym.sin((omega - omega_d) * T) / (omega - omega_d))

        from pystopt.symbolic.mappers import differentiate

        return cost, make_obj_array([
            differentiate(cost, omega),
        ])

    def get_sym_xstar(self, dofdesc=None):
        omega = self.get_sym_control(self.name)
        omega_d = self.get_sym_control(self.name_d)
        c = sym.var(self.offset_name)
        T = sym.var(self.tmax_name)
        t = sym.var("t")

        theta = sym.var("theta")
        return (
            c
            / (np.pi * (omega - omega_d))
            * (
                (T - t) * (omega - omega_d) * sym.cos(theta)
                + sym.sin(theta + (omega - omega_d) * t)
                - sym.sin(theta + (omega - omega_d) * T)
            )
        )

    def get_sym_adjoint_gradient(self, xstar, dofdesc=None):
        x = sym.nodes(self.ambient_dim, dofdesc=dofdesc).as_vector()
        return make_obj_array([
            sym.sintegral(
                -xstar[0] * x[1] + xstar[1] * x[0],
                self.ambient_dim,
                self.dim,
                dofdesc=dofdesc,
            )
        ])


# }}}


# {{{ extensional flow


@dataclass(frozen=True)
class ExtensionalSolutionInfo(SolutionInfo):
    def get_sym_control(self, name):
        return sym.var(name)[0]

    def get_sym_cost(self):
        alpha = self.get_sym_control(self.name)
        alpha_d = self.get_sym_control(self.name_d)
        c = sym.var(self.offset_name)
        t = sym.var("t")

        return c**2 / 2 * (sym.exp(alpha * t) - sym.exp(alpha_d * t)) ** 2

    def get_sym_gradient(self):
        pass

    def get_sym_cost_and_gradient(self):
        alpha = self.get_sym_control(self.name)
        alpha_d = self.get_sym_control(self.name_d)
        c = sym.var(self.offset_name)
        T = sym.var(self.tmax_name)

        cost = (
            c**2
            / (4 * alpha * alpha_d * (alpha + alpha_d))
            * (
                alpha**2 * (sym.exp(2 * T * alpha_d) - 1)
                + alpha_d**2 * (sym.exp(2 * T * alpha) - 1)
                + alpha
                * alpha_d
                * (
                    2
                    + sym.exp(2 * T * alpha)
                    + sym.exp(2 * T * alpha_d)
                    - 4 * sym.exp(T * (alpha + alpha_d))
                )
            )
        )

        from pystopt.symbolic.mappers import differentiate

        return cost, make_obj_array([
            differentiate(cost, alpha),
        ])

    def get_sym_xstar(self, dofdesc=None):
        pass

    def get_sym_adjoint_gradient(self, xstar, dofdesc=None):
        x = sym.nodes(self.ambient_dim, dofdesc=dofdesc).as_vector()
        return make_obj_array([
            sym.sintegral(
                xstar[0] * x[0] - xstar[1] * x[1],
                self.ambient_dim,
                self.dim,
                dofdesc=dofdesc,
            )
        ])


# }}}
