# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

import pathlib
from collections.abc import Callable, Iterator
from dataclasses import dataclass, field, replace
from typing import Any

import extra_mesh_data as emd
import numpy as np

from arraycontext import ArrayContext
from pytools import memoize_method

from pystopt import bind, cb, sym
from pystopt.checkpoint.hdf import dumper, loader
from pystopt.cost import CentroidTrackingFunctional
from pystopt.filtering import FilterType
from pystopt.simulation import unsteady
from pystopt.simulation.state import as_state_container
from pystopt.stokes import TwoPhaseStokesBoundaryCondition
from pystopt.tools import get_cl_array_context, get_default_logger

logger = get_default_logger(module=__file__)


# {{{ analytic solutions


def make_obj_array(x):
    if not isinstance(x, list | tuple | np.ndarray):
        x = [x]

    from pytools import obj_array

    return obj_array.make_obj_array(x)


@dataclass(frozen=True)
class SolutionInfo:
    ambient_dim: int

    name: str = "ginf"
    name_d: str = "ginf_d"

    tmax_name: str = "tmax"
    offset_name: str = "offset"

    u_name: str = "u"
    w_name: str = "w"

    @property
    def dim(self):
        return self.ambient_dim - 1

    def get_sym_control(self, name: str):
        raise NotImplementedError

    def get_sym_cost(self) -> sym.Expression:
        raise NotImplementedError

    def get_sym_gradient(self) -> np.ndarray:
        raise NotImplementedError

    def get_sym_cost_and_gradient(self) -> tuple[sym.Expression, np.ndarray]:
        raise NotImplementedError

    def get_sym_xstar(self) -> sym.Expression:
        raise NotImplementedError

    def get_sym_adjoint_gradient(
        self, xstar: np.ndarray, dofdesc: sym.DOFDescriptor | None = None
    ) -> np.ndarray:
        raise NotImplementedError

    def get_sym_shape_gradient(
        self, xstar: sym.Expression, dofdesc: sym.DOFDescriptor | None = None
    ) -> sym.Expression:
        u = sym.make_sym_vector(self.u_name, self.ambient_dim)
        w = sym.make_sym_vector(self.w_name, self.ambient_dim)

        return (u - w) @ sym.surface_gradient(self.ambient_dim, xstar, dofdesc=dofdesc)


# {{{ uniform


@dataclass(frozen=True)
class UniformSolutionInfo(SolutionInfo):
    def get_sym_control(self, name):
        return sym.make_sym_vector(name, self.ambient_dim)

    def get_sym_cost(self):
        u = self.get_sym_control(self.name)
        ud = self.get_sym_control(self.name_d)

        return sym.var("t") ** 2 / 2 * (u - ud) @ (u - ud)

    def get_sym_gradient(self):
        u = self.get_sym_control(self.name)
        ud = self.get_sym_control(self.name_d)
        T = sym.var(self.tmax_name)
        t = sym.var("t")

        return ((T - t) ** 2 * (2 * T + t) / 6) * (u - ud)

    def get_sym_cost_and_gradient(self):
        u = self.get_sym_control(self.name)
        ud = self.get_sym_control(self.name_d)
        T = sym.var(self.tmax_name)

        cost = T**3 / 6 * (u - ud) @ (u - ud)

        from pystopt.symbolic.mappers import differentiate

        return cost, make_obj_array([differentiate(cost, u[i]) for i in range(u.size)])

    def get_sym_xstar(self, dofdesc=None):
        u = self.get_sym_control(self.name)
        ud = self.get_sym_control(self.name_d)

        volume = 4 * np.pi / 3
        T = sym.var(self.tmax_name)

        x = sym.nodes(self.ambient_dim, dofdesc=dofdesc).as_vector()
        gradj = ((u - ud) @ x) / volume

        return gradj * (T**2 - sym.var("t") ** 2) / 2

    def get_sym_adjoint_gradient(self, xstar, dofdesc=None):
        return make_obj_array([
            sym.sintegral(xstar[i], self.ambient_dim, self.dim, dofdesc=dofdesc)
            for i in range(xstar.size)
        ])


# }}}


# {{{ solid body rotation


@dataclass(frozen=True)
class SolidBodyRotationSolutionInfo(SolutionInfo):
    def get_sym_control(self, name):
        return sym.var(name)[0]

    def get_sym_cost(self):
        omega = self.get_sym_control(self.name)
        omega_d = self.get_sym_control(self.name_d)
        c = sym.var(self.offset_name)
        t = sym.var("t")

        return 2 * c**2 * sym.sin((omega - omega_d) * t / 2) ** 2

    def get_sym_gradient(self):
        omega = self.get_sym_control(self.name)
        omega_d = self.get_sym_control(self.name_d)
        c = sym.var(self.offset_name)
        T = sym.var(self.tmax_name)
        t = sym.var("t")

        result = (
            -(c**2)
            / (omega - omega_d) ** 2
            * (
                (T - t) * (omega - omega_d) * sym.cos((omega - omega_d) * T)
                + sym.sin((omega - omega_d) * t)
                - sym.sin((omega - omega_d) * T)
            )
        )

        return make_obj_array([result])

    def get_sym_cost_and_gradient(self):
        omega = self.get_sym_control(self.name)
        omega_d = self.get_sym_control(self.name_d)
        c = sym.var(self.offset_name)
        T = sym.var(self.tmax_name)

        cost = c**2 * (T - sym.sin((omega - omega_d) * T) / (omega - omega_d))

        from pystopt.symbolic.mappers import differentiate

        return cost, make_obj_array([
            differentiate(cost, omega),
        ])

    def get_sym_xstar(self, dofdesc=None):
        omega = self.get_sym_control(self.name)
        omega_d = self.get_sym_control(self.name_d)
        c = sym.var(self.offset_name)
        T = sym.var(self.tmax_name)
        t = sym.var("t")

        theta = sym.var("theta")
        phi = sym.var("phi")
        return (
            3
            * c
            * sym.sin(theta)
            / (4 * np.pi * (omega - omega_d))
            * (
                (T - t) * (omega - omega_d) * sym.cos(phi)
                + sym.sin(phi + (omega - omega_d) * t)
                - sym.sin(phi + (omega - omega_d) * T)
            )
        )

    def get_sym_adjoint_gradient(self, xstar, dofdesc=None):
        x = sym.nodes(self.ambient_dim, dofdesc=dofdesc).as_vector()
        return make_obj_array([
            sym.sintegral(
                -xstar[0] * x[1] + xstar[1] * x[0],
                self.ambient_dim,
                self.dim,
                dofdesc=dofdesc,
            )
        ])


# }}}


# {{{ extensional flow


@dataclass(frozen=True)
class ExtensionalSolutionInfo(SolutionInfo):
    def get_sym_control(self, name):
        return sym.var(name)[0]

    def get_sym_cost_and_gradient(self):
        alpha = self.get_sym_control(self.name)
        alpha_d = self.get_sym_control(self.name_d)
        c = sym.var(self.offset_name)
        T = sym.var(self.tmax_name)

        cost = (
            c**2
            / (4 * alpha * alpha_d * (alpha + alpha_d))
            * (
                alpha**2 * (sym.exp(2 * T * alpha_d) - 1)
                + alpha_d**2 * (sym.exp(2 * T * alpha) - 1)
                + alpha
                * alpha_d
                * (
                    2
                    + sym.exp(2 * T * alpha)
                    + sym.exp(2 * T * alpha_d)
                    - 4 * sym.exp(T * (alpha + alpha_d))
                )
            )
        )

        from pystopt.symbolic.mappers import differentiate

        return cost, make_obj_array([
            differentiate(cost, alpha),
        ])

    def get_sym_adjoint_gradient(self, xstar, dofdesc=None):
        x = sym.nodes(self.ambient_dim, dofdesc=dofdesc).as_vector()
        return make_obj_array([
            sym.sintegral(
                xstar[0] * x[0] + xstar[1] * x[1] - 2 * xstar[2] * x[2],
                self.ambient_dim,
                self.dim,
                dofdesc=dofdesc,
            )
        ])


# }}}

# }}}


# {{{ callbacks


def get_output_fields_velocity(state, **kwargs: Any) -> dict[str, cb.OutputField]:
    places = state.get_geometry_collection()
    dofdesc = state.wrangler.dofdesc
    actx = state.array_context

    fields = {}

    fields["u"] = cb.OutputField("u", state.get_velocity_field())
    fields["w"] = cb.OutputField("w", state.get_tangential_forcing())

    u_dot_n = bind(places, sym.project_normal(places.ambient_dim), auto_where=dofdesc)(
        actx, x=fields["u"].value
    )

    fields["u_dot_n"] = cb.OutputField.visualize("u_n", u_dot_n)

    w_dot_t = bind(places, sym.project_tangent(places.ambient_dim), auto_where=dofdesc)(
        actx, x=fields["w"].value
    )

    fields["w_dot_t"] = cb.OutputField.visualize("w_t", w_dot_t)

    return fields


def get_output_fields_adjoint(adjoint, **kwargs: Any) -> dict[str, cb.OutputField]:
    state = kwargs["event"].state_component

    places = state.get_geometry_collection()
    dofdesc = state.wrangler.dofdesc

    fields = {}
    fields["xstar"] = cb.OutputField(
        "xstar", state.wrangler.from_state_type(adjoint.xstar)
    )

    sym_xstar_ex = state.wrangler.info.get_sym_xstar(dofdesc)
    if sym_xstar_ex is not None:
        xstar_ex = bind(places, sym_xstar_ex, auto_where=dofdesc)(
            adjoint.array_context, t=adjoint.t[0], **state.wrangler.context
        )

        fields["xstar_ex"] = cb.OutputField("xstar_ex", xstar_ex)

    return fields


@dataclass
class CallbackManager(cb.CallbackManager):
    def get_output_field_getters(self):
        from pystopt.simulation.state import get_output_fields_geometry

        return (
            get_output_fields_geometry,
            get_output_fields_velocity,
        )


@dataclass
class AdjointCallbackManager(cb.CallbackManager):
    def get_output_field_getters(self):
        return (get_output_fields_adjoint,)


@dataclass(frozen=True)
class AdjointVisualizeCallback(cb.VisualizeCallback):
    def __call__(self, *args: Any, **kwargs: Any) -> int:
        state = kwargs["event"].state_component
        return super().__call__(state, **kwargs)


@dataclass(frozen=True)
class HistoryCallback(unsteady.StokesEvolutionHistoryCallback):
    cost: list[float] = field(default_factory=list, repr=False)

    def __call__(self, *args, **kwargs):
        super().__call__(*args, **kwargs)
        (state,) = args

        places = state.get_geometry_collection()
        dofdesc = state.wrangler.dofdesc
        ambient_dim = places.ambient_dim

        actx = state.array_context
        assert actx is not None, "'state' is frozen"

        xd = bind(places, state.wrangler.cost.nodes(), auto_where=dofdesc)(
            actx, t=state.t[0], **state.wrangler.context
        )

        # {{{ optimization

        j = bind(places, state.wrangler.cost(ambient_dim), auto_where=dofdesc)(
            actx, t=state.t[0], **state.wrangler.context
        )
        j = actx.to_numpy(j)

        j_ex = bind(places, state.wrangler.info.get_sym_cost(), auto_where=dofdesc)(
            actx, t=state.t[0], **state.wrangler.context
        )
        j_ex = actx.to_numpy(j_ex)

        j_error = abs(j_ex - j)
        self.cost.append(j_error)

        # }}}

        from pystopt.tools import c

        logger.info(
            "volume %.15e centroid %s / %s",
            abs(self.volume[-1] - self.volume[0]) / self.volume[0],
            self.centroid[-1].squeeze(),
            xd,
        )
        logger.info(
            "cost %.12e exact %.12e error %s",
            j,
            j_ex,
            c.message(f"{j_error:.12e}", success=j_error < 1.0e-10),
        )

        return 1


@dataclass(frozen=True)
class AdjointHistoryCallback(cb.PerformanceHistoryCallback):
    time: list[float] = field(default_factory=list, repr=False)
    grad: list[float] = field(default_factory=list, repr=False)
    xstar: list[float] = field(default_factory=list, repr=False)

    def __call__(self, *args, **kwargs):
        super().__call__(*args, **kwargs)

        (adjoint,) = args
        state = kwargs["event"].state_component

        dofdesc = state.wrangler.dofdesc
        places = state.get_geometry_collection()

        actx = adjoint.array_context
        assert actx is not None, "'state' is frozen"

        self.time.append(state.t[0])

        # {{{ grad

        # NOTE: dj holds the gradient wrt control integrated until on [t, T]
        sym_dj_ex = state.wrangler.info.get_sym_gradient()
        if sym_dj_ex is not None:
            dj_ex = bind(places, sym_dj_ex, auto_where=dofdesc)(
                actx, t=state.t[0], **state.wrangler.context
            )
            dj_ex = actx.to_numpy(dj_ex)

            dj_error = np.linalg.norm(adjoint.dj - dj_ex)
            self.grad.append(actx.to_numpy(dj_error))

        # }}}

        # {{{ xstar

        sym_xstar_ex = state.wrangler.info.get_sym_xstar(dofdesc)
        if sym_xstar_ex is not None:
            xstar_ex = bind(places, sym_xstar_ex, auto_where=dofdesc)(
                actx, t=state.t[0], **state.wrangler.context
            )

            from pystopt.dof_array import dof_array_rnorm

            if len(self.xstar) == 0:
                # NOTE: skips the first one because the error is zero
                xstar_error = 0.0
            else:
                xstar_error = actx.to_numpy(
                    dof_array_rnorm(
                        state.wrangler.from_state_type(adjoint.xstar), xstar_ex
                    )
                )
            self.xstar.append(xstar_error)

        # }}}

        from pystopt.tools import afmt

        if sym_dj_ex is not None:
            logger.info(afmt("dj %ary exact %ary", adjoint.dj), *adjoint.dj, *dj_ex)
        else:
            logger.info(afmt("dj %ary", adjoint.dj), *adjoint.dj)

        if sym_dj_ex is not None:
            from pystopt.tools import c

            logger.info(
                "error dj %s xstar %s",
                c.message(f"{dj_error:.12e}", success=dj_error < 1.0e-10),
                c.message(f"{xstar_error:.12e}", success=xstar_error < 1.0e-10),
            )

        return 1


# }}}


# {{{ evolution


@dataclass(frozen=True)
class GeometryEvolutionWrangler(unsteady.GeometryEvolutionWrangler):
    # time stepping
    stepper_name: str
    stepper_tmax: float
    stepper_maxit: int
    stepper_timestep: float
    stepper_filter: Callable[[float, Any], Any]

    # source terms
    cost: CentroidTrackingFunctional
    bc: TwoPhaseStokesBoundaryCondition
    info: SolutionInfo

    callback: cb.CallbackManager

    def get_initial_state(self, actx):
        x0 = super().get_initial_state(actx)
        return GeometryEvolutionState(
            x=x0.x,
            j=np.array([0.0], dtype=object),
            t=np.array([0.0], dtype=object),
            wrangler=self,
        )

    def evolution_source_term(self, t: float, state: "GeometryEvolutionState"):
        if abs(state.t[0] - t) > 5.0e-15 * abs(t):
            logger.warning(
                "temporal disturbance detected: %.12e", abs(state.t[0] - t) / abs(t)
            )

        actx = state.array_context
        places = state.get_geometry_collection()
        ambient_dim = self.ambient_dim
        context = self.context.copy()

        info = state.wrangler.info

        # # {{{ normal velocity

        sym_u = sym.make_sym_vector(info.u_name, ambient_dim)
        if self.evolution.force_normal_velocity:
            sym_u = sym.project_normal(sym_u)
        context[info.u_name] = state.get_velocity_field()

        # }}}

        # {{{ tangential velocity

        if self.has_tangential_forcing:
            sym_w = sym.project_tangent(sym.make_sym_vector(info.w_name, ambient_dim))
            context[info.w_name] = state.get_tangential_forcing()
        else:
            sym_w = 0

        # }}}

        # {{{ evaluate

        v = bind(places, sym_u + sym_w, auto_where=self.dofdesc)(actx, t=t, **context)

        j = bind(places, self.cost(ambient_dim), auto_where=self.dofdesc)(
            actx, t=t, **self.context
        )
        j = actx.to_numpy(j)

        # }}}

        return replace(state, x=self.to_state_type(v), t=1.0, j=j)


@as_state_container
class GeometryEvolutionState(unsteady.GeometryEvolutionState):
    j: np.ndarray

    def get_velocity_field(self) -> np.ndarray:
        if self.velocity is None:
            from pystopt.stokes import sti

            places = self.get_geometry_collection()

            u = bind(places, sti.velocity(self.wrangler.bc) * sym.Ones())(
                self.array_context, t=self.t, **self.wrangler.context
            )

            u = self.wrangler.apply_filter(
                self.array_context,
                places,
                u,
                force_spectral=False,
            )

            object.__setattr__(self, "velocity", u)

        return self.velocity


@dataclass
class AdjointEvolutionWrangler:
    forward: GeometryEvolutionWrangler
    callback: Any

    @property
    def ambient_dim(self):
        return self.forward.ambient_dim

    @property
    def dofdesc(self):
        return self.forward.dofdesc

    @property
    def context(self):
        return self.forward.context

    def from_state_type(self, x):
        return self.forward.from_state_type(x)

    def get_initial_state(self, actx, state):
        name = self.forward.info.name
        return AdjointEvolutionState(
            xstar=actx.np.zeros_like(state.x[0]),
            t=state.t,
            dj=np.array([0.0] * self.context[name].size, dtype=object),
            wrangler=self,
        )

    def evolution_source_term(
        self,
        t: float,
        state: "GeometryEvolutionState",
        adjoint: "AdjointEvolutionState",
    ):
        assert state.discr is not None
        if abs(state.t[0] - t) > 7.5e-15 * abs(t):
            logger.warning(
                "temporal disturbance detected: %.12e", abs(state.t[0] - t) / abs(t)
            )

        actx = adjoint.array_context
        xstar = self.forward.from_state_type(adjoint.xstar)

        places = state.get_geometry_collection()
        info = state.wrangler.info

        # {{{ get velocity field

        context = {"t": t, "xstar": xstar, **self.context}
        context[info.u_name] = state.get_velocity_field()
        if self.forward.has_tangential_forcing:
            context[info.w_name] = state.get_tangential_forcing()

        # }}}

        # {{{ evaluate

        from pystopt import grad

        n = sym.normal(self.ambient_dim).as_vector()
        sym_xstar = sym.var("xstar")
        sym_xstar_vec = sym_xstar * n

        # {{{ shape gradient

        cost = state.wrangler.cost

        sym_vstar = sym.n_dot(
            grad.shape(cost, self.ambient_dim, dofdesc=self.dofdesc)
        ) + info.get_sym_shape_gradient(sym_xstar, dofdesc=self.dofdesc)
        vstar = bind(places, sym_vstar, auto_where=self.dofdesc)(actx, **context)

        # }}}

        # {{{ cost gradient

        sym_dj = (
            # NOTE: the cost functional does not depend on the velocity
            # grad.velocity(cost, self.ambient_dim, dofdesc=self.dofdesc)
            +info.get_sym_adjoint_gradient(sym_xstar_vec, dofdesc=self.dofdesc)
        )

        dj = bind(places, sym_dj, auto_where=self.dofdesc)(actx, **context)
        dj = actx.to_numpy(dj)

        # }}}

        # }}}

        return replace(
            adjoint,
            # xstar=self.forward.filter_field(actx, places, vstar),
            xstar=self.forward.to_state_type(vstar),
            t=-1.0,
            dj=dj,
        )


@as_state_container
class AdjointEvolutionState:
    xstar: np.ndarray
    t: np.ndarray
    dj: np.ndarray

    wrangler: AdjointEvolutionWrangler

    @property
    def array_context(self):
        return self.xstar.array_context


@dumper.register(GeometryEvolutionState)
def _dump_geometry_evolution_state(obj: GeometryEvolutionState, parent, name=None):
    group = parent.create_type(name, obj)

    from pystopt.checkpoint.hdf import _dump_state_fields

    group.attrs["t"] = obj.t[0]
    group.attrs["j"] = obj.j[0]
    dumper(obj.x, group, name="x")
    _dump_state_fields(obj, group, ("discr", "velocity", "forcing"))


@loader.register(GeometryEvolutionState)
def _load_geometry_evolution_state(parent):
    from pystopt.checkpoint.hdf import _load_state_fields

    return _load_state_fields(
        parent,
        GeometryEvolutionWrangler,
        fields=("discr", "velocity", "forcing"),
        t=np.array([parent.attrs["t"]], dtype=object),
        j=np.array([parent.attrs["j"]], dtype=object),
    )


# }}}


# {{{ wrangler


@dataclass(frozen=True)
class QuasiFarfieldWrangler:
    evolution: GeometryEvolutionWrangler
    context: dict[str, Any]

    checkpoint_directory_series: Iterator[pathlib.Path]
    visualize: bool
    overwrite: bool

    @property
    def ambient_dim(self):
        return self.evolution.ambient_dim

    def get_initial_state(self, actx: ArrayContext) -> "QuasiFarfieldState":
        name = self.evolution.info.name
        return QuasiFarfieldState(
            x=make_obj_array(self.context[name]), wrangler=self, array_context=actx
        )

    def make_forward_callback(self, checkpoint_directory_name: pathlib.Path):
        visualize_callback_factory = self.visualize
        if visualize_callback_factory:
            visualize_callback_factory = cb.VisualizeCallback

        from pystopt.dof_array import dof_array_norm

        return cb.make_default_evolution_callback(
            checkpoint_directory_name / "checkpoint_fwd.h5",
            manager_factory=CallbackManager,
            history_callback_factory=HistoryCallback,
            checkpoint_callback_factory=None,
            visualize_callback_factory=visualize_callback_factory,
            norm=dof_array_norm,
            log=logger.info,
            prefix="fwd",
            overwrite=self.overwrite,
        )

    def make_adjoint_callback(self, checkpoint_directory_name: pathlib.Path):
        visualize_callback_factory = self.visualize
        if visualize_callback_factory:
            visualize_callback_factory = AdjointVisualizeCallback

        from pystopt.dof_array import dof_array_norm

        return cb.make_default_evolution_callback(
            checkpoint_directory_name / "checkpoint_adj.h5",
            manager_factory=AdjointCallbackManager,
            history_callback_factory=AdjointHistoryCallback,
            checkpoint_callback_factory=None,
            visualize_callback_factory=visualize_callback_factory,
            norm=dof_array_norm,
            log=logger.info,
            prefix="adj",
            overwrite=self.overwrite,
        )

    def make_time_stepper(self, state: "QuasiFarfieldState"):
        wrangler = state.forward_evolution_wrangler
        state0 = wrangler.get_initial_state(state.array_context)

        from pystopt.tools import dc_stringify

        logger.info("\n%s", dc_stringify(wrangler.context))

        from pystopt.checkpoint import make_hdf_checkpoint_manager

        checkpoint = wrangler.callback["checkpoint"].checkpoint
        checkpoint = make_hdf_checkpoint_manager(checkpoint.filename.parent / "ts.h5")

        from pystopt.evolution.stepping import make_adjoint_time_stepper

        stepper = make_adjoint_time_stepper(
            wrangler.stepper_name,
            "state",
            state0,
            wrangler.evolution_source_term,
            dt_start=wrangler.stepper_timestep,
            t_start=0.0,
            checkpoint=checkpoint,
            state_filter=wrangler.stepper_filter,
        )

        return stepper

    def evaluate_cost(self, state: "QuasiFarfieldState") -> float:
        wrangler = state.forward_evolution_wrangler
        stepper = state.time_stepper

        result = unsteady.quasi_evolve_stokes(
            state.array_context,
            wrangler,
            stepper,
            tmax=wrangler.stepper_tmax,
            maxit=wrangler.stepper_maxit,
            callback=wrangler.callback,
            verbose=True,
        )
        assert abs(result.t[0] - self.context["tmax"]) / self.context["tmax"] < 1.0e-8

        return result

    def evaluate_gradient(self, state: "QuasiFarfieldState") -> np.ndarray:
        wrangler = state.adjoint_evolution_wrangler
        state0 = wrangler.get_initial_state(
            state.array_context, state._forward_solution
        )
        assert abs(state0.t[0] - self.context["tmax"]) / self.context["tmax"] < 1.0e-8

        stepper = state.time_stepper.make_adjoint(
            "adjoint", state0, wrangler.evolution_source_term
        )

        from pystopt.checkpoint.hdf import wrangler_for_pickling

        with wrangler_for_pickling(wrangler.forward):
            result = unsteady.quasi_evolve_adjoint(
                state.array_context, wrangler, stepper, callback=wrangler.callback
            )

        return result


@as_state_container
class QuasiFarfieldState:
    x: np.ndarray
    wrangler: QuasiFarfieldWrangler
    array_context: ArrayContext

    # {{{ wranglers

    @property
    @memoize_method
    def checkpoint_directory_name(self):
        return next(self.wrangler.checkpoint_directory_series)

    @property
    @memoize_method
    def forward_evolution_wrangler(self):
        name = self.wrangler.evolution.info.name
        return replace(
            self.wrangler.evolution,
            callback=self.wrangler.make_forward_callback(
                self.checkpoint_directory_name
            ),
            context={**self.wrangler.context, name: self.x},
        )

    @property
    @memoize_method
    def adjoint_evolution_wrangler(self):
        return AdjointEvolutionWrangler(
            forward=self.forward_evolution_wrangler,
            callback=self.wrangler.make_adjoint_callback(
                self.checkpoint_directory_name
            ),
        )

    @property
    @memoize_method
    def time_stepper(self):
        return self.wrangler.make_time_stepper(self)

    # }}}

    # {{{ optim

    @property
    @memoize_method
    def _forward_solution(self):
        return self.wrangler.evaluate_cost(self)

    @property
    @memoize_method
    def cost(self) -> float:
        return self._forward_solution.j[0]

    @property
    @memoize_method
    def _adjoint_solution(self):
        return self.wrangler.evaluate_gradient(self)

    @property
    @memoize_method
    def gradient(self) -> np.ndarray:
        return self._adjoint_solution.dj

    @property
    def cost_gradient(self) -> tuple[float, np.ndarray]:
        return self.cost, self.gradient

    # }}}


# }}}


# {{{ parameters


def _make_perturbed_forcing(
    actx, places, velocity, *, dofdesc, forcing="nodes", eps=1.0e-2
):
    from pytools import memoize_in

    ambient_dim = places.ambient_dim

    @memoize_in(actx, _make_perturbed_forcing)
    def sym_forcing():
        if forcing == "normal":
            sym_w = sym.normal(ambient_dim).as_vector()
        elif forcing == "nodes":
            sym_w = sym.nodes(ambient_dim).as_vector()
        elif forcing == "principal":
            sym_w = sum(sym.extrinsic_principal_directions(ambient_dim))
            print(sym.pretty(sym_w))
        elif forcing == "tangent":
            sym_w = sum(sym.tangential_onb(ambient_dim).T)
        elif forcing == "curvature":
            sym_w = (
                sym.summed_curvature(ambient_dim) * sym.normal(ambient_dim).as_vector()
            )
        else:
            raise ValueError()

        from pystopt.symbolic.execution import prepare_expr

        return prepare_expr(places, sym_w, dofdesc)

    w_tilde = bind(places, sym_forcing(), auto_where=dofdesc, _skip_prepare=True)(actx)
    assert w_tilde.shape == (ambient_dim,)

    return velocity + eps * w_tilde


@dataclass(frozen=True)
class ExampleParameters(emd.ExampleParameters):
    ambient_dim: int = 3
    control_name: str = "<undefined>"
    info_cls: type = SolutionInfo

    # mesh
    resolution: int = 32
    target_order: int = 3
    mesh_name: str = "spharm_sphere"
    mesh_arguments: dict[str, Any] = field(
        default_factory=lambda: {
            "offset": np.array([0.0, 0.0, 0.0]),
        }
    )

    # time
    time_step_method: str = "assprk22"
    tmax: float = np.pi / 4
    timestep: float = 1.0e-3

    # forcing
    # stabilizer_name: str = "none"
    # stabilizer_name: str = "custom"
    # stabilizer_name: str = "default"
    stabilizer_name: str = "zinchenko2002"
    # stabilizer_name: str = "loewenberg2001"
    tangential_forcing_arguments: dict[str, Any] = field(
        default_factory=lambda: {
            "optim_options": {"maxiter": 256},
            # "func": _make_perturbed_forcing,
        }
    )

    # filter
    filter_type: str = FilterType.Unfiltered
    filter_default_arguments: dict[str, Any] = field(
        default_factory=lambda: {
            # "method": "tikhonov_ideal", "alpha": 1.e-6, "p": 2, "kmax": None,
            "method": "ideal",
            "p": np.inf,
            "kmax": None,
        }
    )

    @property
    def control_name_d(self):
        return f"{self.control_name}_d"

    @property
    def component_names(self):
        return [f"\\{self.control_name}"]

    def get_info(self):
        return self.info_cls(
            ambient_dim=self.ambient_dim,
            name="ginf",
            name_d="ginf_d",
            tmax_name="tmax",
            offset_name="offset",
        )


@dataclass(frozen=True)
class UniformParameters(ExampleParameters):
    control_name: str = "uinf"
    info_cls: type = UniformSolutionInfo

    farfield_name: str = "uniform"
    farfield_arguments: dict[str, Any] = field(
        default_factory=lambda: {"uinf": np.array([1.0, 1.5, 0.75])}
    )

    cost_name: str = "uniform_centroid_tracking"
    cost_arguments: dict[str, Any] = field(
        default_factory=lambda: {
            "uinf_d": np.array([2.0, 1.0, 0.5]),
        }
    )

    @property
    def component_names(self):
        return [rf"{name}_\infty" for name in "uvw"]


@dataclass(frozen=True)
class SolidBodyRotationParameters(UniformParameters):
    control_name: str = "omega"
    info_cls: type = SolidBodyRotationSolutionInfo

    mesh_arguments: dict[str, Any] = field(
        default_factory=lambda: {
            "offset": np.array([2.0, 0.0, 0.0]),
        }
    )

    farfield_name: str = "solid_body_rotation"
    farfield_arguments: dict[str, Any] = field(
        default_factory=lambda: {
            "omega": np.array([1.0]),
        }
    )

    cost_name: str = "sbr_centroid_tracking"
    cost_arguments: dict[str, Any] = field(
        default_factory=lambda: {
            "omega_d": np.array([1.5]),
            "offset": 2.0,
        }
    )


@dataclass(frozen=True)
class ExtensionalParameters(UniformParameters):
    control_name: str = "alpha"
    info_cls: type = ExtensionalSolutionInfo

    mesh_name: str = "spharm_sphere"
    mesh_arguments: dict[str, Any] = field(
        default_factory=lambda: {
            "offset": np.array([1.0, 0.0, 0.0]),
        }
    )

    farfield_name: str = "extensional"
    farfield_arguments: dict[str, Any] = field(
        default_factory=lambda: {
            "alpha": np.array([0.25]),
        }
    )

    cost_name: str = "extensional_centroid_tracking"
    cost_arguments: dict[str, Any] = field(
        default_factory=lambda: {
            "alpha_d": np.array([0.5]),
            "offset": 1.0,
        }
    )


# }}}


# {{{ setup


def get_cls_from_name(name: str) -> type:
    return {
        "uniform": UniformParameters,
        "sbr": SolidBodyRotationParameters,
        "extensional": ExtensionalParameters,
    }[name]


def get_wrangler(
    actx, p: ExampleParameters, *, visualize: bool = False
) -> QuasiFarfieldWrangler:
    ambient_dim = p.ambient_dim
    case = p.get_info()

    # {{{ geometry

    import extra_optim_data as eod

    places = eod.get_geometry_collection_from_param(actx, p, qbx=False)
    dofdesc = places.auto_source

    discr = places.get_discretization(dofdesc.geometry)
    logger.info("nspec:     %d", discr.nspec)
    logger.info("ndofs:     %d", discr.ndofs)
    logger.info("nelements: %d", discr.mesh.nelements)

    # }}}

    # {{{ wrangler

    # {{{ cost

    import pystopt.cost as pc

    cost = pc.cost_functional_from_name(
        p.cost_name,
        **{
            "xd": sym.make_sym_vector("xd", ambient_dim),
            p.control_name_d: case.get_sym_control(case.name_d),
            "t": sym.var("t"),
        },
    )

    # }}}

    # {{{ velocity

    import pystopt.stokes as stk

    farfield_arguments = {}
    farfield_arguments[p.control_name] = case.get_sym_control(case.name)

    bc = stk.get_farfield_boundary_from_name(
        p.ambient_dim, p.farfield_name, **farfield_arguments
    )

    from pystopt.evolution.stokes import StokesEvolutionOperator

    evolution = StokesEvolutionOperator(
        force_normal_velocity=True,
        force_tangential_velocity=p.stabilizer_name != "none",
    )

    # }}}

    # {{{ evolution

    from pystopt.mesh.processing import compute_group_volumes

    vd = compute_group_volumes(actx, places, dofdesc=dofdesc)

    def state_filter(state):
        from pystopt.simulation import rescale_state_to_volume

        return rescale_state_to_volume(state, vd=vd)

    from pystopt.evolution.estimates import fixed_time_step_from

    maxit, tmax, dt = fixed_time_step_from(
        maxit=p.maxit, tmax=p.tmax, timestep=p.timestep
    )
    evolution = GeometryEvolutionWrangler(
        # geometry
        places=places,
        dofdesc=dofdesc,
        is_spectral=True,
        # source terms
        cost=cost,
        bc=bc,
        info=case,
        # evolution
        evolution=evolution,
        stepper_name=p.time_step_method,
        stepper_tmax=tmax,
        stepper_maxit=maxit,
        # stepper_tmax=None,
        # stepper_maxit=5,
        stepper_timestep=dt,
        stepper_filter=state_filter,
        # filtering
        filter_type=p.filter_type,
        filter_arguments=p.filter_arguments,
        # stabilizer
        stabilizer_name=p.stabilizer_name,
        stabilizer_arguments=p.tangential_forcing_arguments,
        callback=None,
        context={},
    )

    # }}}

    context = {case.tmax_name: tmax}
    context.update(p.stokes_arguments)

    context[case.name] = p.farfield_arguments.pop(p.control_name)
    context.update(p.farfield_arguments)

    context["xd"] = actx.to_numpy(
        bind(
            places,
            sym.surface_centroid(ambient_dim),
            auto_where=dofdesc,
        )(actx)
    )
    context[case.name_d] = p.cost_arguments.pop(p.control_name_d)
    context.update(p.cost_arguments)

    x = actx.thaw(discr.nodes()) - p.mesh_arguments["offset"]
    context["theta"] = actx.np.arccos(x[2] / actx.np.sqrt(x @ x))
    context["phi"] = actx.np.arctan2(x[1], x[0])

    from pystopt.paths import generate_dirname_series

    dirname = p.checkpoint_file_name.parent
    wrangler = QuasiFarfieldWrangler(
        evolution=evolution,
        context=context,
        checkpoint_directory_series=generate_dirname_series(
            "evolution", cwd=dirname, create=True
        ),
        visualize=visualize,
        overwrite=True,
    )

    # }}}

    return wrangler


# }}}


# {{{ run


def run_finite_difference(
    ctx_factory_or_actx,
    *,
    name: str = "uniform",
    suffix: str = "v0",
    visualize: bool = False,
) -> None:
    actx = get_cl_array_context(ctx_factory_or_actx)

    p = emd.make_param_from_class(
        get_cls_from_name(name), suffix=suffix, name=f"finite_{name}"
    )
    dirname = p.checkpoint_file_name.parent
    logger.info("\n%s", p)

    wrangler = get_wrangler(actx, p, visualize=visualize)
    case = wrangler.evolution.info
    places = wrangler.evolution.places

    # {{{ finite difference

    state0 = wrangler.get_initial_state(actx)

    from pystopt.checkpoint.hdf import array_context_for_pickling

    with array_context_for_pickling(actx):
        sym_cost, sym_grad = case.get_sym_cost_and_gradient()

        cost = bind(places, sym_cost)(actx, **wrangler.context)
        grad = bind(places, sym_grad)(actx, **wrangler.context)

        from pystopt.measure import EOCRecorder, stringify_eoc

        logger.info("cost: %.12e / %.12e", state0.cost, cost)

        if False:
            return

        from pystopt.tools import afmt

        logger.info(afmt("grad %ary / %ary", grad), *state0.gradient, *grad)
        eoc = [EOCRecorder(name=f"{name}^{{AD}}") for name in p.component_names] + [
            EOCRecorder(name=f"{name}") for name in p.component_names
        ]

        if visualize:
            visualize_forward_history(
                dirname / f"optim_quasi_{name}_history_forward",
                state0.forward_evolution_wrangler.callback["history"],
            )
            visualize_adjoint_history(
                dirname / f"optim_quasi_{name}_history_adjoint",
                state0.adjoint_evolution_wrangler.callback["history"],
            )

        if True:
            return

        from pytools import wandering_element

        dc_duinf_fd = np.empty(state0.x.size)
        dc_duinf_ad = state0.gradient

        eps = 10.0 ** -np.arange(2, 6)
        for k in range(eps.size):
            for i, e_i in enumerate(wandering_element(state0.x.size)):
                state = replace(state0, x=state0.x + eps[k] * np.array(e_i))
                logger.info("%.2e %3d cost %s %.12e", eps[k], i, state.x, state.cost)

                # estimate gradient
                dc_duinf_fd[i] = (state.cost - state0.cost) / eps[k]

                # error: adjoint vs finite difference
                error = abs(dc_duinf_ad[i] - dc_duinf_fd[i]) / abs(dc_duinf_fd[i])
                eoc[i].add_data_point(eps[k], error)

                # error: exact vs finite difference
                error = abs(dc_duinf_fd[i] - grad[i]) / abs(grad[i])
                eoc[i + state0.x.size].add_data_point(eps[k], error)

            logger.info(
                afmt("grad: ad %ary / fd %ary / exact %ary", grad),
                *dc_duinf_ad,
                *dc_duinf_fd,
                *grad,
            )

        logger.info(
            "\n%s",
            stringify_eoc(*eoc, abscissa_name=r"\epsilon", table_format="latex"),
        )

        from pystopt.measure import visualize_eoc

        visualize_eoc(
            dirname / f"optim_quasi_finite_{name}", *eoc, order=1, overwrite=True
        )

    # }}}


def run_convergence(
    ctx_factory_or_actx,
    *,
    name: str = "uniform",
    suffix: str = "v0",
    visualize: bool = False,
) -> None:
    actx = get_cl_array_context(ctx_factory_or_actx)

    timesteps = np.array([5.0e-2, 2.5e-2, 1.0e-2, 7.5e-3, 5.0e-3])
    timesteps = np.array([2.5e-2, 1.0e-2, 7.5e-3, 5.0e-3])
    # timesteps = np.array([2.5e-2, 1.0e-2])
    # timesteps = np.array([5.0e-3])
    error_c = np.empty_like(timesteps)
    error_g = np.empty_like(timesteps)

    from pystopt.measure import EOCRecorder, stringify_eoc

    eoc_c = EOCRecorder(name="J")
    eoc_g = EOCRecorder(name="g")

    from pystopt.checkpoint.hdf import array_context_for_pickling

    with array_context_for_pickling(actx):
        for i, dt in enumerate(timesteps):
            p = emd.make_param_from_class(
                get_cls_from_name(name),
                timestep=dt,
                suffix=suffix,
                name=f"convergence_{name}_{i:02d}",
            )
            if i == 0:
                logger.info("\n%s", p)

            wrangler = get_wrangler(actx, p, visualize=visualize)
            case = wrangler.evolution.info
            places = wrangler.evolution.places

            sym_cost, sym_grad = case.get_sym_cost_and_gradient()

            cost_ex = bind(places, sym_cost)(actx, **wrangler.context)
            grad_ex = bind(places, sym_grad)(actx, **wrangler.context)

            state = wrangler.get_initial_state(actx)
            cost_ad = state.cost
            grad_ad = state.gradient
            # grad_ad = grad_ex

            import numpy.linalg as la

            error_c[i] = abs(cost_ex - cost_ad) / abs(cost_ex)
            error_g[i] = la.norm(grad_ex - grad_ad) / la.norm(grad_ex)

            eoc_c.add_data_point(dt, error_c[i])
            eoc_g.add_data_point(dt, error_g[i])

            from pystopt.tools import afmt

            logger.info("cost %.12e / %.12e", cost_ad, cost_ex)
            logger.info(afmt("grad %ary / %ary", grad_ad), *grad_ad, *grad_ex)

        logger.info(
            "\n%s",
            stringify_eoc(
                eoc_c, eoc_g, abscissa_name=r"\Delta t", table_format="latex"
            ),
        )

    from pystopt.measure import visualize_eoc

    dirname = p.checkpoint_file_name.parent
    visualize_eoc(
        dirname / f"optim_quasi_{name}_convergence",
        eoc_c,
        eoc_g,
        order=1,
        overwrite=True,
    )


# }}}


# {{{ visualize


def visualize_forward_history(basename, history, *, overwrite=True):
    import matplotlib.pyplot as mp

    fig = mp.figure()

    t = np.array(history.time)

    from pystopt.visualization.matplotlib import subplots

    with subplots(fig, filename=f"{basename}_cost", overwrite=overwrite):
        ax = fig.gca()

        ax.semilogy(t, np.array(history.cost))
        ax.set_xlabel("$t$")
        ax.set_ylabel("$Error$")


def visualize_adjoint_history(basename, history, *, overwrite=True):
    import matplotlib.pyplot as mp

    fig = mp.figure()

    t = np.array(history.time)

    from pystopt.visualization.matplotlib import subplots

    with subplots(fig, filename=f"{basename}_grad", overwrite=overwrite):
        ax = fig.gca()

        ax.semilogy(t, np.array(history.grad))
        ax.set_xlabel("$t$")
        ax.set_ylabel("$Error$")

    with subplots(fig, filename=f"{basename}_xstar", overwrite=overwrite):
        ax = fig.gca()

        ax.semilogy(t, np.array(history.xstar))
        ax.set_xlabel("$t$")
        ax.set_ylabel("$X^*$")


# }}}


if __name__ == "__main__":
    import sys

    import pyopencl as cl

    if len(sys.argv) > 1:
        exec(sys.argv[1])
    else:
        run_finite_difference(cl._csc)
