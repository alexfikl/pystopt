# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

from dataclasses import dataclass

import numpy as np

import pystopt.callbacks as cb
from pystopt import bind, sym
from pystopt.simulation.unsteady import (
    StokesEvolutionCallbackManager as CallbackManager,
)
from pystopt.simulation.unsteady import (
    StokesEvolutionHistoryCallback,
    StokesEvolutionVisualizeCallback,
    StokesEvolutionWrangler,
)
from pystopt.tools import get_cl_array_context, get_default_logger

logger = get_default_logger(module=__file__)


# {{{ callbacks


@dataclass(frozen=True)
class HistoryCallback(StokesEvolutionHistoryCallback):
    rtol: float = np.inf

    def __call__(self, *args, **kwargs):
        ret = super().__call__(*args, **kwargs)

        logger.info(
            "volume %.5e / %.5e area %.5e / %5e u_dot_n %.5e / %.5e",
            self.volume[-1],
            abs(self.volume[-1] - self.volume[0]) / self.volume[0],
            self.area[-1],
            abs(self.area[-1] - self.area[0]) / self.area[0],
            self.u_dot_n[-1],
            self.rtol,
        )

        if self.u_dot_n[-1] < self.rtol:
            from pystopt.tools import c

            logger.info(c.wrap("Relative tolerance reached. Stopping...", c.LightGreen))
            ret = 0

        return ret


@dataclass(frozen=True)
class VisualizeCallback(StokesEvolutionVisualizeCallback):
    def get_geometry_kwargs(self, state):
        return {"xlim": [-2.25, 2.25], "ylim": [-2.25, 2.25]}


# }}}


# {{{ run


def run(
    ctx_factory,
    ambient_dim: int = 2,
    *,
    suffix: str = "v0",
    visualize: bool = True,
    overwrite: bool = True,
) -> None:
    actx = get_cl_array_context(ctx_factory)

    # {{{ parameters

    if ambient_dim == 2:
        from extra_mesh_data import StokesSteadyEvolution2d_2021_10_04 as Parameters
    elif ambient_dim == 3:
        from extra_mesh_data import StokesSteadyEvolution3d_2021_10_04 as Parameters
    else:
        raise ValueError(f"unsupported dimension: {ambient_dim}")

    import extra_mesh_data as emd

    p = emd.make_param_from_class(
        Parameters, name="evo-steady", suffix=suffix, overwrite=overwrite
    )
    assert p.ambient_dim == ambient_dim

    logger.info("\n%s", p)

    # }}}

    # {{{ geometry

    import extra_optim_data as eod

    places = eod.get_geometry_collection_from_param(actx, p, qbx=True)
    dofdesc = places.auto_source

    discr = places.get_discretization(dofdesc.geometry, dofdesc.discr_stage)
    logger.info("nspec:     %d", discr.nspec)
    logger.info("ndofs:     %d", discr.ndofs)
    logger.info("nelements: %d", discr.mesh.nelements)

    # }}}

    # {{{ stokes

    from pystopt import stokes

    bc = stokes.get_farfield_boundary_from_name(
        p.ambient_dim, p.farfield_name, **p.farfield_arguments
    )
    op = stokes.TwoPhaseSingleLayerStokesRepresentation(bc)

    from pystopt.evolution.stokes import StokesEvolutionOperator

    evolution = StokesEvolutionOperator(
        force_normal_velocity=p.force_normal_velocity,
        force_tangential_velocity=p.force_tangential_velocity,
    )

    uinf_dot_n = bind(
        places,
        sym.norm(ambient_dim, sym.n_dot(stokes.velocity(bc)), p=np.inf),
        auto_where=dofdesc,
    )(actx)
    rtol = 1.0e-4 * actx.to_numpy(uinf_dot_n)

    # }}}

    # {{{ output

    if visualize:
        visualize_callback_factory = VisualizeCallback
    else:
        visualize_callback_factory = False

    from pystopt.dof_array import dof_array_norm

    output = cb.make_default_evolution_callback(
        p.checkpoint_file_name,
        manager_factory=CallbackManager,
        history_callback_factory=HistoryCallback(norm=dof_array_norm, rtol=rtol),
        visualize_callback_factory=visualize_callback_factory,
        norm=dof_array_norm,
        overwrite=overwrite,
    )

    wrangler = StokesEvolutionWrangler(
        # geometry
        places=places,
        dofdesc=dofdesc,
        is_spectral=True,
        # stokes
        op=op,
        lambdas=p.stokes_lambdas,
        context=p.stokes_arguments,
        gmres_arguments={"rtol": 1.0e-8},
        # evolution
        evolution=evolution,
        stabilizer_name=p.stabilizer_name,
        stabilizer_arguments=p.stabilizer_arguments,
        # filtering
        filter_type=p.filter_type,
        filter_arguments=p.filter_arguments,
    )

    # }}}

    # {{{ time stepping

    from pystopt.evolution import make_adjoint_time_stepper

    state0 = wrangler.get_initial_state(actx)
    stepper = make_adjoint_time_stepper(
        p.time_step_method,
        "state",
        state0,
        wrangler.evolution_source_term,
        dt_start=p.timestep,
        t_start=0.0,
        dt_max=7.5 * p.timestep,
        estimate_time_step=wrangler.estimate_time_step,
    )

    from pystopt.checkpoint.hdf import array_context_for_pickling

    history = output["history"]

    with array_context_for_pickling(actx):
        for event in stepper.run(tmax=None, maxit=5):
            ret = output(event.state_component, event=event)
            if ret == 0:
                break

        checkpoint = output["checkpoint"].checkpoint
        history = output["history"].to_numpy()

        from pystopt.tools import dc_asdict

        checkpoint.write_to("parameters", dc_asdict(p), overwrite=True)
        checkpoint.write_to("orig_discr", discr, overwrite=True)
        checkpoint.write_to("history", history, overwrite=True)

        checkpoint.done()

    if visualize:
        plot_evolution_history(actx, p.checkpoint_file_name)
        plot_shape_gradient(actx, p.checkpoint_file_name)

    # }}}


# }}}


# {{{ plot


def plot_evolution_history(ctx_factory, filename):
    actx = get_cl_array_context(ctx_factory)

    import pathlib

    filename = pathlib.Path(filename)

    from pystopt.paths import get_filename

    dirname = filename.parent
    visualize_file_name = get_filename("history", cwd=dirname)

    # {{{ load

    from pystopt.checkpoint import make_hdf_checkpoint

    checkpoint = make_hdf_checkpoint(filename, mode="r")

    from pystopt.checkpoint.hdf import array_context_for_pickling

    with array_context_for_pickling(actx):
        history = checkpoint.read("history")

    # }}}

    # {{{ plot

    from pystopt.visualization.optimization import visualize_stokes_evolution_history

    visualize_stokes_evolution_history(visualize_file_name, history)

    # }}}


def plot_shape_gradient(ctx_factory, filename):
    actx = get_cl_array_context(ctx_factory)

    import pathlib

    filename = pathlib.Path(filename)

    from pystopt.paths import get_filename

    dirname = filename.parent
    visualize_file_name = get_filename("history", cwd=dirname)

    # {{{ load

    from pystopt.checkpoint import OnDiskIteratorCheckpointManager, make_hdf_checkpoint

    rw = make_hdf_checkpoint(filename, mode="r")
    checkpoint = OnDiskIteratorCheckpointManager(rw=rw)

    from pystopt.checkpoint.hdf import array_context_for_pickling

    with array_context_for_pickling(actx):
        param = checkpoint.read_from("parameters")

        checkpoint.advance()
        data = checkpoint.read()
        discr = data["discr"]

    # }}}

    # {{{ get geometry

    from extra_mesh_data import ExampleParameters

    param["cost_name"] = "steady"
    param["cost_arguments"].update({"gamma": 0, "vd": np.pi})
    p = ExampleParameters(**param)

    from extra_optim_data import get_geometry_collection_from_param

    places = get_geometry_collection_from_param(actx, p, qbx=True)
    dofdesc = places.auto_source

    qbx = places.get_geometry(dofdesc.geometry).copy(density_discr=discr)
    places = places.merge({dofdesc.geometry: qbx})

    # }}}

    # {{{ get optimization

    from pystopt import stokes

    bc = stokes.get_extensional_farfield(p.ambient_dim, **p.farfield_arguments)

    from pystopt.stokes import TwoPhaseSingleLayerStokesRepresentation

    op = TwoPhaseSingleLayerStokesRepresentation(bc)

    import extra_optim_data as eod

    cost, context = eod.make_stokes_cost_functional(
        actx, places, op, p, source_dd=dofdesc, desire_dd=dofdesc
    )

    from pystopt.simulation.staticstokes import make_stokes_shape_optimization_wrangler

    wrangler = make_stokes_shape_optimization_wrangler(
        places,
        cost,
        op,
        viscosity_ratio=p.viscosity_ratio,
        capillary_number=p.capillary_number,
        dofdesc=dofdesc,
        context=context,
        filter_type=p.filter_type,
        filter_arguments=p.filter_arguments,
    )
    state = wrangler.get_initial_state(actx)

    grad = wrangler.from_state_type(
        wrangler.apply_filter(actx, places, state.evaluate_shape_gradient())
    )
    g_dot_n = bind(
        places,
        sym.n_dot(places.ambient_dim),
        auto_where=dofdesc,
    )(actx, x=grad)

    # }}}

    from pystopt.visualization import make_visualizer

    vis = make_visualizer(actx, discr, vis_order=int(p.target_order))

    filename = visualize_file_name.with_suffix("gradient")
    vis.write_file(
        filename,
        [
            ("grad", grad),
            ("g_n", g_dot_n),
        ],
        markers=["-", "--"],
        overwrite=True,
    )

    if discr.ambient_dim == 2:
        filename = visualize_file_name.with_suffix("geometry")
        vis.write_file(filename, [], aspect="equal", overwrite=True)

        from pystopt.mesh.fourier import visualize_fourier_modes

        filename = visualize_file_name.with_suffix("gradient_modes")
        visualize_fourier_modes(
            filename,
            actx,
            discr,
            [
                (r"\hat{x}", grad[0]),
                (r"\hat{y}", grad[1]),
            ],
            overwrite=True,
        )

        filename = visualize_file_name.with_suffix("gradient_normal_modes")
        visualize_fourier_modes(
            filename,
            actx,
            discr,
            [
                (r"\hat{g}", g_dot_n),
            ],
            overwrite=True,
        )
    else:
        from pystopt.mesh.spharm import visualize_spharm_modes

        for i, name in enumerate(["x", "y", "z"]):
            filename = visualize_file_name.with_suffix(f"gradient_{name}_modes")
            visualize_spharm_modes(
                filename,
                discr,
                [
                    (rf"\hat{{{name}}}", grad[i]),
                ],
                overwrite=True,
            )

        filename = visualize_file_name.with_suffix("gradient_normal_modes")
        visualize_spharm_modes(
            filename,
            discr,
            [
                (r"\hat{g}", g_dot_n),
            ],
            overwrite=True,
        )


# }}}


if __name__ == "__main__":
    import sys

    import pyopencl as cl

    if len(sys.argv) > 1:
        exec(sys.argv[1])
    else:
        run(cl._csc)
