# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

from dataclasses import replace
from typing import Any

import numpy as np

import pystopt.callbacks as cb
from pystopt.tools import get_cl_array_context, get_default_logger

logger = get_default_logger(module=__file__)


# {{{ run


def get_discretization_from_param(actx, p, mesh_name, mesh_arguments):
    import extra_optim_data as eod

    discrs = []

    for offset in [-2, 2]:
        new_mesh_arguments = mesh_arguments.copy()
        new_mesh_arguments["offset"] = np.array([offset, 0, 0][: p.ambient_dim])

        px = replace(p, mesh_name=mesh_name)
        discr, _ = eod.get_discretization_from_param(
            actx, px, mesh_name, new_mesh_arguments
        )

        discrs.append(discr)

    from pystopt.mesh import merge_disjoint_spectral_discretizations

    return merge_disjoint_spectral_discretizations(actx, discrs)


def get_geometry_collection_from_param(actx, p):
    discr_d = get_discretization_from_param(
        actx, p, p.desired_mesh_name, p.desired_mesh_arguments
    )
    pre_density_discr = get_discretization_from_param(
        actx, p, p.mesh_name, p.mesh_arguments
    )

    from pystopt.qbx import QBXLayerPotentialSource

    qbx = QBXLayerPotentialSource(pre_density_discr, **p.qbx_arguments)

    from pytential import GeometryCollection

    return GeometryCollection(
        {p.long_name: qbx, "desired": discr_d}, auto_where=p.long_name
    )


def make_parameters(
    *,
    ambient_dim: int,
    extra_kwargs: dict[str, Any],
    suffix: str = "v0",
    overwrite: bool = True,
):
    if ambient_dim == 2:
        from extra_mesh_data import StokesVolumeConservation2d_2021_08_09 as Param
    elif ambient_dim == 3:
        # from extra_mesh_data import StokesVolumeConservation3d_2021_09_14 as Param
        from extra_mesh_data import StokesHelicalBoundary_2022_04_28 as Param
    else:
        raise ValueError(f"unsupported dimension: {ambient_dim}")

    import extra_mesh_data as emd

    p = emd.make_param_from_class(
        Param,
        name="evo-gravity",
        suffix=suffix,
        overwrite=overwrite,
        **extra_kwargs,
    )
    assert p.ambient_dim == ambient_dim

    return p


def run(
    ctx_factory_or_actx,
    *,
    ambient_dim: int = 2,
    suffix: str = "v0",
    enable_gravity: bool = True,
    enable_variable_surface_tension: bool = False,
    double_helix: bool = False,
    overwrite: bool = True,
    visualize: bool = True,
) -> None:
    actx = get_cl_array_context(ctx_factory_or_actx)
    p = make_parameters(
        ambient_dim=ambient_dim, extra_kwargs={}, suffix=suffix, overwrite=overwrite
    )
    logger.info("\n%s", p)

    # {{{ geometry

    import extra_optim_data as eod

    if double_helix:
        places = get_geometry_collection_from_param(actx, p)
    else:
        places = eod.get_geometry_collection_from_param(actx, p, qbx=True)
    dofdesc = places.auto_source

    discr = places.get_discretization(dofdesc.geometry, dofdesc.discr_stage)
    logger.info("nspec:     %d", discr.nspec)
    logger.info("ndofs:     %d", discr.ndofs)
    logger.info("nelements: %d", discr.mesh.nelements)

    # }}}

    # {{{ wrangler

    # {{{ callback

    from pystopt.simulation.unsteady import (
        StokesEvolutionCallbackManager,
        StokesEvolutionHistoryCallback,
        StokesEvolutionVisualizeCallback,
    )

    visualize_callback_factory = visualize
    if visualize:
        visualize_callback_factory = StokesEvolutionVisualizeCallback

    callback = cb.make_default_evolution_callback(
        p.checkpoint_file_name,
        manager_factory=StokesEvolutionCallbackManager,
        history_callback_factory=StokesEvolutionHistoryCallback,
        visualize_callback_factory=visualize_callback_factory,
        overwrite=overwrite,
    )
    checkpoint = callback["checkpoint"].checkpoint

    # }}}

    # {{{ stokes

    context = p.stokes_arguments.copy()

    from pystopt import stokes

    bc = stokes.get_farfield_boundary_from_name(
        p.ambient_dim, p.farfield_name, **p.farfield_arguments
    )

    deltafs = bc.deltafs
    if enable_variable_surface_tension:
        vst = stokes.VariableYoungLaplaceJumpCondition(
            ambient_dim=ambient_dim,
            capillary_number_name="ca",
            surface_tension_name="gamma",
        )

        deltafs = (vst,)

    if enable_gravity:
        gravity = stokes.GravityJumpCondition(
            ambient_dim=ambient_dim, direction=ambient_dim - 1, bond_number_name="bo"
        )
        deltafs = (
            *deltafs,
            gravity,
        )
        context["bo"] = 1.0

    bc = replace(bc, deltafs=deltafs)
    op = stokes.TwoPhaseSingleLayerStokesRepresentation(bc)

    # }}}

    # {{{ evolution

    from pystopt.mesh.processing import compute_group_volumes

    vd = compute_group_volumes(actx, places, dofdesc=dofdesc)

    def state_filter(state):
        from pystopt.simulation import rescale_state_to_volume

        return rescale_state_to_volume(state, vd=vd)

    from pystopt.evolution.stokes import StokesEvolutionOperator

    evolution = StokesEvolutionOperator(
        force_normal_velocity=p.force_normal_velocity,
        force_tangential_velocity=p.force_tangential_velocity,
        force_volume_conservation=False,
    )

    # }}}

    # {{{ time

    from pystopt.simulation.unsteady import StokesEvolutionWrangler

    wrangler = StokesEvolutionWrangler(
        # geometry
        places=places,
        dofdesc=dofdesc,
        is_spectral=True,
        # stokes
        op=op,
        lambdas=p.stokes_lambdas,
        gmres_arguments={},
        # evolution
        evolution=evolution,
        # filtering
        filter_type=p.filter_type,
        filter_arguments=p.filter_arguments,
        stabilizer_name=p.stabilizer_name,
        stabilizer_arguments=p.stabilizer_arguments,
        context=context,
    )
    state0 = wrangler.get_initial_state(actx)

    from pystopt.evolution import make_adjoint_time_stepper

    time = make_adjoint_time_stepper(
        p.time_step_method,
        "state",
        state0,
        wrangler.evolution_source_term,
        dt_start=p.timestep,
        t_start=0.0,
        state_filter=state_filter,
    )

    # }}}

    # }}}

    # {{{ evolve

    from pystopt.checkpoint.hdf import array_context_for_pickling

    with array_context_for_pickling(actx):
        from pystopt.simulation.unsteady import quasi_evolve_stokes

        result = quasi_evolve_stokes(
            actx,
            wrangler,
            time,
            # tmax=None, maxit=5, callback=callback,
            tmax=p.tmax,
            maxit=p.maxit,
            callback=callback,
        )
        assert result is not None

        from pystopt.tools import dc_asdict

        history = callback["history"].to_numpy()
        checkpoint.write_to("parameters", dc_asdict(p), overwrite=True)
        checkpoint.write_to("orig_discr", discr, overwrite=True)
        checkpoint.write_to("history", history, overwrite=True)
        checkpoint.done()

    # }}}


# }}}


if __name__ == "__main__":
    import sys

    import pyopencl as cl

    if len(sys.argv) > 1:
        exec(sys.argv[1])
    else:
        run(cl._csc)
