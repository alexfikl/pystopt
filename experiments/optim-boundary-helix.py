# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

from dataclasses import dataclass, replace
from typing import Any

import numpy as np

import pystopt.callbacks as cb
from pystopt.simulation.farfield import (
    AdjointHelicalFarfieldStokesEvolutionWrangler,
    FarfieldOptimizationCallbackManager,
    FarfieldOptimizationHistoryCallback,
    HelicalFarfieldOptimizationWrangler,
    HelicalFarfieldStokesEvolutionWrangler,
)
from pystopt.simulation.quasistokes import StokesUnsteadyOptimizationVisualizeCallback
from pystopt.simulation.run import RunMode
from pystopt.simulation.unsteady import (
    AdjointStokesEvolutionCallbackManager,
    AdjointStokesEvolutionVisualizeCallback,
    StokesEvolutionCallbackManager,
    StokesEvolutionHistoryCallback,
    StokesEvolutionVisualizeCallback,
)
from pystopt.tools import get_cl_array_context, get_default_logger

logger = get_default_logger(module=__file__)


# {{{ callbacks


@dataclass(frozen=True)
class OptimizationHistoryCallback(FarfieldOptimizationHistoryCallback):
    def __call__(self, *args, **kwargs):
        r = super().__call__(*args, **kwargs)

        (state,) = args
        grad = state.evaluate_gradient()
        context = state.wrangler.forward.context

        logger.info("=" * 128)
        logger.info(
            "omega %.5e / %.5e height %.5e / %.5e ",
            state.x[0],
            context["omega_d"],
            state.x[1],
            context["height_d"],
        )
        logger.info("gradient omega %.12e height %.12e", grad[0], grad[1])
        logger.info("=" * 128)

        return r


@dataclass(frozen=True)
class EvolutionHistoryCallback(StokesEvolutionHistoryCallback):
    def __call__(self, *args, **kwargs):
        r = super().__call__(*args, **kwargs)

        (state,) = args
        ndroplets = len(state.wrangler.discr.groups)
        vd = ndroplets * 4.0 / 3.0 * np.pi

        from pystopt.tools import afmt

        logger.info("volume %.15e", abs(self.volume[-1] - vd) / vd)
        xc = np.array(self.centroid[-1]).squeeze().T
        for xc_i in xc:
            logger.info(afmt("centroid %ary", xc_i), *xc_i)

        return r


# }}}


# {{{ wranglers


@dataclass(frozen=True)
class StokesEvolutionWrangler(HelicalFarfieldStokesEvolutionWrangler):
    pass


@dataclass(frozen=True)
class AdjointStokesEvolutionWrangler(AdjointHelicalFarfieldStokesEvolutionWrangler):
    pass


@dataclass(frozen=True)
class BoundaryOptimizationWrangler(HelicalFarfieldOptimizationWrangler):
    visualize: bool

    def get_forward_callback(self, actx, checkpoint_directory_name):
        visualize_callback_factory = self.visualize
        if self.visualize:
            visualize_callback_factory = StokesEvolutionVisualizeCallback

        from pystopt.dof_array import dof_array_norm

        return cb.make_default_evolution_callback(
            checkpoint_directory_name / "forward",
            manager_factory=StokesEvolutionCallbackManager,
            history_callback_factory=EvolutionHistoryCallback,
            checkpoint_callback_factory=False,
            visualize_callback_factory=visualize_callback_factory,
            norm=dof_array_norm,
            overwrite=True,
        )

    def get_adjoint_callback(self, actx, checkpoint_directory_name):
        visualize_callback_factory = self.visualize
        if self.visualize:
            visualize_callback_factory = AdjointStokesEvolutionVisualizeCallback

        from pystopt.dof_array import dof_array_norm

        return cb.make_default_evolution_callback(
            checkpoint_directory_name / "adjoint",
            manager_factory=AdjointStokesEvolutionCallbackManager,
            history_callback_factory=False,
            checkpoint_callback_factory=False,
            visualize_callback_factory=visualize_callback_factory,
            norm=dof_array_norm,
            overwrite=True,
        )


# }}}


# {{{ geometry


def get_discretization_from_param(actx, p, mesh_name, mesh_arguments):
    import extra_optim_data as eod

    discrs = []

    for offset in [-2, 2]:
        new_mesh_arguments = mesh_arguments.copy()
        new_mesh_arguments["offset"] = np.array([offset, 0, 0][: p.ambient_dim])

        px = replace(p, mesh_name=mesh_name)
        discr = eod.get_discretization_from_param(
            actx, px, mesh_name, new_mesh_arguments
        )

        discrs.append(discr)

    from pystopt.mesh import merge_disjoint_spectral_discretizations

    return merge_disjoint_spectral_discretizations(actx, discrs)


def get_geometry_collection_from_param(actx, p):
    discr_d = get_discretization_from_param(
        actx, p, p.desired_mesh_name, p.desired_mesh_arguments
    )
    pre_density_discr = get_discretization_from_param(
        actx, p, p.mesh_name, p.mesh_arguments
    )

    from pystopt.qbx import QBXLayerPotentialSource

    qbx = QBXLayerPotentialSource(pre_density_discr, **p.qbx_arguments)

    from pytential import GeometryCollection

    return GeometryCollection(
        {p.long_name: qbx, "desired": discr_d}, auto_where=p.long_name
    )


# }}}


# {{{ setup


def make_parameters(
    *,
    extra_kwargs: dict[str, Any] | None,
    suffix: str = "v0",
    overwrite: bool = True,
):
    import extra_mesh_data as emd

    if isinstance(extra_kwargs, emd.ExampleParameters):
        from pystopt.tools import dc_asdict

        extra_kwargs = dc_asdict(extra_kwargs)
    else:
        if extra_kwargs is None:
            extra_kwargs = {}

        extra_kwargs.update({
            "viscosity_ratio": 1.0,
            "capillary_number": 0.5,
        })

    import os

    if os.environ.get("PYSTOPT_EXPERIMENT_TEST"):
        extra_kwargs.update({
            "maxit": 5,
            "tmax": None,
            "cg_arguments": {**extra_kwargs.get("cg_arguments", {}), "maxit": 3},
        })

    return emd.make_param_from_class(
        emd.StokesHelicalBoundary_2022_08_29,
        name="boundary-optim",
        suffix=suffix,
        overwrite=overwrite,
        **extra_kwargs,
    )


def make_wrangler(
    actx, p, *, visualize: bool = False, overwrite: bool = False
) -> BoundaryOptimizationWrangler:
    import extra_optim_data as eod

    from pystopt import sym

    if p.farfield_name != "helix":
        raise ValueError(p.farfield_name)

    if p.cost_name != "helix_centroid_tracking":
        raise ValueError(p.cost_name)

    # {{{ geometry

    places = get_geometry_collection_from_param(actx, p)
    dofdesc = places.auto_source
    dofdesc_d = dofdesc.copy(geometry="desired")

    # }}}

    # {{{ stokes

    farfield_arguments = {}
    farfield_arguments["tmax"] = sym.var("tmax")
    farfield_arguments["height"] = sym.var("height")
    farfield_arguments["omega"] = sym.var("omega")

    from pystopt import stokes

    bc = stokes.get_farfield_boundary_from_name(
        p.ambient_dim, p.farfield_name, **farfield_arguments
    )
    op = stokes.TwoPhaseSingleLayerStokesRepresentation(bc)

    from pystopt.evolution.stokes import StokesEvolutionOperator

    evolution = StokesEvolutionOperator(
        force_normal_velocity=p.force_normal_velocity,
        force_tangential_velocity=p.force_tangential_velocity,
    )

    # }}}

    # {{{ cost

    cost, context = eod.make_unsteady_cost_functional(
        actx, places, op, p, source_dd=dofdesc, desire_dd=dofdesc_d
    )

    context.update(p.stokes_arguments)
    context.update(p.farfield_arguments)

    # }}}

    # {{{ evolution wrangler

    from pystopt.mesh.processing import compute_group_volumes

    vd = compute_group_volumes(actx, places, dofdesc=dofdesc)

    def state_filter(state):
        from pystopt.simulation import rescale_state_to_volume

        return rescale_state_to_volume(state, vd=vd)

    from pystopt.evolution.estimates import fixed_time_step_from

    maxit, tmax, dt = fixed_time_step_from(
        maxit=p.maxit, tmax=p.tmax, timestep=p.timestep
    )

    from pystopt.evolution import TimeWrangler

    time = TimeWrangler(
        time_step_name=p.time_step_method,
        maxit=maxit,
        tmax=tmax,
        dt=dt,
        theta=1.0,
        estimate_time_step=False,
        state_filter=state_filter,
    )

    forward = StokesEvolutionWrangler(
        # geometry
        places=places,
        dofdesc=dofdesc,
        is_spectral=True,
        # stokes
        op=op,
        lambdas=p.stokes_lambdas,
        gmres_arguments={},
        # cost
        cost=cost,
        cost_final=None,
        # evolution
        evolution=evolution,
        stabilizer_name=p.stabilizer_name,
        stabilizer_arguments=p.stabilizer_arguments,
        # filtering
        filter_type=p.filter_type,
        filter_arguments=p.filter_arguments,
        context=context,
    )

    # }}}

    # {{{ optimization wrangler

    from pystopt.paths import generate_filename_series

    checkpoint_directory_series = generate_filename_series(
        "evolution", cwd=p.checkpoint_file_name.parent
    )

    wrangler = BoundaryOptimizationWrangler(
        time=time,
        forward=forward,
        checkpoint_directory_series=checkpoint_directory_series,
        visualize=visualize,
        overwrite=overwrite,
    )

    # }}}

    return wrangler


def make_callback(
    actx,
    wrangler,
    checkpoint_file_name,
    *,
    visualize: bool = False,
    overwrite: bool = False,
) -> FarfieldOptimizationCallbackManager:
    visualize_callback_factory = visualize
    if visualize:
        visualize_callback_factory = StokesUnsteadyOptimizationVisualizeCallback

    return cb.make_default_shape_callback(
        checkpoint_file_name,
        manager_factory=FarfieldOptimizationCallbackManager,
        visualize_callback_factory=visualize_callback_factory,
        history_callback_factory=OptimizationHistoryCallback,
        norm=lambda x: np.sqrt(wrangler.vdot(x, x)),
        overwrite=overwrite,
    )


def make_minimize_options(actx, wrangler, p):
    from pystopt.optimize import CartesianEucledeanSpace

    manifold = CartesianEucledeanSpace(vdot=wrangler.vdot)

    from pystopt.optimize import get_line_search_from_name

    linesearch = get_line_search_from_name(
        p.cg_linesearch_name,
        fun=lambda x: x.evaluate_cost(),
        **p.cg_linesearch_arguments,
    )

    from pystopt.optimize import get_descent_direction_from_name

    descent = get_descent_direction_from_name(
        p.cg_descent_name, **p.cg_descent_arguments
    )

    return {
        **p.cg_arguments,
        "linesearch": linesearch,
        "descent": descent,
        "manifold": manifold,
    }


def prepare_and_run(
    ctx_factory_or_actx,
    mode: RunMode,
    p,
    *,
    eps: float | np.ndarray | None = None,
    cached: bool = True,
    overwrite: bool = True,
    visualize: bool = True,
):
    actx = get_cl_array_context(ctx_factory_or_actx)

    wrangler = make_wrangler(
        actx,
        p,
        # NOTE: do not visualize evolution during optimization
        visualize=False if mode == RunMode.OPTIMIZATION else visualize,
        overwrite=overwrite,
    )

    callback = options = None
    if mode == RunMode.OPTIMIZATION:
        callback = make_callback(
            actx,
            wrangler,
            p.checkpoint_file_name,
            visualize=visualize,
            overwrite=overwrite,
        )
        options = make_minimize_options(actx, wrangler, p)
        checkpoint = callback["checkpoint"].checkpoint
    else:
        from pystopt.checkpoint import make_hdf_checkpoint_manager

        checkpoint = make_hdf_checkpoint_manager(p.checkpoint_file_name)

    from pystopt.checkpoint.hdf import array_context_for_pickling

    with array_context_for_pickling(actx):
        key = "cache_result"
        if cached and key in checkpoint:
            obj = checkpoint.read_from(key)
            result = obj["result"]
            h_max = obj["h_max"]
        else:
            from pystopt import bind, sym

            h_max = bind(
                wrangler.forward.places,
                sym.h_max_from_volume(wrangler.ambient_dim),
                auto_where=wrangler.forward.dofdesc,
            )(actx)
            h_max = actx.to_numpy(h_max)

            from pystopt.simulation.run import driver_run

            result = driver_run(
                actx, wrangler, mode, eps=eps, callback=callback, options=options
            )

            from pystopt.tools import dc_asdict

            checkpoint.write_to(
                key,
                {
                    "result": result,
                    "h_max": h_max,
                    "parameters": dc_asdict(p),
                    "history": None if callback is None else callback["history"],
                },
                overwrite=overwrite,
            )

        checkpoint.done()

    return result, h_max


# }}}


# {{{ main drivers


def run_forward_only(
    ctx_factory_or_actx,
    *,
    suffix: str = "v0",
    cached: bool = False,
    visualize: bool = False,
    overwrite: bool = False,
):
    mode = RunMode.FORWARD_ONLY
    p = make_parameters(
        extra_kwargs={}, suffix=f"{suffix}-{mode.name}", overwrite=overwrite
    )
    logger.info("\n%s", p)

    result, _ = prepare_and_run(
        ctx_factory_or_actx,
        mode,
        p,
        cached=cached,
        visualize=visualize,
        overwrite=overwrite,
    )

    logger.info("result: mode %s", mode)
    logger.info("        cost %.12e", result.cost)


def run_adjoint_only(
    ctx_factory_or_actx,
    *,
    suffix: str = "v0",
    cached: bool = False,
    visualize: bool = False,
    overwrite: bool = False,
):
    mode = RunMode.ADJOINT_ONLY
    p = make_parameters(
        extra_kwargs={}, suffix=f"{suffix}-{mode.name}", overwrite=overwrite
    )
    logger.info("\n%s", p)

    result, _ = prepare_and_run(
        ctx_factory_or_actx,
        mode,
        p,
        cached=cached,
        visualize=visualize,
        overwrite=overwrite,
    )

    logger.info("result: mode %s", mode)
    logger.info("        cost %.12e", result.cost)
    logger.info("        grad %.12e %.12e", *result.grad)


def run_optimization(
    ctx_factory_or_actx,
    *,
    suffix: str = "v0",
    cached: bool = False,
    visualize: bool = False,
    overwrite: bool = False,
):
    mode = RunMode.OPTIMIZATION
    p = make_parameters(
        extra_kwargs={}, suffix=f"{suffix}-{mode.name}", overwrite=overwrite
    )
    logger.info("\n%s", p)

    result, _ = prepare_and_run(
        ctx_factory_or_actx,
        mode,
        p,
        cached=cached,
        visualize=visualize,
        overwrite=overwrite,
    )
    solution = result.solution.x

    logger.info("result: mode %s", mode)
    logger.info("        cost %.12e", result.cost)
    logger.info("        grad %.12e %.12e", *result.grad)
    logger.info("        omega %.12e height %.12e", *solution.x)


def run_finite_difference(
    ctx_factory_or_actx,
    *,
    suffix: str = "v0",
    cached: bool = True,
    overwrite: bool = True,
    visualize: bool = False,
):
    actx = get_cl_array_context(ctx_factory_or_actx)

    omega_d = np.pi
    height_d = 1.0
    extra_kwargs = {
        "tmax": np.pi / 2,
        "timestep": 5.0e-3,
        "farfield_arguments": {
            "tau": "t",
            "height": None,
            "omega": None,
            "tmax": np.pi,
        },
        "cost_arguments": {
            "t": "t",
            "height_d": height_d,
            "omega_d": omega_d,
        },
    }

    # NOTE: runs on every side of the desired `(2, pi)` values to ensure
    # the gradients are correctly pointed at least
    eps = 1.0e-4
    height_omegas = [
        (height_d, omega_d),
        (height_d - 0.5, omega_d),
        (height_d + 0.5, omega_d),
        (height_d, omega_d - np.pi / 2),
        (height_d, omega_d + np.pi / 2),
    ]
    grad_ad = np.empty((len(height_omegas), 2))
    grad_fd = np.empty((len(height_omegas), 2))

    mode = RunMode.FINITE_DIFFERENCE
    for i, (height, omega) in enumerate(height_omegas):
        extra_kwargs["farfield_arguments"]["height"] = height
        extra_kwargs["farfield_arguments"]["omega"] = omega

        p = make_parameters(
            extra_kwargs=extra_kwargs,
            suffix=f"{suffix}-{mode.name}-{i}",
            overwrite=overwrite,
        )
        if i == 0:
            logger.info("\n%s", p)

        result, _ = prepare_and_run(
            actx,
            mode,
            p,
            eps=eps,
            cached=cached,
            overwrite=overwrite,
            visualize=visualize,
        )

        grad_ad[i, :] = result.grad
        grad_fd[i, :] = result.grad_fd[0, :]

        logger.info("result: mode %s", RunMode.FINITE_DIFFERENCE)
        logger.info("       omega %.12e height   %.12e", omega, height)
        logger.info(
            "     omega_d %.12e height_d %.12e",
            extra_kwargs["cost_arguments"]["omega_d"],
            extra_kwargs["cost_arguments"]["height_d"],
        )
        logger.info("        cost %.12e", result.cost)
        logger.info("        grad %.12e %.12e", *result.grad)
        logger.info("     grad_fd %.12e %.12e", *result.grad_fd[0, :])

    filename = p.checkpoint_file_name.parent / "results.npz"
    np.savez(
        filename,
        eps=eps,
        height_omegas=height_omegas,
        grad_ad=grad_ad,
        grad_fd=grad_fd,
    )

    from pystopt.table import LatexTable

    tb = LatexTable(ncolumns=5)
    tb.add_header(("(H, omega)", "ad[omega]", "fd[omega]", "ad[H]", "fd[H]"))

    for i, (height, omega) in enumerate(height_omegas):
        tb.add_row((
            f"({height:.2f}, {omega:.2f})",
            f"{grad_ad[i, 0]:.5e}",
            f"{grad_fd[i, 0]:.5e}",
            f"{grad_ad[i, 1]:.5e}",
            f"{grad_fd[i, 1]:.5e}",
        ))
    logger.info("\n%s", tb.render())


def run_contour(
    ctx_factory_or_actx,
    *,
    suffix: str = "v0",
    cached: bool = True,
    overwrite: bool = True,
    visualize: bool = False,
):
    actx = get_cl_array_context(ctx_factory_or_actx)

    height_d = 1.0
    omega_d = np.pi
    extra_kwargs = {
        "tmax": np.pi / 2,
        "timestep": 5.0e-3,
        "farfield_arguments": {
            "tau": "t",
            "height": None,
            "omega": None,
            "tmax": np.pi,
        },
        "cost_arguments": {
            "tau": "t",
            "height_d": height_d,
            "omega_d": omega_d,
        },
    }

    # plot a neighborhood of (1.5, 3 pi/2) with the lower left corner being the min
    height_s = height_d + 0.5
    omega_s = omega_d + np.pi / 2
    heights = np.linspace(height_s - 0.5, height_s + 0.5, 5)
    omegas = np.linspace(omega_s - np.pi / 2, omega_s + np.pi / 2, 5)
    cost = np.empty((len(heights), len(omegas)))

    from itertools import product

    for i, j in product(range(cost.shape[0]), range(cost.shape[1])):
        extra_kwargs["farfield_arguments"]["height"] = heights[i]
        extra_kwargs["farfield_arguments"]["omega"] = omegas[j]

        mode = RunMode.FORWARD_ONLY
        p = make_parameters(
            extra_kwargs=extra_kwargs,
            suffix=f"{suffix}-{mode.name}-{i}-{j}",
            overwrite=overwrite,
        )
        if i == j == 0:
            logger.info("\n%s", p)

        from pystopt.tools import dc_stringify

        logger.info("\n%s", dc_stringify(extra_kwargs))

        result, _ = prepare_and_run(
            actx, mode, p, cached=cached, overwrite=overwrite, visualize=visualize
        )

        cost[i, j] = result.cost

    extra_kwargs["farfield_arguments"]["height"] = height_s
    extra_kwargs["farfield_arguments"]["omega"] = omega_s

    mode = RunMode.FINITE_DIFFERENCE
    p = make_parameters(
        extra_kwargs=extra_kwargs,
        suffix=f"{suffix}-{mode.name}",
        overwrite=overwrite,
    )

    result, _ = prepare_and_run(
        actx,
        mode,
        p,
        eps=1.0e-5,
        cached=cached,
        overwrite=overwrite,
        visualize=visualize,
    )

    dc_duinf_ad = result.grad
    dc_duinf_fd = result.grad_fd

    filename = p.checkpoint_file_name.parent / "results.npz"
    np.savez(
        filename,
        heights=heights,
        omegas=omegas,
        dc_duinf_fd=dc_duinf_fd,
        dc_duinf_ad=dc_duinf_ad,
        cost=cost,
    )

    logger.info("dc_duinf_fd: %.12e %.12e", *dc_duinf_fd[0, :])
    logger.info("dc_duinf_ad: %.12e %.12e", *dc_duinf_ad)

    if not visualize:
        return

    plot_contour(filename, overwrite=overwrite)


# }}}


# {{{ plot_contour


def plot_contour(filename: str, *, overwrite: bool = True) -> None:
    import pathlib

    filename = pathlib.Path(filename).resolve()
    d = np.load(filename, allow_pickle=True)

    m_omegas, m_heights = np.meshgrid(d["omegas"], d["heights"])
    cost = d["cost"]

    dc_duinf_fd = d["dc_duinf_fd"].squeeze()
    dc_duinf_ad = d["dc_duinf_ad"]

    from pystopt.visualization.matplotlib import subplots

    filename = filename.parent / "optim_boundary_helix_contour"
    with subplots(filename, overwrite=overwrite) as fig:
        ax = fig.gca()
        ax.grid(visible=False, which="both")

        im = ax.contourf(m_heights, m_omegas, cost, levels=12)
        ax.contour(m_heights, m_omegas, cost, colors="k", linewidths=(1,), levels=12)
        fig.colorbar(im, ax=ax)

        ax.quiver(
            [np.pi],
            [np.pi],
            [dc_duinf_fd[0]],
            [dc_duinf_fd[1]],
            scale_units="xy",
            color="w",
            scale=20,
        )
        ax.quiver(
            [np.pi],
            [np.pi],
            [dc_duinf_ad[0]],
            [dc_duinf_ad[1]],
            scale_units="xy",
            color="r",
            scale=20,
        )

        ax.set_xlabel("$H$")
        ax.set_ylabel(r"$\omega$")


# }}}


# {{{ plot_optim_solution


def plot_optim_solution(
    ctx_factory_or_actx, filename: str, *, overwrite: bool = True
) -> None:
    actx = get_cl_array_context(ctx_factory_or_actx)

    import pathlib

    filename = pathlib.Path(filename)

    from pystopt.checkpoint import make_hdf_checkpoint_manager

    checkpoint = make_hdf_checkpoint_manager(filename, mode="r")

    # {{{ history

    from pystopt.paths import get_filename

    visualize_file_name = get_filename("history", cwd=filename.parent)

    from pystopt.checkpoint.hdf import array_context_for_pickling

    with array_context_for_pickling(actx):
        history = checkpoint.read_from("cache_result/history")

    from pystopt.visualization.optimization import visualize_optimization_history

    visualize_optimization_history(
        visualize_file_name,
        history,
        overwrite=overwrite,
    )

    # }}}


# }}}


# {{{ plot_centroid_evolution


def plot_centroid_evolution(
    ctx_factory_or_actx, filename: str, *, overwrite: bool = True
) -> None:
    actx = get_cl_array_context(ctx_factory_or_actx)

    import pathlib

    filename = pathlib.Path(filename)

    from pystopt.checkpoint import make_hdf_checkpoint_manager

    checkpoint = make_hdf_checkpoint_manager(filename, mode="r")

    from pystopt.paths import get_filename

    visualize_file_name = get_filename("centroid", cwd=filename.parent)

    from pystopt.checkpoint.hdf import array_context_for_pickling

    with array_context_for_pickling(actx):
        history = checkpoint.read_from("history/history")

    from pystopt.visualization.matplotlib import subplots

    with subplots(visualize_file_name, overwrite=overwrite) as fig:
        ax = fig.gca()

        t = history["time"]
        xc = history["centroid"].squeeze().T

        ax.plot(t, xc[0], label="$x$")
        ax.plot(t, xc[1], label="$y$")
        ax.plot(t, xc[2], label="$z$")

        ax.set_xlabel("$t$")
        ax.legend()


# }}}


if __name__ == "__main__":
    import sys

    import pyopencl as cl

    if len(sys.argv) > 1:
        exec(sys.argv[1])
    else:
        run_adjoint_only(cl._csc)
