# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

from dataclasses import dataclass
from typing import Any

import numpy as np

import pystopt.callbacks as cb
from pystopt.simulation.farfield import (
    AdjointBezierUniformFarfieldStokesEvolutionWrangler,
    BezierUniformFarfieldOptimizationWrangler,
    BezierUniformFarfieldStokesEvolutionWrangler,
    FarfieldOptimizationCallbackManager,
    FarfieldOptimizationHistoryCallback,
)
from pystopt.simulation.quasistokes import StokesUnsteadyOptimizationVisualizeCallback
from pystopt.simulation.run import RunMode
from pystopt.simulation.unsteady import (
    AdjointStokesEvolutionCallbackManager,
    AdjointStokesEvolutionVisualizeCallback,
    StokesEvolutionCallbackManager,
    StokesEvolutionHistoryCallback,
    StokesEvolutionVisualizeCallback,
)
from pystopt.tools import get_cl_array_context, get_default_logger

logger = get_default_logger(module=__file__)


# {{{ callbacks

_GEOMETRY_KWARGS = {
    "xlim": [-1.25, 8.25],
    "ylim": [-1.25, 4.25],
    "aspect": "equal",
    "markers": ["--"],
}


@dataclass(frozen=True)
class OptimizationHistoryCallback(FarfieldOptimizationHistoryCallback):
    def __call__(self, *args, **kwargs):
        r = super().__call__(*args, **kwargs)

        (state,) = args
        grad = state.evaluate_gradient()

        from pystopt.tools import afmt

        logger.info("=" * 128)
        for i, name in range(state.wrangler.forward.fieldnames):
            logger.info(
                afmt("p%d %ary p%d_d %ary grad %ary", grad[i]),
                i,
                *state.x[i],
                i,
                *state.wrangler.context[f"{name}_d"],
                *grad[i],
            )
        logger.info("=" * 128)

        return r


@dataclass(frozen=True)
class EvolutionHistoryCallback(StokesEvolutionHistoryCallback):
    def __call__(self, *args, **kwargs):
        r = super().__call__(*args, **kwargs)
        xc = self.centroid[-1]

        from pystopt.tools import afmt

        logger.info(afmt("centroid %ary", xc), xc)

        return r


class OptimizationVisualizeCallback(StokesUnsteadyOptimizationVisualizeCallback):
    def get_geometry_kwargs(self, state):
        return _GEOMETRY_KWARGS


class EvolutionVisualizeCallback(StokesEvolutionVisualizeCallback):
    def get_geometry_kwargs(self, state):
        return _GEOMETRY_KWARGS


# }}}


# {{{ wranglers


@dataclass(frozen=True)
class StokesEvolutionWrangler(BezierUniformFarfieldStokesEvolutionWrangler):
    pass


@dataclass(frozen=True)
class AdjointStokesEvolutionWrangler(
    AdjointBezierUniformFarfieldStokesEvolutionWrangler
):
    pass


@dataclass(frozen=True)
class BoundaryOptimizationWrangler(BezierUniformFarfieldOptimizationWrangler):
    visualize: bool

    def get_forward_callback(self, actx, checkpoint_directory_name):
        if not self.visualize:
            return super().get_forward_callback(actx, checkpoint_directory_name)

        from pystopt.dof_array import dof_array_norm

        return cb.make_default_evolution_callback(
            checkpoint_directory_name / "forward",
            manager_factory=StokesEvolutionCallbackManager,
            history_callback_factory=EvolutionHistoryCallback,
            checkpoint_callback_factory=False,
            visualize_callback_factory=EvolutionVisualizeCallback,
            norm=dof_array_norm,
            overwrite=True,
        )

    def get_adjoint_callback(self, actx, checkpoint_directory_name):
        if not self.visualize:
            return super().get_forward_callback(actx, checkpoint_directory_name)

        from pystopt.dof_array import dof_array_norm

        return cb.make_default_evolution_callback(
            checkpoint_directory_name / "adjoint",
            manager_factory=AdjointStokesEvolutionCallbackManager,
            history_callback_factory=False,
            checkpoint_callback_factory=False,
            visualize_callback_factory=AdjointStokesEvolutionVisualizeCallback,
            norm=dof_array_norm,
            overwrite=True,
        )


# }}}


# {{{ setup


def make_parameters(
    *,
    extra_kwargs: dict[str, Any] | None,
    suffix: str = "v0",
    overwrite: bool = True,
):
    import extra_mesh_data as emd

    if isinstance(extra_kwargs, emd.ExampleParameters):
        from pystopt.tools import dc_asdict

        extra_kwargs = dc_asdict(extra_kwargs)
    else:
        if extra_kwargs is None:
            extra_kwargs = {}

        extra_kwargs.update({
            "viscosity_ratio": 1.0,
            "capillary_number": 0.5,
        })

    import os

    value = os.environ.get("PYSTOPT_EXPERIMENT_TEST")
    if value is not None:
        try:
            maxit = int(value)
        except ValueError:
            maxit = 5

        extra_kwargs.update({
            "maxit": maxit,
            "tmax": None,
            "cg_arguments": {**extra_kwargs.get("cg_arguments", {}), "maxit": 3},
        })

    return emd.make_param_from_class(
        emd.StokesBezierBoundary2d_2021_11_28,
        name="boundary-optim",
        suffix=suffix,
        overwrite=overwrite,
        **extra_kwargs,
    )


def make_wrangler(
    actx, p, *, visualize: bool = False, overwrite: bool = False
) -> BoundaryOptimizationWrangler:
    import extra_optim_data as eod

    from pystopt import sym

    if p.farfield_name != "bezier_uniform":
        raise ValueError(p.farfield_name)

    if p.cost_name != "bezier_uniform_geometry_tracking":
        raise ValueError(p.cost_name)

    # {{{ geometry

    places = eod.get_geometry_collection_from_param(actx, p, qbx=True)
    dofdesc = places.auto_source
    dofdesc_d = dofdesc.copy(geometry="desired")

    # }}}

    # {{{ stokes

    points = {}
    sym_points = []
    for i, pts in enumerate(p.farfield_arguments["points_or_order"]):
        name = f"bezier_p{i}"
        points[name] = pts
        sym_points.append(sym.make_sym_vector(name, p.ambient_dim))

    farfield_arguments = p.farfield_arguments.copy()
    farfield_arguments["points_or_order"] = tuple(sym_points)
    farfield_arguments["tmax"] = sym.var("tmax")

    from pystopt import stokes

    bc = stokes.get_farfield_boundary_from_name(
        p.ambient_dim, p.farfield_name, **farfield_arguments
    )
    op = stokes.TwoPhaseSingleLayerStokesRepresentation(bc)

    from pystopt.evolution.stokes import StokesEvolutionOperator

    evolution = StokesEvolutionOperator(
        force_normal_velocity=p.force_normal_velocity,
        force_tangential_velocity=p.force_tangential_velocity,
    )
    evolution = StokesEvolutionOperator(
        force_normal_velocity=False, force_tangential_velocity=False
    )

    # }}}

    # {{{ cost

    cost, context = eod.make_unsteady_cost_functional(
        actx, places, op, p, source_dd=dofdesc, desire_dd=dofdesc_d
    )

    context.update(p.stokes_arguments)
    context.update(points)

    # }}}

    # {{{ evolution wrangler

    from pystopt.mesh.processing import compute_group_volumes

    vd = compute_group_volumes(actx, places, dofdesc=dofdesc)

    def state_filter(state):
        from pystopt.simulation import rescale_state_to_volume

        return rescale_state_to_volume(state, vd=vd)

    from pystopt.evolution.estimates import fixed_time_step_from

    maxit, tmax, dt = fixed_time_step_from(maxit=p.maxit, tmax=p.tmax, timestep=p.dt)

    from pystopt.evolution import TimeWrangler

    time = TimeWrangler(
        time_step_name=p.time_step_method,
        maxit=maxit,
        tmax=tmax,
        dt=dt,
        theta=1.0,
        estimate_time_step=False,
        state_filter=state_filter,
    )

    forward = StokesEvolutionWrangler(
        # geometry
        places=places,
        dofdesc=dofdesc,
        is_spectral=True,
        # stokes
        op=op,
        lambdas=p.stokes_lambdas,
        gmres_arguments={},
        # cost
        cost=cost,
        cost_final=None,
        # evolution
        evolution=evolution,
        stabilizer_name=p.stabilizer_name,
        stabilizer_arguments=p.stabilizer_arguments,
        # filtering
        filter_type=p.filter_type,
        filter_arguments=p.filter_arguments,
        context={**context, "tmax": time.tmax},
    )

    # }}}

    # {{{ optimization wrangler

    from pystopt.paths import generate_filename_series

    checkpoint_directory_series = generate_filename_series(
        "evolution", cwd=p.checkpoint_file_name.parent
    )

    wrangler = BoundaryOptimizationWrangler(
        time=time,
        forward=forward,
        checkpoint_directory_series=checkpoint_directory_series,
        visualize=visualize,
        overwrite=overwrite,
    )

    # }}}

    return wrangler


def make_callback(
    actx,
    wrangler,
    checkpoint_file_name,
    *,
    visualize: bool = False,
    overwrite: bool = False,
) -> FarfieldOptimizationCallbackManager:
    visualize_callback_factory = visualize
    if visualize:
        visualize_callback_factory = OptimizationVisualizeCallback

    return cb.make_default_shape_callback(
        checkpoint_file_name,
        manager_factory=FarfieldOptimizationCallbackManager,
        visualize_callback_factory=visualize_callback_factory,
        history_callback_factory=OptimizationHistoryCallback,
        norm=lambda x: np.sqrt(wrangler.vdot(x, x)),
        overwrite=overwrite,
    )


def make_minimize_options(actx, wrangler, p):
    from pystopt.optimize import CartesianEucledeanSpace

    manifold = CartesianEucledeanSpace(vdot=wrangler.vdot)

    from pystopt.optimize import get_line_search_from_name

    linesearch = get_line_search_from_name(
        p.cg_linesearch_name,
        fun=lambda x: x.evaluate_cost(),
        **p.cg_linesearch_arguments,
    )

    from pystopt.optimize import get_descent_direction_from_name

    descent = get_descent_direction_from_name(
        p.cg_descent_name, **p.cg_descent_arguments
    )

    return {
        **p.cg_arguments,
        "linesearch": linesearch,
        "descent": descent,
        "manifold": manifold,
    }


def prepare_and_run(
    ctx_factory_or_actx,
    mode: RunMode,
    p,
    *,
    eps: float | np.ndarray | None = None,
    cached: bool = True,
    overwrite: bool = True,
    visualize: bool = True,
):
    actx = get_cl_array_context(ctx_factory_or_actx)

    wrangler = make_wrangler(
        actx,
        p,
        # NOTE: do not visualize evolution during optimization
        visualize=False if mode == RunMode.OPTIMIZATION else visualize,
        overwrite=overwrite,
    )

    callback = options = None
    if mode == RunMode.OPTIMIZATION:
        callback = make_callback(
            actx,
            wrangler,
            p.checkpoint_file_name,
            visualize=visualize,
            overwrite=overwrite,
        )
        options = make_minimize_options(actx, wrangler, p)
        checkpoint = callback["checkpoint"].checkpoint
    else:
        from pystopt.checkpoint import make_hdf_checkpoint_manager

        checkpoint = make_hdf_checkpoint_manager(p.checkpoint_file_name)

    from pystopt.checkpoint.hdf import array_context_for_pickling

    with array_context_for_pickling(actx):
        key = "cache_result"
        if cached and key in checkpoint:
            obj = checkpoint.read_from(key)
            result = obj["result"]
            h_max = obj["h_max"]
        else:
            from pystopt import bind, sym

            h_max = bind(
                wrangler.forward.places,
                sym.h_max_from_volume(wrangler.ambient_dim),
                auto_where=wrangler.forward.dofdesc,
            )(actx)
            h_max = actx.to_numpy(h_max)

            from pystopt.simulation.run import driver_run

            result = driver_run(
                actx, wrangler, mode, eps=eps, callback=callback, options=options
            )

            checkpoint.write_to(
                key,
                {
                    "result": result,
                    "h_max": h_max,
                    "parameters": p,
                    "history": None if callback is None else callback["history"],
                },
                overwrite=overwrite,
            )

        checkpoint.done()

    return result, h_max


# }}}


# {{{ run


def run_forward_only(
    ctx_factory_or_actx,
    *,
    suffix: str = "v0",
    cached: bool = False,
    visualize: bool = False,
    overwrite: bool = False,
):
    mode = RunMode.FORWARD_ONLY
    p = make_parameters(
        extra_kwargs={}, suffix=f"{suffix}-{mode.name}", overwrite=overwrite
    )
    logger.info("\n%s", p)

    result, _ = prepare_and_run(
        ctx_factory_or_actx,
        mode,
        p,
        cached=cached,
        visualize=visualize,
        overwrite=overwrite,
    )

    logger.info("result: mode %s", mode)
    logger.info("        cost %.12e", result.cost)


def run_adjoint_only(
    ctx_factory_or_actx,
    *,
    suffix: str = "v0",
    cached: bool = False,
    visualize: bool = False,
    overwrite: bool = False,
):
    mode = RunMode.ADJOINT_ONLY
    p = make_parameters(
        extra_kwargs={}, suffix=f"{suffix}-{mode.name}", overwrite=overwrite
    )
    logger.info("\n%s", p)

    result, _ = prepare_and_run(
        ctx_factory_or_actx,
        mode,
        p,
        cached=cached,
        visualize=visualize,
        overwrite=overwrite,
    )

    logger.info("result: mode %s", mode)
    logger.info("        cost %.12e", result.cost)
    logger.info("        grad %.12e %.12e", *result.grad)


def run_finite_difference(
    ctx_factory_or_actx,
    *,
    suffix: str = "v0",
    cached: bool = False,
    visualize: bool = False,
    overwrite: bool = False,
):
    mode = RunMode.OPTIMIZATION
    p = make_parameters(
        extra_kwargs={}, suffix=f"{suffix}-{mode.name}", overwrite=overwrite
    )
    logger.info("\n%s", p)

    result, _ = prepare_and_run(
        ctx_factory_or_actx,
        mode,
        p,
        eps=1.0e-7,
        cached=cached,
        visualize=visualize,
        overwrite=overwrite,
    )

    logger.info("result: mode %s", mode)
    logger.info("        cost %.12e", result.cost)
    logger.info("        grad %.12e %.12e", *result.grad)
    logger.info("      finite %.12e %.12e", *result.grad_fd[0, :])


def run_optimization(
    ctx_factory_or_actx,
    *,
    suffix: str = "v0",
    cached: bool = False,
    visualize: bool = False,
    overwrite: bool = False,
):
    mode = RunMode.OPTIMIZATION
    p = make_parameters(
        extra_kwargs={}, suffix=f"{suffix}-{mode.name}", overwrite=overwrite
    )
    logger.info("\n%s", p)

    result, _ = prepare_and_run(
        ctx_factory_or_actx,
        mode,
        p,
        cached=cached,
        visualize=visualize,
        overwrite=overwrite,
    )
    solution = result.solution.x

    logger.info("result: mode %s", mode)
    logger.info("        cost %.12e", result.cost)
    logger.info("        grad %.12e %.12e", *result.grad)
    logger.info("        uinf %.12e vinf %.12e", *solution.x)


# }}}


# {{{ plot_optim_solution


def plot_optim_solution(
    ctx_factory_or_actx, filename: str, *, overwrite: bool = True
) -> None:
    actx = get_cl_array_context(ctx_factory_or_actx)

    import pathlib

    filename = pathlib.Path(filename)

    from pystopt.checkpoint import make_hdf_checkpoint_manager

    checkpoint = make_hdf_checkpoint_manager(filename, mode="r")

    # {{{ history

    from pystopt.paths import get_filename

    visualize_file_name = get_filename("history", cwd=filename.parent)

    from pystopt.checkpoint.hdf import array_context_for_pickling

    with array_context_for_pickling(actx):
        history = checkpoint.read_from("cache_result/history")

    from pystopt.visualization.optimization import visualize_optimization_history

    visualize_optimization_history(
        visualize_file_name,
        history,
        overwrite=overwrite,
    )

    # }}}


# }}}


if __name__ == "__main__":
    import sys

    import pyopencl as cl

    if len(sys.argv) > 1:
        exec(sys.argv[1])
    else:
        run_adjoint_only(cl._csc)
