# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

from dataclasses import dataclass, replace

import numpy as np

from pytools.obj_array import make_obj_array

import pystopt.callbacks as cb
from pystopt.simulation.spheroid import SpheroidOptimizationHistoryCallback
from pystopt.simulation.unconstrained import (
    UnconstrainedShapeOptimizationVisualizeCallback,
)
from pystopt.tools import get_cl_array_context, get_default_logger

logger = get_default_logger(module=__file__)


@dataclass(frozen=True)
class SpheroidVisualizeCallback(UnconstrainedShapeOptimizationVisualizeCallback):
    def get_geometry_kwargs(self, state):
        return {"xlim": [-2.25, 2.25], "ylim": [-2.25, 2.25]}


@dataclass(frozen=True)
class LoggedHistoryCallback(SpheroidOptimizationHistoryCallback):
    def __call__(self, *args, **kwargs):
        r = super().__call__(*args, **kwargs)

        (state,) = args
        grad = state.evaluate_gradient()

        from pystopt.tools import afmt

        logger.info(
            afmt("radius %.5e aspect_ratio %.5e angles %ary", grad[2:]),
            state.x[0],
            state.x[1],
            *state.x[2:],
        )
        logger.info(
            afmt("radius %.5e aspect_ratio %.5e angles %ary", grad[2:]),
            grad[0],
            grad[1],
            *grad[2:],
        )

        return r


# {{{ run


def check_gradient_vs_finite(state0, *, eps: float = 1.0e-5):
    logger.info("eps: %.5e", eps)
    cost0 = state0.cost

    grad_ad = state0.gradient
    grad_fd = np.empty_like(grad_ad)
    for i in range(state0.x.size):
        x = state0.x.copy()
        x[i] = x[i] + eps
        state = replace(state0, x=x)

        grad_fd[i] = (state.cost - cost0) / eps
        logger.info(
            "gradient: %6s ad %+.5e fd %+.5e error %.5e",
            state0._FIELDNAMES[i],
            grad_ad[i],
            grad_fd[i],
            abs(grad_ad[i] - grad_fd[i]) / abs(grad_fd[i]),
        )

    return grad_fd


def make_parameters(
    ambient_dim: int,
    *,
    suffix: str = "v0",
    from_restart_filename: str | None = None,
    overwrite: bool = True,
):
    if ambient_dim == 2:
        from extra_mesh_data import ShapeEllipseParameters_2021_12_19 as Parameters
    elif ambient_dim == 3:
        from extra_mesh_data import ShapeSpheroidParameters_2021_12_19 as Parameters
    else:
        raise ValueError(f"unsupported dimension: '{ambient_dim}'")

    import extra_mesh_data as emd

    p = emd.make_param_from_class(
        Parameters,
        name="optim-shape-rom",
        from_restart_filename=from_restart_filename,
        suffix=suffix,
        overwrite=overwrite,
    )
    assert p.ambient_dim == ambient_dim

    return p


def make_wrangler(actx, p):
    if p.ambient_dim == 2:
        x = make_obj_array([
            p.mesh_arguments["radius"],
            p.mesh_arguments["aspect_ratio"],
            p.mesh_arguments.pop("alpha"),
        ])
    else:
        x = make_obj_array([
            p.mesh_arguments["radius"],
            p.mesh_arguments["aspect_ratio"],
            *p.mesh_arguments.pop("alpha"),
        ])

    from pystopt.mesh.generation import make_transform_matrix_from_angles

    p.mesh_arguments["transform_matrix"] = make_transform_matrix_from_angles(*x[2:])

    import extra_optim_data as eod

    places = eod.get_geometry_collection_from_param(actx, p, qbx=False)
    srcdesc = places.auto_source
    tgtdesc = srcdesc.copy(geometry="desired")

    cost, context = eod.make_shape_cost_functional(
        actx, places, p, source_dd=srcdesc, desire_dd=tgtdesc
    )

    from pystopt.simulation.spheroid import SpheroidShapeOptimizationWrangler

    wrangler = SpheroidShapeOptimizationWrangler(
        places=places,
        dofdesc=srcdesc,
        is_spectral=True,
        cost=cost,
        context=context,
        filter_type=p.filter_type,
        filter_arguments=p.filter_arguments,
        resolution=p.resolution,
        mesh_kwargs=p.mesh_arguments.copy(),
    )
    state0 = wrangler.get_initial_state(actx, x)

    return wrangler, state0


def run(
    ctx_factory_or_actx,
    *,
    ambient_dim: int = 2,
    from_restart: bool | int = False,
    from_restart_filename: str | None = None,
    suffix: str = "v0",
    overwrite: bool = True,
    visualize: bool = True,
):
    actx = get_cl_array_context(ctx_factory_or_actx)

    p = make_parameters(
        ambient_dim,
        suffix=suffix,
        from_restart_filename=from_restart_filename,
        overwrite=overwrite,
    )
    wrangler, state0 = make_wrangler(actx, p)

    # {{{ output

    visualize_callback_factory = visualize
    if visualize:
        visualize_callback_factory = SpheroidVisualizeCallback

    from pystopt.simulation.spheroid import SpheroidOptimizationCallbackManager

    callback = cb.make_default_shape_callback(
        p.checkpoint_file_name,
        manager_factory=SpheroidOptimizationCallbackManager,
        visualize_callback_factory=visualize_callback_factory,
        history_callback_factory=LoggedHistoryCallback,
        norm=lambda x: np.linalg.norm(getattr(x, "x", x)),
        from_restart=from_restart,
        overwrite=overwrite,
    )
    checkpoint = callback["checkpoint"].checkpoint

    logger.info("\n%s", str(p))

    import os

    logger.info("=" * 48)
    logger.info("Send SIGUSR1 to PID '%s' to stop optimization", os.getpid())

    from pystopt.paths import relative_to

    logger.info("Stop file '%s'", relative_to(callback.stop_file_name))
    if from_restart:
        logger.info("Restarted from '%s'", relative_to(p.checkpoint_file_name))
    logger.info("=" * 48)

    places = wrangler.places
    srcdesc = places.auto_source
    tgtdesc = getattr(wrangler.cost, "dofdesc", None)
    discr = places.get_discretization(srcdesc.geometry, srcdesc.discr_stage)

    logger.info("nelements: %d", discr.mesh.nelements)
    logger.info("ndofs:     %d", discr.ndofs)
    logger.info("nspec:     %d", discr.nspec)

    # }}}

    # {{{ initial state

    from pystopt.checkpoint.hdf import array_context_for_pickling

    if from_restart:
        from pystopt.checkpoint import get_result_from_checkpoint

        with array_context_for_pickling(actx):
            result = get_result_from_checkpoint(
                checkpoint, from_restart=from_restart - 1
            )

            state0 = replace(state0, x=actx.thaw(result))

        # TODO: reset rtol and ftol so that they match

    if visualize and ambient_dim == 3:
        from pystopt.visualization import make_visualizer

        discr_d = places.get_discretization(tgtdesc.geometry, tgtdesc.discr_stage)
        vis = make_visualizer(actx, discr_d)

        from pystopt.paths import get_filename

        filename = get_filename("visualize", suffix="desired", cwd=p.dirname)
        vis.write_file(filename, [], overwrite=overwrite)

    # }}}

    # {{{ optimize

    from pystopt.optimize import CartesianEucledeanSpace

    manifold = CartesianEucledeanSpace(vdot=lambda x, y: np.vdot(x.x, y.x))

    from pystopt.optimize import get_line_search_from_name

    linesearch = get_line_search_from_name(
        p.cg_linesearch_name, fun=lambda x: x.cost, **p.cg_linesearch_arguments
    )

    from pystopt.optimize import get_descent_direction_from_name

    descent = get_descent_direction_from_name(
        p.cg_descent_name, **p.cg_descent_arguments
    )

    options = {
        **p.cg_arguments,
        "linesearch": linesearch,
        "descent": descent,
        "manifold": manifold,
    }

    from pystopt.optimize.steepest import minimize

    with array_context_for_pickling(actx):
        r = minimize(
            fun=lambda x: x.evaluate_cost(),
            x0=state0,
            jac=lambda x: replace(x, x=x.evaluate_gradient()),
            funjac=lambda x: (
                x.evaluate_cost(),
                replace(x, x=x.evaluate_gradient()),
            ),
            callback=callback,
            options=options,
        )

    # }}}

    # {{{ save results

    if from_restart:
        from pystopt.optimize.cg_utils import combine_cg_results

        with array_context_for_pickling(actx):
            rprev = checkpoint.read_from("result")

        r = combine_cg_results(r, rprev, n=max(0, from_restart - 1))

    logger.info("result:\n%s", r.pretty())

    with array_context_for_pickling(actx):
        from pystopt.tools import dc_asdict

        checkpoint.write_to("parameters", dc_asdict(p), overwrite=True)
        checkpoint.write_to("callback", callback, overwrite=True)
        checkpoint.write_to("result", r, overwrite=True)

        checkpoint.done()

    # }}}

    if not visualize:
        return

    plot_optimization_convergence(actx, p.checkpoint_file_name, overwrite=overwrite)


# }}}


# {{{ run convergence


def make_analytic_spheroid_gradient(actx, state0):
    from pystopt import bind, sym

    ambient_dim = state0.wrangler.ambient_dim
    dofdesc = state0.wrangler.dofdesc

    if ambient_dim == 2:
        from pystopt.simulation.spheroid import make_ellipse_gradient

        r = make_ellipse_gradient(sym.var("rho"), sym.var("a"), sym.var("alpha"))
    elif ambient_dim == 3:
        from pystopt.simulation.spheroid import make_spheroid_gradient

        r = make_spheroid_gradient(
            sym.var("rho"),
            sym.var("a"),
            [sym.var("alpha"), sym.var("beta"), sym.var("gamma")],
        )
    else:
        raise ValueError(f"unsupported dimension: {ambient_dim}")

    return [
        bind(state0.places, grad, auto_where=dofdesc)(
            actx, **state0.wrangler.state_to_fieldnames(state0)
        )
        for grad in r
    ]


def make_finite_difference_gradient(actx, state0, eps):
    dofdesc = state0.dofdesc

    discr = state0.places.get_discretization(dofdesc.geometry, dofdesc.discr_stage)
    x = actx.thaw(discr.nodes())

    r = [None] * state0.x.size
    for i in range(state0.x.size):
        p = state0.x.copy()
        p[i] += eps
        state = replace(state0, x=p)

        discr = state.places.get_discretization(dofdesc.geometry, dofdesc.discr_stage)
        xp = actx.thaw(discr.nodes())

        r[i] = (xp - x) / eps

    return r


def run_convergence_eps(
    ctx_factory_or_actx,
    *,
    ambient_dim: int = 2,
    suffix: str = "v0",
    overwrite: bool = True,
):
    actx = get_cl_array_context(ctx_factory_or_actx)

    nvariables = 3 if ambient_dim == 2 else 5
    eps = 10.0 ** (-np.arange(1, 8))

    p = make_parameters(ambient_dim, suffix=suffix, overwrite=overwrite)
    wrangler, state0 = make_wrangler(actx, p)
    logger.info("\n%s", p)

    from pystopt.measure import EOCRecorder, stringify_eoc

    eoc = [EOCRecorder(name=name) for name in wrangler.fieldnames]

    grad_ad = state0.evaluate_gradient()
    grad_fd = np.empty((nvariables, eps.size))
    # grad_ad = make_analytic_spheroid_gradient(actx, state0)
    # grad_fd = np.empty((nvariables, eps.size), dtype=object)
    # from pystopt.dof_array import dof_array_rnorm

    for i in range(eps.size):
        grad_fd[:, i] = check_gradient_vs_finite(state0, eps=eps[i])
        # grad_fd[:, i] = make_finite_difference_gradient(actx, state0, eps=eps[i])

        for j in range(nvariables):
            error = abs(grad_fd[j, i])
            if error < 1.0e-14:
                error = 1

            error = abs(grad_fd[j, i] - grad_ad[j]) / error
            # error = dof_array_rnorm(grad_ad[j], grad_fd[j, i])

            eoc[j].add_data_point(eps[i], actx.to_numpy(error))

    # NOTE: this is not going to converge because we're not comparing the same
    # values. basically when deriving the shape gradient, we assume that the
    # perturbations only have normal components. however, here the perturbation
    # is of the form
    #       xhat = A x
    # where A depends on which variable we're taking the derivative wrt, and
    # is generally not only in the normal direction.

    logger.info("\n%s", stringify_eoc(*eoc))

    filename = p.checkpoint_file_name.with_suffix(".npz")
    logger.info("output: %s", filename)

    from pystopt.tools import dc_asdict

    np.savez(
        filename, eps=eps, grad_ad=grad_ad, grad_fd=grad_fd, parameters=dc_asdict(p)
    )


# }}}


# {{{ plot


def plot_optimization_convergence(ctx_factory, filename, *, overwrite=True):
    actx = get_cl_array_context(ctx_factory)

    import pathlib

    filename = pathlib.Path(filename)
    dirname = filename.parent

    from pystopt.paths import get_filename

    visualize_file_name = get_filename("history", cwd=dirname)

    from pystopt.checkpoint import make_hdf_checkpoint

    checkpoint = make_hdf_checkpoint(filename)

    from pystopt.checkpoint.hdf import array_context_for_pickling

    with array_context_for_pickling(actx):
        history = checkpoint.read("callback/callbacks/history")
    del checkpoint

    from pystopt.visualization.optimization import visualize_optimization_history

    visualize_optimization_history(visualize_file_name, history, overwrite=overwrite)


# }}}


if __name__ == "__main__":
    import sys

    import pyopencl as cl

    if len(sys.argv) > 1:
        exec(sys.argv[1])
    else:
        run(cl._csc)
