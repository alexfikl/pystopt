# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

from dataclasses import dataclass, field
from typing import Any

import numpy as np

import pystopt.callbacks as cb
import pystopt.simulation as sim
import pystopt.simulation.common as scm
from pystopt.tools import get_cl_array_context, get_default_logger

logger = get_default_logger(module=__file__)


# {{{ output


@dataclass
class CallbackManager(sim.StokesShapeOptimizationCallbackManager):
    def get_output_field_getters(self):
        return (
            *super().get_output_field_getters(),
            scm.get_output_fields_modes,
        )


@dataclass(frozen=True)
class ModesVisualizeCallback(sim.StokesShapeOptimizationVisualizeCallback):
    plot_modes: bool = False

    def get_geometry_kwargs(self, state):
        return {"xlim": [-2.25, 2.25], "ylim": [-2.25, 2.25]}

    def get_modes_kwargs(self, state):
        return {
            "mode": "imshow",
            "component": "abs",
            "semilogy": True,
            "output": "abs",
        }

    def get_file_writers(self):
        writers = super().get_file_writers()
        if self.plot_modes:
            if self.ambient_dim == 2:
                writers += (scm.visualize_fourier_mode_writer,)
            else:
                writers += (scm.visualize_spharm_mode_writer,)

        return writers


# }}}


# {{{ main


@dataclass(frozen=True)
class StokesShapeOptimizationWrangler(sim.StokesShapeOptimizationWrangler):
    _density_cache: dict[Any, Any] = field(default_factory=dict, init=False, repr=False)

    def evaluate_shape_gradient(self, actx, places, context=None) -> np.ndarray:
        grad = super().evaluate_shape_gradient(actx, places, context=context)

        return self.filter_field(actx, places, grad)


def run(
    ctx_factory_or_actx,
    *,
    ambient_dim: int = 2,
    viscosity_ratio: float = 2.0,
    capillary_number: float = 0.05,
    suffix: str = "v0",
    from_restart: bool | int = False,
    overwrite: bool = True,
    visualize: bool = True,
) -> None:
    actx = get_cl_array_context(ctx_factory_or_actx)

    if ambient_dim == 2:
        from extra_mesh_data import StokesCircleSteady_2022_01_10 as Parameters
    elif ambient_dim == 3:
        from extra_mesh_data import StokesSphereSteady_2021_09_13 as Parameters
    else:
        raise ValueError(f"unsupported dimension: '{ambient_dim}'")

    import extra_mesh_data as emd

    p = emd.make_param_from_class(
        Parameters,
        name="optim-stokes",
        suffix=f"ca-{capillary_number:.3f}-vr-{viscosity_ratio:.2f}-{suffix}",
        overwrite=overwrite,
        viscosity_ratio=viscosity_ratio,
        capillary_number=capillary_number,
    )
    assert p.ambient_dim == ambient_dim

    logger.info("\n%s", str(p))

    # {{{ output

    if visualize:
        visualize_callback_factory = ModesVisualizeCallback
    else:
        visualize_callback_factory = False

    from pystopt.dof_array import dof_array_norm

    callback = cb.make_default_shape_callback(
        p.checkpoint_file_name,
        manager_factory=CallbackManager,
        visualize_callback_factory=visualize_callback_factory,
        history_callback_factory=sim.StokesShapeOptimizationHistoryCallback,
        norm=dof_array_norm,
        from_restart=from_restart,
        overwrite=overwrite,
    )
    checkpoint = callback["checkpoint"].checkpoint

    # }}}

    # {{{ geometry

    import extra_optim_data as eod

    places, _ = eod.get_geometry_collection_from_param(actx, p, qbx=True)
    srcdesc = places.auto_source
    tgtdesc = srcdesc.copy(geometry="desired")

    discr = places.get_discretization(srcdesc.geometry, srcdesc.discr_stage)

    logger.info("\n%s", str(p))

    import os

    logger.info("=" * 48)
    logger.info("Send SIGUSR1 to PID '%s' to stop optimization", os.getpid())

    from pystopt.paths import relative_to

    logger.info("Stop file '%s'", relative_to(callback.stop_file_name))
    if from_restart:
        logger.info("Restarted from '%s'", relative_to(p.checkpoint_file_name))
    logger.info("=" * 48)

    logger.info("nelements: %d", discr.mesh.nelements)
    logger.info("ndofs:     %d", discr.ndofs)
    logger.info("nspec:     %d", discr.nspec)

    # }}}

    # {{{ optimization problem

    from pystopt import stokes

    bc = stokes.get_extensional_farfield(
        p.ambient_dim, alpha=1.0, axis=2, capillary_number_name="ca"
    )

    from pystopt.stokes import TwoPhaseSingleLayerStokesRepresentation

    op = TwoPhaseSingleLayerStokesRepresentation(bc)

    cost, context = eod.make_stokes_cost_functional(
        actx, places, op, p, source_dd=srcdesc, desire_dd=tgtdesc
    )

    from dataclasses import replace

    wrangler = sim.make_stokes_shape_optimization_wrangler(
        places,
        cost,
        op,
        viscosity_ratio=p.viscosity_ratio,
        capillary_number=p.capillary_number,
        dofdesc=srcdesc,
        context=context,
        is_spectral=True,
        filter_type=p.grad_filtered,
        filter_arguments=p.filter_arguments,
    )

    from pystopt.tools import dc_asdict

    wrangler = StokesShapeOptimizationWrangler(**dc_asdict(wrangler))

    # }}}

    # {{{ minimize

    state0 = wrangler.get_initial_state(actx)

    from pystopt.optimize import CartesianEucledeanSpace

    manifold = CartesianEucledeanSpace(vdot=wrangler.vdot)

    from pystopt.optimize import get_line_search_from_name

    linesearch = get_line_search_from_name(
        p.cg_linesearch_name, fun=lambda x: x.cost, **p.cg_linesearch_arguments
    )

    from pystopt.optimize import get_descent_direction_from_name

    descent = get_descent_direction_from_name(
        p.cg_descent_name, **p.cg_descent_arguments
    )

    options = {
        **p.cg_arguments,
        "linesearch": linesearch,
        "descent": descent,
        "manifold": manifold,
    }

    from pystopt.checkpoint.hdf import array_context_for_pickling

    if from_restart:
        from pystopt.checkpoint import get_result_from_checkpoint

        with array_context_for_pickling(actx):
            if not isinstance(from_restart, bool):
                from_restart = from_restart - 1

            result = get_result_from_checkpoint(checkpoint, from_restart=from_restart)

            state0 = replace(state0, x=result)

        if "ftol" in options:
            f0 = callback["history"].f[0]
            f1 = state0.cost

            # NOTE: should ensure that the tolerance stays roughly the same
            # between restarts, otherwise it would constantly decrease
            options["ftol"] = 0.1 * f0 / f1 * options["ftol"]

        if "rtol" in options:
            # FIXME: these don't quite line up because they're using different
            # norms :\
            g0 = callback["history"].g[0]
            g1 = actx.to_numpy(dof_array_norm(state0.gradient, ord=2))

            options["rtol"] = 0.1 * g0 / g1 * options["rtol"]

    with array_context_for_pickling(actx):
        from pystopt.optimize.steepest import minimize

        r = minimize(
            fun=lambda x: x.cost,
            x0=state0,
            jac=lambda x: x.wrap(x.gradient),
            funjac=lambda x: (x.cost, x.wrap(x.gradient)),
            callback=callback,
            options=options,
        )

    if from_restart:
        from pystopt.optimize.cg_utils import combine_cg_results

        with array_context_for_pickling(actx):
            rprev = checkpoint.read_from("result")

        n = 0
        if not isinstance(from_restart, bool):
            n = max(0, from_restart - 1)

        r = combine_cg_results(r, rprev, n=n)

    logger.info("result:\n%s", r.pretty())

    # }}}

    # {{{ plot

    checkpoint_file_name = checkpoint.filename

    with array_context_for_pickling(actx):
        checkpoint.write_to("parameters", dc_asdict(p), overwrite=True)
        checkpoint.write_to("callback", callback, overwrite=True)

        r = replace(r, x=r.x.x)
        checkpoint.write_to("result", r, overwrite=True)

        checkpoint.done()

    plot_solution(actx, checkpoint_file_name, overwrite=overwrite)

    # }}}


# }}}


# {{{ plot


def get_deformation_from_file(filename, *, capillary_number):
    lambda_vs_ca = np.genfromtxt(
        filename, dtype=np.float64, delimiter=",", skip_header=True, usemask=True
    ).T

    ca = lambda_vs_ca[0]
    lm = lambda_vs_ca[1]

    try:
        from scipy.interpolate import interp1d

        return interp1d(ca, lm, kind="quadratic", copy=False)(capillary_number)
    except ImportError:
        return np.interp(capillary_number, ca, lm)


def plot_solution(
    ctx_factory,
    filename,
    *,
    deformation_file_name: str | None = None,
    overwrite: bool = True,
):
    actx = get_cl_array_context(ctx_factory)

    import pathlib

    filename = pathlib.Path(filename)

    from pystopt.paths import get_filename

    visualize_file_name = get_filename("history", cwd=filename.parent)

    from pystopt.checkpoint import make_hdf_checkpoint

    checkpoint = make_hdf_checkpoint(filename, mode="r")

    from pystopt.checkpoint.hdf import array_context_for_pickling

    with array_context_for_pickling(actx):
        history = checkpoint.read("callback/callbacks/history/history")
        capillary_number = checkpoint.read("parameters")["capillary_number"]

    del checkpoint

    deformation = 0.22
    if deformation_file_name:
        deformation = get_deformation_from_file(
            deformation_file_name, capillary_number=capillary_number
        )

    from pystopt.visualization.optimization import visualize_optimization_history

    visualize_optimization_history(
        visualize_file_name,
        history,
        overwrite=overwrite,
    )

    from pystopt.visualization.optimization import visualize_stokes_evolution_history

    visualize_stokes_evolution_history(
        visualize_file_name,
        history,
        expected_deformation=deformation,
        overwrite=overwrite,
    )


# }}}


if __name__ == "__main__":
    import sys

    import pyopencl as cl

    if len(sys.argv) > 1:
        exec(sys.argv[1])
    else:
        run(cl._csc)
