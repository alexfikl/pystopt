# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT


import numpy as np

from pystopt import bind, sym
from pystopt.tools import get_cl_array_context, get_default_logger

logger = get_default_logger(module=__file__)


# {{{ wranglers


def make_geometry_wrangler(actx, p, places, srcdesc=None, tgtdesc=None):
    import extra_optim_data as eod

    cost, context = eod.make_shape_cost_functional(
        actx, places, p, source_dd=srcdesc, desire_dd=tgtdesc
    )

    from pystopt.simulation.unconstrained import (
        make_unconstrained_shape_optimization_wrangler,
    )

    wrangler = make_unconstrained_shape_optimization_wrangler(
        places,
        cost,
        is_spectral=False,
        dofdesc=srcdesc,
        context=context,
        filter_type=p.filter_type,
        filter_arguments=p.filter_arguments,
    )

    return wrangler


def make_stokes_wrangler(actx, p, places, srcdesc=None, tgtdesc=None):
    from pystopt import stokes

    bc = stokes.get_farfield_boundary_from_name(
        p.ambient_dim, p.farfield_name, **p.farfield_arguments
    )
    op = stokes.TwoPhaseSingleLayerStokesRepresentation(bc)

    import extra_optim_data as eod

    cost, context = eod.make_stokes_cost_functional(
        actx, places, op, p, source_dd=srcdesc, desire_dd=tgtdesc
    )

    from pystopt.simulation.staticstokes import make_stokes_shape_optimization_wrangler

    wrangler = make_stokes_shape_optimization_wrangler(
        places,
        cost,
        op,
        viscosity_ratio=p.viscosity_ratio,
        capillary_number=p.capillary_number,
        is_spectral=False,
        dofdesc=srcdesc,
        context=context,
        filter_type=p.filter_type,
        filter_arguments=p.filter_arguments,
    )

    return wrangler


# }}}


# {{{ compare_global_finite_difference


def compare_global_finite_difference(
    actx,
    p,
    checkpoint,
    *,
    eps: float,
    mode: str,
    sigma: float | None,
    single: bool | None = None,
    visualize: bool = False,
):
    import extra_optim_data as eod

    if single is None:
        single = p.ambient_dim == 3

    if sigma is None:
        sigma = 5.0e-2 if p.ambient_dim == 2 else 7.5e-1

    key = f"nelements_{p.resolution:03d}_eps_{eps:.0e}_sigma_{sigma:.1e}"

    # {{{ geometry

    places = eod.get_geometry_collection_from_param(actx, p, qbx=mode == "stokes")
    srcdesc = places.auto_source
    tgtdesc = srcdesc.copy(geometry="desired")

    discr = places.get_discretization(srcdesc.geometry, srcdesc.discr_stage)
    logger.info("nelements: %d", discr.mesh.nelements)
    logger.info("ndofs:     %d", discr.ndofs)

    # }}}

    # {{{ optimization problem

    if mode == "stokes":
        wrangler = make_stokes_wrangler(
            actx, p, places, srcdesc=srcdesc, tgtdesc=tgtdesc
        )
    elif mode == "shape":
        wrangler = make_geometry_wrangler(
            actx, p, places, srcdesc=srcdesc, tgtdesc=tgtdesc
        )
    else:
        raise ValueError(f"unrecognized mode: '{mode}'")

    state = wrangler.get_initial_state(actx)

    # }}}

    # {{{ output

    from pystopt.paths import get_filename

    visualize_file_name = (
        get_filename("visualize", cwd=p.dirname) if visualize else None
    )

    vis = None
    if visualize:
        from pystopt.visualization import make_visualizer

        vis = make_visualizer(actx, discr)

    # }}}

    # {{{ adjoint gradient

    grad_ex = state.evaluate_shape_gradient()
    grad_dot_n_ex = bind(
        places, sym.n_dot(places.ambient_dim), auto_where=wrangler.dofdesc
    )(actx, x=grad_ex)

    if visualize:
        filename = visualize_file_name.with_suffix(f"{key}_adjoint")
        vis.write_file(
            filename,
            [
                ("g_n", grad_dot_n_ex),
            ],
            overwrite=True,
            add_legend=False,
        )

    # }}}

    # {{{ finite difference

    if single:
        from pystopt.finite import generate_random_dofs

        rng = np.random.default_rng(42)
        indices = generate_random_dofs(discr, 10, rng=rng)

        logger.info("indices: %s", indices)
    else:
        indices = None

    from pystopt.finite import compare_finite_difference_shape_gradient

    r = compare_finite_difference_shape_gradient(
        state, grad=grad_dot_n_ex, eps=eps, sigma=sigma, indices=indices
    )

    logger.info("sigma:     %.5e", sigma)
    logger.info("grad:  eps %.5e error %.5e", r.eps, r.error)

    # }}}

    # {{{ output

    from pystopt.checkpoint.hdf import array_context_for_pickling

    with array_context_for_pickling(actx):
        from pystopt.tools import dc_asdict

        checkpoint.write(
            key,
            {
                "discr": discr,
                "grad_ex": grad_dot_n_ex,
                "grad_ad": r.grad_ad,
                "grad_fd": r.grad_fd,
                "eps": eps,
                "hmax": r.hmax,
                "sigma": sigma,
                "indices": indices,
                "parameters": dc_asdict(p),
            },
            overwrite=True,
        )

    if visualize and indices is None:
        error = actx.np.log10(actx.np.abs(r.grad_ad - r.grad_fd) + 1.0e-16)

        filename = visualize_file_name.with_suffix(f"{key}_gradient")
        vis.write_file(
            filename,
            [
                ("g_{AD}", r.grad_ad),
                ("g_{FD}", r.grad_fd),
            ],
            overwrite=True,
        )

        filename = visualize_file_name.with_suffix(f"{key}_error")
        vis.write_file(filename, [("error", error)], overwrite=True, add_legend=False)

    # }}}

    return r


# }}}


# {{{ single run


def run_shape(
    ctx_factory_or_actx,
    *,
    ambient_dim: int = 2,
    eps: float = 1.0e-4,
    sigma: float | None = None,
    single: bool | None = None,
    suffix: str = "v0",
    visualize: bool = True,
):
    actx = get_cl_array_context(ctx_factory_or_actx)

    if ambient_dim == 2:
        from extra_mesh_data import ShapeCircleParameters_2021_05_29 as Parameters
    elif ambient_dim == 3:
        from extra_mesh_data import ShapeSphereParameters_2021_05_29 as Parameters
    else:
        raise ValueError(f"unsupported dimension: '{ambient_dim}'")

    import extra_mesh_data as emd

    p = emd.make_param_from_class(
        Parameters, name="shape-gradient", suffix=suffix, overwrite=True
    )
    logger.info("\n%s", p)

    from pystopt.checkpoint import make_hdf_checkpoint

    checkpoint = make_hdf_checkpoint(p.checkpoint_file_name)

    compare_global_finite_difference(
        actx,
        p,
        checkpoint,
        eps=eps,
        sigma=sigma,
        single=single,
        mode="shape",
        visualize=visualize,
    )


def run_stokes(
    ctx_factory_or_actx,
    *,
    ambient_dim: int = 2,
    eps: float = 1.0e-4,
    sigma: float | None = None,
    single: bool | None = None,
    suffix: str = "v0",
    visualize: bool = True,
):
    actx = get_cl_array_context(ctx_factory_or_actx)

    if ambient_dim == 2:
        from extra_mesh_data import StokesEllipseFD2021_05_13 as Parameters
    elif ambient_dim == 3:
        from extra_mesh_data import StokesSphereFD2021_05_13 as Parameters
    else:
        raise ValueError(f"unsupported dimension: '{ambient_dim}'")

    p = Parameters(name="stokes-gradient")
    import extra_mesh_data as emd

    p = emd.make_param_from_class(
        Parameters,
        name="stokes-gradient",
        suffix=suffix,
        overwrite=True,
        viscosity_ratio=1.0,
        capillary_number=0.05,
    )
    logger.info("\n%s", p)

    from pystopt.checkpoint import make_hdf_checkpoint

    checkpoint = make_hdf_checkpoint(p.checkpoint_file_name)

    compare_global_finite_difference(
        actx,
        p,
        checkpoint,
        eps=eps,
        sigma=sigma,
        single=single,
        mode="stokes",
        visualize=visualize,
    )


# }}}


# {{{ convergence


def run_convergence(
    ctx_factory_or_actx,
    p,
    *,
    mode: str,
    ambient_dim: int = 2,
    eps: float = 1.0e-8,
    vs: str = "eps",
    sigma: float | None = None,
    single: bool | None = True,
    qbx: bool = False,
    visualize: bool = True,
):
    actx = get_cl_array_context(ctx_factory_or_actx)
    logger.info("\n%s", p)

    from pystopt.checkpoint import make_hdf_checkpoint

    checkpoint = make_hdf_checkpoint(p.checkpoint_file_name)

    # {{{ convergence

    if single is None:
        single = p.ambient_dim == 3

    if sigma is None:
        sigma = 5.0e-2 if p.ambient_dim == 2 else 1.0e-1

    from pystopt.measure import EOCRecorder, stringify_eoc

    eoc = EOCRecorder(name="grad", expected_order=1.0)

    if vs == "eps":
        if ambient_dim == 2:
            exp = np.arange(2, 10)
        elif ambient_dim == 3:
            exp = np.arange(2, 7)
        else:
            raise ValueError(f"unsupported dimension: {ambient_dim}")

        for i in range(exp.size):
            r = compare_global_finite_difference(
                actx,
                p,
                checkpoint,
                mode=mode,
                eps=10.0 ** -exp[i],
                sigma=sigma,
                single=single,
                visualize=visualize,
            )

            eoc.add_data_point(r.eps, r.error)
    elif vs == "resolution":
        if ambient_dim == 2:
            resolutions = np.array([32, 48, 64, 80, 96, 128])
        elif ambient_dim == 3:
            resolutions = np.array([16, 24, 32, 40])
        else:
            raise ValueError(f"unsupported dimension: {ambient_dim}")

        from dataclasses import replace

        for i in range(resolutions.size):
            p = replace(p, resolution=resolutions[i])
            r = compare_global_finite_difference(
                actx,
                p,
                checkpoint,
                mode=mode,
                eps=eps,
                sigma=sigma,
                single=single,
                visualize=visualize,
            )

            eoc.add_data_point(r.hmax, r.error)
    else:
        raise ValueError(f"unknown value for 'vs': '{vs}'")

    logger.info("error\n%s", stringify_eoc(eoc))

    # }}}

    # {{{ plot

    if not visualize:
        return

    from pystopt.paths import get_filename

    visualize_file_name = get_filename("visualize", cwd=p.dirname)

    from pystopt.measure import visualize_eoc

    filename = visualize_file_name("convergence")
    visualize_eoc(filename, eoc, abscissa=r"\epsilon", overwrite=True)

    # }}}


def run_shape_convergence(
    ctx_factory_or_actx,
    *,
    ambient_dim: int = 2,
    vs: str = "eps",
    suffix: str = "v0",
    overwrite: bool = True,
    visualize: bool = True,
):
    actx = get_cl_array_context(ctx_factory_or_actx)

    if ambient_dim == 2:
        from extra_mesh_data import ShapeCircleParameters_2021_05_29 as Parameters
    elif ambient_dim == 3:
        from extra_mesh_data import ShapeSphereParameters_2021_05_29 as Parameters
    else:
        raise ValueError(f"unsupported dimension: '{ambient_dim}'")

    import extra_mesh_data as emd

    p = emd.make_param_from_class(
        Parameters,
        name=f"shape-convergence-{vs}",
        suffix=suffix,
        overwrite=overwrite,
    )

    run_convergence(
        actx,
        p,
        vs=vs,
        eps=1.0e-7,
        sigma=None,
        single=None,
        mode="shape",
        visualize=visualize,
    )


def run_stokes_convergence(
    ctx_factory_or_actx,
    *,
    ambient_dim: int = 2,
    vs: str = "eps",
    suffix: str = "v0",
    overwrite: bool = True,
    visualize: bool = True,
):
    actx = get_cl_array_context(ctx_factory_or_actx)

    if ambient_dim == 2:
        from extra_mesh_data import StokesEllipseFD2021_05_13 as Parameters
    elif ambient_dim == 3:
        from extra_mesh_data import StokesSphereFD2021_05_13 as Parameters
    else:
        raise ValueError(f"unsupported dimension: '{ambient_dim}'")

    import extra_mesh_data as emd

    p = emd.make_param_from_class(
        Parameters,
        name=f"stokes-convergence-{vs}",
        suffix=suffix,
        overwrite=overwrite,
        viscosity_ratio=1.0,
        capillary_number=np.inf,
    )

    run_convergence(
        actx,
        p,
        vs=vs,
        eps=1.0e-8,
        sigma=None,
        single=True,
        mode="stokes",
        visualize=visualize,
    )


# }}}


if __name__ == "__main__":
    import sys

    import pyopencl as cl

    if len(sys.argv) > 1:
        exec(sys.argv[1])
    else:
        run_shape(cl._csc)
