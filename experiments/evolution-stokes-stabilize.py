# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

from dataclasses import dataclass, field
from typing import Any

import numpy as np

from pystopt import bind, sym
from pystopt.simulation.unsteady import (
    StokesEvolutionCallbackManager as CallbackManager,
)
from pystopt.simulation.unsteady import (
    StokesEvolutionHistoryCallback,
    StokesEvolutionVisualizeCallback,
    StokesEvolutionWrangler,
)
from pystopt.tools import get_cl_array_context, get_default_logger

logger = get_default_logger(module=__file__)


# {{{ callbacks


@dataclass(frozen=True)
class HistoryCallback(StokesEvolutionHistoryCallback):
    h_max: list[float] = field(default_factory=list, repr=False)
    h_mean: list[float] = field(default_factory=list, repr=False)
    h_std: list[float] = field(default_factory=list, repr=False)

    def __call__(self, *args, **kwargs):
        ret = super().__call__(*args, **kwargs)

        (state,) = args

        places = state.get_geometry_collection()
        dofdesc = state.wrangler.dofdesc
        actx = state.array_context

        # {{{

        from arraycontext import flatten

        h = bind(
            places,
            sym.element_length(places.ambient_dim, dofdesc=dofdesc),
            auto_where=dofdesc,
        )(actx)
        h = actx.to_numpy(flatten(h, actx))

        self.h_max.append(np.max(h))
        self.h_mean.append(np.mean(h))
        self.h_std.append(np.std(h, ddof=1))

        # }}}

        logger.info(
            "volume %.5e / %.5e area %.5e / %.5e h_mean %.5e ± %.5e",
            self.volume[-1],
            abs(self.volume[-1] - self.volume[0]) / self.volume[0],
            self.area[-1],
            abs(self.area[-1] - self.area[0]) / self.area[0],
            self.h_mean[-1],
            self.h_std[-1],
        )

        return ret


@dataclass(frozen=True)
class VisualizeCallback(StokesEvolutionVisualizeCallback):
    def get_geometry_kwargs(self, state):
        return {
            "xlim": [-1.25, 3.25],
            "ylim": [-1.25, 1.25],
            "show_vertices": True,
            "aspect": "equal",
        }


# }}}


# {{{ run


def run(
    ctx_factory,
    ambient_dim: int = 2,
    *,
    extra_kwargs: dict[str, Any] | None = None,
    suffix: str = "v0",
    skip_if_exists: bool = False,
    visualize: bool = True,
    overwrite: bool = True,
) -> None:
    if extra_kwargs is None:
        extra_kwargs = {}

    actx = get_cl_array_context(ctx_factory)

    # {{{ parameters

    if ambient_dim == 2:
        from extra_mesh_data import StokesStabilizedEvolution2d_2021_12_31 as Parameters
    elif ambient_dim == 3:
        from extra_mesh_data import StokesStabilizedEvolution3d_2021_12_31 as Parameters
    else:
        raise ValueError(f"unsupported dimension: {ambient_dim}")

    import extra_mesh_data as emd

    p = emd.make_param_from_class(
        Parameters,
        name="evo-stabs",
        suffix=suffix,
        overwrite=overwrite,
        **extra_kwargs,
    )
    assert p.ambient_dim == ambient_dim

    if skip_if_exists and p.checkpoint_file_name.exists():
        return

    logger.info("\n%s", p)

    # }}}

    # {{{ geometry

    import extra_optim_data as eod

    places = eod.get_geometry_collection_from_param(actx, p, qbx=True)
    dofdesc = places.auto_source

    discr = places.get_discretization(dofdesc.geometry, dofdesc.discr_stage)
    logger.info("nspec:     %d", discr.nspec)
    logger.info("ndofs:     %d", discr.ndofs)
    logger.info("nelements: %d", discr.mesh.nelements)

    # }}}

    # {{{ stokes

    from pystopt import stokes

    bc = stokes.get_farfield_boundary_from_name(
        p.ambient_dim, p.farfield_name, **p.farfield_arguments
    )
    op = stokes.TwoPhaseSingleLayerStokesRepresentation(bc)

    from pystopt.evolution.stokes import StokesEvolutionOperator

    evolution = StokesEvolutionOperator(
        force_normal_velocity=p.force_normal_velocity,
        force_tangential_velocity=p.force_tangential_velocity,
    )

    # }}}

    # {{{ output

    if visualize:
        visualize_callback_factory = VisualizeCallback
    else:
        visualize_callback_factory = False

    from pystopt.callbacks import make_default_evolution_callback
    from pystopt.dof_array import dof_array_norm

    output = make_default_evolution_callback(
        p.checkpoint_file_name,
        manager_factory=CallbackManager,
        history_callback_factory=HistoryCallback(norm=dof_array_norm),
        visualize_callback_factory=visualize_callback_factory,
        norm=dof_array_norm,
        overwrite=overwrite,
    )

    wrangler = StokesEvolutionWrangler(
        # geometry
        places=places,
        dofdesc=dofdesc,
        is_spectral=True,
        # stokes
        op=op,
        lambdas=p.stokes_lambdas,
        context=p.stokes_arguments,
        gmres_arguments={"rtol": 1.0e-8},
        # evolution
        evolution=evolution,
        stabilizer_name=p.stabilizer_name,
        stabilizer_arguments=p.stabilizer_arguments,
        # filtering
        filter_type=p.filter_type,
        filter_arguments=p.filter_arguments,
    )

    # }}}

    # {{{ time stepping

    from pystopt.evolution.estimates import fixed_time_step_from

    maxit, tmax, timestep = fixed_time_step_from(
        maxit=5,
        tmax=None,
        timestep=p.timestep,
        # maxit=p.maxit, tmax=p.tmax, timestep=p.timestep,
    )

    logger.info("tmax:  %g", tmax)
    logger.info("maxit: %d", maxit)
    logger.info("dt:    %.5e", timestep)

    from pystopt.evolution import make_adjoint_time_stepper

    state0 = wrangler.get_initial_state(actx)
    stepper = make_adjoint_time_stepper(
        p.time_step_method,
        "state",
        state0,
        wrangler.evolution_source_term,
        dt_start=p.timestep,
        t_start=0.0,
    )

    from pystopt.checkpoint.hdf import array_context_for_pickling

    with array_context_for_pickling(actx):
        for event in stepper.run(tmax=tmax, maxit=maxit):
            ret = output(event.state_component, event=event)

            if ret == 0:
                from pystopt.tools import c

                logger.info(c.warn("Stopped by callback..."))
                break

        checkpoint = output["checkpoint"].checkpoint
        history = output["history"].to_numpy()

        from pystopt.tools import dc_asdict

        checkpoint.write_to("parameters", dc_asdict(p), overwrite=True)
        checkpoint.write_to("orig_discr", discr, overwrite=True)
        checkpoint.write_to("history", history, overwrite=True)

        checkpoint.done()

    plot_evolution_history(actx, p.checkpoint_file_name)

    # }}}


def run_all_stabs(
    ctx_factory,
    ambient_dim: int = 2,
    *,
    suffix: str = "v0",
    visualize: bool = True,
    overwrite: bool = True,
) -> None:
    actx = get_cl_array_context(ctx_factory)

    stabilizers = []
    stabilizers.append({
        "name": "full",
        "tangential_forcing_arguments": {},
        "force_normal_velocity": False,
        "force_tangential_velocity": False,
    })
    stabilizers.append({
        "name": "normal",
        "tangential_forcing_arguments": {},
        "force_normal_velocity": True,
        "force_tangential_velocity": False,
    })

    if ambient_dim == 2:
        # stabilizers.append({
        #     "name": "loewenberg2001",
        #     "tangential_forcing_arguments": {
        #         "name": "loewenberg2001"},
        #     })
        stabilizers.append({
            "name": "kropinski2001_arc",
            "tangential_forcing_arguments": {
                "name": "kropinski2001",
                "alpha": 1,
                "beta": 0,
            },
        })
        # stabilizers.append({
        #     "name": "kropinski2001_kappa",
        #     "tangential_forcing_arguments": {
        #         "name": "kropinski2001", "alpha": 0.9, "beta": 0.1},
        #     })
        # stabilizers.append({
        #     "name": "zinchenko2002",
        #     "tangential_forcing_arguments": {
        #         "name": "zinchenko2002"},
        #     })
        stabilizers.append({
            "name": "zinchenko2013",
            "tangential_forcing_arguments": {"name": "zinchenko2013"},
        })
    elif ambient_dim == 3:
        stabilizers.append({
            "name": "zinchenko2013",
            "tangential_forcing_arguments": {
                "name": "zinchenko2013",
                "verbose": True,
                "optim_options": {
                    "method": "L-BFGS-B",
                    "maxiter": 128,
                    "disp": False,
                },
            },
        })
    else:
        raise ValueError(f"unsupported dimension: {ambient_dim}")

    for kwargs in stabilizers:
        extra_kwargs = kwargs.copy()
        name = extra_kwargs.pop("name")

        run(
            actx,
            ambient_dim=ambient_dim,
            extra_kwargs=extra_kwargs,
            suffix=f"{name}-{suffix}",
            skip_if_exists=True,
            visualize=visualize,
            overwrite=overwrite,
        )


# }}}


# {{{ plot


def plot_evolution_history(ctx_factory, filename):
    actx = get_cl_array_context(ctx_factory)

    import pathlib

    filename = pathlib.Path(filename)

    from pystopt.paths import get_filename

    dirname = filename.parent
    visualize_file_name = get_filename("history", cwd=dirname)

    # {{{ load

    from pystopt.checkpoint import make_hdf_checkpoint

    checkpoint = make_hdf_checkpoint(filename, mode="r")

    from pystopt.checkpoint.hdf import array_context_for_pickling

    with array_context_for_pickling(actx):
        history = checkpoint.read("history")

    # }}}

    # {{{ plot

    from pystopt.visualization.optimization import visualize_stokes_evolution_history

    visualize_stokes_evolution_history(visualize_file_name, history)

    from pystopt.visualization.matplotlib import subplots

    filename = visualize_file_name.with_suffix("element")
    with subplots(filename, overwrite=True) as fig:
        ax = fig.gca()

        ax.fill_between(
            history["time"],
            history["h_mean"] - history["h_std"],
            history["h_mean"] + history["h_std"],
            alpha=0.15,
        )
        ax.plot(history["time"], history["h_max"], label="$h_{max}$")
        ax.plot(history["time"], history["h_mean"], label="$h_{mean}$")
        ax.set_xlabel("$t$")
        ax.set_ylabel("$h$")
        ax.set_ylim([0.0, 0.5])
        ax.legend()

    # }}}


# }}}


if __name__ == "__main__":
    import sys

    import pyopencl as cl

    if len(sys.argv) > 1:
        exec(sys.argv[1])
    else:
        run(cl._csc)
