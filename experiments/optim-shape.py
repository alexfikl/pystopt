# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

import pathlib
from dataclasses import dataclass, field, replace

import numpy as np

import pystopt.callbacks as cb
from pystopt import bind, sym
from pystopt.simulation.unconstrained import (
    UnconstrainedShapeOptimizationCallbackManager,
    UnconstrainedShapeOptimizationVisualizeCallback,
)
from pystopt.tools import get_cl_array_context, get_default_logger

logger = get_default_logger(module=__file__)


# {{{ output


@dataclass
class CallbackManager(UnconstrainedShapeOptimizationCallbackManager):
    def get_output_field_getters(self):
        from pystopt.simulation.common import get_output_fields_modes

        return (
            *super().get_output_field_getters(),
            get_output_fields_modes,
        )


@dataclass(frozen=True)
class ModesVisualizeCallback(UnconstrainedShapeOptimizationVisualizeCallback):
    plot_modes: bool = False

    def get_geometry_kwargs(self, state):
        return {"xlim": [-2.25, 2.25], "ylim": [-2.25, 2.25]}

    def get_modes_kwargs(self, state):
        return {"mode": "linen", "semilogy": True, "output": "error"}

    def get_file_writers(self):
        from pystopt.simulation.common import (
            visualize_fourier_mode_writer,
            visualize_spharm_mode_writer,
        )

        writers = super().get_file_writers()
        if self.plot_modes:
            if self.ambient_dim == 2:
                writers += (visualize_fourier_mode_writer,)
            else:
                writers += (visualize_spharm_mode_writer,)

        return writers


@dataclass(frozen=True)
class HistoryCallback(cb.OptimizationHistoryCallback):
    # CG step estimates
    dogan: list[float] = field(default_factory=list, repr=False)
    feppon: list[float] = field(default_factory=list, repr=False)
    # Stokes time step estimates
    brackbill: list[float] = field(default_factory=list, repr=False)
    loewenberg: list[float] = field(default_factory=list, repr=False)

    def __call__(self, *args, **kwargs):
        info = kwargs.get("info")
        if info is not None:
            if info.it == 0 and len(self.f) > 0:
                return 1

            super().__call__(*args, **kwargs)

        (state,) = args

        dofdesc = state.dofdesc
        places = state.places
        actx = state.array_context

        # {{{ cg

        g_dot_n = kwargs["fields"]["g_dot_n"].value

        import pystopt.optimize.linesearch as ls

        for ary, func in [
            (self.dogan, ls.estimate_line_search_step_dogan),
            (self.feppon, ls.estimate_line_search_step_feppon),
        ]:
            sym_dt = func(places.ambient_dim, sym.var("g"))
            dt = bind(places, sym_dt, auto_where=dofdesc)(actx, g=g_dot_n)

            ary.append(actx.to_numpy(dt))

        # }}}

        # {{{ stokes

        from pystopt.evolution.estimates import capillary_time_step_estimation

        for ary, name in [
            (self.brackbill, "brackbill"),
            (self.loewenberg, "loewenberg"),
        ]:
            dt = capillary_time_step_estimation(
                actx,
                places,
                method=name,
                velocity=kwargs["fields"]["g"].value,
                capillary_number=1,
                viscosity_ratio=1,
            )
            ary.append(dt)

        # }}}

        return 1


# }}}


# {{{ run


def run(
    ctx_factory_or_actx,
    *,
    ambient_dim: int = 2,
    from_restart: bool | int = False,
    from_restart_filename: str | None = None,
    suffix: str = "v0",
    overwrite: bool = True,
    visualize: bool = True,
):
    actx = get_cl_array_context(ctx_factory_or_actx)

    # {{{ parameters

    if ambient_dim == 2:
        from extra_mesh_data import ShapeCircleParameters_2021_09_05 as Parameters
    elif ambient_dim == 3:
        from extra_mesh_data import ShapeSphereParameters_2021_09_05 as Parameters
    else:
        raise ValueError(f"unsupported dimension: '{ambient_dim}'")

    import extra_mesh_data as emd

    p = emd.make_param_from_class(
        Parameters,
        name="optim-shape",
        from_restart_filename=from_restart_filename,
        suffix=suffix,
        overwrite=overwrite,
    )
    assert p.ambient_dim == ambient_dim

    # }}}

    # {{{ geometry

    import extra_optim_data as eod

    places = eod.get_geometry_collection_from_param(actx, p, qbx=False)
    srcdesc = places.auto_source
    tgtdesc = srcdesc.copy(geometry="desired")

    discr = places.get_discretization(srcdesc.geometry, srcdesc.discr_stage)

    # }}}

    # {{{ optimization problem

    cost, context = eod.make_shape_cost_functional(
        actx, places, p, source_dd=srcdesc, desire_dd=tgtdesc
    )

    from pystopt.simulation.unconstrained import UnconstrainedShapeOptimizationWrangler

    wrangler = UnconstrainedShapeOptimizationWrangler(
        places=places,
        dofdesc=srcdesc,
        is_spectral=True,
        cost=cost,
        context=context,
        filter_type=p.filter_type,
        filter_arguments=p.filter_arguments,
    )

    # }}}

    # {{{ output

    visualize_callback_factory = visualize
    if visualize:
        visualize_callback_factory = ModesVisualizeCallback
    else:
        visualize_callback_factory = False

    from pystopt.dof_array import dof_array_norm

    callback = cb.make_default_shape_callback(
        p.checkpoint_file_name,
        manager_factory=CallbackManager,
        visualize_callback_factory=visualize_callback_factory,
        history_callback_factory=HistoryCallback,
        norm=dof_array_norm,
        from_restart=from_restart,
        overwrite=overwrite,
    )
    checkpoint = callback["checkpoint"].checkpoint

    logger.info("\n%s", str(p))

    import os

    logger.info("=" * 48)
    logger.info("Send SIGUSR1 to PID '%s' to stop optimization", os.getpid())

    from pystopt.paths import relative_to

    logger.info("Stop file '%s'", relative_to(callback.stop_file_name))
    if from_restart:
        logger.info("Restarted from '%s'", relative_to(p.checkpoint_file_name))
    logger.info("=" * 48)

    logger.info("nelements: %d", discr.mesh.nelements)
    logger.info("ndofs:     %d", discr.ndofs)
    logger.info("nspec:     %d", discr.nspec)

    # }}}

    # {{{ initial state

    from pystopt.checkpoint.hdf import array_context_for_pickling

    state0 = wrangler.get_initial_state(actx)

    if from_restart:
        from pystopt.checkpoint import get_result_from_checkpoint

        with array_context_for_pickling(actx):
            result = get_result_from_checkpoint(
                checkpoint, from_restart=from_restart - 1
            )

            state0 = replace(state0, x=actx.thaw(result))

        # TODO: reset rtol and ftol so that they match

    if visualize and ambient_dim == 3:
        from pystopt.visualization import make_visualizer

        discr_d = places.get_discretization(tgtdesc.geometry, tgtdesc.discr_stage)
        vis = make_visualizer(actx, discr_d)

        from pystopt.paths import get_filename

        filename = get_filename("visualize", suffix="desired", cwd=p.dirname)
        vis.write_file(filename, [], overwrite=overwrite)

    # }}}

    # {{{ optimize

    from pystopt.optimize import CartesianEucledeanSpace

    manifold = CartesianEucledeanSpace(vdot=wrangler.vdot)

    from pystopt.optimize import get_line_search_from_name

    linesearch = get_line_search_from_name(
        p.cg_linesearch_name, fun=lambda x: x.cost, **p.cg_linesearch_arguments
    )

    from pystopt.optimize import get_descent_direction_from_name

    descent = get_descent_direction_from_name(
        p.cg_descent_name, **p.cg_descent_arguments
    )

    options = {
        **p.cg_arguments,
        "linesearch": linesearch,
        "descent": descent,
        "manifold": manifold,
    }

    from pystopt.optimize.steepest import minimize

    with array_context_for_pickling(actx):
        r = minimize(
            fun=lambda x: x.cost,
            x0=state0,
            jac=lambda x: x.wrap(x.gradient),
            funjac=lambda x: (x.cost, x.wrap(x.gradient)),
            callback=callback,
            options=options,
        )

    # }}}

    # {{{ save results

    if from_restart:
        from pystopt.optimize.cg_utils import combine_cg_results

        with array_context_for_pickling(actx):
            rprev = checkpoint.read_from("result")

        r = combine_cg_results(r, rprev, n=max(0, from_restart - 1))

    logger.info("result:\n%s", r.pretty())

    with array_context_for_pickling(actx):
        from pystopt.tools import dc_asdict

        checkpoint.write_to("parameters", dc_asdict(p), overwrite=True)
        checkpoint.write_to("callback", callback, overwrite=True)

        r = replace(r, x=r.x)
        checkpoint.write_to("result", r, overwrite=True)

        checkpoint.done()

    # }}}

    plot_optimization_convergence(actx, p.checkpoint_file_name, overwrite=overwrite)


# }}}


# {{{ plot


def plot_optimization_convergence(ctx_factory, filename, *, overwrite=True):
    actx = get_cl_array_context(ctx_factory)

    filename = pathlib.Path(filename)
    dirname = filename.parent

    from pystopt.paths import get_filename

    visualize_file_name = get_filename("history", cwd=dirname)

    from pystopt.checkpoint import make_hdf_checkpoint

    checkpoint = make_hdf_checkpoint(filename)

    from pystopt.checkpoint.hdf import array_context_for_pickling

    with array_context_for_pickling(actx):
        history = checkpoint.read("callback/callbacks/history")
    del checkpoint

    from pystopt.visualization.optimization import visualize_optimization_history

    visualize_optimization_history(visualize_file_name, history, overwrite=overwrite)

    # {{{ step estimates

    from pystopt.visualization.matplotlib import subplots

    filename = visualize_file_name.with_suffix("alpha")
    with subplots(filename, overwrite=True) as fig:
        ax = fig.gca()

        alpha_max = 100
        ax.semilogy(np.minimum(history.alpha, alpha_max), label="Simulation")
        ax.semilogy(np.minimum(history.dogan, alpha_max), label="Doǧan")
        ax.semilogy(np.minimum(history.feppon, alpha_max), label="Feppon")
        ax.semilogy(np.minimum(history.brackbill, alpha_max), label="Brackbill")
        ax.semilogy(np.minimum(history.loewenberg, alpha_max), label="Loewenberg")

        ax.set_xlabel("$Iteration$")
        ax.set_ylabel(r"$\alpha$")
        ax.legend()

    # }}}


# }}}


if __name__ == "__main__":
    import sys

    import pyopencl as cl

    if len(sys.argv) > 1:
        exec(sys.argv[1])
    else:
        run(cl._csc)
