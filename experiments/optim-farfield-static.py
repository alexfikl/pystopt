# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

from dataclasses import dataclass, field, replace
from typing import Any

import extra_mesh_data as emd
import numpy as np

from arraycontext import ArrayContext
from pytential.collection import GeometryCollection
from pytools import memoize_method

from pystopt import bind, sim, sym
from pystopt.cost import ShapeFunctional
from pystopt.stokes import StokesSolution, TwoPhaseSingleLayerStokesRepresentation
from pystopt.tools import get_cl_array_context, get_default_logger

logger = get_default_logger(module=__file__)


# {{{ wrangler


def get_sym_gradient(wrangler: "StaticFarfieldWrangler") -> sym.Expression:
    ambient_dim = wrangler.ambient_dim
    dim = ambient_dim - 1

    dofdesc = wrangler.dofdesc
    duinf = 0
    dvinf = 0

    from pystopt import grad

    if wrangler.cost is not None:
        du = grad.velocity(wrangler.cost, ambient_dim, dofdesc=dofdesc)
    ustar = sym.make_sym_vector("ustar", ambient_dim)
    vlambda = sym.var("viscosity_ratio")

    if wrangler.farfield_name == "uniform":
        # {{{ uniform

        if wrangler.cost is not None:
            duinf += sym.sintegral(du[0], ambient_dim, dim, dofdesc=dofdesc)
            dvinf += sym.sintegral(du[1], ambient_dim, dim, dofdesc=dofdesc)

        # }}}

    elif wrangler.farfield_name == "solid_body_rotation":
        # {{{ solid body rotation

        if wrangler.cost is not None:
            x = sym.nodes(ambient_dim, dofdesc=dofdesc).as_vector()

            duinf += sym.sintegral(-du[0] * x[1], ambient_dim, dim, dofdesc=dofdesc)
            dvinf += sym.sintegral(du[1] * x[0], ambient_dim, dim, dofdesc=dofdesc)

        n = sym.normal(ambient_dim, dofdesc=dofdesc).as_vector()
        ustar_dot_n = -(1 - vlambda) * (ustar[0] * n[1] + ustar[1] * n[0])

        duinf += sym.sintegral(-ustar_dot_n, ambient_dim, dim, dofdesc=dofdesc)
        dvinf += sym.sintegral(+ustar_dot_n, ambient_dim, dim, dofdesc=dofdesc)

        # }}}
    else:
        raise ValueError(f"unknown farfield: '{wrangler.farfield_name}'")

    from pytools.obj_array import make_obj_array

    return make_obj_array([duinf, dvinf])


@dataclass(frozen=True)
class StaticFarfieldWrangler:
    # geometry
    places: GeometryCollection
    dofdesc: sym.DOFDescriptor

    # stokes
    op: TwoPhaseSingleLayerStokesRepresentation
    lambdas: dict[str, float]
    gmres_arguments: dict[str, Any]
    farfield_name: str

    # optim
    cost: ShapeFunctional

    context: dict[str, Any]

    @property
    def ambient_dim(self):
        return self.places.ambient_dim

    def get_initial_state(self, actx: ArrayContext) -> "StaticFarfieldState":
        from pytools.obj_array import make_obj_array

        return StaticFarfieldState(
            x=make_obj_array(self.context["uinf"]), wrangler=self, array_context=actx
        )

    @property
    @memoize_method
    def ad(self):
        from pystopt.stokes import make_static_adjoint_boundary_conditions

        bc = make_static_adjoint_boundary_conditions(self.ambient_dim, self.cost)

        return TwoPhaseSingleLayerStokesRepresentation(bc)

    def solve_stokes(
        self,
        actx: ArrayContext,
        state: "StaticFarfieldState",
        op: TwoPhaseSingleLayerStokesRepresentation,
        context: dict[str, Any],
    ) -> StokesSolution:
        context = {**self.context, **context, "uinf": state.x}

        from pystopt.stokes import single_layer_solve

        r = single_layer_solve(
            actx,
            self.places,
            op,
            gmres_arguments=self.gmres_arguments,
            lambdas=self.lambdas,
            sources=[self.dofdesc],
            context=context,
        )

        return r

    def evaluate_cost(
        self,
        actx: ArrayContext,
        state: "StaticFarfieldState",
        *,
        context: dict[str, Any],
    ) -> float:
        context = {**self.context, **context, "uinf": state.x}

        r = bind(
            self.places,
            self.cost(self.ambient_dim, dofdesc=self.dofdesc),
        )(actx, **context)

        return actx.to_numpy(r)

    def evaluate_gradient(
        self,
        actx: ArrayContext,
        state: "StaticFarfieldState",
        *,
        context: dict[str, Any],
    ) -> np.ndarray:
        context = {**self.context, **context, "uinf": state.x}

        r = bind(
            self.places,
            get_sym_gradient(self),
        )(actx, **context)

        return actx.to_numpy(r)


@sim.as_state_container
class StaticFarfieldState:
    x: np.ndarray
    wrangler: StaticFarfieldWrangler
    array_context: ArrayContext

    # {{{ stokes

    @property
    @memoize_method
    def _stokes_solution(self):
        return self.wrangler.solve_stokes(
            self.array_context, self, self.wrangler.op, context={}
        )

    @property
    @memoize_method
    def _adjoint_solution(self):
        r = self.wrangler.solve_stokes(
            self.array_context,
            self,
            self.wrangler.ad,
            context={"q": self.density, "u": self.velocity},
        )

        return replace(r, density_name="qstar")

    @property
    @memoize_method
    def velocity(self):
        from pystopt.stokes import sti

        return sti.velocity(
            self._stokes_solution,
            side=None,
            qbx_forced_limit=+1,
            dofdesc=self.wrangler.dofdesc,
        )

    @property
    @memoize_method
    def adjoint_velocity(self):
        from pystopt.stokes import sti

        return sti.velocity(
            self._adjoint_solution,
            side=None,
            qbx_forced_limit=+1,
            dofdesc=self.wrangler.dofdesc,
        )

    @property
    def density(self):
        return self._stokes_solution.density

    @property
    def adjoint_density(self):
        return self._adjoint_solution.density

    # }}}

    # {{{ optim

    @property
    @memoize_method
    def cost(self):
        return self.wrangler.evaluate_cost(
            self.array_context,
            self,
            context={
                "q": self.density,
                "u": self.velocity,
            },
        )

    @property
    @memoize_method
    def gradient(self):
        return self.wrangler.evaluate_gradient(
            self.array_context,
            self,
            context={
                "q": self.density,
                "u": self.velocity,
                "qstar": self.adjoint_density,
                "ustar": self.adjoint_velocity,
            },
        )

    @property
    def cost_gradient(self) -> tuple[float, np.ndarray]:
        return self.cost, self.gradient

    # }}}


# }}}


# {{{ run


@dataclass(frozen=True)
class Parameters(emd.ExampleParameters):
    ambient_dim: int = 2

    # mesh
    mesh_name: str = "fourier_ellipse"
    resolution: int = 256
    target_order: int = 4
    source_ovsmp: int = 6
    qbx_order: int = 4

    # stokes
    viscosity_ratio: float = 1.0
    capillary_number: float = 0.1
    farfield_name: str = "solid_body_rotation"
    farfield_arguments: dict[str, Any] = field(
        default_factory=lambda: {"uinf": np.array([1.0, 2.0])}
    )

    # cost
    cost_name: str = "steady"
    cost_arguments: dict[str, Any] = field(
        default_factory=lambda: {
            "gamma": 0,
        }
    )


def run(
    ctx_factory_or_actx,
    *,
    ambient_dim: int = 2,
    viscosity_ratio: float = 1.0,
    capillary_number: float = 0.1,
    visualize: bool = True,
) -> None:
    actx = get_cl_array_context(ctx_factory_or_actx)
    p = emd.make_param_from_class(
        Parameters,
        name="farfield",
        viscosity_ratio=viscosity_ratio,
        capillary_number=capillary_number,
    )

    logger.info("\n%s", p)

    # {{{ geometry

    import extra_optim_data as eod

    places = eod.get_geometry_collection_from_param(actx, p, qbx=True)
    dofdesc = places.auto_source

    discr = places.get_discretization(dofdesc.geometry, dofdesc.discr_stage)
    logger.info("nspec:     %d", discr.nspec)
    logger.info("ndofs:     %d", discr.ndofs)
    logger.info("nelements: %d", discr.mesh.nelements)

    # }}}

    # {{{ wrangler

    import pystopt.cost as pc

    cost = pc.NormalVelocityFunctional(u=sym.make_sym_vector("u", ambient_dim), ud=0)

    import pystopt.stokes as stk

    context = p.stokes_arguments.copy()
    context["uinf"] = p.farfield_arguments["uinf"]

    farfield_arguments = {}
    farfield_arguments["omega"] = sym.make_sym_vector("uinf", ambient_dim)

    bc = stk.get_farfield_boundary_from_name(
        p.ambient_dim, p.farfield_name, **farfield_arguments
    )
    op = stk.TwoPhaseSingleLayerStokesRepresentation(bc)

    wrangler = StaticFarfieldWrangler(
        places=places,
        dofdesc=dofdesc,
        op=op,
        lambdas=p.stokes_lambdas,
        gmres_arguments=p.gmres_arguments,
        farfield_name=p.farfield_name,
        cost=cost,
        context=context,
    )
    state0 = wrangler.get_initial_state(actx)

    # }}}

    # {{{ finite difference

    from pystopt.measure import EOCRecorder, stringify_eoc

    eps = 10.0 ** -np.arange(2, 10)
    eoc = [EOCRecorder(name=r"u_{\infty}"), EOCRecorder(name=r"v_{\infty}")]

    from pytools import wandering_element

    dc_duinf_fd = np.empty(state0.x.size)
    dc_duinf_ad = state0.gradient

    for k in range(eps.size):
        for i, e_i in enumerate(wandering_element(state0.x.size)):
            state = replace(state0, x=state0.x + eps[k] * np.array(e_i))
            logger.info("%.2e %3d cost %s %.12e", eps[k], i, state.x, state.cost)

            dc_duinf_fd[i] = (state.cost - state0.cost) / eps[k]

            error = abs(dc_duinf_ad[i] - dc_duinf_fd[i]) / abs(dc_duinf_fd[i])
            eoc[i].add_data_point(eps[k], error)

        logger.info("grad: %s %s", dc_duinf_ad, dc_duinf_fd)

    logger.info(
        "\n%s", stringify_eoc(*eoc, abscissa_name=r"\epsilon", table_format="latex")
    )

    if not visualize:
        return

    from pystopt.measure import visualize_eoc

    visualize_eoc("optim_farfield_static_convergence", *eoc, order=1, overwrite=True)

    # }}}


# }}}


if __name__ == "__main__":
    import sys

    import pyopencl as cl

    if len(sys.argv) > 1:
        exec(sys.argv[1])
    else:
        run(cl._csc)
