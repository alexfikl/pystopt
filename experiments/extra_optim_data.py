# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

from typing import Any

import numpy as np

from pytools.obj_array import make_obj_array

from pystopt import bind, sym
from pystopt.tools import get_default_logger

logger = get_default_logger(__name__)


# {{{ mesh


def get_discretization_from_param(actx, param, mesh_name, mesh_arguments):
    from pystopt.mesh.generation import get_mesh_generator_class_from_name

    cls = get_mesh_generator_class_from_name(mesh_name)
    assert cls is not None, mesh_name

    from pystopt.mesh import generate_discretization

    gen = cls(**mesh_arguments)
    return generate_discretization(gen, actx, int(param.resolution))


def get_geometry_collection_from_param(actx, param, *, qbx: bool = True):
    discr = get_discretization_from_param(
        actx, param, param.mesh_name, param.mesh_arguments
    )
    if qbx:
        from pystopt.qbx import QBXLayerPotentialSource

        discr = QBXLayerPotentialSource(discr, **param.qbx_arguments)

    places = {param.mesh_name: discr}

    from pystopt.mesh.generation import get_mesh_generator_class_from_name

    cls = get_mesh_generator_class_from_name(param.desired_mesh_name)
    if cls is not None:
        discr_d = get_discretization_from_param(
            actx, param, param.desired_mesh_name, param.desired_mesh_arguments
        )

        if qbx:
            from pystopt.qbx import QBXLayerPotentialSource

            discr_d = QBXLayerPotentialSource(discr_d, **param.qbx_arguments)

        places["desired"] = discr_d

    from pytential import GeometryCollection

    places = GeometryCollection(places, auto_where=param.mesh_name)

    return places


# }}}


# {{{ shape cost functionals


def _normal_test_data(ambient_dim, to_pytential=None):
    import sympy as sp

    import pystopt.symbolic.sympy_interop as spi

    if to_pytential is None:
        to_pytential = spi.to_pytential

    x = spi.nodes(3)

    f = make_obj_array(
        [
            sp.cos(2 * np.pi * x[0]),
            sp.exp(x[1]),
            x[2] ** 3,
        ][:ambient_dim]
    )
    divf = spi.divergence(f)

    return (to_pytential(ambient_dim, f), to_pytential(ambient_dim, divf))


def _curvature_test_data(ambient_dim, to_pytential=None):
    import sympy as sp

    import pystopt.symbolic.sympy_interop as spi

    if to_pytential is None:
        to_pytential = spi.to_pytential

    x = spi.nodes(3)

    f = sum(
        [
            sp.cos(2 * np.pi * x[0]),
            sp.exp(x[1]),
            x[2] ** 3,
        ][:ambient_dim]
    )
    gradf = spi.gradient(ambient_dim, f)
    hessf = spi.hessian(ambient_dim, f)

    return (
        to_pytential(ambient_dim, f),
        to_pytential(ambient_dim, gradf),
        to_pytential(ambient_dim, hessf),
    )


def _curvature_vector_test_data(ambient_dim, to_pytential=None):
    import sympy as sp

    import pystopt.symbolic.sympy_interop as spi

    if to_pytential is None:
        to_pytential = spi.to_pytential

    x = spi.nodes(3)

    f = make_obj_array(
        [
            sp.cos(2 * np.pi * x[0]) * x[1],
            x[0] * sp.exp(x[1]),
            x[2] ** 3,
        ][:ambient_dim]
    )

    from itertools import product

    gradf = np.empty((ambient_dim, ambient_dim), dtype=object)
    for i, j in product(range(ambient_dim), repeat=2):
        gradf[i, j] = f[j].diff(x[i])

    hessf = np.empty((ambient_dim, ambient_dim, ambient_dim), dtype=object)
    for i, j, k in product(range(ambient_dim), repeat=3):
        hessf[i, j, k] = f[k].diff(x[i]).diff(x[j])

    return (
        to_pytential(ambient_dim, f),
        to_pytential(ambient_dim, gradf),
        to_pytential(ambient_dim, hessf),
    )


def _tangential_gradient_test_data(ambient_dim):
    return _curvature_test_data(ambient_dim)


def make_shape_cost_functional(
    actx, places, p, *, source_dd: sym.DOFDescriptor, desire_dd: sym.DOFDescriptor
):
    import pystopt.cost as pc

    if p.cost_name == "geometry_tracking":
        discr_d = places.get_discretization(desire_dd.geometry, desire_dd.discr_stage)
        context = {"xd": actx.thaw(discr_d.nodes())}

        cost = pc.GeometryTrackingFunctional(
            xd=sym.make_sym_vector("xd", places.ambient_dim),
            t=sym.var("t"),
            dofdesc=desire_dd,
            allow_tangential_gradient=True,
        )
    elif p.cost_name == "geometry_tracking_volume":
        discr_d = places.get_discretization(desire_dd.geometry, desire_dd.discr_stage)
        vd = bind(discr_d, sym.surface_volume(discr_d.ambient_dim))(actx)
        context = {"xd": actx.thaw(discr_d.nodes()), "vd": vd}

        cg = pc.GeometryTrackingFunctional(
            xd=sym.make_sym_vector("xd", places.ambient_dim),
            t=sym.var("t"),
            dofdesc=desire_dd,
            allow_tangential_gradient=True,
        )
        cv = pc.VolumeTrackingFunctional(vd=sym.var("vd"))

        gamma = p.cost_arguments["gamma"]
        assert 0 <= gamma <= 1

        cost = gamma * cg + (1 - gamma) * cv
    elif p.cost_name == "normal":
        f, divf = _normal_test_data(places.ambient_dim)
        context = {}

        cost = pc.NormalTestFunctional(f=f, divf=divf)
    elif p.cost_name == "curvature":
        f, gradf, hessf = _curvature_test_data(places.ambient_dim)
        context = {}

        cost = pc.CurvatureTestFunctional(f=f, gradf=gradf, hessf=hessf)
    elif p.cost_name == "curvature_vector":
        f, gradf, hessf = _curvature_vector_test_data(places.ambient_dim)
        context = {}

        cost = pc.CurvatureVectorTestFunctional(f=f, gradf=gradf, hessf=hessf)
    elif p.cost_name == "tangential_gradient":
        f, gradf, hessf = _tangential_gradient_test_data(places.ambient_dim)
        context = {}

        cost = pc.TangentialGradientFunctional(f=f, gradf=gradf, hessf=hessf)
    elif p.cost_name == "normal_velocity":
        ud = bind(
            places,
            sym.n_dot(sym.nodes(places.ambient_dim).as_vector()),
            auto_where=desire_dd,
        )(actx)
        context = {"ud": ud}

        cost = pc.NormalVelocityFunctional(
            u=sym.nodes(places.ambient_dim).as_vector(),
            divu=places.ambient_dim,
            ud=sym.var("ud"),
            dofdesc=desire_dd,
        )
    else:
        raise ValueError(f"unrecognized cost: '{p.cost_name}'")

    return cost, context


# }}}


# {{{ stokes cost functional


def make_velocity_tracking_cost_functional(
    actx,
    places,
    op,
    *,
    lambdas,
    desire_dd: sym.DOFDescriptorLike,
    source_dd: sym.DOFDescriptorLike,
    gamma: float | None = None,
    context: dict[str, Any] | None = None,
    **kwargs,
):
    import pystopt.cost as pc

    cost = pc.NormalVelocityFunctional(
        u=sym.make_sym_vector("u", places.ambient_dim),
        ud=sym.var("ud"),
        dofdesc=desire_dd,
    )

    context = pc.get_desired_normal_velocity(
        actx, places, op, context=context, lambdas=lambdas, dofdesc=desire_dd
    )

    if gamma is not None:
        assert 0 <= gamma <= 1

        cost_xd = pc.GeometryTrackingFunctional(
            xd=sym.make_sym_vector("xd", places.ambient),
            t=sym.var("t"),
            dofdesc=desire_dd,
            allow_tangential_gradient=True,
        )
        cost = gamma * cost + (1 - gamma) * cost_xd

        discr_d = places.get_discretization(desire_dd.geometry, desire_dd.discr_stage)
        context.update({"xd": actx.thaw(discr_d.nodes())})

    return cost, context


def make_steady_cost_functional(
    actx,
    places,
    op,
    *,
    desire_dd: sym.DOFDescriptorLike,
    source_dd: sym.DOFDescriptorLike,
    gamma: float | None = None,
    **kwargs,
):
    if gamma is None:
        gamma = 0.5

    import pystopt.cost as pc

    cost_un = pc.NormalVelocityFunctional(
        u=sym.make_sym_vector("u", places.ambient_dim), ud=0, dofdesc=desire_dd
    )
    cost_vd = pc.VolumeTrackingFunctional(vd=sym.var("vd"))
    cost = (1 - gamma) * cost_un + gamma * cost_vd

    if kwargs.get("vd") is None:
        volume_d = actx.to_numpy(
            bind(places, sym.surface_volume(places.ambient_dim), auto_where=source_dd)(
                actx
            )
        )
    else:
        volume_d = kwargs["vd"]

    context = {"vd": volume_d}
    return cost, context


def make_stokes_cost_functional(
    actx,
    places,
    op,
    p,
    *,
    source_dd: sym.DOFDescriptorLike | None = None,
    desire_dd: sym.DOFDescriptorLike | None = None,
):
    if source_dd is None:
        source_dd = places.auto_source
    source_dd = sym.as_dofdesc(source_dd)

    if desire_dd is None:
        if "desired" in places.places:
            desire_dd = sym.DOFDescriptor("desired")
        else:
            desire_dd = source_dd

    desire_dd = sym.as_dofdesc(desire_dd)

    if p.cost_name == "steady":
        return make_steady_cost_functional(
            actx,
            places,
            op,
            source_dd=source_dd,
            desire_dd=desire_dd,
            **p.cost_arguments,
        )
    elif p.cost_name == "tracking":
        return make_velocity_tracking_cost_functional(
            actx,
            places,
            op,
            lambdas=p.lambdas,
            context=p.get_stokes_context(),
            source_dd=source_dd,
            desire_dd=desire_dd,
            **p.cost_arguments,
        )
    else:
        raise ValueError(f"unknown cost name: '{p.cost_name}'")


# }}}


# {{{ unsteady cost functionals


def make_unsteady_cost_functional(
    actx,
    places,
    op,
    p,
    *,
    source_dd: sym.DOFDescriptorLike | None = None,
    desire_dd: sym.DOFDescriptorLike | None = None,
):
    if source_dd is None:
        source_dd = places.auto_source
    source_dd = sym.as_dofdesc(source_dd)
    ambient_dim = places.ambient_dim

    import pystopt.cost as pc

    if p.cost_name == "geometry_tracking":
        discr_d = places.get_discretization(desire_dd.geometry, desire_dd.discr_stage)
        context = {"xd": actx.thaw(discr_d.nodes())}

        cost = pc.GeometryTrackingFunctional(
            xd=sym.make_sym_vector("xd", ambient_dim),
            t=sym.var("t"),
            dofdesc=desire_dd,
            allow_tangential_gradient=True,
        )
    elif p.cost_name == "uniform_geometry_tracking":
        assert places.ambient_dim == 2

        discr_d = places.get_discretization(desire_dd.geometry, desire_dd.discr_stage)
        context = {
            "xd": actx.thaw(discr_d.nodes()),
            "uinf_d": p.cost_arguments["uinf_d"][0],
            "vinf_d": p.cost_arguments["uinf_d"][1],
        }
        uinf = make_obj_array([sym.var("uinf_d"), sym.var("vinf_d")])

        cost = pc.UniformGeometryTrackingFunctional(
            xd=sym.make_sym_vector("xd", ambient_dim),
            uinf_d=uinf,
            t=sym.var("t"),
            dofdesc=desire_dd,
            allow_tangential_gradient=True,
        )
    elif p.cost_name == "sbr_geometry_tracking":
        discr_d = places.get_discretization(desire_dd.geometry, desire_dd.discr_stage)
        context = {"xd": actx.thaw(discr_d.nodes())}
        context.update(p.cost_arguments)

        if places.ambient_dim == 2:
            omega_d = sym.var("omega_d")
        else:
            omega_d = sym.make_sym_vector("omega_d", places.ambient_dim)

        cost = pc.SolidBodyRotationGeometryTrackingFunctional(
            xd=sym.make_sym_vector("xd", ambient_dim),
            omega_d=omega_d,
            t=sym.var("t"),
            dofdesc=desire_dd,
            allow_tangential_gradient=True,
        )
    elif p.cost_name == "bezier_uniform_geometry_tracking":
        discr_d = places.get_discretization(desire_dd.geometry, desire_dd.discr_stage)
        context = {"xd": actx.thaw(discr_d.nodes())}

        points_d = []
        for i, pts in enumerate(p.cost_arguments["points_d"]):
            name = f"bezier_p{i}_d"
            context[name] = pts
            points_d.append(sym.make_sym_vector(name, places.ambient_dim))

        cost = pc.BezierGeometryTrackingFunctional(
            xd=sym.make_sym_vector("xd", ambient_dim),
            points_d=tuple(points_d),
            t=sym.var("t") / sym.var("tmax"),
            allow_tangential_gradient=True,
            dofdesc=desire_dd,
        )
    elif p.cost_name == "helix_geometry_tracking":
        discr_d = places.get_discretization(desire_dd.geometry, desire_dd.discr_stage)

        context = {"xd": actx.thaw(discr_d.nodes())}
        context["height_d"] = p.cost_arguments["height_d"]
        context["omega_d"] = p.cost_arguments["omega_d"]

        cost = pc.HelixGeometryTrackingFunctional(
            xd=sym.make_sym_vector("xd", ambient_dim),
            height_d=sym.var("height_d"),
            omega_d=sym.var("omega_d"),
            t=sym.var("t") / sym.var("tmax"),
            allow_tangential_gradient=True,
            dofdesc=desire_dd,
        )
    elif p.cost_name == "sbr_centroid_tracking":
        xd = bind(
            places,
            sym.surface_centroid(places.ambient_dim, groupwise=True),
            auto_where=desire_dd,
        )(actx)

        context = {"xd": xd}
        context.update(p.cost_arguments)

        if places.ambient_dim == 2:
            omega_d = sym.var("omega_d")
        else:
            omega_d = sym.make_sym_vector("omega_d", places.ambient_dim)

        cost = pc.SolidBodyRotationCentroidTrackingFunctional(
            xd=sym.make_sym_vector("xd", ambient_dim),
            omega_d=omega_d,
            t=sym.var("t"),
        )
    elif p.cost_name == "helix_centroid_tracking":
        xd = bind(
            places,
            sym.surface_centroid(places.ambient_dim, groupwise=True),
            auto_where=desire_dd,
        )(actx)

        context = {"xd": xd}
        context.update(p.cost_arguments)

        cost = pc.HelixCentroidTrackingFunctional(
            xd=sym.make_sym_vector("xd", ambient_dim),
            omega_d=sym.var("omega_d"),
            height_d=sym.var("height_d"),
            t=sym.var("t") / sym.var("tmax"),
        )
    else:
        raise ValueError(f"unknown cost functional: {p.cost_name}")

    return cost, context


# }}}
