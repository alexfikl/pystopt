# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

from dataclasses import dataclass, field
from typing import Any

import numpy as np

from arraycontext import flatten
from pytools import memoize_method
from pytools.obj_array import make_obj_array

from pystopt import bind, sym
from pystopt.simulation import as_state_container
from pystopt.simulation.unsteady import (
    GeometryEvolutionState as GeometryEvolutionStateBase,
)
from pystopt.simulation.unsteady import (
    GeometryEvolutionWrangler as GeometryEvolutionWranglerBase,
)
from pystopt.tools import get_cl_array_context, get_default_logger

logger = get_default_logger(module=__file__)


# {{{


@as_state_container
class GeometryEvolutionState(GeometryEvolutionStateBase):
    @memoize_method
    def get_velocity_field(self):
        alpha = self.wrangler.context["alpha"]
        return self.wrangler.discr.zeros(self.array_context) + make_obj_array(
            [alpha, 0, 0][: self.wrangler.ambient_dim]
        )


@dataclass(frozen=True)
class GeometryEvolutionWrangler(GeometryEvolutionWranglerBase):
    def get_initial_state(self, actx):
        x0 = super().get_initial_state(actx)
        return GeometryEvolutionState(
            x=x0.x,
            t=np.array([0.0], dtype=object),
            wrangler=self,
        )


@dataclass(frozen=True)
class History:
    u_dot_n: list[float] = field(default_factory=list, repr=False)
    w_dot_t: list[float] = field(default_factory=list, repr=False)

    volume: list[float] = field(default_factory=list, repr=False)
    area: list[float] = field(default_factory=list, repr=False)

    h_max: list[float] = field(default_factory=list, repr=False)
    h_avg: list[float] = field(default_factory=list, repr=False)
    h_std: list[float] = field(default_factory=list, repr=False)

    def append(self, **kwargs: Any) -> None:
        self.u_dot_n.append(kwargs["u_dot_n"])
        self.w_dot_t.append(kwargs["w_dot_t"])

        self.volume.append(kwargs["volume"])
        self.area.append(np.sum(kwargs["area"]))

        self.h_max.append(np.max(kwargs["area"]))
        self.h_avg.append(np.mean(kwargs["area"]))
        self.h_std.append(np.std(kwargs["area"], ddof=1))

        logger.info(
            "volume %.12e area %.12e",
            abs(self.volume[-1] - self.volume[0]) / abs(self.volume[0]),
            self.area[-1],
        )

        if "cost" in kwargs:
            logger.info(
                "norm(u) %.5e norm(w) %.5e area %.5e ± %.5e norm(xlm) %.5e cost %.5e",
                self.u_dot_n[-1],
                self.w_dot_t[-1],
                self.h_avg[-1],
                self.h_std[-1],
                kwargs["xlm"],
                kwargs["cost"],
            )
        else:
            logger.info(
                "norm(u) %.5e norm(w) %.5e area %.5e ± %.5e",
                self.u_dot_n[-1],
                self.w_dot_t[-1],
                self.h_avg[-1],
                self.h_std[-1],
            )


def plot_solution(actx, basename, places, vis, v, u, u_dot_n, w, w_dot_t, dofdesc):
    if places.ambient_dim == 2:
        filename = basename.with_suffix("geometry")
        vis.write_file(
            filename,
            [],
            show_vertices=True,
            write_mode="geometry",
            aspect="equal",
            xlim=[-1.5, 1.5],
            ylim=[-1.25, 1.25],
            overwrite=True,
        )

        filename = basename.with_suffix("velocity")
        vis.write_file(
            filename,
            [
                (r"\mathbf{u}_n", u_dot_n),  # noqa: RUF027
                (r"\mathbf{w}_t", w_dot_t),  # noqa: RUF027
            ],
            markers=["-", "--"],
            overwrite=True,
        )
    else:
        vis.write_file(
            basename.aspath(),
            [
                ("v", v),
                ("u", u),
                ("u_dot_n", u_dot_n),
                ("w", w),
                ("w_dot_t", w_dot_t),
            ],
            overwrite=True,
        )


# }}}


def run(
    actx_factory,
    *,
    ambient_dim: int = 2,
    stab_name: str | None = None,
    stab_arguments: dict[str, Any] | None = None,
    visualize: bool = True,
):
    actx = get_cl_array_context(actx_factory)

    if visualize:
        from pystopt.paths import generate_filename_series, make_dirname

        dirname = make_dirname(
            f"stabilize_{ambient_dim}d_{stab_name}".lower(),
            today=False,
            cwd=make_dirname("output", today=True),
        )

        filenames = generate_filename_series("visualize", cwd=dirname)

    # {{{ geometry

    kwargs = {"target_order": 3}
    if ambient_dim == 2:
        mesh_name = "fourier_circle"
        resolution = 32
    elif ambient_dim == 3:
        mesh_name = "spharm_sphere"
        resolution = 32
    else:
        raise ValueError(f"unsupported dimension: {ambient_dim}")

    from pystopt.mesh import generate_discretization, get_mesh_generator_from_name

    gen = get_mesh_generator_from_name(mesh_name, **kwargs)
    orig_discr = generate_discretization(gen, actx, resolution)

    from pytential import GeometryCollection

    places = GeometryCollection(orig_discr, auto_where="stab")
    dofdesc = places.auto_source

    if visualize:
        from pystopt.visualization import make_visualizer

        orig_vis = make_visualizer(actx, orig_discr)

    # }}}

    # {{{ setup stabilizer

    import pystopt.stabilization as stab

    if stab_name is not None:
        if stab_arguments is None:
            if stab_name == "veerapaneni2011":
                # NOTE: the geometry is just a sphere with one mode
                stab_arguments = {"nmax": 3}
            else:
                stab_arguments = {}

        stabilizer = stab.make_stabilizer_from_name(stab_name, **stab_arguments)
    else:
        stabilizer = None

    # }}}

    # {{{ wrangler

    if places.ambient_dim == 2:
        filter_arguments = {"method": "ideal", "kmax": resolution // 2}
    else:
        filter_arguments = {
            "method": "tikhonov_ideal",
            "alpha": 1.0e-4,
            "p": 2,
            "kmax": resolution // 4,
        }

    from pystopt.evolution.stokes import StokesEvolutionOperator

    evolution = StokesEvolutionOperator(
        force_normal_velocity=True,
        force_tangential_velocity=stabilizer is not None,
    )

    from pystopt.filtering import FilterType

    wrangler = GeometryEvolutionWrangler(
        places=places,
        dofdesc=dofdesc,
        is_spectral=True,
        # evolution
        evolution=evolution,
        context={"alpha": 1.0},
        stabilizer_name=stab_name,
        stabilizer_arguments=stab_arguments,
        # filtering
        filter_type=FilterType.Full,
        filter_arguments=filter_arguments,
    )
    state0 = wrangler.get_initial_state(actx)

    history = History()

    # }}}

    # {{{ setup evolution source term

    from pystopt.mesh.processing import compute_group_volumes

    vd = compute_group_volumes(actx, state0.get_geometry_collection())

    def state_filter(state):
        from pystopt.simulation import rescale_state_to_volume

        return rescale_state_to_volume(state, vd=vd)

    def postprocess(t, state):
        places = state.get_geometry_collection()

        dofdesc = state.wrangler.dofdesc
        discr = places.get_discretization(dofdesc.geometry)

        # {{{ velocities

        u = state.get_velocity_field()
        w = state.get_tangential_forcing()

        u_dot_n = bind(
            places, sym.project_normal(places.ambient_dim), auto_where=dofdesc
        )(actx, x=u)
        w_dot_t = bind(
            places, sym.project_tangent(places.ambient_dim), auto_where=dofdesc
        )(actx, x=w)
        v = u_dot_n + w_dot_t

        # }}}

        # {{{ history

        area = actx.to_numpy(
            flatten(bind(places, sym.element_area(ambient_dim))(actx), actx)
        )
        volume = actx.to_numpy(bind(places, sym.surface_volume(ambient_dim))(actx))

        from pystopt.dof_array import dof_array_norm

        results = {
            "u_dot_n": actx.to_numpy(dof_array_norm(u_dot_n, ord=np.inf)),
            "w_dot_t": actx.to_numpy(dof_array_norm(w_dot_t, ord=2)),
            "area": area,
            "volume": volume,
        }

        if isinstance(stabilizer, stab.Veerapaneni2011Stabilizer):
            from pystopt.stabilization.veerapaneni2011 import _v2011_cost_functional

            results["xlm"] = actx.to_numpy(dof_array_norm(state.x, ord=2))
            results["cost"] = _v2011_cost_functional(
                actx, discr, stabilizer.alpha, dofdesc=dofdesc
            )

        history.append(**results)

        # }}}

        # {{{ visualize

        if visualize:
            from pystopt.visualization import make_same_connectivity_visualizer

            vis = make_same_connectivity_visualizer(actx, orig_vis, discr)

            plot_solution(
                actx,
                next(filenames),
                places,
                vis,
                v,
                u,
                u_dot_n,
                w,
                w_dot_t,
                dofdesc=dofdesc,
            )

        # }}}

    # }}}

    # {{{ time stepping

    from pystopt.evolution.estimates import fixed_time_step_from

    maxiter, tmax, dt = fixed_time_step_from(
        maxit=5,
        tmax=None,
        timestep=1.0e-2,
        # maxit=None, tmax=2.0, timestep=1.0e-2,
    )

    from pystopt.evolution import make_adjoint_time_stepper

    stepper = make_adjoint_time_stepper(
        "assprk22",
        "state",
        state0,
        wrangler.evolution_source_term,
        state_filter=state_filter,
        dt_start=dt,
        t_start=0.0,
    )

    for event in stepper.run(tmax=tmax, maxit=maxiter):
        state = event.state_component
        postprocess(event.t, state)

    # }}}

    if not visualize:
        return

    from pystopt.mesh import update_discr_from_spectral

    discr = update_discr_from_spectral(actx, orig_discr, state.x, keep_vertices=True)

    from pystopt.paths import get_filename
    from pystopt.visualization.matplotlib import subplots

    filename = get_filename("visualize_velocity", cwd=dirname)
    with subplots(filename.aspath(), overwrite=True) as fig:
        ax = fig.gca()
        ax.plot(history.u_dot_n, label=r"$\|\mathbf{u}_n\|$")
        ax.plot(history.w_dot_t, label=r"$\|\mathbf{w}_t\|$")

        ax.set_xlabel("$n$")
        ax.legend()

    filename = get_filename("visualize_area", cwd=dirname)
    with subplots(filename.aspath(), overwrite=True) as fig:
        ax = fig.gca()

        h_avg = np.array(history.h_avg)
        h_std = np.array(history.h_std)
        ax.fill_between(
            np.arange(h_avg.size),
            h_avg - h_std,
            h_avg + h_std,
            alpha=0.15,
        )
        ax.plot(history.h_max, label="$h_{max}$")
        ax.plot(h_avg, color="k", label="$h_{avg}$")

        ax.set_xlabel("$n$")
        ax.legend()

    if ambient_dim == 2:
        filename = get_filename("visualize_geometry", cwd=dirname)
        with subplots(filename.aspath(), overwrite=True) as fig:
            ax = fig.gca()

            nodes = orig_discr.nodes()
            x = actx.to_numpy(flatten(nodes[0], actx))
            y = actx.to_numpy(flatten(nodes[1], actx))
            ax.plot(x, y)

            nodes = discr.nodes()
            x = actx.to_numpy(flatten(nodes[0], actx))
            y = actx.to_numpy(flatten(nodes[1], actx))
            ax.plot(x, y)

            x, y = discr.mesh.vertices
            ax.plot(x, y, "ko", ms=3)
            x, y = orig_discr.mesh.vertices
            ax.plot(x, y, "ko", ms=3)

            ax.set_xlabel("$x$")
            ax.set_ylabel("$y$")
            ax.set_xlim([-1.25, 1.25])
            ax.set_ylim([-1.75, 1.75])
            ax.set_aspect("equal")


if __name__ == "__main__":
    import sys

    import pyopencl as cl

    if len(sys.argv) > 1:
        exec(sys.argv[1])
    else:
        run(cl._csc)
