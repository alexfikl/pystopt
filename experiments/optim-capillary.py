# SPDX-FileCopyrightText: 2022 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

from dataclasses import dataclass, replace
from typing import Any

import numpy as np

from arraycontext import ArrayContext

import pystopt.callbacks as cb
import pystopt.simulation as sim
from pystopt.tools import get_cl_array_context, get_default_logger

logger = get_default_logger(module=__file__)


# {{{ callbacks


class StokesEvolutionVisualizeCallback(sim.StokesEvolutionVisualizeCallback):
    def get_geometry_kwargs(self, state):
        return {"xlim": [-2.25, 2.25], "ylim": [-2.25, 2.25]}


# }}}


# {{{ wranglers


@dataclass(frozen=True)
class StokesEvolutionWrangler(sim.CapillaryStokesEvolutionWrangler):
    pass


@dataclass(frozen=True)
class AdjointStokesEvolutionWrangler(sim.AdjointCapillaryStokesEvolutionWrangler):
    pass


@dataclass(frozen=True)
class CapillaryOptimizationWrangler(sim.CapillaryOptimizationWrangler):
    visualize: bool

    def get_callback(self, actx, checkpoint_directory_name, *, prefix):
        if not self.visualize:
            return None

        if prefix == "forward":
            manager_factory = sim.StokesEvolutionCallbackManager
            visualize_callback_factory = StokesEvolutionVisualizeCallback
        else:
            manager_factory = sim.AdjointStokesEvolutionCallbackManager
            visualize_callback_factory = sim.AdjointStokesEvolutionVisualizeCallback

        from pystopt.dof_array import dof_array_norm

        return cb.make_default_evolution_callback(
            checkpoint_directory_name / "dummy",
            manager_factory=manager_factory,
            history_callback_factory=False,
            checkpoint_callback_factory=False,
            visualize_callback_factory=visualize_callback_factory,
            norm=dof_array_norm,
            prefix=prefix,
            overwrite=True,
        )


# }}}


# {{{ run


def run(
    actx: ArrayContext,
    p,
    *,
    eps: float | np.ndarray,
    overwrite: bool = True,
    visualize: bool = True,
) -> tuple[float, float]:
    import extra_optim_data as eod

    # {{{ geometry

    places, _ = eod.get_geometry_collection_from_param(actx, p, qbx=True)
    dofdesc = places.auto_source
    desire_dd = dofdesc.copy(geometry="desired")

    discr = places.get_discretization(dofdesc.geometry, dofdesc.discr_stage)
    logger.info("nspec:     %d", discr.nspec)
    logger.info("ndofs:     %d", discr.ndofs)
    logger.info("nelements: %d", discr.mesh.nelements)

    from pystopt import bind, sym

    h_max = bind(places, sym.h_max_from_volume(places.ambient_dim), auto_where=dofdesc)(
        actx
    )
    h_max = actx.to_numpy(h_max)

    # }}}

    # {{{ stokes

    from pystopt import stokes

    bc = stokes.get_farfield_boundary_from_name(
        p.ambient_dim, p.farfield_name, **p.farfield_arguments
    )
    st_op = stokes.TwoPhaseSingleLayerStokesRepresentation(bc)

    from pystopt.evolution.stokes import StokesEvolutionOperator

    evo_op = StokesEvolutionOperator(
        force_normal_velocity=True, force_tangential_velocity=False
    )

    # }}}

    # {{{ cost

    cost, context = eod.make_shape_cost_functional(
        actx,
        places,
        p,
        source_dd=dofdesc,
        desire_dd=desire_dd,
    )
    context.update(p.stokes_arguments)

    # }}}

    # {{{ evolution

    from pystopt.evolution import TimeWrangler

    time = TimeWrangler(
        time_step_name=p.time_step_method,
        maxit=p.maxit,
        tmax=p.tmax,
        dt=p.timestep,
        theta=1.0,
        estimate_time_step=False,
    )

    forward = StokesEvolutionWrangler(
        # geometry
        places=places,
        dofdesc=dofdesc,
        is_spectral=True,
        keep_reconstruction_vertices=p.force_tangential_velocity,
        # stokes
        op=st_op,
        lambdas=p.stokes_lambdas,
        gmres_arguments={},
        # cost
        cost=None,
        cost_final=cost,
        # evolution
        evolution=evo_op,
        filter_type=p.grad_filtered,
        filter_arguments=p.filter_arguments,
        stabilizer_arguments=p.tangential_forcing_arguments,
        context=context,
    )

    # }}}

    # {{{ optimization

    from pystopt.paths import generate_filename_series

    checkpoint_directory_series = generate_filename_series("evolution", cwd=p.dirname)

    wrangler = CapillaryOptimizationWrangler(
        time=time,
        forward=forward,
        checkpoint_directory_series=checkpoint_directory_series,
        visualize=visualize,
    )

    # }}}

    # {{{ cost

    state_m = wrangler.get_initial_state(actx)
    cost_m = state_m.cost
    logger.info("cost_m: %.12e", cost_m)

    # }}}

    # {{{ gradient

    if eps is not None:
        is_eps_float = isinstance(eps, float)
        if is_eps_float:
            eps = np.array([eps])

        # finite difference
        dc_dca_fd = np.empty(eps.size)

        for i in range(eps.size):
            state_p = replace(state_m, x=state_m.x + eps[i])

            cost_p = state_p.cost
            logger.info("cost_p: %.12e", cost_p)

            dc_dca_fd[i] = (cost_p - cost_m) / eps[i]
            logger.info("gradient[fd]: eps %.15e grad %.15e", eps[i], dc_dca_fd[i])

    # adjoint
    dc_dca_ad = state_m.gradient
    logger.info("gradient[ad]: %.15e", dc_dca_ad)

    # }}}

    if eps is not None:
        if is_eps_float:
            dc_dca_fd = float(dc_dca_fd[0])
    else:
        dc_dca_fd = np.inf

    return dc_dca_fd, dc_dca_ad, h_max


def prepare_run(
    ctx_factory_or_actx,
    *,
    ambient_dim: int = 2,
    suffix: str = "v0",
    eps: float | np.ndarray = 1.0e-2,
    extra_kwargs: dict[str, Any] | None = None,
    cached: bool = False,
    overwrite: bool = True,
    visualize: bool = True,
):
    actx = get_cl_array_context(ctx_factory_or_actx)
    if extra_kwargs is None:
        extra_kwargs = {}

    # {{{ parameters

    if ambient_dim == 2:
        from extra_mesh_data import StokesCapillary2d_2021_10_12 as Parameters
    elif ambient_dim == 3:
        from extra_mesh_data import StokesCapillary3d_2021_10_12 as Parameters
    else:
        raise ValueError(f"unsupported dimension: {ambient_dim}")

    import extra_mesh_data as emd

    p = emd.make_param_from_class(
        Parameters,
        name="optim-capillary",
        suffix=suffix,
        overwrite=overwrite,
        **extra_kwargs,
    )
    assert p.ambient_dim == ambient_dim

    # }}}

    # {{{ output

    from pystopt.checkpoint import make_hdf_checkpoint_manager

    checkpoint = make_hdf_checkpoint_manager(p.checkpoint_file_name)

    # }}}

    from pystopt.checkpoint.hdf import array_context_for_pickling

    with array_context_for_pickling(actx):
        if cached and "result" in checkpoint:
            r = checkpoint.read_from("result")
            grad_ad = r["grad_ad"]
            grad_fd = r["grad_fd"]
            h_max = r["h_max"]
        else:
            logger.info("\n%s", p)
            logger.info("dirname:   %s", p.dirname)

            grad_fd, grad_ad, h_max = run(
                actx, p, eps=eps, overwrite=overwrite, visualize=visualize
            )

            from pystopt.tools import dc_asdict

            checkpoint.write_to("parameters", dc_asdict(p), overwrite=True)
            checkpoint.write_to(
                "result",
                {
                    "grad_fd": grad_fd,
                    "grad_ad": grad_ad,
                    "h_max": h_max,
                },
                overwrite=True,
            )

    logger.info("h_max %.5e grad_ad %.5e grad_fd %s", h_max, grad_ad, grad_fd)

    return grad_fd, grad_ad, h_max


def main(
    ctx_factory_or_actx,
    *,
    ambient_dim: int = 2,
    viscosity_ratio: float = 1.0,
    capillary_number: float = 0.05,
    suffix: str = "v0",
    overwrite: bool = True,
    visualize: bool = True,
):
    prepare_run(
        ctx_factory_or_actx,
        ambient_dim=ambient_dim,
        extra_kwargs={
            "viscosity_ratio": viscosity_ratio,
            "capillary_number": capillary_number,
        },
        eps=None,
        suffix=suffix,
        overwrite=overwrite,
        visualize=visualize,
    )


def main_run_multiple_capillary(
    ctx_factory_or_actx,
    *,
    ambient_dim: int = 2,
    viscosity_ratio: float = 1.0,
    suffix: str = "v0",
    istart: int = 0,
    cached: bool = True,
    overwrite: bool = True,
    visualize: bool = False,
):
    actx = get_cl_array_context(ctx_factory_or_actx)

    from pystopt.paths import get_filename, make_dirname

    dirname = make_dirname("output", today=True)
    filename = get_filename(
        f"capillary_optim_gradients_{suffix}",
        ext="npz",
        cwd=dirname,
    ).aspath()

    if cached and filename.exists():
        data = np.load(filename)

        ca = data["ca"]
        eps = data["eps"]
        grad_fd = data["grad_fd"]
        grad_ad = data["grad_ad"]
    else:
        if ambient_dim == 2:
            ca = np.linspace(0.01, 0.1, 10)
        else:
            ca = np.array([0.05, 0.07, 0.1])
        eps = np.array([1.0e-2, 1.0e-3, 1.0e-4])
        grad_fd = np.empty((eps.size, ca.size))
        grad_ad = np.empty(ca.size)

        for j in range(ca.size):
            grad_fd[:, j], grad_ad[j], _ = prepare_run(
                actx,
                ambient_dim=ambient_dim,
                extra_kwargs={
                    "viscosity_ratio": viscosity_ratio,
                    "capillary_number": ca[j],
                },
                eps=eps,
                suffix=f"ca_{j}_{suffix}",
                cached=cached,
                overwrite=overwrite,
                visualize=visualize,
            )

        np.savez_compressed(
            filename,
            # extra parameters
            ambient_dim=ambient_dim,
            viscosity_ratio=viscosity_ratio,
            # results
            ca=ca,
            eps=eps,
            grad_fd=grad_fd,
            grad_ad=grad_ad,
        )

    from pystopt.measure import EOCRecorder, stringify_eoc

    for i in range(istart, ca.size):
        eoc = EOCRecorder(name=f"{ca[i]:g}")
        for j in range(eps.size):
            error = abs(grad_fd[j, i] - grad_ad[i]) / abs(grad_fd[j, i])
            eoc.add_data_point(eps[j], error)

        logger.info("\n%s", stringify_eoc(eoc))

    from pystopt.visualization.matplotlib import subplots

    filename = get_filename(
        f"capillary_optim_visualize_gradient_{suffix}",
        cwd=dirname,
    ).aspath()

    with subplots(filename, overwrite=overwrite) as fig:
        ax = fig.gca()

        ax.plot(ca[istart:], grad_ad[istart:], "k--", label="$Adjoint$")
        for i in range(eps.size):
            ax.plot(ca[istart:], grad_fd[i][istart:], label=f"${eps[i]:.0e}$")

        ax.set_xlabel(r"$\mathrm{Ca}$")
        ax.set_ylabel(r"$\nabla J$")
        ax.legend(
            bbox_to_anchor=(0, 1.02, 1.0, 0.2),
            loc="lower left",
            mode="expand",
            borderaxespad=0,
            ncol=3,
            prop={"size": 24},
        )


def main_run_convergence(
    ctx_factory_or_actx,
    *,
    ambient_dim: int = 2,
    viscosity_ratio: float = 1.0,
    capillary_number: float = 0.05,
    suffix: str = "v0",
    cached: str | bool = True,
    overwrite: bool = True,
    visualize: bool = False,
):
    actx = get_cl_array_context(ctx_factory_or_actx)

    import pathlib

    from pystopt.paths import get_filename, make_dirname

    if isinstance(cached, str):
        filename = pathlib.Path(cached)
        dirname = filename.parent
        cached = True
    else:
        dirname = make_dirname("output", today=True)
        filename = get_filename(
            f"capillary_optim_convergence_{suffix}",
            ext="npz",
            cwd=dirname,
        ).aspath()

    if cached and filename.exists():
        data = np.load(filename)

        eps = data["eps"]
        resolutions = data["resolutions"]
        h_max = data["h_max"]

        grad_fd = data["grad_fd"]
        grad_ad = data["grad_ad"]
    else:
        if ambient_dim == 2:
            resolutions = np.array([16, 32, 64, 64, 96, 128, 160, 196])
            eps = 10.0 ** -np.arange(2, 9)
        else:
            resolutions = np.array([16, 24, 32, 48])
            eps = 10.0 ** -np.arange(2, 9)
            eps = np.array([1.0e-6])

        logger.info("resolutions:   %s", resolutions)
        logger.info("eps:           %s", eps)
        logger.info("=" * 42)

        grad_fd = np.empty((resolutions.size, eps.size))
        grad_ad = np.empty(resolutions.size)
        h_max = np.empty(resolutions.size)

        for j in range(resolutions.size):
            grad_fd[j, :], grad_ad[j], h_max[j] = prepare_run(
                actx,
                ambient_dim=ambient_dim,
                extra_kwargs={
                    "viscosity_ratio": viscosity_ratio,
                    "capillary_number": capillary_number,
                    "resolution": resolutions[j],
                },
                eps=eps,
                suffix=f"resolutions_{resolutions[j]}_{suffix}",
                cached=cached,
                overwrite=overwrite,
                visualize=visualize,
            )

        np.savez_compressed(
            filename,
            # extra parameters
            ambient_dim=ambient_dim,
            viscosity_ratio=viscosity_ratio,
            capillary_number=capillary_number,
            # results
            resolutions=resolutions,
            h_max=h_max,
            eps=eps,
            grad_fd=grad_fd,
            grad_ad=grad_ad,
        )

    from pystopt.measure import EOCRecorder, stringify_eoc, visualize_eoc

    eocf = [EOCRecorder(name=f"{r}") for r in resolutions]
    eocs = [EOCRecorder(name=f"{eps[i]:.1e}") for i in range(eps.size)]

    for i in range(eps.size):
        for j in range(resolutions.size):
            error = abs(grad_fd[j, i] - grad_ad[j]) / abs(grad_fd[j, i])

            eocf[j].add_data_point(eps[i], error)
            eocs[i].add_data_point(h_max[j], error)

    logger.info("\n%s", stringify_eoc(*eocf, table_format="latex"))
    logger.info("\n%s", stringify_eoc(*eocs, table_format="latex"))

    filename = get_filename(
        f"capillary_optim_visualize_convergence_eps_{suffix}",
        cwd=dirname,
    ).aspath()
    visualize_eoc(filename, *eocf, order=1, enable_legend=False, overwrite=True)

    filename = get_filename(
        f"capillary_optim_visualize_convergence_hmax_{suffix}",
        cwd=dirname,
    ).aspath()
    # FIXME: having this 2 hardcoded here is not great :(
    visualize_eoc(filename, *eocs, order=2, enable_legend=False, overwrite=True)

    from pystopt.visualization.matplotlib import subplots

    filename = get_filename(
        f"capillary_optim_visualize_adjoint_{suffix}",
        cwd=dirname,
    ).aspath()

    with subplots(filename, overwrite=True) as fig:
        ax = fig.gca()

        ax.plot(h_max, grad_ad, "o-")
        ax.axhline(grad_fd[-1, -1], ls="--", color="k")
        # ax.plot(4.600589447179004e-01, ls="--", color="k")
        ax.set_xlabel("$h_{max}$")
        ax.set_ylabel(r"$\nabla J_{ad}$")


# }}}


if __name__ == "__main__":
    import sys

    import pyopencl as cl

    if len(sys.argv) > 1:
        exec(sys.argv[1])
    else:
        main(cl._csc)
